﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Test.Stubs;
using uWebshop.Umbraco.Test;

namespace uWebshop.Test.Services.StoreServiceTests
{
	[TestFixture]
	public class LoadStoreUrlTests
	{
		const int StoreId = 7351;
		private IStoreService _storeService;
		private Store _store;
		private Mock<ICMSApplication> _cmsApplicationMock;
		private Mock<ICMSEntityRepository> _cmsEntityRepositoryMock;

		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			IOC.CMSApplication.Mock(out _cmsApplicationMock);
			_store = new Store { Id = StoreId };
		}
		
		[Test]
		public void LoadStoreUrl_StorePickerNotFound_SetsRoot()
		{
			IOC.CMSEntityRepository.Mock();
			_storeService = IOC.StoreService.Actual().Resolve();

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnRootNodeWithHideTopLevelAndDomainNotFound_SetsRoot()
		{
			IOC.CMSEntityRepository.Mock(out _cmsEntityRepositoryMock);
			_cmsEntityRepositoryMock.Setup(m => m.GetNodeWithStorePicker(StoreId)).Returns(new UwbsNode {Level = 1});
			_cmsApplicationMock.Setup(m => m.HideTopLevelNodeFromPath).Returns(true);
			_cmsApplicationMock.Setup(m => m.GetDomainForNodeId(It.IsAny<int>())).Returns(() => null);
			_storeService = IOC.StoreService.Actual().Resolve();

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnRootNodeWithHideTopLevel_SetsDomain()
		{
			ConfigureNodeChainWithHideTopLevelNode("http://mijndomain.nl/", "ignored");

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("http://mijndomain.nl/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnRootNodeWithHideTopLevel_SetsDomainSlashAdded()
		{
			ConfigureNodeChainWithHideTopLevelNode("http://mijndomain.nl", "ignored");

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("http://mijndomain.nl/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnLevel2Node_SetsBothUrls()
		{
			ConfigureNodeChainWithUrls("part1", "part2");

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("/part1/part2/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnLevel3Node_SetsThreeUrls()
		{
			ConfigureNodeChainWithUrls("part1", "part2", "part3");

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("/part1/part2/part3/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnLevel2NodeWithHideTopLevelShortDomainWithSlash_SetsDomainPlusUrl()
		{
			ConfigureNodeChainWithHideTopLevelNode("http://mijndomain.nl/", "ignored", "part2");

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("http://mijndomain.nl/part2/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnLevel2NodeWithHideTopLevelFullDomainName_SetsDomainPlusUrl()
		{
			ConfigureNodeChainWithHideTopLevelNode("http://mijndomain.nl/", "ignored", "part2");

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("http://mijndomain.nl/part2/", _store.StoreURL);
		}

		[Test]
		public void LoadStoreUrl_OnRootNode_SetsUrlName()
		{
			ConfigureNodeChainWithUrls("store");

			_storeService.LoadStoreUrl(_store);

			Assert.AreEqual("/store/", _store.StoreURL);
		}


		[Test]
		public void CategoryNiceUrl_NoStorePickerSet_ReturnsSlashCategoryUrl()
		{
			IOC.StoreService.Actual();
			
			IOC.CMSEntityRepository.Mock(out _cmsEntityRepositoryMock);
			_cmsEntityRepositoryMock.Setup(m => m.GetByGlobalId(1)).Returns(new UwbsNode {NodeTypeAlias = Category.NodeAlias});
			_cmsEntityRepositoryMock.Setup(m => m.GetObjectsByAliasUncached<Store>(Store.NodeAlias, It.IsAny<string>(), It.IsAny<int>())).Returns(new List<Store> {StubStore.CreateDefaultStore()});
			Mock<ICategoryService> catalogServiceMock;
			IOC.CategoryService.Mock(out catalogServiceMock);
			catalogServiceMock.Setup(m => m.GetById(1, It.IsAny<string>())).Returns(new Category{UrlName = "shoes", ParentId = -1});

			Assert.AreEqual(IOC.StoreService.Resolve(), StoreHelper.StoreService);

			var actual = StoreHelper.GetNiceUrl(1);

			Assert.AreEqual("/shoes", actual);
		}


		private void ConfigureNodeChainWithUrls(params string[] urls)
		{
			var nodes = UwbsNodes(urls);

			_cmsEntityRepositoryMock = IOC.CMSEntityRepository.SetupFake(nodes.ToArray());
			_storeService = IOC.StoreService.Actual().Resolve();

			_cmsEntityRepositoryMock.Setup(m => m.GetNodeWithStorePicker(StoreId)).Returns(nodes.Last());
			_cmsApplicationMock.Setup(m => m.HideTopLevelNodeFromPath).Returns(false);
		}

		private static List<UwbsNode> UwbsNodes(string[] urls)
		{
			var nodes = urls.Select(url => new UwbsNode {UrlName = url}).ToList();
			int parentId = -1;
			int level = 1;
			foreach (var node in nodes)
			{
				node.Id = level;
				node.Level = level;
				node.ParentId = parentId;
				parentId = level;
				level++;
			}
			return nodes;
		}

		private void ConfigureNodeChainWithHideTopLevelNode(string domainForTopLevelNode, params string[] urls)
		{
			var nodes = UwbsNodes(urls);

			_cmsEntityRepositoryMock = IOC.CMSEntityRepository.SetupFake(nodes.ToArray());
			_storeService = IOC.StoreService.Actual().Resolve();

			_cmsEntityRepositoryMock.Setup(m => m.GetNodeWithStorePicker(StoreId)).Returns(nodes.Last());
			_cmsApplicationMock.Setup(m => m.HideTopLevelNodeFromPath).Returns(true);
			_cmsApplicationMock.Setup(m => m.GetDomainForNodeId(nodes.First().Id)).Returns(domainForTopLevelNode);
		}

	}
}
