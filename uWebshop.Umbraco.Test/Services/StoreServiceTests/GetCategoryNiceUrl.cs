﻿using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco.Test;

namespace uWebshop.Test.Services.StoreServiceTests
{
	[TestFixture]
	public class GetCategoryNiceUrl
	{
		private Category _category;
		private IStoreService _storeService;

		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			UwebshopRequest.Current.CurrentStore = DefaultFactoriesAndSharedFunctionality.CreateDefaultStore();
			_category = new Category { UrlName = "cat", Id = 1234, ParentId = 0 };

			_storeService = IOC.StoreService.Actual().Resolve();
		}

		[Test]
		public void SingleLevelIncludingDomainAndHideTopLevel()
		{
			var actual = _storeService.GetCategoryNiceUrl(_category);

			Assert.AreEqual("http://my.uwebshop.com/cat", actual);
		}

		[Test]
		public void SingleLevelNoHideTopLevel()
		{
			_storeService.GetCurrentStore().StoreURL = "/nl/";

			var actual = _storeService.GetCategoryNiceUrl(_category);

			Assert.AreEqual("/nl/cat", actual);
		}
	}
}
