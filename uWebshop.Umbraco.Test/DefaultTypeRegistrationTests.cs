﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Domain.IoCContainerTests
{
	[TestFixture]
	public class DefaultTypeRegistrationTests
	{
		[Test]
		public void RegisterActualTypesAndResolveSomeKeyServices()
		{
			uWebshop.Umbraco.RegisterTypes.Register(true);
			IO.Container.Resolve<IUrlRewritingService>();
			IO.Container.Resolve<IProductService>();
			IO.Container.Resolve<IProductVariantService>();
		}
		
		[Test]
		public void ServicesDependencies()
		{
			var fakeContainer = new FakeContainer();
			IO.Container = fakeContainer;
			uWebshop.Umbraco.RegisterTypes.Register(true);

			var dependencies = fakeContainer.TypeMap.Values.ToDictionary(type => type, 
				type => type.GetConstructors().First().GetParameters().Select(p => p.ParameterType).ToList());

			foreach (var dependency in dependencies.OrderBy(k => k.Value.Count))
			{
				if (dependency.Value.Any())
					Console.WriteLine("" + dependency.Key.Name + " directly depends on " + string.Join(", ", dependency.Value.Select(p => p.Name)));
				else
					Console.WriteLine(dependency.Key.Name + " has no dependencies");
			}
			IO.Container = new IoCContainer();
		}
		class FakeContainer : IIocContainer
		{
			public readonly Dictionary<Type, Type> TypeMap = new Dictionary<Type, Type>();
			public void RegisterType<T, T1>() where T1 : T
			{
				TypeMap.Add(typeof(T), typeof(T1));
			}
			public T Resolve<T>() where T : class
			{
				return null;
			}
			public void RegisterInstance<T>(T instance)
			{
				throw new NotImplementedException();
			}
			public void SetDefaultServiceFactory(IServiceFactory serviceFactory)
			{
				throw new NotImplementedException();
			}
		}
	}
}
