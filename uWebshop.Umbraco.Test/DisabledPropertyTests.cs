﻿using System;
using System.Linq;
using System.Text;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Test.Repositories;
using uWebshop.Test.Services.OrderUpdatingsServiceTests;
using uWebshop.Umbraco.Repositories;
using uWebshop.Umbraco.Test;

namespace uWebshop.Test.Domain.Domain_classes.ProductTests
{
	[TestFixture]
	public class DisabledPropertyTests
	{
		private UmbracoProductRepository _repository;
		private Product _product;
		private TestPropertyProvider _propertyProvider;
		const string StoreAlias = "TestStoreAlias";

		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			_repository = (UmbracoProductRepository)IOC.ProductRepository.Actual().Resolve();
            _product = new Product { ParentId = 0 };
			_propertyProvider = new TestPropertyProvider();
		}

		[Test]
		public void LoadProductProperties_WithoutSetting_ShouldDefaultToDisabledFalse()
		{
			_repository.LoadDataFromPropertiesDictionary(_product, _propertyProvider, StoreAlias);

			Assert.False(_product.Disabled);
		}


		[Test]
		public void LoadProductProperties_WithGlobalDisabledTrue_ShouldSetDisabledTrue()
		{
			SetGlobalDisablePropertyTrue();

			_repository.LoadDataFromPropertiesDictionary(_product, _propertyProvider, StoreAlias);

			Assert.True(_product.Disabled);
		}

		[Test]
		public void LoadProductProperties_WithGlobalDisabledFalseStoreTrue_ShouldSetDisabledTrue()
		{
			SetGlobalDisablePropertyFalse();
			SetStoreDisablePropertyTrue();

			_repository.LoadDataFromPropertiesDictionary(_product, _propertyProvider, StoreAlias);

			Assert.True(_product.Disabled);
		}

		private void SetGlobalDisablePropertyTrue()
		{
			_propertyProvider.Dictionary.Add("disable", "1");
		}
		private void SetGlobalDisablePropertyFalse()
		{
			_propertyProvider.Dictionary.Add("disable", "0");
		}
		private void SetStoreDisablePropertyTrue()
		{
			_propertyProvider.Dictionary.Add(StoreHelper.CreateMultiStorePropertyAlias("disable", StoreAlias), "1");
		}
	}
}
