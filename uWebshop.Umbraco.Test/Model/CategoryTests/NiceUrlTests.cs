﻿using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Test;

namespace uWebshop.Umbraco.Test.Model.CategoryTests
{
	[TestFixture]
	public class NiceUrlTests
	{
		private Category _category;

		[SetUp]
		public void Setup()
		{
			IOC.IntegrationTest();
			IOC.StoreService.Actual();
			_category = new Category {UrlName = "cat", Id = 1234, ParentId = 0};
			UwebshopRequest.Current.CurrentStore = DefaultFactoriesAndSharedFunctionality.CreateDefaultStore();
		}

		[Test]
		public void SingleLevelIncludingDomainAndHideTopLevel()
		{
			var actual = _category.NiceUrl();

			Assert.AreEqual("http://my.uwebshop.com/cat", actual);
		}

		[Test]
		public void SingleLevelNoHideTopLevel()
		{
			IOC.StoreService.Resolve().GetCurrentStore().StoreURL = "/nl/";

			var actual = _category.NiceUrl();

			Assert.AreEqual("/nl/cat", actual);
		}
	}
}
