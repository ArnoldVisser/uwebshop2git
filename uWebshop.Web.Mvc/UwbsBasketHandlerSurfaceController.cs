﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using uWebshop.Domain.Helpers;

namespace uWebshop.Web.Mvc
{
	public class uwbsBasketHandlerSurfaceController : SurfaceController
	{
		[HttpPost]
		public ActionResult Handle()
		{
			var redirectAfterHandle = new BasketRequestHandler().HandleuWebshopBasketRequest(Request.Params);
			if (redirectAfterHandle && !Request.Params.AllKeys.Any(x => x == "disableReload"))
			{
				if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.RawUrl))
					Response.Redirect(System.Web.HttpContext.Current.Request.RawUrl, true);

				if (System.Web.HttpContext.Current.Request.UrlReferrer != null)
					Response.Redirect(System.Web.HttpContext.Current.Request.UrlReferrer.AbsolutePath, true);
				return new EmptyResult(); // todo: think about/experiment with what to return
			}

			return RedirectToCurrentUmbracoPage();
		}
	}
}