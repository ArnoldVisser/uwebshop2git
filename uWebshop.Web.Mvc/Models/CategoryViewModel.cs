﻿using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using uWebshop.Domain;

namespace uWebshop.Web.Mvc.Models
{
	public class CategoryViewModel : RenderModel
	{
		public CategoryViewModel(IPublishedContent content, CultureInfo culture) : base(content, culture)
		{
		}
		
		public CategoryViewModel(IPublishedContent content) : base(content)
		{
		}

		public Category Category { get; internal set; }
	}
}