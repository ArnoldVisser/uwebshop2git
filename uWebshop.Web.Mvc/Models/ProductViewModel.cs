﻿using System.Globalization;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using uWebshop.Domain;

namespace uWebshop.Web.Mvc.Models
{
	public class ProductViewModel : RenderModel
	{
		public ProductViewModel(IPublishedContent content, CultureInfo culture) : base(content, culture)
		{
		}

		public ProductViewModel(IPublishedContent content) : base(content)
		{
		}

		public Product Category { get; internal set; }
	}
}