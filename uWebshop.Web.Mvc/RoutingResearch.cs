﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Xml;
using Umbraco.Core;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Businesslogic;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Web.Mvc.Models;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.NodeFactory;
using umbraco.cms.businesslogic.web;


[assembly: PreApplicationStartMethod(typeof(uWebshop.Web.Mvc.RouteSetup), "Setup")]

namespace uWebshop.Web.Mvc
{
	public class RouteSetup
	{
		public static void Setup()
		{
			RenderMvcController.AfterTemplateDecided += RenderMvcControllerOnAfterTemplateDecided; // this will work when our pull request has been pulled
			//System.Web.Mvc.DependencyResolver.Current.GetService<ICMSApplication>();
			//throw new Exception("hij komt hier!");
			//RouteTable.Routes.Add(new MyUrlRoute()); // Add before your default Routes
			//RouteTable.Routes.MapRoute(
			//    "Default", // Route name
			//    "{controller}/{action}/{id}", // URL with parameters
			//    new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
			//);
//            RouteTable.Routes.MapRoute(
//    "iets", // Route name
//    "cat/{controller}/{action}", // URL with parameters
//    new { controller = "RenderMvc", action = "Category" } // Parameter defaults
//);
		}

		//#region to change MVC view with event

		private static void RenderMvcControllerOnAfterTemplateDecided(AfterTemplateDecidedEventArgs afterTemplateDecidedEventArgs)
		{
			//var page = (UmbracoDefault)sender;
			var currentCategory = UwebshopRequest.Current.Category;
			var currentProduct = UwebshopRequest.Current.Product;

			if (currentProduct == null && currentCategory != null)
			{
				if (currentCategory.Disabled)
				{
					HttpContext.Current.Response.StatusCode = 404;
					HttpContext.Current.Response.Redirect(library.NiceUrl(int.Parse(GetCurrentNotFoundPageId())));
					return;
				}

				var hasAccess = Access.HasAccess(currentCategory.Id, currentCategory.Path, Membership.GetUser());
				if (hasAccess)
				{
					if (currentCategory.Template != 0)
					{
						var template = new template(currentCategory.Template);
						afterTemplateDecidedEventArgs.Template = template.TemplateAlias;
						afterTemplateDecidedEventArgs.Model = new CategoryViewModel(new UmbracoHelper(UmbracoContext.Current).TypedContent(currentCategory.Id))
							{
								Category = currentCategory
							};
					}
				}
				else
				{
					var loginPageId = Access.GetLoginPage(currentCategory.Path);
					var errorPageId = Access.GetErrorPage(currentCategory.Path);

					//Log.Instance.LogDebug( DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " UmbracoDefaultBeforeRequestInit eind");
					if (HttpContext.Current.User.Identity.IsAuthenticated)
					{
						HttpContext.Current.Response.Redirect(library.NiceUrl(errorPageId));
					}
					HttpContext.Current.Response.Redirect(library.NiceUrl(loginPageId));
				}
			}
			else if (currentProduct != null)
			{
				var hasAccess = Access.HasAccess(currentProduct.Id, currentProduct.Path, Membership.GetUser());

				if (hasAccess)
				{
					if (currentProduct.Template != 0)
					{
						var template = new template(currentProduct.Template);
						afterTemplateDecidedEventArgs.Template = template.TemplateAlias;
						afterTemplateDecidedEventArgs.Model = new ProductViewModel(new UmbracoHelper(UmbracoContext.Current).TypedContent(currentProduct.Id))
							{
								Category = currentProduct
							};
					}
				}
				else
				{
					var loginPageId = Access.GetLoginPage(currentProduct.Path);
					var errorPageId = Access.GetErrorPage(currentProduct.Path);

					//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " UmbracoDefaultBeforeRequestInit eind");
					if (HttpContext.Current.User.Identity.IsAuthenticated)
					{
						HttpContext.Current.Response.Redirect(library.NiceUrl(errorPageId));
					}
					HttpContext.Current.Response.Redirect(library.NiceUrl(loginPageId));
				}
			}
		}
		internal static string GetCurrentNotFoundPageId()
		{
			library.GetCurrentDomains(1);
			string error404 = "";
			XmlNode error404Node = UmbracoSettings.GetKeyAsNode("/settings/content/errors/error404");
			if (error404Node.ChildNodes.Count > 0 && error404Node.ChildNodes[0].HasChildNodes)
			{
				// try to get the 404 based on current culture (via domain)
				XmlNode cultureErrorNode;
				if (umbraco.cms.businesslogic.web.Domain.Exists(HttpContext.Current.Request.ServerVariables["SERVER_NAME"]))
				{
					umbraco.cms.businesslogic.web.Domain d = umbraco.cms.businesslogic.web.Domain.GetDomain(HttpContext.Current.Request.ServerVariables["SERVER_NAME"]);
					// test if a 404 page exists with current culture
					cultureErrorNode = error404Node.SelectSingleNode(String.Format("errorPage [@culture = '{0}']", d.Language.CultureAlias));
					if (cultureErrorNode != null && cultureErrorNode.FirstChild != null)
						error404 = cultureErrorNode.FirstChild.Value;
				}
				else if (error404Node.SelectSingleNode(string.Format("errorPage [@culture = '{0}']", System.Threading.Thread.CurrentThread.CurrentUICulture.Name)) != null)
				{
					cultureErrorNode = error404Node.SelectSingleNode(string.Format("errorPage [@culture = '{0}']", System.Threading.Thread.CurrentThread.CurrentUICulture.Name));
					if (cultureErrorNode.FirstChild != null)
						error404 = cultureErrorNode.FirstChild.Value;
				}
				else
				{
					cultureErrorNode = error404Node.SelectSingleNode("errorPage [@culture = 'default']");
					if (cultureErrorNode != null && cultureErrorNode.FirstChild != null)
						error404 = cultureErrorNode.FirstChild.Value;
				}
			}
			else
				error404 = UmbracoSettings.GetKey("/settings/content/errors/error404");
			return error404;
		}

		//#endregion
	}

	//public class MyUrlRoute : RouteBase
	//{
	//	public override RouteData GetRouteData(HttpContextBase httpContext)
	//	{
	//		//throw new Exception("hij komt hier ook!");

	//		string url = httpContext.Request.AppRelativeCurrentExecutionFilePath;

	//		//throw new Exception("url: "+ url); //url: ~/cat
	//		RouteData rd = new RouteData(this, new MvcRouteHandler());
	//		rd.Values.Add("controller", "RenderMvc");
	//		rd.Values.Add("action", "Category");
	//		//rd.Values.Add("url", "~/");
	//		//rd.DataTokens.Add("umbraco", "umbraco");
	//		//var umbracoToken = request.RouteData.DataTokens["umbraco"];
	//		return rd;
	//	}

	//	public override VirtualPathData GetVirtualPath(RequestContext requestContext, RouteValueDictionary values)
	//	{
	//		return null;
	//	}
	//}

	//public class ThemedViewEngine : WebFormViewEngine
	//{
	//	public override ViewEngineResult FindView(ControllerContext controllerContext, string viewName, string masterName, bool useCache)
	//	{
	//		throw new Exception("hij komt hier niet....");
	//		if (string.IsNullOrEmpty(masterName))
	//		{
	//			masterName = "MasterOne";
	//		}
	//		return base.FindView(controllerContext, viewName, masterName, useCache);
	//	}
	//}

//	public class AppStart : IApplicationEventHandler
//	{
//		public void OnApplicationInitialized(UmbracoApplication httpApplication, ApplicationContext applicationContext)
//		{
//			//your code here
//		}

//		public void OnApplicationStarting(UmbracoApplication httpApplication, ApplicationContext applicationContext)
//		{
//			//your code here
//		}

//		public void OnApplicationStarted(UmbracoApplication httpApplication, ApplicationContext applicationContext)
//		{
//			UmbracoDefault.BeforeRequestInit += UmbracoDefaultBeforeRequestInit;
//			UmbracoDefault.AfterRequestInit += UmbracoDefaultAfterRequestInit;
//			//throw new Exception("hij komt hier ook!");
//			ViewEngines.Engines.Clear();
//			ViewEngines.Engines.Add(new ThemedViewEngine());

////            RouteTable.Routes.MapRoute(
////"iets", // Route name
////"cat/{controller}/{action}", // URL with parameters
////new { controller = "RenderMvc", action = "Category" } // Parameter defaults
////);
//			// RouteTable.Routes.MapRoute(
//			//     name: "CityApiId",
//			//     url: "api/city/{cityId}/{cityName}",
//			//     defaults: new { controller = "City" }
//			// );

//			// RouteTable.Routes.MapHttpRoute(
//			//    name: "CityApiName",
//			//    routeTemplate: "api/city/{cityName}",
//			//    defaults: new { controller = "City", cityname = RouteParameter.Optional }
//			//);

//			// RouteTable.Routes.MapHttpRoute(
//			//     name: "DefaultApi",
//			//     routeTemplate: "api/{controller}/{id}",
//			//     defaults: new { id = RouteParameter.Optional }
//			// );
//		}

//		protected void UmbracoDefaultBeforeRequestInit(object sender, RequestInitEventArgs e)
//		{
//			//throw new Exception("hij komt hier niet..."); wel in webforms..
//			//ViewEngines.Engines.Clear();
//			//ViewEngines.Engines.Add(new ThemedViewEngine());
//		}

//		private static void UmbracoDefaultAfterRequestInit(object sender, RequestInitEventArgs e)
//		{
//			var page = (UmbracoDefault) sender;
//		}
//	}

	//public class UrlRewriting : IHttpModule
	//{
	//	public void Init(HttpApplication app)
	//	{
	//		app.BeginRequest += AppBeginRequest;
	//		app.PostMapRequestHandler += AppPostMapRequest;
	//	}

	//	private void AppPostMapRequest(object source, EventArgs e)
	//	{
	//		var context = ((HttpApplication) source).Context;

	//		//var helper = new UmbracoHelper(UmbracoContext.Current); // this influences rendering somehow..
	//		//context.Response.Clear();
	//		var categoryTemplateId = 1;
	//		//context.Response.Write(helper.RenderTemplate(Node.getCurrentNodeId(), categoryTemplateId));  6.0.0
	//		//context.Response.End();

	//		// create the Umbraco context
	//		//UmbracoContext.Current = new UmbracoContext(context);
	//		//var ucontext = new UmbracoContext(context);

	//		//page.MasterPageFile = template.GetMasterPageName(currentCategory.Template);
	//	}

	//	protected void AppBeginRequest(object source, EventArgs e)
	//	{
	//		var context = ((HttpApplication) source).Context;
	//		var absolutePath = context.Request.Url.AbsolutePath.ToLowerInvariant();
	//		//throw new Exception("blaappp: " + absolutePath);
	//	}

	//	public void Dispose()
	//	{
	//	}
	//}
}