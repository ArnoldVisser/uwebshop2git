﻿using System.Web.Mvc;
using Umbraco.Web.Mvc;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco.NodeFactory;

namespace uWebshop.Web.Mvc
{
	public class uwbsPaymentHandlerSurfaceController : SurfaceController
	{
		[HttpPost]
		public ActionResult Handle()
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(Node.GetCurrent().Id);

			new PaymentRequestHandler().HandleuWebshopPaymentRequest(paymentProvider.Id);

			return new EmptyResult();
		}
	}
}