﻿using System;
using System.Linq;
using System.Web.Configuration;
using SuperSimpleWebshop.Common;
using SuperSimpleWebshop.Domain;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.presentation.nodeFactory;

namespace SuperSimpleWebshop.Payment.FakeProvider
{
    public class FakePaymentRequestHandler : IPaymentRequestHandler
    {
        #region IPaymentRequestHandler Members
        public string GetName()
        {
            return "No Online Payment";
        }

        public PaymentRequest CreatePaymentRequest(Order order)
        {
            try
            {
                var paymentProvider = new OrderRepository().GetAllPaymentProviders().Where(x => x.Name == order.PaymentModule).FirstOrDefault();

                #region build urls
                var baseUrl = string.Format("http://{0}", library.GetCurrentDomains(Node.GetCurrent().Id).FirstOrDefault().Name);
                baseUrl = baseUrl.Substring(0, baseUrl.LastIndexOf("/"));

                var nodeId = DomainHelper.GetNodeIdForDocument("OrderProcessNode", library.GetDictionaryItem("PaymentConfirmation"), LanguageHelper.GetLanguageByCountryCode(order.ShopCountry).Name);
                var returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(nodeId));

                string reportUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(paymentProvider.Id));
                #endregion


            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, "FakePaymentRequestHandler.CreatePaymentRequest: " + ex.ToString());
            }

            return null;
        }
        #endregion
    }
}
