﻿using System.IO;
using System.Web.UI;
using SuperSimpleWebshop.Common;
using SuperSimpleWebshop.Domain;
using umbraco;
using umbraco.BusinessLogic;

namespace SuperSimpleWebshop.Payment.FakeProvider
{
    public class FakePaymentProviderResultHandler : IPaymentResultHandler
    {
        #region propeties
        private IOrderRepository _OrderRepository = new OrderRepository();
        #endregion

        #region IPaymentResultHandler Members
        public string GetName()
        {
            return "No Online Payment";
        }

        public string GetResultHtml(Order order)
        {
            var transactionId = library.Request("transaction_id");

            Log.Add(LogTypes.Debug, 0, "FakePaymentResultHandler.GetResultHtml: trans id = " + transactionId);

            var stringWriter = new StringWriter();
            var htmlWriter = new HtmlTextWriter(stringWriter);

            if (order != null)
            {
                htmlWriter.WriteBeginTag("div");
                htmlWriter.WriteAttribute("class", "onecolumn");
                htmlWriter.Write(HtmlTextWriter.TagRightChar);

                if (order.Status == OrderStatus.Ready)
                {
                                        
                    htmlWriter.RenderBeginTag("strong");
                    htmlWriter.Write(library.GetDictionaryItem("PaymentSuccess"));
                    htmlWriter.RenderEndTag();

                    htmlWriter.RenderBeginTag("p");
                    htmlWriter.Write(library.GetDictionaryItem("PaymentThankYou"));
                    htmlWriter.RenderEndTag();

                    htmlWriter.WriteEndTag("div");

                    return stringWriter.ToString();
                }

                if (order.Status == OrderStatus.Payment_Failed)
                {
                    htmlWriter.RenderBeginTag("strong");
                    htmlWriter.Write(library.GetDictionaryItem("PaymentFailed"));
                    htmlWriter.RenderEndTag();

                    htmlWriter.WriteEndTag("div");

                    return stringWriter.ToString();
                }
            }
            else
            {
                return "Order not found";
            }

            return string.Empty;
        }
        #endregion
    }
}
