﻿using System.Collections.Generic;
using System.Configuration;
using SuperSimpleWebshop.Common;
using SuperSimpleWebshop.Domain;

namespace SuperSimpleWebshop.Payment.FakeProvider
{
    public class FakePaymentProvider : IPaymentProvider
    {
        #region IPaymentProvider Members
        public string GetName()
        {
            return "No Online Payment";
        }

        public PaymentParameterRenderMethod GetParameterRenderMethod()
        {
            return PaymentParameterRenderMethod.QueryString;
        }

        public List<PaymentMethod> GetAllPaymentMethods()
        {

            var paymentMethods = new List<PaymentMethod>();

            paymentMethods.Add(new PaymentMethod
                {
                    Id = "0",
                    Name = GetName(),
                    Description = "No Online Payment"
                });

            return paymentMethods;
        }
        #endregion
    }
}
