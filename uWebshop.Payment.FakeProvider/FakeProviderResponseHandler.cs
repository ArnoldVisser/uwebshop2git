﻿using System;
using System.Web.Configuration;
using SuperSimpleWebshop.Common;
using SuperSimpleWebshop.Domain;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.web;

namespace SuperSimpleWebshop.Payment.FakeProvider
{
    public class FakePaymentProviderResponseHandler : IPaymentResponseHandler
    {
        #region IPaymentResponseHandler Members
        IOrderRepository _OrderRepository = new OrderRepository();

        public string GetName()
        {
            return "No Online Payment";
        }

        public void HandlePaymentResponse()
        {
            try
            {
                //Mollie gives us the transaction Id.
                string transactionId = library.Request("transaction_id");

                if (!string.IsNullOrEmpty(transactionId))
                {
                    var order = this._OrderRepository.GetOrder(x => x.PaymentTransactionId == transactionId);

                    Document sender = order.Document;

                    order.Status = OrderStatus.Ready;
                    EmailHelper.SendPaymentSuccessConfirmationToCustomer(sender.Id);

                    order.Save();
                }
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, "FakePaymentResponseHandler.HandlePaymentResponse: " + ex.ToString());
            }
        }
        #endregion
    }
}
