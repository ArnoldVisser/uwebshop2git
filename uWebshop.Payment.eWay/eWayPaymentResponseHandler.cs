﻿using System;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.eWay
{
    public class eWayPaymentResponseHandler : IPaymentResponseHandler
    {
        #region IPaymentResponseHandler Members
        public string GetName()
        {
            return "eWay";
        }

        public string HandlePaymentResponse()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
