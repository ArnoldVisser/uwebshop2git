﻿using System.Linq;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Repositories;
using umbraco;
using umbraco.NodeFactory;
using System.Web;

namespace uWebshop.Payment.eWay
{
    public class eWayPaymentRequestHandler : IPaymentRequestHandler
    {
        #region IPaymentRequestHandler Members
        public string GetName()
        {
            return "eWay";
        }

        public Domain.PaymentRequest CreatePaymentRequest(Domain.Order order)
        {
            var helper = new PaymentConfigHelper(order.OrderInfo.PaymentInfo.PaymentProviderName);

            var paymentProvider = new OrderRepository().GetAllPaymentProviders().Where(x => x.Name == order.OrderInfo.PaymentInfo.PaymentProviderName).FirstOrDefault();

            var request = new PaymentRequest();

            var baseUrl = string.Format("http://{0}", library.GetCurrentDomains(Node.GetCurrent().Id).First().Name);
            baseUrl = baseUrl.Substring(0, baseUrl.LastIndexOf("/"));

            if (baseUrl == string.Empty || baseUrl == "http:/")
            {
                baseUrl = "http://" + HttpContext.Current.Request.Url.Authority;
            }

            var paymentDataId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentData"), ShopAliasHelper.GetCurrentShopAlias());

            request.PaymentUrlBase = library.NiceUrl(paymentDataId);

            order.OrderInfo.PaymentInfo.PaymentURL = request.PaymentUrl;
            order.OrderInfo.PaymentInfo.PaymentParameters = request.ParametersAsString;

            return request;
        }

        /// <summary>
        /// Returns the URL to redirect to
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public string GetPaymentUrl(Order order)
        {
            return string.Empty;
        }
        #endregion
    }
}
