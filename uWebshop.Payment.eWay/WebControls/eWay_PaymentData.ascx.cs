﻿using System;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco;
using umbraco.BusinessLogic;

namespace uWebshop.Payment.eWay.WebControls
{
    public partial class eWay_PaymentData : System.Web.UI.UserControl
    {
        #region properties
        private Domain.Order Order;
        #endregion

        #region event handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            Order = new Domain.Order(library.RequestCookies("PaymentOrderId"));
            var paymentConfigHelper = new PaymentConfigHelper(Order.OrderInfo.PaymentInfo.PaymentProviderName);

            //create eWAY gateway object
            GatewayConnector eWAYgateway = new GatewayConnector();
            GatewayRequest eWAYRequest = new GatewayRequest();

            try
            {
                eWAYRequest.EwayCustomerID = paymentConfigHelper.Settings["customerId"];
                eWAYRequest.CardNumber = ewayCardNumber.Text.ToString();
                eWAYRequest.CardHolderName = ewayCardHoldersName.Text.ToString();
                eWAYRequest.CardExpiryMonth = ewayCardExpiryMonth.Text.Substring(0, 2);
                eWAYRequest.CardExpiryYear = ewayCardExpiryYear.Text.Substring(2, 2);
                eWAYRequest.InvoiceAmount = Convert.ToInt32(Order.OrderInfo.TotalAmount * 100); //int

                // Do the payment, send XML doc containing information gathered
                if (eWAYgateway.ProcessRequest(eWAYRequest))
                {
                    // payment succesfully sent to gateway
                    GatewayResponse eWAYResponse = eWAYgateway.Response;

                    if (eWAYResponse != null)
                    {
                        Order.OrderInfo.Status = OrderStatus.Ready_for_Dispatch;
                        Order.Save();

                        var nodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmation"), ShopAliasHelper.GetCurrentShopAlias());
                        Response.Redirect(library.NiceUrl(nodeId));
                    }
                    else
                    {
                        // invalid response received from server.
                        Log.Add(LogTypes.Debug, 0, "Error: An invalid response was received from the payment gateway.");

                        Order.OrderInfo.Status = OrderStatus.Payment_Failed;
                        Order.Save();

                        var nodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmationFailed"), ShopAliasHelper.GetCurrentShopAlias());
                        Response.Redirect(library.NiceUrl(nodeId));
                    }
                }
                else
                {
                    // gateway connection failed
                    Log.Add(LogTypes.Debug, 0, "Error: Connection to the payment gateway failed. Check gateway URL or XML sent." + Server.UrlEncode(eWAYRequest.ToXml().ToString()));

                    Order.OrderInfo.Status = OrderStatus.Payment_Failed;
                    Order.Save();

                    var nodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmationFailed"), ShopAliasHelper.GetCurrentShopAlias());
                    Response.Redirect(library.NiceUrl(nodeId));
                }
            }
            // eway exception
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, "btnPay_Click: " + ex.ToString());
            }
        }
        #endregion
    }
}
