﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="eWay_PaymentData.ascx.cs" Inherits="uWebshop.Payment.eWay.WebControls.eWay_PaymentData" %>

<form id="form1" runat="server">
    <table>
        <tr>
            <td>ewayCardHoldersName</td>
            <td><asp:TextBox ID="ewayCardHoldersName" runat="server" /></td>
        </tr>
        <tr>
            <td>ewayCardNumber</td>
            <td><asp:TextBox ID="ewayCardNumber" runat="server" /></td>
        </tr>
        <tr>
            <td>ewayCardExpiryMonth</td>
            <td>
                <asp:DropDownList ID="ewayCardExpiryMonth" runat="server">
                    <asp:ListItem Text="01" Value="01" />
                    <asp:ListItem Text="02" Value="02" />
                    <asp:ListItem Text="03" Value="03" />
                    <asp:ListItem Text="04" Value="04" />
                    <asp:ListItem Text="05" Value="05" />
                    <asp:ListItem Text="06" Value="06" />
                    <asp:ListItem Text="07" Value="07" />
                    <asp:ListItem Text="08" Value="08" />
                    <asp:ListItem Text="09" Value="09" />
                    <asp:ListItem Text="10" Value="10" />
                    <asp:ListItem Text="11" Value="11" />
                    <asp:ListItem Text="12" Value="12" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>ewayCardExpiryYear</td>
            <td>
                <asp:DropDownList ID="ewayCardExpiryYear" runat="server">
                    <asp:ListItem Text="2012" Value="2012" />
                    <asp:ListItem Text="2013" Value="2013" />
                    <asp:ListItem Text="2014" Value="2014" />
                    <asp:ListItem Text="2015" Value="2015" />
                    <asp:ListItem Text="2016" Value="2016" />
                    <asp:ListItem Text="2017" Value="2017" />
                    <asp:ListItem Text="2018" Value="2018" />
                    <asp:ListItem Text="2019" Value="2019" />
                    <asp:ListItem Text="2020" Value="2020" />
                    <asp:ListItem Text="2021" Value="2021" />
                </asp:DropDownList>
            </td>
        </tr>
    </table>

    <asp:Button ID="btnPay" OnClick="btnPay_Click" Text="Pay" runat="server" />
</form>
