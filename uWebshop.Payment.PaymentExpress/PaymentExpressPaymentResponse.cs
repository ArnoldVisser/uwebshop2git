﻿using System.Linq;
using System.Web;
using PaymentExpress.PxPay;
using umbraco;
using umbraco.NodeFactory;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.PaymentExpress
{
	public class PaymentExpressPaymentResponse : IPaymentResponseHandler
	{
		public string GetName()
		{
			return "PaymentExpress";
		}

		public string HandlePaymentResponse()
		{
			//Determine if the page request is for a user returning from the payment page

			var resultQs = HttpContext.Current.Request.QueryString["result"];

			if (!string.IsNullOrEmpty(resultQs))
			{
				var paymentProvider = PaymentProviderHelper.GetAllPaymentProviders().FirstOrDefault(x => x.Name.ToLower() == GetName().ToLower());

				var helper = new PaymentConfigHelper(paymentProvider);

				var webserviceUrl = helper.Settings["WebserviceUrl"];
				if (string.IsNullOrEmpty(webserviceUrl))
				{
					Log.Instance.LogError(string.Format("{0}: Missing PaymentProvider.Config  field with name: webserviceUrl, paymentProviderNodeId: {1}", GetName(), paymentProvider.Id));
				}

				//PxPayUserId	 Yes	 Your account's user ID
				var pxPayUserId = helper.Settings["PxPayUserId"];
				if (string.IsNullOrEmpty(pxPayUserId))
				{
					Log.Instance.LogError(string.Format("{0}: Missing PaymentProvider.Config  field with name: PxPayUserId, paymentProviderNodeId: {1}", GetName(), paymentProvider.Id));
				}

				//PxPayKey	 Yes	 Your account's 64 character key
				var pxPayKey = helper.Settings["PxPayKey"];
				if (string.IsNullOrEmpty(pxPayKey))
				{
					Log.Instance.LogError(string.Format("{0}: Missing PaymentProvider.Config  field with name: pxPayKey, paymentProviderNodeId: {1}", GetName(), paymentProvider.Id));
				}

				#region build urls

				var currentNodeId = Node.GetCurrent().Id;
				var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

				var succesUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.SuccesNodeId)));
				var cancelUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.ErrorNodeId)));
				
				#endregion

				// Obtain the transaction result
				var paymentExpressWebservice = new PxPay(pxPayUserId, pxPayKey);

				var output = paymentExpressWebservice.ProcessResponse(resultQs, webserviceUrl);

				var redirect = succesUrl;

				if (output.Success == "1")
				{
					var transactionId = output.TxnId;

					var orderInfo = OrderHelper.GetOrderInfo(transactionId);


					if (orderInfo != null && orderInfo.Paid == false)
					{
						orderInfo.Paid = true;
						orderInfo.Status = OrderStatus.ReadyForDispatch;

						orderInfo.Save();
					}
					else
					{
						redirect = cancelUrl;
					}
				}
				else
				{
					redirect = cancelUrl;
				}

				HttpContext.Current.Response.Redirect(redirect);
			}

			
			return string.Empty;
		}
	}
}
