﻿using System;
using System.Globalization;
using PaymentExpress.PxPay;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.interfaces;
using umbraco.NodeFactory;

namespace uWebshop.Payment.PaymentExpress
{
	public class PaymentExpressPaymentRequest : IPaymentRequestHandler
	{

			//Required	 Description
			
			
			
			//BillingId	 No	 Optional identifier when adding a card for recurring billing
			
			//EmailAddress	 No	 Optional email address
			//EnableAddBillCard	 No	 Required when adding a card to the DPS system for recurring billing. Set element to "1" for true
			//TxnData1	 No	 Optional free text
			//TxnData2	 No	 Optional free text
			//TxnData3	 No	 Optional free text
			//Opt	 No	 Optional additional parameter

		public string GetName()
		{
			return "PaymentExpress";
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			var webserviceUrl = helper.Settings["WebserviceUrl"];
			if (string.IsNullOrEmpty(webserviceUrl))
			{
				Log.Instance.LogError(string.Format("{0}: Missing PaymentProvider.Config  field with name: webserviceUrl, paymentProviderNodeId: {1}", GetName(), paymentProvider.Id));
			}

			//PxPayUserId	 Yes	 Your account's user ID
			var pxPayUserId = helper.Settings["PxPayUserId"];
			if (string.IsNullOrEmpty(pxPayUserId))
			{
				Log.Instance.LogError(string.Format("{0}: Missing PaymentProvider.Config  field with name: PxPayUserId, paymentProviderNodeId: {1}", GetName(), paymentProvider.Id));
			}

			//PxPayKey	 Yes	 Your account's 64 character key
			var pxPayKey = helper.Settings["PxPayKey"];
			if (string.IsNullOrEmpty(pxPayKey))
			{
				Log.Instance.LogError(string.Format("{0}: Missing PaymentProvider.Config  field with name: pxPayKey, paymentProviderNodeId: {1}", GetName(), paymentProvider.Id));
			}


			//AmountInput	 Yes	 Amount value in d.cc format
			var amountInput = orderInfo.ChargedAmount.ToString("F2", orderInfo.StoreInfo.CultureInfo);

			//CurrencyInput	 Yes	 Currency of AmountInput
			var ri = new RegionInfo(orderInfo.StoreInfo.Store.CurrencyCultureInfo.LCID);
			var currencyInput = ri.ISOCurrencySymbol;

			//MerchantReference	 No	 Reference field to appear on transaction reports
			var merchantReference = orderInfo.OrderNumber;

			//TxnType	 Yes	 "Auth" or "Purchase"
			var TxnType = helper.Settings["TxnType"];
			if (string.IsNullOrEmpty(TxnType))
			{
				Log.Instance.LogError(string.Format("{0}: Missing PaymentProvider.Config  field with name: TxnType, paymentProviderNodeId: {1}", GetName(), paymentProvider.Id));
			}

			//TxnId	 No	 A value that uniquely identifies the transaction
			var txnId = orderInfo.OrderNumber + "x" + DateTime.Now.ToString("hhmmss");

			#region build urls
			var currentNodeId = Node.GetCurrent().Id;
			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);
			
			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);
			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));

			#endregion
			
			var paymentExpressWebservice = new PxPay(pxPayUserId, pxPayKey);

			var input = new RequestInput
				            {
					            AmountInput = amountInput,
					            CurrencyInput = currencyInput,
					            MerchantReference = merchantReference,
					            TxnType = TxnType,
					            UrlFail = reportUrl,
								UrlSuccess = reportUrl,
					            TxnId = txnId,
				            };
			
			var output = paymentExpressWebservice.GenerateRequest(input, webserviceUrl);

			if (output.valid == "1")
			{
				// Redirect user to payment page

				orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.ServerPost;
				orderInfo.PaymentInfo.Url = output.Url;
				orderInfo.PaymentInfo.TransactionId = txnId;
				uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, txnId);
				orderInfo.Save();
			}

			return null;
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return orderInfo.PaymentInfo.Url;
		}
	}

}
