﻿using System.Collections.Generic;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.PaymentExpress
{
	public class PaymentExpressPaymentProvider : IPaymentProvider
    {
		public string GetName()
		{
			return "PaymentExpress";
		}

		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			var paymentProviderMethodList = new List<PaymentProviderMethod>
				{
					new PaymentProviderMethod
						{
							Id = "PaymentExpress",
							ProviderName = GetName(),
							Title = "PaymentExpress",
							Description = "PaymentExpress"
						}
				};

			return paymentProviderMethodList;
		}
    }
}
