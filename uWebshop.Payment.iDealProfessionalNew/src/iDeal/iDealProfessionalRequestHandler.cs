﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Web;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace iDeal
{
	public class iDealProfessionalRequestHandler : IPaymentRequestHandler
	{
		public string GetName()
		{
			return "iDealProfessional";
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			RequestTransaction(orderInfo);

			return null;
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return orderInfo.PaymentInfo.Url;
		}

		private void RequestTransaction(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			try
			{
				decimal grandTotal = 0;

				if (paymentProvider.TestMode)
				{
					//Test mode
					grandTotal = helper.Settings["testAmountInCents"] == "0" ? orderInfo.GrandtotalInCents : decimal.Parse(helper.Settings["testAmountInCents"]);
				}
				else
				{
					grandTotal = long.Parse(orderInfo.ChargedAmountInCents.ToString());
				}

				#region build urls

				var currentNodeId = 0;

				try
				{
					currentNodeId = PaymentProviderHelper.GetCurrentNodeId();
				}
				catch
				{
				}

				var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

				var paymentNiceUrl = PaymentProviderHelper.GetUrlForContentWithId(paymentProvider.Id);

				string reportUrl;

				if (paymentNiceUrl.StartsWith("http"))
				{
					reportUrl = paymentNiceUrl;
				}
				else
				{
					reportUrl = string.Format("{0}{1}", baseUrl, paymentNiceUrl);
				}

				//Log.Instance.LogDebug("iDeal Professional reportUrl: " + reportUrl + ", paymentProviderId: " + paymentProvider.Id);

				#endregion

				var orderGuidAsString = orderInfo.UniqueOrderId.ToString();

				var transactionId = orderGuidAsString.Substring(0, 16);

				//Log.Instance.LogDebug("iDeal Professional RequestTransaction orderGuidAsString: " + orderGuidAsString + ", paymentProviderId: " + paymentProvider.Id);
				//Log.Instance.LogDebug("iDeal Professional RequestTransaction transactionId: " + transactionId + ", paymentProviderId: " + paymentProvider.Id);

				var iDealService = new iDealService();
				var transactionResponse = iDealService.SendTransactionRequest(
					issuerId: Convert.ToInt32(orderInfo.PaymentInfo.MethodId),
					merchantReturnUrl: reportUrl,
					purchaseId: transactionId,
					amount: Convert.ToInt32(grandTotal.ToString()),
					expirationPeriod: TimeSpan.FromMinutes(5),
					description: orderInfo.OrderNumber,
					entranceCode: orderInfo.OrderNodeId.ToString());

				orderInfo.PaymentInfo.TransactionId = transactionResponse.TransactionId;
				orderInfo.PaymentInfo.Url = HttpUtility.HtmlDecode(transactionResponse.IssuerAuthenticationUrl);

				uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, transactionResponse.TransactionId);

				orderInfo.Save();
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("iDeal Transaction " + ex);
				//throw;
			}
		}
	}
}