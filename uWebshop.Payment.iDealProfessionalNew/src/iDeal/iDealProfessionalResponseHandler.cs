﻿using System.Web;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace iDeal
{
	public class iDealProfessionalResponseHandler : IPaymentResponseHandler
	{
		public string GetName()
		{
			return "iDealProfessional";
		}

		public string HandlePaymentResponse()
		{
			var currentNodeId = PaymentProviderHelper.GetCurrentNodeId();

			var transactionId = PaymentProviderHelper.Request("trxid");

			var orderInfo = OrderHelper.GetOrderInfo(transactionId);

			if (orderInfo != null)
			{
				if (orderInfo.Paid == true)
				{
					return string.Empty;
				}
					var iDealService = new iDealService();

					var transaction = iDealService.SendStatusRequest(transactionId);

					var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
					

					#region build urls

					var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

					var returnNiceUrl = PaymentProviderHelper.GetUrlForContentWithId(int.Parse(paymentProvider.SuccesNodeId));
					var cancelNiceUrl = PaymentProviderHelper.GetUrlForContentWithId(int.Parse(paymentProvider.ErrorNodeId));
					
					string returnUrl;

					if (returnNiceUrl.StartsWith("http"))
					{
						returnUrl = returnNiceUrl;
					}
					else
					{
						returnUrl = string.Format("{0}{1}", baseUrl, PaymentProviderHelper.GetUrlForContentWithId(int.Parse(paymentProvider.SuccesNodeId)));
					}

					string cancelUrl;

					if (cancelNiceUrl.StartsWith("http"))
					{
						cancelUrl = cancelNiceUrl;
					}
					else
					{
						cancelUrl = string.Format("{0}{1}", baseUrl, PaymentProviderHelper.GetUrlForContentWithId(int.Parse(paymentProvider.ErrorNodeId)));
					}
					
					#endregion

					var status = transaction.Status.ToString();

					//Log.Instance.LogDebug("iDealProfessional status: " + status + ", currentNodeId: " + currentNodeId);

					var redirectUrl = baseUrl;

					
					switch (status)
					{
						case "Success":
							orderInfo.Status = OrderStatus.ReadyForDispatch;
							orderInfo.Paid = true;

							redirectUrl = returnUrl;

							break;
						case "Cancelled":
						case "Expired":
						case "Failure":
							orderInfo.Status = OrderStatus.PaymentFailed;
							orderInfo.PaymentInfo.ErrorMessage = status;
							orderInfo.Paid = false;

							redirectUrl = cancelUrl;
							break;
						case "Open":
							orderInfo.Status = OrderStatus.WaitingForPayment;
							break;
					}

					orderInfo.Save();

					//Log.Instance.LogDebug("iDealProfessional redirectUrl: " + redirectUrl + ", currentNodeId: " + currentNodeId);

					HttpContext.Current.Response.Redirect(redirectUrl);
				
			}
			else
			{
				//Log.Instance.LogDebug("iDealProfessional orderInfo == null!!!" + ", currentNodeId: " + currentNodeId);
			}


			return null;
		}
	}
}