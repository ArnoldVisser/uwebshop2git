﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using iDeal.Directory;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace iDeal
{
	public class iDealProfessionalProvider : IPaymentProvider
	{
		public string GetName()
		{
			return "iDealProfessional";
		}

		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			Log.Instance.LogError("iDeal Issuers GetAllPaymentMethods() LOADED (no error)");

			//Log.Instance.LogDebug("iDealPrefessionalProvider Start GetAllPaymentMethods");
			var paymentMethods = new List<PaymentProviderMethod>();

			try
			{
				var issuers = (IList<Issuer>)HttpContext.Current.Cache["67ebb113-6163-4a9c-b87b-14cbf4ab8539"];

				if (issuers == null || !issuers.Any())
				{
					var connector = new iDealService();
					issuers = connector.SendDirectoryRequest().Issuers;
					HttpContext.Current.Cache.Add("67ebb113-6163-4a9c-b87b-14cbf4ab8539", issuers, null, DateTime.Now.AddDays(1),
						Cache.NoSlidingExpiration, CacheItemPriority.High, null);
				}
				

				foreach (var issuer in issuers)
				{
					Log.Instance.LogDebug("iDeal Issuer: " + issuer.Name);
					paymentMethods.Add(new PaymentProviderMethod
						{
							Id = issuer.Id.ToString(),
							Description = issuer.Name,
							Title = issuer.Name,
							Name = issuer.Name,
							ProviderName = GetName()
						});
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError( "iDeal Issuers ERROR: " + ex);
				return null;
			}

			if (!paymentMethods.Any())
			{
				paymentMethods.Add(new PaymentProviderMethod
				                   {
					                   Id = "1234",
					                   Description = "Nothing from iDEAL",
					                   Title = "Nothing from iDEAL",
					                   Name = "Nothing from iDEAL",
					                   ProviderName = GetName()
				                   });
			}
			
			return paymentMethods;
			
		}
	}
}