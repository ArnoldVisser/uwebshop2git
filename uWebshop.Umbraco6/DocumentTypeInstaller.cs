﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.DataTypes;
using DataTypeDefinition = Umbraco.Core.Models.DataTypeDefinition;
using Helpers = uWebshop.Umbraco.Helpers;

namespace uWebshop.Umbraco6
{
    public class DocumentTypeInstaller
	{
		public void InstallDocumentTypes()
		{
			var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
			var dataTypeService = ApplicationContext.Current.Services.DataTypeService;

			//var DiscountTypeDataTypeDef = dataTypeService.GetDataTypeDefinitionById(new Guid("2d89188e-33a7-4885-a6d0-1caef40320a7"));
			//if (DiscountTypeDataTypeDef == null)
			//{
			//	DiscountTypeDataTypeDef = new DataTypeDefinition(-1, new Guid("c66b5dea-00f7-49df-bc5a-074e5802bce2"));
			//	DiscountTypeDataTypeDef.Name = "Discount Picker";
			//	DiscountTypeDataTypeDef.Key = new Guid("2d89188e-33a7-4885-a6d0-1caef40320a7");
			//	DiscountTypeDataTypeDef.DatabaseType = DataTypeDatabaseType.Ntext;
			//}

			IO.Container.Resolve<ICMSInstaller>().Install();
			//ContentInstaller.InstallContent();
			return;

		}

		public void DeleteAllDocTypes()
		{
			var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
			// contentTypeService.GetAllContentTypes().Where(c => c.Level)
			var allContentTypes = contentTypeService.GetAllContentTypes().ToList();
			contentTypeService.Delete(contentTypeService.GetAllContentTypes().Where(c => !allContentTypes.Any(d=> d.ParentId == c.Id)));

			allContentTypes = contentTypeService.GetAllContentTypes().ToList();
			contentTypeService.Delete(contentTypeService.GetAllContentTypes().Where(c => !allContentTypes.Any(d => d.ParentId == c.Id)));
			allContentTypes = contentTypeService.GetAllContentTypes().ToList();
			contentTypeService.Delete(contentTypeService.GetAllContentTypes().Where(c => !allContentTypes.Any(d => d.ParentId == c.Id)));

			var dataTypeService = ApplicationContext.Current.Services.DataTypeService;
			
			//dataTypeService.GetAllDataTypeDefinitions().Where(d => !new List<int>{-87, 1041, -49}.Contains(d.Id)).ForEach(dt => dataTypeService.Delete(dt));
		}
		
        public string RunT4Code()
        {
            Dictionary<Type, ContentTypeAttribute> mapping = new Dictionary<Type, ContentTypeAttribute>();
            List<Tuple<Type, Type>> lazyParentIdMapping = new List<Tuple<Type, Type>>();
            List<Tuple<Type, Type[]>> childTypesMapping = new List<Tuple<Type, Type[]>>();

            Write("var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;\r\n");
            Write("var dataTypeService = ApplicationContext.Current.Services.DataTypeService;\r\n");
            Write("var contentTypeList = new List<IContentType>();\r\n");
            Write("var newDataTypesList = new List<IDataTypeDefinition>();\r\n");

            var dataTypesToGenerate = new uWebshop.DataTypes.DataTypes().LoadDataTypeDefinitions();
            foreach (var def in dataTypesToGenerate.OrderBy(d => d.Name))
            {
                Write("var " + def.DataType + "DataTypeDef = dataTypeService.GetDataTypeDefinitionById(new Guid(\"" +
                                                    (def.Name == null ? def.DefinitionGuid : def.KeyGuid) + "\"));\r\n");
                if (def.Name != null)
                {
                    Write(@"			if (" + def.DataType + @"DataTypeDef == null)
			{
				" + def.DataType + @"DataTypeDef = new DataTypeDefinition(-1, new Guid(""" + def.DefinitionGuid + @"""));
				" + def.DataType + @"DataTypeDef.Name = """ + def.Name + @""";
				" + def.DataType + @"DataTypeDef.Key = new Guid(""" + def.KeyGuid + @""");
				" + def.DataType + @"DataTypeDef.DatabaseType = DataTypeDatabaseType." + def.Type + @";
				
				newDataTypesList.Add(" + def.DataType + @"DataTypeDef);
			}
			");
                }
                else
                {
					Write("if (" + def.DataType + "DataTypeDef == null) throw new Exception(\"Could not load default umbraco " + def.DataType + " datatype\");\r\n");
                }
            }

            Write("if (newDataTypesList.Any()) dataTypeService.Save(newDataTypesList);\r\n");
            
            Write("\r\n");
			foreach (var def in dataTypesToGenerate.Where(def => def.Name != null).OrderBy(d => d.Name))
            {
                Write("" + def.DataType + "DataTypeDef = dataTypeService.GetDataTypeDefinitionById(new Guid(\"" + def.KeyGuid + "\"));\r\n");
				Write("if (" + def.DataType + "DataTypeDef == null) throw new Exception(\"Could not create and/or load " + def.DataType + " datatype\");\r\n");
            }
            Write("\r\n");

			foreach (var def in dataTypesToGenerate.Where(def => def.PreValues != null && def.PreValues.Any()).OrderBy(d => d.Name))
            {
                Write("if (newDataTypesList.Contains(" + def.DataType + "DataTypeDef))\r\n");
                Write("	dataTypeService.SavePreValues(" + def.DataType + "DataTypeDef.Id, new[] { " + string.Join(",", def.PreValues.Select(s => "\"" + s.Replace("\\", "\\\\").Replace("\"", "\\\"") + "\"").ToArray()) + " });\r\n");
            }

            Write("\r\n");
            Write("if (newDataTypesList.Any()) dataTypeService.Save(newDataTypesList);\r\n");
            Write("\r\n");

			foreach (Type type in Assembly.GetAssembly(typeof(Settings)).GetTypes().Where(x => Attribute.IsDefined(x, typeof(ContentTypeAttribute), false)).OrderBy(d => d.Name))
            {
                var attribute = (ContentTypeAttribute)type.GetCustomAttributes(typeof(ContentTypeAttribute), false).Single();
                mapping.Add(type, attribute);
                var contentTypeVarName = attribute.Alias + "ContentType";
                Write("\r\n");
                Write("var " + contentTypeVarName + " = contentTypeService.GetContentType(\"" + attribute.Alias + "\") ?? new ContentType(-1) {\r\n");
                Write("Alias = \"" + attribute.Alias + "\",\r\n");
                if (type == typeof(UwebshopRootContentType))
                    Write("AllowedAsRoot = true,\r\n");
                Write("Name = \"" + attribute.Name + "\",\r\n");
                Write("Description = \"" + attribute.Description + "\",\r\n");
                Write("Thumbnail = \"" + attribute.Thumbnail + ".png\",\r\n");
				Write("Icon = \"" + uWebshop.Umbraco.Businesslogic.InternalHelpers.GetFileNameForIcon(attribute.Icon) + "\",\r\n");
                Write("SortOrder = 1,\r\n");
                Write("AllowedContentTypes = new List<ContentTypeSort>(),\r\n");
                Write("AllowedTemplates = new List<ITemplate>(),\r\n");
                Write("PropertyGroups = new PropertyGroupCollection(new List<PropertyGroup>()),};\r\n");
                Write("contentTypeList.Add(" + contentTypeVarName + ");\r\n");
                Write("\r\n");

                if (attribute.ParentContentType != null)
                {
                    lazyParentIdMapping.Add(new Tuple<Type, Type>(type, attribute.ParentContentType));
                }
                if (attribute.AllowedChildTypes != null && attribute.AllowedChildTypes.Any())
                {
                    childTypesMapping.Add(new Tuple<Type, Type[]>(type, attribute.AllowedChildTypes));
                }

                var properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => Attribute.IsDefined(x, typeof(ContentPropertyTypeAttribute), false)).Select(property => (ContentPropertyTypeAttribute)property.GetCustomAttributes(typeof(ContentPropertyTypeAttribute), false).Single());
				foreach (var property in properties.OrderBy(p => p.SortOrder + ((int)p.Tab * 50)))
                {
                    Write("if (" + contentTypeVarName + ".PropertyTypes.All(p => p.Alias != \"" + property.Alias + "\")){\r\n");
                    Write("GetOrAddPropertyGroup(" + contentTypeVarName + ", \"" + property.Tab + "\").PropertyTypes.Add(");
                    Write("new PropertyType(" + property.DataType + "DataTypeDef) { ");
                    Write("Alias = \"" + property.Alias + "\", ");
                    Write("Name = \"" + property.Name + "\", ");
                    Write("Description = \"" + property.Description + "\",");
                    if (property.Mandatory)
                        Write("Mandatory = true, ");
                    Write("});\r\n");
                    Write("}\r\n");
                }
                Write("\r\n");
            }

            Write("contentTypeService.Save(contentTypeList);\r\n");
            Write("\r\n");

			foreach (var tuple in lazyParentIdMapping.OrderBy(d => d.Item1.Name))
            {
                Write(mapping[tuple.Item1].Alias + "ContentType.SetLazyParentId(new Lazy<int>(() => " + mapping[tuple.Item2].Alias + "ContentType.Id));\r\n");
            }
            Write("\r\n");
            Write("contentTypeService.Save(contentTypeList);\r\n");
            Write("\r\n");
			foreach (var tuple in childTypesMapping.OrderBy(d => d.Item1.Name))
            {
                Write(mapping[tuple.Item1].Alias + "ContentType.AllowedContentTypes = new List<ContentTypeSort> { ");// + mapping[tuple.Item2].Alias + "ContentType.Id));");
                foreach (var childType in tuple.Item2)
                {
                    if (!mapping.ContainsKey(childType))
                        throw new Exception("NOT FOUND: " + childType.Name);
                    var alias = mapping[childType].Alias;
                    Write("new ContentTypeSort { Alias = \"" + alias + "\", Id = new Lazy<int>(() => " + alias + "ContentType.Id) },");
                }
                Write("};\r\n");
            }
            Write("\r\n");
            Write("contentTypeService.Save(contentTypeList);");
            return generatedCode.ToString();
        }
	    private StringBuilder generatedCode = new StringBuilder();
	    private void Write(string s)
	    {
	        generatedCode.Append(s);
	    }
	}

	public partial class CMSInstaller : ICMSInstaller
	{
		public void Install()
		{
			Helpers.uWebshopInstalleruWebshopPaymentsHandlerTemplate();
			InstallGenerated();
		}

		public bool InstallStorePickerOnNodeWithId(int nodeId, out string feedbackSmall, out string feedbackLarge)
		{
			var contentService = ApplicationContext.Current.Services.ContentService;
			var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
			
			var storePickerDataTypeDef = LoadOrCreateStorePickerDataTypeDefinition();
			
			var homepage = contentService.GetById(nodeId);
			var contentType = homepage.ContentType;
			if (contentType.PropertyTypes.All(p => p.Alias != Constants.StorePickerAlias))
			{
				contentType.AddPropertyType(new PropertyType(storePickerDataTypeDef) { Alias = Constants.StorePickerAlias, Name = "#StorePicker", Description = "#StorePickerDescription", Mandatory = false, });
				contentTypeService.Save(new[] {contentType});
				feedbackSmall = "StorePicker installed!";
				feedbackLarge = "StorePicker installed on " +
								contentType.Alias +
								" document type";
				return true;
			}
			feedbackSmall = "StorePicker install ignored";
			feedbackLarge = "StorePicker install ignored on " +
							contentType.Alias + " document type, property with alias " + Constants.StorePickerAlias + " already existed";
			return true;
		}

		private static IDataTypeDefinition LoadOrCreateStorePickerDataTypeDefinition()
		{
			var dataTypeService = ApplicationContext.Current.Services.DataTypeService;
			var newDataTypesList = new List<IDataTypeDefinition>();
			var storePickerDataTypeDef = dataTypeService.GetDataTypeDefinitionById(new Guid("1e8cdc0b-436e-46f5-bfec-57be45745771"));
			if (storePickerDataTypeDef == null)
			{
				storePickerDataTypeDef = new DataTypeDefinition(-1, new Guid("5fa345e3-9352-45d6-adaa-2da6cdc9aca3"));
				storePickerDataTypeDef.Name = "uWebshop Store Picker";
				storePickerDataTypeDef.Key = new Guid("1e8cdc0b-436e-46f5-bfec-57be45745771");
				storePickerDataTypeDef.DatabaseType = DataTypeDatabaseType.Integer;

				newDataTypesList.Add(storePickerDataTypeDef);
			}
			if (newDataTypesList.Any()) dataTypeService.Save(newDataTypesList);

			storePickerDataTypeDef = dataTypeService.GetDataTypeDefinitionById(new Guid("1e8cdc0b-436e-46f5-bfec-57be45745771"));
			return storePickerDataTypeDef;
		}

		public bool InstallSandBoxStarterkit(out bool storePresent)
		{
			storePresent = false;
			var contentService = ApplicationContext.Current.Services.ContentService;
			var contentTypeService = ApplicationContext.Current.Services.ContentTypeService;
			var fileService = ApplicationContext.Current.Services.FileService;

			var categoryDocType = contentTypeService.GetContentType(Category.NodeAlias);
			var productDocType = contentTypeService.GetContentType(Product.NodeAlias);
			var storeDocType = contentTypeService.GetContentType(Store.NodeAlias);
					
			if (storeDocType == null || categoryDocType == null || productDocType == null) return false;

			storePresent = contentService.GetContentOfContentType(storeDocType.Id).Any(x => !x.Trashed);

			var categoryTemplate = fileService.GetTemplate("uwbsCategory") as Template;
			categoryDocType.AllowedTemplates = categoryDocType.AllowedTemplates.Concat(new List<ITemplate> {categoryTemplate});
			categoryDocType.SetDefaultTemplate(categoryTemplate);

			var productTemplate = fileService.GetTemplate("uwbsProduct") as Template;
			productDocType.AllowedTemplates = productDocType.AllowedTemplates.Concat(new List<ITemplate> {productTemplate});
			productDocType.SetDefaultTemplate(productTemplate);

			contentTypeService.Save(new[] { categoryDocType, productDocType });

			var homepagePackageDocType = contentTypeService.GetContentType("uwbsHomepage");
			if (homepagePackageDocType.PropertyTypes.All(p => p.Alias != Constants.StorePickerAlias))
			{
				homepagePackageDocType.AddPropertyType(new PropertyType(LoadOrCreateStorePickerDataTypeDefinition()) 
				{ Alias = Constants.StorePickerAlias, Name = "#StorePicker", Description = "#StorePickerDescription", Mandatory = false, });
				contentTypeService.Save(new[] { homepagePackageDocType });
				homepagePackageDocType = contentTypeService.GetContentType("uwbsHomepage");
			}

			var contentToSaveAndPublish = new List<IContent>(15);

			var categoryRepositoryDocType = contentTypeService.GetContentType(Catalog.CategoryRepositoryNodeAlias);
			var categoryRepository = contentService.GetContentOfContentType(categoryRepositoryDocType.Id).FirstOrDefault(x => !x.Trashed);

			var testCategory = contentService.CreateContent("Your First Category", categoryRepository, Category.NodeAlias);
			testCategory.SetValue("title", "Your First Category");
			testCategory.SetValue("url", "Your First Category");
			testCategory.SetValue("metaDescription", "Your First Category");
			contentToSaveAndPublish.Add(testCategory);

			var productContent = CreateProductContent(contentService, testCategory, "Your First Product", "PROD001", "10000", 5);
			contentToSaveAndPublish.Add(productContent);
			contentToSaveAndPublish.Add(CreateProductContent(contentService, testCategory, "Your Second Product", "PROD002", "5000", 10));
			contentToSaveAndPublish.Add(CreateVariantContent(contentService, productContent, "color", "Orange", "VARCOL001", "1000", 5, "Your First Color Variant"));
			contentToSaveAndPublish.Add(CreateVariantContent(contentService, productContent, "color", "Blue", "VARCOL002", "2000", 11, "Your Second Color Variant"));
			contentToSaveAndPublish.Add(CreateVariantContent(contentService, productContent, "type", "Manual", "VARTYP001", "500", 14, "Your First Type Variant"));
			contentToSaveAndPublish.Add(CreateVariantContent(contentService, productContent, "type", "Automatic", "VARTYP002", "200", 9, "Your Second Type Variant"));

			var uwebshopDocType = contentTypeService.GetContentType("uWebshop");
			var uwebshop = contentService.GetContentOfContentType(uwebshopDocType.Id).FirstOrDefault(x => !x.Trashed);
			if (uwebshop != null)
			{
				uwebshop.SortOrder = 10;
				contentToSaveAndPublish.Add(uwebshop);
			}

			if (!storePresent)
			{
				Umbraco.Helpers.InstallStore("uWebshop");
			}
			var stores = contentService.GetContentOfContentType(storeDocType.Id);
			var store = stores.FirstOrDefault(x => !x.Trashed);
			if (store != null && (!storePresent || stores.All(s => !s.Published)))
			{
				var email = string.Format(storePresent ? "starterkitdemo@{0}.com" : "info@{0}.com", store.Name);
				store.SetValue("orderNumberPrefix", store.Name);
				store.SetValue("globalVat", "0");
				store.SetValue("countryCode", "DK");
				store.SetValue("storeEmailFrom", email);
				store.SetValue("storeEmailTo", email);
				store.SetValue("storeEmailFromName", "uWebshop Demo");
				contentToSaveAndPublish.Add(store);
			}

			var homepage = contentService.GetContentOfContentType(homepagePackageDocType.Id).OrderByDescending(x => x.CreateDate).FirstOrDefault(x => !x.Trashed);
			if (homepage != null)
			{
				if (store != null) homepage.SetValue(Constants.StorePickerAlias, store.Id);
				homepage.SortOrder = 1;
				//contentToSaveAndPublish.Add(homepage);
				contentService.PublishWithChildren(homepage);
			}

			contentService.Save(contentToSaveAndPublish);
			contentToSaveAndPublish.ForEach(content => contentService.Publish(content));

			contentService.RePublishAll();
		//	try
		//	{
		//		library.RefreshContent();
		//	}
		//	catch
		//	{

		//	}
		//	try
		//	{
		//		if (uWebshopDocId > 0)
		//		{
		//			//var uWebshopDocument = new Document(uWebshopDocId);
		//			PublishWithChildrenWithResult(uWebshopDocument, author);
		//		}
		//		else
		//		{
		//			umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: uWebshopDocId == 0 No Nodes Published");
		//		}
		//	}
		//	catch
		//	{
		//		umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: PublishWithChildrenWithResult");
		//	}

		//	try
		//	{
		//		if (uWebshopDocId > 0)
		//		{
		//			//var uWebshopDocument = new Document(uWebshopDocId);
		//			UpdateDocumentCache(uWebshopDocument);
		//		}
		//		else
		//		{
		//			umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: uWebshopDocId == 0 No Nodes Published");
		//		}
		//	}
		//	catch
		//	{
		//		umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop  SandboxInstaller: UpdateDocumentCache");
		//	}

		//	try
		//	{
		//		if (homepageDocId > 0)
		//		{
		//			PublishWithChildrenWithResult(new Document(homepageDocId), author);
		//		}
		//		else
		//		{
		//			umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: homePageDoc == 0 No Nodes Published");
		//		}
		//	}
		//	catch
		//	{
		//		umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer homePageDoc: PublishWithChildrenWithResult");
		//	}

		//	try
		//	{
		//		if (homepageDocId > 0)
		//		{
		//			UpdateDocumentCache(new Document(homepageDocId));
		//		}
		//		else
		//		{
		//			umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: homePageDoc == 0 No Nodes Published");
		//		}
		//	}
		//	catch
		//	{
		//		umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop  SandboxInstaller homePageDoc: UpdateDocumentCache");
		//	}

		//	BasePage.Current.ClientTools.RefreshTree("content");

		//	try
		//	{
		//		ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"].RebuildIndex();
		//	}
		//	catch
		//	{
		//		BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "ExternalIndexer Error", "Please Republish All uWebshop Nodes");
		//		uWebshop.Domain.Log.Instance.LogError("uWebshop Installer: Could Not Rebuild ExternalIndexer; Publish uWebshop node + children manually");
		//		return false;
		//	}
			return true;
		}

		private static IContent CreateProductContent(IContentService contentService, IContent testCategory, string title, string sku, string price, int stock)
		{
			var testProduct = contentService.CreateContent(title, testCategory, Product.NodeAlias);
			testProduct.SetValue("title", title);
			testProduct.SetValue("url", title);
			testProduct.SetValue("sku", sku);
			testProduct.SetValue("price", price);
			UWebshopStock.UpdateStock(testProduct.Id, stock, false, string.Empty);
			return testProduct;
		}

		private static IContent CreateVariantContent(IContentService contentService, IContent testProduct, string variantGroup, string color, string sku, string price, int stock, string variantName)
		{
			var testProductVariant = contentService.CreateContent(variantName, testProduct, ProductVariant.NodeAlias);
			testProductVariant.SetValue("title", color);
			testProductVariant.SetValue("sku", sku);
			testProductVariant.SetValue("group", variantGroup);
			testProductVariant.SetValue("price", price);
			testProductVariant.SetValue("backorderStatus", "disable");
			UWebshopStock.UpdateStock(testProductVariant.Id, stock, false, string.Empty);
			return testProductVariant;
		}

		public static PropertyGroup GetOrAddPropertyGroup(IContentType contentType, string groupName)
		{
			var group = contentType.PropertyGroups.FirstOrDefault(g => g.Name == groupName);
			if (group == null)
			{
				var sortOrder = contentType.PropertyGroups.Select(g => g.SortOrder).OrderByDescending(x => x).FirstOrDefault() + 1;
				group = new PropertyGroup { Name = groupName, SortOrder = sortOrder };
				contentType.PropertyGroups.Add(group);
			}
			return group;
		}


	}
}
