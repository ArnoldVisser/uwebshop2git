﻿using System;
using System.Web.UI;
using uWebshop.Domain;

namespace uWebshop.Umbraco6
{
	public partial class uWebshopDashBoard : UserControl
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			Licensing.uWebshopTrialMessage();
		}

		public void BtnInstallDocumentTypesClick(object sender, EventArgs e)
		{
			var docInstaller = new DocumentTypeInstaller();

			docInstaller.InstallDocumentTypes();
		}

		public void BtnDeleteAllDocumentTypesClick(object sender, EventArgs e)
		{
			var docInstaller = new DocumentTypeInstaller();

			docInstaller.DeleteAllDocTypes();
		}

	}
}