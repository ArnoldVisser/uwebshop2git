﻿using Umbraco.Core;
using Umbraco.Web;
using uWebshop.Common.Interfaces;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco;
using uWebshop.Umbraco.Interfaces;

namespace uWebshop.Umbraco6
{
	public class RegisterTypes : IUmbracoTypeRegisteringService
	{
		public void Register(IIocContainer container)
		{
			container.RegisterType<ILoggingService, UmbracoLoggingService>();
			container.RegisterType<IUmbracoVersion, UmbracoVersion>();
			container.RegisterType<ICMSInstaller, CMSInstaller>();
            container.RegisterType<IUmbracoDocumentTypeInstaller, UmbracoDocumentTypeInstaller>();
			container.RegisterType<ICMSChangeContentService, CMSChangeContentService>();

			container.RegisterInstance(ApplicationContext.Current.Services.ContentService);
			container.RegisterInstance(ApplicationContext.Current.Services.ContentTypeService);
		}
	}
}
