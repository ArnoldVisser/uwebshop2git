using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Xml;
using System.Xml.Linq;
using umbraco;
using umbraco.BasePages;
using umbraco.BusinessLogic;
using umbraco.businesslogic;
using umbraco.cms.businesslogic;
using umbraco.cms.businesslogic.web;
using umbraco.cms.helpers;
using umbraco.cms.presentation.Trees;
using Umbraco.Core;
using umbraco.NodeFactory;
using umbraco.presentation.masterpages;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Services;
using uWebshop.Umbraco.Businesslogic;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Umbraco6
{
	//todo: new api, major cleanup
	public class ApplicationEventHandler : ApplicationStartupHandler
	{
		static ApplicationEventHandler()
		{
			try
			{
				Umbraco.RegisterTypes.Register();
				Log.Instance.LogDebug("uWebshop initialized");
			}
			catch (Exception ex)
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "Error while initializing uWebshop, most likely due to wrong umbraco.config, please republish the site " + ex.Message);
			}
		}
		#region constructors

		public ApplicationEventHandler()
		{
			Document.BeforePublish += DocumentBeforePublish;
			Document.AfterPublish += DocumentAfterPublish;
			Document.BeforeSave += DocumentBeforeSave;
			Document.AfterSave += DocumentAfterSave;
			Document.New += DocumentNew;
			Document.AfterMoveToTrash += DocumentAfterMoveToTrash;
			Document.AfterUnPublish += DocumentOnAfterUnPublish;
			Document.AfterDelete += DocumentOnAfterDelete;
			DocumentType.AfterSave += DocumentTypeOnAfterSave;
			BaseTree.BeforeNodeRender += BaseContentTree_BeforeNodeRender;
			umbracoPage.Load += UmbracoPageLoad;
			content.AfterUpdateDocumentCache += ContentOnAfterUpdateDocumentCache;
			OrderInfo.BeforeStatusChanged += OrderBeforeStatusChanged;
			OrderInfo.AfterStatusChanged += OrderEvents.OrderStatusChanged;
			//OrderInfo.OrderLoaded += OrderInfoOrderLoaded;	example
			
			UmbracoDefault.BeforeRequestInit += UmbracoDefaultBeforeRequestInit;
			UmbracoDefault.AfterRequestInit += UmbracoDefaultAfterRequestInit;
		}

		private void DocumentOnAfterDelete(Document sender, umbraco.cms.businesslogic.DeleteEventArgs e)
		{
			ClearCaches(sender);
		}

		//public void OrderInfoOrderLoaded(OrderInfo orderInfo)
		//{ // example
		//	const int productLimit = 10;
		//	orderInfo.RegisterCustomOrderValidation(
		//	order => order.OrderLines.All(line => line.ProductInfo.ItemCount.GetValueOrDefault(1) <= productLimit),
		//	order => "TheUniqueKeyThatWillShowInTheError");
		//}

		private void DocumentTypeOnAfterSave(DocumentType sender, SaveEventArgs saveEventArgs)
		{
			//if (UwebshopConfiguration.Current.RebuildExamineIndex && ExamineManager.Instance != null)
			//{
			//	var externalIndex = ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"];
			//	if (externalIndex != null) externalIndex.RebuildIndex();
			//}
			// todo: clear full cache after some time
		}

		private void ContentOnAfterUpdateDocumentCache(Document sender, DocumentCacheEventArgs documentCacheEventArgs)
		{
			if (sender.ContentType.Alias.StartsWith("uwbs") && sender.ContentType.Alias != Order.NodeAlias)
			{
				IO.Container.Resolve<IApplicationCacheManagingService>().ReloadEntityWithGlobalId(sender.Id, sender.ContentType.Alias);
				//UmbracoStaticCachedEntityRepository.ResetStaticCache();
			}
		}
		

		private void DocumentOnAfterUnPublish(Document sender, UnPublishEventArgs unPublishEventArgs)
		{
			// todo: check whether this event is thrown when node is automatically unpublished by umbraco (after certain datetime)
			ClearCaches(sender);
		}

		private static void ClearCaches(Document sender)
		{
			if (sender.ContentType.Alias.StartsWith("uwbs") && sender.ContentType.Alias != Order.NodeAlias)
			{
				IO.Container.Resolve<IApplicationCacheManagingService>().UnloadEntityWithGlobalId(sender.Id, sender.ContentType.Alias);
				//UmbracoStaticCachedEntityRepository.ResetStaticCache();
			}
		}

		private static void DocumentBeforeSave(Document sender, SaveEventArgs e)
		{
			if (sender.ContentType.Alias.StartsWith(Store.NodeAlias))
			{
				var reg = new Regex(@"\s*");
				var storeAlias = reg.Replace(sender.Text, "");

				sender.Text = storeAlias;
				return;
			}

			var doesCategoryWithNameAlreadyExists = IO.Container.Resolve<IUrlRewritingService>().ResolveUwebshopEntityUrl("/" + sender.Text);
			if (!sender.ContentType.Alias.StartsWith(Category.NodeAlias) && doesCategoryWithNameAlreadyExists.Entity != null)
			{
				sender.Text = sender.Text + " (1)";
			}

			var parentId = sender.ParentId;

			if (parentId < 0)
			{
				return;
			}
			
			var parentDoc = new Document(sender.ParentId);
			if (parentDoc.ContentType != null && (Category.IsAlias(sender.ContentType.Alias) && parentDoc.ContentType.Alias == Catalog.CategoryRepositoryNodeAlias)) 
			{
				var docs = GlobalSettings.HideTopLevelNodeFromPath ? Document.GetRootDocuments().SelectMany(d => d.Children).ToArray() : Document.GetRootDocuments();

				FixRootCategoryUrlName(sender, docs, "url");
				
				foreach (var store in StoreHelper.GetAllStores())
				{
					string multiStorePropertyName;
					if (GetMultiStorePropertyName(sender, store, out multiStorePropertyName)) continue;
					FixRootCategoryUrlName(sender, docs, multiStorePropertyName);
				}
			}
		}

		private static bool GetMultiStorePropertyName(Document sender, Store store, out string propertyName)
		{
			propertyName = "url_" + store.Alias.ToLower();

			if (sender.getProperty(propertyName) != null) return false;
			propertyName = "url_" + store.Alias.ToUpper();

			if (sender.getProperty(propertyName) != null) return false;
			propertyName = "url_" + store.Alias;

			return sender.getProperty(propertyName) == null;
		}

		private static void FixRootCategoryUrlName(Document sender, Document[] docs, string propertyName)
		{
			var urlName = sender.getProperty(propertyName).Value.ToString();
			if (docs.Any(x => urlName == x.Text))
			{
				int count = 1;
				var existingRenames = docs.Where(x => x.Text.StartsWith(urlName)).Select(x => x.Text)
					.Select(x => Regex.Match(x, urlName + @" [(](\d)[)]").Groups[1].Value).Select(x => { int i; int.TryParse(x, out i); return i; });
				if (existingRenames.Any())
				{
					count = existingRenames.Max() + 1;
				}
				sender.SetProperty(propertyName, urlName + " (" + count + ")");
			}
		}

		private static void UmbracoPageLoad(object sender, EventArgs e)
		{
		
		}
		
		private static void DocumentAfterMoveToTrash(Document sender, MoveToTrashEventArgs e)
		{
			if (sender.ContentType.Alias.StartsWith(Store.NodeAlias))
			{
                var reg = new Regex(@"\s*");
                var storeAlias = reg.Replace(sender.Text, "");
                StoreHelper.UnInstallStore(storeAlias);
			}
			ClearCaches(sender);
		}

		private static void DocumentNew(Document sender, NewEventArgs e)
		{
			if (sender.ContentType.Alias.StartsWith(Store.NodeAlias))
			{
				var reg = new Regex(@"\s*");
				var storeAlias = reg.Replace(sender.Text, "");

				Umbraco.Helpers.InstallStore(storeAlias, sender);

				sender.Text = storeAlias;
				sender.Save();
			}
		}

		protected void UmbracoDefaultBeforeRequestInit(object sender, RequestInitEventArgs e)
		{
			try
			{
				var currentNode = Node.GetCurrent();

				if (currentNode.NodeTypeAlias.StartsWith(ProductVariant.NodeAlias))
				{
					var product = DomainHelper.GetProductById(currentNode.Parent.Id);
					if (product != null) HttpContext.Current.Response.RedirectPermanent(product.NiceUrl(), true);
				}
				if (currentNode.NodeTypeAlias.StartsWith(Product.NodeAlias))
				{
					var product = DomainHelper.GetProductById(currentNode.Id);
					if (product != null) HttpContext.Current.Response.RedirectPermanent(product.NiceUrl(), true);
				}
				if (currentNode.NodeTypeAlias.StartsWith(Category.NodeAlias))
				{
					var category = DomainHelper.GetCategoryById(currentNode.Id);
					if (category != null) HttpContext.Current.Response.RedirectPermanent(category.NiceUrl(), true);
				}
			}
// ReSharper disable once EmptyGeneralCatchClause
			catch (Exception)
			{
				// intentionally left empty, because Umbraco will serve a 404
			}
		}

		private static void UmbracoDefaultAfterRequestInit(object sender, RequestInitEventArgs e)
		{
			var currentMember = Membership.GetUser();

			var currentorder = UwebshopRequest.Current.OrderInfo ?? OrderHelper.GetOrderInfo();

			if (currentorder != null && (currentMember != null && currentorder.CustomerInfo.LoginName != currentMember.UserName))
			{
				
				uWebshopOrders.SetCustomer(currentorder.UniqueOrderId, currentMember.UserName); // TODO: dip
				currentorder.CustomerInfo.LoginName = currentMember.UserName;

				if (currentMember.ProviderUserKey != null)
				{
					uWebshopOrders.SetCustomerId(currentorder.UniqueOrderId, (int) currentMember.ProviderUserKey);
					currentorder.CustomerInfo.CustomerId = (int) currentMember.ProviderUserKey; // TODO: dip
				}


				currentorder.ResetDiscounts();
				currentorder.Save();
			}

			if (currentorder != null && currentMember == null && !string.IsNullOrEmpty(currentorder.CustomerInfo.LoginName))
			{

				uWebshopOrders.SetCustomer(currentorder.UniqueOrderId, string.Empty); // TODO: dip
				currentorder.CustomerInfo.LoginName = string.Empty;

				uWebshopOrders.SetCustomerId(currentorder.UniqueOrderId, 0);
				currentorder.CustomerInfo.CustomerId = 0; // TODO: dip
				
				currentorder.ResetDiscounts();
				currentorder.Save();
			}

			var paymentProvider = UwebshopRequest.Current.PaymentProvider;
			if (paymentProvider != null)
			{
				Log.Instance.LogDebug("UmbracoDefaultAfterRequestInit paymentProvider: " + paymentProvider.Name);
				((UmbracoDefault) sender).MasterPageFile = template.GetMasterPageName(paymentProvider.Node.template);
				return;
			}

			// todo: ombouwen naar UwebshopRequest.Current.Category, UwebshopRequest.Current lostrekken (ivm speed)
			var currentCategoryId = HttpContext.Current.Request["resolvedCategoryId"];
			var currentProductId = HttpContext.Current.Request["resolvedProductId"];

			if (!string.IsNullOrEmpty(currentCategoryId))   //string.IsNullOrEmpty(currentProductUrl))
			{
				int categoryId;
				if (!int.TryParse(currentCategoryId, out categoryId))
					return;
				var categoryFromUrl = DomainHelper.GetCategoryById(categoryId);
				if (categoryFromUrl == null) return;

				if (categoryFromUrl.Disabled)
				{
					HttpContext.Current.Response.StatusCode = 404;
					HttpContext.Current.Response.Redirect(library.NiceUrl(int.Parse(GetCurrentNotFoundPageId())), true);
					return;
				}

				if (Access.HasAccess(categoryFromUrl.Id, categoryFromUrl.Path, Membership.GetUser()))
				{
					if (categoryFromUrl.Template != 0)
					{
						//umbraco.cms.businesslogic.template.Template.GetTemplate(currentCategory.Template).TemplateFilePath
						((UmbracoDefault) sender).MasterPageFile = template.GetMasterPageName(categoryFromUrl.Template);
						//// get the template
						//var t = template.GetMasterPageName(currentCategory.Template);
						//// you did this and it works pre-4.10, right?
						//page.MasterPageFile = t;
						//// now this should work starting with 4.10
						//e.Page.Template = t;
					}

					var altTemplate = HttpContext.Current.Request["altTemplate"];
					if (!string.IsNullOrEmpty(altTemplate))
					{
						var altTemplateId = umbraco.cms.businesslogic.template.Template.GetTemplateIdFromAlias(altTemplate);

						if (altTemplateId != 0)
						{
							((UmbracoDefault) sender).MasterPageFile = template.GetMasterPageName(altTemplateId);
						}
					}
				}
				else
				{
					if (HttpContext.Current.User.Identity.IsAuthenticated)
					{
						HttpContext.Current.Response.Redirect(library.NiceUrl(Access.GetErrorPage(categoryFromUrl.Path)), true);
					}
					HttpContext.Current.Response.Redirect(library.NiceUrl(Access.GetLoginPage(categoryFromUrl.Path)), true);
				}
			}
			else if (!string.IsNullOrEmpty(currentProductId)) // else
			{
				int productId;
				if (!int.TryParse(currentProductId, out productId))
					return;
				var productFromUrl = DomainHelper.GetProductById(productId);
				if (productFromUrl == null) return;

				if (Access.HasAccess(productFromUrl.Id, productFromUrl.Path, Membership.GetUser()))
				{
					if (productFromUrl.Template != 0)
					{
						((UmbracoDefault) sender).MasterPageFile = template.GetMasterPageName(productFromUrl.Template);
					}

					var altTemplate = HttpContext.Current.Request["altTemplate"];
					if (!string.IsNullOrEmpty(altTemplate))
					{
						var altTemplateId = umbraco.cms.businesslogic.template.Template.GetTemplateIdFromAlias(altTemplate);

						if (altTemplateId != 0)
						{
							((UmbracoDefault)sender).MasterPageFile = template.GetMasterPageName(altTemplateId);
						}
					}
				}
				else
				{
					if (HttpContext.Current.User.Identity.IsAuthenticated)
					{
						HttpContext.Current.Response.Redirect(library.NiceUrl(Access.GetErrorPage(productFromUrl.Path)), true);
					}
					HttpContext.Current.Response.Redirect(library.NiceUrl(Access.GetLoginPage(productFromUrl.Path)), true);
				}
			}
		}

		internal static string GetCurrentNotFoundPageId()
		{
			library.GetCurrentDomains(1);
			string error404 = "";
			XmlNode error404Node = UmbracoSettings.GetKeyAsNode("/settings/content/errors/error404");
			if (error404Node.ChildNodes.Count > 0 && error404Node.ChildNodes[0].HasChildNodes)
			{
				// try to get the 404 based on current culture (via domain)
				XmlNode cultureErrorNode;
				if (umbraco.cms.businesslogic.web.Domain.Exists(HttpContext.Current.Request.ServerVariables["SERVER_NAME"]))
				{
					umbraco.cms.businesslogic.web.Domain d = umbraco.cms.businesslogic.web.Domain.GetDomain(HttpContext.Current.Request.ServerVariables["SERVER_NAME"]);
					// test if a 404 page exists with current culture
					cultureErrorNode = error404Node.SelectSingleNode(String.Format("errorPage [@culture = '{0}']", d.Language.CultureAlias));
					if (cultureErrorNode != null && cultureErrorNode.FirstChild != null)
						error404 = cultureErrorNode.FirstChild.Value;
				}
				else if (error404Node.SelectSingleNode(string.Format("errorPage [@culture = '{0}']", System.Threading.Thread.CurrentThread.CurrentUICulture.Name)) != null)
				{
					cultureErrorNode = error404Node.SelectSingleNode(string.Format("errorPage [@culture = '{0}']", System.Threading.Thread.CurrentThread.CurrentUICulture.Name));
					if (cultureErrorNode.FirstChild != null)
						error404 = cultureErrorNode.FirstChild.Value;
				}
				else
				{
					cultureErrorNode = error404Node.SelectSingleNode("errorPage [@culture = 'default']");
					if (cultureErrorNode != null && cultureErrorNode.FirstChild != null)
						error404 = cultureErrorNode.FirstChild.Value;
				}
			}
			else
				error404 = UmbracoSettings.GetKey("/settings/content/errors/error404");
			return error404;
		}


		protected void OrderBeforeStatusChanged(OrderInfo orderInfo, BeforeOrderStatusChangedEventArgs e)
		{
		}

		#endregion

		public static SortedDictionary<string, string> GetCountryList()
		{
			//create a new Generic list to hold the country names returned
			var cultureList = new SortedDictionary<string, string>();

			//create an array of CultureInfo to hold all the cultures found, these include the users local cluture, and all the
			//cultures installed with the .Net Framework
			var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);

			//loop through all the cultures found
			foreach (var culture in cultures)
			{
				try
				{
					//pass the current culture's Locale ID (http://msdn.microsoft.com/en-us/library/0h88fahh.aspx)
					//to the RegionInfo contructor to gain access to the information for that culture
					var region = new RegionInfo(culture.LCID);

					//make sure out generic list doesnt already
					//contain this country
					if (!cultureList.ContainsKey(region.EnglishName))
						//not there so add the EnglishName (http://msdn.microsoft.com/en-us/library/system.globalization.regioninfo.englishname.aspx)
						//value to our generic list

						cultureList.Add(region.EnglishName, region.TwoLetterISORegionName);
				}
				catch
				{
				}
			}

			return cultureList;
		}

		public static void DocumentAfterSave(Document sender, SaveEventArgs e)
		{
		}

		protected void DocumentAfterPublish(Document sender, PublishEventArgs e)
		{
			if (sender.ContentType.Alias.StartsWith(Store.NodeAlias))
			{
				var node = new Node(sender.Id);
				if (!sender.Text.Equals(node.Name))
				{
					StoreHelper.RenameStore(node.Name, sender.Text);
				}
			}

			if (sender.Level > 2)
			{

				if (sender.ContentType.Alias == Order.NodeAlias || sender.Parent != null &&
					(OrderedProduct.IsAlias(sender.ContentType.Alias) ||
					 sender.Parent.Parent != null && OrderedProductVariant.IsAlias(sender.ContentType.Alias)))
				{
					var orderDoc = sender.ContentType.Alias == Order.NodeAlias
									   ? sender
									   : (OrderedProduct.IsAlias(sender.ContentType.Alias) && !OrderedProductVariant.IsAlias(sender.ContentType.Alias)
											  ? new Document(sender.Parent.Id)
											  : new Document(sender.Parent.Parent.Id));

					if (orderDoc.ContentType.Alias != Order.NodeAlias)
						throw new Exception("There was an error in the structure of the order documents");

					// load existing orderInfo (why..? => possibly to preserve information not represented in the umbraco documents)

					if (string.IsNullOrEmpty(orderDoc.getProperty("orderGuid").Value.ToString()))
					{
						Store store = null;
						var storeDoc = sender.GetAncestorDocuments().FirstOrDefault(x => x.ContentType.Alias == "uwbsOrderStoreFolder");

						if (storeDoc != null)
						{
							store = StoreHelper.GetAllStores().FirstOrDefault(x => x.Name == storeDoc.Text);
						}

						if (store == null)
						{
							store = StoreHelper.GetAllStores().FirstOrDefault(x => !string.IsNullOrEmpty(x.CurrencyCulture));
						}

						var orderInfo = OrderHelper.CreateOrder(store);
						int generatedId;
						orderInfo.OrderNumber = OrderHelper.GenerateOrderNumber(orderInfo.StoreInfo.Store, orderInfo, out generatedId);
						orderInfo.Status = OrderStatus.Confirmed;
						orderInfo.Save();

						sender.SetProperty("orderGuid", orderInfo.UniqueOrderId.ToString());
						sender.Save();
					}
					else
					{
						var orderGuid = Guid.Parse(orderDoc.getProperty("orderGuid").Value.ToString());

						var orderInfo = OrderHelper.GetOrderInfo(orderGuid);

						var order = new Order(orderDoc.Id);
						orderInfo.CustomerEmail = order.CustomerEmail;
						orderInfo.CustomerFirstName = order.CustomerFirstName;
						orderInfo.CustomerLastName = order.CustomerLastName;

						var dictionaryCustomer =
							orderDoc.GenericProperties.Where(x => x.PropertyType.Alias.StartsWith("customer"))
									.ToDictionary(customerProperty => customerProperty.PropertyType.Alias,
												  customerProperty => customerProperty.Value.ToString());
						orderInfo.AddCustomerFields(dictionaryCustomer, CustomerDatatypes.Customer);

						var dictionaryShipping =
							orderDoc.GenericProperties.Where(x => x.PropertyType.Alias.StartsWith("shipping"))
									.ToDictionary(property => property.PropertyType.Alias, property => property.Value.ToString());
						orderInfo.AddCustomerFields(dictionaryShipping, CustomerDatatypes.Shipping);

						var dictionarExtra =
							orderDoc.GenericProperties.Where(x => x.PropertyType.Alias.StartsWith("extra"))
									.ToDictionary(property => property.PropertyType.Alias, property => property.Value.ToString());
						orderInfo.AddCustomerFields(dictionarExtra, CustomerDatatypes.Extra);

						//orderInfo.SetVATNumber(order.CustomerVATNumber); happens in AddCustomerFields
						var orderPaidProperty = order.Document.getProperty("orderPaid");
						if (orderPaidProperty != null && orderPaidProperty.Value != null)
							orderInfo.Paid = orderPaidProperty.Value.ToString() == "1";

						// load data recursively from umbraco documents into order tree
						orderInfo.OrderLines = orderDoc.Children.Select(d =>
						{
							var fields = d.GenericProperties.Where(x => !OrderedProduct.DefaultProperties.Contains(x.PropertyType.Alias))
									.ToDictionary(s => s.PropertyType.Alias, s => d.GetProperty<string>(s.PropertyType.Alias));

							var xDoc = new XDocument(new XElement("Fields"));

							OrderUpdatingService.AddFieldsToXDocumentBasedOnCMSDocumentType(xDoc, fields, d.ContentType.Alias);

							var orderedProduct = new OrderedProduct(d.Id);

							var productInfo = new ProductInfo(orderedProduct, orderInfo);
							productInfo.ProductVariants = d.Children.Select(cd => new ProductVariantInfo(new OrderedProductVariant(cd.Id), productInfo, productInfo.Vat)).ToList();
							return new OrderLine(productInfo, orderInfo)
							{
								_customData = xDoc
							};
						}).ToList();

						// store order
						IO.Container.Resolve<IOrderRepository>().SaveOrderInfo(orderInfo);
					}
					// cancel does give a warning message balloon in Umbraco.
					//e.Cancel = true;

					//if (sender.ContentType.Alias != Order.NodeAlias)
					//{
					//	orderDoc.Publish(new User(0));
					//}

					//if (orderDoc.ParentId != 0)
					//{
					BasePage.Current.ClientTools.SyncTree(sender.Parent.Path, false);
					BasePage.Current.ClientTools.ChangeContentFrameUrl(String.Concat("editContent.aspx?id=", sender.Id));
					//}
					//orderDoc.delete();
					BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.success, "Order Updated!", "This order has been updated!");


				}
			}
			// remove order nodes from cache after publish, so umbraco.config is not polluted
			if (sender.ContentType.Alias == Order.NodeAlias)
			{
				sender.UnPublish();
				//library.UpdateDocumentCache(sender.Id);
			}
		}

		protected void BaseContentTree_BeforeNodeRender(ref XmlTree sender, ref XmlTreeNode node, EventArgs e)
		{
		}

		// UNPUBLISH & REMOVE Incomplete Orders AFTER EXPIREDATE HAS BEEN PASSED
		protected void DocumentBeforePublish(Document sender, PublishEventArgs e)
		{
			if (sender.ContentType.Alias.StartsWith(Category.NodeAlias))
			{
				SetAliasedPropertiesIfEnabled(sender, "categoryUrl");
			}
			if (sender.ContentType.Alias.StartsWith(Product.NodeAlias))
			{
				SetAliasedPropertiesIfEnabled(sender, "productUrl");
			}

			
			// order => delete node, update SQL

			if (sender.ContentType.Alias == Order.NodeAlias)
			{
				if (string.IsNullOrEmpty(sender.getProperty("orderGuid").Value.ToString()))
				{
					
				}
			}

		}

		private static void SetAliasedPropertiesIfEnabled(Document sender, string propertyName)
		{
			var property = sender.getProperty(propertyName);
			if (property == null) return;
			property.Value = url.FormatUrl(property.Value.ToString());

			foreach (var shopAlias in StoreHelper.GetAllStores())
			{
				var aliasedEnabled = sender.getProperty("enable_" + shopAlias.Alias.ToUpper());
				if (aliasedEnabled == null || aliasedEnabled.Value.ToString() != "1") continue;

				var aliasedproperty = sender.getProperty(propertyName + "_" + shopAlias.Alias.ToUpper());

				// test == test --> overerf van global
				// test == "" --> overef van global
				if (aliasedproperty != null && aliasedproperty.Value == property.Value ||
				    aliasedproperty != null && string.IsNullOrEmpty(aliasedproperty.Value.ToString()))
				{
					aliasedproperty.Value = url.FormatUrl(property.Value.ToString());
				}
					// test == bla --> niets doen
				else if (aliasedproperty != null && !string.IsNullOrEmpty(aliasedproperty.Value.ToString()))
				{
					aliasedproperty.Value = url.FormatUrl(aliasedproperty.Value.ToString());
				}
			}
		}

		//public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		//{
			
		//}

		//public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		//{
			
		//}

		//public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		//{
			
		//}
	}
}