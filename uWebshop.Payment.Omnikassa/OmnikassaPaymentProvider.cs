﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.Omnikassa
{
	public class OmnikassaPaymentProvider : IPaymentProvider
    {
		public string GetName()
		{
			return "OmniKassa";
		}

		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id = 0)
		{
			var paymentProviderMethodList = new List<PaymentProviderMethod>
				{
					new PaymentProviderMethod
						{
							Id = "iDeal",
							ProviderName = GetName(),
							Title = "iDeal",
							Description = "iDeal via Omnikassa"
						}
				};

			return paymentProviderMethodList;
		}
    }
}
