﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.NodeFactory;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.Omnikassa
{
	public class OmniKassaRequestHandler : IPaymentRequestHandler
	{
		public string GetName()
		{
			return "OmniKassa";
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);
			

			var currentNodeId = Node.GetCurrent().Id;
			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

			var successNodeId = 0;
			int.TryParse(paymentProvider.SuccesNodeId, out successNodeId);
			
			var reportUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(paymentProvider.Id));
			
			//	<provider title="OmniKassa">
			//  <MerchantId>002020000000001</MerchantId>
			//  <CurrencyCode>978</CurrencyCode>
			//  <normalReturnUrl>http://www.hetanker.tv</normalReturnUrl>
			//  <KeyVersion>1</KeyVersion>
			//  <TestAmount>56</TestAmount>
			//  <Url>https://payment-webinit.omnikassa.rabobank.nl/paymentServlet</Url>
			//  <TestUrl>https://payment-webinit.simu.omnikassa.rabobank.nl/paymentServlet</TestUrl>
			//  <ForwardUrl>https://payment-web.omnikassa.rabobank.nl/payment</ForwardUrl>
			//  <TestForwardUrl>https://payment-web.simu.omnikassa.rabobank.nl/payment</TestForwardUrl>
			//</provider>

			var merchantId = helper.Settings["MerchantId"];
			if (string.IsNullOrEmpty(merchantId))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: MerchantId" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			
			var keyVersion = helper.Settings["KeyVersion"];
			if (string.IsNullOrEmpty(keyVersion))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: keyVersion" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			
			var currencyCode = helper.Settings["CurrencyCode"];
			if (string.IsNullOrEmpty(currencyCode))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: CurrencyCode" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var securityKey = WebConfigurationManager.AppSettings["SecurityKey"];
			if (string.IsNullOrEmpty(securityKey))
			{
				Log.Instance.LogError("OmniKassa: Missing in Web.Config AppData field with name: SecurityKey" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var testAmount = helper.Settings["TestAmount"];
			if (string.IsNullOrEmpty(testAmount))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: TestAmount" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var testUrl = helper.Settings["TestUrl"];
			if (string.IsNullOrEmpty(testUrl))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: TestUrl" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var liveUrl = helper.Settings["Url"];
			if (string.IsNullOrEmpty(liveUrl))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: Url" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var testForwardUrl = helper.Settings["TestForwardUrl"];
			if (string.IsNullOrEmpty(testForwardUrl))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: TestForwardUrl" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var liveForwardUrl = helper.Settings["ForwardUrl"];
			if (string.IsNullOrEmpty(liveForwardUrl))
			{
				Log.Instance.LogError("OmniKassa: Missing PaymentProvider.Config  field with name: ForwardUrl" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			
			var postUrl = paymentProvider.TestMode ? testUrl : liveUrl;

			var forwardUrl = paymentProvider.TestMode ? testForwardUrl : liveForwardUrl;

			var amount = paymentProvider.TestMode ? testAmount : orderInfo.ChargedAmountInCents.ToString();

			var orderId = orderInfo.OrderNumber;

			if (orderId.Length > 32)
			{
				Log.Instance.LogError("OmniKassa: orderInfo.OrderNumber: Too Long, Max 32 Characters!" + ", paymentProviderNodeId: " + paymentProvider.Id);
				orderId = orderInfo.OrderNumber.Substring(0, 31);
			}

			var uniqueId = orderId + "x" + DateTime.Now.ToString("hhmmss");

			Regex rgx = new Regex("[^a-zA-Z0-9]");
			uniqueId = rgx.Replace(uniqueId, "");

			if (uniqueId.Length > 35)
			{
				Log.Instance.LogError("OmniKassa: uniqueId (orderId + 'x' + DateTime.Now.ToString('hhmmss')): Too Long, Max 35 Characters!" + ", paymentProviderNodeId: " + paymentProvider.Id);
				uniqueId = uniqueId.Substring(0, 34);
			}
			

			if (reportUrl.Length > 512)
			{
				Log.Instance.LogError("OmniKassa: reportUrl: Too Long, Max 512 Characters!" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			// Data-veld samenstellen
			var data = String.Format("merchantId={0}", merchantId)
						  + String.Format("|orderId={0}", orderId)
						  + String.Format("|amount={0}", amount)
						  + String.Format("|customerLanguage={0}", "NL")
						  + String.Format("|keyVersion={0}", keyVersion)
						  + String.Format("|currencyCode={0}", currencyCode)
				// + String.Format("|PaymentMeanBrandList={0}", "IDEAL")
						  + String.Format("|normalReturnUrl={0}", reportUrl)
						  + String.Format("|automaticResponseUrl={0}", reportUrl)
						  + String.Format("|transactionReference={0}", uniqueId);
			
			// Seal-veld berekenen
			var sha256 = SHA256.Create();
			var hashValue = sha256.ComputeHash(new UTF8Encoding().GetBytes(data + securityKey));


			// POST data samenstellen
			var postData = new NameValueCollection
				               {
					               {"Data", data},
					               {"Seal", ByteArrayToHexString(hashValue)},
					               {"InterfaceVersion", "HP_1.0"}
				               };

			uWebshop.DataAccess.uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, uniqueId);
			orderInfo.PaymentInfo.TransactionId = uniqueId;

			//// Posten van data 
			byte[] response;
			using (var client = new WebClient())
				response = client.UploadValues(postUrl, postData);

			try
			{
				var responseData = Encoding.UTF8.GetString(response);

				var trimmedResponse = responseData.Trim();


				var matchedHiddenfield = Regex.Matches(trimmedResponse, "<input type=HIDDEN.+/>",
				                                       RegexOptions.IgnoreCase | RegexOptions.Multiline);

				var postValueFromHiddenField = Regex.Matches(matchedHiddenfield[0].Value, "(?<=\\bvalue=\")[^\"]*",
				                                       RegexOptions.IgnoreCase | RegexOptions.Multiline);

				var redirectionDataField = string.Format("redirectionData={0}", postValueFromHiddenField[0].Value);


				orderInfo.PaymentInfo.Url = forwardUrl;
				orderInfo.PaymentInfo.Parameters = redirectionDataField;
				orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;
				orderInfo.Save();


				var request = new PaymentRequest();

				return request;
			}
			catch
			{
				var responseResult = Encoding.UTF8.GetString(response);
				Log.Instance.LogError("Omnikassa: " + responseResult);
				throw new Exception("OmniKassa Issue Please Notice Shopowner");
			}
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return orderInfo.PaymentInfo.Url;
		}

		// Converteer een String naar Hexadecimale waarde
		public string ByteArrayToHexString(byte[] bytes)
		{
			var result = new StringBuilder(bytes.Length * 2);
			const string hexAlphabet = "0123456789ABCDEF";

			foreach (var b in bytes)
			{
				result.Append(hexAlphabet[b >> 4]);
				result.Append(hexAlphabet[b & 0xF]);
			}

			return result.ToString();
		}
	}
}
