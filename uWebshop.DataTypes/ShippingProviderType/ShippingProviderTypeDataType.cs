﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uWebshop.DataTypes.CountySelector;
using umbraco.cms.businesslogic.datatype;
using umbraco.interfaces;

namespace uWebshop.DataTypes.ShippingProviderType
{
	public class ShippingProviderTypeDataType : BaseDataType, IDataType
	{
		public static readonly Guid DefId = new Guid("2cdfc385-15dd-4165-84d9-b5e4b2aca970");
		public static readonly Guid Key = new Guid("c035ada7-5413-48de-8e62-b4b61e2e934f");
		public static readonly string Name = "Shipping Provider Type";
		public static readonly DatabaseType DatabaseType = DatabaseType.Ntext;

		private IDataEditor _editor;
		private IData _baseData;

		public override Guid Id
		{
			get { return DefId; }
		}

		public override string DataTypeName
		{
			get { return Name; }
		}

		public override IDataEditor DataEditor
		{
			get { return _editor ?? (_editor = new ShippingProviderTypeDataEditor(Data)); }
		}

		private PrevalueEditor _prevalueeditor;

		public override IDataPrevalue PrevalueEditor
		{
			get { return _prevalueeditor ?? (_prevalueeditor = new PrevalueEditor()); }
		}

		public override IData Data
		{
			get { return _baseData ?? (_baseData = new DefaultData(this)); }
		}
	}
}