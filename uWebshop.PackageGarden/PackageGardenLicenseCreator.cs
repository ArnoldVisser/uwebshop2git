﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Xml;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco.BusinessLogic;
using PackageGarden.Licensing;
using PackageGarden.Licensing.UmbracoLibrary;
using umbraco.cms.businesslogic.member;
using Log = uWebshop.Domain.Log;
using uWebshop.Umbraco;

namespace uWebshop.PackageGarden
{
	public class PackageGardenLicenseCreator : ApplicationBase
    {
		public PackageGardenLicenseCreator()
		{
			OrderInfo.OrderPaidChanged += OrderInfo_OrderPaidChanged;
		}

		private void OrderInfo_OrderPaidChanged(OrderInfo orderInfo, OrderPaidChangedEventArgs e)
		{
			Log.Instance.LogError("PackageGarden Start");

			if (e.Paid != true)
			{
				Log.Instance.LogError("PackageGarden e.Paid != true");
				return;
			}
			//packageGardenLicense

			var licencesOrderlines =
				orderInfo.OrderLines.Where(x => !string.IsNullOrEmpty(x.ProductInfo.Product.GetProperty("packageGardenLicense")) ||
				                                x.ProductInfo.ProductVariants.Any(
					                                y => !string.IsNullOrEmpty(y.Variant.GetProperty("packageGardenLicense"))));

			if (!licencesOrderlines.Any())
			{
				Log.Instance.LogError("!PackageGarden licencesOrderlines.Any()");
				return;
			}

			var companyName = OrderHelper.CustomerInformationValue(orderInfo, "customerCompany");

			if (string.IsNullOrEmpty(companyName))
			{
				Log.Instance.LogError("PackageGarden customerCompany not on order");
				return;
			}

			var memberId = orderInfo.CustomerInfo.CustomerId;
			if (orderInfo.CustomerInfo.CustomerId == 0)
			{
				Log.Instance.LogError("PackageGarden orderInfo.CustomerInfo.CustomerId == 0: Member Create");
				var createdMemberId = CreateMember(orderInfo);
				if (createdMemberId == 0)
				{
					Log.Instance.LogError("PackageGarden License CREATE MEMBER FAILED");
					return;
				}
				orderInfo.CustomerInfo.CustomerId = createdMemberId;
				orderInfo.Save();
				memberId = createdMemberId;
			}

			foreach (var orderline in licencesOrderlines)
			{

				var devDomain = string.Empty;
				var stagingDomain = string.Empty;
				var liveDomain = string.Empty;

				var licenseValue = orderline.ProductInfo.Product.GetProperty("packageGardenLicense");
				var licenseDaysValue = orderline.ProductInfo.Product.GetProperty("packageGardenLicenseDays");
				var licenseForVersion = orderline.ProductInfo.Product.GetProperty("packageGardenLicenseVersion");

				if (!string.IsNullOrEmpty(licenseValue))
				{
					var licenseId = licenseValue.Split('|')[0];
					var licenseType = licenseValue.Split('|')[1];


					if (licenseType.ToLower() != "unlimited")
					{
						devDomain = OrderHelper.CustomOrderLineValue(orderInfo, orderline.OrderLineId, "devDomain");
						stagingDomain = OrderHelper.CustomOrderLineValue(orderInfo, orderline.OrderLineId, "stagingDomain");
						liveDomain = OrderHelper.CustomOrderLineValue(orderInfo, orderline.OrderLineId, "liveDomain");

						if (string.IsNullOrEmpty(devDomain))
						{
							Log.Instance.LogError("PackageGarden devDomain not on orderline");
							return;
						}
						if (string.IsNullOrEmpty(stagingDomain))
						{
							Log.Instance.LogError("PackageGarden stagingDomain not on orderline");
							return;
						}
						if (string.IsNullOrEmpty(liveDomain))
						{
							Log.Instance.LogError("PackageGarden liveDomain not on orderline");
							return;
						}
					}


					if (string.IsNullOrEmpty(licenseDaysValue))
					{
						if (string.IsNullOrEmpty(licenseForVersion))
						{
							LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
														liveDomain);
						}
						else
						{
							var version = 0;
							int.TryParse(licenseForVersion, out version);

							LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
														liveDomain, version, null);
						}
					}
					else
					{
						var days = int.Parse(licenseDaysValue);

						var endDate = DateTime.Now.AddDays(days);

						if (string.IsNullOrEmpty(licenseForVersion))
						{
							LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
							                            liveDomain, null, endDate);
						}
						else
						{
							var version = 0;
							int.TryParse(licenseForVersion, out version);
							LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
														liveDomain, version, endDate);
						}
					}
				}

				foreach (var variant in  orderline.ProductInfo.ProductVariants)
				{
					var licenseVariantValue = variant.Variant.GetProperty("packageGardenLicense");
					var licenseVariantDaysValue = variant.Variant.GetProperty("packageGardenLicenseDays");
					var licenseVariantForVersion = variant.Variant.GetProperty("packageGardenLicenseVersion");

					if (!string.IsNullOrEmpty(licenseVariantValue))
					{
						var licenseId = licenseVariantValue.Split('|')[0];
						var licenseType = licenseVariantValue.Split('|')[1];

						if (licenseType.ToLower() != "unlimited")
						{
							devDomain = OrderHelper.CustomOrderLineValue(orderInfo, orderline.OrderLineId, "devDomain");
							stagingDomain = OrderHelper.CustomOrderLineValue(orderInfo, orderline.OrderLineId, "stagingDomain");
							liveDomain = OrderHelper.CustomOrderLineValue(orderInfo, orderline.OrderLineId, "liveDomain");

							if (string.IsNullOrEmpty(devDomain))
							{
								Log.Instance.LogError("PackageGarden devDomain not on orderline");
								return;
							}
							if (string.IsNullOrEmpty(stagingDomain))
							{
								Log.Instance.LogError("PackageGarden stagingDomain not on orderline");
								return;
							}
							if (string.IsNullOrEmpty(liveDomain))
							{
								Log.Instance.LogError("PackageGarden liveDomain not on orderline");
								return;
							}
						}

						if (string.IsNullOrEmpty(licenseVariantDaysValue))
						{
							if (string.IsNullOrEmpty(licenseVariantForVersion))
							{
								LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
														liveDomain);
							}
							else
							{
								var version = 0;
								int.TryParse(licenseForVersion, out version);

								LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
														liveDomain, version, null);
							}
							
						}
						else
						{
							var days = int.Parse(licenseVariantDaysValue);

							var endDate = DateTime.Now.AddDays(days);

							if (string.IsNullOrEmpty(licenseVariantForVersion))
							{
								LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
								                            liveDomain, null, endDate);
							}
							else
							{
								var version = 0;
								int.TryParse(licenseForVersion, out version);

								LicenseHelper.CreateLicense(licenseType, int.Parse(licenseId), memberId, companyName, devDomain, stagingDomain,
															liveDomain, version, endDate);
							}
						}
					}
				}
			}
		}

		private static int CreateMember(OrderInfo orderInfo)
		{
			var passwordValue = Membership.GeneratePassword(Membership.MinRequiredPasswordLength, Membership.MinRequiredNonAlphanumericCharacters);
			
			var webConfig = new XmlDocument();
			webConfig.Load(HttpContext.Current.Server.MapPath("~/web.config"));

			var umbracoDefaultMemberTypeAlias = webConfig.SelectSingleNode("//add[@defaultMemberTypeAlias]");

			var memberTypes = MemberType.GetAll;

			if (umbracoDefaultMemberTypeAlias == null || umbracoDefaultMemberTypeAlias.Attributes == null)
			{
				Log.Instance.LogError("DefaultMemberTypeAliasError");
				return 0;
			}
			var memberTypevalue = umbracoDefaultMemberTypeAlias.Attributes["defaultMemberTypeAlias"].Value;

			if (memberTypes.All(x => x.Alias != memberTypevalue))
			{
				Log.Instance.LogError("DefaultMemberTypeAliasNonExistingError");
				return 0;
			}
			if (string.IsNullOrEmpty(orderInfo.CustomerEmail))
			{
				Log.Instance.LogError("CustomerEmailEmpty");
				return 0;
			}

			

			var memberUserName = Membership.GetUserNameByEmail(orderInfo.CustomerEmail);

			if (memberUserName != null)
			{
				var existingMember = Membership.GetUser(memberUserName);
				if (existingMember != null && existingMember.ProviderUserKey != null)
				{
					return (int) existingMember.ProviderUserKey;
				}
				Log.Instance.LogDebug( "CreateAccount: MemberExists, but errors");
				return 0;
			}

			var membershipUser = Membership.CreateUser(orderInfo.CustomerEmail, passwordValue, orderInfo.CustomerEmail);

			if (!Roles.GetAllRoles().Any())
			{
				const string customersRole = "Customers";

				if (!Roles.RoleExists(customersRole))
				{
					Roles.CreateRole(customersRole);
				}

				Roles.AddUserToRole(membershipUser.UserName, customersRole);
			}
			else
			{
				Roles.AddUserToRole(membershipUser.UserName, Roles.GetAllRoles().First());
			}

			var testRol = Roles.GetUsersInRole("test");

			foreach (var user in testRol)
			{
				var orders = OrderHelper.GetOrdersForCustomer(user);
			}

			var profile = ProfileBase.Create(membershipUser.UserName);
			
			profile.Save();

			var currentStore = orderInfo.StoreInfo.Store;

			if (string.IsNullOrEmpty(currentStore.AccountCreatedEmail))
			{
				Log.Instance.LogDebug( "CreateAccount: AccountCreatedEmail not set: No email send to customer");
				return 0;
			}

			if (membershipUser.ProviderUserKey != null)
			{
				var emailNodeId = Convert.ToInt32(currentStore.AccountCreatedEmail);
				EmailHelper.SendMemberEmailCustomer(emailNodeId, currentStore, orderInfo.CustomerEmail, passwordValue);

				return (int) membershipUser.ProviderUserKey;
			}

			return 0;
		}
    }
}
