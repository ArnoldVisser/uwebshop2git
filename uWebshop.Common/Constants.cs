﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uWebshop.Common
{
	public static class Constants
	{
		public const string ErrorMessagesSessionKey = "uwbsErrorMessages";

		public const string CreateMemberSessionKey = "AccountCreate";
		public const string CreateMemberSessionKeyAddition = "AccountCreateAddition";

		public const string CouponCodeResultSessionKey = "CouponCodeResult";
		public const string CouponCodeSessionKey = "CouponCode";

		public const string StorePickerAlias = "uwbsStorePicker";

		public const string UpdateMemberSessionKey = "AccountUpdate";
		public const string UpdateMemberSessionKeyAddition = "AccountUpdateAddition";

		public const string SignInMemberSessionKey = "AccountSignIn";

		public const string SignOutMemberSessionKey = "AccountSignOut";

		public const string RequestPasswordSessionKey = "AccountRequestPassword";

		public const string PaymentProviderSessionKey = "PaymentProvider";

		public const string ShippingProviderSessionKey = "ShippingProvider";

		public const string OrderedItemcountHigherThanStockKey = "OrderedItemcountHigherThanStockKey";

		public const string NonMultiStoreAlias = "AllStores";
	}
}