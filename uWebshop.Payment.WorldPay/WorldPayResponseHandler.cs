﻿using System;
using System.Globalization;
using System.Linq;
using System.Web;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.NodeFactory;

namespace uWebshop.Payment.WorldPay
{
	public class WorldPayResponseHandler : IPaymentResponseHandler
	{
		public string GetName()
		{
			return "WorldPay";
		}

		public string HandlePaymentResponse()
		{
			
			var transactionId = HttpContext.Current.Request["reference"];
			Log.Instance.LogDebug("WorldPay reference: " + transactionId);

			//Result of the transaction - "Y" for a successful payment authorisation, "C" for a cancelled payment.
			var status = HttpContext.Current.Request["transStatus"];

			Log.Instance.LogDebug("WorldPay status: " + status);
			
			var authAmount = HttpContext.Current.Request["authAmount"];

			var paymentProvider =
				PaymentProviderHelper.GetAllPaymentProviders().FirstOrDefault(x => x.Name.ToLower() == GetName().ToLower());

			#region build urls

			var currentNodeId = Node.GetCurrent().Id;

			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

			var success = true;
			var successUrl = string.Empty;
			var cancelUrl = string.Empty;


			if (paymentProvider != null)
			{
				successUrl = string.Format("{0}{1}", baseUrl,
				                              library.NiceUrl(int.Parse(paymentProvider.SuccesNodeId)));
				cancelUrl = string.Format("{0}{1}", baseUrl,
				                              library.NiceUrl(int.Parse(paymentProvider.ErrorNodeId)));
			}
			else
			{
				Log.Instance.LogDebug("paymentProvider == null, no redirect urls build");
			}
			#endregion

			if (transactionId != null)
			{

				var orderInfo = OrderHelper.GetOrderInfo(transactionId);
				

				if (orderInfo != null && orderInfo.Paid == false)
				{
					Log.Instance.LogDebug("WorldPay order found, payment was false, so continue!");
					
					switch (status)
					{
						case "Y":

							var chargedAmount = String.Format("{0:0.00}", orderInfo.ChargedAmount);
							Log.Instance.LogDebug("WorldPay orderInfo.ChargedAmount: " + chargedAmount);
							Log.Instance.LogDebug("WorldPay authAmount: " + authAmount);
							
							if(chargedAmount != authAmount)
							{
								Log.Instance.LogDebug("WorldPay chargedAmount != authAmount");
								orderInfo.Paid = false;
								orderInfo.SetStatus(OrderStatus.PaymentFailed);
								success = false;
								break;
							}

							success = true;
							orderInfo.Paid = true;
							orderInfo.SetStatus(OrderStatus.ReadyForDispatch);
							break;
						case "C":
							success = false;
							orderInfo.Paid = false;
							orderInfo.SetStatus(OrderStatus.PaymentFailed);
							break;
					}

					orderInfo.Save();

					Log.Instance.LogDebug("WorldPay Succes Url: " + successUrl);
					Log.Instance.LogDebug("WorldPay Cancel Url: " + cancelUrl);

					//const string successHtml = "<html><head></head><body><p>Passed</p></body></html>";
					//const string errrorHtml = "<html><head></head><body><p>Failed</p></body></html>";
					var successHtml = string.Format("<html><head><meta http-equiv=\"refresh\" content=\"0;url={0}\" /></head><body></body></html>", successUrl);
					var errrorHtml = string.Format("<html><head><meta http-equiv=\"refresh\" content=\"0;url={0}\" /></head><body></body></html>", cancelUrl);

					if (success)
					{
						HttpContext.Current.Response.Write(successHtml);
						return errrorHtml;
					}

					HttpContext.Current.Response.Write(errrorHtml);
					return errrorHtml;
				}

				Log.Instance.LogDebug("WorldPay OrderInfo == Null or Order Already Paid");

				//HttpContext.Current.Response.Write("<html><head><meta http-equiv=\"refresh\" content=\"0;url=" + cancelUrl +"\" /><body></body></html>");
				return string.Empty;
			}

			Log.Instance.LogDebug("WorldPay cartId == Null");

			//HttpContext.Current.Response.Write("<html><head><meta http-equiv=\"refresh\" content=\"0;url=" + cancelUrl +"\" /><body></body></html>");
			return string.Empty;
		}
	}
}
