﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.WorldPay
{
	public class WorldPayPaymentProvider : IPaymentProvider
    {
		public string GetName()
		{
			return "WorldPay";
		}

		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			var paymentProviderMethodList = new List<PaymentProviderMethod>
				{
					new PaymentProviderMethod
						{
							Id = "WorldPay",
							ProviderName = GetName(),
							Title = "WorldPay",
							Description = "WorldPay"
						}
				};

			return paymentProviderMethodList;
		}
    }
}
