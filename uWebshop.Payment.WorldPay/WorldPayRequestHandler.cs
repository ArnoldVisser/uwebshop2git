﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.NodeFactory;

namespace uWebshop.Payment.WorldPay
{
	public class WorldPayRequestHandler : IPaymentRequestHandler
	{
		public string GetName()
		{
			return "WorldPay";
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);
			
			#region build urls

			var currentNodeId = Node.GetCurrent().Id;
			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

			var successNodeId = 0;
			int.TryParse(paymentProvider.SuccesNodeId, out successNodeId);

			var errorNodeId = 0;
			int.TryParse(paymentProvider.ErrorNodeId, out errorNodeId);
			
			var returnUrl = baseUrl;
			var errorUrl = baseUrl;

			if (successNodeId != 0)
			{
				returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(successNodeId));
			}

			if (errorNodeId != 0)
			{
				errorUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(errorNodeId));
			}

			Log.Instance.LogDebug("WorldPay returnUrl " + returnUrl + ", paymentProviderNodeId: " + paymentProvider.Id);
			Log.Instance.LogDebug("WorldPay errorUrl " + errorUrl + ", paymentProviderNodeId: " + paymentProvider.Id);

			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);

			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));

			Log.Instance.LogDebug("WorldPay reportUrl " + reportUrl + ", paymentProviderNodeId: " + paymentProvider.Id);

			#endregion

			#region config helper

			var accountId = helper.Settings["instId"];
			if (string.IsNullOrEmpty(accountId))
			{
				Log.Instance.LogDebug("WorldPay: Missing PaymentProvider.Config  field with name: instId" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var testResult = helper.Settings["TestResult"];
			if (string.IsNullOrEmpty(testResult))
			{
				//REFUSED
				//Will be equivalent to a refused transaction at the bank.

				//AUTHORISED
				//Will be equivalent to a successful authorisation at the bank, but no funds transferred.

				//ERROR
				//Will be equivalent to a payment that ends in error.

				//CAPTURED
				//Will be equivalent to a successful capture result - where funds are ready to be transferred (settled) to the merchant's account. Please note that this process depends on the capture delay set in the Merchant Interface.
				Log.Instance.LogDebug("WorldPay: Missing PaymentProvider.Config  field with name: TestResult" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			
			#endregion


			//<!-- The first line of code creates a form, it has the POST method and its action is to send the form to us. You do not need to set-up a special connection to us before using it - an Internet connection is all you need to communicate with us.-->

			//<form action="https://secure.worldpay.com/wcc/purchase" method=POST>

			//<!-- A mandatory parameter. Put your Installation ID inside the quotes after value= -->
			//<input type="hidden" name="instId" value="Your installation ID ">

			//<!-- A mandatory parameter. Put your own reference identifier for the item purchased inside the quotes after value= -->
			//<input type="hidden" name="cartId" value="Your reference ID for the product ">

			//<!-- A mandatory parameter. Put the total cost of the item inside the quotes after value= -->
			//<input type="hidden" name="amount" value="The cost of the product ">

			//<!-- A mandatory parameter. Put the code for the purchase currency inside the quotes after value= -->
			//<input type="hidden" name="currency" value="currency code e.g. GBP, USD ">

			//<!-- An optional parameter. Put your description of the item inside the quotes after value= -->
			//<input type="hidden" name="desc" value=" what you are selling ">

			//<!-- An optional parameter. The shopper's email address, this passes it to our server. We can then send an email to the shopper to inform them of the transaction. -->
			//<input type="hidden" name="email" value="Shopper's email address ">

			//<!-- An optional parameter. The shopper's name, this passes it to our server. The shoppper's name parameter can be used to specify the result of a test transaction and you can use the name "AUTHORISED" to specify that the test payment should be authorised.
			//<input type="hidden" name="name" value="AUTHORISED">

			//<!-- This creates the button. When it is selected in the browser the form submits the gathered details to us. -->
			//<input type=submit value="Buy This">

			//<input type="hidden" name="address1" value="4 The Street">
			// <input type="hidden" name="address2" value="My Suburb">
			// <input type="hidden" name="address2" value="">
			// <input type="hidden" name="town" value="my town">
			// <input type="hidden" name="region" value="my region or county">
			// <input type="hidden" name="postcode" value="AB10 5AB">
			// <input type="hidden" name="country" value="GB">
			// <input type="hidden" name="tel" value="0123456789">
			// <input type="hidden" name="email" value="demo@worldpay.com">

			//</form>

			var uniqueId = orderInfo.OrderNumber + "x" + DateTime.Now.ToString("hhmmss");

			var request = new PaymentRequest();
			request.Parameters.Add("instId", accountId);
			request.Parameters.Add("cartId", uniqueId);

			request.Parameters.Add("amount", orderInfo.ChargedAmount.ToString(new CultureInfo("en-GB")));
			request.Parameters.Add("currency", orderInfo.StoreInfo.Store.CurrencyCultureSymbol);

			request.Parameters.Add("desc", orderInfo.OrderNumber);

			request.Parameters.Add("email", orderInfo.CustomerEmail);

			request.Parameters.Add("address1", OrderHelper.CustomerInformationValue(orderInfo, "customerAddress1"));
			request.Parameters.Add("address2", OrderHelper.CustomerInformationValue(orderInfo, "customerAddress2"));
			request.Parameters.Add("address3", OrderHelper.CustomerInformationValue(orderInfo, "customerAddress3"));

			request.Parameters.Add("town", OrderHelper.CustomerInformationValue(orderInfo, "customerCity"));

			if (string.IsNullOrEmpty(orderInfo.CustomerInfo.RegionName))
			{
				request.Parameters.Add("region", OrderHelper.CustomerInformationValue(orderInfo, "customerRegion"));
			}
			else
			{
				request.Parameters.Add("region", orderInfo.CustomerInfo.RegionName);
			}

			request.Parameters.Add("country", orderInfo.CustomerCountry);
			request.Parameters.Add("tel", OrderHelper.CustomerInformationValue(orderInfo, "customerPhone"));

			uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, uniqueId);
			orderInfo.PaymentInfo.TransactionId = uniqueId;

			if (paymentProvider.TestMode)
			{
				request.Parameters.Add("testMode", "100");
				request.Parameters.Add("name", testResult);
			}
			else
			{
				request.Parameters.Add("testMode", "0");
			}

			request.Parameters.Add("MC_successUrl", returnUrl);
			request.Parameters.Add("MC_failureUrl", errorUrl);


			var url = "https://secure.worldpay.com/wcc/purchase";

			if (paymentProvider.TestMode)
			{
				url = "https://secure-test.worldpay.com/wcc/purchase";
			}

			orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;
			orderInfo.PaymentInfo.Url = url;
			orderInfo.PaymentInfo.Parameters = request.ParametersAsString;

			orderInfo.Save();

			return request;
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return string.Empty;
		}
	}
}
