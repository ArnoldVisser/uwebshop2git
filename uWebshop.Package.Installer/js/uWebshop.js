﻿/* UWEBSHOP CLASS */

if (typeof uWebshop === 'undefined') {
	var uWebshop = {};
}

(function () {

	/* UWEBSHOP PUBLIC METHODS */

	/* Gets the current order */
	uWebshop.GetCurrentOrder = function (success, error) {
		var params = ["current"], formData = null;
		return uWebshopService.callBase('GetOrderInfo', params, formData, success, error, true);
	};

	/* Get an order by unique id */
	uWebshop.GetOrder = function (orderGuid, success, error) {
		var params = [orderGuid], formData = null;
		return uWebshopService.callBase('GetOrderInfo', params, formData, success, error, true);
	}; uWebshop.GetOrderInfo = uWebshop.GetOrder;

	/* Add an item to the current order */
	uWebshop.AddOrUpdateOrderLine = function (productId, action, quantity, variants, properties, success, error) {
		var params = [productId, action, quantity, variants, properties], formData = null;
		return uWebshopService.callBase('AddOrUpdateOrderLine', params, formData, success, error, true);
	};

	/* Update an orderline */
	uWebshop.AddProduct = function (productId, action, quantity, variants, properties, success, error) {
		var params = [productId, action, quantity, variants, properties], formData = null;
		return uWebshopService.callBase('AddProduct', params, formData, success, error, true);
	};

	/* Update an orderline */
	uWebshop.UpdateOrderLine = function (orderlineId, action, quantity, variants, properties, success, error) {
		var params = [orderlineId, action, quantity, variants, properties], formData = null;
		return uWebshopService.callBase('UpdateOrderLine', params, formData, success, error, true);
	};

	/* Set a payment provider to the order */
	uWebshop.AddPaymentProvider = function (paymentProviderId, paymentMethodId, success, error) {
		var params = [paymentProviderId, paymentMethodId], formData = null;
		return uWebshopService.callBase('AddPaymentProvider', params, formData, success, error, true);
	};

	/* Add a discount coupon to the order */
	uWebshop.AddCoupon = function (couponCode, success, error) {
		var params = [couponCode], formData = null;
		return uWebshopService.callBase('AddCoupon', params, formData, success, error, true);
	};

	/* Remove a discount coupon from the order */
	uWebshop.RemoveCoupon = function (couponCode, success, error) {
		var params = [couponCode], formData = null;
		return uWebshopService.callBase('RemoveCoupon', params, formData, success, error, true);
	};

	/* Set a shipping provider to the order */
	uWebshop.AddShippingProvider = function (shippingProviderId, shippingMethodId, success, error) {
		var params = [shippingProviderId, shippingMethodId], formData = null;
		return uWebshopService.callBase('AddShippingProvider', params, formData, success, error, true);
	};

	/* add customer information to the order */
	uWebshop.AddCustomerFields = function (customerFields, success, error) {
		var params = [customerFields], formData = null;
		return uWebshopService.callBase('AddCustomerFields', params, formData, success, error, true);
	};

	/* add customer shipping information to the order */
	uWebshop.AddShippingFields = function (shippingFields, success, error) {
		var params = [shippingFields], formData = null;
		return uWebshopService.callBase('AddShippingFields', params, formData, success, error, true);
	};

	/* add customer extra information to the order */
	uWebshop.AddExtraFields = function (extraFields, success, error) {
		var params = [extraFields], formData = null;
		return uWebshopService.callBase('AddExtraFields', params, formData, success, error, true);
	};

	/* Validate the current order */
	uWebshop.ValidateCurrentOrder = function (success, error) {
		var params = ["current"], formData = null;
		return uWebshopService.callBase('ValidateOrder', params, formData, success, error, true);
	};

	/* Validate order with guid */
	uWebshop.ValidateOrder = function (orderGuid, success, error) {
		var params = [orderGuid], formData = null;
		return uWebshopService.callBase('ValidateOrder', params, formData, success, error, true);
	};

	/* Validate the customer/shipping/extra information in the order */
	uWebshop.ValidateCustomerForCurrentOrder = function (success, error) {
		var params = ["current"], formData = null;
		return uWebshopService.callBase('ValidateCustomer', params, formData, success, error, true);
	};

	/* Validate the customer/shipping/extra information in the order */
	uWebshop.ValidateCustomerForOrderWithGuid = function (orderGuid, success, error) {
		var params = [orderGuid], formData = null;
		return uWebshopService.callBase('ValidateCustomer', params, formData, success, error, true);
	}; uWebshop.ValidateCustomer = uWebshop.ValidateCustomerForOrderWithGuid;

	/* Validate the stock of the current order */
	uWebshop.ValidateStockForCurrentOrder = function (success, error) {
		var params = ["current"], formData = null;
		return uWebshopService.callBase('ValidateStock', params, formData, success, error, true);
	};

	/* Validate the stock of order with guid */
	uWebshop.ValidateStockForOrderWithGuid = function (orderGuid, success, error) {
		var params = [orderGuid], formData = null;
		return uWebshopService.callBase('ValidateStock', params, formData, success, error, true);
	}; uWebshop.ValidateStock = uWebshop.ValidateStockForOrderWithGuid;

	/* Validate the orderlines of the current order */
	uWebshop.ValidateOrderLinesForCurrentOrder = function (success, error) {
		var params = ["current"], formData = null;
		return uWebshopService.callBase('ValidateOrderLines', params, formData, success, error, true);
	};

	/* Validate the orderlines of order with guid */
	uWebshop.ValidateOrderLinesForOrderWithGuid = function (orderGuid, success, error) {
		var params = [orderGuid], formData = null;
		return uWebshopService.callBase('ValidateOrderLines', params, formData, success, error, true);
	}; uWebshop.ValidateOrderLines = uWebshop.ValidateOrderLinesForOrderWithGuid;

	/* Validate the custom created validation of the current order */
	uWebshop.ValidateCustomValidationForCurrentOrder = function (success, error) {
		var params = ["current"], formData = null;
		return uWebshopService.callBase('ValidateCustomValidation', params, formData, success, error, true);
	};

	/* Validate the custom created validation of order with guid */
	uWebshop.ValidateCustomValidationForOrderWithGuid = function (orderGuid, success, error) {
		var params = [orderGuid], formData = null;
		return uWebshopService.callBase('ValidateCustomValidation', params, formData, success, error, true);
	}; uWebshop.ValidateCustomValidation = uWebshop.ValidateCustomValidationForOrderWithGuid;

	/* confirm the current order */
	uWebshop.ConfirmOrder = function (success, error) {
		var params = [0], formData = null;
		return uWebshopService.callBase('ConfirmOrder', params, formData, success, error, true);
	};

	/* confirm the current order and redirect to node with id */
	uWebshop.ConfirmOrderAndRedirectToNodeWithId = function (confirmNodeId, success, error) {
		var params = [confirmNodeId], formData = null;
		return uWebshopService.callBase('ConfirmOrder', params, formData, success, error, true);
	};

	/* Get a category */
	uWebshop.Category = function (categoryId, success, error) {
		var params = [categoryId, 0], formData = null;
		return uWebshopService.callBase('Category', params, formData, success, error, true);
	};

	/* Get a category from a store */
	uWebshop.CategoryFromStore = function (categoryId, storeId, success, error) {
		var params = [categoryId, storeId], formData = null;
		return uWebshopService.callBase('Category', params, formData, success, error, true);
	};

	/* Get a product */
	uWebshop.Product = function (productId, success, error) {
		var params = [productId, 0], formData = null;
		return uWebshopService.callBase('Product', params, formData, success, error, true);
	};

	/* Get a product from a store */
	uWebshop.ProductFromStore = function (productId, storeId, success, error) {
		var params = [productId, storeId], formData = null;
		return uWebshopService.callBase('Product', params, formData, success, error, true);
	};

	/* Get a product variant */
	uWebshop.ProductVariant = function (productVariantId, success, error) {
		var params = [productVariantId, 0], formData = null;
		return uWebshopService.callBase('ProductVariant', params, formData, success, error, true);
	};

	/* Get a product variant from a store */
	uWebshop.ProductVariantFromStore = function (productVariantId, storeId, success, error) {
		var params = [productVariantId, storeId], formData = null;
		return uWebshopService.callBase('ProductVariant', params, formData, success, error, true);
	};

	/* Get the payment providers available for the current order */
	uWebshop.GetPaymentProviders = function (success, error) {
		var params = null, formData = null;
		return uWebshopService.callBase('GetPaymentProviders', params, formData, success, error, false);
	};

	/* Get the shipping providers available for the current order */
	uWebshop.GetShippingProviders = function (success, error) {
		var params = null, formData = null;
		return uWebshopService.callBase('GetShippingProviders', params, formData, success, error, false);
	};


	/* UWEBSHOP PRIVATE SERVICE OBJECT */

	var uWebshopService = function () {

		uWebshopService.callBase = function (methodName, args, data, success, error, useArgs) {
			$.ajax({
				type: "POST",
				url: uWebshopService.createUrl(methodName, args, useArgs),
				data: JSON.stringify(args),
				async: true,
				processData: false,
				success: function (data, status, xmlHttpRequest) {
					if (success) {
						success(data);
					}
				},
				error: function (xmlHttpRequest, status, errorThrown) {
					if (error) {
						error(xmlHttpRequest, status, errorThrown);
					}
				},
				cache: false,
				contentType: "application/json; charset=utf-8",
				dataType: "json"
			});
		};

		uWebshopService.createUrl = function (method, args, useArgs) {
			var baseUrl = '/Base/uWebshop/' + method;
			var argList = "";
			if (args) {
				for (var i = 0; i < args.length; i++) {
					argList += '/' + args[i];
				}
			}

			if (useArgs) {
				baseUrl += argList;
			}

			return baseUrl + ".aspx";
		};
	};
	var uwbsService = new uWebshopService();

})();