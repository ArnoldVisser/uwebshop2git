﻿// UWEBSHOP EXAMPLE SCRIPTS

$(function() {

	$('#customerSubmit').click(function(event) {
		// stop href from firing
		event.preventDefault();
		// get all the fields where the id starts with "customer"
		var $customerInputs = $('input[type=text][id^="customer"]');
		// build an object array out of them
		var obj = {};
		$.each($customerInputs, function() {
			obj[this.id] = $(this).val();
		});
		// create JSON from the object
		var output = JSON.stringify(obj);
		// add them to the order
		uWebshop.AddCustomerFields(output);
	});

	$('#paymentsubmit').click(function(event) {
		// stop href from firing
		event.preventDefault();
		// select the chosen provider
		var selectedProvider = $("#paymentProvider option:selected").val();
		// split the value to have Provider & the method values separated
		var selectedProviderArray = selectedProvider.split('-');
		// add to order
		uWebshop.AddPaymentProvider(selectedProviderArray[0], selectedProviderArray[1]);
	});

	$('#shippingsubmit').click(function(event) {
		// stop href from firing
		event.preventDefault();
		// select the chosen provider
		var selectedProvider = $("#shippingProvider option:selected").val();
		// split the value to have Provider & the method values separated
		var selectedProviderArray = selectedProvider.split('-');
		// add to order
		uWebshop.AddShippingProvider(selectedProviderArray[0], selectedProviderArray[1], function onSuccess() {
			uWebshop.GetCurrentOrder(function(data) {
				alert(data.orderinfo.ShippingProviderCostsWithVat);
			});
		});
	});

	$('#confirmOrder').click(function(event) {
		// stop href from firing
		event.preventDefault();
		// confirm order
		uWebshop.ConfirmOrder();
	});
	
});