﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="uWebshopInstaller.ascx.cs" Inherits="uWebshop.Package.Installer.uWebshopInstaller" %>
<%@ Register TagPrefix="umb" Namespace="umbraco.uicontrols" Assembly="controls" %>


<link href="/umbraco_client/propertypane/style.css" rel="stylesheet" />

<style>
	hr.line {
		border: none;
		border-bottom: 1px solid #ccc;
	}

	input.button {
		padding: 5px 10px;
		font-size: 12px;
		margin-left: 10px;
	}
</style>

<div class="dashboardWrapper">
	<h2>Thank you for trying out uWebshop 2!</h2>
	<img src="/App_Plugins/uWebshop/images/uWebshop32x32.png" alt="uWebshop" class="dashboardIcon" />
	<p>There are <a href="#start">some steps</a> left to finish before this installation is complete.</p>
	<p>If you need any help please visit <a href="http://support.uwebshop.com" title="uWebshop Support">our support site</a></p>
</div>

</div></div>

<umb:pane ID="panel2" runat="server" visible="true">
	<div class="dashboardWrapper">
		<h2>Create a store</h2>
		<img src="/App_Plugins/uWebshop/images/store32x32.png" alt="Create an uWebshop Store!" class="dashboardIcon" />
		<p>This will create a store in uWebshop. You need at least one store, but you can create and remove as many stores as you like.</p>
		<p>The installed Store does need some extra values before it can be used. You can enter these details at the Store node itself.</p>  
		<asp:Label ID="Label1" runat="server" AssociatedControlID="txtStoreAlias" Text="Name of the store:"></asp:Label>
		<asp:TextBox runat="server" ID="txtStoreAlias"/>
		<asp:Button CssClass="button" runat="server" ID="btnInstallStore" OnClick="BtnInstallStoreClick" Text="Create Store"/>
	</div>
</umb:pane>

<umb:pane ID="panel3" runat="server" visible="true">
	<div class="dashboardWrapper">
		<h2>Add store picker to your current website</h2>
		<img src="/App_Plugins/uWebshop/images/storepicker32x32.png" alt="Create an uWebshop Store!" class="dashboardIcon" />
		<p>Adding a store picker to your website will allow your site to work with uWebshop.<br/>If you don't have a website yet <strong><a href="/install/?installStep=skinning" target="_blank">Install a Starter Kit</a></strong> or create a new website.</p>
				
		<p>Add the "Store Picker" datatype to the node where the store specific pages will be located under (Usually this will the Homepage).</p>
		<p>This control can also be found at the developer section of your Umbraco installation on the uWebshop tab.</p>  
		<asp:Label runat="server" ID="lblStorePicker" Text="Pick the node:" /><br/>
		<asp:PlaceHolder runat="server" ID="phInstallStorePicker"/><asp:Button runat="server" ID="btnInstallStorePicker" CssClass="button" Text="Install Store Picker" OnClick="BtnStorePickerCLick"/>
	</div>
</umb:pane>

<umb:pane ID="panel5" runat="server" visible="true">
	<div class="dashboardWrapper">
		<h2>What's next?</h2>
		<img src="/App_Plugins/uWebshop/images/documentation32x32.png" alt="Work to do!" class="dashboardIcon" />
		<div class="dashboardColWrapper">
			<div class="dashboardCols">
				<ul>
					<li><a href="http://uwebshop.com/download" target="_blank">Download and Install our uWebshop Sandbox Starterkit package</a> to get a great set of example code without having to worry about layout!</li>
					<li>You can find the post installation/getting started guide on our support platform. <a target="_blank" href="http://documentation.uWebshop.com">Read the post-installation documentation</a></li>              
					<li>Create templates to display categories and products and connect them to the uwbsCategory &amp; uwbsProduct document types. <a target="_blank" href="http://support.uWebshop.com">Read about uWebshop templating</a></li>
					<li>The installer added a new macro <em>"BasketHandler"</em> to handle all the order updates. This macro should be placed on the template or the node you want to use for basket update (we advise to use the Master Document Template)  <a target="_blank" href="http://support.uWebshop.com">read about the uWebshop basket handler</a></li>
				</ul>
			</div>
		</div>
	</div>
</umb:pane>