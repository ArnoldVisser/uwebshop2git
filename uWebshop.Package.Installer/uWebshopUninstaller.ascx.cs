﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;
using System.Xml;
using uWebshop.ActionHandlers;
using uWebshop.Domain.Helpers;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.datatype;
using umbraco.cms.businesslogic.member;
using umbraco.cms.businesslogic.propertytype;
using umbraco.cms.businesslogic.web;
using umbraco.cms.presentation.Trees;
using umbraco.cms.businesslogic.language;

namespace uWebshop.Package.Installer
{
	public partial class uWebshopUninstaller : System.Web.UI.UserControl
	{
		private List<umbraco.cms.businesslogic.language.Language> m_langs = umbraco.cms.businesslogic.language.Language.GetAllAsList().ToList();

		protected void Page_Load(object sender, EventArgs e)
		{
			// SET THE UI LANGUAGE BACK TO THE DEFAULT EN-GB
			XmlDocument EnLangDoc = new XmlDocument();
			EnLangDoc.Load(Server.MapPath("~/umbraco/config/lang/en.xml"));

			XmlElement XMLlanguageElement = EnLangDoc.DocumentElement;

			XMLlanguageElement.SetAttribute("intName", "English (gb)");
			XMLlanguageElement.SetAttribute("culture", "en-GB");

			EnLangDoc.Save(Server.MapPath("~/umbraco/config/lang/en.xml"));


			// REVERT CHANGES TO WEB.CONFIG THAT DON'T BREAK
			XmlDocument WebConfig = new XmlDocument();
			WebConfig.Load(Server.MapPath("~/web.config"));
		}
	}
}