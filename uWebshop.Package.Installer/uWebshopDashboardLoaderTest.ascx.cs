﻿using System;
using umbraco.BasePages;

namespace uWebshop.Package.Installer
{
	public partial class uWebshopDashboardLoaderTest : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			BasePage.Current.LoadControl("/usercontrols/uWebshop/uWebshopDashBoard.ascx");
			BasePage.Current.LoadControl("/umbraco/dashboard/developerdashboardintro.ascx");
		}
	}
}