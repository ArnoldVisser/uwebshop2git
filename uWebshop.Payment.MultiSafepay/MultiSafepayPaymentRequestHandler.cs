﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.NodeFactory;
using System.Threading;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.MultiSafePay
{
	public class MultiSafePayPaymentRequestHandler : MultiSafePayPaymentBase, IPaymentRequestHandler
	{
		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			#region build urls

			var currentNodeId = Node.GetCurrent().Id;
			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

			var returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.SuccesNodeId)));
			var cancelUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.ErrorNodeId)));
			
			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);
			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));

			#endregion

			#region config helper

			var accountId = helper.Settings["accountId"];
			if (string.IsNullOrEmpty(accountId))
			{
				Log.Instance.LogError("MultiSafePay: Missing PaymentProvider.Config field with name: accountId" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			var siteId = helper.Settings["siteId"];
			if (string.IsNullOrEmpty(siteId))
			{
				Log.Instance.LogError("MultiSafePay: Missing PaymentProvider.Config  field with name: siteId" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			var siteSecureId = helper.Settings["siteSecureId"];
			if (string.IsNullOrEmpty(siteSecureId))
			{
				Log.Instance.LogError("MultiSafePay: Missing PaymentProvider.Config  field with name: siteSecureId" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var testURL = helper.Settings["testURL"];
			if (string.IsNullOrEmpty(testURL))
			{
				Log.Instance.LogError("MultiSafePay: Missing PaymentProvider.Config  field with name: testURL" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var liveUrl = helper.Settings["url"];
			if (string.IsNullOrEmpty(liveUrl))
			{
				Log.Instance.LogError("MultiSafePay: Missing PaymentProvider.Config  field with name: url" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			#endregion


			#region fill transactionrequest object
			
			var transactionRequest = new TransactionRequest
				{
					AccountId = long.Parse(accountId),
					SiteId = long.Parse(siteId),
					SiteSecureId = long.Parse(siteSecureId),
					NotificationUrl = reportUrl,
					CancelUrl = cancelUrl,
					RedirectUrl = returnUrl,
					Locale =
						orderInfo.StoreInfo.CultureInfo.TwoLetterISOLanguageName + "_" +
						orderInfo.CustomerInfo.CountryCode,
					IPAddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"],
					ForwardedIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"],
					FirstName = OrderHelper.CustomerInformationValue(orderInfo, "customerFirstName"),
					LastName = OrderHelper.CustomerInformationValue(orderInfo, "customerLastName"),
					Country = OrderHelper.CustomerInformationValue(orderInfo, "customerCountry"),
					Email = OrderHelper.CustomerInformationValue(orderInfo, "customerEmail")
				};

			var amount = orderInfo.ChargedAmountInCents;

			transactionRequest.TransactionId = orderInfo.UniqueOrderId.ToString();
			var ri = new RegionInfo(orderInfo.StoreInfo.Store.CurrencyCultureInfo.LCID);

			transactionRequest.Currency = ri.ISOCurrencySymbol;
			transactionRequest.Amount = amount;
			transactionRequest.Decription = orderInfo.OrderNumber;
			transactionRequest.Gateway = orderInfo.PaymentInfo.MethodId;

			var stringToHash = amount + transactionRequest.Currency + transactionRequest.AccountId + transactionRequest.SiteId + transactionRequest.TransactionId;

			transactionRequest.Signature = GetMd5Sum(stringToHash);

			#endregion

			var apiURL = paymentProvider.TestMode ? testURL : liveUrl;

			var httpWebRequest = (HttpWebRequest) WebRequest.Create(apiURL);
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentLength = Encoding.UTF8.GetByteCount(transactionRequest.GetXml());
			httpWebRequest.ContentType = "application/x-www-form-urlencoded";

			Log.Instance.LogError("MultiSafePay GetAllPaymentMethods transactionRequest GetXml: " + HttpUtility.HtmlEncode(transactionRequest.GetXml()));

			var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());
			streamWriter.Write(transactionRequest.GetXml());
			streamWriter.Close();

			var httpWebResponse = (HttpWebResponse) httpWebRequest.GetResponse();
			var streamReader = new StreamReader(httpWebResponse.GetResponseStream());

			//<?xml version="1.0" encoding="UTF-8" ?>
			//<redirecttransaction result="ok">
			//  <transaction>
			//      <id>4084044</id>
			//      <payment_url>https://user.multisafepay.com/pay/?trans...</payment_url>
			//  </transaction>
			//</redirecttransaction>
			var xmlDoc = XDocument.Parse(streamReader.ReadToEnd());

			Log.Instance.LogDebug("MultiSafePay xmlDoc: " + xmlDoc);

			XElement transaction = null;

			if (xmlDoc.Root != null)
			{
				transaction = xmlDoc.Root.Element("transaction");
			}
			else
			{
				Log.Instance.LogDebug("MultiSafePay xmlDoc.Root == null");
			}
			if (transaction != null)
			{
				var paymentUrl = transaction.Element("payment_url");

				if (paymentUrl != null)
				{
					orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.ServerPost;
					orderInfo.PaymentInfo.Url = paymentUrl.Value;
					orderInfo.PaymentInfo.TransactionId = orderInfo.UniqueOrderId.ToString();
				}
				else
				{
					Log.Instance.LogDebug("MultiSafePay paymentUrl == null");
				}
			}
			else
			{
				Log.Instance.LogDebug("MultiSafePay transaction == null");
			}
			
			return null;
		}

		// Create an md5 sum string of this string
		public static string GetMd5Sum(string str)
		{
			var x = new MD5CryptoServiceProvider();

			var data = Encoding.ASCII.GetBytes(str);

			data = x.ComputeHash(data);

			return BitConverter.ToString(data).Replace("-", "").ToLower();
		}


		/// <summary>
		/// Returns the URL to redirect to
		/// </summary>
		/// <param name="order"></param>
		/// <returns></returns>
		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return orderInfo.PaymentInfo.Url;
		}
	}
}