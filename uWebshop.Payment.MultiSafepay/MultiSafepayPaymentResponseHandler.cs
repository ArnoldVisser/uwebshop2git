﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.BusinessLogic;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.MultiSafePay
{
	public class MultiSafePayPaymentResponseHandler : MultiSafePayPaymentBase, IPaymentResponseHandler
	{
		#region IPaymentResponseHandler Members

		public string HandlePaymentResponse()
		{
			var transactionId = library.Request("transactionid");

			if (!string.IsNullOrEmpty(transactionId))
			{
				var orderInfo = OrderHelper.GetOrderInfo(Guid.Parse(transactionId));

				if (orderInfo.Paid == false)
				{

					var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
					var helper = new PaymentConfigHelper(paymentProvider);

					var statusRequest = new StatusRequest
						                    {
							                    AccountId = long.Parse(helper.Settings["accountId"]),
							                    TransactionId = transactionId,
							                    SiteId = long.Parse(helper.Settings["siteId"]),
							                    SiteSecureId = long.Parse(helper.Settings["siteSecureId"])
						                    };

					var apiURL = paymentProvider.TestMode ? helper.Settings["testURL"] : helper.Settings["url"];

					try
					{
						var httpWebRequest = (HttpWebRequest) HttpWebRequest.Create(apiURL);
						httpWebRequest.Method = "POST";
						httpWebRequest.ContentLength = Encoding.UTF8.GetByteCount(statusRequest.GetXml());
						httpWebRequest.ContentType = "application/x-www-form-urlencoded";

						var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());
						streamWriter.Write(statusRequest.GetXml());
						streamWriter.Close();

						var httpWebResponse = (HttpWebResponse) httpWebRequest.GetResponse();
						var streamReader = new StreamReader(httpWebResponse.GetResponseStream());
						var xmlDoc = XDocument.Parse(streamReader.ReadToEnd());

						Log.Instance.LogError("MultiSafePay CreatePaymentRequest MultiSafePay XML Answer: " +
						                      HttpUtility.HtmlEncode(xmlDoc.ToString(SaveOptions.None)));

						var ewallet = xmlDoc.Root.Element("ewallet");
						var status = ewallet.Element("status").Value;

						//if (order.OrderInfo.Status == OrderStatus.Ready_for_Dispatch && status == "completed")
						//{
						//    return null;
						//}

						orderInfo.Status = OrderStatus.WaitingForPayment;

						//– completed: succesvol voltooid
						//– initialized: aangemaakt, maar nog niet voltooid
						//– uncleared: aangemaakt, maar nog niet vrijgesteld (credit cards)
						//– void: geannuleerd
						//– declined: afgewezen
						//– refunded: terugbetaald
						//– expired: verlopen

						if (status == "completed")
						{
							orderInfo.Paid = true;
							orderInfo.Status = OrderStatus.ReadyForDispatch;
						}
						if (status == "uncleared")
						{
							orderInfo.Status = OrderStatus.WaitingForPaymentProvider;
						}
						if (status == "declined")
						{
							orderInfo.Paid = false;
							orderInfo.Status = OrderStatus.PaymentFailed;
							orderInfo.PaymentInfo.ErrorMessage = status;
						}
						if (status == "expired")
						{
							orderInfo.Paid = false;
							orderInfo.Status = OrderStatus.PaymentFailed;
							orderInfo.PaymentInfo.ErrorMessage = status;
						}
						if (status == "void")
						{
							orderInfo.Paid = false;
							orderInfo.Status = OrderStatus.PaymentFailed;
							orderInfo.PaymentInfo.ErrorMessage = status;
						}

						orderInfo.Save();
					}
					catch (Exception ex)
					{
						Log.Instance.LogError("MultiSafePayPaymentResponseHandler.HandlePaymentResponse: " + ex);
					}
				}
			}

			return null;
		}

		#endregion
	}
}