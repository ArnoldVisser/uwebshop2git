﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using umbraco.BusinessLogic;
using uWebshop.Domain.Helpers;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.MultiSafePay
{
	public class MultiSafePayPaymentProvider : MultiSafePayPaymentBase, IPaymentProvider
	{
		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.ServerPost;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			var currentOrder = OrderHelper.GetOrderInfo();
			var paymentProvider = PaymentProvider.GetPaymentProvider(id);
			if (paymentProvider == null) throw new Exception("PaymentProvider with id " + id + " not found.");

			var helper = new PaymentConfigHelper(paymentProvider);

			var customerCountry = currentOrder.StoreInfo.CountryCode;
			if (currentOrder.CustomerInfo.CustomerInformation != null && currentOrder.CustomerInfo.CustomerInformation.Element("customerCountry") != null)
			{
				var customerCountryField = currentOrder.CustomerInfo.CustomerInformation.Element("customerCountry");
				if (customerCountryField != null)
				{
					customerCountry = customerCountryField.Value;
				}
			}

			var gatewaysRequest = new GatewayRequest
				{
					AccountId = long.Parse(helper.Settings["accountId"]),
					Country = customerCountry,
					SiteId = int.Parse(helper.Settings["siteId"]),
					SiteSecureId = int.Parse(helper.Settings["siteSecureId"])
				};

			var apiURL = paymentProvider.TestMode ? helper.Settings["testURL"] : helper.Settings["url"];

			var httpWebRequest = (HttpWebRequest) WebRequest.Create(apiURL);
			httpWebRequest.Method = "POST";
			httpWebRequest.ContentLength = Encoding.UTF8.GetByteCount(gatewaysRequest.GetXml());
			httpWebRequest.ContentType = "application/x-www-form-urlencoded";

			var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());
			streamWriter.Write(gatewaysRequest.GetXml());
			streamWriter.Close();

			var httpWebResponse = (HttpWebResponse) httpWebRequest.GetResponse();
			//Log.Instance.LogError("MultiSafePay httpWebResponse: " + httpWebResponse);
			var streamReader = new StreamReader(httpWebResponse.GetResponseStream());
			var xmlDoc = XDocument.Parse(streamReader.ReadToEnd());

			Log.Instance.LogError("MultiSafePay GetAllPaymentMethods MultiSafePay XML Answer: " + HttpUtility.HtmlEncode(xmlDoc.ToString(SaveOptions.None)));

			var gateways = xmlDoc.Descendants("gateway");

			//<?xml version="1.0" encoding="UTF-8"?>
			//<gateways result="ok">
			//    <gateways>
			//      <gateway>
			//        <id>IDEAL</id>
			//        <description>iDeal</description>
			//      </gateway>
			//      <gateway>
			//        <id> MASTERCARD</id>
			//        <description>Visa via Multipay</description>
			//      </gateway>
			//      <gateway>
			//        <id> BANKTRANS</id>
			//       <description> Bank Transfer</description>
			//      </gateway>
			//      <gateway>
			//        <id> VISA</id>
			//        <description> Visa CreditCardsdescription>
			//      </gateway>
			//    </gateways>
			//</gateways>
			//int paymentImageId;
			//int.TryParse(umbraco.library.GetDictionaryItem(gateway.Element("id").Value + "LogoId"), out paymentImageId);
			return gateways.Select(gateway => new PaymentProviderMethod
			{
				Id = gateway.Element("id").Value, Description = gateway.Element("description").Value, Title = gateway.Element("description").Value, 
				Name = gateway.Element("description").Value, ProviderName = GetName()
			}).ToList();
		}
	}
}