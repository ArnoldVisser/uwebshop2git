﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Examine;
using umbraco.BusinessLogic;

namespace uWebshop.Examine
{
	public class ExamineEvents : ApplicationBase
	{
		public ExamineEvents()
		{
			var indexer = ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"];
			indexer.GatheringNodeData += GatheringNodeDataHandler;
		}

		protected void GatheringNodeDataHandler(object sender, IndexingNodeDataEventArgs e)
		{
			try
			{
				foreach (var xElement in e.Node.Elements().Where(element => element.Name.LocalName.StartsWith("description")))
				{
					if (xElement != null) e.Fields.Add("RTEItem" + xElement.Name.LocalName, xElement.Value);
				}
			}
			catch
			{
			}

			foreach (var field in defaultPriceValues())
			{
				try
				{
					//grab the current data from the Fields collection
					string value;
					if (e.Fields == null || !e.Fields.TryGetValue(field, out value))
						continue;
					var currencyFieldValue = e.Fields[field];

					var currencyValueAsInt = int.Parse(currencyFieldValue);

					//prefix with leading zero's
					currencyFieldValue = currencyValueAsInt.ToString("D8");

					//now put it back into the Fields so we can pretend nothing happened! ;)
					e.Fields[field] = currencyFieldValue;
				}
				catch (Exception ex)
				{
					Domain.Log.Instance.LogError("GatheringNodeDataHandler defaultPriceValues Examine: " + ex);
				}
			}

			foreach (var field in DefaultCsvValues())
			{
				try
				{
					string value;
					if (e.Fields == null || !e.Fields.TryGetValue(field, out value))
						continue;
					var csvFieldValue = e.Fields[field];
					//Log.Instance.LogDebug( "examine MNTP before: " + mntp);
					//let's get rid of those commas!
					csvFieldValue = csvFieldValue.Replace(",", " ");
					//Log.Instance.LogDebug( "examine MNTP after: " + mntp);
					//now put it back into the Fields so we can pretend nothing happened!
					e.Fields[field] = csvFieldValue;
				}
				catch (Exception ex)
				{
					Domain.Log.Instance.LogError("GatheringNodeDataHandler DefaultCsvValues Examine: " + ex);
				}
			}
		}

		public List<string> defaultPriceValues()
		{
			return new List<string>
				{
					"price"
				};
		}

		public List<string> DefaultCsvValues()
		{
			return new List<string>
				{
					"categories",
					//"metaTags", metatags can't be stripped of comma
					"images",
					"files"
				};
		}
	}
}