﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using umbraco.NodeFactory;
using umbraco;
using System.Diagnostics;
using Examine;
using Examine.SearchCriteria;
using Examine.LuceneEngine;
using Examine.LuceneEngine.Providers;
using Examine.LuceneEngine.SearchCriteria;

namespace uWebshop.Examine
{
	public partial class ExamineSearch : System.Web.UI.UserControl
	{
		public int searchNodeID { get; set; }
		public string noValueText { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(txtSearch.Text))
			{
				Node searchNode = new Node(searchNodeID);
				Response.Redirect(library.NiceUrl(searchNode.Id) + "?s=" + txtSearch.Text);
			}
			else
			{
				txtSearch.Text = noValueText;
			}
		}


		protected IBooleanOperation Range(string fieldName, int start, int end, bool includeLower, bool includeUpper)
		{
			var sc = ExamineManager.Instance.CreateSearchCriteria();

			var query = sc.NodeTypeAlias("uwbsPricing");

			query = query.And().Range(fieldName, start.ToString("D8"), end.ToString("D8"), includeLower, includeUpper);

			return query;
		}
	}
}