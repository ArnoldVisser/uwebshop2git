﻿using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Test.Services.OrderUpdatingsServiceTests;
using uWebshop.Umbraco.Repositories;

namespace uWebshop.Umbraco6.Test.Umbraco
{
	[TestFixture]
	public class NodeAliasIsExtensionOfGivenNodeAliasTest
	{
		[TestCase(Product.NodeAlias + "Customized", Product.NodeAlias, true)]
		[TestCase(Catalog.ProductRepositoryNodeAlias, Product.NodeAlias, false)]
		[TestCase(ProductVariant.NodeAlias, Product.NodeAlias, false)]
		[TestCase(ProductVariant.NodeAlias + "Customized", Product.NodeAlias, false)]
		[TestCase(Product.NodeAlias, "notproduct", true)]
		[TestCase(Category.NodeAlias, Catalog.ProductRepositoryNodeAlias, true)]
		[TestCase(Catalog.ProductRepositoryNodeAlias, Catalog.ProductRepositoryNodeAlias, true)]
		[TestCase(ProductVariant.NodeAlias, ProductVariant.NodeAlias, true)]
		[TestCase("anything", Catalog.CategoryRepositoryNodeAlias, true)]
		[TestCase(Category.NodeAlias + "Customized", Category.NodeAlias, true)]
		[TestCase(Catalog.CategoryRepositoryNodeAlias, Category.NodeAlias, false)]
		[TestCase(Category.NodeAlias, "something", true)]
		[TestCase(Category.NodeAlias, Catalog.CategoryRepositoryNodeAlias, true)]
		[TestCase(Catalog.CategoryRepositoryNodeAlias, Catalog.CategoryRepositoryNodeAlias, true)]
		[TestCase(PaymentProvider.NodeAlias + "Customized", PaymentProvider.NodeAlias, true)]
		[TestCase(PaymentProvider.PaymentProviderZoneSectionNodeAlias, PaymentProvider.NodeAlias, false)]
		[TestCase(PaymentProvider.NodeAlias, "something", true)]
		[TestCase(PaymentProvider.PaymentProviderZoneSectionNodeAlias, PaymentProvider.PaymentProviderZoneSectionNodeAlias, true)]
		[TestCase(ShippingProvider.NodeAlias + "Customized", ShippingProvider.NodeAlias, true)]
		[TestCase(ShippingProvider.ShippingProviderZoneSectionNodeAlias, ShippingProvider.NodeAlias, false)]
		[TestCase(ShippingProvider.NodeAlias, "something", true)]
		[TestCase(ShippingProvider.ShippingProviderZoneSectionNodeAlias, ShippingProvider.ShippingProviderZoneSectionNodeAlias, true)]
		[TestCase(Store.NodeAlias + "Extended", Store.NodeAlias, true)]
		[TestCase(Store.StoreRepositoryNodeAlias, Store.NodeAlias, false)]
		[TestCase(Store.StoreRepositoryNodeAlias, Store.StoreRepositoryNodeAlias, true)]
		[TestCase("anything", Store.StoreRepositoryNodeAlias, true)]
		[TestCase("anything", Catalog.CategoryRepositoryNodeAlias, true)]
		public void Total(string foundNodetypeAlias, string requestedNodetypeAlias, bool shouldMatch)
		{
			// optional alternative to this test is DIP and test that all the four checkmethods are called (disadvantage: yet another class to inject through the AppDependencies)
			var actual = UmbracoStaticCachedEntityRepository.CheckNodeTypeAliasForImproperOverlap(foundNodetypeAlias.ToLowerInvariant(), requestedNodetypeAlias.ToLowerInvariant());

			Assert.AreEqual(shouldMatch, actual);
		}

		[TestCase(Store.NodeAlias + "Extended", Store.NodeAlias, true)]
		[TestCase(Store.StoreRepositoryNodeAlias, Store.NodeAlias, false)]
		[TestCase(Store.StoreRepositoryNodeAlias, Store.StoreRepositoryNodeAlias, true)]
		[TestCase("anything", Store.StoreRepositoryNodeAlias, true)]
		[TestCase("anything", Catalog.CategoryRepositoryNodeAlias, true)]
		[TestCase(Category.NodeAlias + "Customized", Category.NodeAlias, true)]
		[TestCase(Catalog.CategoryRepositoryNodeAlias, Category.NodeAlias, false)]
		[TestCase(Category.NodeAlias, "something", true)]
		[TestCase(Category.NodeAlias, Catalog.CategoryRepositoryNodeAlias, true)]
		[TestCase(Catalog.CategoryRepositoryNodeAlias, Catalog.CategoryRepositoryNodeAlias, true)]
		[TestCase(PaymentProvider.PaymentProviderRepositoryNodeAlias, PaymentProvider.PaymentProviderRepositoryNodeAlias, true)]
		[TestCase(ShippingProvider.ShippingProviderRepositoryNodeAlias, ShippingProvider.NodeAlias, false)]
		[TestCase(ShippingProvider.ShippingProviderRepositoryNodeAlias, ShippingProvider.ShippingProviderRepositoryNodeAlias, true)]
		public void TestRepositories(string foundNodetypeAlias, string requestedNodetypeAlias, bool shouldMatch)
		{
			var actual = UmbracoStaticCachedEntityRepository.CheckRepositories(foundNodetypeAlias.ToLowerInvariant(), requestedNodetypeAlias.ToLowerInvariant());

			Assert.AreEqual(shouldMatch, actual);
		}

		[TestCase(Product.NodeAlias + "Customized", Product.NodeAlias, true)]
		[TestCase(ProductVariant.NodeAlias, Product.NodeAlias, false)]
		[TestCase(ProductVariant.NodeAlias + "Customized", Product.NodeAlias, false)]
		[TestCase(Product.NodeAlias, "notproduct", true)]
		[TestCase(ProductVariant.NodeAlias, ProductVariant.NodeAlias, true)]
		public void TestProduct(string foundNodetypeAlias, string requestedNodetypeAlias, bool shouldMatch)
		{
			var actual = UmbracoStaticCachedEntityRepository.CheckProduct(foundNodetypeAlias.ToLowerInvariant(), requestedNodetypeAlias.ToLowerInvariant());

			Assert.AreEqual(shouldMatch, actual);
		}

		[TestCase(PaymentProvider.NodeAlias + "Customized", PaymentProvider.NodeAlias, true)]
		[TestCase(PaymentProvider.PaymentProviderSectionNodeAlias, PaymentProvider.NodeAlias, false)]
		[TestCase(Zone.PaymentZoneNodeAlias, PaymentProvider.NodeAlias, false)]
		[TestCase(PaymentProvider.PaymentProviderZoneSectionNodeAlias, PaymentProvider.NodeAlias, false)]
		[TestCase(PaymentProvider.NodeAlias, "something", true)]
		[TestCase(PaymentProvider.PaymentProviderSectionNodeAlias, PaymentProvider.PaymentProviderSectionNodeAlias, true)]
		[TestCase(Zone.PaymentZoneNodeAlias, Zone.PaymentZoneNodeAlias, true)]
		[TestCase(PaymentProvider.PaymentProviderZoneSectionNodeAlias, PaymentProvider.PaymentProviderZoneSectionNodeAlias, true)]
		public void TestPaymentProvider(string foundNodetypeAlias, string requestedNodetypeAlias, bool shouldMatch)
		{
			var actual = UmbracoStaticCachedEntityRepository.CheckPaymentProvider(foundNodetypeAlias.ToLowerInvariant(), requestedNodetypeAlias.ToLowerInvariant());

			Assert.AreEqual(shouldMatch, actual);
		}

		[TestCase(ShippingProvider.NodeAlias + "Customized", ShippingProvider.NodeAlias, true)]
		[TestCase(ShippingProvider.ShippingProviderSectionNodeAlias, ShippingProvider.NodeAlias, false)]
		[TestCase(Zone.ShippingZoneNodeAlias, ShippingProvider.NodeAlias, false)]
		[TestCase(ShippingProvider.ShippingProviderZoneSectionNodeAlias, ShippingProvider.NodeAlias, false)]
		[TestCase(ShippingProvider.NodeAlias, "something", true)]
		[TestCase(ShippingProvider.ShippingProviderSectionNodeAlias, ShippingProvider.ShippingProviderSectionNodeAlias, true)]
		[TestCase(Zone.ShippingZoneNodeAlias, Zone.ShippingZoneNodeAlias, true)]
		[TestCase(ShippingProvider.ShippingProviderZoneSectionNodeAlias, ShippingProvider.ShippingProviderZoneSectionNodeAlias, true)]
		public void TestShippingProvider(string foundNodetypeAlias, string requestedNodetypeAlias, bool shouldMatch)
		{
			var actual = UmbracoStaticCachedEntityRepository.CheckShippingProvider(foundNodetypeAlias.ToLowerInvariant(), requestedNodetypeAlias.ToLowerInvariant());

			Assert.AreEqual(shouldMatch, actual);
		}

	}
}
