﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.PayPal
{
	/// <summary>
	/// DLL naming must be: uWebshop.Payment.PaymentProviderName
	/// </summary>
	public class PayPalPaymentProvider : IPaymentProvider
	{
		#region IPaymentProvider Members

		/// <summary>
		/// Gets the payment provider name
		/// </summary>
		/// <returns>Name of the payment provider</returns>
		public string GetName()
		{
			return "PayPal";
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id = 0)
		{
			var paymentProviderMethodList = new List<PaymentProviderMethod>
				{
					new PaymentProviderMethod
						{
							Id = "PayPal",
							ProviderName = GetName(),
							Title = "PayPal",
							Description = "PayPal"
						}
				};

			return paymentProviderMethodList;
		}

		#endregion

		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}
	}
}