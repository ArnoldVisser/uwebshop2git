﻿using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Configuration;
using uWebshop.Common;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.PayPal
{
	/// <summary>
	/// Handles the response from the payment provider
	/// </summary>
	public class PayPalPaymentResponseHandler : IPaymentResponseHandler
	{
		/// <summary>
		/// Gets the payment provider name
		/// </summary>
		/// <returns>Name of the payment provider</returns>
		public string GetName()
		{
			return "PayPal";
		}

		/// <summary>
		/// Handles the response
		/// </summary>
		public string HandlePaymentResponse()
		{
			//Post back to either sandbox or live
			const string strSandbox = "https://www.sandbox.paypal.com/cgi-bin/webscr";
			const string strLive = "https://www.paypal.com/cgi-bin/webscr";

			var payPalTestMode = WebConfigurationManager.AppSettings["PayPalTestMode"];

			var payPalNode = PaymentProviderHelper.GetAllPaymentProviders().FirstOrDefault(x => x.Name == GetName());

			if (string.IsNullOrEmpty(payPalTestMode))
			{
				payPalTestMode = payPalNode != null && payPalNode.TestMode ? "true" : "false";
			}

			if (string.IsNullOrEmpty(payPalTestMode))
			{
				Log.Instance.LogError("PayPal: Missing in web.config appsettings field with name: add key='PayPalTestMode' value='true' ");
			}

			var req = payPalTestMode != null && payPalTestMode == "true" ? (HttpWebRequest)WebRequest.Create(strSandbox) : (HttpWebRequest)WebRequest.Create(strLive);
			
			//Set values for the request back
			req.Method = "POST";
			req.ContentType = "application/x-www-form-urlencoded";
			var param = HttpContext.Current.Request.BinaryRead(HttpContext.Current.Request.ContentLength);
			var strRequest = Encoding.ASCII.GetString(param);
			strRequest += "&cmd=_notify-validate";
			req.ContentLength = strRequest.Length;

			//for proxy
			//WebProxy proxy = new WebProxy(new Uri("http://url:port#"));
			//req.Proxy = proxy;

			//Send the request to PayPal and get the response
			var streamOut = new StreamWriter(req.GetRequestStream(), Encoding.ASCII);
			streamOut.Write(strRequest);
			streamOut.Close();
			var streamIn = new StreamReader(req.GetResponse().GetResponseStream());
			var strResponse = streamIn.ReadToEnd();
			streamIn.Close();

			Log.Instance.LogDebug( "PAYPAL RETURN strResponse: " + strResponse);

			switch (strResponse)
			{
				case "VERIFIED":
					{
						// PayPal POSTS some values
						var paymentStatus = HttpContext.Current.Request["payment_status"];

						Log.Instance.LogDebug( "PAYPAL RETURN paymentStatus: " + paymentStatus);

						// Get identifier created with the RequestHandler
						var transactionId = HttpContext.Current.Request["custom"];

						Log.Instance.LogDebug( "PAYPAL RETURN transactionId: " + transactionId);

						// Match  identifier to order
						//var order = this._OrderRepository.GetOrderByPaymentTransactionId(transactionId);

						var orderInfo = OrderHelper.GetOrderInfo(transactionId);

						// Check for match
						if (orderInfo != null && orderInfo.Paid == false)
						{
							Log.Instance.LogDebug( "PAYPAL RETURN STATUS: " + paymentStatus);
							// Get statusses from payment provider Response
							switch (paymentStatus)
							{
								case "Completed":
									orderInfo.Paid = true;
									orderInfo.Status = OrderStatus.ReadyForDispatch;
									break;
								case "Failed":
									orderInfo.Paid = false;
									orderInfo.Status = OrderStatus.PaymentFailed;
									break;
								case "Denied":
									orderInfo.Paid = false;
									orderInfo.Status = OrderStatus.PaymentFailed;
									break;
								case "Pending":
									orderInfo.Status = OrderStatus.WaitingForPaymentProvider;

									break;
							}
							orderInfo.Save();
						}
					}
					break;
				case "INVALID":
					break;
			}

			return null;
		}
	}
}