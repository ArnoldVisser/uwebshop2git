﻿using System;
using System.Globalization;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.NodeFactory;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.PayPal
{
	/// <summary>
	/// Create request to Payment Provider API
	/// </summary>
	public class PayPalPaymentRequestHandler : IPaymentRequestHandler
	{
		#region IPaymentRequestHandler Members

		/// <summary>
		/// Gets the payment provider name
		/// </summary>
		/// <returns>Name of the payment provider</returns>
		public string GetName()
		{
			return "PayPal";
		}

		/// <summary>
		/// Creates a payment request for this payment provider
		/// </summary>
		/// <param name="orderInfo"> </param>
		/// <returns>Payment request</returns>
		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			#region build urls

			var baseUrl = PaymentProviderHelper.GenerateBaseUrl();

			var successNodeId = 0;
			int.TryParse(paymentProvider.SuccesNodeId, out successNodeId);

			var returnUrl = baseUrl;

			if (successNodeId != 0)
			{
				var url = library.NiceUrl(successNodeId);
				if (!url.StartsWith("http"))
				{
					returnUrl = string.Format("{0}{1}", baseUrl, url);
				}
			}

			Log.Instance.LogDebug("Paypal returnUrl " + returnUrl + ", paymentProviderNodeId: " + paymentProvider.Id);

			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);

			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));

			Log.Instance.LogDebug("Paypal reportUrl " + reportUrl + ", paymentProviderNodeId: " + paymentProvider.Id);
		
			#endregion

			#region config helper

			var accountId = helper.Settings["accountId"];
			if (string.IsNullOrEmpty(accountId))
			{
				Log.Instance.LogDebug("PayPal: Missing PaymentProvider.Config  field with name: accountId" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var testUrl = helper.Settings["testURL"];
			if (string.IsNullOrEmpty(testUrl))
			{
				Log.Instance.LogDebug("PayPal: Missing PaymentProvider.Config field with name: testURL" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var liveUrl = helper.Settings["url"];
			if (string.IsNullOrEmpty(testUrl))
			{
				Log.Instance.LogDebug("PayPal: Missing PaymentProvider.Config field with name: url" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var uniqueId = orderInfo.OrderNumber + "x" + DateTime.Now.ToString("hhmmss");

			Log.Instance.LogDebug("Paypal uniqueId " + uniqueId + ", paymentProviderNodeId: " + paymentProvider.Id);

			#endregion

			var request = new PaymentRequest();
			request.Parameters.Add("cmd", "_xclick");

			// retrieve Account ID
			request.Parameters.Add("business", accountId);

			//request.Parameters.Add("invoice", order.OrderInfo.OrderNumber.ToString());
			request.Parameters.Add("invoice", orderInfo.OrderNumber);
			var ci = new CultureInfo("en-US");
			var totalAmountAsString = orderInfo.ChargedAmount.ToString("N", ci);
			request.Parameters.Add("amount", totalAmountAsString);
			request.Parameters.Add("tax_cart", totalAmountAsString);
			request.Parameters.Add("no_note", "0");
			var ri = new RegionInfo(orderInfo.StoreInfo.Store.CurrencyCultureInfo.LCID);
			request.Parameters.Add("currency_code", ri.ISOCurrencySymbol);
			request.Parameters.Add("lc", orderInfo.StoreInfo.CultureInfo.TwoLetterISOLanguageName);

			request.Parameters.Add("return", returnUrl);

			request.Parameters.Add("shopping_url", baseUrl);

			request.Parameters.Add("notify_url", reportUrl);

			#region testmode

			if (paymentProvider.TestMode)
			{
				request.Parameters.Add("cn", "Test");
			}

			#endregion

			// Order as shown with PayPal
			Log.Instance.LogDebug( "Paypal OrderInfo.OrderNumber: " + orderInfo.OrderNumber);
			request.Parameters.Add("item_name", orderInfo.OrderNumber);

			// Set GUID to identify order in SSWS
			// Sent GUID for identification to PayPal
			// PayPal will return custom value to validate order

			request.Parameters.Add("custom", uniqueId);

			orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;

			#region URLforTestMode

			// check if provider is in testmode to send request to right URL
			request.PaymentUrlBase = paymentProvider.TestMode ? testUrl : liveUrl;

			#endregion
			uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, uniqueId);
			orderInfo.PaymentInfo.TransactionId = uniqueId;
			orderInfo.PaymentInfo.Url = request.PaymentUrl;
			orderInfo.PaymentInfo.Parameters = request.ParametersAsString;
			
			return request;
		}

		#endregion

		/// <summary>
		/// Returns the URL to redirect to
		/// </summary>
		/// <param name="orderInfo"> </param>
		/// <returns></returns>
		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return string.Empty;
		}
	}
}