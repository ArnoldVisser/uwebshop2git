﻿using System;
using System.Collections.Generic;
using uWebshop.Domain.Helpers;
using umbraco.BusinessLogic.Actions;
using umbraco.cms.businesslogic.web;
using umbraco.cms.presentation.Trees;
using umbraco.interfaces;
using umbraco.NodeFactory;
using umbraco.BasePages;

namespace uWebshop.UmbracoSection.Trees
{
    public class LoadCustomTree : BaseContentTree
    {
        #region properties

        public override int StartNodeID
        {
            get
            {
                if (this.app == "uwbs")
                {
                    var nodeId = DomainHelper.GetNodeIdForDocument("uWebshop", "uWebshop");

                    return nodeId;
                }
                else
                {
                    UmbracoEnsuredPage page = new UmbracoEnsuredPage();
                    return page.getUser().StartNodeId;
                }
            }
        }

        private Document m_document;

        protected Document StartNode
        {
            get
            {
                if (m_document == null)
                {
                    m_document = new Document(true, StartNodeID);
                }

                if (!m_document.Path.Contains(CurrentUser.StartNodeId.ToString()))
                {
                    var doc = new Document(true, CurrentUser.StartNodeId);
                    if (!string.IsNullOrEmpty(doc.Path) && doc.Path.Contains(this.StartNodeID.ToString()))
                    {
                        m_document = doc;
                    }
                    else
                    {
                        return null;
                    }
                }

                return m_document;
            }
        }
        #endregion

        public LoadCustomTree(string application) : base(application) { }

        protected override void CreateRootNode(ref XmlTreeNode rootNode)
        {
            if (this.IsDialog)
                rootNode.Action = "javascript:openContent(-1);";
            else
                if(this.app == "uwbs")
                    rootNode.Action = "javascript:" + ClientTools.Scripts.OpenDashboard("uwbs");
                else
                    rootNode.Action = "javascript:" + ClientTools.Scripts.OpenDashboard("content");

            if (this.app == "uwbs")
            {
                rootNode.Icon = "uWebshop.png";
                rootNode.OpenIcon = "uWebshop.png";
                rootNode.NodeType = TreeAlias;
                rootNode.NodeID = DomainHelper.GetNodeIdForDocument("uWebshop", "uWebshop").ToString();
                rootNode.Text = "uWebshop";
            }
        }

        protected override void CreateRootNodeActions(ref List<IAction> actions)
        {
            if (this.app == "uwbs")
            {
                actions.Clear();
                actions.Add(ActionSort.Instance);
                actions.Add(ContextMenuSeperator.Instance);
                actions.Add(ActionPublish.Instance);
                actions.Add(ContextMenuSeperator.Instance);
                actions.Add(ActionRefresh.Instance);
           }
            else
            {
                actions.Clear();

                if (StartNodeID != -1)
                {
                    //get the document for the start node id
                    Document doc = StartNode;
                    if (doc == null)
                    {
                        return;
                    }

                    //get the allowed actions for the user for the current node
                    List<IAction> nodeActions = GetUserActionsForNode(doc);
                    //get the allowed actions for the tree based on the users allowed actions
                    List<IAction> allowedMenu = GetUserAllowedActions(AllowedActions, nodeActions);
                    actions.AddRange(allowedMenu);
                }
                else
                {
                    ///add the default actions to the content tree
                    actions.Add(ActionNew.Instance);
                    actions.Add(ActionSort.Instance);
                    actions.Add(ContextMenuSeperator.Instance);
                    actions.Add(ActionRePublish.Instance);
                    actions.Add(ContextMenuSeperator.Instance);
                    actions.Add(ActionRefresh.Instance);
                }
            }
        }

        protected override void CreateAllowedActions(ref List<IAction> actions)
        {
            actions.Clear();
            actions.Add(ActionNew.Instance);
            actions.Add(ActionLiveEdit.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionDelete.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionMove.Instance);
            actions.Add(ActionCopy.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionSort.Instance);
            actions.Add(ActionRollback.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionPublish.Instance);
            actions.Add(ActionToPublish.Instance);
            actions.Add(ActionAssignDomain.Instance);
            actions.Add(ActionRights.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionProtect.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionUnPublish.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionNotify.Instance);
            actions.Add(ActionSendToTranslate.Instance);
            actions.Add(ContextMenuSeperator.Instance);
            actions.Add(ActionRefresh.Instance);
        }

        public override void Render(ref XmlTree tree)
        {
            Document[] childrenForTree = Document.GetChildrenForTree(base.m_id);
            TreeEventArgs e = new TreeEventArgs(tree);
            this.OnBeforeTreeRender(childrenForTree, e);
            foreach (Document document in childrenForTree)
            {
                List<IAction> userActionsForNode = this.GetUserActionsForNode(document);
                if (this.CanUserAccessNode(document, userActionsForNode))
                {
                    XmlTreeNode xNode = this.CreateNode(document, userActionsForNode);
                    this.OnRenderNode(ref xNode, document);
                    this.OnBeforeNodeRender(ref tree, ref xNode, EventArgs.Empty);

                    if (xNode != null)
                    {
                        var n = new Node(int.Parse(xNode.NodeID));



                        if (!IsDialog &&
                            xNode.NodeType == "content" &&
                            n.NodeTypeAlias == "uWebshop")
                        {
                        }
                        else
                        {
                            tree.Add(xNode);
                            this.OnAfterNodeRender(ref tree, ref xNode, EventArgs.Empty);
                        }

                           // uncomment this and comment above to disable tree in own section
                        //tree.Add(xNode);
                        //this.OnAfterNodeRender(ref tree, ref xNode, EventArgs.Empty);
                    }
                }
            }
            this.OnAfterTreeRender(childrenForTree, e);

            XmlTreeNode recycleBin = CreateRecycleBin();
            if (recycleBin != null)
            {
                tree.Add(recycleBin);
            }
        }

        protected new virtual bool CanUserAccessNode(Document doc, List<IAction> allowedUserOptions)
        {
            return allowedUserOptions.Contains(ActionBrowse.Instance);
        }

        protected XmlTreeNode CreateRecycleBin()
        {
            if (m_id == -1 && !this.IsDialog || m_id == DomainHelper.GetNodeIdForDocument("uWebshop", "uWebshop"))
            {
                //create a new content recycle bin tree, initialized with it's startnodeid
                ContentRecycleBin bin = new ContentRecycleBin("content");
                bin.ShowContextMenu = this.ShowContextMenu;
                bin.id = bin.StartNodeID;
                return bin.RootNode;
            }

            return null;
        }

        protected new List<IAction> GetUserActionsForNode(Document dd)
        {
            List<IAction> actions = umbraco.BusinessLogic.Actions.Action.FromString(CurrentUser.GetPermissions(dd.Path));

            // A user is allowed to delete their own stuff
            if (dd.UserId == CurrentUser.Id && !actions.Contains(ActionDelete.Instance))
                actions.Add(ActionDelete.Instance);

            return actions;
        }

        protected new List<IAction> GetUserAllowedActions(List<IAction> actions, List<IAction> userAllowedActions)
        {
            return actions.FindAll(delegate(IAction a)
            {
                return !a.CanBePermissionAssigned || (a.CanBePermissionAssigned && userAllowedActions.Contains(a));
            });
        }
    }
}
