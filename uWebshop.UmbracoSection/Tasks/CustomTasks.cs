﻿
namespace uWebshop.UmbracoSection.Tasks
{
    public class CustomTasks : umbraco.interfaces.ITaskReturnUrl
    {
        #region properties
        public string Alias { get; set; }
        public int ParentID { get; set; }
        public int TypeID { get; set; }
        public int UserId { private get; set; }
        #endregion

        #region constructors
        public bool Save()
        {
            //Code that will execute on creation
            ReturnUrl = string.Format("editContent.aspx?id={0}", "1");

            return true;
        }
        #endregion

        #region public methods
        public bool Delete()
        {
            //Code that will execute when deleting

            return false;
        }
        #endregion

        #region ITaskReturnUrl Members
        public string ReturnUrl { get; set; }
        #endregion
    }
}
