﻿using Examine;
using System;
using System.Linq;
using System.Web.UI;
using uWebshop.Common;
using uWebshop.DataTypes.StorePicker;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco;
using umbraco.BasePages;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.datatype;
using umbraco.cms.businesslogic.template;
using umbraco.cms.businesslogic.web;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Starterkits.Sandbox
{
	public partial class uWebshopStarterkitInstaller : UserControl
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			Licensing.uWebshopTrialMessage();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			var categoryDocType = DocumentType.GetByAlias("uwbsCategory");
			var productDocType = DocumentType.GetByAlias("uwbsProduct");

			var categoryTemplate = Template.GetByAlias("uwbsCategory");
			var productTemplate = Template.GetByAlias("uwbsProduct");

			categoryDocType.allowedTemplates = new[] {categoryTemplate};
			categoryDocType.DefaultTemplate = categoryTemplate.Id;

			categoryDocType.Save();

			productDocType.allowedTemplates = new[] {productTemplate};
			productDocType.DefaultTemplate = productTemplate.Id;

			productDocType.Save();


			var installedHomePageDt = DocumentType.GetByAlias("uwbsHomepage");

			var storePickerDataTypeDef = new StorePickerDataType();
			var storePickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == storePickerDataTypeDef.Id);

			if (storePickerDataType != null)
			{
				if (installedHomePageDt.getPropertyType(Constants.StorePickerAlias) == null)
				{
					installedHomePageDt.AddPropertyType(storePickerDataType, Constants.StorePickerAlias,
					                                    "#StorePicker");
				}
			}

			var storeDocumentType = DocumentType.GetByAlias(Store.NodeAlias);

			if (storeDocumentType != null)
			{
				var storeDocuments = Document.GetDocumentsOfDocumentType(storeDocumentType.Id);

				if (storeDocuments.All(x => x.IsTrashed))
				{
					panel2.Visible = true;
				}
			}

			var installedHomePageDocs = Document.GetDocumentsOfDocumentType(installedHomePageDt.Id);

			var installedHomePageDoc = installedHomePageDocs.OrderBy(x => x.CreateDateTime).Last(x => !x.IsTrashed);

			if (installedHomePageDoc != null)
			{
				installedHomePageDoc.sortOrder = 1;
				installedHomePageDoc.Save();
				installedHomePageDoc.Publish(new User(0));
				library.UpdateDocumentCache(installedHomePageDoc.Id);
			}

			var uwebshopDt = DocumentType.GetByAlias("uWebshop");
			if (uwebshopDt != null)
			{
				var uWebshopDocs = Document.GetDocumentsOfDocumentType(uwebshopDt.Id);

				if (uWebshopDocs != null)
				{
					var uWebshopDoc = uWebshopDocs.First(x => !x.IsTrashed);

					uWebshopDoc.sortOrder = 10;
					uWebshopDoc.Save();
					uWebshopDoc.Publish(new User(0));
					library.UpdateDocumentCache(uWebshopDoc.Id);
				}
			}


			try
			{
				library.RefreshContent();
			}
			finally
			{
				Document.RePublishAll();
			}

			BasePage.Current.ClientTools.RefreshTree("content");


			try
			{
				ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"].RebuildIndex();
			}
			catch
			{
				BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error,
				                                              "ExternalIndexer Error",
				                                              "Please Republish All uWebshop Nodes");
				Log.Instance.LogError("uWebshop Installer: Could Not Rebuild ExternalIndexer; Publish uWebshop node + children manually");
			}
		}

		protected void BtnInstallStoreClick(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtStoreAlias.Text))
			{
				BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error,
				                                              "Error",
				                                              "No Store name entered");
			}
			else
			{
				Umbraco.Helpers.InstallStore(txtStoreAlias.Text);

				BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.success,
				                                              "Store Installed!",
				                                              "Your " + txtStoreAlias.Text + " Store is installed!");
			}
		}
	}
}