﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Xml.Linq;
using uWebshop.Common;
using umbraco;
using umbraco.BasePages;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.web;

namespace uWebshop.Payment
{
	public partial class SagePayInstaller : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}


		protected void InstallConfig(object sender, EventArgs e)
		{
			var vendorName = "#YOUR VENDORNAME#";

			if (!string.IsNullOrEmpty(txtSagePayVendorName.Text))
			{
				vendorName = txtSagePayVendorName.Text;
			}

			var paymentProviderXML = HttpContext.Current.Server.MapPath("/config/uWebshop/PaymentProviders.config");

			if (paymentProviderXML != null)
			{
				var paymentProviderXDoc = XDocument.Load(paymentProviderXML);

				if (paymentProviderXDoc.Descendants("provider").Any(x =>
					{
						var xAttribute = x.Attribute("title");
						return xAttribute != null && xAttribute.Value == "SagePay";
					}))
				{
					BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.info,
																  "SagePay config",
																  "SagePay config already created");
				}
				else
				{
					//<provider title="SagePay">
					//	<VendorName>uwebshop</VendorName>
					//	<DirectUrl>https://test.sagepay.com/Simulator/VSPDirectGateway.asp</DirectUrl>
					//	<DirectTestURL>https://test.sagepay.com/Simulator/VSPDirectGateway.asp</DirectTestURL>
					//  </provider> 


					var paymentNode = new XElement("provider", new XAttribute("title", "SagePay"),
												  new XElement("VendorName", vendorName),
												new XElement("DirectUrl", "https://live.sagepay.com/Simulator/VSPDirectGateway.asp"),
												  new XElement("DirectTestURL", "https://test.sagepay.com/Simulator/VSPDirectGateway.asp")
										);

					paymentProviderXDoc.Descendants("providers").FirstOrDefault().Add(paymentNode);

					paymentProviderXDoc.Save(paymentProviderXML);

					var dtuwbsPaymentProviderSection = DocumentType.GetByAlias("uwbsPaymentProviderSection");

					var author = new User(0);

					var uwbsPaymentProviderSectionDoc =
						Document.GetDocumentsOfDocumentType(dtuwbsPaymentProviderSection.Id).
						         FirstOrDefault();

					var dtuwbsPaymentProvider = DocumentType.GetByAlias("uwbsPaymentProvider");

					if (uwbsPaymentProviderSectionDoc != null)
					{
						var providerDoc = Document.MakeNew("SagePay", dtuwbsPaymentProvider, author, uwbsPaymentProviderSectionDoc.Id);
						providerDoc.SetProperty("title", "SagePay");
						providerDoc.SetProperty("description", "SagePay Payment Provider for uWebshop");

						providerDoc.SetProperty("type", PaymentProviderType.OnlinePayment.ToString());
						providerDoc.SetProperty("dllName", "uWebshop.Payment.SagePay");
						
						providerDoc.Save();

						BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.success,
																	  "SagePay Installed!",
																	  "SagePay config added and nodes created");
					}
				}
			}

		}
	}
}