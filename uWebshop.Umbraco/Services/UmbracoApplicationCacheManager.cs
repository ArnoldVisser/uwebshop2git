﻿using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco.Repositories;

namespace uWebshop.Umbraco.Services
{
	internal class UmbracoApplicationCacheManagingService : IApplicationCacheManagingService
	{
		private readonly IProductService _productService;
		private readonly IProductVariantService _productVariantService;
		private readonly ICategoryService _categoryService;

		public UmbracoApplicationCacheManagingService(IProductService productService, IProductVariantService productVariantService, ICategoryService categoryService)
		{
			_productService = productService;
			_productVariantService = productVariantService;
			_categoryService = categoryService;
		}

		public void ClearCache()
		{
			UmbracoStaticCachedEntityRepository.ResetStaticCache();
			UmbracoStaticCachedEntityRepository.ResetEntityCache();
		}

		public void ReloadEntityWithGlobalId(int id, string typeName = null)
		{
			//if (nodeTypeAlias == null) nodeTypeAlias = new Document(id).ContentType.Alias;

			//IO.Container.Resolve<IEntityServiceService>().GetByTypeAlias<int>(nodeTypeAlias).ReloadEntityWithId(id);

			//if (Product.IsAlias(nodeTypeAlias))
			//	_productService.FullResetCache();
			////_productService.ReloadEntityWithId(id);
			//else if (ProductVariant.IsAlias(nodeTypeAlias))
			//	_productVariantService.FullResetCache();
			//	//_productVariantService.ReloadEntityWithId(id);
			//else if (Category.IsAlias(nodeTypeAlias))
			//	_categoryService.FullResetCache();
			//	//_categoryService.ReloadEntityWithId(id);
			//else 
				if (Settings.NodeAlias == typeName)
			{
				IO.Container.RegisterInstance<ISettingsService>(SettingsLoader.GetSettings()); // bit of a hack, todo: improve
				_productService.ReloadWithVATSetting();
			}
			else
			{
				Log.Instance.LogDebug("Issueing Full Reset Cache after 'reload entity with id'");
				_productService.FullResetCache();
				_productVariantService.FullResetCache();
				_categoryService.FullResetCache();
				
				UmbracoStaticCachedEntityRepository.ResetStaticCache();
			}
			UmbracoStaticCachedEntityRepository.ResetEntityCache();
		}

		public void UnloadEntityWithGlobalId(int id, string typeName = null)
		{
			//if (typeName == null) typeName = new Document(id).ContentType.Alias;

			//if (Product.IsAlias(typeName))
			//	_productService.UnloadEntityWithId(id);
			//else if (ProductVariant.IsAlias(typeName))
			//	_productVariantService.UnloadEntityWithId(id);
			//else if (Category.IsAlias(typeName))
			//	_categoryService.UnloadEntityWithId(id);
			//else
			//{
				Log.Instance.LogDebug("Issueing Full Reset Cache after 'unload entity with id'");

				_productService.FullResetCache();
				_productVariantService.FullResetCache();
				_categoryService.FullResetCache();
				UmbracoStaticCachedEntityRepository.ResetStaticCache();
			//}
			UmbracoStaticCachedEntityRepository.ResetEntityCache();
		}
	}
	internal interface IEntityServiceService
	{
		IEntityService<T> GetByTypeAlias<T>(string alias);
	}

}
