﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Umbraco.Services
{
	// todo: move to Domain when possible
	internal class UmbracoZoneService : IZoneService
	{
		private readonly ICountryRepository _countryRepository;

		public UmbracoZoneService(ICountryRepository countryRepository)
		{
			_countryRepository = countryRepository;
		}

		/// <summary>
		/// Returns all the payment zones
		/// </summary>
		public List<Zone> GetAllPaymentZones(string storeAlias)
		{
			var zoneNodes = DomainHelper.GetObjectsByAlias<Zone>(Zone.PaymentZoneNodeAlias, storeAlias).ToList();
			return !zoneNodes.Any() ? GetFallBackZone(storeAlias) : zoneNodes;
		}

		/// <summary>
		/// Returns all the shipping zones
		/// </summary>
		public List<Zone> GetAllShippingZones(string storeAlias)
		{
			var zoneNodes = DomainHelper.GetObjectsByAlias<Zone>(Zone.ShippingZoneNodeAlias, storeAlias).ToList();
			return !zoneNodes.Any() ? GetFallBackZone(storeAlias) : zoneNodes;
		}

		public Zone GetByIdOrFallbackZone(int id, string storeAlias)
		{
			return GetAllPaymentZones(storeAlias).FirstOrDefault(z => z.Id == id)
				   ?? GetAllShippingZones(storeAlias).FirstOrDefault(z => z.Id == id)
				   ?? FallBackZone(storeAlias);
		}

		private Zone FallBackZone(string storeAlias)
		{
			return new Zone { CountryCodes = _countryRepository.GetAllCountries(storeAlias).Select(c => c.Code).ToList() };
		}
		private List<Zone> GetFallBackZone(string storeAlias)
		{
			return new List<Zone> { FallBackZone(storeAlias) };
		}
	}
}
