﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Documents;
using Lucene.Net.QueryParsers;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.NodeFactory;
using uWebshop.Umbraco.Businesslogic;

namespace uWebshop.Umbraco.Services
{
	internal class ExamineCatalogUrlSplitterService : ICatalogUrlSplitterService
	{
		private readonly ICMSApplication _cmsApplication;

		public ExamineCatalogUrlSplitterService(ICMSApplication cmsApplication)
		{
			_cmsApplication = cmsApplication;
		}

		public CatalogUrlResolveServiceResult DetermineCatalogUrlComponents(string url)
		{
			var result = default(CatalogUrlResolveServiceResult);

			// todo: een betere/mooiere code voor het opsplitsen van de url
			
			var nodeUrls = url.TrimEnd('/').ToLower().Split('/').Where(u => u.Length > 0).ToList();

			var treeNodes = SearchResults(nodeUrls); // todo: DIP and test this service

			if (treeNodes == null)
			{
				Log.Instance.LogError("UrlRewrite: No SearchResult");
				return result;
			}

			if (!treeNodes.Any(node => node.StoreRootNodeId.HasValue) && StoreHelper.GetAllStores().Skip(1).Any())
			{
				// either the examine indices need to be rebuilt or we are in a part of the node tree not under uWebshop
				// todo: this will really fuckup the cache if it's populated when rebuilding the index
				//new ExamineRebuildIndicesService().Rebuild(); // todo: DIP
				return result;
			}
			ITreeNode currentNode = null;

			int currentLevel = _cmsApplication.HideTopLevelNodeFromPath ? 2 : 1;
			foreach (var u in nodeUrls)
			{
				var node = treeNodes.FirstOrDefault(n => n.UrlName == u && n.Level == currentLevel); // is first wel altijd goed?
				if (node == null) break;

				currentNode = node;
			    currentLevel++;

			    // todo: deze code kan niet goed zijn: faalt wanneer 1e deel path wel matcht en dan een deel niet, vermoedelijke fix: && currentNode == null in if
			}

			string categoryProductPart;
			if (currentNode == null) // none found, so we are at / with HideTopLevelNodeFromPath = true
			{
				var rootNodes = treeNodes.Where(node => node.Level == 1 && node.StoreRootNodeId != null);
				if (rootNodes.Count() == 1)
				{
					//var rootId = umbraco.cms.businesslogic.web.Domain.GetRootFromDomain(context.Request.Url.Host);
				}
				// todo: hier moet nog een domein check in!
				categoryProductPart = url;
				if (HttpContext.Current != null)
				{
					var domain = HttpContext.Current.Request.Url.Host;

					// todo: hier moet een check in, want als er geen hostname is gezet maar wel 2 stores aangemaakt, gaat dit fout!
					var store = StoreHelper.GetAllStores().FirstOrDefault(x => x.StoreURL.Contains(domain) || x.Domains.Contains(domain));

					if (store != null)
					{
						result.Store = store;
						UwebshopRequest.Current.CurrentStore = store;
					}
				}
			}
			else
			{
				if (!url.EndsWith("/"))
				{
					url += "/";
				}
				// remove the path of the node
				categoryProductPart = url.Substring(url.LastIndexOf("/" + currentNode.UrlName.ToLowerInvariant() + "/") + currentNode.UrlName.Length + 1);

				var storePickerValue = currentNode.StoreRootNodeId.GetValueOrDefault(0);
				if (storePickerValue != 0)
				{
					var currentStore = DomainHelper.StoreById(storePickerValue);
					result.Store = currentStore;
				    UwebshopRequest.Current.CurrentStore = currentStore;
				}
			}

			//return categoryProductPart;
			if (String.IsNullOrEmpty(categoryProductPart))
				return result;
			var rewriteNodeUrl = url.ToLowerInvariant().Replace(categoryProductPart, string.Empty);
			if (rewriteNodeUrl == string.Empty)
				rewriteNodeUrl = "/";

			result.CatalogUrl = categoryProductPart;
			result.StoreNodeUrl = rewriteNodeUrl;
			if (result.Store != null) result.Store.StoreURL = rewriteNodeUrl;
			return result;
		}

		private static List<ITreeNode> SearchResults(IEnumerable<string> nodeUrls)
		{
            //var xpath = "//*[" + String.Join(" or ", nodeUrls.Select(urlValue => "@urlName = '" + QueryParser.Escape(urlValue) + "'")) + " or @level = '1']"; // reminder: moving of QueryParser.Escape is an untested functional change while refactoring
            //var it = library.GetXmlNodeByXPath(xpath);

            //var result = new List<ITreeNode>();
            //while (it.MoveNext())
            //{
            //    if (it.Current == null) continue;
            //    var id = it.Current.GetAttribute("id", it.Current.NamespaceURI);
            //    if (string.IsNullOrWhiteSpace(id)) continue;
            //    result.Add(new TreeNode(Convert.ToInt32(id)));
            //}
            //return result;

            //var criteria = ExamineManager.Instance
            //        .SearchProviderCollection[UwebshopConfiguration.ExamineSearcher]
            //        .CreateSearchCriteria().ParentId(0); // IndexTypes.Content
            //Examine.SearchCriteria.IBooleanOperation filter = criteria;
            ////filter = criteria.Field("parentID", @"\-1");
            //// filter = filter.And().GroupedOr(new string[] { "nodeName", "title", "title_EN", "title_DE", "url", "sku", "metaTags", "metaDescription", "metaDescription_EN", "metaDescription_DE", "description", "description_DE", "description_EN" }, SearchArray[i].MultipleCharacterWildcard());
            //var searchResult = ExamineManager.Instance.SearchProviderCollection[UwebshopConfiguration.ExamineSearcher].Search(filter.Compile());
			var query = String.Join(" OR ", nodeUrls.Select(urlValue => "(urlName:" + QueryParser.Escape(urlValue) + ")")); // reminder: moving of QueryParser.Escape is an untested functional change while refactoring

            //not(starts-with(name(),'{1}Repository')) // [not(self::'uWebshop')]
            var xpath = "/root/*"; // reminder: moving of QueryParser.Escape is an untested functional change while refactoring
            var it = library.GetXmlNodeByXPath(xpath);

            var result = new List<ITreeNode>();
            while (it.MoveNext())
            {
                if (it.Current == null) continue;
                var id = it.Current.GetAttribute("id", it.Current.NamespaceURI);
                if (string.IsNullOrWhiteSpace(id)) continue;
                result.Add(new TreeNode(Convert.ToInt32(id)));
            }
            //return result;

			var searchResult = InternalHelpers.GetSearchResults(query);

			//var treeNodes = searchResult.Where(x => !x.Fields["__NodeTypeAlias"].StartsWith(Category.NodeAlias.ToLower()) && !x.Fields["__NodeTypeAlias"].StartsWith(Product.NodeAlias.ToLower())).Select(examineNode => new TreeNode(examineNode)).ToList();
			var uwebshopNode = DomainHelper.GetUwebShopNode();

			return searchResult.Where(x => uwebshopNode != null
			                               && !x.GetField("path").StringValue().Contains(uwebshopNode.Id.ToString())
										   && x.GetField("__IndexType").StringValue() == "content").Select(examineNode => (ITreeNode)new TreeNode(examineNode))
                                           
                                           .Concat(result)

                                           .ToList();
		}

		internal interface ITreeNode
		{
			string UrlName { get; set; }
			int? StoreRootNodeId { get; set; }
			int Level { get; set; }
			int Id { get; set; }
			string Path { get; set; }
		}

		internal class TreeNode : ITreeNode
		{
			public int Id { get; set; }
			public string NodeTypeAlias { get; set; }
			public string Path { get; set; }
			public int ParentId { get; set; }
			public string UrlName { get; set; }
			public int Level { get; set; }
			public int? SortOrder { get; set; }
			public int? StoreRootNodeId { get; set; }

			public TreeNode(int nodeId)
			{
				var node = new Node(nodeId);
				Path = node.Path;
				NodeTypeAlias = node.NodeTypeAlias;
				if (node.Name != null && node.Parent != null)
					ParentId = node.Parent.Id;
				Id = node.Id;
				UrlName = node.UrlName;
				SortOrder = node.SortOrder;
				Level = node.Level;

				StoreRootNodeId = node.GetProperty<int>("uwbsStorePicker");
			}

			public TreeNode(Document examineNode)
			{
				if (examineNode.GetField("id") != null)
				{
					string value = examineNode.GetField("id").StringValue();
					int id;
					if (int.TryParse(value, out id))
						Id = id;
				}
				if (examineNode.GetField("parentID") != null)
				{
					string value = examineNode.GetField("parentID").StringValue();
					int id;
					if (int.TryParse(value, out id))
						ParentId = id;
				}
				if (examineNode.GetField("nodeTypeAlias") != null)
				{
					NodeTypeAlias = examineNode.GetField("nodeTypeAlias").StringValue();
				}
				if (examineNode.GetField("path") != null)
				{
					Path = examineNode.GetField("path").StringValue();
				}
				if (examineNode.GetField("sortOrder") != null)
				{
					string value = examineNode.GetField("sortOrder").StringValue();
					int id;
					if (int.TryParse(value, out id))
						SortOrder = id;
				}
				if (examineNode.GetField("level") != null)
				{
					string value = examineNode.GetField("level").StringValue();
					int id;
					if (int.TryParse(value, out id))
						Level = id;
				}
				if (examineNode.GetField("urlName") != null)
				{
					UrlName = examineNode.GetField("urlName").StringValue();
				}

				if (examineNode.GetField("uwbsStorePicker") != null)
				{
					var value = examineNode.GetField("uwbsStorePicker").StringValue(); 
					int uwbsStorePicker;
					if (Int32.TryParse(value, out uwbsStorePicker))
						StoreRootNodeId = uwbsStorePicker;
				}
				
			}
		}
	}
}