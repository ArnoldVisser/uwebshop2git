﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Examine;
using umbraco;
using umbraco.cms.businesslogic.web;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Umbraco.Services
{
	internal class UmbracoStoreService : IStoreService
	{
		private readonly IStoreRepository _storeRepository;
		private readonly ICMSApplication _cmsApplication;
		private readonly ICMSEntityRepository _cmsEntityRepository;
		internal static List<Store> AllStoresCache;

		public UmbracoStoreService(IStoreRepository storeRepository, ICMSApplication cmsApplication, ICMSEntityRepository cmsEntityRepository)
		{
			_storeRepository = storeRepository;
			_cmsApplication = cmsApplication;
			_cmsEntityRepository = cmsEntityRepository;
		}

		public Store GetCurrentStore()
		{
			var uwbsRequest = UwebshopRequest.Current;

			var store = uwbsRequest.CurrentStore; // currently url overrules everything (when doing that differently, do it in CatalogUrlResolveService!)
			if (store != null) return store;

			// in feite zou onderstaande code alleen in de backend bereikt kunnen worden of wanneer op node buiten shop tree
			if (HttpContext.Current != null)
			{
				store = _storeRepository.TryGetStoreFromBackendCurrentOrder()
						?? _storeRepository.TryGetStoreFromCurrentNode()
				        ?? TryGetStoreFromDomain();
			}

			if (store == null)
			{
				Log.Instance.LogWarning("Could not determine current store, fallback to first store");
				store = GetAllStores().OrderBy(x => x.SortOrder).FirstOrDefault(); // fallback
			}

			if (store != null && !String.IsNullOrEmpty(store.Alias))
			{
				uwbsRequest.CurrentStore = store;
			}
			return store;
		}

		private static Store TryGetStoreFromQueryString()
		{
			if (HttpContext.Current == null) return null;
			int storeId;
			var storeIdValue = HttpContext.Current.Request.QueryString["storeId"];
			if (!String.IsNullOrWhiteSpace(storeIdValue) && Int32.TryParse(storeIdValue, out storeId))
			{
				return DomainHelper.StoreById(storeId);
			}
			return null;
		}

		private Store TryGetStoreFromDomain()
		{
			try
			{
				return GetAllStores().FirstOrDefault(x => x.StoreURL.Contains(HttpContext.Current.Request.Url.Host));
			}
			catch
			{

			}
			return null;
		}

		public string CurrentStoreAlias()
		{
			var store = GetCurrentStore();
			return store != null ? store.Alias : String.Empty;
		}

		public IEnumerable<Store> GetAllStores()
		{
			if (AllStoresCache == null || !AllStoresCache.Any())
			{
				AllStoresCache = _storeRepository.GetAll();
				//AllStoresCache = DomainHelper.GetNodeIdForDocumentAlias(Store.NodeAlias).Select(id => new Store(id)).ToList();

				AllStoresCache.ForEach(LoadStoreUrl);
			}
			return AllStoresCache;
		}

		public Store GetById(int id)
		{
			return GetAllStores().FirstOrDefault(store => store.Id == id) ?? _storeRepository.GetById(id); // node fallback to be a little bit more sure
		}

		public Store GetByAlias(string storeAlias)
		{
			if (storeAlias == null) return null;
			return GetAllStores().FirstOrDefault(store => store.Alias != null && (store.Alias == storeAlias || store.Alias.ToLowerInvariant() == storeAlias.ToLowerInvariant()));
		}
		
		public void LoadStoreUrl(Store store)
		{
			// todo: (niet zeker) dit is niet altijd goed, Request afhankelijk (neemt nu altijd eerste storePicker)

			var node = _cmsEntityRepository.GetNodeWithStorePicker(store.Id);

			if (node == null)
			{
				Log.Instance.LogDebug("StoreURL: could not find node with StorePicker:" + store.Id + " falling back to root url");
				store.StoreURL = "/";
				return;
			}

			var storeAliasUrl = GetUrlPart(node);
			while (node.Level > 1) 
			{
				node = node.Parent;
				storeAliasUrl = String.Format("{0}/{1}", GetUrlPart(node), storeAliasUrl);
			}
			
			// check if the url starts with http, and make sure it starts with a / if not starting with http so url is absolute
			if (storeAliasUrl != "" && !storeAliasUrl.StartsWith("http"))
			{
				store.StoreURL = "/" + storeAliasUrl.Trim('/') + "/";
			}
			else
			{
				store.StoreURL = storeAliasUrl.Trim('/') + "/";
			}
		}

		public string GetProductNiceUrl(Product product, string storeAlias = null, bool getCanonicalUrl = false)
		{
			if (!string.IsNullOrEmpty(storeAlias))
			{
				product = DomainHelper.GetProductById(product.Id, storeAlias);
			}

			var store = GetStoreByAliasOrCurrentStore(storeAlias);

			var categoryId = 0;

			if (product.HasCategories && getCanonicalUrl)
			{
				var firstOrDefaultCategory = product.Categories.FirstOrDefault();
				if (firstOrDefaultCategory != null)
				{
					categoryId = firstOrDefaultCategory.Id;
				}
			}

			var resultUrl = GetLocalizedProductUrlHelper(product, categoryId, store.StoreURL, store.Alias);

			return FixNiceUrl(resultUrl);
		}

		private static string GetLocalizedProductUrlHelper(Product product, int categoryId, string storeAliasUrl, string storeAlias = null)
		{
			ICategory productCategory = null;

			if (!string.IsNullOrEmpty(storeAlias))
			{
				product = DomainHelper.GetProductById(product.Id, storeAlias);
			}

			if (product == null)
			{
				return string.Empty;
			}

			if (product.HasCategories)
			{
				if (categoryId != 0)
					productCategory = DomainHelper.GetCategoryById(categoryId, storeAlias);

				if (productCategory == null)
				{
					productCategory = UwebshopRequest.Current.Category;
					if (productCategory != null) productCategory = DomainHelper.GetCategoryById(productCategory.Id, storeAlias);
				}

				productCategory = productCategory != null &&
				                  product.Categories.Any(category => category.Id == productCategory.Id)
					? productCategory
					: product.Categories.FirstOrDefault();
			}
			else if (product.ParentId != 0)
			{
				productCategory = DomainHelper.GetCategoryById(product.ParentId, storeAlias);
			}

			if (productCategory == null || productCategory.UrlName == null)
			{
				return product.UrlName;
			}

			var productCategoryUrl = productCategory.UrlName;
			while (productCategory.ParentCategory != null)
			{
				productCategory = DomainHelper.GetCategoryById(productCategory.ParentCategory.Id, storeAlias);
				productCategoryUrl = String.Format("{0}/{1}", productCategory.UrlName, productCategoryUrl);
			}
			if (string.IsNullOrEmpty(storeAliasUrl))
			{
				return String.Format("{0}/{1}", productCategoryUrl, product.UrlName);
			}
			return String.Format("{0}/{1}/{2}", storeAliasUrl.TrimEnd('/'), productCategoryUrl.TrimStart('/'), product.UrlName);
		}

		private Store GetStoreByAliasOrCurrentStore(string storeAlias)
		{
			var store = GetByAlias(storeAlias) ?? GetCurrentStore();

			if (store == null) throw new Exception("No published stores, please publish");
			return store;
		}

		public string GetCategoryNiceUrl(ICategory category, string storeAlias = null)
		{
			var store = GetStoreByAliasOrCurrentStore(storeAlias);

			var storeAliasUrl = store.StoreURL.TrimEnd('/') + "/"; //GetStoreAliasUrl();


			if (!string.IsNullOrEmpty(storeAlias))
			{
				category = DomainHelper.GetCategoryById(category.Id, storeAlias);
			}

			if (category != null)
			{
				var currentCategoryUrl = category.UrlName;
				while (category.ParentCategory != null)
					// (category.ParentNodeAlias.StartsWith(Category.NodeAlias) && category.ParentNodeAlias != Catalog.CategoryRepositoryNodeAlias) => alternatief is iets sneller wanneer er veel categories zijn
				{
					category = category.ParentCategory; // DomainHelper.GetCategoryById(category.ParentId, storeAlias); // cache
					currentCategoryUrl = String.Format("{0}/{1}", category.UrlName, currentCategoryUrl);
				}
				var resultUrl = !String.IsNullOrEmpty(storeAliasUrl)
					? String.Format("{0}{1}", storeAliasUrl, currentCategoryUrl.TrimStart('/'))
					: currentCategoryUrl;

				return FixNiceUrl(resultUrl);
			}

			return string.Empty;
		}

		public void RenameStore(string oldStoreAlias, string newStoreAlias)
		{
			if (String.IsNullOrEmpty(oldStoreAlias) || String.IsNullOrEmpty(newStoreAlias)) return;

			var docTypeList = new List<DocumentType>();

			// get all documenttypes that startswith the store specific doctypes
			foreach (var storeDependantAlias in StoreHelper.StoreDependantDocumentTypeAliasList)
			{
				docTypeList.AddRange(DocumentType.GetAllAsList().Where(x => x.Alias.StartsWith(storeDependantAlias)));
			}
			
			foreach (var dt in docTypeList)
			{
				// rename properties
				foreach (var property in dt.PropertyTypes)
				{
					property.Alias = property.Alias.Replace("_" + oldStoreAlias, "_" + newStoreAlias);
					property.Save();
				}
				
				// rename tabs + move properties to new tab
				foreach (var tab in dt.getVirtualTabs)
				{
					if(tab.Caption == oldStoreAlias)
					{
						dt.SetTabName(tab.Id, newStoreAlias);
					}
				}

				dt.Save();
			}
			
			library.RefreshContent();
			ExamineManager.Instance.IndexProviderCollection[UwebshopConfiguration.Current.ExamineSearcher].RebuildIndex();
		}

		private string FixNiceUrl(string resultUrl)
		{
			if (_cmsApplication.AddTrailingSlash && !resultUrl.EndsWith("/"))
			{
				resultUrl += "/";
			}
			if (resultUrl.StartsWith("//"))
			{
				resultUrl = resultUrl.Substring(1);
			}
			if (!resultUrl.StartsWith("/") && !resultUrl.StartsWith("http"))
			{
				resultUrl = resultUrl.Insert(0, "/");
			}
			return IO.Container.Resolve<ISettingsService>().UseLowercaseUrls ? resultUrl.ToLower() : resultUrl;
		}

		private string GetUrlPart(UwbsNode node)
		{
			return (node.Level == 1 && _cmsApplication.HideTopLevelNodeFromPath ? _cmsApplication.GetDomainForNodeId(node.Id) ?? "" : node.UrlName ?? "").Trim('/');
		}

		public string GetNiceUrl(int id, int categoryId, string storeAlias)
		{
			var store = GetByAlias(storeAlias) ?? GetCurrentStore();
			if (store == null) throw new Exception("No published stores, please publish");

			// todo: domain, including or not and when?
			string storeAliasUrl = store.StoreURL.TrimEnd('/'); //GetStoreAliasUrl();

			var uwebshopNode = IO.Container.Resolve<ICMSEntityRepository>().GetByGlobalId(id);
			if (uwebshopNode == null) return string.Empty;

			var typeAlias = uwebshopNode.NodeTypeAlias;
			if (Product.IsAlias(typeAlias))
			{
				return FixNiceUrl(GetLocalizedProductUrlHelper(DomainHelper.GetProductById(id, storeAlias), categoryId, storeAliasUrl, store.Alias));
			}
			if (Category.IsAlias(typeAlias))
			{
				return FixNiceUrl(GetCategoryNiceUrl(DomainHelper.GetCategoryById(id, storeAlias), storeAlias));
			}
			return FixNiceUrl(library.NiceUrl(id));
		}
	}
}