﻿namespace uWebshop.Umbraco.Interfaces
{
	interface ICMSChangeContentService
	{
		ICMSContent GetById(int nodeId);
	}
}
