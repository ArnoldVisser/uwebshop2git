﻿namespace uWebshop.Umbraco.Interfaces
{
	interface ICMSContent
	{
		void SetValue(string propertyAlias, string value);
		void SaveAndPublish();
		string ContentTypeAlias { get; }
		bool HasProperty(string key);
	}
}