﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using umbraco.BusinessLogic;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco.Repositories;
using uWebshop.Umbraco.Services;

namespace uWebshop.Umbraco
{
	internal static class RegisterTypes
	{
		internal static bool VersionSpecificTypesConfiguredInIOCContainer = false;

		public static void Register(bool testMode = false)
		{
			Domain.RegisterTypes.Register();

			// entity services (will need to move to Domain)
			IO.Container.RegisterType<IStoreService, UmbracoStoreService>();  // todo: move to Domain
			IO.Container.RegisterType<IZoneService, UmbracoZoneService>();
			
			IO.Container.RegisterType<ICatalogUrlSplitterService, ExamineCatalogUrlSplitterService>();
			IO.Container.RegisterType<IRebuildIndicesService, ExamineRebuildIndicesService>();
			IO.Container.RegisterType<ICMSApplication, UmbracoApplication>();
			IO.Container.RegisterType<IApplicationCacheManagingService, UmbracoApplicationCacheManagingService>();
			IO.Container.RegisterType<ICMSDocumentTypeService, UmbracoDocumentTypeService>();
			IO.Container.RegisterType<IAuthenticationProvider, UmbracoDotnetMembershipAuthenticationProvider>();

			IO.Container.RegisterType<IStoreRepository, UmbracoStoreRepository>();
			IO.Container.RegisterType<IPaymentProviderRepository, UmbracoPaymentProviderRepository>();
			IO.Container.RegisterType<IShippingProviderRepository, UmbracoShippingProviderRepository>();
			IO.Container.RegisterType<ICMSEntityRepository, UmbracoStaticCachedEntityRepository>();
			IO.Container.RegisterType<IDiscountRepository, UmbracoDiscountRepository>();
			IO.Container.RegisterType<IProductDiscountRepository, UmbracoProductDiscountRepository>();
			IO.Container.RegisterType<IProductRepository, UmbracoProductRepository>();
			IO.Container.RegisterType<IProductVariantRepository, UmbracoProductVariantRepository>();
			IO.Container.RegisterType<ICategoryRepository, UmbracoCategoryRepository>();

			IO.Container.RegisterType<ICMSContentService, CMSContentService>();

			//IO.Container.RegisterType<ISettingsService, Settings>();
			try
			{
				if (!testMode)
					IO.Container.RegisterInstance<ISettingsService>(SettingsLoader.GetSettings());
			}
			catch
			{
				IO.Container.RegisterInstance<ISettingsService>(new SettingsProxy());
			}

			IO.Container.RegisterType<IHttpContextWrapper, uWebshop.Domain.Businesslogic.HttpContextWrapper>();
			if (!testMode)
				LoadVersionSpecificTypes();
		}

		private static void LoadVersionSpecificTypes()
		{
			try
			{
				var targetType = typeof(IUmbracoTypeRegisteringService);
				var path = HttpContext.Current.Server.MapPath("/bin");

				var files = Directory.GetFiles(path);
				var dlls = files.Select(filepath => new FileInfo(filepath))
								.Where(fileInfo => fileInfo.Name.StartsWith("uWebshop.") && fileInfo.Name.EndsWith(".dll")).ToList();
				if (dlls.Count == 0) throw new Exception("uWebshop dlls not found");
				var assemblies = dlls.Select(fileInfo => Assembly.LoadFrom(fileInfo.FullName))
				                     .Where(assembly => assembly != null).ToList();
				if (assemblies.Count == 0) throw new Exception("uWebshop dlls not loaded");

				var registeringServices = assemblies.SelectMany(assembly => assembly.GetTypes()).Where(type => targetType.IsAssignableFrom(type) && targetType != type)
				                                    .Select(type => (IUmbracoTypeRegisteringService) Activator.CreateInstance(type)).ToList();

				if (!registeringServices.Any()) throw new Exception("uWebshop Umbraco version specific dll's not found");
				if (registeringServices.Skip(1).Any()) throw new Exception("uWebshop Umbraco version specific dll's of both v4 and v6 found, please remove the incompatible dll");
				registeringServices.First().Register(IO.Container);
				VersionSpecificTypesConfiguredInIOCContainer = true;
			}
			catch (Exception ex)
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " failure loading umbraco version specific dll " + ex.Message);
				// NB: Logging kan hier niet omdat dat juist verkeerd is gegaan!
				//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " failure loading umbraco version specific dll");
				throw;
			}
		}
	}

	public class SettingsProxy : ISettingsService
	{
		private ISettingsService LoadActualSettings()
		{
			try
			{
				var settings = SettingsLoader.GetSettings();
				IO.Container.RegisterInstance<ISettingsService>(settings);
				return settings;
			}
			catch (Exception)
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + "Could not load Settings. Please republish nodes.");
				//Log.Instance.LogError("Could not load Settings. Please republish nodes.");
			}
			return null;
		}
		public bool IncludingVat
		{
			get
			{
				var settings = LoadActualSettings();
				return settings != null && settings.IncludingVat;
			}
		}

		public bool UseLowercaseUrls
		{
			get
			{
				var settings = LoadActualSettings();
				return settings != null && settings.UseLowercaseUrls;
			}
		}

		public int IncompleOrderLifetime
		{
			get
			{
				var settings = LoadActualSettings();
				return settings != null ? settings.IncompleOrderLifetime : 360;
			}
		}
	}

	public interface IUmbracoTypeRegisteringService
	{
		void Register(IIocContainer container);
	}

}
