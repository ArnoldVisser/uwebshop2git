﻿using System;
using System.Linq;
using System.Threading;
using System.Web.UI;
using uWebshop.Domain;
using uWebshop.Domain.Businesslogic;
using uWebshop.Domain.Helpers;
using System.Web;

namespace uWebshop.Web.Usercontrols
{
	public partial class UwbsBasketHandler : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			
			if (IsPostBack)
			{
				return;
			}


			try
			{
				var redirectAfterHandle = new BasketRequestHandler().HandleuWebshopBasketRequest(Request.Params);
				if ((Request.Params.AllKeys.Any() && (!redirectAfterHandle || Request.Params.AllKeys.Any(x => x == "disableReload")))) return;
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception ex)
			{
				Log.Instance.LogError(ex, "Unhandled exception during execution of uWebshop basket handler");
			}


			if (!string.IsNullOrEmpty(HttpContext.Current.Request.RawUrl))
			{
				Response.Redirect(HttpContext.Current.Request.RawUrl, false);
				return;
			}

			if (HttpContext.Current.Request.UrlReferrer != null)
			{
				Response.Redirect(HttpContext.Current.Request.UrlReferrer.AbsolutePath, false);
			}
		}
	}
}