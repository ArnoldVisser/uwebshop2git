﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco.NodeFactory;

namespace uWebshop.Web.WebControls
{
	/// <summary>
	/// This usercontrol should be placed on each payment provider
	/// It handles the payment response from the payment provider.
	/// </summary>
	public partial class UwbsPaymentHandler : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			var paymentProvider = HttpContext.Current.Request["paymentprovider"];

			if (string.IsNullOrEmpty(paymentProvider))
			{
				Log.Instance.LogDebug("UwbsPaymentHandler: PaymentProvider Not Found");
				throw new Exception("PaymentProvider via request Not Found");
			}

			var paymentProviderNode = IO.Container.Resolve<IPaymentProviderService>().GetPaymentProviderWithName(paymentProvider, StoreHelper.CurrentStoreAlias);
			
			if (paymentProviderNode == null)
			{
				Log.Instance.LogDebug("UwbsPaymentHandler: PaymentProvider " + paymentProvider + " Not Found");
				throw new Exception("PaymentProvider: " + paymentProvider + " Not Found.");
			}

			new PaymentRequestHandler().HandleuWebshopPaymentRequest(paymentProviderNode.Id);
		}
	}
}