﻿using System.Linq;
using System.Web.Security;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoProductDiscountRepository : IProductDiscountRepository
	{
		// todo: dit is sowieso niet goed, is functionaliteit voor de service, niet de repo
		public DiscountProduct GetDiscountByProductId(int productId, string storeAlias)
		{
			return DomainHelper.GetObjectsByAlias<DiscountProduct>(DiscountProduct.NodeAlias, storeAlias)
							   .Where(discount => discount.IsActive && 
								   discount.Items.Contains(productId) && 
								   (!discount.MemberGroups.Any() || 
									Membership.GetUser() != null && discount.MemberGroups.Intersect(Roles.GetRolesForUser(Membership.GetUser().UserName)).Any()))
				// todo: below can give some unexpected behaviour since GetDiscountAmountInCents doens't take ranges into account
									.OrderByDescending(discount => discount.GetDiscountAmountInCents(productId)).FirstOrDefault();
		}

		public DiscountProduct GetById(int discountId, string storeAlias)
		{
			return DomainHelper.GetObjectsByAlias<DiscountProduct>(DiscountProduct.NodeAlias, storeAlias).FirstOrDefault(d => d.Id == discountId);
		}
	}
}
