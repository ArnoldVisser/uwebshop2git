﻿using System;
using System.Collections.Generic;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoShippingProviderRepository : UmbracoMultiStoreEntityRepository<ShippingProvider>, IShippingProviderRepository
	{
		private readonly IZoneService _zoneService;
		private readonly IStoreService _storeService;

		public UmbracoShippingProviderRepository(IZoneService zoneService, IStoreService storeService)
		{
			_zoneService = zoneService;
			_storeService = storeService;
		}

		public override void LoadDataFromPropertiesDictionary(ShippingProvider entity, IPropertyProvider fields, string storeAlias)
		{
			var store = _storeService.GetByAlias(storeAlias);

			entity.Title = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("title", storeAlias, fields) ?? string.Empty;
			entity.Description = IO.Container.Resolve<ICMSApplication>().ParseInternalLinks(StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("description", storeAlias, fields)) ?? string.Empty;
			
			var testMode = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("testMode", storeAlias, fields);
			if (testMode == "default" || testMode == String.Empty)
			{
				entity.TestMode = store.EnableTestmode;
			}
			else
			{
				entity.TestMode = testMode == "enable" || testMode == "1" || testMode == "true";
			}

			entity.ImageId = StoreHelper.GetMultiStoreIntValue("image", storeAlias, fields);
			entity.Zone = _zoneService.GetByIdOrFallbackZone(StoreHelper.GetMultiStoreIntValue("zone", storeAlias, fields), storeAlias);
			var shippingProviderType = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("type", storeAlias, fields);
			entity.Type = (ShippingProviderType)Enum.Parse(typeof(ShippingProviderType), shippingProviderType);
			var shippingRangeType = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("rangeType", storeAlias, fields);
			entity.TypeOfRange = (ShippingRangeType)Enum.Parse(typeof(ShippingRangeType), shippingRangeType);
			entity.Overrule = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("overrule", storeAlias, fields) == "1";
			var vat = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("vat", storeAlias, fields);
			decimal vatPercentage = 0;
			if (string.IsNullOrWhiteSpace(vat) || !decimal.TryParse(vat, out vatPercentage))
			{
				vatPercentage = store.GlobalVat;
			}
			entity.Vat = vatPercentage;
			var dllName = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("dllName", storeAlias, fields);
			if (!string.IsNullOrEmpty(dllName))
			{
				entity.DLLName = !dllName.EndsWith(".dll") ? string.Format("{0}.dll", dllName) : dllName;
			}

			entity.RangeFrom = StoreHelper.GetMultiStoreDecimalValue("rangeStart", storeAlias, fields);
			entity.RangeTo = StoreHelper.GetMultiStoreDecimalValue("rangeEnd", storeAlias, fields);
			if (entity.RangeTo == 0) entity.RangeTo = decimal.MaxValue;
		}

		public override string TypeAlias
		{
			get { return ShippingProvider.NodeAlias; }
		}
	}
}