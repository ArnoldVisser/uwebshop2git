﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco.Businesslogic;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoCategoryRepository : UmbracoMultiStoreEntityRepository<Category>, ICategoryRepository
	{
		// watch out: it's not possible to use the CategoryService here, since the IOCContainer will loop (currently)

		public override void LoadDataFromPropertiesDictionary(Category category, IPropertyProvider fields, string storeAlias)
		{
			category.StoreAlias = storeAlias;
			category.ClearCachedValues(); // (hack to reload relations)

			// todo: hmmmmmm.... (tijdelijk zo, maar hoe structureler?)
			MultiStoreUwebshopContent.LoadDataFromPropertyProvider(category, storeAlias, fields);

			// todo: refactor below to set simpel properties on Category instead of internal fields that are used by product to make the actual properties

			var imagesProperty = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("images", storeAlias, fields);
			category.Images = DomainHelper.ParseIntegersFromUwebshopProperty(imagesProperty).Select(InternalHelpers.LoadImageWithId).ToList();
			
			// todo: load categories from service (using stubs???)
			var values = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("categories", storeAlias, fields);
			category._categoryIds = DomainHelper.ParseIntegersFromUwebshopProperty(values).ToList();
			category.HasCategories = !string.IsNullOrEmpty(values);

			var tagsValue = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("metaTags", storeAlias, fields);
			category.Tags = InternalHelpers.ParseTagsString(tagsValue);

			category.ProductsFactory = () => DomainHelper.GetAllProducts(false, storeAlias).Where(product => product.ParentId == category.Id || product.HasCategories && product.Categories.Any(cat => cat.Id == category.Id)).ToList();
			category.Disabled = StoreHelper.GetMultiStoreDisableExamine(storeAlias, fields);
		}

		public override string TypeAlias
		{
			get { return Category.NodeAlias; }
		}
	}
}
