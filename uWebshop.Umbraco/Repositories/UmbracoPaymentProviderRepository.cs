﻿using System;
using System.Collections.Generic;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoPaymentProviderRepository : UmbracoMultiStoreEntityRepository<PaymentProvider>, IPaymentProviderRepository
	{
		private readonly IZoneService _zoneService;
		private readonly IStoreService _storeService;

		public UmbracoPaymentProviderRepository(IZoneService zoneService, IStoreService storeService)
		{
			_zoneService = zoneService;
			_storeService = storeService;
		}

		public override void LoadDataFromPropertiesDictionary(PaymentProvider entity, IPropertyProvider fields, string storeAlias)
		{
			var store = _storeService.GetByAlias(storeAlias);

			entity.Title = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("title", storeAlias, fields) ?? string.Empty;
			entity.Description = IO.Container.Resolve<ICMSApplication>().ParseInternalLinks(StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("description", storeAlias, fields)) ?? string.Empty;
		
			var testMode = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("testMode", storeAlias, fields);
			if (testMode == "default" || testMode == String.Empty)
			{
				entity.TestMode = store.EnableTestmode;
			}
			else
			{
				entity.TestMode = testMode == "enable" || testMode == "1" || testMode == "true";
			}
			
			entity.ImageId = StoreHelper.GetMultiStoreIntValue("image", storeAlias, fields);
			entity.Zone = _zoneService.GetByIdOrFallbackZone(StoreHelper.GetMultiStoreIntValue("zone", storeAlias, fields), storeAlias);
			var paymentProviderType = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("type", storeAlias, fields);
			entity.Type = (PaymentProviderType) Enum.Parse(typeof (PaymentProviderType), paymentProviderType);
			var dllName = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("dllName", storeAlias, fields);
			if (!string.IsNullOrEmpty(dllName))
			{
				entity.DLLName = !dllName.EndsWith(".dll") ? string.Format("{0}.dll", dllName) : dllName;
			}
			var vat = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("vat", storeAlias, fields);
			decimal vatPercentage = 0;
			if (string.IsNullOrWhiteSpace(vat) || !decimal.TryParse(vat, out vatPercentage))
			{
				if (store != null) vatPercentage = store.GlobalVat;
			}
			entity.Vat = vatPercentage;
			entity.ControlNodeId = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("controlNode", storeAlias, fields);
			entity.SuccesNodeId = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("successNode", storeAlias, fields);
			entity.ErrorNodeId = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("errorNode", storeAlias, fields);
		}

		public override string TypeAlias
		{
			get { return PaymentProvider.NodeAlias; }
		}
	}
}