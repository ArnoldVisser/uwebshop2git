﻿using uWebshop.Domain.Interfaces;
using umbraco.NodeFactory;

namespace uWebshop.Umbraco.Repositories
{
	public class UmbracoNodePropertyProvider : IPropertyProvider
	{
		private readonly Node _node;

		public UmbracoNodePropertyProvider(Node node)
		{
			_node = node;
		}

		public bool ContainsKey(string property)
		{
			var prop = _node.GetProperty(property);
			return prop != null;
		}

		public bool UpdateValueIfPropertyPresent(string property, ref string value)
		{
			var prop = _node.GetProperty(property);
			if (prop != null)
			{
				value = prop.Value;
				return true;
			}
			return false;
		}

		public string GetStringValue(string property)
		{
			var prop = _node.GetProperty(property);
			if (prop != null)
			{
				return prop.Value;
			}
			return null;
		}
	}
}