﻿using System;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco.Businesslogic;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoProductRepository : UmbracoMultiStoreEntityRepository<Product>, IProductRepository
	{
		private readonly ISettingsService _settingsService;
		private readonly IStoreService _storeService;
		private readonly ICategoryService _categoryService;
		private readonly IProductDiscountRepository _productDiscountRepository;
		private readonly IProductVariantService _variantService;

		public UmbracoProductRepository(ISettingsService settingsService, IStoreService storeService, ICategoryService categoryService, IProductDiscountRepository productDiscountRepository, IProductVariantService variantService)
		{
			_settingsService = settingsService;
			_storeService = storeService;
			_categoryService = categoryService;
			_productDiscountRepository = productDiscountRepository;
			_variantService = variantService;
		}

		public override void LoadDataFromPropertiesDictionary(Product product, IPropertyProvider fields, string storeAlias)
		{
			product.StoreAlias = storeAlias;

			var store = _storeService.GetByAlias(storeAlias);

			product.ClearCachedValues(); // (hack to reload relations)

			product.PricesIncludingVat = _settingsService.IncludingVat;

			//product.ProductDiscount = _productDiscountRepository.GetDiscountByProductId(product.Id);
			//product.IsDiscounted = product.ProductDiscount != null;

			// todo: hmmmmmm.... (tijdelijk zo, maar hoe structureler?)
			MultiStoreUwebshopContent.LoadDataFromPropertyProvider(product, storeAlias, fields);


			product.SKU = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("sku", storeAlias, fields);

			// todo: refactor below to set simpel properties on Product instead of internal fields that are used by product to make the actual properties

			var imagesProperty = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("images", storeAlias, fields);
			product.Images = DomainHelper.ParseIntegersFromUwebshopProperty(imagesProperty).Select(InternalHelpers.LoadImageWithId).Where(x => x != null).ToList();

			var filesProperty = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("files", storeAlias, fields);
			product.Files = DomainHelper.ParseIntegersFromUwebshopProperty(filesProperty).Select(InternalHelpers.LoadFileWithId).Where(x => x !=null).ToList();

			// todo: load categories from service
			var values = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("categories", storeAlias, fields);
			product.CategoryIds = DomainHelper.ParseIntegersFromUwebshopProperty(values).ToList();
			if (!product.CategoryIds.Contains(product.ParentId)) product.CategoryIds = new List<int>{product.ParentId}.Concat(product.CategoryIds).ToList();
			
			product.HasCategories = !string.IsNullOrEmpty(values);

			product.OriginalPriceInCents = StoreHelper.GetMultiStoreIntValue("price", storeAlias, fields);

			product.TotalItemsOrdered = StoreHelper.GetMultiStoreIntValue("ordered", storeAlias, fields);

			product.Weight = StoreHelper.GetMultiStoreDoubleValue("weight", storeAlias, fields);
			product.Width = StoreHelper.GetMultiStoreDoubleValue("width", storeAlias, fields);
			product.Length = StoreHelper.GetMultiStoreDoubleValue("length", storeAlias, fields);
			product.Height = StoreHelper.GetMultiStoreDoubleValue("height", storeAlias, fields);

			var vatProperty = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("vat", storeAlias, fields);
			decimal vat;
			if (!string.IsNullOrEmpty(vatProperty) && decimal.TryParse(vatProperty, out vat))
				product.Vat = vat;
			else
			{	product.Vat = store.GlobalVat;
			}

			var stockStatus = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("stockStatus", storeAlias, fields);
			if (stockStatus == "default" || stockStatus == String.Empty)
			{
				product.StockStatus = store.UseStock;
			}
			else
			{
				product.StockStatus = stockStatus == "enable" || stockStatus == "1" || stockStatus == "true";
			}
			var backorderStatus = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("backorderStatus", storeAlias, fields);
			if (backorderStatus == "default" || backorderStatus == String.Empty)
			{
				product.BackorderStatus = store.UseBackorders;
			}
			else
			{
				product.BackorderStatus = backorderStatus == "enable" || backorderStatus == "1" || backorderStatus == "true";
			}

			var useVariantStock = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("useVariantStock", storeAlias, fields);
			product.UseVariantStock = useVariantStock == "enable" || useVariantStock == String.Empty;

			var tagsValue = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("metaTags", storeAlias, fields);
			product.Tags = InternalHelpers.ParseTagsString(tagsValue);

			var rangesString = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("ranges", storeAlias, fields);
			product.Ranges = Range.CreateFromString(rangesString);

			// todo: tricky en misschien niet correct (niet in repo)
			product.Variants = _variantService.GetAll(storeAlias).Where(variant => variant.ParentId == product.Id).ToList();

			// todo: disabled depends on relation Categories, which cannot be loaded at this point, Lazy<bool> or value factory?
			product.Disabled = StoreHelper.GetMultiStoreDisableExamine(storeAlias, fields) || product.Categories.Any() && product.Categories.All(category => category.Disabled);// logic is a tiny bit unsure
		}

		public override string TypeAlias
		{
			get { return Product.NodeAlias; }
		}

		//// nice xml fallback for loading products under a category
		//var iterator = library.GetXmlNodeByXPath(String.Format("//*[starts-with(name(),'" + Product.NodeAlias + "') and not(starts-with(name(),'" + Product.NodeAlias + "Repository'))]|/categories[contains(.,'" + categoryId + "')] | //*[starts-with(name(),'" + Product.NodeAlias + "') and not(starts-with(name(),'" + Product.NodeAlias + "Repository')) and @parentID='" + categoryId + "']"));
		//var objects = new List<Product>();
		//while (iterator.MoveNext())
		//{
		//	if (iterator.Current == null) continue;
		//	var nodeId = Convert.ToInt32(iterator.Current.GetAttribute("id", iterator.Current.NamespaceURI));

		//	var constructorInfo = typeof (Product).GetConstructor(new[] {typeof (int)});
		//	if (constructorInfo != null)
		//		objects.Add((Product) constructorInfo.Invoke(new object[] {nodeId}));
		//}
		//return objects.Where(product => !product.Disabled).AsQueryable();
	}

	class ProductRepoData
	{
		public Product Product { get; set; }
		public bool VatLoaded { get; set; }
		public List<int> CategoryIds { get; set; }
		public bool ProductDisabled { get; set; }
	}

	class DeslegteProductRepository : IProductRepository
	{
		public Product GetById(int id, string storeAlias)
		{
			throw new NotImplementedException();
		}

		public List<Product> GetAll(string storeAlias)
		{
			throw new NotImplementedException();
		}

		public void ReloadData(Product product, string storeAlias)
		{
			throw new NotImplementedException();
		}
	}



	class ProductProxy : Product
	{
		public override bool Disabled
		{
			get
			{
				return base.Disabled;
			}
			set
			{
				base.Disabled = value;
			}
		}

		public override System.Collections.Generic.IEnumerable<ICategory> Categories
		{
			get
			{
				return base.Categories;
			}
			set
			{
				base.Categories = value;
			}
		}
		
	}

}
