﻿using System;
using System.Collections.Generic;
using System.Linq;
using Examine;
using uWebshop.Domain;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Interfaces;
using umbraco.NodeFactory;

namespace uWebshop.Umbraco.Repositories
{
	internal abstract class UmbracoMultiStoreEntityRepository<T> : IEntityRepository<T> where T : uWebshopEntity
	{
		public T GetById(int id, string storeAlias)
		{
			return CreateEntityFromNode(new Node(id), storeAlias);
		}

		public List<T> GetAll(string storeAlias)
		{
			//Log.Instance.LogDebug("Reloading all entities of type "+ TypeAlias + " for store "+storeAlias);

			var examineResults = UmbracoStaticCachedEntityRepository.GetExamineResultsForNodeTypeAlias(TypeAlias);
			if (examineResults != null && examineResults.Any()) return examineResults.Select(e => CreateEntityFromExamineData(e, storeAlias)).ToList();
			return UmbracoStaticCachedEntityRepository.GetNodeIdsFromXMLStoreForNodeTypeAlias(TypeAlias).Select(id => CreateEntityFromNode(new Node(id), storeAlias)).Where(e => e != null).ToList();
		}

		private T CreateEntityFromExamineData(SearchResult examineNode, string storeAlias)
		{
			if (examineNode == null) throw new Exception("Trying to load data from null examine SearchResult");

			if (!examineNode.Fields.ContainsKey("id") || string.IsNullOrEmpty(examineNode.Fields["id"]))
				throw new Exception("Trying to load data from null examine SearchResult without id field. Actual fields: {" + string.Join(", ", examineNode.Fields.Select(a => a.Key + " = " + a.Value)) + "}");

			var entity = Activator.CreateInstance<T>();
			var fields = new DictionaryPropertyProvider(examineNode);
			entity.NodeTypeAlias = TypeAlias;
			entity.LoadFieldsFromExamine(fields);
			LoadDataFromPropertiesDictionary(entity, fields, storeAlias);
			return entity;
		}

		internal T CreateEntityFromNode(Node node, string storeAlias)
		{
			if (node == null) throw new Exception("Trying to load data from null node");
			if (node.NodeTypeAlias == null || (node.NodeTypeAlias != TypeAlias && !node.NodeTypeAlias.StartsWith(TypeAlias))) return null;

			var entity = Activator.CreateInstance<T>();
			LoadDataFromNode(entity, node, storeAlias);
			return entity;
		}

		internal void LoadDataFromNode(T entity, Node node, string storeAlias)
		{
			Helpers.LoadUwebshopEntityPropertiesFromNode(entity, node);
			LoadDataFromPropertiesDictionary(entity, new UmbracoNodePropertyProvider(node), storeAlias);
		}

		public void ReloadData(T entity, string storeAlias)
		{
			LoadDataFromNode(entity, new Node(entity.Id), storeAlias);
		}

		public abstract void LoadDataFromPropertiesDictionary(T entity, IPropertyProvider fields, string storeAlias);
		public abstract string TypeAlias { get; }
	}
}