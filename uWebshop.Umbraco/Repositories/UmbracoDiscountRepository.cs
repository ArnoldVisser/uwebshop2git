﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoDiscountRepository : IDiscountRepository
	{
		private readonly ICMSEntityRepository _cmsEntityRepository;

		public UmbracoDiscountRepository(ICMSEntityRepository cmsEntityRepository)
		{
			_cmsEntityRepository = cmsEntityRepository;
		}

		public List<IOrderDiscount> GetAll()
		{
			return new List<IOrderDiscount>(_cmsEntityRepository.GetObjectsByAlias<DiscountOrder>(DiscountOrder.NodeAlias).ToList());
		}

		public IOrderDiscount GetById(int id)
		{
			return GetAll().FirstOrDefault(discount => discount.OriginalId == id);
		}
	}
}
