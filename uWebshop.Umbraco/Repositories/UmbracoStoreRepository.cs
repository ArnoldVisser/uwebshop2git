﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Lucene.Net.Documents;
using umbraco.cms.businesslogic.language;
using umbraco.cms.businesslogic.web;
using umbraco.NodeFactory;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using Document = umbraco.cms.businesslogic.web.Document;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoStoreRepository : IStoreRepository
	{
		private readonly ICMSApplication _cmsApplication;
		private readonly ICMSEntityRepository _cmsEntityRepository;
		private readonly UmbracoStoreRepo _storeRepo;

		public UmbracoStoreRepository(ICMSApplication cmsApplication, ICMSEntityRepository cmsEntityRepository)
		{
			_cmsApplication = cmsApplication;
			_cmsEntityRepository = cmsEntityRepository;
			_storeRepo= new UmbracoStoreRepo();
		}

		public List<Store> GetAll()
		{
			return _storeRepo.GetAll(Constants.NonMultiStoreAlias);
			//return _cmsEntityRepository.GetObjectsByAliasUncached<Store>(Store.NodeAlias, Constants.NonMultiStoreAlias).ToList();
		}

		public Store TryGetStoreFromBackendCurrentOrder()
		{
			// Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore 1");
			var idFromQueryString = HttpContext.Current.Request.QueryString["id"];
			if (idFromQueryString != null)
			{
				var documentId = Int32.Parse(idFromQueryString);

				var uwbsNode = IO.Container.Resolve<ICMSEntityRepository>().GetByGlobalId(documentId);

				if (uwbsNode == null || (uwbsNode.NodeTypeAlias != Order.NodeAlias && (!OrderedProduct.IsAlias(uwbsNode.NodeTypeAlias) || OrderedProductVariant.IsAlias(uwbsNode.NodeTypeAlias))))
					return null;
				
				var doc = new Document(documentId);
				//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore 2");
				if (doc.ContentType != null && (doc.ContentType.Alias == Order.NodeAlias || doc.ContentType.Alias.StartsWith(OrderedProduct.NodeAlias)))
				{
					if (doc.ContentType.Alias.StartsWith(OrderedProduct.NodeAlias) && !doc.ContentType.Alias.StartsWith(ProductVariant.NodeAlias))
						doc = new Document(doc.ParentId);
					//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore 3");
					//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore >>>>SQL<<<< SELECT orderInfo");

					// todo: 
					var xml = uWebshopOrders.GetOrderInfo(Guid.Parse(doc.getProperty("orderGuid").Value.ToString())).OrderXML;
					var storeInfoLoc = xml.IndexOf("<StoreInfo>");
					var storeAliasLoc = xml.IndexOf("<Alias>", storeInfoLoc) + 7;
					var storeAliasEndLoc = xml.IndexOf("</Alias>", storeInfoLoc);
					// Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore 4");
					var alias = xml.Substring(storeAliasLoc, storeAliasEndLoc - storeAliasLoc); // this is because you can't use orderInfo.StoreInfo.Store
					if (!String.IsNullOrEmpty(alias))
					{
						var store = StoreHelper.GetByAlias(alias);
						if (store != null && !String.IsNullOrEmpty(store.Alias))
						{
							//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore 5");
							return store;
						}
					}
					throw new Exception("Store could not be found, was the alias changed?");
				}
			}
			return null;
		}

		public Store TryGetStoreFromCurrentNode()
		{
			if (_cmsApplication.RequestIsInCMSBackend(HttpContext.Current)) return null;
			try
			{
				Node n = Node.GetCurrent();
				if (n != null)
				{
					while (n.Parent != null && (n.GetProperty(Constants.StorePickerAlias) == null || String.IsNullOrEmpty(n.GetProperty(Constants.StorePickerAlias).Value)))
						n = new Node(n.Parent.Id);
					//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore 7");
					var pickerProperty = n.GetProperty(Constants.StorePickerAlias);
					if (pickerProperty != null && pickerProperty.Value != "0")
					{
						var value = pickerProperty.Value;
						if (!String.IsNullOrEmpty(value) && value != "0")
						{
							//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore maakt new Store object (=Examine call)");
							var currentStore = GetById(Convert.ToInt32(value)); // dit kan nasty zijn omdat de properties via GetMultiStore deze functie aan kunnen roepen
							if (String.IsNullOrEmpty(currentStore.Alias)) throw new Exception("Failed to load store data");
							//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " StoreHelper.GetCurrentStore 8");
							if (currentStore.NodeTypeAlias == Store.NodeAlias)
							{
								return currentStore;
							}
						}
					}
				}
			}
			catch
			{
			}
			return null;
		}

		public Store GetById(int id)
		{
			var store = new Store();
			_storeRepo.LoadDataFromNode(store, new Node(id), Constants.NonMultiStoreAlias);
			return store;
		}

		private class UmbracoStoreRepo : UmbracoMultiStoreEntityRepository<Store>
		{
			public override void LoadDataFromPropertiesDictionary(Store store, IPropertyProvider fields, string storeAlias)
			{
				// note: it's impossible to use StoreHelper.GetMultiStoreItemExamine here (or any multi store)

				if (fields.ContainsKey("incompleteOrderLifetime"))
				{
					double incompleteOrderLifetime;
					if (double.TryParse(fields.GetStringValue("incompleteOrderLifetime"), out incompleteOrderLifetime))
						store.IncompleOrderLifetime = incompleteOrderLifetime;
				}
				if (fields.ContainsKey("globalVat"))
				{
					decimal globalVat;
					if (decimal.TryParse(fields.GetStringValue("globalVat"), out globalVat))
						store.GlobalVat = globalVat;
				}
				if (fields.ContainsKey("storeCulture"))
				{
					var culture = fields.GetStringValue("storeCulture");
					int languageId = 0;

					if (culture != null) int.TryParse(culture, out languageId);

					store.Culture = languageId != 0 ? new Language(languageId).CultureAlias : string.Empty;
				}
				if (fields.ContainsKey("countryCode"))
				{
					store.CountryCode = fields.GetStringValue("countryCode");
				}
				if (fields.ContainsKey("defaultCountryCode"))
				{
					store.DefaultCountryCode = fields.GetStringValue("defaultCountryCode");
				}
				if (fields.ContainsKey("currencyCulture"))
				{
					var culture = fields.GetStringValue("currencyCulture");
					int languageId = 0;

					if (culture != null) int.TryParse(culture, out languageId);

					store.CurrencyCulture = languageId != 0 ? new Language(languageId).CultureAlias : string.Empty;
				}
				if (fields.ContainsKey("orderNumberPrefix"))
				{
					store.OrderNumberPrefix = fields.GetStringValue("orderNumberPrefix");
				}
				if (fields.ContainsKey("orderNumberTemplate"))
				{
					store.OrderNumberTemplate = fields.GetStringValue("orderNumberTemplate");
				}
				if (fields.ContainsKey("orderNumberStartNumber"))
				{
					int orderNumberStartNumber;
					if (int.TryParse(fields.GetStringValue("orderNumberStartNumber"), out orderNumberStartNumber))
						store.OrderNumberStartNumber = orderNumberStartNumber;
				}
				if (fields.ContainsKey("enableStock"))
				{
					string enableStockValue = fields.GetStringValue("enableStock");
					store.UseStock = enableStockValue == "enable" || enableStockValue == "1" || enableStockValue == "true";
				}
				if (fields.ContainsKey("storeStock"))
				{
					string storeStockValue = fields.GetStringValue("storeStock");
					var value = storeStockValue == "enable" || storeStockValue == "1" || storeStockValue == "true";
					if (value)
					{
						DocumentType productDt = DocumentType.GetByAlias(Product.NodeAlias);
						if (!productDt.PropertyTypes.Any(x => x.Alias.ToLower() == "stock_" + store.Alias.ToLower()))
						{
							value = false;
						}
					}
					store.UseStoreSpecificStock = value;
				}

				if (fields.ContainsKey("useBackorders"))
				{
					string useBackorders = fields.GetStringValue("useBackorders");
					
					store.UseBackorders = useBackorders == "enable" || useBackorders == "1" || useBackorders == "true";
				}

				if (fields.ContainsKey("enableTestmode"))
				{
					string enableTestmode = fields.GetStringValue("enableTestmode");
				
					store.EnableTestmode = enableTestmode == "enable" || enableTestmode == "1" || enableTestmode == "true";
				}

				store.EmailAddressFrom = FieldsValueOrEmpty("storeEmailFrom", fields);
				store.EmailAddressFromName = FieldsValueOrEmpty("storeEmailFromName", fields);
				store.EmailAddressTo = FieldsValueOrEmpty("storeEmailTo", fields);
				store.AccountCreatedEmail = FieldsValueOrEmpty("accountEmailCreated", fields);
				store.AccountForgotPasswordEmail = FieldsValueOrEmpty("accountForgotPassword", fields);
				store.ConfirmationEmailStore = FieldsValueOrEmpty("confirmationEmailStore", fields);
				store.ConfirmationEmailCustomer = FieldsValueOrEmpty("confirmationEmailCustomer", fields);
				store.OnlinePaymentEmailStore = FieldsValueOrEmpty("onlinePaymentEmailStore", fields);
				store.OnlinePaymentEmailCustomer = FieldsValueOrEmpty("onlinePaymentEmailCustomer", fields);
				store.OfflinePaymentEmailStore = FieldsValueOrEmpty("offlinePaymentEmailStore", fields);
				store.OfflinePaymentEmailCustomer = FieldsValueOrEmpty("offlinePaymentEmailCustomer", fields);
				store.PaymentFailedEmailStore = FieldsValueOrEmpty("paymentFailedEmailStore", fields);
				store.PaymentFailedEmailCustomer = FieldsValueOrEmpty("paymentFailedEmailCustomer", fields);
				store.DispatchedEmailStore = FieldsValueOrEmpty("dispatchedEmailStore", fields);
				store.DispatchEmailCustomer = FieldsValueOrEmpty("dispatchedEmailCustomer", fields);
				store.CancelEmailStore = FieldsValueOrEmpty("cancelEmailStore", fields);
				store.CancelEmailCustomer = FieldsValueOrEmpty("cancelEmailCustomer", fields);
				store.ClosedEmailStore = FieldsValueOrEmpty("closedEmailStore", fields);
				store.ClosedEmailCustomer = FieldsValueOrEmpty("closedEmailCustomer", fields);
				store.PendingEmailStore = FieldsValueOrEmpty("pendingEmailStore", fields);
				store.PendingEmailCustomer = FieldsValueOrEmpty("pendingEmailCustomer", fields);
				store.TemporaryOutOfStockEmailStore = FieldsValueOrEmpty("temporaryOutOfStockEmailStore", fields);
				store.TemporaryOutOfStockEmailCustomer = FieldsValueOrEmpty("temporaryOutOfStockEmailCustomer", fields);
				store.UndeliverableEmailStore = FieldsValueOrEmpty("undeliverableEmailStore", fields);
				store.UndeliverableEmailCustomer = FieldsValueOrEmpty("undeliverableEmailCustomer", fields);
				store.ReturnedEmailStore = FieldsValueOrEmpty("returnEmailStore", fields);
				store.ReturnedEmailCustomer = FieldsValueOrEmpty("returnEmailCustomer", fields);
			}

			private string FieldsValueOrEmpty(string propertyAlias, IPropertyProvider fields)
			{
				if (fields.ContainsKey(propertyAlias))
				{
					return fields.GetStringValue(propertyAlias);
				}
				return string.Empty;
			}

			public override string TypeAlias
			{
				get { return Store.NodeAlias; }
			}
		}
	}
}