﻿using System;
using uWebshop.Domain;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Umbraco.Repositories
{
	internal class UmbracoProductVariantRepository : UmbracoMultiStoreEntityRepository<ProductVariant>, IProductVariantRepository
	{
		private readonly IProductDiscountRepository _productDiscountRepository;
		private readonly IStoreService _storeService;

		public UmbracoProductVariantRepository(IProductDiscountRepository productDiscountRepository, IStoreService storeService)
		{
			_productDiscountRepository = productDiscountRepository;
			_storeService = storeService;
		}

		public override void LoadDataFromPropertiesDictionary(ProductVariant variant, IPropertyProvider fields, string storeAlias)
		{
			variant.StoreAlias = storeAlias;
			var store = _storeService.GetByAlias(storeAlias);

			// todo: hmmmmmm.... (tijdelijk zo, maar hoe structureler?)
			ProductVariant.LoadDataFromPropertyProvider(variant, storeAlias, fields);

			//variant.ProductVariantDiscount = _productDiscountRepository.GetDiscountByProductId(variant.Id); // todo: better?
			//variant.IsDiscounted = variant.ProductVariantDiscount != null;

			variant.SKU = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("sku", storeAlias, fields);

			variant.OriginalPriceInCents = StoreHelper.GetMultiStoreIntValue("price", storeAlias, fields);

			variant.Weight = StoreHelper.GetMultiStoreDoubleValue("weight", storeAlias, fields);
			variant.Width = StoreHelper.GetMultiStoreDoubleValue("width", storeAlias, fields);
			variant.Length = StoreHelper.GetMultiStoreDoubleValue("length", storeAlias, fields);
			variant.Height = StoreHelper.GetMultiStoreDoubleValue("height", storeAlias, fields);

			var stockStatus = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("stockStatus", storeAlias, fields);
			if (stockStatus == "default" || stockStatus == String.Empty)
			{
				variant.StockStatus = store.UseStock;
			}
			else
			{
				variant.StockStatus = stockStatus == "enable" || stockStatus == "1" || stockStatus == "true";
			}
			var backorderStatus = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("backorderStatus", storeAlias, fields);
			if (backorderStatus == "default" || backorderStatus == String.Empty)
			{
				variant.BackorderStatus = store.UseBackorders;
			}
			else
			{
				variant.BackorderStatus = backorderStatus == "enable" || backorderStatus == "1" || backorderStatus == "true";
			}


			variant.Group = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("group", storeAlias, fields);
			var value = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("requiredVariant", storeAlias, fields);
			variant.Required = value == "1" || value == "true";

			var rangesString = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("ranges", storeAlias, fields);
			variant.Ranges = Range.CreateFromString(rangesString);
		}

		public override string TypeAlias
		{
			get { return ProductVariant.NodeAlias; }
		}
	}
}
