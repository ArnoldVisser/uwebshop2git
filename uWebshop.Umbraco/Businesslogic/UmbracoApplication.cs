﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using Examine;
using uWebshop.Domain.BaseClasses;
using umbraco;
using umbraco.interfaces;
using umbraco.IO;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco.DataLayer;
using umbraco.NodeFactory;
using umbraco.cms.businesslogic.member;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco.Businesslogic;
using library = umbraco.library;

namespace uWebshop.Umbraco
{
	internal class UmbracoApplication : ICMSApplication
	{
		private readonly IHttpContextWrapper _httpContextWrapper;

		public UmbracoApplication(IHttpContextWrapper httpContextWrapper)
		{
			_httpContextWrapper = httpContextWrapper;
		}

		public bool RequestIsInCMSBackend(HttpContext context)
		{
			return GlobalSettings.RequestIsInUmbracoApplication(context);
		}

		public bool IsReservedPathOrUrl(string path)
		{
			return GlobalSettings.IsReservedPathOrUrl(path);
		}

		public int VersionMajor
		{
			get { return GlobalSettings.VersionMajor; }
		}

		public int VersionMinor { get { return GlobalSettings.VersionMinor; }}

		public string GetuWebshopCMSNodeUrlName()
		{
			var uWebshopNode = DomainHelper.GetUwebShopNode();
			return uWebshopNode == null ? null : uWebshopNode.UrlName;
		}

		public string GetCatalogRepositoryCMSNodeUrlName()
		{
			var catalogRepositoryNode = DomainHelper.GetObjectsByAlias<uWebshopEntity>(Catalog.NodeAlias, Constants.NonMultiStoreAlias).FirstOrDefault();
			return catalogRepositoryNode == null ? null : catalogRepositoryNode.UrlName;
		}

		public string GetCategoryRepositoryCMSNodeUrlName()
		{
			var categoryRepositoryNode = Catalog.GetCategoryRepositoryNode();
			return categoryRepositoryNode == null ? null : categoryRepositoryNode.UrlName;
		}
		
		public string GetPaymentProviderRepositoryCMSNodeUrlName()
		{
			var paymentProviderRepositoryNode = DomainHelper.GetObjectsByAlias<Node>(PaymentProvider.PaymentProviderRepositoryNodeAlias, Constants.NonMultiStoreAlias).FirstOrDefault();
			return paymentProviderRepositoryNode == null ? null : paymentProviderRepositoryNode.UrlName;
		}

		public string GetPaymentProviderSectionCMSNodeUrlName()
		{
			var paymentProviderSectionNode = DomainHelper.GetObjectsByAlias<Node>(PaymentProvider.PaymentProviderSectionNodeAlias, Constants.NonMultiStoreAlias).FirstOrDefault();
			return paymentProviderSectionNode == null ? null : paymentProviderSectionNode.UrlName;
		}
		
		public string GetDictionaryItem(string key)
		{
			return library.GetDictionaryItem(key);
		}

		public bool MemberLoggedIn()
		{
			return Member.GetCurrentMember() != null;
		}

		public int CurrentMemberId()
		{
			return Member.GetCurrentMember().Id;
		}

		public bool UsesSQLCEDatabase()
		{
			return DataLayerHelper.IsEmbeddedDatabase(GlobalSettings.DbDSN);
		}

		public bool UsesMySQLDatabase()
		{
			return GlobalSettings.DbDSN.ToLower().Contains("mysql");
		}

		public bool HideTopLevelNodeFromPath
		{
			get { return GlobalSettings.HideTopLevelNodeFromPath; }
		}

		public bool AddTrailingSlash { get { return UmbracoSettings.AddTrailingSlash; } }

		public bool UsesMvcRendermode
		{
			get { return InternalHelpers.MvcRenderMode; }
		}

		public IMemberInfo CurrentMemberInfo()
		{
			return new UmbracoMemberInfo();
		}

		public string ParseInternalLinks(string text)
		{
			return Helpers.ParseInternalLinks(text);
		}
		
		public int CurrentNodeId()
		{
			return Node.getCurrentNodeId();
		}

		public bool HasValidLicense()
		{
			return Licensing.IsValid();
		}

		// unsure about location! (maybe other service)
		public string GetMultiStoreContentProperty(int contentId, string propertyAlias, string storeAlias, bool globalOverrulesStore = false)
		{
			SearchResult examineNode = Helpers.GetNodeFromExamine(contentId, "GetMultiStoreItem::" + propertyAlias);
			if (string.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}
			string multiStoreAlias = StoreHelper.CreateMultiStorePropertyAlias(propertyAlias, storeAlias);
			if (examineNode != null)
			{
				if (multiStoreAlias.StartsWith("description"))
				{
					multiStoreAlias = "RTEItem" + multiStoreAlias;
					propertyAlias = "RTEItem" + propertyAlias;
				}

				if (!globalOverrulesStore && examineNode.Fields.ContainsKey(multiStoreAlias))
				{
					return examineNode.Fields[multiStoreAlias] ?? String.Empty;
				}

				if (examineNode.Fields.ContainsKey(propertyAlias))
				{
					return examineNode.Fields[propertyAlias] ?? String.Empty;
				}

				if (globalOverrulesStore && examineNode.Fields.ContainsKey(multiStoreAlias))
				{
					return examineNode.Fields[multiStoreAlias] ?? String.Empty;
				}
			}

			var node = new Node(contentId);
			if (node.Name != null)
			{
				IProperty property = node.GetProperty(propertyAlias);
				if (!globalOverrulesStore && property != null)
				{
					return property.Value;
				}
				var propertyMultistore = node.GetProperty(multiStoreAlias);
				if (propertyMultistore != null)
				{
					return propertyMultistore.Value;
				}
				if (globalOverrulesStore && property != null)
				{
					return property.Value;
				}
			}
			return String.Empty;
		}

		public string RenderMacro(string templateAlias, int contentId, params object[] properties)
		{
			var razorFileLocation = string.Format("{0}/{1}", SystemDirectories.MacroScripts, templateAlias);

			return RazorLibraryExtensions.RenderMacro(razorFileLocation, contentId, properties);
		}

		public string ApplyUrlFormatRules(string url)
		{
			Dictionary<string, string> replacements = UrlReplacementsHack();

			foreach (string x in replacements.Keys)
				url = url.Replace(x, replacements[x]);

			// check for double dashes
			if (RemoveDoubleDashesFromUrlReplacingHack())
			{
				url = Regex.Replace(url, @"[-]{2,}", "-");
			}
			return url;
		}

		public List<ICustomerType> GetAllMemberTypes()
		{
			return MemberType.GetAll.Select(mt => new UmbracoMemberAdaptor(){ Alias = mt.Alias}).ToList<ICustomerType>();
		}

		public string GetUrlForContentWithId(int id)
		{
			return library.NiceUrl(id);
		}

		public string RenderXsltMacro(string templateAlias, Dictionary<string, object> xsltParameters, XmlDocument entityXml = null)
		{
			if (entityXml == null) entityXml = content.Instance.XmlContent;
			return macro.GetXsltTransformResult(entityXml, macro.getXslt(templateAlias), xsltParameters);
		}

		class UmbracoMemberAdaptor : ICustomerType
		{
			public string Alias { get; set; }
		}

		private const string RemoveDoubleDashesFromUrlReplacingHackCacheKey = "RemoveDoubleDashesFromUrlReplacingHackCacheKey";
		private const string UrlReplacementsHackCacheKey = "UrlReplacementsHackCacheKey";

		private static bool RemoveDoubleDashesFromUrlReplacingHack()
		{
			if (HttpContext.Current.Items.Contains(RemoveDoubleDashesFromUrlReplacingHackCacheKey))
			{
				return (bool)HttpContext.Current.Items[RemoveDoubleDashesFromUrlReplacingHackCacheKey];
			}
			bool wtf = UmbracoSettings.RemoveDoubleDashesFromUrlReplacing;
			HttpContext.Current.Items[RemoveDoubleDashesFromUrlReplacingHackCacheKey] = wtf;
			return wtf;
		}
		private static Dictionary<string, string> UrlReplacementsHack()
		{
			if (HttpContext.Current.Items[UrlReplacementsHackCacheKey] != null) return (Dictionary<string, string>)HttpContext.Current.Items[UrlReplacementsHackCacheKey];
			var replacements = new Dictionary<string, string>();
			XmlNode replaceChars = UmbracoSettings.UrlReplaceCharacters;
			foreach (XmlNode n in replaceChars.SelectNodes("char"))
			{
				if (n.Attributes.GetNamedItem("org") != null && n.Attributes.GetNamedItem("org").Value != "")
					replacements.Add(n.Attributes.GetNamedItem("org").Value, xmlHelper.GetNodeValue(n));
			}
			HttpContext.Current.Items[UrlReplacementsHackCacheKey] = replacements;
			return replacements;
		}

		public string GetDomainForNodeId(int id)
		{
			//return umbraco.cms.businesslogic.web.Domain.GetDomainsById(id).Select(domain => domain.Name).FirstOrDefault();
			var domains = umbraco.cms.businesslogic.web.Domain.GetDomainsById(id).Select(d => d.Name);

			var domain = domains.FirstOrDefault();

			//if (domains.Any())
			//{
			//	var currentDomainName = HttpContext.Current.Request.ServerVariables["SERVER_NAME"];

			//	if (currentDomainName != null && domains.Contains(currentDomainName))
			//	{
			//		// todo: this does nothing, Arnold please check the intended functionality!
			//		domain = umbraco.cms.businesslogic.web.Domain.GetDomain(HttpContext.Current.Request.ServerVariables["SERVER_NAME"]).Name;
			//	}
			//}

			if (domain == null)
			{
				//Log.Instance.LogWarning("GetDomainForNodeId: could not find domain for node with id " + id);
				return "";
			}
			domain = domain.TrimEnd('/');
			if (!domain.StartsWith("http"))
			{
				var connection = _httpContextWrapper.IsSecureConnection ? "https" : "http";
				domain = String.Format("{0}://{1}", connection, domain);
			}
			return domain;

		}
	}
	public class UmbracoMemberInfo : IMemberInfo
	{
		public bool VATNumberCheckedAsValid
		{
			get
			{
				var member = Member.GetCurrentMember();

				var vatValid = "0";
				if (member.getProperty("customerValidVAT") != null &&
					member.getProperty("customerValidVAT").Value != null)
				{
					vatValid = member.getProperty("customerValidVAT").Value.ToString();
				}
				return vatValid != "0";
			}
		}
	}

}