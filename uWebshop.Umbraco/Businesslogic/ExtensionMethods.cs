﻿using System;
using System.Linq;
using umbraco;
using umbraco.cms.businesslogic.web;
using umbraco.interfaces;
using umbraco.NodeFactory;
using uWebshop.Domain;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Businesslogic;
using uWebshop.Domain.Helpers;

namespace uWebshop.Umbraco
{
	public static class ExtensionMethods
	{
		public static umbraco.cms.businesslogic.property.Property GetMultiStorelItem(this Document document, string alias)
		{
			var originalAlias = alias;

			#region frontend

			var orderInfo = OrderHelper.GetOrderInfo();
			if (String.IsNullOrEmpty(library.Request("id")))
			{
				if (orderInfo != null)
					alias = StoreHelper.CreateMultiStorePropertyAlias(alias, orderInfo.StoreInfo.Alias);
				else
				{
					var sAlias = StoreHelper.GetCurrentStore();
					alias = StoreHelper.CreateMultiStorePropertyAlias(alias, sAlias.Alias);
				}
				return document.getProperty(alias);
			}

			#endregion

			#region backend

			//var nodeId = int.Parse(library.Request("id"));
			//var orderNode = new Order(nodeId);

			var typeAlias = document.ContentType.Alias;
			var orderDoc = document;
			if (OrderedProduct.IsAlias(orderDoc.ContentType.Alias) && !OrderedProductVariant.IsAlias(orderDoc.ContentType.Alias))
				orderDoc = new Document(orderDoc.ParentId);

			if (typeAlias == Order.NodeAlias || OrderedProduct.IsAlias(typeAlias) && !OrderedProductVariant.IsAlias(typeAlias))
			{
				var orderInfoDoc = OrderHelper.GetOrderInfo(Guid.Parse(orderDoc.getProperty("orderGuid").Value.ToString()));
				var store = StoreHelper.GetByAlias(orderInfoDoc.StoreInfo.Alias);

				if (store != null) alias = StoreHelper.CreateMultiStorePropertyAlias(alias, store.Alias);
			}

			var property = document.getProperty(alias);

			if (property == null || property.Value == null)
			{
				property = document.getProperty(originalAlias);
			}

			return property;

			#endregion
		}

		public static string GetProperty(this ProductVariant variant, string propertyAlias)
		{
			IProperty property = new Node(variant.Id).GetMultiStoreItem(propertyAlias);
			if (property == null) return String.Empty;
			return property.Value;
		}

		public static string GetProperty(this MultiStoreUwebshopContent content, string propertyAlias)
		{
			IProperty property = new Node(content.Id).GetMultiStoreItem(propertyAlias);
			if (property == null) return String.Empty;
			return property.Value;
		}

		/// <summary>
		/// Get the value based on the Store Alias
		/// </summary>
		/// <param name="node">The node.</param>
		/// <param name="alias">The alias.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static IProperty GetMultiStoreItem(this INode node, string alias, string storeAlias = null)
		{
			string originalAlias = alias;
			if (node == null) return null;

			#region backend

			// todo: fix backend
			int id;
			if (!String.IsNullOrEmpty(library.Request("id")) && Int32.TryParse(library.Request("id"), out id))
			{
				int nodeId = id; // int.Parse(library.Request("id"));

				string typeAlias = node.NodeTypeAlias;
				//if (typeAlias == null)
				//{
				//	var doc = new Document(nodeId);
				//	typeAlias = doc.ContentType.Alias;
				//}

				INode orderNode = node;
				if (OrderedProduct.IsAlias(typeAlias) && !OrderedProductVariant.IsAlias(typeAlias))
					orderNode = orderNode.Parent;

				if (typeAlias == Order.NodeAlias || OrderedProduct.IsAlias(typeAlias) && !OrderedProductVariant.IsAlias(typeAlias))
				{
					//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " INode.GetMultiStoreItem >>>>SQL<<<< SELECT orderInfo");
					OrderInfo orderInfoDoc = OrderHelper.GetOrderInfo(Guid.Parse(orderNode.GetProperty("orderGuid").Value));
					alias = StoreHelper.CreateMultiStorePropertyAlias(alias, orderInfoDoc.StoreInfo.Alias);

					IProperty propertyDoc = node.GetProperty(alias);

					if (propertyDoc == null || String.IsNullOrEmpty(propertyDoc.Value))
					{
						propertyDoc = node.GetProperty(originalAlias);
					}

					return propertyDoc;
				}
			}

			#endregion

			#region frontend

			// dit gebeurt duizenden keren

			//Log.Instance.LogDebug( DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " INode.GetMultiStoreItem >>>>SQL<<<< SELECT orderInfo");
			if (storeAlias == null)
			{
				storeAlias = StoreHelper.GetCurrentStore().Alias;
				//var orderInfo = OrderHelper.GetOrderInfo(); // cached
				//var storeAliasNode = StoreHelper.GetCurrentStore();
				//if (storeAliasNode != null && (storeAliasNode.Alias == null && orderInfo.StoreInfo.Alias != null && string.IsNullOrEmpty(storeAliasNode.Alias)))
				//{
				//	storeAlias = orderInfo.StoreInfo.Alias;
				//}
				//else
				//{
				//	if (storeAliasNode != null)
				//		storeAlias = storeAliasNode.Alias;
				//}
			}
			if (storeAlias != null)
				alias = StoreHelper.CreateMultiStorePropertyAlias(alias, storeAlias);

			IProperty property = node.GetProperty(alias);

			if (property == null || String.IsNullOrEmpty(property.Value))
			{
				property = node.GetProperty(originalAlias);
			}

			return property;

			#endregion
		}
	}
}



