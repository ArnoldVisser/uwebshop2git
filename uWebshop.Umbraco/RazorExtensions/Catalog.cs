﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Umbraco;
using uWebshop.Umbraco.Businesslogic;

namespace uWebshop.RazorExtensions
{
	public class Catalog
	{
		/// <summary>
		/// Return the property value for the current shopalias
		/// </summary>
		/// <param name="nodeId">The node unique identifier.</param>
		/// <param name="propertyAlias">The property alias.</param>
		/// <returns></returns>
		public static string GetPropertyValueForCurrentShop(int nodeId, string propertyAlias)
		{
			var store = StoreHelper.GetCurrentStore();

			propertyAlias = StoreHelper.CreateMultiStorePropertyAlias(propertyAlias, store.Alias);

			var node = new umbraco.NodeFactory.Node(nodeId);

			var property = node.GetProperty(propertyAlias);
			
			return property == null ? string.Empty : property.Value;
		}

		/// <summary>
		/// Gets all categories.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Category> GetAllCategories(string storeAlias = null)
		{
			return DomainHelper.GetAllCategories(false, storeAlias).ToList();
		}

		/// <summary>
		/// Gets all root categories.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Category> GetAllRootCategories(string storeAlias = null)
		{
			return Domain.Catalog.GetAllRootCategories(storeAlias);
		}

		/// <summary>
		/// Returns all the payment zones
		/// </summary>
		/// <returns></returns>
		public static List<Zone> GetAllPaymentZones()
		{
			return IO.Container.Resolve<IZoneService>().GetAllPaymentZones(StoreHelper.CurrentStoreAlias);
		}

		/// <summary>
		/// Returns all the shipping zones
		/// </summary>
		/// <returns></returns>
		public static List<Zone> GetAllShippingZones()
		{
			return IO.Container.Resolve<IZoneService>().GetAllShippingZones(StoreHelper.CurrentStoreAlias);
		}

		/// <summary>
		/// Get all the countires from country.xml or country_storealias.xml
		/// If not found a fallback list will be used
		/// </summary>
		/// <returns></returns>
		public static List<Country> GetAllCountries()
		{
			return StoreHelper.GetAllCountries(StoreHelper.CurrentStoreAlias);
		}

		/// <summary>
		/// Returns the full country name from the given countrycode
		/// </summary>
		/// <param name="countryCode">The country code.</param>
		/// <returns></returns>
		public static string CountryNameFromCode(string countryCode)
		{
			var country = StoreHelper.GetAllCountries().FirstOrDefault(x => x.Code == countryCode);
			return country != null ? country.Name : string.Empty;
		}

		/// <summary>
		/// Returns all the products with prices for the given store or current store
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Product> GetAllProducts(string storeAlias = null)
		{
			if (string.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}
			return IO.Container.Resolve<IProductService>().GetAllEnabledAndWithCategory(storeAlias);
		}

		/// <summary>
		/// Get a list of all stores
		/// </summary>
		/// <returns></returns>
		public static List<Store> GetAllStores()
		{
			return StoreHelper.GetAllStores().ToList();
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("use GetAllVariants()")]
		public static List<ProductVariant> GetAllPricingVariants()
		{
			return GetAllVariants();
		}

		/// <summary>
		/// Returns all the pricingvariants for the given store or current store
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<ProductVariant> GetAllVariants(string storeAlias = null)
		{
			return DomainHelper.GetAllProductVariants(false, storeAlias).Where(x => x.Product.Categories.Any()).ToList();
		}

		/// <summary>
		/// Returns a category item based on the URLname
		/// </summary>
		/// <param name="categoryUrlName">Name of the category URL.</param>
		/// <returns></returns>
		public static Category GetCategoryFromUrlName(string categoryUrlName)
		{
			return !string.IsNullOrEmpty(categoryUrlName) ? IO.Container.Resolve<ICatalogUrlResolvingService>().GetCategoryFromUrlName(categoryUrlName) : null;
		}

		/// <summary>
		/// Returns a product item based on the URLname
		/// </summary>
		/// <param name="categoryUrlName">Name of the category URL.</param>
		/// <param name="productUrlName">Name of the product URL.</param>
		/// <returns></returns>
		public static Product GetProductFromUrlName(string categoryUrlName, string productUrlName)
		{
			if (!string.IsNullOrEmpty(productUrlName) && !string.IsNullOrEmpty(categoryUrlName))
			{
				return IO.Container.Resolve<ICatalogUrlResolvingService>().GetProductFromUrlName(categoryUrlName, productUrlName);
			}
			return null;
		}

		/// <summary>
		/// Get image by Image Id
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		public static UwbsImage GetImageById(int id)
		{
			return Helpers.GetImageById(id);
		}

		/// <summary>
		/// Get Media values by Id
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		public static MediaValues GetUmbracoMedia(int id)
		{
			return Helpers.GetUmbracoMedia(id);
		}

		/// <summary>
		/// Returns the current catagory
		/// </summary>
		/// <param name="ignoreDisabled">if set to <c>true</c> ignore disabled.</param>
		/// <returns></returns>
        public static Category GetCategory(bool ignoreDisabled = false)
		{
		    var category = (Category)UwebshopRequest.Current.Category;

            if (category == null || (!ignoreDisabled && category.Disabled)) return null;
            return category;
		}

		/// <summary>
		/// Gets the category.
		/// </summary>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static Category GetCategory(int categoryId, string storeAlias = null)
		{
			var category = DomainHelper.GetCategoryById(categoryId, storeAlias);

			return category == null ? null : (category.Disabled ? null : category);
		}

		/// <summary>
		/// Gets the category.
		/// </summary>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static Category GetCategory(string categoryId, string storeAlias = null)
		{
			var parsedId = int.Parse(categoryId);
			return GetCategory(parsedId, storeAlias);
		}

		/// <summary>
		/// Returns the current product
		/// </summary>
		/// <param name="ignoreDisabled">if set to <c>true</c> ignore disabled.</param>
		/// <returns></returns>
		public static Product GetProduct(bool ignoreDisabled = false)
		{
		    var product = UwebshopRequest.Current.Product;

		    if (product == null || (!ignoreDisabled && product.Disabled)) return null;
			return product;
		}

		/// <summary>
		/// Returns the product information for the given productId, with prices for the given store or current store
		/// </summary>
		/// <param name="productId">The product unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static Product GetProduct(int productId, string storeAlias = null)
		{
			var product = DomainHelper.GetProductById(productId, storeAlias); // new Product(productId);
			return product != null && product.Disabled ? null : product;
		}

		/// <summary>
		/// Returns the product information for the given productId, with prices for the given storeAlias
		/// </summary>
		/// <param name="productId">The product unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static Product GetProduct(string productId, string storeAlias = null)
		{
			var parsedProductId = int.Parse(productId);
			return GetProduct(parsedProductId);
		}

		/// <summary>
		/// Returns unique products witch have one ore more tags in common with the given productId
		/// </summary>
		/// <param name="productId">The product unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Product> GetMatchingTagProducts(int productId, string storeAlias = null)
		{
			var currentProduct = DomainHelper.GetProductById(productId, storeAlias);

			return GetMatchingTagProducts(currentProduct);
		}

		/// <summary>
		/// Returns unique products witch have one ore more tags in common with the given productId
		/// </summary>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Category> GetMatchingTagCategories(int categoryId, string storeAlias = null)
		{
			var currentCategory = DomainHelper.GetCategoryById(categoryId, storeAlias);

			return GetMatchingTagCategories(currentCategory);
		}

		/// <summary>
		/// Returns unique products witch have one ore more tags in common with the given productId
		/// </summary>
		/// <param name="product">The product.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Product> GetMatchingTagProducts(Product product, string storeAlias = null)
		{
			return DomainHelper.GetAllProducts(false, storeAlias).Where(x => x.Tags.Intersect(product.Tags).Any()).ToList();
		}

		/// <summary>
		/// Returns unique products witch have one ore more tags in common with the given productId
		/// </summary>
		/// <param name="category">The category.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Category> GetMatchingTagCategories(Category category, string storeAlias = null)
		{
			return DomainHelper.GetAllCategories(false, storeAlias).Where(x => x.Tags.Intersect(category.Tags).Any()).ToList();
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("use GetVariant()")]
		public static ProductVariant GetPricingVariant(int variantId)
		{
			return GetVariant(variantId);
		}

		/// <summary>
		/// Returns the pricingVariant information for the given pricingVariantId
		/// </summary>
		/// <param name="variantId">The variant unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static ProductVariant GetVariant(int variantId, string storeAlias = null)
		{
			var pricingVariant = DomainHelper.GetProductVariantById(variantId, storeAlias);
			return pricingVariant.Disabled == false ? pricingVariant : null;
		}

		/// <summary>
		/// Returns the pricingVariant information for the given pricingVariantId
		/// </summary>
		/// <param name="variantId">The variant unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static ProductVariant GetVariant(string variantId, string storeAlias = null)
		{
			var parsedId = int.Parse(variantId);
			var pricingVariant = DomainHelper.GetProductVariantById(parsedId, storeAlias);
			return pricingVariant.Disabled == false ? pricingVariant : null;
		}

		/// <summary>
		/// Returns all the products in this category, including any sublevel
		/// </summary>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static List<Product> GetProductsRecursive(int categoryId, string storeAlias = null)
		{
			return DomainHelper.GetCategoryById(categoryId, storeAlias).ProductsRecursive.ToList();
		}

		/// <summary>
		/// Returns all the categories in this category, including any sublevel
		/// </summary>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns>
		/// All subcategories, independent of level of the given categoryId
		/// </returns>
		public static List<Category> GetCategoriesRecursive(int categoryId, string storeAlias = null)
		{
			return StoreHelper.GetCategoriesRecursive(categoryId, storeAlias);
		}

		/// <summary>
		/// returns a list of payment providers
		/// </summary>
		/// <returns>
		/// All list of all the Payment Providers in uWebshop
		/// </returns>
		public static List<PaymentProvider> GetAllPaymentProviders()
		{
			return PaymentProviderHelper.GetAllPaymentProviders().ToList();
		}

		/// <summary>
		/// Return a list of shipping providers
		/// </summary>
		/// <returns>
		/// All list of all the Shipping Providers in uWebshop
		/// </returns>
		public static List<ShippingProvider> GetAllShippingProviders()
		{
			return ShippingProviderHelper.GetAllShippingProviders().ToList();
		}

		/// <summary>
		/// Returns all the product discounts
		/// </summary>
		/// <returns>
		/// A list of all the Product discounts in uWebshop
		/// </returns>
		public static List<DiscountProduct> GetAllProductDiscounts()
		{
			return DomainHelper.GetObjectsByAlias<DiscountProduct>(DiscountProduct.NodeAlias).Where(x => x.Disable == false).ToList();
		}

		/// <summary>
		/// Returns all the order discounts
		/// </summary>
		/// <returns>
		/// A list of all the Order discounts in uWebshop
		/// </returns>
		public static List<DiscountOrder> GetAllOrderDiscounts()
		{
			return DomainHelper.GetObjectsByAlias<DiscountOrder>(DiscountOrder.NodeAlias).Where(x => x.Disable == false).ToList();
		}

		/// <summary>
		/// Return the current store
		/// </summary>
		/// <returns>
		/// Store Object for the current store
		/// </returns>
		public static Store GetCurrentStore()
		{
			return StoreHelper.GetCurrentStore();
		}

		/// <summary>
		/// Generate paging based on the itemcount and the items per page
		/// </summary>
		/// <param name="itemCount">The item count.</param>
		/// <param name="itemsPerPage">The items per page.</param>
		/// <returns>
		/// Paging Object
		/// </returns>
		public static Paging GetPages(int itemCount, int itemsPerPage)
		{
			return Paging.GetPages(itemCount, itemsPerPage);
		}
		
	}
}