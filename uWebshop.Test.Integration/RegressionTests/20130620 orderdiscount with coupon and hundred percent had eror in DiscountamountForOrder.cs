﻿using System.Linq;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using uWebshop.Test.Repositories;

namespace uWebshop.Test.Integration.RegressionTests
{
    [TestFixture]
    public class _20130620_orderdiscount_with_coupon_and_hundred_percent_had_eror_in_DiscountamountForOrder
    {
        [Test]
        public void bla()
        {
            IOC.IntegrationTest();
            var productInfo = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(12990, 1);
            productInfo.Id = TestProductService.ProductId1;
            var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateIncompleteOrderInfo(productInfo);

            var discount1 = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithPercentage(100);
            discount1.CouponCode = "coup";
            IOC.DiscountRepository.SetupFake(discount1);

            DefaultFactoriesAndSharedFunctionality.SetDiscountsOnOrderInfo(orderInfo, discount1);
            orderInfo.SetCouponCode("coup");

            Assert.AreEqual(12990, orderInfo.OrderLines.Sum(orderline => orderline.OrderLineAmountInCents));
            Assert.AreEqual(12990, orderInfo.OrderDiscounts.Sum(d => IO.Container.Resolve<IDiscountCalculationService>().DiscountAmountForOrder(d, orderInfo)));

            Assert.AreEqual(12990, orderInfo.DiscountAmountInCents);
            Assert.AreEqual(0, orderInfo.OrderTotalInCents);

            Assert.NotNull(orderInfo.OrderDiscountsFactory);

            var orderService = IO.Container.Resolve<IOrderService>();
            var discountRepository = IO.Container.Resolve<IDiscountRepository>();

            var orderDiscounts = discountRepository.GetAll();
            Assert.IsTrue(orderDiscounts.Any());

            var orderLinesAmount = orderInfo.OrderLines.Sum(orderline => orderline.GetOrderLineGrandTotalInCents(orderInfo.PricesAreIncludingVAT));
            Assert.AreEqual(12990, orderLinesAmount);

            var discount = orderDiscounts.First();

            Assert.IsTrue(!discount.Disable && orderLinesAmount >= discount.MinimalOrderAmount
                && (!discount.RequiredItemIds.Any() || orderService.OrderContainsItem(orderInfo, discount.RequiredItemIds))
                && (!discount.CounterEnabled || discount.Counter > 0));

            Assert.IsFalse(!string.IsNullOrEmpty(discount.CouponCode) && !orderInfo.CouponCodes.Contains(discount.CouponCode));

            var discountService = IO.Container.Resolve<IDiscountService>();
            Assert.IsTrue(discountService.GetApplicableDiscountsForOrder(orderInfo).Any());

            Assert.IsTrue(orderInfo.OrderDiscountsFactory().Any());
        }
    }
}
