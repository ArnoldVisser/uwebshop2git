﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Xml.Linq;
using Newtonsoft.Json;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Businesslogic;
using uWebshop.Domain.Helpers;
using umbraco.presentation.umbracobase;
using Log = uWebshop.Domain.Log;

namespace uWebshop
{
	// code die het mogelijk maakt om DLLs te embedden
	////   misschien nog een ander entry point?
	//public class UrlRewriting : IHttpModule
	//{
	//	static UrlRewriting()
	//	{
	//		AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
	//			{
	//				//if (!args.Name.StartsWith("uWebshop.Common")) return null;
	//				String resourceName = "uWebshop.Resources." + new AssemblyName(args.Name).Name + ".dll"; // fully qualified name of THIS assembly + folder + name of embedded assembly

	//				using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
	//				{
	//					if (stream == null) return null;
	//					Byte[] assemblyData = new Byte[stream.Length];
	//					stream.Read(assemblyData, 0, assemblyData.Length);
	//					return Assembly.Load(assemblyData);
	//				}
	//			};
	//	}
	//	public void Init(HttpApplication context) { }
	//	public void Dispose() { }
	//}

	[RestExtension("uWebshop")]
	public class Base
	{
		/// <summary>
		/// Validate the full order
		/// </summary>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ValidateOrder(string uniqueOrderGuid = null)
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				Guid orderId;
				var order = Guid.TryParse(uniqueOrderGuid, out orderId) ? OrderHelper.GetOrderInfo(orderId) : OrderHelper.GetOrderInfo();

				OrderHelper.ValidateOrder(order);
				order.Save();

				var orderValidationErrors = order.OrderValidationErrors;

				successFailed.Add("success", "true");
				successFailed.Add("ordervalidationerrors", orderValidationErrors);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("ValidateOrder: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Validate the customer information
		/// </summary>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ValidateCustomer(string uniqueOrderGuid = null)
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				Guid orderId;
				OrderInfo order = Guid.TryParse(uniqueOrderGuid, out orderId) ? OrderHelper.GetOrderInfo(orderId) : OrderHelper.GetOrderInfo();
				OrderHelper.ValidateCustomer(order, true);
				order.Save();

				var orderValidationErrors = order.OrderValidationErrors;

				successFailed.Add("success", "true");
				successFailed.Add("ordervalidationerrors", orderValidationErrors);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("ValidateCustomer: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Validate the stock
		/// </summary>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ValidateStock(string uniqueOrderGuid = null)
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				Guid orderId;
				var order = Guid.TryParse(uniqueOrderGuid, out orderId) ? OrderHelper.GetOrderInfo(orderId) : OrderHelper.GetOrderInfo();

				OrderHelper.ValidateStock(order, true);
				order.Save();

				var orderValidationErrors = order.OrderValidationErrors;

				successFailed.Add("success", "true");
				successFailed.Add("ordervalidationerrors", orderValidationErrors);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("ValidateStock: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Validate the order lines
		/// </summary>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ValidateOrderLines(string uniqueOrderGuid = null)
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				Guid orderId;
				var order = Guid.TryParse(uniqueOrderGuid, out orderId) ? OrderHelper.GetOrderInfo(orderId) : OrderHelper.GetOrderInfo();

				OrderHelper.ValidateOrderLines(order, true);
				order.Save();

				var orderValidationErrors = order.OrderValidationErrors;

				successFailed.Add("success", "true");
				successFailed.Add("ordervalidationerrors", orderValidationErrors);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("ValidateOrderLines: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Validate the order lines
		/// </summary>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ValidateCustomValidation(string uniqueOrderGuid = null)
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				Guid orderId;
				var order = Guid.TryParse(uniqueOrderGuid, out orderId) ? OrderHelper.GetOrderInfo(orderId) : OrderHelper.GetOrderInfo();

				OrderHelper.ValidateCustomValidation(order, true);
				order.Save();

				var orderValidationErrors = order.OrderValidationErrors;

				successFailed.Add("success", "true");
				successFailed.Add("ordervalidationerrors", orderValidationErrors);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("ValidatCustomValidation: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}


		/// <summary>
		/// Clears all the orderlines from the order
		/// </summary>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ClearBasket()
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				var orderInfo = OrderHelper.GetOrderInfo();
				orderInfo.OrderLines.Clear();

				orderInfo.Save();

				successFailed.Add("success", "true");
				successFailed.Add("orderinfo", orderInfo);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("ClearBasket: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Get the current order
		/// </summary>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void GetOrderInfo(string uniqueOrderGuid = null)
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				Guid orderId;
				var order = Guid.TryParse(uniqueOrderGuid, out orderId) ? OrderHelper.GetOrderInfo(orderId) : OrderHelper.GetOrderInfo();
				
				successFailed.Add("success", "true");
				successFailed.Add("orderinfo", order);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("GetOrderInfo: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Get orderinfo based on unique order GUID
		/// </summary>
		/// <param name="uniqueOrderGuid">unique order Guid</param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		[Obsolete("Use GetOrderInfo(string uniqueOrderGuid)")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static void GetOrderInfoByUniqueId(string uniqueOrderGuid)
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				var orderInfo = OrderHelper.GetOrderInfo(Guid.Parse(uniqueOrderGuid));

				successFailed.Add("success", "true");
				successFailed.Add("orderinfo", orderInfo);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("GetOrderInfoByUniqueId: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddOrderLineDetails(int orderLineId, string properties)
		{
			var orderInfo = OrderHelper.GetOrderInfo();

			var successFailed = new Dictionary<string, object>();

			try
			{
				var dictionary = new Dictionary<string, string>();

				try
				{
					dictionary =
						properties.Split(';')
								  .Select(part => part.Split('='))
								  .Where(part => part.Length == 2)
								  .ToDictionary(sp => sp[0], sp => sp[1]);
				}
				catch
				{
					Log.Instance.LogError("AddProduct: An issue with properties: should be key1=value1;key2=value2;key3=value3");
				}

				if (dictionary.Any())
				{
					orderInfo.AddOrderLineDetails(dictionary, orderLineId);
					orderInfo.Save();
				}

				successFailed.Add("success", "true");
				successFailed.Add("orderLineDetails", orderInfo);
				
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddOrderLineDetails: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add or update orderline
		/// </summary>
		/// <param name="productId">node Id of the product</param>
		/// <param name="action">add/update/delete</param>
		/// <param name="itemCount">Amount of items</param>
		/// <param name="variants">variant01=test&variant02=test2</param>
		/// <param name="properties"></param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddOrUpdateOrderLine(int productId, string action, int itemCount, string variants, string properties)
		{
			var orderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			var successFailed = new Dictionary<string, object>();

			try
			{
				var dictionary = new Dictionary<string, string>();

				try
				{
					dictionary = properties.Split(';')
								  .Select(part => part.Split('='))
								  .Where(part => part.Length == 2)
								  .ToDictionary(sp => sp[0], sp => sp[1]);
				}
				catch
				{
					Log.Instance.LogError("AddProduct: An issue with properties: should be key1=value1;key2=value2;key3=value3");
				}

				var variantList = CreateVariantList(variants);
				orderInfo.AddOrUpdateOrderLine(0, productId, action, itemCount, variantList, dictionary);
				orderInfo.Save();

				successFailed.Add("success", "true");
				successFailed.Add("orderinfo", orderInfo);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddOrUpdateOrderLine: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add or update orderline
		/// </summary>
		/// <param name="productId">node Id of the product</param>
		/// <param name="action">add/update/delete</param>
		/// <param name="itemCount">Amount of items</param>
		/// <param name="variants">variant01=test&variant02=test2</param>
		/// <param name="properties"></param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddProduct(int productId, string action, int itemCount, string variants, string properties)
		{
			var orderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			var successFailed = new Dictionary<string, object>();

			try
			{
				var dictionary = new Dictionary<string, string>();

				try
				{
					dictionary = properties.Split(';')
						          .Select(part => part.Split('='))
						          .Where(part => part.Length == 2)
						          .ToDictionary(sp => sp[0], sp => sp[1]);
				}
				catch
				{
					Log.Instance.LogError("AddProduct: An issue with properties: should be key1=value1;key2=value2;key3=value3");
				}

				var variantList = CreateVariantList(variants);
				orderInfo.AddOrUpdateOrderLine(0, productId, action, itemCount, variantList, dictionary);
				orderInfo.Save();

				successFailed.Add("success", "true");
				successFailed.Add("orderinfo", orderInfo);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddProduct: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add or update orderline
		/// </summary>
		/// <param name="orderlineId"></param>
		/// <param name="action">add/update/delete</param>
		/// <param name="itemCount">Amount of items</param>
		/// <param name="variants">variant01=test&variant02=test2</param>
		/// <param name="properties"></param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void UpdateOrderLine(int orderlineId, string action, int itemCount, string variants, string properties)
		{
			var orderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			var successFailed = new Dictionary<string, object>();

			try
			{
				var dictionary = new Dictionary<string, string>();

				try
				{
					dictionary = properties.Split(';')
								  .Select(part => part.Split('='))
								  .Where(part => part.Length == 2)
								  .ToDictionary(sp => sp[0], sp => sp[1]);
				}
				catch
				{
					Log.Instance.LogError("AddProduct: An issue with properties: should be key1=value1;key2=value2;key3=value3");
				}

				var variantList = CreateVariantList(variants);
				orderInfo.AddOrUpdateOrderLine(orderlineId, 0, action, itemCount, variantList, dictionary);
				orderInfo.Save();

				successFailed.Add("success", "true");
				successFailed.Add("orderinfo", orderInfo);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("UpdateOrderLine: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add paymnet provider to the order 
		/// </summary>
		/// <param name="paymentProviderId">int payment provider node id</param>
		/// <param name="paymentMethodId">string payment provider method id (node id or name from API)</param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddPaymentProvider(int paymentProviderId, string paymentMethodId)
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				var result = orderInfo.AddPaymentProvider(paymentProviderId, paymentMethodId);
				if (result != ProviderActionResult.Success)
				{
					successFailed.Add("success", "false");
					var dicItemValue = umbraco.library.GetDictionaryItem("Payment" + result.ToString());
					if (string.IsNullOrEmpty(dicItemValue))
					{
						dicItemValue = "#Payment" + result.ToString();
					}
					successFailed.Add("errors", string.Format(dicItemValue, paymentProviderId));
				}
				else
				{
					orderInfo.Save();
					successFailed.Add("success", "true");
					successFailed.Add("paymentprovider", orderInfo.PaymentInfo);
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddPaymentProvider: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add shipping provider to the order 
		/// </summary>
		/// <param name="shippingProviderId">int shipping provider node id</param>
		/// <param name="shipppingMethodId">string shipping provider method id (node id or name from API)</param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddShippingProvider(int shippingProviderId, string shipppingMethodId)
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				var result = orderInfo.AddShippingProvider(shippingProviderId, shipppingMethodId);
				if (result != ProviderActionResult.Success)
				{
					successFailed.Add("success", "false");
					var dicItemValue = umbraco.library.GetDictionaryItem("Shipping" + result.ToString());
					if (string.IsNullOrEmpty(dicItemValue))
					{
						dicItemValue = "#Shipping" + result.ToString();
					}
					successFailed.Add("errors", string.Format(dicItemValue, shippingProviderId));
				}
				else
				{
					orderInfo.Save();
					successFailed.Add("success", "true");
					successFailed.Add("shippingprovider", orderInfo.ShippingInfo);
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddShippingProvider: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add a couponcode tot he order
		/// </summary>
		/// <param name="couponCode">string couponcode</param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddCoupon(string couponCode)
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				var result = orderInfo.AddCoupon(couponCode);
				if (result != CouponCodeResult.Succes)
				{
					successFailed.Add("success", "false");
					var dicItemValue = umbraco.library.GetDictionaryItem(result.ToString());
					if (string.IsNullOrEmpty(dicItemValue))
					{
						dicItemValue = "#" + result.ToString();
					}
					successFailed.Add("errors", string.Format(dicItemValue, couponCode));
				}
				else
				{
					orderInfo.Save();
					successFailed.Add("success", "true");
					successFailed.Add("couponcodes", orderInfo.CouponCodes);
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddCoupon: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add a couponcode tot he order
		/// </summary>
		/// <param name="couponCode">string couponcode</param>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void RemoveCoupon(string couponCode)
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				var result = orderInfo.RemoveCoupon(couponCode);
				if (result != CouponCodeResult.Succes)
				{
					successFailed.Add("success", "false");
					var dicItemValue = umbraco.library.GetDictionaryItem(result.ToString());
					if (string.IsNullOrEmpty(dicItemValue))
					{
						dicItemValue = "#" + result.ToString();
					}
					successFailed.Add("errors", string.Format(dicItemValue, couponCode));
				}
				else
				{
					orderInfo.Save();
					successFailed.Add("success", "true");
					successFailed.Add("couponcodes", orderInfo.CouponCodes);
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddCoupon: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Confirms the order 
		/// </summary>
		/// <returns>URL to redirect to</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ConfirmOrder(int confirmOrderNodeId)
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				if (!orderInfo.ConfirmOrder(true, confirmOrderNodeId))
				{
					successFailed.Add("success", "false");
					successFailed.Add("errors", orderInfo.OrderValidationErrors);
				}
				else
				{
					orderInfo.Save();

					var redirectUrl = OrderHelper.GetRedirectUrlAfterConfirmation(orderInfo, confirmOrderNodeId);

					successFailed.Add("success", "true");
					successFailed.Add("url", redirectUrl);
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("ConfirmOrder: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void OrderValidationErrors()
		{
			var successFailed = new Dictionary<string, object>();

			try
			{
				var orderInfo = OrderHelper.GetOrderInfo();
				var orderValidationErrors = orderInfo.OrderValidationErrors;
				successFailed.Add("success", "true");
				successFailed.Add("ordervalidationerrors", orderValidationErrors);
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("OrderValidationErrors: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}


		private static bool AddOrderFields(OrderInfo orderInfo, string customerDataTypeFieldname, CustomerDatatypes customerDatatype)
		{
			for (var i = 0; i < HttpContext.Current.Request.Params.Count; i++)
			{
				if (HttpContext.Current.Request.Params[i].Contains(customerDataTypeFieldname) && !HttpContext.Current.Request.Params[i].Contains("_"))
				{
					var values = JsonConvert.DeserializeObject<Dictionary<string, string>>(HttpContext.Current.Request.Params[i]);

					if (!orderInfo.AddCustomerFields(values, customerDatatype))
						return false;
					orderInfo.Save();
				}
			}

			return true;
		}

		/// <summary>
		/// Add customer fields to the order 
		/// </summary>
		/// <returns>orderinfo object (JSON) </returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("use AddCustomerInformation()")]
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddCustomerFields()
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				if (!AddOrderFields(orderInfo, "customer", CustomerDatatypes.Customer))
				{
					successFailed.Add("success", "false");
					var dicItemValue = umbraco.library.GetDictionaryItem("CustomerInformationFailedToBeAdded");
					if (string.IsNullOrEmpty(dicItemValue))
					{
						dicItemValue = "#CustomerInformationFailedToBeAdded";
					}
					successFailed.Add("errors", dicItemValue);
				}
				else
				{
					successFailed.Add("success", "true");

					var xDocument = orderInfo.CustomerInfo.customerInformation;

					var xDoc = new XDocument(
						new XDeclaration("1.0", "utf-8", null),
						new XElement("Root"));

					foreach (var xElement in xDocument.Root.Elements())
					{
						xDoc.Root.Add(new XElement(xElement.Name, new XText(xElement.Value)));
					}

					successFailed.Add("customerfields", xDoc.Root.Descendants());
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddCustomerFields: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add shipping fields to the order 
		/// </summary>
		/// <returns>orderinfo object (JSON) </returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddShippingFields()
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				if (!AddOrderFields(orderInfo, "shipping", CustomerDatatypes.Shipping))
				{
					successFailed.Add("success", "false");
					var dicItemValue = umbraco.library.GetDictionaryItem("ShippingInformationFailedToBeAdded");
					if (string.IsNullOrEmpty(dicItemValue))
					{
						dicItemValue = "#ShippingInformationFailedToBeAdded";
					}
					successFailed.Add("errors", dicItemValue);
				}
				else
				{
					successFailed.Add("success", "true");

					var xDocument = orderInfo.CustomerInfo.shippingInformation;

					var xDoc = new XDocument(
						new XDeclaration("1.0", "utf-8", null),
						new XElement("Root"));

					foreach (var xElement in xDocument.Root.Elements())
					{
						xDoc.Root.Add(new XElement(xElement.Name, new XText(xElement.Value)));
					}

					successFailed.Add("shippingfields", xDoc.Root.Descendants());
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddShippingFields: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary>
		/// Add extra fields to the order 
		/// </summary>
		/// <returns>orderinfo object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void AddExtraFields()
		{
			var successFailed = new Dictionary<string, object>();

			var orderInfo = OrderHelper.GetOrderInfo();

			try
			{
				if (!AddOrderFields(orderInfo, "extra", CustomerDatatypes.Extra))
				{
					successFailed.Add("success", "false");

					successFailed.Add("success", "false");
					var dicItemValue = umbraco.library.GetDictionaryItem("ExtraInformationFailedToBeAdded");
					if (string.IsNullOrEmpty(dicItemValue))
					{
						dicItemValue = "#ExtraInformationFailedToBeAdded";
					}
					successFailed.Add("errors", dicItemValue);
				}
				else
				{
					successFailed.Add("success", "true");

					var xDocument = orderInfo.CustomerInfo.extraInformation;

					var xDoc = new XDocument(
						new XDeclaration("1.0", "utf-8", null),
						new XElement("Root"));

					foreach (var xElement in xDocument.Root.Elements())
					{
						xDoc.Root.Add(new XElement(xElement.Name, new XText(xElement.Value)));
					}

					successFailed.Add("extrafields", xDoc.Root.Descendants());
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("AddExtraFields: An exception occurred while processing the order!" + ex.ToLogString());
				successFailed.Add("success", "false");
				successFailed.Add("errors", "An exception occurred while processing the order!" + ex.ToLogString());
			}

			var json = JsonConvert.SerializeObject(successFailed, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		private static IEnumerable<int> CreateVariantList(string variants)
		{
			if (variants.EndsWith(","))
			{
				variants = variants.Remove(variants.Length - 1);
			}
			return variants.Split(',').Where(variant => variant != "0").Select(int.Parse).ToList();
		}

		/// <summary> 
		/// Get a category
		/// </summary>
		/// <param name="categoryId">int category node id</param>
		/// <param name="storeId">the id of the store to use</param>
		/// <returns>category object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void Category(int categoryId, int storeId)
		{
			if (storeId != 0)
			{
				var store = DomainHelper.StoreById(storeId);
                UwebshopRequest.Current.CurrentStore = store;
			}
			var category = DomainHelper.GetCategoryById(categoryId);
			var json = JsonConvert.SerializeObject(category, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary> 
		/// Get a product
		/// </summary>
		/// <param name="productId">int product node id</param>
		/// <param name="storeId">the id of the store to use</param>
		/// <returns>product object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void Product(int productId, int storeId)
		{
			if (storeId != 0)
			{
				var store = DomainHelper.StoreById(storeId);
                UwebshopRequest.Current.CurrentStore = store;
			}

			var product = DomainHelper.GetProductById(productId);
			var json = JsonConvert.SerializeObject(product, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary> 
		/// Get a product variant
		/// </summary>
		/// <param name="productVariantId">int product node id</param>
		/// <param name="storeId">the id of the store to use</param>
		/// <returns>product variant object (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void ProductVariant(int productVariantId, int storeId)
		{
			if (storeId != 0)
			{
				var store = DomainHelper.StoreById(storeId);
                UwebshopRequest.Current.CurrentStore = store;
			}

			var productVariant = DomainHelper.GetProductVariantById(productVariantId);
			var json = JsonConvert.SerializeObject(productVariant, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary> 
		/// Get the payment providers for the current order
		/// </summary>
		/// <returns>payment providers for order (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void GetPaymentProviders()
		{
			var providersList = PaymentProviderHelper.GetPaymentProvidersForOrder();
			var json = JsonConvert.SerializeObject(providersList, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}

		/// <summary> 
		/// Get the shipping providers for the current order
		/// </summary>
		/// <returns>shipping providers for order (JSON)</returns>
		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		[RestExtensionMethod(allowAll = true, returnXml = false)]
		public static void GetShippingProviders()
		{
			var providersList = ShippingProviderHelper.GetShippingProvidersForOrder();
			var json = JsonConvert.SerializeObject(providersList, Formatting.Indented);
			HttpContext.Current.Response.ContentType = "application/json";
			HttpContext.Current.Response.Write(json);
		}
	}
}