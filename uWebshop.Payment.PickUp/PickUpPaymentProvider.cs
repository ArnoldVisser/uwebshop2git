﻿using System.Collections.Generic;
using SuperSimpleWebshop.Common;
using SuperSimpleWebshop.Domain;
using SuperSimpleWebshop.Domain.Helpers;
using SuperSimpleWebshop.Domain.Interfaces;

namespace SuperSimpleWebshop.Payment.MultiSafepay
{
    public class PickUpPaymentProvider : IPaymentProvider
    {
        #region IPaymentProvider Members
        public string GetName()
        {
            return "PickUp";
        }

        public PaymentParameterRenderMethod GetParameterRenderMethod()
        {
            return PaymentParameterRenderMethod.Custom;
        }

        public List<PaymentMethod> GetAllPaymentMethods(string name)
        {
            var paymentNodeID = DomainHelper.GetNodeIdForDocument("sswsPaymentProvider", "PickUp");

            var provider = new PaymentProvider(paymentNodeID);

            var paymentMethods = new List<PaymentMethod>();
            paymentMethods.Add(new PaymentMethod
            {
                Description = provider.PaymentProviderTitle,
                //Id = "PickUp",
                Name = "PickUp"
            });

            return paymentMethods;
        }
        #endregion
    }
}
