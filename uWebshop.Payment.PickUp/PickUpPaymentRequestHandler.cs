﻿using SuperSimpleWebshop.Domain;
using SuperSimpleWebshop.Domain.Interfaces;

namespace SuperSimpleWebshop.Payment.MultiSafepay
{
    public class PickUpPaymentRequestHandler : IPaymentRequestHandler
    {
        #region IPaymentRequestHandler Members
        public string GetName()
        {
            return "PickUp";
        }

        public PaymentRequest CreatePaymentRequest(Order order)
        {
            return null;
        }
        #endregion
    }
}
