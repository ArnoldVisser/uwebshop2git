﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.NodeFactory;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.Buckaroo
{
	public class BuckarooPaymentRequestHandler : IPaymentRequestHandler
	{
		public string GetName()
		{
			return "Buckaroo";
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id, orderInfo.StoreInfo.Alias);
			var helper = new PaymentConfigHelper(paymentProvider);

			var currentNodeId = Node.GetCurrent().Id;
			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);
			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));

			var merchantKey = helper.Settings["MerchantKey"];
			if (string.IsNullOrEmpty(merchantKey))
			{
				Log.Instance.LogError("Buckaroo: Missing PaymentProvider.Config  field with name: MerchantKey" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var secrectKey = helper.Settings["SecretKey"];
			if (string.IsNullOrEmpty(secrectKey))
			{
				Log.Instance.LogError("Buckaroo: Missing PaymentProvider.Config  field with name: SecretKey" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var transactionId = orderInfo.OrderNumber + "x" + DateTime.Now.ToString("hhmmss");

			
			var ri = new RegionInfo(orderInfo.StoreInfo.CurrencyCultureInfo.LCID);
			var currency = ri.ISOCurrencySymbol;

			var testmode = paymentProvider.TestMode ? "1" : "0";

			// MD5(MerchantKey + InvoiceNumber + Amount + Currency + Modus + SecretKey)
			var stringToHash = string.Concat(merchantKey, orderInfo.OrderNumber, orderInfo.ChargedAmountInCents, currency, testmode, secrectKey);

			var signature = GetMd5Sum(stringToHash);

			var request = new PaymentRequest();
			request.Parameters.Add("BPE_Merchant", merchantKey);
			request.Parameters.Add("BPE_Amount", orderInfo.ChargedAmountInCents.ToString());
			request.Parameters.Add("BPE_Currency",currency);
			request.Parameters.Add("BPE_Description", "");
			
			request.Parameters.Add("BPE_Reference", transactionId);
			request.Parameters.Add("BPE_Language", orderInfo.StoreInfo.LanguageCode);
			request.Parameters.Add("BPE_Mode", testmode);
			request.Parameters.Add("BPE_Signature2", signature);
			request.Parameters.Add("BPE_Invoice", orderInfo.OrderNumber);
			request.Parameters.Add("BPE_Return_Success", reportUrl);
			request.Parameters.Add("BPE_Return_Reject", reportUrl);
			request.Parameters.Add("BPE_Return_Error", reportUrl);
			request.Parameters.Add("BPE_Return_Method", "POST");

			if (orderInfo.PaymentInfo.MethodId.ToLower() == "ideal")
			{
				request.PaymentUrlBase = "https://payment.buckaroo.nl/gateway/ideal_payment.asp";
			}

			if (orderInfo.PaymentInfo.MethodId.ToLower() == "creditcard")
			{
				request.PaymentUrlBase = "https://payment.buckaroo.nl/sslplus/request_for_authorization.asp";
			}

			orderInfo.PaymentInfo.Url = request.PaymentUrl;
			orderInfo.PaymentInfo.Parameters = request.ParametersAsString;
			orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;
			
			uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, transactionId);
			orderInfo.PaymentInfo.TransactionId = transactionId;

			orderInfo.Save();

			return request;
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return string.Empty;
		}
		
		// Create an md5 sum string of this string
		static public string GetMd5Sum(string str)
		{
			var x = new MD5CryptoServiceProvider();

			var data = Encoding.ASCII.GetBytes(str);

			data = x.ComputeHash(data);

			return BitConverter.ToString(data).Replace("-", "").ToLower();
		}
	}
}
