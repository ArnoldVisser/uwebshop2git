﻿using System.Collections.Generic;
using System.Configuration;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.Buckaroo
{
    public class BuckarooPaymentProvider : IPaymentProvider
    {
	    public string GetName()
        {
            return "Buckaroo";
        }

	    PaymentTransactionMethod IPaymentProvider.GetParameterRenderMethod()
	    {
		   return PaymentTransactionMethod.QueryString;
	    }

	    public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
	    {
		    return GetAllPaymentMethods(null);
	    }

        public List<PaymentProviderMethod> GetAllPaymentMethods(string name)
        {
	        var paymentMethods = new List<PaymentProviderMethod>();
	        
			if (ConfigurationManager.AppSettings["uwbs_Buckaroo_iDEAL_Enabled"].ToLower() == "true")
	        {
				paymentMethods.Add(new PaymentProviderMethod { Name = "Buckaroo", Description = "iDEAL", Id = "ideal" });
	        }
	        if (ConfigurationManager.AppSettings["uwbs_Buckaroo_CreditCard_Enabled"].ToLower() == "true")
	        {
				paymentMethods.Add(new PaymentProviderMethod { Name = "Buckaroo", Description = "Credit Card", Id = "creditcard" });
	        }

	        return paymentMethods;
        }
    }
}
