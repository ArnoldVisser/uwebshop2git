﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
// do not remove the umbraco.interfaces using below
using umbraco.BusinessLogic;
using umbraco.interfaces;
using umbraco.NodeFactory;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.Sisow
{
	public class SisowPaymentRequestHandler : IPaymentRequestHandler
	{
		private readonly IHttpRequestSender _requestSender;

		public SisowPaymentRequestHandler()
		{
			_requestSender = new HttpRequestSender();
		}

		public SisowPaymentRequestHandler(IHttpRequestSender requestSender = null)
		{
			_requestSender = requestSender ?? new HttpRequestSender();
		}

		public string GetName()
		{
			return "Sisow";
		}

		private static string HttpGet(string url)
		{
			var req = WebRequest.Create(url) as HttpWebRequest;
			string result;
			using (var resp = req.GetResponse() as HttpWebResponse)
			{
				var reader = new StreamReader(resp.GetResponseStream());
				result = reader.ReadToEnd();
			}
			return result;
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			#region build urls

			var currentNodeId = Node.GetCurrent().Id;
			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

			var returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.SuccesNodeId)));
			var cancelUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.ErrorNodeId)));

			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);
			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));
			
			#endregion

			#region config helper
			var apiURL = helper.Settings["TransactionRequestUrl"];
			if (string.IsNullOrEmpty(apiURL))
			{
				Log.Instance.LogError("SiSow: Missing PaymentProvider.Config  field with name: TransactionRequestUrl" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var merchantId = helper.Settings["merchantid"];
			if (string.IsNullOrEmpty(merchantId))
			{
				Log.Instance.LogError("SiSow: Missing PaymentProvider.Config  field with name: merchantid" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var merchantKey = helper.Settings["merchantkey"];
			if (string.IsNullOrEmpty(merchantKey))
			{
				Log.Instance.LogError("SiSow: Missing PaymentProvider.Config  field with name: merchantkey" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			#endregion

			var request = new PaymentRequest();

			var orderGuidAsString = orderInfo.UniqueOrderId.ToString();

			var transactionId = orderGuidAsString.Substring(0, 16);

			request.Parameters.Add("shopid", "001");
			request.Parameters.Add("merchantid", merchantId);
			request.Parameters.Add("payment", string.Empty);
			// character purchase ID (max 16 characters)
			request.Parameters.Add("purchaseid", transactionId);

			var totalAmountInCents = orderInfo.ChargedAmountInCents;
			request.Parameters.Add("amount", totalAmountInCents.ToString());


			request.Parameters.Add("issuerid", orderInfo.PaymentInfo.MethodId);
			request.Parameters.Add("testmode", paymentProvider.TestMode.ToString());
			request.Parameters.Add("entrancecode", orderInfo.OrderNodeId.ToString());
			request.Parameters.Add("description", orderInfo.OrderNumber);

			request.Parameters.Add("returnurl", returnUrl);
			request.Parameters.Add("cancelurl", cancelUrl);
			request.Parameters.Add("callbackurl", reportUrl);
			request.Parameters.Add("notifyurl", reportUrl);

			#region esend

			//request.Parameters.Add("shipping_firstname", "#todo - optional");
			//request.Parameters.Add("shipping_lastname", "#todo");
			//request.Parameters.Add("shipping_mail", "#todo - optional");
			//request.Parameters.Add("shipping_company", "#todo - optional");
			//request.Parameters.Add("shipping_address1", "#todo");
			//request.Parameters.Add("shipping_address2", "#todo - optional");
			//request.Parameters.Add("shipping_zip", "#todo");
			//request.Parameters.Add("shipping_city", "#todo");
			//request.Parameters.Add("shipping_country", "#todo");
			//request.Parameters.Add("shipping_countrycode", "#todo");
			//request.Parameters.Add("shipping_phone", "#todo -optional");
			//request.Parameters.Add("weight", "#todo in KG -optional");
			//request.Parameters.Add("shipping", "#todo -optional");
			//request.Parameters.Add("handling", "#todo -optional");

			#endregion

			//de SHA1 waarde van purchaseid/entrancecode/amount/shopid/merchantid/merchantkey
			var sha1Hash = GetSHA1(transactionId + orderInfo.OrderNodeId.ToString() + totalAmountInCents.ToString() + "001" + merchantId + merchantKey);

			request.Parameters.Add("sha1", sha1Hash);
		
			request.PaymentUrlBase = apiURL;

			orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;

			//var responseString = HttpGet(request.PaymentUrlBase + "?" + request.ParametersAsString);

			var responseString = _requestSender.SendRequest(request.PaymentUrlBase, request.ParametersAsString);
			
			Log.Instance.LogError("SiSow responseString: " + responseString + ", paymentProviderNodeId: " + paymentProvider.Id);

			XNamespace ns = "https://www.sisow.nl/Sisow/REST";
			if (responseString != null)
			{
				var issuerXml = XDocument.Parse(responseString);

				var url = issuerXml.Descendants(ns + "issuerurl").FirstOrDefault();
				var trxId = issuerXml.Descendants(ns + "trxid").FirstOrDefault();

				var decodeUrl = Uri.UnescapeDataString(url.Value);

				if (trxId != null)
				{
				
					var returnedTransactionId = trxId.Value;

					orderInfo.PaymentInfo.Url = decodeUrl;
					orderInfo.PaymentInfo.TransactionId = returnedTransactionId;

					uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, returnedTransactionId);

					orderInfo.Save();

				}
				else
				{
					Log.Instance.LogError("SiSow issuerXml: " + issuerXml + ", paymentProviderNodeId: " + paymentProvider.Id);
				}
			}
			return null;
		}

		// compute SHA1
		private static string GetSHA1(string key)
		{
			var sha = new SHA1Managed();
			var enc = new UTF8Encoding();
			var bytes = sha.ComputeHash(enc.GetBytes(key));
			//string sha1 = System.BitConverter.ToString(sha1).Replace("-", "");
			var sha1 = "";
			for (var j = 0; j < bytes.Length; j++)
				sha1 += bytes[j].ToString("x2");
			return sha1;
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return string.Empty;
		}
	}
}