﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.BusinessLogic;

namespace uWebshop.Payment.Sisow
{
	public class Sisow : IPaymentProvider
	{
		public string GetName()
		{
			return "Sisow";
		}

		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}

		private static string HttpGet(string url)
		{
			var req = WebRequest.Create(url)
			          as HttpWebRequest;
			string result;
			using (var resp = req.GetResponse()
			                  as HttpWebResponse)
			{
				var reader =
					new StreamReader(resp.GetResponseStream());
				result = reader.ReadToEnd();
			}
			return result;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			var paymentMethods = new List<PaymentProviderMethod>();
			var paymentProvider = PaymentProvider.GetPaymentProvider(id);

			var helper = new PaymentConfigHelper(paymentProvider);

			var apiURL = paymentProvider.TestMode ? helper.Settings["DirectoryRequestTestUrl"] : helper.Settings["DirectoryRequestUrl"];

			var issuerRequest = HttpGet(apiURL);

			XNamespace ns = "https://www.sisow.nl/Sisow/REST";
			var issuerXml = XDocument.Parse(issuerRequest);

			foreach (var issuer in issuerXml.Descendants(ns + "issuer"))
			{
				var issuerId = issuer.Element(ns + "issuerid").Value;
				var issuerName = issuer.Element(ns + "issuername").Value;

				var paymentImageId = 0;

				var logoDictionaryItem = library.GetDictionaryItem(issuerId + "LogoId");

				if (string.IsNullOrEmpty(logoDictionaryItem))
				{
					int.TryParse(library.GetDictionaryItem(issuerId + "LogoId"), out paymentImageId);
				}

				paymentMethods.Add(new PaymentProviderMethod
					{
						Id = issuerId,
						Description = issuerName,
						Title = issuerName,
						Name = issuerName,
						ProviderName = GetName(),
						ImageId = paymentImageId
					});
			}

			return paymentMethods;
		}
	}
}