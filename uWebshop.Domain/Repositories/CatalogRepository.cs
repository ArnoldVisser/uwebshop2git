﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Repositories
{
	/// <summary>
	///     Repository for retreiving product catalog data from umbraco
	/// </summary>
	// todo: why?  kan deze verwijderd worden?
	public class CatalogRepository
	{
		/// <summary>
		///     Returns a single category
		/// </summary>
		/// <returns>Category</returns>
		public Category GetCategory(Func<Category, bool> predicate)
		{
			return GetAllCategories().Where(predicate).FirstOrDefault();
		}

		/// <summary>
		/// Gets all categories.
		/// </summary>
		/// <returns></returns>
		public List<Category> GetAllCategories()
		{
			return DomainHelper.GetAllCategories().ToList();
		}

		/// <summary>
		///     Returns a list of categories
		/// </summary>
		/// <returns>List of categories</returns>
		public List<Category> GetAllCategories(Func<Category, bool> predicate)
		{
			return DomainHelper.GetAllCategories(true).Where(predicate).ToList();
		}

		/// <summary>
		///     Returns a single product
		/// </summary>
		/// <returns>Product</returns>
		public Product GetProduct(Func<Product, bool> predicate)
		{
			return DomainHelper.GetAllProducts().Where(predicate).FirstOrDefault();
		}

		/// <summary>
		///     Returns a list of all products
		/// </summary>
		/// <returns>List of products</returns>
		public IEnumerable<Product> GetAllProducts()
		{
			return DomainHelper.GetAllProducts();
		}

		/// <summary>
		///     Retuns a list of products
		/// </summary>
		/// <returns>List of products</returns>
		public List<Product> GetAllProducts(Func<Product, bool> predicate)
		{
			return GetAllProducts().Where(predicate).ToList();
		}

		/// <summary>
		/// Gets the pricing variant.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		[Obsolete("Use GetProductVariant")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public ProductVariant GetPricingVariant(int id)
		{
			return GetAllPricingVariants().FirstOrDefault(x => x.Id == id);
		}

		/// <summary>
		/// Gets the pricing variant.
		/// </summary>
		/// <param name="predicate">The predicate.</param>
		/// <returns></returns>
		[Obsolete("Use GetProductVariant")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public ProductVariant GetPricingVariant(Func<ProductVariant, bool> predicate)
		{
			return GetAllPricingVariants().Where(predicate).FirstOrDefault();
		}

		/// <summary>
		/// Gets all pricing variants.
		/// </summary>
		/// <returns></returns>
		[Obsolete("Use GetAllProductVariants")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public IEnumerable<ProductVariant> GetAllPricingVariants()
		{
			return DomainHelper.GetAllProductVariants();
		}

		/// <summary>
		/// Gets all pricing variants.
		/// </summary>
		/// <param name="predicate">The predicate.</param>
		/// <returns></returns>
		[Obsolete("Use GetAllProductVariants")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public List<ProductVariant> GetAllPricingVariants(Func<ProductVariant, bool> predicate)
		{
			return GetAllPricingVariants().Where(predicate).ToList();
		}


		/// <summary>
		///     Returns a single Product Variant
		/// </summary>
		/// <returns>Pricing</returns>
		public ProductVariant GetProductVariant(int id)
		{
			return GetAllProductVariants().FirstOrDefault(x => x.Id == id);
		}

		/// <summary>
		/// Gets the product variant.
		/// </summary>
		/// <param name="predicate">The predicate.</param>
		/// <returns></returns>
		public ProductVariant GetProductVariant(Func<ProductVariant, bool> predicate)
		{
			return GetAllProductVariants().Where(predicate).FirstOrDefault();
		}

		/// <summary>
		///     Returns a list of all Product Variants
		/// </summary>
		/// <returns>List of Product Variants</returns>
		public IEnumerable<ProductVariant> GetAllProductVariants()
		{
			return DomainHelper.GetAllProductVariants();
		}

		/// <summary>
		///     Returns a list of all Product Variants
		/// </summary>
		/// <returns>List of Product Variants</returns>
		public List<ProductVariant> GetAllProductVariants(Func<ProductVariant, bool> predicate)
		{
			return GetAllProductVariants().Where(predicate).ToList();
		}

		/// <summary>
		/// Gets the order discount.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <returns></returns>
		public List<IOrderDiscount> GetOrderDiscount(OrderInfo orderInfo)
		{
			return IO.Container.Resolve<IDiscountService>().GetApplicableDiscountsForOrder(orderInfo);
		}

		// todo: implement differently ^_^
		/// <summary>
		/// Gets the coupon.
		/// </summary>
		/// <param name="couponCode">The coupon code.</param>
		/// <returns></returns>
		public DiscountBase GetCoupon(string couponCode)
		{
			IEnumerable<DiscountOrder> couponDiscounts = DomainHelper.GetObjectsByAlias<DiscountOrder>(DiscountOrder.NodeAlias); //DomainHelper.GetObjectsByAlias<DiscountBase>(DiscountOrder.NodeAlias);

			return couponDiscounts != null ? couponDiscounts.FirstOrDefault(x => x.Disable == false && x.CouponCode == couponCode) : null;
		}

		///// <summary>
		///// Returns a single sale of a pricing
		///// </summary>
		///// <returns>Sale</returns>
		//public VariantSale GetVariantSale(int pricingVariantId)
		//{
		//	var sales = DomainHelper.GetObjectsByAlias<VariantSale>("uwbsVariantSale");

		//	if (sales != null)
		//	{
		//		sales = sales.Where(x => x.IsActive);
		//	}

		//	if (sales != null && sales.Any())
		//	{
		//		var salesForPricing = new List<VariantSale>();
		//		foreach (var sale in sales)
		//		{
		//			if (sale.PricingVariants != null)
		//			{
		//				var pricings = sale.PricingVariants.Where(x => x.Id == pricingVariantId);

		//				if (pricings != null && pricings.Any())
		//				{
		//					salesForPricing.Add(sale);
		//				}
		//			}
		//		}

		//		VariantSale saleWithLargestDiscount = null;
		//		if (salesForPricing.Count > 0) // (> 1)
		//		{
		//			foreach (var sale in salesForPricing)
		//			{
		//				if (saleWithLargestDiscount == null)
		//				{
		//					saleWithLargestDiscount = sale;
		//				}
		//			}
		//		}

		//		return saleWithLargestDiscount;
		//	}

		//	return null;
		//}
	}
}