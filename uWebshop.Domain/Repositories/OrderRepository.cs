﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using uWebshop.DataAccess;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Repositories
{
	/// <summary>
	/// Repository for retreiving order data from umbraco
	/// </summary>
	internal class OrderRepository : IOrderRepository
	{
		/// <summary>
		/// Returns a single orderinfo
		/// </summary>
		/// <param name="uniqueOrderId"></param>
		/// <returns>
		/// Order
		/// </returns>
		public OrderInfo GetOrderInfo(Guid uniqueOrderId)
		{
			var orderInfoData = uWebshopOrders.GetOrderInfo(uniqueOrderId);

			return orderInfoData != null ? OrderInfo.CreateOrderInfoFromOrderData(orderInfoData) : null;
		}

		public int DetermineLastOrderId()
		{
			return uWebshopOrders.DetermineLastOrderId();
		}

		/// <summary>
		/// Returns a single orderinfo
		/// </summary>
		/// <param name="uniqueOrderId"></param>
		/// <returns>
		/// Order
		/// </returns>
		public XmlDocument GetOrderInfoXML(Guid uniqueOrderId)
		{
			OrderInfo orderInfo = GetOrderInfo(uniqueOrderId);
			string orderInfoXmlString = DomainHelper.SerializeObjectToXmlString(orderInfo);
			var xmlDoc = new XmlDocument();
			if (orderInfoXmlString != null) xmlDoc.LoadXml(orderInfoXmlString);

			return xmlDoc;
		}

		/// <summary>
		/// Get the order based on the transaction ID returned from the Payment Provider
		/// </summary>
		/// <param name="transactionId"></param>
		/// <returns></returns>
		public OrderInfo GetOrderInfo(string transactionId)
		{
			return OrderInfo.CreateOrderInfoFromOrderData(uWebshopOrders.GetOrderInfo(transactionId));
		}

		/// <summary>
		/// Get the orders based on the customer ID
		/// </summary>
		/// <param name="customerId">The customer unique identifier.</param>
		/// <returns>All the orders for the customer</returns>
		public IEnumerable<OrderInfo> GetOrdersFromCustomer(int customerId)
		{
			return (uWebshopOrders.GetOrdersFromCustomer(customerId)).Select(OrderInfo.CreateOrderInfoFromOrderData);
		}

		/// <summary>
		/// Get the orders based on the customer ID
		/// </summary>
		/// <param name="customerUsername">The customer username.</param>
		/// <returns>
		/// All the orders for the customer
		/// </returns>
		public IEnumerable<OrderInfo> GetOrdersFromCustomer(string customerUsername)
		{
			return (uWebshopOrders.GetOrdersFromCustomer(customerUsername)).Select(OrderInfo.CreateOrderInfoFromOrderData);
		}

		/// <summary>
		/// Saves the order.
		/// </summary>
		/// <param name="orderInfo">The order.</param>
		public void SaveOrderInfo(OrderInfo orderInfo)
		{
			uWebshopOrders.StoreOrder(orderInfo.ToOrderData());
		}
	}
}