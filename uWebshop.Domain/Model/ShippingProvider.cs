﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using uWebshop.Common;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Namespace = "")]
	[Serializable]
	[ContentType(ParentContentType = typeof (ShippingProviderSectionContentType), Name = "Shipping Provider", Description = "#ShippingProviderDescription", Alias = NodeAlias, Icon = ContentIcon.BaggageCartBoxLabel, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (ShippingProviderMethodNode)})]
	public class ShippingProvider : uWebshopEntity
	{
		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsShippingProvider";
		/// <summary>
		/// The shipping provider repository node alias
		/// </summary>
		public const string ShippingProviderRepositoryNodeAlias = "uwbsShippingProviderRepository";
		/// <summary>
		/// The shipping provider section node alias
		/// </summary>
		public const string ShippingProviderSectionNodeAlias = "uwbsShippingProviderSection";
		/// <summary>
		/// The shipping provider zone section node alias
		/// </summary>
		public const string ShippingProviderZoneSectionNodeAlias = "uwbsShippingProviderZoneSection";

		private List<ShippingProviderMethod> _shippingProviderMethods;

		/// <summary>
		/// Determines whether [is applicable automatic order] [the specified order information].
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <returns></returns>
		public bool IsApplicableToOrder(OrderInfo orderInfo)
		{
			switch (TypeOfRange)
			{
				case ShippingRangeType.Quantity:
					int quantity = orderInfo.OrderLines.Sum(line => line.ProductInfo.ItemCount.GetValueOrDefault(1)); // orderInfo.OrderLines.Aggregate<OrderLine, decimal>(0, (current, orderLine) => (decimal) (current + orderLine.ProductInfo.ItemCount));
					return (quantity >= RangeFrom && quantity < RangeTo);
				case ShippingRangeType.OrderAmount:
					int subTotal = orderInfo.OrderLines.Sum(orderLine => orderLine.OrderLineAmountInCents);
					return (subTotal >= RangeFrom && subTotal < RangeTo);
				case ShippingRangeType.Weight:
					double weight = orderInfo.OrderLines.Sum(orderLine => orderLine.OrderLineWeight);
					return (weight >= Convert.ToDouble(RangeFrom) && weight < Convert.ToDouble(RangeTo));
				case ShippingRangeType.None:
					return true;
			}
			return false;
		}

		/// <summary>
		/// Loads the shipping methods.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<ShippingProviderMethod> LoadShippingMethods()
		{
			//var shippingMethodList = new List<ShippingProviderMethod>();
			List<IShippingProvider> shippingProviders = ShippingProviderHelper.GetAllShippingProvidersIncludingCustomProviders();
			return shippingProviders.Where(shippingProvider => shippingProvider.GetName() == Title).SelectMany(shippingProvider => shippingProvider.GetAllShippingMethods(Id));

			//if (shippingProviders != null && shippingProviders.Count > 0)
			//{
			//	foreach (var shippingProvider in shippingProviders.Where(shippingProvider => shippingProvider.GetName() == Title))
			//	{
			//		shippingMethodList.AddRange(shippingProvider.GetAllShippingMethods(Id).Select(shippingtMethod => (shippingtMethod)));
			//	}
			//}

			//return shippingMethodList;
		}

		/// <summary>
		/// Gets the name.
		/// </summary>
		/// <returns></returns>
		public string GetName()
		{
			return Name;
		}

		#region properties

		/// <summary>
		///     Payment Provider Title
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "title", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#Title", Description = "#TitleDescription", SortOrder = 1)]
		public string Title { get; set; }

		/// <summary>
		///     Payment Provider Description
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "description", DataType = DataType.RichText, Tab = ContentTypeTab.Global, Name = "#Description", Description = "#DescriptionDescription", SortOrder = 2)]
		public string Description { get; set; }

		/// <summary>
		///     Id of the Shipping Provider Image
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "image", DataType = DataType.MediaPicker, Tab = ContentTypeTab.Global, Name = "#Image", Description = "#ImageDescription", SortOrder = 3)]
		public int ImageId { get; set; }

		/// <summary>
		///     The type of shipping provider
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "type", DataType = DataType.ShippingProviderType, Tab = ContentTypeTab.Details, Name = "#ShippingProviderType", Description = "#ShippingProviderTypeDescription", SortOrder = 4)]
		public ShippingProviderType Type { get; set; }

		/// <summary>
		///     Get the rangetype for the shipping provider
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "rangeType", DataType = DataType.ShippingProviderRangeType, Tab = ContentTypeTab.Details, Name = "#ShippingProviderRangeType", Description = "#ShippingProviderRangeTypeDescription", SortOrder = 5)]
		public ShippingRangeType TypeOfRange { get; set; }

		/// <summary>
		///     Get the range start point for this shipping provider
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "rangeStart", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#RangeStart", Description = "#RangeStartDescription", SortOrder = 6)]
		public decimal RangeFrom { get; set; }

		/// <summary>
		///     Get the range endpoint of this shipping provider
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "rangeEnd", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#RangeEnd", Description = "#RangeEndDescription", SortOrder = 7)]
		public decimal RangeTo { get; set; }

		/// <summary>
		///     Check if the shipping provider should overrule all others
		///     This can be used to set free shipping even if there is weight in the basket.
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "overrule", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Details, Name = "#Overrule", Description = "#OverruleDescription", SortOrder = 9)]
		public bool Overrule { get; set; }

		/// <summary>
		///     Zones for this payment provider
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "zone", DataType = DataType.MultiContentPickerShippingZones, Tab = ContentTypeTab.Details, Name = "#Zone", Description = "#ZoneDescription", SortOrder = 10)]
		public Zone Zone { get; set; }

		/// <summary>
		///     Provider in testmode?
		///     true = testmode enabled
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "testMode", DataType = DataType.EnableDisable, Tab = ContentTypeTab.Details, Name = "#TestMode", Description = "#TestModeDescription", SortOrder = 11)]
		public bool TestMode { get; set; }

		/// <summary>
		///     Get the amountcost of the shipping provider
		/// </summary>
		[IgnoreDataMember]
		public int PriceInCents
		{
			get
			{
				// todo: costly!
				ShippingProviderMethod shippingProviderMethod = ShippingProviderMethods.OrderBy(x => x.PriceWithVatInCents).FirstOrDefault();
				return shippingProviderMethod != null ? shippingProviderMethod.PriceWithVatInCents : 0;
			}
			set { }
		}

		/// <summary>
		///     Full filename of the shippping provider DLL;
		/// </summary>
		[IgnoreDataMember]
		public string DLLName { get; set; }

		/// <summary>
		///     Vat percentage for this shipping provider
		/// </summary>
		[DataMember]
		public decimal Vat { get; set; }

		/// <summary>
		/// Gets or sets the vat amount information cents.
		/// </summary>
		/// <value>
		/// The vat amount information cents.
		/// </value>
		[IgnoreDataMember]
		public decimal VatAmountInCents
		{
			get { return PriceWithVatInCents - PriceWithoutVatInCents; }
			set { }
		}

		/// <summary>
		///     Gets a list of shipping methods
		/// </summary>
		[DataMember]
		public IEnumerable<ShippingProviderMethod> ShippingProviderMethods
		{
			get
			{
				if (_shippingProviderMethods != null)
				{
					return _shippingProviderMethods;
				}

				var shippingProviderMethodList = new List<ShippingProviderMethod>();

				foreach (var child in IO.Container.Resolve<ICMSContentService>().GetReadonlyById(Id).ChildrenAsList.Where(x => x.NodeTypeAlias == ShippingProviderMethodNode.NodeAlias))
				{
					var shippingtMethodNode = new ShippingProviderMethodNode(child.Id);

					var shippingMethodNodeBased = new ShippingProviderMethod {Id = shippingtMethodNode.Id.ToString(), Name = shippingtMethodNode.Title, Title = shippingtMethodNode.Title, Image = shippingtMethodNode.Image, ProviderName = Title, Disabled = shippingtMethodNode.Disabled, PriceInCents = shippingtMethodNode.PriceInCents, Vat = shippingtMethodNode.Vat};

					shippingProviderMethodList.Add(shippingMethodNodeBased);
				}


				shippingProviderMethodList.AddRange(LoadShippingMethods());


				if (!shippingProviderMethodList.Any())
				{
					var shippingMethodDummy = new ShippingProviderMethod {Id = Id.ToString(), Name = Title, Title = Title, ProviderName = Title, PriceInCents = 0};

					Log.Instance.LogWarning(string.Format("ShippingProvider: {0} Without Methods, fallback to code created dummy method", Title));
					shippingProviderMethodList.Add(shippingMethodDummy);
				}

				_shippingProviderMethods = shippingProviderMethodList;
				return _shippingProviderMethods;
			}
			set { }
		}

		/// <summary>
		///     Shipping Provider Amount EXCLUDING Vat
		/// </summary>
		[IgnoreDataMember]
		public int PriceWithoutVatInCents
		{
			get { return IO.Container.Resolve<ISettingsService>().IncludingVat ? VatCalculator.WithoutVat(PriceInCents, Vat) : PriceInCents; }
		}

		/// <summary>
		///     Shipping Provider Amount INCLUDING Vat
		/// </summary>
		[IgnoreDataMember]
		public int PriceWithVatInCents
		{
			get { return IO.Container.Resolve<ISettingsService>().IncludingVat ? PriceInCents : VatCalculator.WithVat(PriceInCents, Vat); }
		}

		#endregion


	}
}