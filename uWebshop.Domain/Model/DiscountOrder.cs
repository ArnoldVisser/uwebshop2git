﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using uWebshop.Common;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (DiscountOrderSectionContentType), Name = "Discount Order", Description = "#DiscountOrderDescription", Alias = NodeAlias, Icon = ContentIcon.Scissors, Thumbnail = ContentThumbnail.Folder)]
	public class DiscountOrder : DiscountBase, IOrderDiscount
	{
		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsDiscountOrder";
		/// <summary>
		/// The section node alias
		/// </summary>
		public const string SectionNodeAlias = "uwbsDiscountOrderSection";
		/// <summary>
		/// The number of items condition property alias
		/// </summary>
		public const string NumberOfItemsConditionPropertyAlias = "numberOfItemsCondition";
		/// <summary>
		/// The order condition property alias
		/// </summary>
		public const string OrderConditionPropertyAlias = "orderCondition";
		/// <summary>
		/// The repository node alias
		/// </summary>
		public static string RepositoryNodeAlias = "uwbsDiscountRepository";
		private List<int> _affectedOrderlines;
		private DiscountOrderCondition? _discountOrderCondition;

		private List<int> _items;
		private int? _numberOfItemsCondition;

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscountOrder"/> class.
		/// </summary>
		public DiscountOrder()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscountOrder"/> class.
		/// </summary>
		/// <param name="nodeId">The node unique identifier.</param>
		public DiscountOrder(int nodeId) : base(nodeId)
		{
		}

		/// <summary>
		/// Gets or sets the items.
		/// </summary>
		/// <value>
		/// The items.
		/// </value>
		[Browsable(false)]
		[Obsolete("Use RequiredItemIds")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)] 
		public List<int> Items
		{
			get { return RequiredItemIds; }
			set { RequiredItemIds = value; }
		}

		/// <summary>
		/// Gets or sets the discount order condition.
		/// </summary>
		/// <value>
		/// The discount order condition.
		/// </value>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[Obsolete("Use Condition")]
		public DiscountOrderCondition DiscountOrderCondition
		{
			get { return Condition; }
			set { Condition = value; }
		}

		/// <summary>
		///     List of products this discount applies to
		/// </summary>
		[ContentPropertyType(Alias = "items", DataType = DataType.MultiNodePicker, Tab = ContentTypeTab.Filter, Name = "#Items", Description = "#ItemsDescription", SortOrder = 3)]
		public List<int> RequiredItemIds
		{
			get
			{
				if (_items == null)
				{
					_items = new List<int>();

					var property = Node.GetProperty("items");

					if (property != null)
					{
						List<string> nodeIds = property.Value.Split(',').ToList();

						// dit wil je niet, want dan worden alle productIds uit een categorie opgeslagen in de order XML
						//var categories = nodeIds.Select(i => DomainHelper.GetCategoryById(Convert.ToInt32(i))).Where(category => category != null);
						//foreach (var category in categories)
						//{
						//	nodeIds.AddRange(category.ProductsRecursive.Select(x => x.Id.ToString()));
						//}

						foreach (string nodeId in nodeIds.Distinct())
						{
							int nodeInt = 0;
							int.TryParse(nodeId, out nodeInt);

							if (nodeInt > 0)
							{
								_items.Add(nodeInt);
							}
						}
					}
				}

				return _items;
			}
			set { _items = value; }
		}

		/// <summary>
		/// Gets the affected orderlines.
		/// </summary>
		/// <value>
		/// The affected orderlines.
		/// </value>
		[ContentPropertyType(Alias = "affectedOrderlines", DataType = DataType.MultiContentPickerProducts, Tab = ContentTypeTab.Filter, Name = "#AffectedOrderlines", Description = "#AffectedOrderlines", SortOrder = 4)]
		public List<int> AffectedOrderlines
		{
			get
			{
				if (_affectedOrderlines == null)
				{
					_affectedOrderlines = new List<int>();

					var property = Node.GetProperty("affectedOrderlines");

					if (property != null)
					{
						List<string> nodeIds = property.Value.Split(',').ToList();


						foreach (string nodeId in nodeIds.Distinct())
						{
							int nodeInt = 0;
							int.TryParse(nodeId, out nodeInt);

							if (nodeInt > 0)
							{
								_affectedOrderlines.Add(nodeInt);
							}
						}
					}
				}

				return _affectedOrderlines;
			}
			set { _affectedOrderlines = value; }
		}

		/// <summary>
		///     Discount condition (None, OnTheXthItem, PerSetOfItems)
		/// </summary>
		[ContentPropertyType(Alias = "orderCondition", DataType = DataType.DiscountOrderCondition, Tab = ContentTypeTab.Conditions, Name = "#OrderCondition", Description = "#OrderConditionDescription")]
		public DiscountOrderCondition Condition
		{
			get
			{
				if (_discountOrderCondition.HasValue) return _discountOrderCondition.GetValueOrDefault();
				var property = Node.GetMultiStoreItem(OrderConditionPropertyAlias);

				if (property != null && !string.IsNullOrEmpty(property.Value))
					return (_discountOrderCondition = (DiscountOrderCondition) Enum.Parse(typeof (DiscountOrderCondition), property.Value)).GetValueOrDefault();

				return (_discountOrderCondition = DiscountOrderCondition.None).GetValueOrDefault();
			}
			set { _discountOrderCondition = value; }
		}

		/// <summary>
		///     Number of items required for OnTheXthItem and PerSetOfItems conditions
		/// </summary>
		[ContentPropertyType(Alias = "numberOfItemsCondition", DataType = DataType.Numeric, Tab = ContentTypeTab.Conditions, Name = "#NumberOfItemsCondition", Description = "#NumberOfItemsConditionDescription")]
		public int NumberOfItemsCondition
		{
			get
			{
				if (_numberOfItemsCondition.HasValue) return _numberOfItemsCondition.GetValueOrDefault();
				int minimumAmount = 0;
				var property = Node.GetMultiStoreItem(NumberOfItemsConditionPropertyAlias);

				if (property != null) int.TryParse(property.Value, out minimumAmount);

				return (_numberOfItemsCondition = minimumAmount).GetValueOrDefault();
			}
			set { _numberOfItemsCondition = value; }
		}

		public int MinimalOrderAmount
		{
			get { return MinimumOrderAmountInCents; }
		}

		#region condition tab

		private string _couponCode;
		private List<string> _memberGroups;
		private int? _minimumAmount;
		private bool? _oncePerCustomer;

		/// <summary>
		///     Minimum amount of the order before discount is valid
		/// </summary>
		[ContentPropertyType(Alias = "minimumAmount", DataType = DataType.Price, Tab = ContentTypeTab.Conditions, Name = "#MinimumAmount", Description = "#MinimumAmountDescription", SortOrder = 20)]
		public int MinimumOrderAmountInCents
		{
			get
			{
				if (!_minimumAmount.HasValue)
				{
					int minimumAmount;
					int.TryParse(StoreHelper.GetMultiStoreItem(Id, "minimumAmount") ?? string.Empty, out minimumAmount);
					_minimumAmount = minimumAmount;
				}
				return _minimumAmount.GetValueOrDefault();
			}
			set { _minimumAmount = value; }
		}


		/// <summary>
		///     Code of the coupon
		/// </summary>
		[ContentPropertyType(Alias = "couponCode", DataType = DataType.String, Tab = ContentTypeTab.Conditions, Name = "#CouponCode", Description = "#CouponCodeDescription", SortOrder = 21)]
		public string CouponCode
		{
			get { return _couponCode ?? (_couponCode = StoreHelper.GetMultiStoreItem(Id, "couponCode") ?? string.Empty); }
			set { _couponCode = value; }
		}

		/// <summary>
		///     Code of the coupon
		/// </summary>
		[ContentPropertyType(Alias = "couponCodes", DataType = DataType.CouponCodes, Tab = ContentTypeTab.Conditions, Name = "#CouponCodes", Description = "#CouponCodesDescription", SortOrder = 22)]
		public string CouponCodes { get; set; }

		/// <summary>
		///     Can this discount be only used once per customer?
		/// </summary>
		[ContentPropertyType(Alias = "oncePerCustomer", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Conditions, Name = "#OncePerCustomer", Description = "#OncePerCustomerDescription", SortOrder = 23)]
		public bool OncePerCustomer
		{
			get
			{
				if (!_oncePerCustomer.HasValue)
				{
					string property = StoreHelper.GetMultiStoreItem(Id, "oncePerCustomer");

					_oncePerCustomer = property != null && (property == "1" || property == "true" || property == "enable");
				}
				return _oncePerCustomer.GetValueOrDefault();
			}
		}
		#endregion

		/// <summary>
		/// Gets the original unique identifier.
		/// </summary>
		/// <value>
		/// The original unique identifier.
		/// </value>
		public int OriginalId
		{
			get { return Id; }
		}
	}
}