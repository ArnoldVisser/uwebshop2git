﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Businesslogic;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	///     Variant of a product
	///     Ordered by group
	/// </summary>
	[DataContract(Namespace = "", IsReference = true)]
	[ContentType(ParentContentType = typeof (Catalog), Name = "Product Variant", Description = "#ProductVariantDescription", Alias = NodeAlias, Icon = ContentIcon.MagnetSmall, Thumbnail = ContentThumbnail.Folder)]
	public class ProductVariant : uWebshopEntity
	{
		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsProductVariant";
		private string _description;
		private bool? _disable;
		private Product _product;
		private int? _template;
		private string _title;
		internal string _url;

		#region Helper properties (with/withoutVat and Non-cents)

		/// <summary>
		///     Price of the product variant based on the price value
		///     !NO VAT RULES APPLIED!
		/// </summary>
		[DataMember]
		public decimal OriginalPrice
		{
			get { return OriginalPriceInCents/100m; }
			set { }
		}

		/// <summary>
		///     Price of the product + this product variant INCLUDING Vat
		/// </summary>
		[DataMember]
		public decimal PriceIncludingProductPriceWithVat
		{
			get { return PriceIncludingProductPriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the original price including product price with vat.
		/// </summary>
		/// <value>
		/// The original price including product price with vat.
		/// </value>
		[DataMember]
		public decimal OriginalPriceIncludingProductPriceWithVat
		{
			get { return OriginalPriceIncludingProductPriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     Price of the product + this product variant EXCLUDING Vat
		/// </summary>
		[DataMember]
		public decimal PriceIncludingProductPriceWithoutVat
		{
			get { return PriceIncludingProductPriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the original price including product price without vat.
		/// </summary>
		/// <value>
		/// The original price including product price without vat.
		/// </value>
		[DataMember]
		public decimal OriginalPriceIncludingProductPriceWithoutVat
		{
			get { return OriginalPriceIncludingProductPriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price with vat.
		/// </summary>
		/// <value>
		/// The price with vat.
		/// </value>
		[DataMember]
		public decimal PriceWithVat
		{
			get { return PriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price without vat.
		/// </summary>
		/// <value>
		/// The price without vat.
		/// </value>
		[DataMember]
		public decimal PriceWithoutVat
		{
			get { return PriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     Price of the product + this product variant INCLUDING Vat
		/// </summary>
		[DataMember]
		public int PriceIncludingProductPriceWithVatInCents
		{
			get { return Product.PricesIncludingVat ? PriceIncludingProductPriceInCents : VatCalculator.WithVat(PriceIncludingProductPriceInCents, Product.Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the original price including product price with vat information cents.
		/// </summary>
		/// <value>
		/// The original price including product price with vat information cents.
		/// </value>
		[DataMember]
		public int OriginalPriceIncludingProductPriceWithVatInCents
		{
			get { return Product.PricesIncludingVat ? OriginalPriceIncludingProductPriceInCents : VatCalculator.WithVat(OriginalPriceIncludingProductPriceInCents, Product.Vat); }
			set { }
		}

		/// <summary>
		///     Price of the product + this product variant EXCLUDING Vat
		/// </summary>
		[DataMember]
		public int PriceIncludingProductPriceWithoutVatInCents
		{
			get { return Product.PricesIncludingVat ? VatCalculator.WithoutVat(PriceIncludingProductPriceInCents, Product.Vat) : PriceIncludingProductPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the original price including product price without vat information cents.
		/// </summary>
		/// <value>
		/// The original price including product price without vat information cents.
		/// </value>
		public int OriginalPriceIncludingProductPriceWithoutVatInCents
		{
			get { return Product.PricesIncludingVat ? VatCalculator.WithoutVat(OriginalPriceIncludingProductPriceInCents, Product.Vat) : OriginalPriceIncludingProductPriceInCents; }
			set { }
		}

		#endregion

		#region Non-cents

		/// <summary>
		///     The discounted tax amount
		/// </summary>
		[DataMember]
		public decimal DiscountedVatAmount
		{
			get { return DiscountedVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     Tax amount
		/// </summary>
		[DataMember]
		public decimal OriginalVatAmount
		{
			get { return OriginalVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     The discounted price with tax
		/// </summary>
		[DataMember]
		public decimal DiscountedPriceWithVat
		{
			get { return DiscountedPriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price without vat.
		/// </summary>
		/// <value>
		/// The discounted price without vat.
		/// </value>
		public decimal DiscountedPriceWithoutVat
		{
			get { return DiscountedPriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     The price of the pricing without VAT
		/// </summary>
		[DataMember]
		public decimal OriginalPriceWithoutVat
		{
			get { return OriginalPriceWithoutVatInCents/100m; }
			set { }
		}


		/// <summary>
		///     The price of the pricing with VAT
		/// </summary>
		[DataMember]
		public decimal OriginalPriceWithVat
		{
			get { return OriginalPriceWithVatInCents/100m; }
			set { }
		}

		#endregion

		#region Vat calculations

		/// <summary>
		///     Tax amount
		/// </summary>
		[DataMember]
		public int OriginalVatAmountInCents
		{
			get { return Product.PricesIncludingVat ? VatCalculator.VatAmountFromWithVat(RangedPriceInCents, Vat) : VatCalculator.VatAmountFromWithoutVat(RangedPriceInCents, Vat); } // OriginalPriceWithoutVat * (Vat / 100m); }
			set { }
		}

		/// <summary>
		///     The discounted tax amount
		/// </summary>
		[DataMember]
		public int DiscountedVatAmountInCents
		{
			get { return Product.PricesIncludingVat ? VatCalculator.VatAmountFromWithVat(DiscountedPriceInCents, Vat) : VatCalculator.VatAmountFromWithoutVat(DiscountedPriceInCents, Vat); } // OriginalPriceWithoutVat * (Vat / 100m); }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price with vat information cents.
		/// </summary>
		/// <value>
		/// The discounted price with vat information cents.
		/// </value>
		[DataMember]
		public int DiscountedPriceWithVatInCents
		{
			get { return Product.PricesIncludingVat ? DiscountedPriceInCents : VatCalculator.WithVat(DiscountedPriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price without vat information cents.
		/// </summary>
		/// <value>
		/// The discounted price without vat information cents.
		/// </value>
		[DataMember]
		public int DiscountedPriceWithoutVatInCents
		{
			get { return Product.PricesIncludingVat ? VatCalculator.WithoutVat(DiscountedPriceInCents, Vat) : DiscountedPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price with vat information cents.
		/// </summary>
		/// <value>
		/// The price with vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithVatInCents
		{
			get { return Product.PricesIncludingVat ? PriceInCents : VatCalculator.WithVat(PriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the price without vat information cents.
		/// </summary>
		/// <value>
		/// The price without vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithoutVatInCents
		{
			get { return Product.PricesIncludingVat ? VatCalculator.WithoutVat(PriceInCents, Vat) : PriceInCents; }
			set { }
		}

		/// <summary>
		///     The price of the pricing without VAT
		/// </summary>
		[DataMember]
		public int OriginalPriceWithoutVatInCents
		{
			get { return Product.PricesIncludingVat ? VatCalculator.WithoutVat(RangedPriceInCents, Vat) : RangedPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the original price with vat information cents.
		/// </summary>
		/// <value>
		/// The original price with vat information cents.
		/// </value>
		[DataMember]
		public int OriginalPriceWithVatInCents
		{
			get { return Product.PricesIncludingVat ? RangedPriceInCents : VatCalculator.WithVat(RangedPriceInCents, Vat); }
			set { }
		}

		#endregion

		/// <summary>
		///     Is this product variant orderable:
		///     stock should be higher then 0
		///     if stock is lower then 0, but backorder is enabled
		///     if the stockstatus is disabled
		/// </summary>
		[DataMember]
		public bool Orderable
		{
			get { return Stock > 0 || Stock <= 0 && BackorderStatus || StockStatus == false; }
			set { }
		}

		/// <summary>
		///     The group the variant is in
		/// </summary>
		[DataMember]
		[Obsolete("Use Group")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public string PricingVariantGroup
		{
			get { return Group; }
		}

		[DataMember]
		internal int RangedPriceInCents
		{
			get
			{
				if (Ranges != null)
				{
					Range range = Ranges.FirstOrDefault(x => x.From <= 1 && x.PriceInCents != 0);
					if (range != null)
					{
						return range.PriceInCents;
					}
				}
				return OriginalPriceInCents;
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price information cents.
		/// </summary>
		/// <value>
		/// The discounted price information cents.
		/// </value>
		protected int DiscountedPriceInCents
		{
			get { return ProductVariantDiscount.GetAdjustedPrice(RangedPriceInCents); }
			set { }
		}

		internal string StoreAlias;

		/// <summary>
		///     Gets the sale of the pricing
		/// </summary>
		[DataMember]
		public DiscountProduct ProductVariantDiscount
		{
			get { return IO.Container.Resolve<IProductDiscountRepository>().GetDiscountByProductId(Id, StoreAlias); }
			set { }
		}

		/// <summary>
		///     Returns if the pricing is discounted
		/// </summary>
		[DataMember]
		public bool IsDiscounted
		{
			get { return ProductVariantDiscount != null; }
			set { }
		}

		/// <summary>
		///     Original price of the product + this product variant
		/// </summary>
		[DataMember]
		internal int PriceIncludingProductPriceInCents
		{
			get { return Product.PriceInCents + (Product.IsDiscounted && !Product.ProductDiscount.ExcludeVariants && Product.ProductDiscount.DiscountType == DiscountType.Percentage ? PriceInCents - (int) Math.Round(PriceInCents*Product.ProductDiscount.DiscountValue/10000m) : PriceInCents); }
			set { }
		}

		internal int OriginalPriceIncludingProductPriceInCents
		{
			get { return Product.OriginalPriceInCents + OriginalPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the vat.
		/// </summary>
		/// <value>
		/// The vat.
		/// </value>
		[DataMember]
		public decimal Vat
		{
			get { return Product.Vat; }
			set { }
		}

		/// <summary>
		///     Gets the product of the variant
		/// </summary>
		public Product Product
		{
			get { return _product ?? (_product = DomainHelper.GetProductById(ParentId)); }
			set { _product = value; }
		}

		internal static bool IsAlias(string nodeAlias)
		{
			return nodeAlias.StartsWith(NodeAlias);
		}

		//todo: mark naar kijken
		internal static void LoadDataFromPropertyProvider(ProductVariant entity, string storeAlias, IPropertyProvider fields)
		{
			entity._disable = StoreHelper.GetMultiStoreDisableExamine(storeAlias, fields);

			entity._title = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("title", storeAlias, fields);

			entity._description = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary(fields.ContainsKey("RTEItemdescription") ? "RTEItemdescription" : "description", storeAlias, fields);

			entity._template = StoreHelper.GetMultiStoreIntValue("template", storeAlias, fields);
		}

		#region global tab

		/// <summary>
		///     Gets the title of the content
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "title", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#Title", Description = "#TitleDescription", Mandatory = true, SortOrder = 1)]
		public string Title
		{
			get { return _title ?? (_title = StoreHelper.GetMultiStoreItem(Id, "title") ?? string.Empty); }
			set { }
		}

		/// <summary>
		/// ProductVariant SKU
		/// </summary>
		/// <value>
		/// The sku.
		/// </value>
		[DataMember]
		[ContentPropertyType(Alias = "sku", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#SKU", Description = "#SKUDescription", SortOrder = 2)]
		public string SKU { get; set; }

		/// <summary>
		/// Gets or sets the group.
		/// </summary>
		/// <value>
		/// The group.
		/// </value>
		[DataMember]
		[ContentPropertyType(Alias = "group", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#Group", Description = "#GroupDescription", SortOrder = 3)]
		public string Group { get; set; }


		/// <summary>
		/// Gets or sets a value indicating whether [required variant].
		/// </summary>
		/// <value>
		///   <c>true</c> if [required variant]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		[Obsolete("Use Required")]
		public bool RequiredVariant { get { return Required; } set { Required = value; } }

		/// <summary>
		/// Gets or sets a value indicating whether [required variant].
		/// </summary>
		/// <value>
		///   <c>true</c> if [required variant]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		[ContentPropertyType(Alias = "requiredVariant", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Global, Name = "#RequiredVariant", Description = "#RequiredVariantDescription", SortOrder = 4)]
		public bool Required { get; set; }

		/// <summary>
		///     Is this content enabled?
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "disable", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Global, Name = "#Disable", Description = "#DisableDescription", Mandatory = false, SortOrder = 5)]
		public virtual bool Disabled
		{
			get { return _disable ?? (_disable = StoreHelper.GetMultiStoreDisable(Id)).GetValueOrDefault(); }
			set { _disable = value; }
		}

		#endregion

		#region details tab

		/// <summary>
		///     Gets the long description of the content
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "description", DataType = DataType.RichText, Tab = ContentTypeTab.Details, Name = "#Description", Description = "#DescriptionDescription", Mandatory = false, SortOrder = 6)]
		public string Description
		{
			get { return IO.Container.Resolve<ICMSApplication>().ParseInternalLinks(_description ?? (_description = StoreHelper.GetMultiStoreItem(Id, "description") ?? string.Empty)); }
			set { }
		}

		/// <summary>
		///     Length of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "length", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Length", Description = "#LengthDescription", SortOrder = 7)]
		public double Length { get; set; }

		/// <summary>
		///     Width of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "width", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Width", Description = "#WidthDescription", SortOrder = 8)]
		public double Width { get; set; }

		/// <summary>
		///     Height of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "height", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Height", Description = "#HeightDescription", SortOrder = 9)]
		public double Height { get; set; }

		/// <summary>
		///     Weight of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "weight", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Weight", Description = "#WeightDescription", SortOrder = 10)]
		public double Weight { get; set; }

		#endregion

		#region price tab

		[DataMember]
		internal int OriginalPriceInCents { get; set; }

		/// <summary>
		/// Gets or sets the price information cents.
		/// </summary>
		/// <value>
		/// The price information cents.
		/// </value>
		[DataMember]
		[ContentPropertyType(Alias = "price", DataType = DataType.Price, Tab = ContentTypeTab.Price, Name = "#Price", Description = "#PriceDescription", SortOrder = 15)]
		public int PriceInCents
		{
			get { return IsDiscounted ? DiscountedPriceInCents : RangedPriceInCents; }
			set { }
		}

		/// <summary>
		///     Ranges from the range data type
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "ranges", DataType = DataType.Ranges, Tab = ContentTypeTab.Price, Name = "#Ranges", Description = "#RangesDescription", SortOrder = 20)]
		public List<Range> Ranges { get; set; }

		/// <summary>
		///     Gets the stock of the variant
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "stock", DataType = DataType.Stock, Tab = ContentTypeTab.Price, Name = "#Stock", Description = "#StockDescription", SortOrder = 25)]
		public int Stock
		{
			// caching here is impossible (breaks with app wide static caching of products), per-request caching is done some levels deeper
			get { return StoreHelper.GetMultiStoreStock(Id); }
			set { }
		}

		/// <summary>
		///     Gets the number of times this ProductVariant is ordered
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "ordered", DataType = DataType.OrderedCount, Tab = ContentTypeTab.Price, Name = "#Ordered", Description = "#OrderedDescription", SortOrder = 26)]
		public int OrderCount
		{
			get { return UWebshopStock.GetOrderCount(Id); }
			set { }
		}

		/// <summary>
		///     The status of the stock
		///     True = enabled
		///     False = disabled
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "stockStatus", DataType = DataType.EnableDisable, Tab = ContentTypeTab.Price, Name = "#StockStatus", Description = "#StockStatusDescription", SortOrder = 27)]
		public bool StockStatus { get; set; }

		/// <summary>
		///     The status of backorder
		///     True = enabled
		///     False = disabled
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "backorderStatus", DataType = DataType.EnableDisable, Tab = ContentTypeTab.Price, Name = "#BackorderStatus", Description = "#BackorderStatusDescription", SortOrder = 28)]
		public bool BackorderStatus { get; set; }

		#endregion
	}
}