﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using uWebshop.Common;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	/// Discount over a product
	/// </summary>
	[Serializable]
	[DataContract(Namespace = "", IsReference = true)]
	[ContentType(ParentContentType = typeof (DiscountProductSectionContentType), Name = "Discount Product", Description = "#DiscountProductDescription", Alias = NodeAlias, Icon = ContentIcon.PriceTagMinus, Thumbnail = ContentThumbnail.Folder)]
	public class DiscountProduct : DiscountBase, IDiscount
	{
		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsDiscountProduct";
		/// <summary>
		/// The section node alias
		/// </summary>
		public const string SectionNodeAlias = "uwbsDiscountProductSection";

		private bool? _excludeVariants;
		private List<int> _items;
		private List<ProductVariant> _productVariants;
		private List<Product> _products;

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscountProduct"/> class.
		/// </summary>
		public DiscountProduct()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscountProduct"/> class.
		/// </summary>
		/// <param name="nodeId">The node unique identifier.</param>
		public DiscountProduct(int nodeId) : base(nodeId)
		{
		}

		/// <summary>
		///     List of products this discount applies to
		/// </summary>
		internal IEnumerable<int> Items
		{
			get
			{
				if (_items == null)
				{
					_items = new List<int>();

					string property = StoreHelper.GetMultiStoreItem(Id, "products");

					if (!string.IsNullOrEmpty(property))
					{
						_items = property.Split(',').Select(int.Parse).ToList();

						return _items;
					}
				}

				return _items;
			}
			set { }
		}

		/// <summary>
		///     List of products this discount applies to
		/// </summary>
		[ContentPropertyType(Alias = "products", DataType = DataType.MultiContentPickerProducts, Tab = ContentTypeTab.Details, Name = "#Products", Description = "#ProductsDescription", SortOrder = 3)]
		public IEnumerable<Product> Products
		{
			get
			{
				if (_products == null)
				{
					_products = new List<Product>();

					IEnumerable<int> nodeIds = Items;

					if (nodeIds != null)
					{
						IEnumerable<Category> categories = nodeIds.Select(DomainHelper.GetCategoryById).Where(category => category != null);
						_products = nodeIds.Select(id => DomainHelper.GetProductById(id)).Where(product => product != null).ToList();

						foreach (Category category in categories)
						{
							_products.AddRange(category.ProductsRecursive);
						}
					}
					return _products;
				}

				return _products;
			}
			set { }
		}


		/// <summary>
		///     List of products variants this discount applies to
		/// </summary>
		public IEnumerable<ProductVariant> ProductVariants
		{
			get
			{
				if (_productVariants == null)
				{
					_productVariants = new List<ProductVariant>();

					IEnumerable<int> nodeIds = Items;

					if (nodeIds != null)
					{
						//_productVariants = nodeIds.Select(DomainHelper.GetProductVariantById).Where(productVariant => productVariant != null).ToList();
						_productVariants = nodeIds.Select(id => DomainHelper.GetProductVariantById(id)).Where(productVariant => productVariant != null).ToList();
					}
					return _productVariants;
				}

				return _productVariants;
			}
			set { }
		}


		/// <summary>
		/// Gets or sets a value indicating whether [exclude variants].
		/// </summary>
		/// <value>
		///   <c>true</c> if [exclude variants]; otherwise, <c>false</c>.
		/// </value>
		[ContentPropertyType(Alias = "excludeVariants", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Details, Name = "#ExcludeVariants", Description = "#ExcludeVariantsDescription", SortOrder = 4)]
		public bool ExcludeVariants
		{
			get { return _excludeVariants ?? (_excludeVariants = StoreHelper.GetMultiStoreItem(Id, "excludeVariants") == "1").GetValueOrDefault(); }
			set { _excludeVariants = value; }
		}


		/// <summary>
		/// Gets or sets a value indicating whether [is active].
		/// </summary>
		/// <value>
		///   <c>true</c> if [is active]; otherwise, <c>false</c>.
		/// </value>
		public bool IsActive
		{
			get { return !Disable && (!CounterEnabled || Counter > 0); }
			set { }
		}

		/// <summary>
		///     The discount value in cents, based on catalog information
		/// </summary>
		public int GetDiscountAmountInCents(int productId = 0)
		{
			// todo: add NewPrice!!
			if (DiscountType == DiscountType.NewPrice)
			{
				Product productById = DomainHelper.GetProductById(productId);
				
				return DiscountValue - productById.RangedPriceInCents;
			}
			if (DiscountType == DiscountType.Amount)
			{
				return DiscountValue;
			}
			if (productId > 0 && DiscountType == DiscountType.Percentage)
			{
				//var productPrice = Products.Where(p => p.Id == productId).Select(p => p.RangedPriceInCents).SingleOrDefault();
				Product productById = DomainHelper.GetProductById(productId);
				int productPrice = productById == null ? 0 : productById.RangedPriceInCents;
				return (int) (DiscountValue*productPrice/10000m);
			}
			return 0;
		}

		public bool OncePerCustomer
		{
			get { return false; }
		}

		public int MinimumOrderAmountInCents
		{
			get { return 0; }
		}
	}
}