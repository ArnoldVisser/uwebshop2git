﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using uWebshop.Common;
using uWebshop.Domain.Helpers;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Namespace = "")]
	[Serializable]
	public class ProductInfo // note: mirror of OrderedProduct, NOT of Product!
	{
		[XmlIgnore] internal IOrderInfo Order;

		/// <summary>
		/// The product variants
		/// </summary>
		[DataMember] public List<ProductVariantInfo> ProductVariants; // entity - entity relation

		[XmlIgnore] private Product _product;

		#region Data fields

		internal int DiscountId;
		/// <summary>
		/// The unique identifier
		/// </summary>
		[DataMember] public int Id; // todo: think about this coupling and its naming
		private int? _itemCount;

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the changed configuration.
		/// </summary>
		/// <value>
		/// The changed configuration.
		/// </value>
		[DataMember]
		public DateTime ChangedOn { get; set; }

		/// <summary>
		/// Gets or sets the sku.
		/// </summary>
		/// <value>
		/// The sku.
		/// </value>
		[DataMember]
		public string SKU { get; set; }

		/// <summary>
		/// Gets or sets the weight.
		/// </summary>
		/// <value>
		/// The weight.
		/// </value>
		[DataMember]
		public double Weight { get; set; }

		/// <summary>
		/// Gets or sets the length.
		/// </summary>
		/// <value>
		/// The length.
		/// </value>
		[DataMember]
		public double Length { get; set; }

		/// <summary>
		/// Gets or sets the height.
		/// </summary>
		/// <value>
		/// The height.
		/// </value>
		[DataMember]
		public double Height { get; set; }

		/// <summary>
		/// Gets or sets the width.
		/// </summary>
		/// <value>
		/// The width.
		/// </value>
		[DataMember]
		public double Width { get; set; }

		/// <summary>
		/// Gets or sets the original price information cents.
		/// </summary>
		/// <value>
		/// The original price information cents.
		/// </value>
		[DataMember]
		public int OriginalPriceInCents { get; set; }

		/// <summary>
		/// Gets or sets the vat.
		/// </summary>
		/// <value>
		/// The vat.
		/// </value>
		[DataMember]
		public decimal Vat { get; set; }

		/// <summary>
		/// Gets or sets the discount amount information cents.
		/// </summary>
		/// <value>
		/// The discount amount information cents.
		/// </value>
		[DataMember]
		public int DiscountAmountInCents { get; set; }

		/// <summary>
		/// Gets or sets the discount percentage.
		/// </summary>
		/// <value>
		/// The discount percentage.
		/// </value>
		[DataMember]
		public decimal DiscountPercentage { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [discount excluding variants].
		/// </summary>
		/// <value>
		/// <c>true</c> if [discount excluding variants]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool DiscountExcludingVariants { get; set; }

		/// <summary>
		/// Gets or sets the ranges string.
		/// </summary>
		/// <value>
		/// The ranges string.
		/// </value>
		[DataMember]
		public string RangesString { get; set; }

		/// <summary>
		/// Gets or sets the document type alias.
		/// </summary>
		/// <value>
		/// The document type alias.
		/// </value>
		[DataMember]
		public string DocTypeAlias { get; set; }

		/// <summary>
		/// Gets or sets the item count.
		/// </summary>
		/// <value>
		/// The item count.
		/// </value>
		[DataMember]
		public int? ItemCount
		{
			get { return _itemCount; }
			set
			{
				_productRangePriceInCents = null; // reset the cache
				_priceInCentsLoadedFromLegacyXML = null;
				_itemCount = value;
			}
		}

		[XmlIgnore]
		internal int OrderTotalItemCount
		{
			get { return Order != null ? Order.OrderLines.Where(line => line.ProductInfo != null && line.ProductInfo.Id == Id).Sum(line => line.ProductInfo.ItemCount.GetValueOrDefault(1)) : ItemCount.GetValueOrDefault(1); }
		}

		#endregion

		/// <summary>
		/// Gets the product.
		/// </summary>
		/// <value>
		/// The product.
		/// </value>
		/// <exception cref="System.Exception">ProductInfo without ProductId</exception>
		[XmlIgnore]
		public Product Product // weird coupling with umbraco dataprovider class
		{
			get
			{
				// todo: remove or change the usages of this property
				if (_product == null)
				{
					if (Id == 0) throw new Exception("ProductInfo without ProductId");
					_product = DomainHelper.GetProductById(Id);
				}
				return _product;
			}
		}

		internal int DiscountInCents { get; set; }

		#region Calculated properties

		// todo: think about when and how these should be datamembers (don't store in xml, but do give them in json/email xml)

		private int? _priceInCentsLoadedFromLegacyXML;
// ReSharper disable once InconsistentNaming
		internal int? _productRangePriceInCents;

		internal int ProductRangePriceWithoutVariantsInCents
		{
			get { return Ranges.GetRangeAmountForValue(OrderTotalItemCount) ?? OriginalPriceInCents; }
		}

		/// <summary>
		/// Gets or sets the product range price information cents.
		/// </summary>
		/// <value>
		/// The product range price information cents.
		/// </value>
		[DataMember]
		public int ProductRangePriceInCents
		{
			get
			{
				// source data: OriginalPriceInCents & Ranges.Amount & VariantInfo.PriceInCents // todo check
				if (!_productRangePriceInCents.HasValue)
				{
					_productRangePriceInCents = ProductRangePriceWithoutVariantsInCents + ProductVariants.Sum(variant => variant.DiscountedPriceInCents);
				}
				return _productRangePriceInCents.GetValueOrDefault();
			}
			set { _productRangePriceInCents = value; }
		}

		/// <summary>
		/// Gets or sets the ranges.
		/// </summary>
		/// <value>
		/// The ranges.
		/// </value>
		public List<Range> Ranges
		{
			get { return Range.CreateFromString(RangesString); }
			set { RangesString = value.ToRangesString(); }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [is discounted].
		/// </summary>
		/// <value>
		///   <c>true</c> if [is discounted]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool IsDiscounted
		{
			get { return DiscountPercentage > 0 || DiscountAmountInCents > 0; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price information cents.
		/// </summary>
		/// <value>
		/// The price information cents.
		/// </value>
		[DataMember]
		public int PriceInCents
		{
			get
			{
				if (DiscountExcludingVariants)
					return _priceInCentsLoadedFromLegacyXML ?? ProductVariants.Sum(variant => variant.DiscountedPriceInCents) + (int) ((100 - DiscountPercentage)*ProductRangePriceWithoutVariantsInCents)/100 - DiscountAmountInCents;
				return _priceInCentsLoadedFromLegacyXML ?? (int) ((100 - DiscountPercentage)*ProductRangePriceInCents)/100 - DiscountAmountInCents;
				//return /*_priceInCents ?? (_priceInCents = */ IsDiscounted ? DiscountedPriceInCents : ProductRangePriceInCents;
			}
			set { _priceInCentsLoadedFromLegacyXML = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductInfo"/> class.
		/// </summary>
		public ProductInfo()
		{
			ProductVariants = new List<ProductVariantInfo>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductInfo"/> class.
		/// </summary>
		/// <param name="orderedProduct">The ordered product.</param>
		/// <param name="order">The order.</param>
		public ProductInfo(OrderedProduct orderedProduct, IOrderInfo order) : this()
		{
			Order = order;
			_productRangePriceInCents = null;

			// possible todo: change such that above values are loaded from product instead of calculated here

			Id = orderedProduct.ProductId;

			Title = orderedProduct.Title;
			SKU = orderedProduct.SKU;
			Weight = orderedProduct.Weight;
			Length = orderedProduct.Length;
			Width = orderedProduct.Width;
			Height = orderedProduct.Height;
			Vat = orderedProduct.Vat;
			ItemCount = orderedProduct.ItemCount.GetValueOrDefault(1);

			ChangedOn = orderedProduct.UpdateDate;

			RangesString = orderedProduct.RangesString;

			OriginalPriceInCents = orderedProduct.PriceInCents;

			DiscountAmountInCents = orderedProduct.OrderedProductDiscountAmount;
			DiscountPercentage = orderedProduct.OrderedProductDiscountPercentage;

			DocTypeAlias = orderedProduct.DoctypeAlias;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductInfo"/> class.
		/// </summary>
		/// <param name="product">The product.</param>
		/// <param name="order">The order.</param>
		/// <param name="itemCount">The item count.</param>
		public ProductInfo(Product product, IOrderInfo order, int itemCount) : this()
		{
			Order = order;
			_product = product;
			_productRangePriceInCents = null;

			Id = product.Id;
			ItemCount = itemCount;
			Title = product.Title;
			SKU = product.SKU;
			Weight = product.Weight;
			Length = product.Length;
			Width = product.Width;
			Height = product.Height;
			Vat = product.Vat;
			ChangedOn = DateTime.Now;

			Ranges = product.Ranges;

			OriginalPriceInCents = product.RangedPriceInCents;
			if (product.IsDiscounted)
			{
				DiscountId = product.ProductDiscount.Id;
				if (product.ProductDiscount.DiscountType == DiscountType.Amount)
					DiscountAmountInCents = product.ProductDiscount.RangedDiscountValue(itemCount);
				else if (product.ProductDiscount.DiscountType == DiscountType.Percentage)
					DiscountPercentage = product.ProductDiscount.RangedDiscountValue(itemCount) / 100m;
				else if (product.ProductDiscount.DiscountType == DiscountType.NewPrice)
				{
					DiscountAmountInCents = OriginalPriceInCents - product.ProductDiscount.RangedDiscountValue(itemCount);
					Ranges = new List<Range>(); // NewPrice overrules any ranges
				}
			}

			DocTypeAlias = product.NodeTypeAlias;
		}

		#endregion

		#region Helper properties (with/withoutVat and Non-cents)

		private int? _productRangePriceWithVatInCentsLegacyDataReadBack;

		/// <summary>
		/// Gets or sets the vat amount.
		/// </summary>
		/// <value>
		/// The vat amount.
		/// </value>
		[DataMember]
		public decimal VatAmount
		{
			get { return VatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product price with vat.
		/// </summary>
		/// <value>
		/// The product price with vat.
		/// </value>
		[DataMember]
		public decimal ProductPriceWithVat
		{
			get { return ProductPriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product price without vat.
		/// </summary>
		/// <value>
		/// The product price without vat.
		/// </value>
		[DataMember]
		public decimal ProductPriceWithoutVat
		{
			get { return ProductPriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product range price without vat.
		/// </summary>
		/// <value>
		/// The product range price without vat.
		/// </value>
		[DataMember]
		public decimal ProductRangePriceWithoutVat
		{
			get { return ProductPriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product range price with vat.
		/// </summary>
		/// <value>
		/// The product range price with vat.
		/// </value>
		[DataMember]
		public decimal ProductRangePriceWithVat
		{
			get { return ProductPriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price with vat.
		/// </summary>
		/// <value>
		/// The discounted price with vat.
		/// </value>
		[DataMember]
		public decimal DiscountedPriceWithVat
		{
			get { return PriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price without vat.
		/// </summary>
		/// <value>
		/// The discounted price without vat.
		/// </value>
		public decimal DiscountedPriceWithoutVat
		{
			get { return PriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted vat.
		/// </summary>
		/// <value>
		/// The discounted vat.
		/// </value>
		public decimal DiscountedVat
		{
			get { return DiscountedVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price with vat.
		/// </summary>
		/// <value>
		/// The price with vat.
		/// </value>
		[DataMember]
		public decimal PriceWithVat
		{
			get { return PriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price without vat.
		/// </summary>
		/// <value>
		/// The price without vat.
		/// </value>
		[DataMember]
		public decimal PriceWithoutVat
		{
			get { return PriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product price with vat information cents.
		/// </summary>
		/// <value>
		/// The product price with vat information cents.
		/// </value>
		[DataMember]
		public int ProductPriceWithVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? ProductRangePriceInCents : VatCalculator.WithVat(ProductRangePriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the product price without variants with vat information cents.
		/// </summary>
		/// <value>
		/// The product price without variants with vat information cents.
		/// </value>
		[DataMember]
		public int ProductPriceWithoutVariantsWithVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? ProductRangePriceWithoutVariantsInCents : VatCalculator.WithVat(ProductRangePriceWithoutVariantsInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the product price without vat information cents.
		/// </summary>
		/// <value>
		/// The product price without vat information cents.
		/// </value>
		[DataMember]
		public int ProductPriceWithoutVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(ProductRangePriceInCents, Vat) : ProductRangePriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product price without variants without vat information cents.
		/// </summary>
		/// <value>
		/// The product price without variants without vat information cents.
		/// </value>
		[DataMember]
		public int ProductPriceWithoutVariantsWithoutVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(ProductRangePriceWithoutVariantsInCents, Vat) : ProductRangePriceWithoutVariantsInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product original price with vat information cents.
		/// </summary>
		/// <value>
		/// The product original price with vat information cents.
		/// </value>
		[DataMember]
		public int ProductOriginalPriceWithVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? OriginalPriceInCents : VatCalculator.WithVat(OriginalPriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the product original price without vat information cents.
		/// </summary>
		/// <value>
		/// The product original price without vat information cents.
		/// </value>
		[DataMember]
		public int ProductOriginalPriceWithoutVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(OriginalPriceInCents, Vat) : OriginalPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the product range price with vat information cents.
		/// </summary>
		/// <value>
		/// The product range price with vat information cents.
		/// </value>
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[Browsable(false)]
		[Obsolete("Use ProductPriceWithVatInCents")]
		[DataMember]
		public int ProductRangePriceWithVatInCents
		{
			get { return _productRangePriceWithVatInCentsLegacyDataReadBack ?? ProductPriceWithVatInCents; }
			set { _productRangePriceWithVatInCentsLegacyDataReadBack = value; }
		}

		/// <summary>
		/// Gets or sets the product range price without vat information cents.
		/// </summary>
		/// <value>
		/// The product range price without vat information cents.
		/// </value>
		[Browsable(false)]
		[Obsolete("Use ProductPriceWithoutVatInCents")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[DataMember]
		public int ProductRangePriceWithoutVatInCents
		{
			get { return ProductPriceWithoutVatInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price with vat information cents.
		/// </summary>
		/// <value>
		/// The price with vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? PriceInCents : VatCalculator.WithVat(PriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the price without vat information cents.
		/// </summary>
		/// <value>
		/// The price without vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithoutVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(PriceInCents, Vat) : PriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price with vat information cents.
		/// </summary>
		/// <value>
		/// The discounted price with vat information cents.
		/// </value>
		[Browsable(false)]
		[DataMember]
		[Obsolete("Use PriceWithVatInCents")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public int DiscountedPriceWithVatInCents
		{
			get { return PriceWithVatInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price without vat information cents.
		/// </summary>
		/// <value>
		/// The discounted price without vat information cents.
		/// </value>
		[Browsable(false)]
		[DataMember]
		[Obsolete("Use PriceWithoutVatInCents")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public int DiscountedPriceWithoutVatInCents
		{
			get { return Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(PriceInCents, Vat) : PriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted vat information cents.
		/// </summary>
		/// <value>
		/// The discounted vat information cents.
		/// </value>
		[DataMember]
		public int DiscountedVatInCents
		{
			get { return VatAmountInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the vat amount information cents.
		/// </summary>
		/// <value>
		/// The vat amount information cents.
		/// </value>
		[DataMember]
		public int VatAmountInCents
		{
			get { return PriceWithVatInCents - PriceWithoutVatInCents; }
			set { }
		}

		#endregion

		// hackish way for orderinfo/orderline to calculate discount
	}
}