﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;
using uWebshop.Domain.Helpers;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Namespace = "")]
	[Serializable]
	public class OrderLine
	{
		[XmlIgnore] internal IOrderInfo Order;

		/// <summary>
		/// The order line unique identifier
		/// </summary>
		[DataMember] public int OrderLineId;
		/// <summary>
		/// The product information
		/// </summary>
		[DataMember] public ProductInfo ProductInfo;

		[XmlIgnore][NonSerialized] internal XDocument _customData;
		private int? _vatAmountAfterOrderDiscountInCents;

		private OrderLine()
		{
			ProductInfo = new ProductInfo();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="OrderLine"/> class.
		/// </summary>
		/// <param name="productInfo">The product information.</param>
		/// <param name="order">The order.</param>
		public OrderLine(ProductInfo productInfo, IOrderInfo order)
		{
			ProductInfo = productInfo;
			Order = order;
		}

		/// <summary>
		/// Gets or sets the custom data.
		/// </summary>
		/// <value>
		/// The custom data.
		/// </value>
		[DataMember]
		public XElement CustomData
		{
			get { return _customData != null ? _customData.Root : null; }
			set
			{
				_customData = new XDocument();
				_customData.Add(value);
			}
		}

		/// <summary>
		/// Gets or sets the orderline weight.
		/// </summary>
		/// <value>
		/// The orderline weight.
		/// </value>
		[DataMember]
		[Browsable(false)]
		[Obsolete("use OrderLineWeight")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)] 
		public double OrderlineWeight
		{
			get { return (ProductInfo.Weight + ProductInfo.ProductVariants.Sum(variant => variant.Weight))*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		/// <summary>
		/// Gets or sets the weight.
		/// </summary>
		/// <value>
		/// The weight.
		/// </value>
		[DataMember]
		public double Weight
		{
			get { return (ProductInfo.Weight + ProductInfo.ProductVariants.Sum(variant => variant.Weight))*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line weight.
		/// </summary>
		/// <value>
		/// The order line weight.
		/// </value>
		[DataMember]
		public double OrderLineWeight
		{
			get { return (ProductInfo.Weight + ProductInfo.ProductVariants.Sum(variant => variant.Weight))*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		/// <summary>
		/// Gets or sets the amount information cents.
		/// </summary>
		/// <value>
		/// The amount information cents.
		/// </value>
		[DataMember]
		public int AmountInCents
		{
			get { return ProductInfo.PriceInCents*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line amount information cents.
		/// </summary>
		/// <value>
		/// The order line amount information cents.
		/// </value>
		[DataMember]
		public int OrderLineAmountInCents
		{
			get { return ProductInfo.PriceInCents*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		/// <summary>
		/// Gets or sets the discount information cents.
		/// </summary>
		/// <value>
		/// The discount information cents.
		/// </value>
		[DataMember]
		public int DiscountInCents
		{
			get { return ProductInfo.DiscountInCents*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set
			{
				if (ProductInfo.ItemCount.GetValueOrDefault(1) != 0)
					ProductInfo.DiscountInCents = value/ProductInfo.ItemCount.GetValueOrDefault(1);
			} // todo: add leftover cents
		}

		/// <summary>
		/// Gets or sets the order discount information cents.
		/// </summary>
		/// <value>
		/// The order discount information cents.
		/// </value>
		[DataMember]
		public int OrderDiscountInCents
		{
			get { return ProductInfo.DiscountInCents*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set
			{
				if (ProductInfo.ItemCount.GetValueOrDefault(1) != 0)
					ProductInfo.DiscountInCents = value/ProductInfo.ItemCount.GetValueOrDefault(1);
			} // todo: add leftover cents
		}

		/// <summary>
		///     Orderline vat amount
		/// </summary>
		[DataMember]
		public decimal Vat
		{
			get { return ProductInfo.Vat; }
			set { }
		}

		/// <summary>
		///     Orderline vat amount
		/// </summary>
		[DataMember]
		public decimal OrderLineVat
		{
			get { return ProductInfo.Vat; }
			set { }
		}

		/// <summary>
		/// Gets or sets the vat amount information cents.
		/// </summary>
		/// <value>
		/// The vat amount information cents.
		/// </value>
		[DataMember]
		public int VatAmountInCents
		{
			get { return ProductInfo.VatAmountInCents*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line vat amount information cents.
		/// </summary>
		/// <value>
		/// The order line vat amount information cents.
		/// </value>
		[DataMember]
		public int OrderLineVatAmountInCents
		{
			get { return ProductInfo.VatAmountInCents*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line vat amount after order discount information cents.
		/// </summary>
		/// <value>
		/// The order line vat amount after order discount information cents.
		/// </value>
		[DataMember]
		public int OrderLineVatAmountAfterOrderDiscountInCents
		{
			get { return (_vatAmountAfterOrderDiscountInCents ?? (_vatAmountAfterOrderDiscountInCents = ProductInfo.PriceWithVatInCents - ProductInfo.PriceWithoutVatInCents - (Order.PricesAreIncludingVAT ? VatCalculator.VatAmountFromWithVat(ProductInfo.DiscountInCents, ProductInfo.Vat) : VatCalculator.VatAmountFromWithoutVat(ProductInfo.DiscountInCents, ProductInfo.Vat))).GetValueOrDefault())*ProductInfo.ItemCount.GetValueOrDefault(1); }
			set { }
		}

		internal bool VariantsMatch(IEnumerable<int> variants)
		{
			return variants.SequenceEqual(ProductInfo.ProductVariants.Select(variant => variant.Id).OrderBy(v => v));
		}

		/// <summary>
		/// Determines whether [has variant with unique identifier] [the specified unique identifier].
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		public bool HasVariantWithId(int id)
		{
			return ProductInfo.ProductVariants.Any(usedVariant => id == usedVariant.Id);
		}

		#region non-cents variants

		/// <summary>
		///     Orderline amount without vat
		/// </summary>
		[DataMember]
		public decimal SubTotal
		{
			get { return OrderLineSubTotalInCents/100m; }
			set { }
		}

		/// <summary>
		///     Orderline amount without vat
		/// </summary>
		[DataMember]
		public decimal OrderLineSubTotal
		{
			get { return OrderLineSubTotalInCents/100m; }
			set { }
		}

		/// <summary>
		///     Orderline vat amount
		/// </summary>
		[DataMember]
		public decimal VatAmount
		{
			get { return OrderLineVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     Orderline vat amount
		/// </summary>
		[DataMember]
		public decimal OrderLineVatAmount
		{
			get { return OrderLineVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     Orderline amount with vat
		/// </summary>
		[DataMember]
		public decimal GrandTotal
		{
			get { return OrderLineGrandTotalInCents/100m; }
			set { }
		}

		/// <summary>
		///     Orderline amount with vat
		/// </summary>
		[DataMember]
		public decimal OrderLineGrandTotal
		{
			get { return OrderLineGrandTotalInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the sub total information cents.
		/// </summary>
		/// <value>
		/// The sub total information cents.
		/// </value>
		[DataMember]
		public int SubTotalInCents
		{
			get { return Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(OrderLineAmountInCents, ProductInfo.Vat) : OrderLineAmountInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line sub total information cents.
		/// </summary>
		/// <value>
		/// The order line sub total information cents.
		/// </value>
		[DataMember]
		public int OrderLineSubTotalInCents
		{
			get { return Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(OrderLineAmountInCents, ProductInfo.Vat) : OrderLineAmountInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the grand total information cents.
		/// </summary>
		/// <value>
		/// The grand total information cents.
		/// </value>
		[DataMember]
		public int GrandTotalInCents
		{
			get { return GetOrderLineGrandTotalInCents(Order.PricesAreIncludingVAT); }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line grand total information cents.
		/// </summary>
		/// <value>
		/// The order line grand total information cents.
		/// </value>
		[DataMember]
		public int OrderLineGrandTotalInCents
		{
			get { return GetOrderLineGrandTotalInCents(Order.PricesAreIncludingVAT); }
			set { }
		}

		internal int GetOrderLineGrandTotalInCents(bool pricesAreIncludingVAT)
		{
			return pricesAreIncludingVAT ? OrderLineAmountInCents : VatCalculator.WithVat(OrderLineAmountInCents, ProductInfo.Vat);
		}

		#endregion
	}
}