﻿using uWebshop.Domain;

internal interface IConvertibleToOrderInfo
{
	OrderInfo ToOrderInfo();
}