﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Xml.Linq;
using System.Xml.Serialization;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain.Businesslogic.VATChecking;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Services;
using uWebshop.Domain.XMLRendering;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Namespace = "")]
	[Serializable]
	public class OrderInfo : IOrderInfo, IOrder
	{
		#region events

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="AfterOrderLineCreatedEventArgs"/> instance containing the event data.</param>
		public delegate void AfterOrderLineCreatedEventHandler(OrderInfo orderInfo, AfterOrderLineCreatedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="AfterOrderLineDeletedEventArgs"/> instance containing the event data.</param>
		public delegate void AfterOrderLineDeletedEventHandler(OrderInfo orderInfo, AfterOrderLineDeletedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="AfterOrderLineUpdatedEventArgs"/> instance containing the event data.</param>
		public delegate void AfterOrderLineUpdatedEventHandler(OrderInfo orderInfo, AfterOrderLineUpdatedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="AfterOrderStatusChangedEventArgs"/> instance containing the event data.</param>
		public delegate void AfterOrderStatusChangedEventHandler(OrderInfo orderInfo, AfterOrderStatusChangedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="AfterOrderUpdatedEventArgs"/> instance containing the event data.</param>
		public delegate void AfterOrderUpdatedEventHandler(OrderInfo orderInfo, AfterOrderUpdatedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="BeforeOrderLineCreatedEventArgs"/> instance containing the event data.</param>
		public delegate void BeforeOrderLineCreatedEventHandler(OrderInfo orderInfo, BeforeOrderLineCreatedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="BeforeOrderLineDeletedEventArgs"/> instance containing the event data.</param>
		public delegate void BeforeOrderLineDeletedEventHandler(OrderInfo orderInfo, BeforeOrderLineDeletedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="BeforeOrderLineUpdatedEventArgs"/> instance containing the event data.</param>
		public delegate void BeforeOrderLineUpdatedEventHandler(OrderInfo orderInfo, BeforeOrderLineUpdatedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="BeforeOrderStatusChangedEventArgs"/> instance containing the event data.</param>
		public delegate void BeforeOrderStatusChangedEventHandler(OrderInfo orderInfo, BeforeOrderStatusChangedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="BeforeOrderUpdatedEventArgs"/> instance containing the event data.</param>
		public delegate void BeforeOrderUpdatedEventHandler(OrderInfo orderInfo, BeforeOrderUpdatedEventArgs e);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		public delegate void OrderLoadedEventHandler(OrderInfo orderInfo);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="e">The <see cref="OrderPaidChangedEventArgs"/> instance containing the event data.</param>
		public delegate void OrderPaidChangedEventHandler(OrderInfo orderInfo, OrderPaidChangedEventArgs e);

		/// <summary>
		/// Occurs when [before order updated].
		/// </summary>
		public static event BeforeOrderUpdatedEventHandler BeforeOrderUpdated;
		/// <summary>
		/// Occurs when [after order updated].
		/// </summary>
		public static event AfterOrderUpdatedEventHandler AfterOrderUpdated;
		/// <summary>
		/// Occurs when [before status changed].
		/// </summary>
		public static event BeforeOrderStatusChangedEventHandler BeforeStatusChanged;
		/// <summary>
		/// Occurs when [after status changed].
		/// </summary>
		public static event AfterOrderStatusChangedEventHandler AfterStatusChanged;
		/// <summary>
		/// Occurs when [before order line created].
		/// </summary>
		public static event BeforeOrderLineCreatedEventHandler BeforeOrderLineCreated;
		/// <summary>
		/// Occurs when [after order line created].
		/// </summary>
		public static event AfterOrderLineCreatedEventHandler AfterOrderLineCreated;
		/// <summary>
		/// Occurs when [before order line updated].
		/// </summary>
		public static event BeforeOrderLineUpdatedEventHandler BeforeOrderLineUpdated;
		/// <summary>
		/// Occurs when [after order line updated].
		/// </summary>
		public static event AfterOrderLineUpdatedEventHandler AfterOrderLineUpdated;
		/// <summary>
		/// Occurs when [before order line deleted].
		/// </summary>
		public static event BeforeOrderLineDeletedEventHandler BeforeOrderLineDeleted;
		/// <summary>
		/// Occurs when [after order line deleted].
		/// </summary>
		public static event AfterOrderLineDeletedEventHandler AfterOrderLineDeleted;
		/// <summary>
		/// Occurs when [order paid changed].
		/// </summary>
		public static event OrderPaidChangedEventHandler OrderPaidChanged;
		/// <summary>
		/// Occurs when [order loaded].
		/// </summary>
		public static event OrderLoadedEventHandler OrderLoaded;

		#endregion

		// dependencies

		private readonly List<CustomOrderValidation> _customValidations = new List<CustomOrderValidation>();

		/// <summary>
		/// The coupon codes data
		/// </summary>
		[Browsable(false)] [EditorBrowsable(EditorBrowsableState.Never)] [DebuggerBrowsable(DebuggerBrowsableState.Never)] [XmlArray("CouponCodes")] //[XmlArrayItem("CouponCode")]
		public List<string> CouponCodesData = new List<string>();

		/// <summary>
		/// The customer information
		/// </summary>
		[DataMember] public CustomerInfo CustomerInfo;
		internal bool EventsOn = false;
		internal bool LegacyDataReadBackMode = false;

		/// <summary>
		///     [Obsolete!! Use ConfirmDate] Gets the unique orderdate for the order
		/// </summary>
		//[Obsolete("Use ConfirmDate")] can't use [Obsolete] because of serialization
		[Browsable(false)] [EditorBrowsable(EditorBrowsableState.Never)] [DebuggerBrowsable(DebuggerBrowsableState.Never)] [DataMember] public string OrderDate;

		internal Func<List<IOrderDiscount>> OrderDiscountsFactory;

		/// <summary>
		///     Gets the unique ordernumber for the order
		/// </summary>
		[DataMember] public string OrderNumber;

		/// <summary>
		/// The payment information
		/// </summary>
		[DataMember] public PaymentInfo PaymentInfo;

		/// <summary>
		///     Gets the payment provider costs of the order
		/// </summary>
		[DataMember]
		public int PaymentProviderPriceInCents
		{
			get
			{
				// todo: percentage or amount
				if (PaymentProviderOrderPercentage > 0)
				{
					return OrderTotalWithoutPaymentInCents*PaymentProviderOrderPercentage/10000;
				}
				return PaymentProviderAmount;
			}
			set
			{
				
			}
		}

		internal int PaymentProviderAmount;
		internal int PaymentProviderOrderPercentage;

		/// <summary>
		/// The preference validate save action
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public ValidateSaveAction ReValidateSaveAction; // version 2.1 hack
		/// <summary>
		/// The revalidate order configuration load
		/// </summary>
		[Browsable(false)] [EditorBrowsable(EditorBrowsableState.Never)] [DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public bool? RevalidateOrderOnLoad; // version 2.1 hack
		/// <summary>
		/// The shipping information
		/// </summary>
		[DataMember] public ShippingInfo ShippingInfo;

		/// <summary>
		///     Gets the shipping costs of the order without Vat
		/// </summary>
		[DataMember] public int ShippingProviderAmountInCents;

		/// <summary>
		/// The store information
		/// </summary>
		[DataMember] public StoreInfo StoreInfo { get; set; }

		/// <summary>
		///     The unique per store id for the order
		/// </summary>
		[DataMember] public int? StoreOrderReferenceId;

		/// <summary>
		///     Unique Order Id
		/// </summary>
		[DataMember] public Guid UniqueOrderId;

		internal IVATCheckService VATCheckService;
		private DateTime? _confirmDate;
		private int? _grandtotalWithoutVatInCents;
		internal Lazy<List<IOrderDiscount>> _orderDiscounts;
		private bool? _pricesAreIncludingVAT;
		internal int? _regionalVatInCents;
		private bool? _vatCharged;

		#region Status

		private OrderStatus _status;

		/// <summary>
		///     Gets the status of the order
		/// </summary>
		[DataMember]
		public OrderStatus Status
		{
			get { return _status; }
			set { SetStatus(value); }
		}

		/// <summary>
		/// Sets the status.
		/// </summary>
		/// <param name="newStatus">The new status.</param>
		/// <param name="sendEmails">if set to <c>true</c> [send emails].</param>
		public void SetStatus(OrderStatus newStatus, bool sendEmails = true)
		{
			//Log.Instance.LogDebug("SetStatus Start SetStatus :" + newStatus);
			if (EventsOn && BeforeStatusChanged != null)
			{
				BeforeStatusChanged(this, new BeforeOrderStatusChangedEventArgs {OrderInfo = this, OrderStatus = _status});
			}
			//Log.Instance.LogDebug("SetStatus After BeforeStatusChanged :" + newStatus);

			//Log.Instance.LogDebug("SetStatus Before datetime :" + newStatus);
			if (_status == OrderStatus.Incomplete && newStatus != OrderStatus.Incomplete)
				ConfirmDate = DateTime.Now;
			//Log.Instance.LogDebug("SetStatus After datetime :" + newStatus);

			_status = newStatus;

			//Log.Instance.LogDebug("SetStatus Start AfterStatusChanged :" + newStatus);
			if (EventsOn && AfterStatusChanged != null)
			{
				AfterStatusChanged(this, new AfterOrderStatusChangedEventArgs {OrderInfo = this, OrderStatus = _status, SendEmails = sendEmails});
			}
			//Log.Instance.LogDebug("SetStatus End  :" + newStatus);
		}

		#endregion

		internal OrderInfo()
		{
			OrderLines = new List<OrderLine>();
			CouponCodesData = new List<string>();

			CustomerInfo = new CustomerInfo();
			ShippingInfo = new ShippingInfo();
			PaymentInfo = new PaymentInfo();
			StoreInfo = new StoreInfo();

			if (OrderLoaded != null)
			{
				OrderLoaded(this);
			}
			OrderValidationErrors = new List<OrderValidationError>();
		}

		/// <summary>
		///     Returns the ID of the order document node when it is created
		///     if not this is 0
		/// </summary>
		[DataMember]
		public int OrderNodeId { get; set; }

		/// <summary>
		///     Gets the unique orderdate for the order
		/// </summary>
		[DataMember]
		public DateTime? ConfirmDate
		{
			get { return _confirmDate; }
			set
			{
				OrderDate = value.GetValueOrDefault().ToString("f");
				_confirmDate = value;
			}
		}

		/// <summary>
		/// Gets or sets the paid.
		/// </summary>
		/// <value>
		/// The paid.
		/// </value>
		[DataMember(IsRequired = false)]
		public bool? Paid
		{
			get { return PaidDate.HasValue; }
			set
			{
				if (Paid != value && OrderPaidChanged != null)
				{
					try
					{
						OrderPaidChanged(this, new OrderPaidChangedEventArgs {OrderInfo = this, Paid = value.GetValueOrDefault()});
					}
					catch
					{
						Log.Instance.LogError("OrderPaidChanged Event Failed for Order: " + UniqueOrderId);
					}
				}
				PaidDate = value.GetValueOrDefault() ? (DateTime?) DateTime.Now : null;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether [terms accepted].
		/// </summary>
		/// <value>
		///   <c>true</c> if [terms accepted]; otherwise, <c>false</c>.
		/// </value>
		[DataMember(IsRequired = false)]
		public bool TermsAccepted { get; set; }

		/// <summary>
		///     Is the stock updated for this order?
		/// </summary>
		[DataMember(IsRequired = false)]
		public bool StockUpdated { get; set; }

		/// <summary>
		/// Gets or sets the paid date.
		/// </summary>
		/// <value>
		/// The paid date.
		/// </value>
		[DataMember(IsRequired = false)]
		public DateTime? PaidDate { get; set; }

		/// <summary>
		///     Gets a list with the coupons of the order
		/// </summary>
		[XmlIgnore]
		public ReadOnlyCollection<string> CouponCodes
		{
			get { return CouponCodesData.AsReadOnly(); }
		}

		// dit geeft errors in de tests, maar *waarschijnlijk* niet in umbraco
		/// <summary>
		/// Gets or sets the applied discounts information.
		/// </summary>
		/// <value>
		/// The applied discounts information.
		/// </value>
		[DataMember]
		public List<OrderDiscount> AppliedDiscountsInformation
		{
			// for the email XML for example
			get { return OrderDiscounts.Select(discount => new OrderDiscount(discount, this)).ToList(); }
			set { }
		}

		/// <summary>
		/// Gets or sets the charged shipping costs.
		/// </summary>
		/// <value>
		/// The charged shipping costs.
		/// </value>
		[DataMember]
		public ChargedShippingCosts ChargedShippingCosts
		{
			get { return new ChargedShippingCosts(ChargedShippingCostsInCents, PricesAreIncludingVAT, AverageOrderVatPercentage); }
			set { }
		}

		private int ChargedShippingCostsInCents
		{
			get
			{
				return OrderDiscounts.Any(discount => discount.DiscountType == DiscountType.FreeShipping) 
					? 0 : ActiveShippingProviderAmountInCents;
			}
		}

		/// <summary>
		/// Gets or sets the payment costs.
		/// </summary>
		/// <value>
		/// The payment costs.
		/// </value>
		[DataMember]
		public PaymentCosts PaymentCosts
		{
			get { return new PaymentCosts(PaymentProviderPriceInCents, PricesAreIncludingVAT, AverageOrderVatPercentage); }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line total.
		/// </summary>
		/// <value>
		/// The order line total.
		/// </value>
		[DataMember]
		public OrderLineTotal OrderLineTotal
		{
			get { return new OrderLineTotal(OrderLineTotalInCents, PricesAreIncludingVAT, AverageOrderVatPercentage); }
			set { }
		}

		/// <summary>
		///     Are shippingcosts still up to date?
		///     TRUE = shipping costs needs to be updated
		///     FALSE = shipping costs are correct!
		/// </summary>
		[DataMember]
		public bool ShippingCostsMightBeOutdated { get; set; }

		internal IEnumerable<CustomOrderValidation> CustomOrderValiations
		{
			get { return _customValidations; }
		}

		/// <summary>
		/// Gets the order discounts.
		/// </summary>
		/// <value>
		/// The order discounts.
		/// </value>
		[XmlIgnore]
		public List<IOrderDiscount> OrderDiscounts
		{
			get
			{
				if (OrderDiscountsFactory == null) return new List<IOrderDiscount>(); // is this correct? (needed for deserialization process of legacy orderinfo xml)
				return OrderDiscountsFactory();
				//return _orderDiscounts.Value;
				//return DiscountService.GetApplicableDiscountsForOrder(this).ToList();
			}
		}

		/// <summary>
		///     Is this order discounted?
		/// </summary>
		[DataMember]
		public string IsDiscounted
		{
			get { return (OrderDiscounts.Any()).ToString(); } // todo: check and ehh, string?
			set { }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [vat charged].
		/// </summary>
		/// <value>
		///   <c>true</c> if [vat charged]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool VATCharged
		{
			get
			{
				if (_vatCharged.HasValue)
				{
					return _vatCharged.GetValueOrDefault();
				}

				IEnumerable<string> vatCountryCodes = IO.Container.Resolve<IVATCountryRepository>().GetAllCountries(StoreInfo.Alias).Select(c => c.Code);

				if (!vatCountryCodes.Contains(CustomerInfo.CountryCode))
				{
					//Log.Instance.LogDebug("VATCharged FALSE: !vatCountryCodes.Contains(CustomerInfo.CountryCode)");
					return (bool) (_vatCharged = false);
				}

				if (string.IsNullOrEmpty(CustomerInfo.VATNumber))
				{
					return (bool) (_vatCharged = true);
				}

				if (CustomerInfo.CountryCode != null && CustomerInfo.CountryCode.ToLowerInvariant() == StoreInfo.CountryCode.ToLowerInvariant())
				{
					Log.Instance.LogDebug("VATCharged TRUE: CustomerInfo.CountryCode != null && StoreInfo.CountryCode != null && CustomerInfo.CountryCode.ToLowerInvariant() == StoreInfo.CountryCode.ToLowerInvariant()");
					return (bool) (_vatCharged = true);
				}

				var cmsApplication = IO.Container.Resolve<ICMSApplication>();
				if (cmsApplication.MemberLoggedIn())
				{
					if (cmsApplication.CurrentMemberInfo().VATNumberCheckedAsValid)
					{
						bool value = StoreInfo.CountryCode == CustomerInfo.CountryCode;
						Log.Instance.LogDebug("VATCharged " + value + ": Member VATNumberCheckedAsValid");
						return (bool) (_vatCharged = value);
					}
				}
				return (bool) (_vatCharged = !VATCheckService.VATNumberValid(CustomerInfo.VATNumber, this));
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the charged amount information cents.
		/// </summary>
		/// <value>
		/// The charged amount information cents.
		/// </value>
		[DataMember]
		public int ChargedAmountInCents
		{
			get { return VATCharged ? GrandtotalInCents : SubtotalInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the order validation errors.
		/// </summary>
		/// <value>
		/// The order validation errors.
		/// </value>
		[DataMember]
		public List<OrderValidationError> OrderValidationErrors { get; set; }

		/// <summary>
		/// Gets or sets the regional vat information cents.
		/// </summary>
		/// <value>
		/// The regional vat information cents.
		/// </value>
		[DataMember]
		public int RegionalVatInCents
		{
			get
			{
				// todo: clean!
				if (_regionalVatInCents != null)
				{
					return _regionalVatInCents.GetValueOrDefault();
				}
				int orderTotalWithTax = PricesAreIncludingVAT ? OrderTotalInCents : VatCalculator.WithVat(OrderTotalInCents, AverageOrderVatPercentage);

				if (StoreInfo.Alias != null)
				{
					List<Region> regions = StoreHelper.GetAllRegionsForStore(StoreInfo.Alias);
					if (regions != null)
					{
						Region region = regions.FirstOrDefault(x => x.Code == CustomerInfo.RegionCode);
						if (region != null)
						{
							decimal tax;
							if (decimal.TryParse(region.Tax, out tax))
							{
								_regionalVatInCents = VatCalculator.VatAmountFromWithVat(orderTotalWithTax, tax);

								return _regionalVatInCents.GetValueOrDefault();
							}
						}
					}
				}
				return 0;
			}
			set { _regionalVatInCents = value; } // test?
		}

		/// <summary>
		/// Gets or sets the total vat information cents.
		/// </summary>
		/// <value>
		/// The total vat information cents.
		/// </value>
		[DataMember]
		public decimal TotalVatInCents
		{
			get { return GrandtotalInCents - SubtotalInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the order total in cents.
		/// </summary>
		/// <value>
		/// The order total in cents.
		/// </value>
		[DataMember]
		public int OrderTotalInCents
		{
			get { return OrderTotalWithoutPaymentInCents + PaymentProviderPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets the order total without payment in cents.
		/// </summary>
		/// <value>
		/// The order total without payment in cents.
		/// </value>
		[IgnoreDataMember]
		public int OrderTotalWithoutPaymentInCents
		{
			get
			{
				int totalInCents = OrderLines.Sum(orderline => orderline.OrderLineAmountInCents);

				totalInCents -= DiscountAmountInCents; // substract order discount
				totalInCents += ShippingProviderAmountInCents;

				return totalInCents;
			}
		}

		/// <summary>
		/// Gets or sets the grandtotal without vat information cents.
		/// </summary>
		/// <value>
		/// The grandtotal without vat information cents.
		/// </value>
		[DataMember]
		public int GrandtotalWithoutVatInCents
		{
			get
			{
				if (_grandtotalWithoutVatInCents != null)
				{
					return _grandtotalWithoutVatInCents.GetValueOrDefault();
				}
				int orderTotalWithoutTax = OrderLines.Sum(orderline => orderline.OrderLineSubTotalInCents);
				orderTotalWithoutTax -= DiscountAmountInCents;
				// todo: trigger apply discounts op een andere plek!

				orderTotalWithoutTax += ShippingProviderCostsWithoutVatInCents;
				orderTotalWithoutTax += PaymentProviderCostsWithoutVatInCents;

				_grandtotalWithoutVatInCents = orderTotalWithoutTax;

				return _grandtotalWithoutVatInCents.GetValueOrDefault();
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the grandtotal information cents.
		/// </summary>
		/// <value>
		/// The grandtotal information cents.
		/// </value>
		[DataMember]
		public int GrandtotalInCents
		{
			get { return RegionalVatInCents + (PricesAreIncludingVAT ? OrderTotalInCents : VatCalculator.WithVat(OrderTotalInCents, AverageOrderVatPercentage)); }
			set { }
		}

		/// <summary>
		/// Gets or sets the vat total in cents.
		/// </summary>
		/// <value>
		/// The vat total in cents.
		/// </value>
		[Obsolete("Use VatTotalInCents")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)] 
		public int VatTotal
		{
			get { return VatTotalInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the vat total in cents.
		/// </summary>
		/// <value>
		/// The vat total in cents.
		/// </value>
		public int VatTotalInCents
		{
			get { return OrderLines.Sum(line => line.OrderLineVatAmountAfterOrderDiscountInCents); }
			set { }
		}

		/// <summary>
		/// Gets or sets the order line total information cents.
		/// </summary>
		/// <value>
		/// The order line total information cents.
		/// </value>
		public int OrderLineTotalInCents
		{
			get { return OrderLines.Sum(line => line.OrderLineAmountInCents) /* - line.OrderDiscountInCents)*/; }
			set { }
		}

		internal string RedirectUrl { get; set; }
		/// <summary>
		/// Gets or sets the database unique identifier.
		/// </summary>
		/// <value>
		/// The database unique identifier.
		/// </value>
		public int DatabaseId { get; set; }

		/// <summary>
		///     Gets a list with the orderlines of the order
		/// </summary>
		[DataMember]
		public List<OrderLine> OrderLines { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether the prices are including vat.
		/// </summary>
		/// <value>
		/// <c>true</c> if the prices are including vat; otherwise, <c>false</c>.
		/// </value>
		public bool PricesAreIncludingVAT
		{
			get { return _pricesAreIncludingVAT ?? IO.Container.Resolve<ISettingsService>().IncludingVat; }
			set { _pricesAreIncludingVAT = value; }
		}

		/// <summary>
		/// Registers the custom order validation.
		/// </summary>
		/// <param name="condition">The condition.</param>
		/// <param name="errorDictionaryItem">The error dictionary item.</param>
		public void RegisterCustomOrderValidation(Predicate<OrderInfo> condition, Func<OrderInfo, string> errorDictionaryItem)
		{
			_customValidations.Add(new CustomOrderValidation {condition = condition, errorDictionaryItem = errorDictionaryItem});
		}

		internal static OrderInfo CreateOrderInfoFromLegacyXmlString(string orderInfoXml, int databaseId = 0, string orderReferenceNumber = "")
		{
			//try
			{
				var orderInfo = DomainHelper.DeserializeXmlStringToObject<OrderInfo>(orderInfoXml);
				orderInfo.LegacyDataReadBackMode = true;
				orderInfo.SetOrderReferenceOnOrderLinesAndProductInfos();
				if (orderInfo.OrderLines.Any())
				{
					// determine whether PricesAreIncludingVAT by comparing ProductRangePriceInCents with ProductRangePriceWithVatInCents
					ProductInfo productInfo = orderInfo.OrderLines.First().ProductInfo;
					orderInfo.PricesAreIncludingVAT = productInfo.ProductRangePriceInCents == productInfo.ProductRangePriceWithVatInCents; // sic!!
				}

				// create custom discount for backwards compatibility
				var discounts = new List<IOrderDiscount>();
				if (orderInfo.DiscountAmountInCents != 0 || orderInfo._discountAmount != null)
				{
					int discount = orderInfo.DiscountAmountInCents > 0 ? orderInfo.DiscountAmountInCents : (int) (orderInfo._discountAmount.GetValueOrDefault()*100);
					discounts.Add(new OrderDTO.OrderDiscount {DiscountType = DiscountType.Amount, DiscountValue = discount, RequiredItemIds = new List<int>(), AffectedOrderlines = new List<int>(), MemberGroups = new List<string>()});
				}

				IO.Container.Resolve<IOrderService>().UseStoredDiscounts(orderInfo, discounts);

				orderInfo.VATCheckService = new FixedValueIvatChecker(false);

				// generate product discount information for backwards compatibility
				foreach (OrderLine orderLine in orderInfo.OrderLines.Where(line => line.ProductInfo.PriceInCents != line.ProductInfo.ProductRangePriceInCents))
				{
					orderLine.ProductInfo.DiscountAmountInCents = orderLine.ProductInfo.ProductRangePriceInCents - orderLine.ProductInfo.PriceInCents;
				}

				if (orderInfo.Status != OrderStatus.Incomplete)
				{
					DateTime val;
					if (!DateTime.TryParse(orderInfo.OrderDate, orderInfo.StoreInfo.CultureInfo.DateTimeFormat, DateTimeStyles.None, out val))
						if (!DateTime.TryParse(orderInfo.OrderDate, new CultureInfo("EN-us").DateTimeFormat, DateTimeStyles.None, out val))
							if (!DateTime.TryParse(orderInfo.OrderDate, new CultureInfo("NL-nl").DateTimeFormat, DateTimeStyles.None, out val))
								DateTime.TryParse(orderInfo.OrderDate, out val);
					orderInfo.ConfirmDate = val;
				}
				orderInfo.LegacyDataReadBackMode = false;

				return orderInfo;
			}
			//catch (Exception ex)
			//{
			//	// todo: for debugging (developing!) you want a greater visibility of exceptions
			//	throw new Exception("Failed to load order data, id: " + databaseId + ", ordernumber: " + orderReferenceNumber, ex);
			//}
		}

		private static OrderInfo CreateOrderInfoFromOrderDataObject<T>(string orderInfoXml) where T : IConvertibleToOrderInfo
		{
			var order = DomainHelper.DeserializeXmlStringToObject<T>(orderInfoXml);
			OrderInfo orderInfo = order.ToOrderInfo();
			return orderInfo;
		}

		internal void SetOrderReferenceOnOrderLinesAndProductInfos()
		{
			OrderLines.ForEach(line =>
			{
				line.Order = this;
				line.ProductInfo.Order = this;
				line.ProductInfo.ProductVariants.ForEach(variant => variant.Product = line.ProductInfo);
			});
		}

		// todo: move to orderservice
		internal static OrderInfo CreateOrderInfoFromOrderData(OrderData orderData)
		{
			var cmsApplication = IO.Container.Resolve<ICMSApplication>();

			if (orderData == null || string.IsNullOrEmpty(orderData.OrderXML))
				throw new Exception("Trying to load order without data (xml), id: " + (orderData == null ? "no data!" : orderData.DatabaseId.ToString()) + ", ordernumber: " + (orderData == null ? "no data!" : orderData.OrderReferenceNumber));

			OrderInfo orderInfo;
			try
			{
				orderInfo = orderData.OrderXML.Contains("<OrderInfo ") ? CreateOrderInfoFromLegacyXmlString(orderData.OrderXML) : CreateOrderInfoFromOrderDataObject<OrderDTO.Order>(orderData.OrderXML);
			}
			catch (Exception ex)
			{
				string message = "Failed to load order data , id: " + orderData.DatabaseId + ", ordernumber: " + orderData.OrderReferenceNumber;
				Log.Instance.LogError(ex, message);
				throw new Exception(message);
			}

			if (orderInfo == null) throw new Exception("Problem with parsing order from database xml");
			if (orderInfo.CustomerInfo == null) throw new Exception("Problem with parsing order from database xml, no CustomerInfo");
			if (orderInfo.PaymentInfo == null) throw new Exception("Problem with parsing order from database xml, no PaymentInfo");
			if (orderInfo.StoreInfo == null) throw new Exception("Problem with parsing order from database xml, no StoreInfo");

			orderInfo.DatabaseId = orderData.DatabaseId;
			orderInfo.UniqueOrderId = orderData.UniqueId;
			orderInfo.StoreInfo.Alias = orderData.StoreAlias; // todo: check if correct
			orderInfo.StoreOrderReferenceId = orderData.StoreOrderReferenceId;
			orderInfo.OrderNumber = orderData.OrderReferenceNumber;

			OrderStatus orderStatus;
			Enum.TryParse(orderData.OrderStatus, out orderStatus);

			// if Incomplete and in frontend, refer to current discounts instead of stored
			if (orderStatus == OrderStatus.Incomplete && !cmsApplication.RequestIsInCMSBackend(HttpContext.Current))
			{
				//orderInfo._pricesAreIncludingVAT = null; todo: yes or no? (if yes, you could argue you need to look at current prices aswell..)
				orderInfo._discountAmountInCents = null;

				IO.Container.Resolve<IOrderService>().UseDatabaseDiscounts(orderInfo);
				//orderInfo.DiscountService = new DiscountService(new UwebshopDefaultRepository()); // todo: DIP and test
				//orderInfo.DiscountRepository = new UwebshopDefaultRepository();

				if (orderInfo.RevalidateOrderOnLoad.GetValueOrDefault())
				{
					if (orderInfo.ReValidateSaveAction == ValidateSaveAction.Order)
					{
						OrderHelper.ValidateOrder(orderInfo); // tricky (recursiveness because of GetStore() -> GetOrderInfo())
					}
					if (orderInfo.ReValidateSaveAction == ValidateSaveAction.Customer)
					{
						OrderHelper.ValidateCustomer(orderInfo, true);
					}
					if (orderInfo.ReValidateSaveAction == ValidateSaveAction.Stock)
					{
						OrderHelper.ValidateStock(orderInfo, true);
					}
					if (orderInfo.ReValidateSaveAction == ValidateSaveAction.Orderlines)
					{
						OrderHelper.ValidateOrderLines(orderInfo, true);
					}
					if (orderInfo.ReValidateSaveAction == ValidateSaveAction.CustomValidation)
					{
						OrderHelper.ValidateCustomValidation(orderInfo, true);
					}

					// todo: prevent unusefull save in ValidateOrder
				}
			}

			orderInfo.EventsOn = false; // somehow a loop with the events keeps popping up, therefore an extra safeguard
			orderInfo.Status = orderStatus;

			orderInfo.CustomerInfo.CustomerId = orderData.CustomerId.GetValueOrDefault();

			if (orderData.CustomerUsername != null) orderInfo.CustomerInfo.LoginName = orderData.CustomerUsername;
			if (orderData.CustomerEmail != null) orderInfo.CustomerEmail = orderData.CustomerEmail;
			if (orderData.CustomerFirstName != null) orderInfo.CustomerFirstName = orderData.CustomerFirstName;
			if (orderData.CustomerLastName != null) orderInfo.CustomerLastName = orderData.CustomerLastName;
			if (orderData.TransactionId != null) orderInfo.PaymentInfo.TransactionId = orderData.TransactionId;

			orderInfo.EventsOn = true;

			return orderInfo;
		}

		internal OrderData ToOrderData()
		{
			var orderData = new OrderData();
			orderData.Order = this;
			orderData.DatabaseId = DatabaseId;
			orderData.UniqueId = UniqueOrderId;
			orderData.StoreAlias = StoreInfo.Alias;
			orderData.StoreOrderReferenceId = StoreOrderReferenceId;
			orderData.OrderReferenceNumber = OrderNumber;
			orderData.OrderStatus = Status.ToString();
			orderData.CustomerId = CustomerInfo.CustomerId;
			orderData.CustomerUsername = CustomerInfo.LoginName;
			orderData.CustomerEmail = CustomerEmail;
			orderData.CustomerFirstName = CustomerFirstName;
			orderData.CustomerLastName = CustomerLastName;
			orderData.TransactionId = PaymentInfo.TransactionId;

			orderData.OrderXML = DomainHelper.SerializeObjectToXmlString(new OrderDTO.Order(this));

			return orderData;
		}

		internal void ClearCachedValues()
		{
			_grandtotalWithoutVatInCents = null;
			_regionalVatInCents = null;
		}

		/// <summary>
		/// Sets the vat number.
		/// </summary>
		/// <param name="VATNumber">The vat number.</param>
		public void SetVATNumber(string VATNumber)
		{
			if (CustomerInfo.VATNumber != VATNumber)
			{
				_vatCharged = null;
				VATCheckService = IO.Container.Resolve<IVATCheckService>();
			}
			CustomerInfo.VATNumber = VATNumber;
		}

		/// <summary>
		///     Add extra fields to any orderline
		/// </summary>
		/// <param name="fields"></param>
		/// <param name="orderLineId"></param>
		public bool AddOrderLineDetails(Dictionary<string, string> fields, int orderLineId)
		{
			OrderLine orderline = OrderLines.FirstOrDefault(x => x.OrderLineId == orderLineId);

			if (orderline == null)
			{
				return false;
			}

			if (IO.Container.Resolve<IOrderUpdatingService>().ChangeOrderToIncompleteAndReturnFalseIfNotPossible(this))
				return false;

			string documentTypeAlias = orderline.ProductInfo.Product.NodeTypeAlias.Replace("uwbsProduct", "uwbsOrderedProduct");

			var xDoc = new XDocument(new XElement("Fields"));
			OrderUpdatingService.AddFieldsToXDocumentBasedOnCMSDocumentType(xDoc, fields, documentTypeAlias);
			orderline._customData = xDoc;

			return true;
		}

		/// <summary>
		/// Adds the store.
		/// </summary>
		/// <param name="store">The store.</param>
		public void AddStore(Store store)
		{
			StoreInfo.Culture = store.Culture;
			StoreInfo.Alias = store.Alias;
			StoreInfo.CountryCode = store.CountryCode;
			StoreInfo.CurrencyCulture = store.CurrencyCulture;
			StoreInfo.LanguageCode = store.Culture;
		}

		/// <summary>
		///     Remove a coupon from the order
		/// </summary>
		/// <param name="couponCode"></param>
		public CouponCodeResult RemoveCoupon(string couponCode)
		{
			if (!CouponCodesData.Contains(couponCode))
			{
				return CouponCodeResult.NotFound;
			}

			CouponCodesData.Remove(couponCode);
			_discountAmountInCents = null;

			return CouponCodeResult.Succes;
		}

		internal void SetCouponCode(string couponCode)
		{
			CouponCodesData.Add(couponCode);
			_discountAmountInCents = null;
		}

		#region Façade to IOrderUpdatingService

		/// <summary>
		///     Change orderstatus
		/// </summary>
		public bool ConfirmOrder(bool termsAccepted, int confirmationNodeId)
		{
			return IO.Container.Resolve<IOrderUpdatingService>().ConfirmOrder(this, termsAccepted, confirmationNodeId);
		}

		/// <summary>
		///     Save the order
		/// </summary>
		/// <param name="revalidateOrderOnLoadHack">check again validation</param>
		/// <param name="validateSaveAction"></param>
		public void Save(bool revalidateOrderOnLoadHack = false, ValidateSaveAction validateSaveAction = ValidateSaveAction.Order)
		{
			IO.Container.Resolve<IOrderUpdatingService>().Save(this, revalidateOrderOnLoadHack, validateSaveAction);
		}

		/// <summary>
		///     Create, Add, Update the orderline
		/// </summary>
		/// <param name="orderLineId">The Id of the orderline</param>
		/// <param name="productId">The productId</param>
		/// <param name="action">The action (add, update, delete, deleteall)</param>
		/// <param name="itemCount">The amount of items to be added</param>
		/// <param name="variantsList">The variants ID's added to the pricing</param>
		/// <param name="fields">custom fields added to this orderline</param>
		public void AddOrUpdateOrderLine(int orderLineId, int productId, string action, int itemCount, IEnumerable<int> variantsList, Dictionary<string, string> fields = null)
		{
			IO.Container.Resolve<IOrderUpdatingService>().AddOrUpdateOrderLine(this, orderLineId, productId, action, itemCount, variantsList, fields);
		}

		/// <summary>
		///     Add a payment provider to the order
		/// </summary>
		/// <param name="paymentProviderId">The Id of the Payment Provider Node</param>
		/// <param name="paymentProviderMethodId">The Id of the Paymetn Method</param>
		public ProviderActionResult AddPaymentProvider(int paymentProviderId, string paymentProviderMethodId)
		{
			return IO.Container.Resolve<IOrderUpdatingService>().AddPaymentProvider(this, paymentProviderId, paymentProviderMethodId);
		}

		/// <summary>
		///     Add a shipping provider to the order
		/// </summary>
		/// <param name="shippingProviderId">The Id of the Shipping Provider Node</param>
		/// <param name="shippingProviderMethodId">The Id of the Shipping Provider Method</param>
		public ProviderActionResult AddShippingProvider(int shippingProviderId, string shippingProviderMethodId)
		{
			return IO.Container.Resolve<IOrderUpdatingService>().AddShippingProvider(this, shippingProviderId, shippingProviderMethodId);
		}

		/// <summary>
		///     Add customer/shipping/extra fields to the order
		/// </summary>
		/// <param name="fields"></param>
		/// <param name="customerDataType"></param>
		public bool AddCustomerFields(Dictionary<string, string> fields, CustomerDatatypes customerDataType)
		{
			return IO.Container.Resolve<IOrderUpdatingService>().AddCustomerFields(this, fields, customerDataType);
		}

		/// <summary>
		///     Add coupon to the order
		/// </summary>
		/// <param name="couponCode"></param>
		public CouponCodeResult AddCoupon(string couponCode)
		{
			return IO.Container.Resolve<IOrderUpdatingService>().AddCoupon(this, couponCode);
		}

		#endregion

		#region Façade to CustomerInfo XML

		/// <summary>
		/// Gets or sets the customer email.
		/// </summary>
		/// <value>
		/// The customer email.
		/// </value>
		public string CustomerEmail
		{
			get
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null)
					return string.Empty;
				XElement element = CustomerInfo.CustomerInformation.Element("customerEmail");
				return element != null ? element.Value : string.Empty;
			}
			set
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null) return;
				XElement element = CustomerInfo.CustomerInformation.Element("customerEmail");
				if (element != null) element.SetValue(value);
			}
		}

		/// <summary>
		/// Gets or sets the last name of the customer.
		/// </summary>
		/// <value>
		/// The last name of the customer.
		/// </value>
		public string CustomerLastName
		{
			get
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null)
					return string.Empty;
				XElement element = CustomerInfo.CustomerInformation.Element("customerLastName");
				return element != null ? element.Value : string.Empty;
			}
			set
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null) return;
				XElement element = CustomerInfo.CustomerInformation.Element("customerLastName");
				if (element != null) element.SetValue(value);
			}
		}

		/// <summary>
		/// Gets or sets the first name of the customer.
		/// </summary>
		/// <value>
		/// The first name of the customer.
		/// </value>
		public string CustomerFirstName
		{
			get
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null)
					return string.Empty;
				XElement element = CustomerInfo.CustomerInformation.Element("customerFirstName");
				return element != null ? element.Value : string.Empty;
			}
			set
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null) return;
				XElement element = CustomerInfo.CustomerInformation.Element("customerFirstName");
				if (element != null) element.SetValue(value);
			}
		}

		/// <summary>
		/// Gets or sets the customer country.
		/// </summary>
		/// <value>
		/// The customer country.
		/// </value>
		public string CustomerCountry
		{
			get
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null)
					return string.Empty;
				XElement element = CustomerInfo.CustomerInformation.Element("customerCountry");
				return element != null ? element.Value : string.Empty;
			}
			set
			{
				if (CustomerInfo == null || CustomerInfo.CustomerInformation == null) return;

				XElement element = CustomerInfo.CustomerInformation.Element("customerCountry");
				if (element != null) element.SetValue(value);
			}
		}

		#endregion

		#region Conversions to NonCents and With/Without VAT

		private decimal? _discountAmount;

		/// <summary>
		///     Gets the total amount of the order, including shippingcosts, including paymentcosts: including Vat
		/// </summary>
		[DataMember]
		public decimal Grandtotal
		{
			get { return GrandtotalInCents/100m; }
			set { }
		}

		/// <summary>
		///     Gets the total of all the orderlines
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[Obsolete("Use OrderLineTotalWithVat")]
		public decimal OrderLineWithVatTotal
		{
			get { return OrderLineTotalWithVat; }
			set { }
		}

		/// <summary>
		///     Gets the total of all the orderlines
		/// </summary>
		[DataMember]
		public decimal OrderLineTotalWithVat
		{
			get { return OrderLineTotalWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     Gets the total of all the orderlines without VAT
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[Obsolete("Use OrderLineTotalWithoutVat")]
		public decimal OrderLineWithoutVatTotal
		{
			get { return OrderLineTotalWithoutVat; }
			set { }
		}

		/// <summary>
		///     Gets the total of all the orderlines without VAT
		/// </summary>
		[DataMember]
		public decimal OrderLineTotalWithoutVat
		{
			get { return OrderLineTotalWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     Gets the total amount of the order, including shippingcosts, including paymentcosts: excluding Vat
		/// </summary>
		[DataMember]
		public decimal GrandtotalWithoutVat
		{
			get { return GrandtotalWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     Gets the total tax of the order, excluding shippingcosts
		/// </summary>
		[DataMember]
		public decimal TotalVat
		{
			get { return TotalVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     The shipping vat amount of this order
		/// </summary>
		[DataMember]
		public decimal ShippingProviderVatAmount
		{
			get { return ShippingProviderVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     The shipping provider costs of this order EXCLUDING Vat
		/// </summary>
		[DataMember]
		public decimal ShippingProviderCostsWithoutVat
		{
			get { return ShippingProviderCostsWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     The shipping provider costs of this order INCLUDING Vat
		/// </summary>
		[DataMember]
		public decimal ShippingProviderCostsWithVat
		{
			get { return ShippingProviderCostsWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     the payment provider vat amount
		/// </summary>
		[DataMember]
		public decimal PaymentProviderVatAmount
		{
			get { return PaymentProviderVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     the payment provider costs EXCLUDING vat
		/// </summary>
		[DataMember]
		public decimal PaymentProviderCostsWithoutVat
		{
			get { return PaymentProviderCostsWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     the payment provider costs INCLUDING vat
		/// </summary>
		[DataMember]
		public decimal PaymentProviderCostsWithVat
		{
			get { return PaymentProviderCostsWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discount amount.
		/// </summary>
		/// <value>
		/// The discount amount.
		/// </value>
		[DataMember]
		public decimal DiscountAmount
		{
			get { return _discountAmount ?? DiscountAmountInCents/100m; }
			set { _discountAmount = value; }
		}

		/// <summary>
		/// Gets or sets the discount amount with vat.
		/// </summary>
		/// <value>
		/// The discount amount with vat.
		/// </value>
		[DataMember]
		public decimal DiscountAmountWithVat
		{
			get { return DiscountAmountWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discount amount without vat.
		/// </summary>
		/// <value>
		/// The discount amount without vat.
		/// </value>
		[DataMember]
		public decimal DiscountAmountWithoutVat
		{
			get { return DiscountAmountWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     Gets the regional vat for this order
		/// </summary>
		[DataMember]
		public decimal RegionalVat
		{
			get { return RegionalVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     Gets the subtotal of the order
		/// </summary>
		[DataMember]
		public decimal Subtotal
		{
			get { return SubtotalInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the charged amount.
		/// </summary>
		/// <value>
		/// The charged amount.
		/// </value>
		[DataMember]
		public decimal ChargedAmount
		{
			get { return ChargedAmountInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discount amount with vat information cents.
		/// </summary>
		/// <value>
		/// The discount amount with vat information cents.
		/// </value>
		[DataMember]
		public int DiscountAmountWithVatInCents
		{
			get { return PricesAreIncludingVAT ? DiscountAmountInCents : VatCalculator.WithVat(DiscountAmountInCents, AverageOrderVatPercentage); }
			set { }
		}

		/// <summary>
		/// Gets or sets the discount amount without vat information cents.
		/// </summary>
		/// <value>
		/// The discount amount without vat information cents.
		/// </value>
		[DataMember]
		public int DiscountAmountWithoutVatInCents
		{
			get { return PricesAreIncludingVAT ? VatCalculator.WithoutVat(DiscountAmountInCents, AverageOrderVatPercentage) : DiscountAmountInCents; }
			set { }
		}

		/// <summary>
		///     Gets the total of all the orderlines in cents with VAT
		/// </summary>
		[DataMember]
		public int OrderLineTotalWithVatInCents
		{
			get { return PricesAreIncludingVAT ? OrderLineTotalInCents : VatCalculator.WithVat(OrderLineTotalInCents, AverageOrderVatPercentage); }
			set { }
		}

		/// <summary>
		///     Gets the subtotal (orderlines + shipping + payment including discounts
		/// </summary>
		[DataMember]
		public int SubtotalInCents
		{
			get { return PricesAreIncludingVAT ? VatCalculator.WithoutVat(OrderTotalInCents, AverageOrderVatPercentage) : OrderTotalInCents; }
			set { }
		}

		/// <summary>
		///     Gets the total of all the orderlines in cents without VAT
		/// </summary>
		[DataMember]
		public int OrderLineTotalWithoutVatInCents
		{
			get { return PricesAreIncludingVAT ? VatCalculator.WithoutVat(OrderLineTotalInCents, AverageOrderVatPercentage) : OrderLineTotalInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the payment provider costs without vat information cents.
		/// </summary>
		/// <value>
		/// The payment provider costs without vat information cents.
		/// </value>
		[DataMember]
		public int PaymentProviderCostsWithoutVatInCents
		{
			get { return PricesAreIncludingVAT ? VatCalculator.WithoutVat(PaymentProviderPriceInCents, AverageOrderVatPercentage) : PaymentProviderPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the payment provider costs with vat information cents.
		/// </summary>
		/// <value>
		/// The payment provider costs with vat information cents.
		/// </value>
		[DataMember]
		public int PaymentProviderCostsWithVatInCents
		{
			get { return PricesAreIncludingVAT ? PaymentProviderPriceInCents : VatCalculator.WithVat(PaymentProviderPriceInCents, AverageOrderVatPercentage); }
			set { }
		}

		/// <summary>
		/// Gets or sets the shipping provider costs without vat information cents.
		/// </summary>
		/// <value>
		/// The shipping provider costs without vat information cents.
		/// </value>
		[DataMember]
		public int ShippingProviderCostsWithoutVatInCents
		{
			get { return PricesAreIncludingVAT ? VatCalculator.WithoutVat(ActiveShippingProviderAmountInCents, AverageOrderVatPercentage) : ActiveShippingProviderAmountInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the shipping provider costs with vat information cents.
		/// </summary>
		/// <value>
		/// The shipping provider costs with vat information cents.
		/// </value>
		[DataMember]
		public int ShippingProviderCostsWithVatInCents
		{
			get { return PricesAreIncludingVAT ? ActiveShippingProviderAmountInCents : VatCalculator.WithVat(ActiveShippingProviderAmountInCents, AverageOrderVatPercentage); }
			set { }
		}

		#endregion

		#region Event firing

		internal BeforeOrderLineUpdatedEventArgs FireBeforeOrderLineUpdatedEvent(OrderLine orderLine)
		{
			BeforeOrderLineUpdatedEventArgs beforeUpdatedEventArgs = null;
			if (BeforeOrderLineUpdated != null)
			{
				beforeUpdatedEventArgs = new BeforeOrderLineUpdatedEventArgs {OrderLine = orderLine};
				BeforeOrderLineUpdated(this, beforeUpdatedEventArgs);
			}
			return beforeUpdatedEventArgs;
		}

		internal void FireBeforeOrderLineCreatedEvent()
		{
			if (BeforeOrderLineCreated != null)
			{
				var afterCreatedEventArgs = new BeforeOrderLineCreatedEventArgs {OrderLine = null};

				BeforeOrderLineCreated(this, afterCreatedEventArgs);
			}
		}

		internal void FireAfterOrderLineCreatedEvent(OrderLine orderLine)
		{
			if (AfterOrderLineCreated != null)
			{
				var afterCreatedEventArgs = new AfterOrderLineCreatedEventArgs {OrderLine = orderLine};

				AfterOrderLineCreated(this, afterCreatedEventArgs);
			}
		}

		internal void FireAfterOrderLineUpdatedEvent(OrderLine orderLine)
		{
			if (AfterOrderLineUpdated != null)
			{
				AfterOrderLineUpdated(this, new AfterOrderLineUpdatedEventArgs {OrderLine = orderLine});
			}
		}

		internal BeforeOrderLineDeletedEventArgs FireBeforeOrderLineDeletedEvent(OrderLine orderLine)
		{
			BeforeOrderLineDeletedEventArgs beforeDeletedEventArgs = null;
			if (BeforeOrderLineDeleted != null)
			{
				beforeDeletedEventArgs = new BeforeOrderLineDeletedEventArgs {OrderLine = orderLine};
				BeforeOrderLineDeleted(this, beforeDeletedEventArgs);
			}
			return beforeDeletedEventArgs;
		}

		internal void FireAfterOrderLineDeletedEvent()
		{
			if (AfterOrderLineDeleted != null)
			{
				var afterDeletedEventArgs = new AfterOrderLineDeletedEventArgs();

				AfterOrderLineDeleted(this, afterDeletedEventArgs);
			}
		}

		internal void FireBeforeOrderUpdatedEvent()
		{
			if (BeforeOrderUpdated != null)
			{
				BeforeOrderUpdated(this, new BeforeOrderUpdatedEventArgs {OrderInfo = this});
			}
		}

		internal void FireAfterOrderUpdatedEvent()
		{
			if (AfterOrderUpdated != null)
			{
				AfterOrderUpdated(this, new AfterOrderUpdatedEventArgs {OrderInfo = this});
			}
		}

		#endregion

		#region Amount Properties

		internal int? _discountAmountInCents; // stored value (XML)

		/// <summary>
		///     The Active shipping provider amount without Vat
		/// </summary>
		private int ActiveShippingProviderAmountInCents
		{
			// todo: check in conjunction with Discounts!!
			get { return ShippingProviderAmountInCents; }
		}

		/// <summary>
		/// Gets or sets the shipping provider vat amount information cents.
		/// </summary>
		/// <value>
		/// The shipping provider vat amount information cents.
		/// </value>
		[DataMember]
		public int ShippingProviderVatAmountInCents
		{
			get { return VatCalculator.VatAmountFromWithoutVat(ShippingProviderCostsWithoutVatInCents, AverageOrderVatPercentage); }
			set { }
		}

		/// <summary>
		/// Gets the average order vat percentage.
		/// </summary>
		/// <value>
		/// The average order vat percentage.
		/// </value>
		public decimal AverageOrderVatPercentage
		{
			// weighted average
			get
			{
				if (OrderLines.Any() && OrderLines.Sum(line => line.ProductInfo.ItemCount.GetValueOrDefault(1)*line.ProductInfo.PriceInCents) != 0)
				{
					return OrderLines.Sum(line => line.OrderLineVat*line.ProductInfo.ItemCount.GetValueOrDefault(1)*line.ProductInfo.PriceInCents)/OrderLines.Sum(line => line.ProductInfo.ItemCount.GetValueOrDefault(1)*line.ProductInfo.PriceInCents);
				}

				return 0;
			}
		}

		/// <summary>
		///     the payment provider vat percentage
		/// </summary>
		//[DataMember] public decimal PaymentProviderVat;
		[DataMember]
		public decimal PaymentProviderVatAmountInCents
		{
			get { return PaymentProviderCostsWithVatInCents - PaymentProviderCostsWithoutVatInCents; }
			set { }
		}

		/// <summary>
		///     The total discount amount over the order
		/// </summary>
		[DataMember]
		public int DiscountAmountInCents
		{
			get
			{
				// dit gaat niet goed in backend
				if (!_discountAmountInCents.HasValue && !LegacyDataReadBackMode)
					//if (EventsOn && Status == OrderStatus.Incomplete) // if incomplete, recalculate based on current kortingen
					ApplyDiscounts();
				return _discountAmountInCents.GetValueOrDefault();
			}
			set { _discountAmountInCents = value; } // never remove this, for backwards compatibility!!
		}

		private void ApplyDiscounts()
		{
			// todo: this function is a bit hackish, find a nice place for this functionality
			OrderLines.ForEach(line => line.OrderDiscountInCents = 0); // reset

			// todo: deze Math.Min houdt geen rekening met FreeShipping!
			_discountAmountInCents = Math.Min(OrderLines.Sum(orderline => orderline.OrderLineAmountInCents), OrderDiscounts.Sum(discount => IO.Container.Resolve<IDiscountCalculationService>().DiscountAmountForOrder(discount, this)));

			OrderLines.ForEach(line => line.OrderDiscountInCents = _discountAmountInCents.GetValueOrDefault()/OrderLines.Count);

			int leftOverCents = _discountAmountInCents.GetValueOrDefault() - OrderLines.Sum(line => line.OrderDiscountInCents);
			//if (leftOverCents > OrderLines.Count) throw new Exception("wzzaa");
			//if (leftOverCents != _discountAmountInCents.GetValueOrDefault() % OrderLines.Count) throw new Exception("wzzaa"); // => dit is raar, maar heeft ws met cents in product te maken..
			for (int i = 0; i < leftOverCents; i++)
			{
				OrderLines[i%OrderLines.Count].OrderDiscountInCents++;
			}
		}

		#endregion

		internal class CustomOrderValidation
		{
			/// <summary>
			/// The condition
			/// </summary>
			public Predicate<OrderInfo> condition;
			/// <summary>
			/// The error dictionary item
			/// </summary>
			public Func<OrderInfo, string> errorDictionaryItem;
		}

		internal void ResetDiscounts()
		{
			_discountAmountInCents = null;
		}

		/// <summary>
		/// Gets or sets a value indicating whether [created information test mode].
		/// </summary>
		/// <value>
		/// <c>true</c> if [created information test mode]; otherwise, <c>false</c>.
		/// </value>
		public bool CreatedInTestMode { get; set; }
	}

	#region Event Argument Classes

	/// <summary>
	/// 
	/// </summary>
	public class OrderPaidChangedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order information.
		/// </summary>
		/// <value>
		/// The order information.
		/// </value>
		public OrderInfo OrderInfo { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether [paid].
		/// </summary>
		/// <value>
		///   <c>true</c> if [paid]; otherwise, <c>false</c>.
		/// </value>
		public bool Paid { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class BeforeOrderStatusChangedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order information.
		/// </summary>
		/// <value>
		/// The order information.
		/// </value>
		public OrderInfo OrderInfo { get; set; }
		/// <summary>
		/// Gets or sets the order status.
		/// </summary>
		/// <value>
		/// The order status.
		/// </value>
		public OrderStatus OrderStatus { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class AfterOrderStatusChangedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order information.
		/// </summary>
		/// <value>
		/// The order information.
		/// </value>
		public OrderInfo OrderInfo { get; set; }
		/// <summary>
		/// Gets or sets the order status.
		/// </summary>
		/// <value>
		/// The order status.
		/// </value>
		public OrderStatus OrderStatus { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether [send emails].
		/// </summary>
		/// <value>
		///   <c>true</c> if [send emails]; otherwise, <c>false</c>.
		/// </value>
		public bool SendEmails { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class BeforeOrderLineCreatedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order line.
		/// </summary>
		/// <value>
		/// The order line.
		/// </value>
		public OrderLine OrderLine { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether [cancel].
		/// </summary>
		/// <value>
		///   <c>true</c> if [cancel]; otherwise, <c>false</c>.
		/// </value>
		public bool Cancel { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class AfterOrderLineCreatedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order line.
		/// </summary>
		/// <value>
		/// The order line.
		/// </value>
		public OrderLine OrderLine { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class BeforeOrderLineUpdatedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order line.
		/// </summary>
		/// <value>
		/// The order line.
		/// </value>
		public OrderLine OrderLine { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether [cancel].
		/// </summary>
		/// <value>
		///   <c>true</c> if [cancel]; otherwise, <c>false</c>.
		/// </value>
		public bool Cancel { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class AfterOrderLineUpdatedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order line.
		/// </summary>
		/// <value>
		/// The order line.
		/// </value>
		public OrderLine OrderLine { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class BeforeOrderLineDeletedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order line.
		/// </summary>
		/// <value>
		/// The order line.
		/// </value>
		public OrderLine OrderLine { get; set; }
		/// <summary>
		/// Gets or sets a value indicating whether [cancel].
		/// </summary>
		/// <value>
		///   <c>true</c> if [cancel]; otherwise, <c>false</c>.
		/// </value>
		public bool Cancel { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class AfterOrderLineDeletedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order line.
		/// </summary>
		/// <value>
		/// The order line.
		/// </value>
		public OrderLine OrderLine { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class BeforeOrderUpdatedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order information.
		/// </summary>
		/// <value>
		/// The order information.
		/// </value>
		public OrderInfo OrderInfo { get; set; }
	}

	/// <summary>
	/// 
	/// </summary>
	public class AfterOrderUpdatedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets or sets the order information.
		/// </summary>
		/// <value>
		/// The order information.
		/// </value>
		public OrderInfo OrderInfo { get; set; }
	}

	#endregion
}