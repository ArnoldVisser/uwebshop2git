﻿using System;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using uWebshop.Common;
using uWebshop.Domain.Helpers;

namespace uWebshop.Domain
{
#pragma warning disable 1591
	[DataContract(Namespace = "")]
	[Serializable]
	public class ProductVariantInfo
	{
		//[XmlIgnore] internal IOrderInfo Order;
		internal int DiscountId;
		[XmlIgnore][NonSerialized] internal ProductInfo Product;
		[XmlIgnore][NonSerialized] private ProductVariant _variant;

		public ProductVariantInfo()
		{
		}

		/// <summary>
		///     Set product variant info based on the variant already in the order
		/// </summary>
		/// <param name="productVariant"></param>
		/// <param name="product"></param>
		/// <param name="productVat"></param>
		public ProductVariantInfo(OrderedProductVariant productVariant, ProductInfo product, decimal productVat)
		{
			Product = product;

			Id = productVariant.VariantId;
			Title = productVariant.Title;
			Group = productVariant.Group;
			Weight = productVariant.Weight;
			Length = productVariant.Length;
			Height = productVariant.Height;
			Width = productVariant.Width;
			PriceInCents = productVariant.PriceInCents;
			ChangedOn = DateTime.Now;
			Vat = productVat;
			DiscountAmountInCents = productVariant.DiscountAmount;
			DiscountPercentage = productVariant.DiscountPercentage;
			DocTypeAlias = productVariant.DocTypeAlias;
		}

		/// <summary>
		/// Set product variant info based on a product variant in the catalog
		/// </summary>
		/// <param name="productVariant">The product variant.</param>
		/// <param name="product">The product.</param>
		/// <param name="itemCount">The item count.</param>
		public ProductVariantInfo(ProductVariant productVariant, ProductInfo product, int itemCount)
		{
			Product = product;

			Id = productVariant.Id;
			Title = productVariant.Title;
			Group = productVariant.Group;
			Weight = productVariant.Weight;
			Length = productVariant.Length;
			Height = productVariant.Height;
			Width = productVariant.Width;
			PriceInCents = productVariant.OriginalPriceInCents;
			ChangedOn = DateTime.Now;
			Vat = productVariant.Vat;

			if (productVariant.IsDiscounted)
			{
				DiscountId = productVariant.ProductVariantDiscount.Id;
				if (productVariant.ProductVariantDiscount.DiscountType == DiscountType.Amount)
					DiscountAmountInCents = productVariant.ProductVariantDiscount.RangedDiscountValue(itemCount);
				else if (productVariant.ProductVariantDiscount.DiscountType == DiscountType.Percentage)
					DiscountPercentage = productVariant.ProductVariantDiscount.RangedDiscountValue(itemCount) / 100m;
				else if (productVariant.ProductVariantDiscount.DiscountType == DiscountType.NewPrice)
					PriceInCents = productVariant.ProductVariantDiscount.RangedDiscountValue(itemCount);
			}

			DocTypeAlias = productVariant.NodeTypeAlias;
		}

		[DataMember]
		public int Id { get; set; }

		[DataMember]
		public string Title { get; set; }

		[DataMember]
		public DateTime ChangedOn { get; set; }

		[DataMember]
		public string Group { get; set; }

		[DataMember]
		public double Weight { get; set; }

		[DataMember]
		public double Length { get; set; }

		[DataMember]
		public double Height { get; set; }

		[DataMember]
		public double Width { get; set; }

		[DataMember]
		public int PriceInCents { get; set; }

		[DataMember]
		public decimal Vat { get; set; }

		[DataMember]
		public string DocTypeAlias { get; set; }

		[DataMember]
		public int DiscountAmountInCents { get; set; }

		[DataMember]
		public decimal DiscountPercentage { get; set; }

		[XmlIgnore]
		public ProductVariant Variant
		{
			get { return _variant ?? (_variant = DomainHelper.GetProductVariantById(Id)); }
		}

		public int DiscountedPriceInCents
		{
			get { return (int) ((100 - DiscountPercentage)*PriceInCents)/100 - DiscountAmountInCents; }
			set { }
		}

		[DataMember]
		public int PriceWithVatInCents
		{
			get { return Product.Order.PricesAreIncludingVAT ? PriceInCents : VatCalculator.WithVat(PriceInCents, Vat); }
			set { }
		}

		[DataMember]
		public int PriceWithoutVatInCents
		{
			get { return Product.Order.PricesAreIncludingVAT ? VatCalculator.WithoutVat(PriceInCents, Vat) : PriceInCents; }
			set { }
		}

		[DataMember]
		public decimal PriceWithoutVat
		{
			get { return PriceWithoutVatInCents/100m; }
			set { }
		}

		[DataMember]
		public decimal PriceWithVat
		{
			get { return PriceWithVatInCents/100m; }
			set { }
		}
	}
}