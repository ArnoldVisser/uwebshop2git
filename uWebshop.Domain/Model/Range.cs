﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace uWebshop.Domain
{
	/// <summary>
	///     Class representing a range
	/// </summary>
	[DataContract(Namespace = "")]
	public class Range
	{
		//internal bool? PricesAreIncludingVAT;

		/// <summary>
		///     Gets the from value of the range
		/// </summary>
		[DataMember]
		public int From { get; set; }

		/// <summary>
		///     Gets the to value of the range
		/// </summary>
		[DataMember]
		public int To { get; set; }

		//[DataMember]
		//public int ProductId { get; set; }

		/// <summary>
		///     Gets the amount of the range
		/// </summary>
		[DataMember]
		public int PriceInCents { get; set; }

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return string.Format("{0}|{1}|{2}", From, To, PriceInCents);
		}

		/// <summary>
		/// Creates from string.
		/// </summary>
		/// <param name="rangesData">The ranges data.</param>
		/// <returns></returns>
		public static List<Range> CreateFromString(string rangesData) //, decimal vat, int productId, bool pricesAreIncludingVAT)
		{
			try
			{
				if (string.IsNullOrWhiteSpace(rangesData) || rangesData.Contains("Range")) return new List<Range>();
				return rangesData.Split('#').Select(rangeCode => rangeCode.Split('|')).Where(splitStrings => splitStrings.Length > 2).Select(splitStrings => new Range {From = int.Parse(splitStrings[0]), PriceInCents = int.Parse(splitStrings[2]), To = splitStrings[1] == "*" ? int.MaxValue : int.Parse(splitStrings[1]), 
					//Vat = vat, 
					//ProductId = productId, 
					//PricesAreIncludingVAT = pricesAreIncludingVAT,
				}).ToList();
			}
			catch (Exception ex)
			{
				Log.Instance.LogDebug("Exception in parsing ranges data: " + ex);
				return new List<Range>();
			}
		}
	}

	/// <summary>
	/// 
	/// </summary>
	public static class RangesListExtensions
	{
		/// <summary>
		/// Automatics the ranges string.
		/// </summary>
		/// <param name="ranges">The ranges.</param>
		/// <returns></returns>
		public static string ToRangesString(this IEnumerable<Range> ranges)
		{
			return string.Join("#", ranges.Select(range => range.ToString()));
		}

		/// <summary>
		/// Finds the range for value.
		/// </summary>
		/// <param name="ranges">The ranges.</param>
		/// <param name="itemCount">The item count.</param>
		/// <returns></returns>
		public static Range FindRangeForValue(this IEnumerable<Range> ranges, int itemCount)
		{
			return ranges.FirstOrDefault(x => itemCount >= x.From && itemCount < x.To);
		}

		/// <summary>
		/// Gets the range amount for value.
		/// </summary>
		/// <param name="ranges">The ranges.</param>
		/// <param name="itemCount">The item count.</param>
		/// <returns></returns>
		public static int? GetRangeAmountForValue(this IEnumerable<Range> ranges, int itemCount)
		{
			Range range = FindRangeForValue(ranges, itemCount);
			if (range == null) return null;
			return range.PriceInCents;
		}
	}
}