﻿using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	///     the uWebshop settings
	/// </summary>
	[ContentType(ParentContentType = typeof (UwebshopRootContentType), Name = "Settings", Description = "#SettingsSectionDescription", Alias = NodeAlias, Icon = ContentIcon.Toolbox, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (Settings)})]
	public class Settings : uWebshopEntity, ISettingsService
	{
		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsSettings";

		internal Settings()
		{
		}

		/// <summary>
		///     Are all prices in the store including VAT?
		/// </summary>
		[ContentPropertyType(Alias = "includingVat", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Global, Name = "#IncludingVat", Description = "#IncludingVatSettingDescription")]
		public bool IncludingVat { get; internal set; }

		/// <summary>
		///     Render all url's lowercase instead of Case Sensitive
		/// </summary>
		[ContentPropertyType(Alias = "lowercaseUrls", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Global, Name = "#LowercaseUrls", Description = "#LowercaseUrlsSettingDescription")]
		public bool UseLowercaseUrls { get; internal set; }

		/// <summary>
		///     Lifetime of an incomplete order in minutes
		/// </summary>
		[ContentPropertyType(Alias = "incompleteOrderLifetime", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#IncompleteOrderLifetime", Description = "#OrderLifetimeSettingDescription", Mandatory = true)]
		public int IncompleOrderLifetime { get; internal set; }
	}
}