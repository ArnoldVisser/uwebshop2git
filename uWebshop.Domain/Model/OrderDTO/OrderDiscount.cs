﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using uWebshop.Common;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.OrderDTO
{
#pragma warning disable 1591
	public class OrderDiscount : IOrderDiscount
	{
		public OrderDiscount()
		{
		}

		


		public OrderDiscount(IOrderDiscount discountOrder)
		{
			Id = discountOrder.Id;
			OriginalId = discountOrder.OriginalId;
			DiscountType = discountOrder.DiscountType;
			DiscountValue = discountOrder.DiscountValue;
			Condition = discountOrder.Condition;
			NumberOfItemsCondition = discountOrder.NumberOfItemsCondition;
			RequiredItemIds = discountOrder.RequiredItemIds ?? new List<int>();
			MinimalOrderAmount = discountOrder.MinimalOrderAmount;
			CouponCode = discountOrder.CouponCode;
			MemberGroups = discountOrder.MemberGroups;
			AffectedOrderlines = discountOrder.AffectedOrderlines;
			RangesString = discountOrder.RangesString;
			OncePerCustomer = discountOrder.OncePerCustomer;
		}

		public int OriginalId { get; set; }

		public DiscountType DiscountType { get; set; }
		public int DiscountValue { get; set; }
		public string RangesString { get; set; }

		// conditions
		public DiscountOrderCondition Condition { get; set; }
		public int NumberOfItemsCondition { get; set; }

		public bool Disable
		{
			get { return false; }
		}

		public List<int> RequiredItemIds { get; set; }
		public int MinimalOrderAmount { get; set; }

		/// <summary>
		///     Minimum amount of the order before discount is valid
		/// </summary>
		public decimal MinimumOrderAmount
		{
			get { return MinimumOrderAmountInCents / 100m; }
		}

		public string CouponCode { get; set; }
		public List<string> MemberGroups { get; set; }

		// interface compliance
		public int MinimumOrderAmountInCents
		{
			get { return MinimalOrderAmount; }
		}

		[XmlIgnore]
		public bool CounterEnabled
		{
			get { return false; }
		}

		[XmlIgnore]
		public int Counter
		{
			get { return 1; }
		}

		public string Title
		{
			get
			{
				//var originalDiscount = IO.Container.Resolve<IDiscountService>().GetById(OriginalId);
				//if (originalDiscount )
				throw new NotImplementedException();
			}
		}

		public List<int> AffectedOrderlines { get; set; }

		public bool OncePerCustomer { get; set; }

		public int Id { get; set; }
	}
}