﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;
using uWebshop.Common;
using uWebshop.Domain.Businesslogic.VATChecking;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.OrderDTO
{
#pragma warning disable 1591
	public class Order : IConvertibleToOrderInfo
	{
		// deze 5 zijn belangrijk, maarr, zitten al in de tabel als kolommen
		//public string OrderNumber;
		//public int? StoreOrderReferenceId;
		//public Guid UniqueOrderId;
		//public DateTime? ConfirmDate;
		//public OrderStatus HandlingStatus;

		public int XMLVersion = 109;

		public bool? CreatedInTestMode;

		public int ChargedAmount;
		public DateTime? ConfirmDate = DateTime.Now;
		public int? CorrespondingOrderDocumentId;
		public List<string> CouponCodes = new List<string>();
		public CustomerInfo CustomerInfo;
		public List<OrderDiscount> Discounts;

		public bool IncludingVAT;
		public List<OrderLine> OrderLines;
		public DateTime? PaidDate;
		public PaymentInfo PaymentInfo;
		public int PaymentProviderPrice;
		public int? PaymentProviderOrderPercentage;
		public ValidateSaveAction ReValidateSaveAction;
		public int RegionalVatAmount;
		public bool? RevalidateOrderOnLoad;

		public ShippingInfo ShippingInfo;
		public int ShippingProviderPrice;
		public bool? StockUpdated;
		public StoreInfo StoreInfo;
		public bool TermsAccepted;
		public bool VATCharged;
		[XmlIgnore] private XDocument _customData;

		public Order()
		{
		}

		public Order(OrderInfo orderInfo)
		{
			TermsAccepted = orderInfo.TermsAccepted;
			PaidDate = orderInfo.PaidDate;
			ConfirmDate = orderInfo.ConfirmDate;
			OrderLines = orderInfo.OrderLines.Select(line => new OrderLine(line)).ToList();
			CouponCodes = orderInfo.CouponCodesData;
			CustomerInfo = orderInfo.CustomerInfo;
			ShippingInfo = orderInfo.ShippingInfo;
			PaymentInfo = orderInfo.PaymentInfo;
			StoreInfo = orderInfo.StoreInfo;
			// todo: onderstaand kan waarschijnlijk anders (geen check = dubbel)
			Discounts = orderInfo.OrderDiscounts
				.Select(discount => new OrderDiscount(discount)).ToList();
			IncludingVAT = orderInfo.PricesAreIncludingVAT;

			PaymentProviderPrice = orderInfo.PaymentProviderAmount;
			PaymentProviderOrderPercentage = orderInfo.PaymentProviderOrderPercentage;
			
			ShippingProviderPrice = orderInfo.ShippingProviderAmountInCents;
			RegionalVatAmount = orderInfo.RegionalVatInCents;

			VATCharged = orderInfo.VATCharged;
			ChargedAmount = orderInfo.ChargedAmountInCents;

			CorrespondingOrderDocumentId = orderInfo.OrderNodeId;

			RevalidateOrderOnLoad = orderInfo.RevalidateOrderOnLoad; // version 2.1 hack
			ReValidateSaveAction = orderInfo.ReValidateSaveAction; // version 2.1 hack

			StockUpdated = orderInfo.StockUpdated;
			CreatedInTestMode = orderInfo.CreatedInTestMode;
		}

		[DataMember]
		public XElement CustomData
		{
			get { return _customData != null ? _customData.Root : null; }
			set
			{
				_customData = new XDocument();
				_customData.Add(value);
			}
		}

		public OrderInfo ToOrderInfo()
		{
			var orderInfo = new OrderInfo();
			orderInfo.CreatedInTestMode = CreatedInTestMode.GetValueOrDefault();
			orderInfo.PaidDate = PaidDate;
			orderInfo.ConfirmDate = ConfirmDate;
			orderInfo.OrderLines = OrderLines.Select(line => line.ToOrderLine(orderInfo)).ToList();
			orderInfo.SetOrderReferenceOnOrderLinesAndProductInfos();
			orderInfo.CouponCodesData = CouponCodes;
			orderInfo.CustomerInfo = CustomerInfo;
			orderInfo.ShippingInfo = ShippingInfo;
			orderInfo.PaymentInfo = PaymentInfo;
			orderInfo.StoreInfo = StoreInfo;
			orderInfo.TermsAccepted = TermsAccepted;

			IO.Container.Resolve<IOrderService>().UseStoredDiscounts(orderInfo, new List<IOrderDiscount>(Discounts));

			orderInfo.VATCheckService = new FixedValueIvatChecker(!VATCharged);

			orderInfo.PricesAreIncludingVAT = IncludingVAT;
			orderInfo.PaymentProviderAmount = PaymentProviderPrice;
			orderInfo.PaymentProviderOrderPercentage = PaymentProviderOrderPercentage.GetValueOrDefault();
			orderInfo.ShippingProviderAmountInCents = ShippingProviderPrice;
			orderInfo.RegionalVatInCents = RegionalVatAmount; // todo!!!
			orderInfo.EventsOn = true;

			orderInfo.OrderNodeId = CorrespondingOrderDocumentId.GetValueOrDefault(0);

			orderInfo.RevalidateOrderOnLoad = RevalidateOrderOnLoad; // version 2.1 hack
			orderInfo.ReValidateSaveAction = ReValidateSaveAction;

			orderInfo.StockUpdated = StockUpdated.GetValueOrDefault();

			return orderInfo;
		}
	}
}