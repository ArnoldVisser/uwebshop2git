﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using uWebshop.Common;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	/// Class representing the catalog in  webshop, containing a group of categories
	/// </summary>
	[ContentType(Name = "Catalog", Description = "#CatalogDescription", Alias = NodeAlias, Icon = ContentIcon.ClipboardList, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (CategoryRepositoryContentType), typeof (ProductRepositoryContentType)}, ParentContentType = typeof (UwebshopRootContentType))]
	[DataContract(Namespace = "")]
	public static class Catalog
	{
		/// <summary>
		/// The category repository node alias
		/// </summary>
		public const string CategoryRepositoryNodeAlias = "uwbsCategoryRepository";
		/// <summary>
		/// The product repository node alias
		/// </summary>
		public const string ProductRepositoryNodeAlias = "uwbsProductRepository";
		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsCatalog";

		/// <summary>
		/// Returns all the categories with products and prices for the given store or current store if null
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		internal static List<Category> GetAllRootCategories(string storeAlias = null)
		{
			if (string.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}
			return IO.Container.Resolve<ICategoryService>().GetAllRootCategories(storeAlias);
		}

		/// <summary>
		/// Gets the category repository nodes.
		/// </summary>
		/// <returns></returns>
		internal static IEnumerable<uWebshopEntity> GetCategoryRepositoryNodes()
		{
			return DomainHelper.GetObjectsByAlias<uWebshopEntity>(CategoryRepositoryNodeAlias, Constants.NonMultiStoreAlias);
		}

		/// <summary>
		/// Gets the category repository node.
		/// </summary>
		/// <returns></returns>
		internal static uWebshopEntity GetCategoryRepositoryNode()
		{
			return GetCategoryRepositoryNodes().FirstOrDefault();
		}
	}
}