﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using uWebshop.Common.Interfaces;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	///     Class representing a category in webshop, containing a group of products
	/// </summary>
	[DataContract(Namespace = "", IsReference = true)]
	[ContentType(ParentContentType = typeof (Catalog), Name = "Category", Description = "#CategoryDescription", Alias = NodeAlias, Icon = ContentIcon.Folder, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (Category), typeof (Product)})]
	public class Category : MultiStoreUwebshopContent, ICategory
	{
		internal string StoreAlias;

		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsCategory";
		internal Func<List<Product>> ProductsFactory;
		private List<Category> _categories;

		internal List<int> _categoryIds;
		private string _localizedUrl;
		private Category _parentCategory;

		private string _parentNodeTypeAlias;
		private List<Product> _products;
		private List<Product> _productsRecursively;

		/// <summary>
		/// Gets or sets the parent node type alias.
		/// </summary>
		/// <value>
		/// The parent node type alias.
		/// </value>
		public string ParentNodeTypeAlias
		{
			// todo: hmm, some issues with loops when trying to load this or ParentCategory in the service/repo
			get { return _parentNodeTypeAlias ?? (_parentNodeTypeAlias = IO.Container.Resolve<ICMSEntityRepository>().GetByGlobalId(ParentId).NodeTypeAlias); }
			set { }
		}

		/// <summary>
		/// Gets or sets the parent category.
		/// </summary>
		/// <value>
		/// The parent category.
		/// </value>
		public ICategory ParentCategory
		{
			get { return _parentCategory ?? (_parentCategory = DomainHelper.GetCategoryById(ParentId, StoreAlias)); }
			set { }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [has categories].
		/// </summary>
		/// <value>
		///   <c>true</c> if [has categories]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool HasCategories { get; set; }

		/// <summary>
		/// Is this content enabled?
		/// </summary>
		public override bool Disabled { get; set; }

		/// <summary>
		///     Gets a list of products which belong to the category
		/// </summary>
		[DataMember]
		public IEnumerable<Product> Products
		{
			// todo: DIP
			get { return _products ?? (_products = ProductsFactory()); } //DomainHelper.GetProducts(Id).ToList()); }
			set { }
		}

		/// <summary>
		///     Gets a list of products which belong to the category
		/// </summary>
		[DataMember]
		public IEnumerable<Product> ProductsRecursive
		{
			get { return _productsRecursively ?? (_productsRecursively = StoreHelper.GetProductsRecursive(Id, StoreAlias)); }
			set { }
		}

		/// <summary>
		/// Gets the parent categories.
		/// </summary>
		/// <value>
		/// The parent categories.
		/// </value>
		public IEnumerable<Category> ParentCategories
		{
			get { return DomainHelper.GetAllCategories(false, StoreAlias).Where(c => c.Categories.Contains(this)); }
		}

		private IEnumerable<Category> ParentCategories1
		{
			get { return DomainHelper.GetAllCategories(false, StoreAlias).Where(c => c._categories != null && c._categories.Contains(this)); }
		}

		/// <summary>
		///     Gets a list of (sub)categories of the current category
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "categories", DataType = DataType.MultiContentPickerCategories, Tab = ContentTypeTab.Global, Name = "#SubCategories", Description = "#SubCategoriesDescription", Mandatory = false, SortOrder = 6)]
		public IEnumerable<Category> Categories
		{
			get
			{
				if (_categories == null)
				{
					List<Category> recursiveParentsIncludingSelf = GetParentCategoriesRecursiveToPreventLoops();
					recursiveParentsIncludingSelf.Add(this);
					// todo: remove loops
					_categories = DomainHelper.GetAllCategories(false, StoreAlias).Where(x => x.ParentId == Id).ToList();
					_categories.AddRange(_categoryIds.Select(id => DomainHelper.GetCategoryById(id, StoreAlias)).Where(x => x != null));
					_categories = _categories.Where(c => !recursiveParentsIncludingSelf.Contains(c)).ToList();
				}
				return _categories;
			}
			set { }
		}

		/// <summary>
		///     Gets the tags of the category
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "metaTags", DataType = DataType.Tags, Tab = ContentTypeTab.Global, Name = "#Tags", Description = "#TagsDescription", Mandatory = false, SortOrder = 4)]
		public string[] Tags { get; set; }

		/// <summary>
		///     Gets the image of the category
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "images", DataType = DataType.MultiContentPickerImages, Tab = ContentTypeTab.Details, Name = "#Images", Description = "#ImagesDescription", Mandatory = false, SortOrder = 7)]
		public IEnumerable<Image> Images { get; set; }

		/// <summary>
		/// Gets or sets the localized URL.
		/// </summary>
		/// <value>
		/// The localized URL.
		/// </value>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		[Obsolete("use UrlName")]
		public string LocalizedUrl
		{
			get { return UrlName; }
			set { }
		}

		/// <summary>
		///     Returns the localized url (ID-URL)
		/// </summary>
		[DataMember(Name = "urlName")]
		public new string UrlName
		{
			get
			{
				if (_localizedUrl != null) return _localizedUrl;

				string urlFormat = DomainHelper.BuildUrlFromTemplate(UwebshopConfiguration.Current.CategoryUrl, this) ?? URL;

				_localizedUrl = IO.Container.Resolve<ICMSApplication>().ApplyUrlFormatRules(urlFormat);

				return _localizedUrl;
			}
			set { _localizedUrl = value; }
		}

		/// <summary>
		///     The url for this category based on the current store
		/// </summary>
		public string Url
		{
			get { return NiceUrl(); }
			set { }
		}

		/// <summary>
		/// Determines whether the specified node type alias is alias.
		/// </summary>
		/// <param name="nodeTypeAlias">The node type alias.</param>
		/// <returns></returns>
		public static bool IsAlias(string nodeTypeAlias)
		{
			return nodeTypeAlias.StartsWith(NodeAlias);
		}

		private List<Category> GetParentCategoriesRecursiveToPreventLoops()
		{
			List<Category> parents = ParentCategories1.ToList();
			var prevparents = new List<Category>();
			while (parents.Count > prevparents.Count)
			{
				prevparents = parents;
				parents = parents.Concat(parents.SelectMany(p => p.ParentCategories1)).Distinct().ToList();
			}
			return parents;
		}

		/// <summary>
		///     The url for this category bsaed on a storeAlias
		/// </summary>
		/// <param name="storeAlias"></param>
		/// <returns></returns>
		public string NiceUrl(string storeAlias = null)
		{
			return StoreHelper.StoreService.GetCategoryNiceUrl(this, storeAlias);
		}

		/// <summary>
		///     The full localized URL (ID-URL) of the category
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use NiceUrl(string storeAlias)")]
		public string FullLocalizedUrl(string storeAlias = null)
		{
			return NiceUrl(storeAlias);
		}

		internal void ClearCachedValues()
		{
// temporary hack
			_categories = null;
			_parentNodeTypeAlias = null;
			_products = null;
			_productsRecursively = null;
			_parentCategory = null;
		}
	}
}