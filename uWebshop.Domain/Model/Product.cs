﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Businesslogic;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Namespace = "", IsReference = true)]
	[ContentType(ParentContentType = typeof (Catalog), Name = "Product", Description = "#ProductDescription", Alias = NodeAlias, Icon = ContentIcon.BoxLabel, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (ProductVariant)})]
	public class Product : MultiStoreUwebshopContent, IProduct
	{
		/// <summary>
		/// The node alias
		/// </summary>
		public const string NodeAlias = "uwbsProduct";

		/// <summary>
		/// The stock alias
		/// </summary>
		public const string StockAlias = "stock";

		// todo: remove all/most logic and use simple properties
		private List<Category> _categories;
		internal List<int> CategoryIds;

		#region Simple properties

		/// <summary>
		/// Gets a value indicating whether [prices including vat].
		/// </summary>
		/// <value>
		///   <c>true</c> if [prices including vat]; otherwise, <c>false</c>.
		/// </value>
		public bool PricesIncludingVat { get; internal set; }

		/// <summary>
		/// Gets or sets a value indicating whether [has categories].
		/// </summary>
		/// <value>
		///   <c>true</c> if [has categories]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool HasCategories { get; set; }

		#region global tab

		/// <summary>
		///     Product Number
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "sku", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#SKU", Description = "#SKUDescription", Mandatory = false, SortOrder = 3)]
		public string SKU { get; set; }

		/// <summary>
		///     Gets the tags of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "metaTags", DataType = DataType.Tags, Tab = ContentTypeTab.Global, Name = "#Tags", Description = "#TagsDescription", Mandatory = false, SortOrder = 5)]
		public string[] Tags { get; set; }

		internal string StoreAlias;

		/// <summary>
		///     Gets the category where the product is part of
		/// </summary>
		[ContentPropertyType(Alias = "categories", DataType = DataType.MultiContentPickerCategories, Tab = ContentTypeTab.Global, Name = "#ProductCategories", Description = "#ProductCategoriesDescription", Mandatory = false, SortOrder = 7)]
		public virtual IEnumerable<ICategory> Categories
		{
			get { return _categories ?? (_categories = CategoryIds.Select(i => DomainHelper.GetCategoryById(i, StoreAlias)).Where(x => x != null).ToList()); }
			set { }
		}

		#endregion

		#region details tab

		/// <summary>
		///     List of images of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "images", DataType = DataType.MultiContentPickerImages, Tab = ContentTypeTab.Details, Name = "#Images", Description = "#ImagesDescription", Mandatory = false, SortOrder = 8)]
		public IEnumerable<Image> Images { get; set; }

		/// <summary>
		///     List of files of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "files", DataType = DataType.MultiContentPickerFiles, Tab = ContentTypeTab.Details, Name = "#Files", Description = "#FilesDescription", Mandatory = false, SortOrder = 9)]
		public IEnumerable<File> Files { get; set; }

		/// <summary>
		///     Length of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "length", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Length", Description = "#LengthDescription", Mandatory = false, SortOrder = 10)]
		public double Length { get; set; }

		/// <summary>
		///     Width of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "width", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Width", Description = "#WidthDescription", Mandatory = false, SortOrder = 11)]
		public double Width { get; set; }

		/// <summary>
		///     Height of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "height", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Height", Description = "#HeightDescription", Mandatory = false, SortOrder = 12)]
		public double Height { get; set; }

		/// <summary>
		///     Weight of the product
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "weight", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Weight", Description = "#WeightDescription", Mandatory = false, SortOrder = 13)]
		public double Weight { get; set; }

		#endregion

		#region price tab

		internal int OriginalPriceInCents { get; set; }

		/// <summary>
		///     Ranges from the range data type
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "ranges", DataType = DataType.Ranges, Tab = ContentTypeTab.Price, Name = "#Ranges", Description = "#RangesDescription", Mandatory = false, SortOrder = 15)]
		public List<Range> Ranges { get; set; }

		/// <summary>
		///     The taxpercentage of the pricing
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "vat", DataType = DataType.VatPicker, Tab = ContentTypeTab.Price, Name = "#VAT", Description = "#VatDescription", Mandatory = false, SortOrder = 16)]
		public decimal Vat { get; set; }

		/// <summary>
		///     Stock count
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "stock", DataType = DataType.Stock, Tab = ContentTypeTab.Price, Name = "#Stock", Description = "#StockDescription", Mandatory = false, SortOrder = 17)]
		public int Stock
		{
			// todo: check multistore?!?!?!
			// caching here is impossible (breaks with app wide static caching of products), per-request caching is done some levels deeper
			get { return IO.Container.Resolve<IProductService>().GetStockForProduct(Id); }
			set { }
		}

		/// <summary>
		///     Number of times this item was ordered
		/// </summary>
		[DataMember]
		public int TotalItemsOrdered { get; set; }

		/// <summary>
		///     Gets the number of times this Product is ordered
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "ordered", DataType = DataType.OrderedCount, Tab = ContentTypeTab.Price, Name = "#Ordered", Description = "#OrderedDescription", Mandatory = false, SortOrder = 18)]
		public int OrderCount
		{
			get { return UWebshopStock.GetOrderCount(Id, StoreAlias); }
			set { }
		}

		/// <summary>
		///     The status of the stock
		///     True = enabled
		///     False = disabled
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "stockStatus", DataType = DataType.EnableDisable, Tab = ContentTypeTab.Price, Name = "#StockStatus", Description = "#StockStatusDescription", Mandatory = false, SortOrder = 19)]
		public bool StockStatus { get; set; }

		/// <summary>
		///     The status of backorder
		///     True = enabled
		///     False = disabled
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "backorderStatus", DataType = DataType.EnableDisable, Tab = ContentTypeTab.Price, Name = "#BackorderStatus", Description = "#BackorderStatusDescription", Mandatory = false, SortOrder = 20)]
		public bool BackorderStatus { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [use variant stock].
		/// </summary>
		/// <value>
		///   <c>true</c> if [use variant stock]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		[ContentPropertyType(Alias = "useVariantStock", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Price, Name = "#UseVariantStock", Description = "#UseVariantStockDescription", Mandatory = false, SortOrder = 21)]
		public bool UseVariantStock { get; set; }

		#endregion

		#endregion

		#region Relations

		private List<ProductVariantGroup> _productVariantGroups;

		/// <summary>
		/// Gets or sets the variants.
		/// </summary>
		/// <value>
		/// The variants.
		/// </value>
		public IEnumerable<ProductVariant> Variants { get; set; }

		/// <summary>
		///     Gets the sale of the pricing
		/// </summary>
		[DataMember]
		public DiscountProduct ProductDiscount
		{
			get { return IO.Container.Resolve<IProductDiscountRepository>().GetDiscountByProductId(Id, StoreAlias); }
			set { }
		}

		/// <summary>
		///     The groups with variants for this product
		/// </summary>
		[DataMember]
		public IEnumerable<ProductVariantGroup> ProductVariantGroups
		{
			get { return _productVariantGroups ?? (_productVariantGroups = Variants.GroupBy(v => v.Group).Select(g => new ProductVariantGroup(g.Key, g.ToList())).ToList()); }
		}

		#endregion

		#region Calculated properties

		private string _localizedUrl;

		/// <summary>
		///     Is this product orderable
		///     stock should be higher then 0
		///     if stock is lower then 0, but backorder is enabled
		///     if the stockstatus is disabled
		///     if UseVariantStock and product has variants and all variants can be ordered
		/// </summary>
		[DataMember]
		public bool Orderable
		{
			get
			{
				if (ProductVariantGroups.Any(x => x.Required && !x.ProductVariants.Any(variant => variant.Orderable)))
				{
					return false;
				}

				if (UseVariantStock && Variants.Any() && Variants.All(x => x.Orderable))
				{
					return true;
				}
				if (UseVariantStock && Variants.Any() && Variants.All(x => !x.Orderable))
				{
					return false;
				}

				return Stock > 0 || Stock <= 0 && BackorderStatus || !StockStatus;
			}
			set { }
		}

		[DataMember]
		internal int RangedPriceInCents
		{
			get
			{
				if (Ranges != null && Ranges.Any())
				{
					Range range = Ranges.FirstOrDefault(x => x.From <= 1 && x.PriceInCents != 0);

					if (range != null)
					{
						return range.PriceInCents;
					}
				}
				return OriginalPriceInCents;
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the price information cents.
		/// </summary>
		/// <value>
		/// The price information cents.
		/// </value>
		[DataMember]
		[ContentPropertyType(Alias = "price", DataType = DataType.Price, Tab = ContentTypeTab.Price, Name = "#Price", Description = "#PriceDescription", Mandatory = false, SortOrder = 14)]
		public int PriceInCents
		{
			get { return IsDiscounted ? DiscountedPriceInCents : RangedPriceInCents; }
			set { }
		}

		/// <summary>
		///     The active tax amount in cents
		/// </summary>
		[DataMember]
		public int VatAmountInCents
		{
			get { return PriceWithVatInCents - PriceWithoutVatInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price information cents.
		/// </summary>
		/// <value>
		/// The discounted price information cents.
		/// </value>
		protected int DiscountedPriceInCents
		{
			get { return ProductDiscount.GetAdjustedPrice(RangedPriceInCents); }
			set { }
		}

		/// <summary>
		///     Returns if the pricing is discounted
		/// </summary>
		[DataMember]
		public bool IsDiscounted
		{
			get { return ProductDiscount != null; }
			set { }
		}

		/// <summary>
		/// Gets the localized URL.
		/// </summary>
		/// <value>
		/// The localized URL.
		/// </value>
		[Browsable(false)]
		[Obsolete("use UrlName")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		public string LocalizedUrl
		{
			get { return UrlName; }
		}

		/// <summary>
		///     The localized URL of the product
		///     By default (ID-URL)
		///     can be configured in the web.config
		/// </summary>
		[DataMember]
		public new string UrlName
		{
			get
			{
				if (_localizedUrl != null) return _localizedUrl;

				string urlFormat = DomainHelper.BuildUrlFromTemplate(UwebshopConfiguration.Current.ProductUrl, this) ?? URL;

				_localizedUrl = IO.Container.Resolve<ICMSApplication>().ApplyUrlFormatRules(urlFormat);

				return _localizedUrl;
			}
			set { }
		}

		/// <summary>
		///     The url for this product based on the current store
		/// </summary>
		public string Url
		{
			get { return NiceUrl(true); }
			set { }
		}

		/// <summary>
		/// The url for this product
		/// </summary>
		/// <returns></returns>
		public string NiceUrl()
		{
			return NiceUrl(false);
		}

		/// <summary>
		/// The url for this product based on the current store
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public string NiceUrl(string storeAlias)
		{
			return NiceUrl(false, storeAlias);
		}

		/// <summary>
		/// The url for this product based on the current store
		/// </summary>
		/// <param name="getCanonicalUrl">if set to <c>true</c> get the canonical URL.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public string NiceUrl(bool getCanonicalUrl, string storeAlias = null)
		{
			return StoreHelper.StoreService.GetProductNiceUrl(this, storeAlias, getCanonicalUrl);
		}

		[Browsable(false)]
		[Obsolete("Use NiceUrl()")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string FullLocalizedUrl(string storeAlias = null)
		{
			return NiceUrl(storeAlias);
		}
		
		#endregion

		#region Non-cents properties

		/// <summary>
		///     The discounted tax amount
		/// </summary>
		[DataMember]
		public decimal DiscountedVatAmount
		{
			get { return DiscountedVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     Tax amount
		/// </summary>
		[DataMember]
		public decimal OriginalVatAmount
		{
			get { return OriginalVatAmountInCents/100m; }
			set { }
		}

		/// <summary>
		///     The discounted price with tax
		/// </summary>
		[DataMember]
		public decimal DiscountedPriceWithVat
		{
			get { return DiscountedPriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets the discounted price without vat.
		/// </summary>
		/// <value>
		/// The discounted price without vat.
		/// </value>
		public decimal DiscountedPriceWithoutVat
		{
			get { return DiscountedPriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     The price of the pricing without VAT
		/// </summary>
		[DataMember]
		public decimal OriginalPriceWithoutVat
		{
			get { return OriginalPriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     The active price of the item without tax
		///     Gives the discounted price when the pricing is discounted
		///     Gives the normal price when the pricing isn't discounted
		/// </summary>
		[DataMember]
		public decimal PriceWithoutVat
		{
			get { return PriceWithoutVatInCents/100m; } // return IsDiscounted ? DiscountedPriceWithoutVat : OriginalPriceWithoutVat; }
			set { }
		}

		/// <summary>
		///     The price of the pricing with VAT
		/// </summary>
		[DataMember]
		public decimal OriginalPriceWithVat
		{
			get { return OriginalPriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     The active price of the item with tax
		///     Gives the discounted price when the pricing is discounted
		///     Gives the normal price when the pricing isn't discounted
		/// </summary>
		[DataMember]
		public decimal PriceWithVat
		{
			get { return PriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		///     The active tax amount
		/// </summary>
		[DataMember]
		public decimal VatAmount
		{
			get { return VatAmountInCents/100m; }
			set { }
		}

		#endregion

		#region Vat calculations

		/// <summary>
		///     Tax amount
		/// </summary>
		[DataMember]
		public int OriginalVatAmountInCents
		{
			get { return PricesIncludingVat ? VatCalculator.VatAmountFromWithVat(RangedPriceInCents, Vat) : VatCalculator.VatAmountFromWithoutVat(RangedPriceInCents, Vat); } // OriginalPriceWithoutVat * (Vat / 100m); }
			set { }
		}

		/// <summary>
		///     The discounted tax amount
		/// </summary>
		[DataMember]
		public int DiscountedVatAmountInCents
		{
			get { return PricesIncludingVat ? VatCalculator.VatAmountFromWithVat(DiscountedPriceInCents, Vat) : VatCalculator.VatAmountFromWithoutVat(DiscountedPriceInCents, Vat); } // OriginalPriceWithoutVat * (Vat / 100m); }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price with vat information cents.
		/// </summary>
		/// <value>
		/// The discounted price with vat information cents.
		/// </value>
		[DataMember]
		public int DiscountedPriceWithVatInCents
		{
			get { return PricesIncludingVat ? DiscountedPriceInCents : VatCalculator.WithVat(DiscountedPriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the discounted price without vat information cents.
		/// </summary>
		/// <value>
		/// The discounted price without vat information cents.
		/// </value>
		[DataMember]
		public int DiscountedPriceWithoutVatInCents
		{
			get { return PricesIncludingVat ? VatCalculator.WithoutVat(DiscountedPriceInCents, Vat) : DiscountedPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price with vat information cents.
		/// </summary>
		/// <value>
		/// The price with vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithVatInCents
		{
			get { return PricesIncludingVat ? PriceInCents : VatCalculator.WithVat(PriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the price without vat information cents.
		/// </summary>
		/// <value>
		/// The price without vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithoutVatInCents
		{
			get { return PricesIncludingVat ? VatCalculator.WithoutVat(PriceInCents, Vat) : PriceInCents; }
			set { }
		}

		/// <summary>
		///     The price of the pricing without VAT
		/// </summary>
		[DataMember]
		public int OriginalPriceWithoutVatInCents
		{
			get { return PricesIncludingVat ? VatCalculator.WithoutVat(RangedPriceInCents, Vat) : RangedPriceInCents; }
			set { }
		}

		/// <summary>
		/// Gets or sets the original price with vat information cents.
		/// </summary>
		/// <value>
		/// The original price with vat information cents.
		/// </value>
		[DataMember]
		public int OriginalPriceWithVatInCents
		{
			get { return PricesIncludingVat ? RangedPriceInCents : VatCalculator.WithVat(RangedPriceInCents, Vat); }
			set { }
		}

		#endregion

		/// <summary>
		/// Determines whether the specified node type alias is alias.
		/// </summary>
		/// <param name="nodeTypeAlias">The node type alias.</param>
		/// <returns></returns>
		public static bool IsAlias(string nodeTypeAlias)
		{
			return nodeTypeAlias.StartsWith(NodeAlias) && !nodeTypeAlias.StartsWith(ProductVariant.NodeAlias);
		}

		internal void ClearCachedValues()
		{
			// temporary hack
			//foreach (var category in Categories)
			//{
			//	category.ClearCachedValues();
			//}

			_categories = null;
			_productVariantGroups = null;
		}

		/// <summary>
		/// Returns a <see cref="System.String" /> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String" /> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return "Product: " + Name;
		}
	}
}