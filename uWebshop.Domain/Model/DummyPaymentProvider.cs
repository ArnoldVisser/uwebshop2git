﻿using System;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Common;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	///     Class representing a payment provider in Umbraco
	/// </summary>
	public class DummyPaymentProvider : uWebshopEntity, IPaymentProvider
	{
		private List<PaymentProviderMethod> _paymentMethods;

		/// <summary>
		/// Gets the id of the node
		/// </summary>
		// todo: waarom?
		public new int Id
		{
			get { return Node.Id; }
		}

		/// <summary>
		///     Gets a list of payment methods
		/// </summary>
		public IEnumerable<PaymentProviderMethod> PaymentMethods
		{
			get
			{
				if (_paymentMethods == null)
				{
					LoadPaymentMethods();
				}

				return _paymentMethods;
			}
			private set { _paymentMethods = value.ToList(); }
		}

		/// <summary>
		///     Gets a list with the payment provider types
		/// </summary>
		public PaymentProviderType PaymentProviderType
		{
			get
			{
				if (Node.GetProperty("providerType").Value != null)
				{
					string preValue = Node.GetProperty("providerType").Value.Replace(" ", "_");

					return (PaymentProviderType) Enum.Parse(typeof (PaymentProviderType), preValue);
				}

				return PaymentProviderType.Unknown;
			}
		}

		/// <summary>
		///     Initializes a new instance of the uWebshop.Domain.DummyPaymentProvider class
		/// </summary>
		/// <param name="id">NodeId of the payment provider</param>
		public DummyPaymentProvider(int id) : base(id)
		{
		}

		private void LoadPaymentMethods()
		{
			var paymentProvider = PaymentProviderHelper.GetAllIPaymentProviders().FirstOrDefault(provider => provider.GetName() == Name);
			if (paymentProvider != null)
				_paymentMethods = paymentProvider.GetAllPaymentMethods(0).ToList();
		}

		/// <summary>
		/// Gets the payment provider title.
		/// </summary>
		/// <value>
		/// The payment provider title.
		/// </value>
		protected string PaymentProviderTitle
		{
			get { return Node.GetMultiStoreItem("title") != null ? Node.GetMultiStoreItem("title").Value : string.Empty; }
		}

		/// <summary>
		/// Gets the name.
		/// </summary>
		/// <returns></returns>
		public string GetName()
		{
			return PaymentProviderTitle;
		}

		/// <summary>
		/// Gets all payment methods.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			return Enumerable.Empty<PaymentProviderMethod>();
		}

		/// <summary>
		/// Gets all payment methods.
		/// </summary>
		/// <returns></returns>
		public List<PaymentProviderMethod> GetAllPaymentMethods()
		{
			return new List<PaymentProviderMethod>();
		}

		/// <summary>
		/// Gets the parameter render method.
		/// </summary>
		/// <returns></returns>
		public PaymentTransactionMethod GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.Custom;
		}
	}
}