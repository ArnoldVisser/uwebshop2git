﻿namespace uWebshop.Domain.ContentTypes
{
	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (UwebshopRootContentType), Name = "Discount Repository", Description = "#DiscountRepositoryDescription", Alias = "uwbsDiscountRepository", Icon = ContentIcon.MegaPhone, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (DiscountOrderSectionContentType), typeof (DiscountProductSectionContentType)})]
	public class DiscountRepositoryContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (DiscountRepositoryContentType), Name = "Discount Order Section", Description = "#DiscountOrderSectionDescription", Alias = DiscountOrder.SectionNodeAlias, Icon = ContentIcon.InboxDocument, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (DiscountOrder)})]
	public class DiscountOrderSectionContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (DiscountRepositoryContentType), Name = "Discount Product Section", Description = "#DiscountProductSectionDescription", Alias = DiscountProduct.SectionNodeAlias, Icon = ContentIcon.InboxDocument, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (DiscountProduct)})]
	public class DiscountProductSectionContentType
	{
	}
}