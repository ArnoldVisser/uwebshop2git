﻿namespace uWebshop.Domain.ContentTypes
{
	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (UwebshopRootContentType), Name = "Shipping Provider Repository", Description = "#ShippingProviderRepositoryDescription", Alias = ShippingProvider.ShippingProviderRepositoryNodeAlias, Icon = ContentIcon.TruckBoxLabel, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (ShippingProviderSectionContentType), typeof (ShippingProviderZoneSectionContentType)})]
	public class ShippingProviderRepositoryContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (ShippingProviderRepositoryContentType), Name = "Shipping Provider Section", Description = "#ShippingProviderSectionDescription", Alias = ShippingProvider.ShippingProviderSectionNodeAlias, Icon = ContentIcon.BaggageCart, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (ShippingProvider)})]
	public class ShippingProviderSectionContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (ShippingProviderRepositoryContentType), Name = "Shipping Provider Zone Section", Description = "#ShippingProviderZoneSectionDescription", Alias = ShippingProvider.ShippingProviderZoneSectionNodeAlias, Icon = ContentIcon.GlobeGreen, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (ShippingProviderZone)})]
	public class ShippingProviderZoneSectionContentType
	{
	}
}