﻿namespace uWebshop.Domain.ContentTypes
{
	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (UwebshopRootContentType), Name = "Payment Provider Repository", Description = "#PaymentProviderRepositoryDescription", Alias = PaymentProvider.PaymentProviderRepositoryNodeAlias, Icon = ContentIcon.Wallet, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (PaymentProviderSectionContentType), typeof (PaymentProviderZoneSectionContentType)})]
	public class PaymentProviderRepositoryContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (PaymentProviderRepositoryContentType), Name = "Payment Provider Section", Description = "#PaymentProviderSectionDescription", Alias = PaymentProvider.PaymentProviderSectionNodeAlias, Icon = ContentIcon.Bank, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (PaymentProvider)})]
	public class PaymentProviderSectionContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (PaymentProviderRepositoryContentType), Name = "Payment Provider Zone Section", Description = "#PaymentProviderZoneSectionDescription", Alias = PaymentProvider.PaymentProviderZoneSectionNodeAlias, Icon = ContentIcon.GlobeGreen, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (PaymentProviderZone)})]
	public class PaymentProviderZoneSectionContentType
	{
	}
}