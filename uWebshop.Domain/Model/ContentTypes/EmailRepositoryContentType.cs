﻿namespace uWebshop.Domain.ContentTypes
{
	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (UwebshopRootContentType), Name = "Email Repository", Description = "#EmailRepositoryDescription", Alias = Email.EmailRepositoryNodeAlias, Icon = ContentIcon.MailsStack, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (EmailStoreSectionContentType), typeof (EmailCustomerSectionContentType)})]
	public class EmailRepositoryContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (EmailRepositoryContentType), Name = "Email Template Store Section", Description = "#EmailTemplateStoreSectionDescription", Alias = Email.EmailTemplateStoreSectionNodeAlias, Icon = ContentIcon.MailAir, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (EmailStore)})]
	public class EmailStoreSectionContentType
	{
	}

	/// <summary>
	/// 
	/// </summary>
	[ContentType(ParentContentType = typeof (EmailRepositoryContentType), Name = "Email Template Customer Section", Description = "#EmailTemplateCustomerSectionDescription", Alias = Email.EmailTemplateCustomerSectionNodeAlias, Icon = ContentIcon.MailAir, Thumbnail = ContentThumbnail.Folder, AllowedChildTypes = new[] {typeof (EmailCustomer)})]
	public class EmailCustomerSectionContentType
	{
	}
}