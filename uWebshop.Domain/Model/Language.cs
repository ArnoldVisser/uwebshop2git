﻿using System.Globalization;
using System.Linq;
using SuperSimpleWebshop.Domain.BaseClasses;
using umbraco.presentation.nodeFactory;

namespace SuperSimpleWebshop.Domain
{
    /// <summary>
    /// Class representing a language in Umbraco
    /// </summary>
    public partial class Language : NodeBase
    {
        #region properties
        /// <summary>
        /// Gets the language code
        /// </summary>
        public string LanguageCode
        {
            get
            {
                try
                {
                    return Node.GetProperty("languageCode").Value;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Get the language alias
        /// </summary>
        public string LanguageAlias
        {
            get
            {
                try
                {
                    return Node.Name;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets the country code
        /// </summary>
        public string CountryCode
        {
            get
            {
                try
                {
                    return Node.GetProperty("countryCode").Value;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Gets the currency code
        /// </summary>
        public string CurrencyCode
        {
            get
            {
                try
                {
                    return Node.GetProperty("currencyCode").Value;
                }
                catch
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// /// Gets a System.Globalization.CultureInfo object that is set to the languagecode and countrycode
        /// </summary>
        public CultureInfo CultureInfo
        {
            get
            {
                var cultureInfo = new CultureInfo(string.Format("{0}-{1}", LanguageCode, CountryCode));

                var regionInfo = (from c in CultureInfo.GetCultures(CultureTypes.SpecificCultures)
                                  let r = new RegionInfo(c.LCID)
                                  where r.ISOCurrencySymbol == CurrencyCode
                                  select r).FirstOrDefault();

                NumberFormatInfo numberFormatInfo = (NumberFormatInfo)cultureInfo.NumberFormat.Clone();
                numberFormatInfo.CurrencySymbol = regionInfo.CurrencySymbol;

                cultureInfo.NumberFormat = numberFormatInfo;

                return cultureInfo;
            }
        }
        #endregion

        #region constructors
        /// <summary>
        /// Initializes a new instance of the SuperSimpleWebshop.Domain.Language class
        /// </summary>
        /// <param name="id">NodeId of the language</param>
        public Language(int id)
            : base(id)
        {

        }
        #endregion
    }
}
