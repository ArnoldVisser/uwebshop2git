﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using uWebshop.Common;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Namespace = "")]
	[Serializable]
	public class PaymentProviderMethod
	{
		/// <summary>
		/// Gets or sets the unique identifier.
		/// </summary>
		/// <value>
		/// The unique identifier.
		/// </value>
		[DataMember]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		[DataMember]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the description.
		/// </summary>
		/// <value>
		/// The description.
		/// </value>
		[DataMember]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the name of the provider.
		/// </summary>
		/// <value>
		/// The name of the provider.
		/// </value>
		[DataMember]
		public string ProviderName { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [disabled].
		/// </summary>
		/// <value>
		///   <c>true</c> if [disabled]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool Disabled { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether [test mode].
		/// </summary>
		/// <value>
		///   <c>true</c> if [test mode]; otherwise, <c>false</c>.
		/// </value>
		[DataMember]
		public bool TestMode { get; set; }

		/// <summary>
		/// Gets or sets the settings.
		/// </summary>
		/// <value>
		/// The settings.
		/// </value>
		[DataMember]
		public Dictionary<string, string> Settings { get; set; }

		/// <summary>
		/// Gets or sets the image unique identifier.
		/// </summary>
		/// <value>
		/// The image unique identifier.
		/// </value>
		[DataMember]
		public int ImageId { get; set; }

		/// <summary>
		/// Gets or sets the price information cents.
		/// </summary>
		/// <value>
		/// The price information cents.
		/// </value>
		[DataMember]
		public int PriceInCents { get; set; }

		/// <summary>
		/// Gets or sets the image.
		/// </summary>
		/// <value>
		/// The image.
		/// </value>
		[DataMember]
		public Image Image { get; set; }

		/// <summary>
		/// Gets or sets the vat.
		/// </summary>
		/// <value>
		/// The vat.
		/// </value>
		[DataMember]
		public decimal Vat { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[DataMember]
		public string Name { get; set; }

		/// <summary>
		/// Gets the type to calculate the payment amount for the order.
		/// </summary>
		/// <value>
		/// The type to calculate the payment amount for the order.
		/// </value>
		public PaymentProviderAmountType AmountType { get; set; }

		/// <summary>
		/// Gets or sets the price with vat.
		/// </summary>
		/// <value>
		/// The price with vat.
		/// </value>
		[DataMember]
		public decimal PriceWithVat
		{
			get { return PriceWithVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price with vat information cents.
		/// </summary>
		/// <value>
		/// The price with vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithVatInCents
		{
			get { return IO.Container.Resolve<ISettingsService>().IncludingVat ? PriceInCents : VatCalculator.WithVat(PriceInCents, Vat); }
			set { }
		}

		/// <summary>
		/// Gets or sets the price without vat.
		/// </summary>
		/// <value>
		/// The price without vat.
		/// </value>
		[DataMember]
		public decimal PriceWithoutVat
		{
			get { return PriceWithoutVatInCents/100m; }
			set { }
		}

		/// <summary>
		/// Gets or sets the price without vat information cents.
		/// </summary>
		/// <value>
		/// The price without vat information cents.
		/// </value>
		[DataMember]
		public int PriceWithoutVatInCents
		{
			get { return IO.Container.Resolve<ISettingsService>().IncludingVat ? VatCalculator.WithoutVat(PriceInCents, Vat) : PriceInCents; }
			set { }
		}
	}
}