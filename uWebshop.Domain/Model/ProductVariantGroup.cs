﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace uWebshop.Domain
{
	/// <summary>
	///     Product variants are grouped by a group title
	/// </summary>
	[DataContract(Namespace = "", IsReference = true)]
	public class ProductVariantGroup
	{
		/// <summary>
		///     Product variants group
		/// </summary>
		/// <param name="title"></param>
		/// <param name="productVariants"></param>
		public ProductVariantGroup(string title, List<ProductVariant> productVariants = null)
		{
			Title = title;
			ProductVariants = productVariants ?? new List<ProductVariant>();
		}

		/// <summary>
		///     Product Variants in this group
		/// </summary>
		public List<ProductVariant> ProductVariants { get; set; }

		/// <summary>
		/// Gets or sets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		public string Title { get; set; }

		/// <summary>
		/// Gets a value indicating whether the variant group is required.
		/// </summary>
		/// <value>
		///   <c>true</c> if the variant group is required; otherwise, <c>false</c>.
		/// </value>
		public bool Required
		{
			get
			{
				return ProductVariants.Any(pv => pv.Required);
			}
		}

	}
}