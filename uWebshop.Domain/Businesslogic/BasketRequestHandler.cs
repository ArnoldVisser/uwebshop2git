﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Web.SessionState;
using System.Xml;
using uWebshop.Common;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Businesslogic
{
	/// <summary>
	/// 
	/// </summary>
	internal class BasketRequestHandler
	{
		private OrderInfo OrderInfo { get; set; }

		/// <summary>
		/// Gets the session.
		/// </summary>
		/// <value>
		/// The session.
		/// </value>
		protected HttpSessionState Session
		{
			get { return HttpContext.Current.Session; }
		}

		/// <summary>
		/// Handleus the webshop basket request.
		/// </summary>
		/// <param name="requestParameters">The request parameters.</param>
		/// <returns></returns>
		public bool HandleuWebshopBasketRequest(NameValueCollection requestParameters)
		{
			#region remove session errors

			Session.Remove(Constants.CouponCodeSessionKey);
			Session.Remove(Constants.CouponCodeResultSessionKey);
			Session.Remove(Constants.CreateMemberSessionKey);
			Session.Remove(Constants.CreateMemberSessionKeyAddition);
			Session.Remove(Constants.ErrorMessagesSessionKey);
			Session.Remove(Constants.PaymentProviderSessionKey);
			Session.Remove(Constants.RequestPasswordSessionKey);
			Session.Remove(Constants.ShippingProviderSessionKey);
			Session.Remove(Constants.SignInMemberSessionKey);
			Session.Remove(Constants.SignOutMemberSessionKey);
			Session.Remove(Constants.UpdateMemberSessionKey);
			Session.Remove(Constants.UpdateMemberSessionKeyAddition);
			Session.Remove(Constants.OrderedItemcountHigherThanStockKey);

			#endregion

			bool redirectAfterHandle = false;

			List<string> shippingQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower().StartsWith("shipping")).ToList();

			if (shippingQueryStringCollection.Any())
			{
				AddCustomerInformation(requestParameters, shippingQueryStringCollection, CustomerDatatypes.Shipping);
				redirectAfterHandle = true;
			}

			List<string> customerQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower().StartsWith("customer")).ToList();

			if (customerQueryStringCollection.Any())
			{
				AddCustomerInformation(requestParameters, customerQueryStringCollection, CustomerDatatypes.Customer);
				redirectAfterHandle = true;
			}

			//var customerIsShipping = requestParameters.AllKeys.FirstOrDefault(x => x != null && x.ToLower() == "customerisshipping");

			//if (customerIsShipping != null && (shippingQueryStringCollection.Any() && string.IsNullOrEmpty(customerIsShipping)))
			//{
			//	AddCustomerInformation(requestParameters, shippingQueryStringCollection, CustomerDatatypes.Shipping);
			//	redirectAfterHandle = true;
			//}

			List<string> extraQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower().StartsWith("extra")).ToList();

			if (extraQueryStringCollection.Any())
			{
				AddCustomerInformation(requestParameters, extraQueryStringCollection, CustomerDatatypes.Extra);
				redirectAfterHandle = true;
			}

			List<string> accountQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower().StartsWith("createaccount")).ToList();

			if (accountQueryStringCollection.Any())
			{
				CreateAccount(requestParameters);
				redirectAfterHandle = true;
			}

			List<string> cleanOrderLinesCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "clearbasket").ToList();

			if (cleanOrderLinesCollection.Any())
			{
				ClearBasket(requestParameters);
				redirectAfterHandle = true;
			}

			List<string> productQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "productid").ToList();
			List<string> orderlineIdQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "orderlineid").ToList();

			if (productQueryStringCollection.Any() || orderlineIdQueryStringCollection.Any())
			{
				OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();
				AddProduct(requestParameters, OrderInfo);
				redirectAfterHandle = true;
			}

			List<string> orderlineDetailsQueryStringCollection = requestParameters.AllKeys.Where(x => x != null).ToList();

			string orderLineIdKey = requestParameters.AllKeys.FirstOrDefault(x => x != null && x.ToLower() == "orderlineid");
			if (orderLineIdKey != null)
			{
				string orderLineIdValue = requestParameters[orderLineIdKey];

				if (orderLineIdValue != null && (orderlineDetailsQueryStringCollection.Any() && !string.IsNullOrEmpty(orderLineIdValue)))
				{
					int orderlineId = 0;

					int.TryParse(orderLineIdValue, out orderlineId);

					if (orderlineId != 0)
					{
						OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

						OrderLine orderline = OrderInfo.OrderLines.FirstOrDefault(x => x.OrderLineId == orderlineId);

						if (orderline != null)
						{
							AddOrderLineDetails(requestParameters, orderlineDetailsQueryStringCollection, orderlineId);
						}
					}

					redirectAfterHandle = true;
				}
			}

			List<string> couponCodeQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "couponcode").ToList();

			if (couponCodeQueryStringCollection.Any())
			{
				AddCoupon(requestParameters);
				redirectAfterHandle = true;
			}

			List<string> shippingProviderQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "shippingprovider").ToList();

			if (shippingProviderQueryStringCollection.Any())
			{
				AddShippingMethod(requestParameters);
				redirectAfterHandle = true;
			}

			List<string> paymentProviderQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "paymentprovider").ToList();

			if (paymentProviderQueryStringCollection.Any())
			{
				AddPaymentMethod(requestParameters);
				redirectAfterHandle = true;
			}

			List<string> createAccountQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "createaccount").ToList();

			if (createAccountQueryStringCollection.Any())
			{
				CreateAccount(requestParameters);
			}

			List<string> updateAccountQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "updateaccount").ToList();

			if (updateAccountQueryStringCollection.Any())
			{
				UpdateAccount(requestParameters);
			}

			List<string> accountSignOutQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "accountsignout").ToList();

			if (accountSignOutQueryStringCollection.Any())
			{
				AccountSignOut(requestParameters);
				redirectAfterHandle = true;
			}

			List<string> accountSignInQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "accountsignin").ToList();

			if (accountSignInQueryStringCollection.Any())
			{
				AccountSignIn(requestParameters);
				//redirectAfterHandle = true;
			}

			List<string> accountRequestPaswordQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "requestpassword").ToList();

			if (accountRequestPaswordQueryStringCollection.Any())
			{
				RequestPassword(requestParameters);
			}

			List<string> validateQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "validate").ToList();

			if (validateQueryStringCollection.Any())
			{
				ValidateOrder(requestParameters);
			}

			List<string> validateCustomerQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "validatecustomer").ToList();

			if (validateCustomerQueryStringCollection.Any())
			{
				ValidateCustomer(requestParameters);
			}

			List<string> validateCustomQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "validatecustom").ToList();

			if (validateCustomQueryStringCollection.Any())
			{
				ValidateCustomValidation(requestParameters);
			}

			List<string> validateStockQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "validatestock").ToList();

			if (validateStockQueryStringCollection.Any())
			{
				ValidateStock(requestParameters);
			}

			List<string> validateOrderlinesQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "validateorderlines").ToList();

			if (validateOrderlinesQueryStringCollection.Any())
			{
				ValidateOrderlines(requestParameters);
			}

			List<string> confirmQueryStringCollection = requestParameters.AllKeys.Where(x => x != null && x.ToLower() == "confirm").ToList();

			if (confirmQueryStringCollection.Any())
			{
				ConfirmOrder(requestParameters);
			}
			return redirectAfterHandle;
		}

		private static void ValidateOrder(NameValueCollection requestParameters)
		{
			string validateOrderKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "validate"); //requestParameters["validate"];
			string validateOrderReffererKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "backtoreferreronerror");
			string validateOrder = requestParameters[validateOrderKey];
			string validateOrderRefferer = requestParameters[validateOrderReffererKey];

			if (validateOrder.ToLower() != "true" && validateOrder.ToLower() != "validate" && validateOrder.ToLower() != "on" && validateOrder != "1") return;

			OrderInfo order = OrderHelper.GetOrderInfo();

			if (order == null) return;

			if (OrderHelper.ValidateOrder(order))
			{
				// order is valid, ok!
				return;
			}
			order.Save(true);

			if (validateOrderRefferer != null && (validateOrderRefferer.ToLower() != "true" && validateOrderRefferer.ToLower() != "backtoreferreronerror" && validateOrderRefferer.ToLower() != "on" && validateOrderRefferer != "1")) return;

			if (HttpContext.Current.Request.UrlReferrer != null)
			{
				HttpContext.Current.Response.Redirect(string.Format("{0}#validation", HttpContext.Current.Request.UrlReferrer), true);
			}
		}

		private static void ValidateCustomer(NameValueCollection requestParameters)
		{
			string validateOrderKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "validatecustomer"); //requestParameters["validate"];
			string validateOrderReffererKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "backtoreferreronerror");
			string validateOrder = requestParameters[validateOrderKey];
			string validateOrderRefferer = requestParameters[validateOrderReffererKey];

			if (validateOrder.ToLower() != "true" && validateOrder.ToLower() != "validate" && validateOrder.ToLower() != "on" && validateOrder != "1") return;

			OrderInfo order = OrderHelper.GetOrderInfo();

			if (order == null) return;

			if (OrderHelper.ValidateCustomer(order, true))
			{
				// order is valid, ok!
				return;
			}
			order.Save(true, ValidateSaveAction.Customer);

			if (validateOrderRefferer != null && (validateOrderRefferer.ToLower() != "true" && validateOrderRefferer.ToLower() != "backtoreferreronerror" && validateOrderRefferer.ToLower() != "on" && validateOrderRefferer != "1")) return;

			if (HttpContext.Current.Request.UrlReferrer != null)
			{
				HttpContext.Current.Response.Redirect(string.Format("{0}#validation", HttpContext.Current.Request.UrlReferrer), true);
			}
		}

		private static void ValidateCustomValidation(NameValueCollection requestParameters)
		{
			string validateOrderKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "validatecustom"); //requestParameters["validate"];
			string validateOrderReffererKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "backtoreferreronerror");
			string validateOrder = requestParameters[validateOrderKey];
			string validateOrderRefferer = requestParameters[validateOrderReffererKey];

			if (validateOrder.ToLower() != "true" && validateOrder.ToLower() != "validate" && validateOrder.ToLower() != "on" && validateOrder != "1") return;

			OrderInfo order = OrderHelper.GetOrderInfo();

			if (order == null) return;

			if (OrderHelper.ValidateCustomValidation(order, true))
			{
				// order is valid, ok!
				return;
			}
			order.Save(true, ValidateSaveAction.CustomValidation);

			if (validateOrderRefferer != null && (validateOrderRefferer.ToLower() != "true" && validateOrderRefferer.ToLower() != "backtoreferreronerror" && validateOrderRefferer.ToLower() != "on" && validateOrderRefferer != "1")) return;

			if (HttpContext.Current.Request.UrlReferrer != null)
			{
				HttpContext.Current.Response.Redirect(string.Format("{0}#validation", HttpContext.Current.Request.UrlReferrer), true);
			}
		}

		private static void ValidateStock(NameValueCollection requestParameters)
		{
			string validateOrderKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "validatestock"); //requestParameters["validate"];
			string validateOrderReffererKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "backtoreferreronerror");
			string validateOrder = requestParameters[validateOrderKey];
			string validateOrderRefferer = requestParameters[validateOrderReffererKey];

			if (validateOrder.ToLower() != "true" && validateOrder.ToLower() != "validate" && validateOrder.ToLower() != "on" && validateOrder != "1") return;

			OrderInfo order = OrderHelper.GetOrderInfo();

			if (order == null) return;

			if (OrderHelper.ValidateStock(order, true))
			{
				// order is valid, ok!
				return;
			}
			order.Save(true, ValidateSaveAction.Stock);

			if (validateOrderRefferer != null && (validateOrderRefferer.ToLower() != "true" && validateOrderRefferer.ToLower() != "backtoreferreronerror" && validateOrderRefferer.ToLower() != "on" && validateOrderRefferer != "1")) return;

			if (HttpContext.Current.Request.UrlReferrer != null)
			{
				HttpContext.Current.Response.Redirect(string.Format("{0}#validation", HttpContext.Current.Request.UrlReferrer), true);
			}
		}

		private static void ValidateOrderlines(NameValueCollection requestParameters)
		{
			string validateOrderlinesKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "validateorderlines"); //requestParameters["validate"];
			string validateOrderReffererKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "backtoreferreronerror");
			string validateOrderlines = requestParameters[validateOrderlinesKey];
			string validateOrderRefferer = requestParameters[validateOrderReffererKey];

			if (validateOrderlines.ToLower() != "true" && validateOrderlines.ToLower() != "validate" && validateOrderlines.ToLower() != "on" && validateOrderlines != "1") return;

			OrderInfo order = OrderHelper.GetOrderInfo();

			if (order == null) return;

			if (OrderHelper.ValidateOrderLines(order, true))
			{
				// order is valid, ok!
				return;
			}
			order.Save(true, ValidateSaveAction.Orderlines);
		}

		private static void ClearBasket(NameValueCollection requestParameters)
		{
			string clearbasketKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "clearbasket");
			string clearbasketValue = requestParameters[clearbasketKey];
			if (clearbasketValue != null && (clearbasketValue.ToLower() != "true" && clearbasketValue.ToLower() != "clearbasket" && clearbasketValue.ToLower() != "on" && clearbasketValue != "1")) return;

			OrderInfo order = OrderHelper.GetOrderInfo();

			if (order == null) return;
			order.OrderLines.Clear();

			order.Save();
		}

		private void AccountSignOut(NameValueCollection requestParameters)
		{
			string accountSignOutKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "accountsignout"); //requestParameters["accountSignOut"];
			string accountSignOutValue = requestParameters[accountSignOutKey];
			if (accountSignOutValue != null && (accountSignOutValue.ToLower() != "true" && accountSignOutValue.ToLower() != "accountsignout" && accountSignOutValue.ToLower() != "on" && accountSignOutValue != "1")) return;

			FormsAuthentication.SignOut();

			Session.Add(Constants.SignOutMemberSessionKey, AccountActionResult.Success);
			Session.Remove(Constants.SignInMemberSessionKey);
		}

		private void AccountSignIn(NameValueCollection requestParameters)
		{
			string accountSignInKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "accountsignin") ?? string.Empty;
			string accountSignInValue = requestParameters[accountSignInKey];
			if (accountSignInValue != null && (accountSignInValue.ToLower() != "true" && accountSignInValue.ToLower() != "accountsignin" && accountSignInValue != "on" && accountSignInValue != "1")) return;

			string userNameKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "customeremail") ?? string.Empty;
			string passwordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "password") ?? string.Empty;

			string userNameValue = requestParameters[userNameKey];
			string passwordValue = requestParameters[passwordKey];

			MembershipUser user = Membership.GetUser(userNameValue);


			if (user == null)
			{
				Session.Add(Constants.SignInMemberSessionKey, AccountActionResult.MemberNotExists);
				return;
			}

			if (!Membership.ValidateUser(userNameValue, passwordValue))
			{
				Session.Add(Constants.SignInMemberSessionKey, AccountActionResult.ValidateUserError);
				return;
			}

			FormsAuthentication.SetAuthCookie(userNameValue, true);

			Session.Add(Constants.SignInMemberSessionKey, AccountActionResult.Success);
			Session.Remove(Constants.SignOutMemberSessionKey);
		}

		private void RequestPassword(NameValueCollection requestParameters)
		{
			string requestPasswordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "requestpassword"); //requestParameters["requestPassword"];
			string requestPasswordValue = requestParameters[requestPasswordKey];

			if (requestPasswordValue != null && (requestPasswordValue.ToLower() != "true" && requestPasswordValue.ToLower() != "requestpassword" && requestPasswordValue.ToLower() != "on" && requestPasswordValue != "1")) return;

			string userNameKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "customeremail"); //requestParameters["customerEmail"];
			string userNameValue = requestParameters[userNameKey];

			if (string.IsNullOrEmpty(userNameValue))
			{
				Session.Add(Constants.RequestPasswordSessionKey, AccountActionResult.CustomerEmailEmpty);
				return;
			}

			Store currentStore = StoreHelper.GetCurrentStore();

			if (string.IsNullOrEmpty(currentStore.AccountForgotPasswordEmail))
			{
				Session.Add(Constants.RequestPasswordSessionKey, AccountActionResult.AccountForgotPasswordEmailNotConfigured);
				return;
			}
			int emailNodeId = Convert.ToInt32(currentStore.AccountForgotPasswordEmail);

			MembershipUser user = Membership.GetUser(userNameValue);


			if (user == null)
			{
				Session.Add(Constants.RequestPasswordSessionKey, AccountActionResult.MemberNotExists);
				return;
			}


			if (!Membership.EnablePasswordReset)
			{
				Session.Add(Constants.RequestPasswordSessionKey, AccountActionResult.EnablePasswordResetDisabled);
				return;
			}

			string resetPassword = user.ResetPassword();

			string newPassword = Membership.GeneratePassword(Membership.MinRequiredPasswordLength, Membership.MinRequiredNonAlphanumericCharacters);

			if (user.ChangePassword(resetPassword, newPassword))
			{
				EmailHelper.SendMemberEmailCustomer(emailNodeId, currentStore, userNameValue, newPassword);
				Session.Add(Constants.RequestPasswordSessionKey, AccountActionResult.Success);
			}
			else
			{
				Session.Add(Constants.RequestPasswordSessionKey, AccountActionResult.ChangePasswordError);
			}
		}

		private void UpdateAccount(NameValueCollection requestParameters)
		{
			string updateAccountKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "updateaccount"); //requestParameters["updateAccount"];
			string updateAccountValue = requestParameters[updateAccountKey];
			if (updateAccountValue != null && (updateAccountValue.ToLower() != "true" && updateAccountValue.ToLower() != "updateaccount" && updateAccountValue.ToLower() != "on" && updateAccountValue != "1")) return;

			MembershipUser memberShipUser = Membership.GetUser();

			if (memberShipUser == null)
			{
				Log.Instance.LogDebug("UpdateAccount: " + AccountActionResult.MemberNotExists);
				Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.MemberNotExists);
				return;
			}

			ProfileBase profile = ProfileBase.Create(memberShipUser.UserName);

			// note don't forget to disable automatic profile save!: <profile defaultProvider="UmbracoMemberProfileProvider" enabled="true" automaticSaveEnabled="false">
			foreach (object prop in ProfileBase.Properties)
			{
				try
				{
					var settingsProperty = (SettingsProperty) prop;

					string settingsPropertyName = settingsProperty.Name;

					if (!string.IsNullOrEmpty(requestParameters[settingsPropertyName]))
					{
						profile[settingsPropertyName] = requestParameters[settingsPropertyName];
					}
				}
				catch
				{
					var settingsProperty = (SettingsProperty) prop;

					Log.Instance.LogDebug(string.Format("UpdateAccount Failed for Request Property: {0}, currentNodeId: {1}", settingsProperty.Name, IO.Container.Resolve<ICMSApplication>().CurrentNodeId()));
				}
			}


			string currentPasswordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "currentpassword"); //requestParameters["currentPassword"];
			string newPasswordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "newpassword"); //requestParameters["newPassword"];
			string validatePasswordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "validatenewpassword"); //requestParameters["validateNewPassword"];
			string generatePasswordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "generatepassword"); //requestParameters["generatePassword"];

			string currentPasswordValue = requestParameters[currentPasswordKey];
			string newPasswordValue = requestParameters[newPasswordKey];
			string validatePasswordValue = requestParameters[validatePasswordKey];
			string generatePasswordValue = requestParameters[generatePasswordKey];


			if (!string.IsNullOrEmpty(currentPasswordValue) || !string.IsNullOrEmpty(newPasswordValue) || !string.IsNullOrEmpty(validatePasswordValue) || !string.IsNullOrEmpty(generatePasswordValue))
			{
				Log.Instance.LogDebug("Update Account Password Section");
				if (!string.IsNullOrEmpty(generatePasswordValue))
				{
					newPasswordValue = Membership.GeneratePassword(Membership.MinRequiredPasswordLength, Membership.MinRequiredNonAlphanumericCharacters);
				}
				else
				{
					if (newPasswordValue != validatePasswordValue)
					{
						Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.PasswordMismatch);
						return;
					}

					if (string.IsNullOrEmpty(newPasswordValue))
					{
						Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.Failed);
						return;
					}

					if (Membership.MinRequiredPasswordLength > 0)
					{
						if (newPasswordValue.Length < Membership.MinRequiredPasswordLength)
						{
							Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.MinRequiredPasswordLengthError);
							Session.Add(Constants.UpdateMemberSessionKeyAddition, Membership.MinRequiredPasswordLength);
							return;
						}
					}

					if (!string.IsNullOrEmpty(Membership.PasswordStrengthRegularExpression))
					{
						if (!Regex.IsMatch(newPasswordValue, Membership.PasswordStrengthRegularExpression))
						{
							Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.PasswordStrengthRegularExpressionError);
							Session.Add(Constants.UpdateMemberSessionKeyAddition, Membership.PasswordStrengthRegularExpression);
							return;
						}
					}

					if (Membership.MinRequiredNonAlphanumericCharacters > 0)
					{
						int num = newPasswordValue.Where((t, i) => !char.IsLetterOrDigit(newPasswordValue, i)).Count();

						if (num < Membership.MinRequiredNonAlphanumericCharacters)
						{
							Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.MinRequiredNonAlphanumericCharactersError);
							Session.Add(Constants.UpdateMemberSessionKeyAddition, Membership.MinRequiredNonAlphanumericCharacters);
							return;
						}
					}
				}

				if (currentPasswordValue != null)
				{
					memberShipUser.ChangePassword(currentPasswordValue, newPasswordValue);
				}
				else
				{
					Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.ChangePasswordError);
					return;
				}
			}
			profile.Save();

			Session.Add(Constants.UpdateMemberSessionKey, AccountActionResult.Success);
		}

		private void CreateAccount(NameValueCollection requestParameters)
		{
			string createAccountKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "createaccount");
			//requestParameters["createAccount"];
			string createAccountValue = requestParameters[createAccountKey];
			if (createAccountValue != null && (createAccountValue.ToLower() != "true" && createAccountValue.ToLower() != "createaccount" && createAccountValue.ToLower() != "on" && createAccountValue.ToLower() != "1"))
			{
				Log.Instance.LogError("createAccount != true && createAccount != createAccount: " + createAccountValue);
				return;
			}

			string emailKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "customeremail");
			//requestParameters["customerEmail"];
			string passwordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "password");
			//requestParameters["password"];
			string validatePasswordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "validatepassword");
			//requestParameters["validatePassword"];
			string generatePasswordKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "generatepassword");
			//requestParameters["generatePassword"];

			string emailValue = requestParameters[emailKey];

			if (!Regex.IsMatch(emailValue, "\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", RegexOptions.IgnoreCase))
			{
				Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.EmailAddressNotValid);
				return;
			}

			string passwordValue = requestParameters[passwordKey];
			string validatePasswordValue = requestParameters[validatePasswordKey];
			string generatePasswordValue = requestParameters[generatePasswordKey];

			if (string.IsNullOrEmpty(generatePasswordValue))
			{
				if (passwordValue == validatePasswordValue)
				{
					if (string.IsNullOrEmpty(passwordValue))
					{
						Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.Failed);
						return;
					}
					if (Membership.MinRequiredPasswordLength > 0)
					{
						if (passwordValue.Length < Membership.MinRequiredPasswordLength)
						{
							Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.MinRequiredPasswordLengthError);
							Session.Add(Constants.CreateMemberSessionKeyAddition, Membership.MinRequiredPasswordLength);
							Log.Instance.LogError("MinRequiredPasswordLengthError");
							return;
						}
					}

					if (!string.IsNullOrEmpty(Membership.PasswordStrengthRegularExpression))
					{
						if (!Regex.IsMatch(passwordValue, Membership.PasswordStrengthRegularExpression))
						{
							Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.PasswordStrengthRegularExpressionError);
							Session.Add(Constants.CreateMemberSessionKeyAddition, Membership.PasswordStrengthRegularExpression);
							Log.Instance.LogError("PasswordStrengthRegularExpression");
							return;
						}
					}

					if (Membership.MinRequiredNonAlphanumericCharacters > 0)
					{
						int num = passwordValue.Where((t, i) => !char.IsLetterOrDigit(passwordValue, i)).Count();

						if (num < Membership.MinRequiredNonAlphanumericCharacters)
						{
							Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.MinRequiredNonAlphanumericCharactersError);
							Session.Add(Constants.CreateMemberSessionKeyAddition, Membership.MinRequiredNonAlphanumericCharacters);
							Log.Instance.LogError("MinRequiredNonAlphanumericCharacters");
							return;
						}
					}
				}
				else
				{
					Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.PasswordMismatch);
					Log.Instance.LogError("PasswordMismatch");
					return;
				}
			}
			else
			{
				passwordValue = Membership.GeneratePassword(Membership.MinRequiredPasswordLength, Membership.MinRequiredNonAlphanumericCharacters);
			}

			var webConfig = new XmlDocument();
			webConfig.Load(HttpContext.Current.Server.MapPath("~/web.config"));

			XmlNode umbracoDefaultMemberTypeAlias = webConfig.SelectSingleNode("//add[@defaultMemberTypeAlias]");

			var memberTypes = IO.Container.Resolve<ICMSApplication>().GetAllMemberTypes();

			if (umbracoDefaultMemberTypeAlias == null || umbracoDefaultMemberTypeAlias.Attributes == null)
			{
				Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.DefaultMemberTypeAliasError);
				Log.Instance.LogError("DefaultMemberTypeAliasError");
				return;
			}
			string memberTypevalue = umbracoDefaultMemberTypeAlias.Attributes["defaultMemberTypeAlias"].Value;

			if (memberTypes.All(x => x.Alias != memberTypevalue))
			{
				Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.DefaultMemberTypeAliasNonExistingError);
				Log.Instance.LogError("DefaultMemberTypeAliasNonExistingError");
				return;
			}
			if (string.IsNullOrEmpty(emailValue))
			{
				Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.CustomerEmailEmpty);
				Log.Instance.LogError("CustomerEmailEmpty");
				return;
			}

			string member = Membership.GetUserNameByEmail(emailValue);

			if (member != null)
			{
				Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.MemberExists);
				Log.Instance.LogError("MemberExists");
				return;
			}

			MembershipUser membershipUser = Membership.CreateUser(emailValue, passwordValue, emailValue);

			string roleKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "membergroup");
			string roleValue = requestParameters[roleKey];

			if (!string.IsNullOrEmpty(roleValue))
			{
				if (Roles.RoleExists(roleValue))
				{
					Roles.AddUserToRole(membershipUser.UserName, roleValue);
				}
				else
				{
					Roles.CreateRole(roleValue);

					Roles.AddUserToRole(membershipUser.UserName, roleValue);
				}
			}
			else
			{
				if (!Roles.GetAllRoles().Any())
				{
					const string customersRole = "Customers";

					Roles.CreateRole(customersRole);

					Roles.AddUserToRole(membershipUser.UserName, customersRole);
				}
				else
				{
					Roles.AddUserToRole(membershipUser.UserName, Roles.GetAllRoles().First());
				}
			}

			ProfileBase profile = ProfileBase.Create(emailValue);

			foreach (object prop in ProfileBase.Properties)
			{
				try
				{
					var settingsProperty = (SettingsProperty) prop;

					string settingsPropertyName = settingsProperty.Name;

					if (!string.IsNullOrEmpty(requestParameters[settingsPropertyName]))
					{
						profile[settingsPropertyName] = requestParameters[settingsPropertyName];
					}
				}
				catch
				{
					var settingsProperty = (SettingsProperty) prop;

					Log.Instance.LogDebug(string.Format("UpdateAccount Failed for Request Property: {0}, currentNodeId: {1}", settingsProperty.Name, IO.Container.Resolve<ICMSApplication>().CurrentNodeId()));
				}
			}

			profile.Save();

			FormsAuthentication.SetAuthCookie(emailValue, true);

			Session.Add(Constants.CreateMemberSessionKey, AccountActionResult.Success);

			Store currentStore = StoreHelper.GetCurrentStore();

			if (string.IsNullOrEmpty(currentStore.AccountCreatedEmail))
			{
				Log.Instance.LogDebug("CreateAccount: AccountCreatedEmail not set: No email send to customer");
				return;
			}

			int emailNodeId = Convert.ToInt32(currentStore.AccountCreatedEmail);
			EmailHelper.SendMemberEmailCustomer(emailNodeId, currentStore, emailValue, passwordValue);
		}

		private void ConfirmOrder(NameValueCollection requestParameters)
		{
			OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			string orderStatusKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "confirm"); //requestParameters["confirm"];
			string acceptTermsKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "acceptterms"); //requestParameters["acceptterms"];
			string orderStatusValue = requestParameters[orderStatusKey];
			string acceptTermsValue = requestParameters[acceptTermsKey];

			if (string.IsNullOrEmpty(orderStatusValue)) return;

			bool termsAccepted = acceptTermsValue != null && (acceptTermsValue.ToLower() == "true" || acceptTermsValue.ToLower() == "acceptterms" || acceptTermsValue.ToLower() == "on" || acceptTermsValue == "1");


			int confirmOrderNodeId = 0;

			Log.Instance.LogDebug("BasketHandler ConfirmOrder confirm: " + orderStatusValue);

			string confirmOrderNodeRequestKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "confirmordernodeid"); //requestParameters["confirmOrderNodeId"];
			string confirmOrderNodeRequestValue = requestParameters[confirmOrderNodeRequestKey];

			if (!string.IsNullOrEmpty(confirmOrderNodeRequestValue))
			{
				int.TryParse(confirmOrderNodeRequestValue, out confirmOrderNodeId);
			}

			if (!OrderInfo.ConfirmOrder(termsAccepted, confirmOrderNodeId))
			{
				return;
			}

			if (OrderInfo.PaymentInfo.TransactionMethod == PaymentTransactionMethod.WebClient)
			{
				HttpContext.Current.Response.Write(Encoding.UTF8.GetString(GetBytes(OrderInfo.PaymentInfo.Parameters)));
			}
			else
			{
				if (!string.IsNullOrEmpty(OrderInfo.RedirectUrl))
				{
					HttpContext.Current.Response.Redirect(OrderInfo.RedirectUrl, true);
				}
			}
		}

		internal static byte[] GetBytes(string str)
		{
			var bytes = new byte[str.Length*sizeof (char)];
			Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			return bytes;
		}

		private void AddCoupon(NameValueCollection requestParameters)
		{
			OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();
			string couponCodeKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "couponcode"); //requestParameters["couponCode"];
			string removeCouponKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "removecoupon"); //requestParameters["removeCoupon"];

			string couponCodeValue = requestParameters[couponCodeKey];
			string removeCouponValue = requestParameters[removeCouponKey];

			CouponCodeResult result;
			bool saveOrder = true;
			if (!string.IsNullOrEmpty(removeCouponValue))
			{
				if (removeCouponValue.ToLower() == "true" || removeCouponValue.ToLower() == "removecoupon" && removeCouponValue.ToLower() != "on" && removeCouponValue != "1")
				{
					result = OrderInfo.RemoveCoupon(couponCodeValue);
					if (result != CouponCodeResult.Succes)
					{
						saveOrder = false;
					}
				}
				else
				{
					result = CouponCodeResult.Failed;
					saveOrder = false;
				}
			}
			else
			{
				result = OrderInfo.AddCoupon(couponCodeValue);
				if (result != CouponCodeResult.Succes)
				{
					saveOrder = false;
				}
			}

			if (saveOrder)
			{
				OrderInfo.Save();
			}

			Session.Add(Constants.CouponCodeSessionKey, result);
		}

		private void AddOrderLineDetails(NameValueCollection requestParameters, IEnumerable<string> queryStringCollection, int orderlineId)
		{
			OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			Dictionary<string, string> fields = queryStringCollection.ToDictionary(s => s, s => requestParameters[s]);

			if (!fields.Any()) return;
			OrderInfo.AddOrderLineDetails(fields, orderlineId);

			OrderInfo.Save();
		}

		private void AddCustomerInformation(NameValueCollection requestParameters, IEnumerable<string> queryStringCollection, CustomerDatatypes customerDataType)
		{
			OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			Dictionary<string, string> fields = queryStringCollection.ToDictionary(s => s, s => requestParameters[s]);

			if (!fields.Any()) return;
			IO.Container.Resolve<IOrderUpdatingService>().AddCustomerFields(OrderInfo, fields, customerDataType);

			string customerIsShippingKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "customerisshipping"); //requestParameters["customerIsShipping"];

			if (customerIsShippingKey != null)
			{
				string customerIsShippingValue = requestParameters[customerIsShippingKey];

				if (customerIsShippingValue != null && (customerIsShippingValue.ToLower() == "customerisshipping" || customerIsShippingValue.ToLower() == "true" || customerIsShippingValue.ToLower() == "on" || customerIsShippingValue == "1"))
				{
					Dictionary<string, string> shippingFields = queryStringCollection.ToDictionary(s => s.Replace("customer", "shipping"), s => requestParameters[s]);

					if (!shippingFields.Any()) return;
					OrderInfo.AddCustomerFields(shippingFields, CustomerDatatypes.Shipping);
				}
			}

			OrderInfo.Save();
		}

		private void AddShippingMethod(NameValueCollection requestParameters)
		{
			OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			string shippingProviderKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "shippingprovider"); //requestParameters["shippingProvider"];

			string shippingProviderValue = requestParameters[shippingProviderKey];

			if (shippingProviderValue != null && (shippingProviderValue.ToLower() == "indication" || shippingProviderValue.ToLower() == "auto"))
			{
				ShippingProviderHelper.AutoSelectShippingProvider(OrderInfo);

				OrderInfo.Save();
			}
			else
			{
				if (shippingProviderValue != null)
				{
					if (!shippingProviderValue.Contains("-"))
					{
						Log.Instance.LogDebug("AddShippingMethod: " + ProviderActionResult.NoCorrectInput + " value: " + shippingProviderValue);
						Session.Add(Constants.ShippingProviderSessionKey, ProviderActionResult.NoCorrectInput);
						return;
					}

					int shippingProviderId = 0;
					int.TryParse(shippingProviderValue.Split('-')[0], out shippingProviderId);
					string shippingProviderMethodId = shippingProviderValue.Split('-')[1];

					if (shippingProviderId == 0)
					{
						Log.Instance.LogDebug("AddShippingMethod: " + ProviderActionResult.ProviderIdZero + " value: " + shippingProviderValue);
						Session.Add(Constants.ShippingProviderSessionKey, ProviderActionResult.ProviderIdZero);
						return;
					}

					ProviderActionResult result = OrderInfo.AddShippingProvider(shippingProviderId, shippingProviderMethodId);

					OrderInfo.Save();

					Session.Add(Constants.ShippingProviderSessionKey, result);
				}
				else
				{
					Log.Instance.LogDebug("AddShippingMethod: " + ProviderActionResult.NoCorrectInput + " value: null");
					Session.Add(Constants.PaymentProviderSessionKey, ProviderActionResult.NoCorrectInput);
				}
			}
		}

		private void AddPaymentMethod(NameValueCollection requestParameters)
		{
			OrderInfo = OrderHelper.GetOrderInfo() ?? OrderHelper.CreateOrder();

			string paymentProviderKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "paymentprovider"); //requestParameters["paymentProvider"];

			string paymentProviderValue = requestParameters[paymentProviderKey];

			if (paymentProviderValue == null)
			{
				Log.Instance.LogError("BasketHandler AddPaymentMethod paymentProviderValue == null");
				return;
			}

			if (!paymentProviderValue.Contains("-"))
			{
				PaymentProvider paymentProviderNode = IO.Container.Resolve<IPaymentProviderService>().GetPaymentProviderWithName(paymentProviderValue, StoreHelper.CurrentStoreAlias);

				if (paymentProviderNode != null)
				{
					new PaymentRequestHandler().HandleuWebshopPaymentRequest(paymentProviderNode.Id);
					return;
				}

				Session.Add(Constants.PaymentProviderSessionKey, ProviderActionResult.NoCorrectInput);
				Log.Instance.LogError("BasketHandler AddPaymentMethod !paymentProviderValue.Contains(-) && paymentProviderNode != null");
				return;
			}

			if (paymentProviderValue.Split('-').Length <= 0)
			{
				Session.Add(Constants.PaymentProviderSessionKey, ProviderActionResult.NoCorrectInput);
				Log.Instance.LogError("BasketHandler AddPaymentMethod paymentProviderValue.Split('-').Length <= 0");
				return;
			}

			int paymentProviderId;
			int.TryParse(paymentProviderValue.Split('-')[0], out paymentProviderId);
			string paymentProviderMethodId = paymentProviderValue.Split('-')[1];

			if (paymentProviderId == 0)
			{
				Session.Add(Constants.PaymentProviderSessionKey, ProviderActionResult.ProviderIdZero);
				Log.Instance.LogError("BasketHandler AddPaymentMethod ProviderActionResult.ProviderIdZero");
				return;
			}
			OrderInfo.AddPaymentProvider(paymentProviderId, paymentProviderMethodId);

			OrderInfo.Save();

			Session.Add(Constants.PaymentProviderSessionKey, ProviderActionResult.Success);
		}

		internal void AddProduct(NameValueCollection requestParameters, OrderInfo order)
		{
			var orderUpdater = IO.Container.Resolve<IOrderUpdatingService>();

			string productIdKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "productid");
			string orderLineIdKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "orderlineid");

			string productIdValue = requestParameters[productIdKey];
			string orderlineIdValue = requestParameters[orderLineIdKey];

			int productId = 0;
			int orderLineId = 0;

			int.TryParse(productIdValue, out productId);
			int.TryParse(orderlineIdValue, out orderLineId);

			if (productId <= 0 && orderLineId <= 0)
			{
				Log.Instance.LogError("ADD TO BASKET ERROR: productId <= 0 && orderLineId <= 0");
				return;
			}

			string actionKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "action");
			string actionValue = requestParameters[actionKey];
			string action = !string.IsNullOrEmpty(actionValue) ? actionValue : string.Empty;

			string quantityKey = requestParameters.AllKeys.FirstOrDefault(x => x.ToLower() == "quantity");
			string quantityValue = requestParameters[quantityKey];

			int quantity;
			int.TryParse(quantityValue, out quantity);

			List<int> variants = requestParameters.Keys.Cast<string>().Where(key => key.ToLower().StartsWith("variant")).Select(key => Common.Helpers.ParseInt(requestParameters[key])).ToList();

			List<string> orderlineDetailsQueryStringCollection = requestParameters.AllKeys.Where(x => x != null).ToList();

			Dictionary<string, string> fields = orderlineDetailsQueryStringCollection.Where(s => !string.IsNullOrEmpty(requestParameters[s])).ToDictionary(s => s, s => requestParameters[s]);

			if (order != null)
				Log.Instance.LogDebug("ADD TO BASKET DEBUG order: " + order.UniqueOrderId + " orderLineId: " + orderLineId + " productId: " + productId + " action: " + action + " quantity: " + quantity + " variantsCount: " + variants.Count() + " fieldsCount: " + fields.Count());

			orderUpdater.AddOrUpdateOrderLine(order, orderLineId, productId, action, quantity, variants, fields); // unit test dat dit aangeroepen wordt
			orderUpdater.Save(order);
		}
	}
}