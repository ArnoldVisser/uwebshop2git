﻿using System;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	internal class PaymentRequestHandler
	{
		/// <summary>
		/// Handles the uwebshop payment request.
		/// </summary>
		/// <param name="paymentProviderNodeId">The payment provider node unique identifier.</param>
		/// <exception cref="System.Exception">HandleuWebshopPaymentRequest paymentProvider.Name == null
		/// or
		/// HandleuWebshopPaymentRequest responsehandler == null:  + paymentProvider.Name</exception>
		public void HandleuWebshopPaymentRequest(int paymentProviderNodeId)
		{
			PaymentProvider paymentProvider = PaymentProvider.GetPaymentProvider(paymentProviderNodeId);

			if (paymentProvider.Name == null)
			{
				throw new Exception("HandleuWebshopPaymentRequest paymentProvider.Name == null");
			}

			IPaymentResponseHandler responsehandler = PaymentProviderHelper.GetAllPaymentResponseHandlers()
				.FirstOrDefault(paymentResponseHandler => paymentResponseHandler.GetName().ToLower() == paymentProvider.Name.ToLower());

			if (responsehandler == null)
			{
				throw new Exception("HandleuWebshopPaymentRequest responsehandler == null: " + paymentProvider.Name);
			}

			responsehandler.HandlePaymentResponse();
		}
	}
}