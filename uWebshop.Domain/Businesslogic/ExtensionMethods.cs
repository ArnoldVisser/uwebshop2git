﻿using System;
using System.Text;
using System.Web;
using uWebshop.Common;

// todo: think about namespace
namespace uWebshop.Domain.Businesslogic
{
	// todo: remove umbraco stuff
	/// <summary>
	/// 
	/// </summary>
	public static class ExtensionMethods
	{
		/// <summary>
		///   <para>Creates a log-string from the Exception.</para>
		///   <para>
		/// The result includes the stacktrace, innerexception et cetera, separated by
		///   <seealso cref="Environment.NewLine" />.
		///   </para>
		/// </summary>
		/// <param name="ex">The exception to create the string from.</param>
		/// <param name="printTrace">if set to <c>true</c> [print trace].</param>
		/// <param name="additionalMessage">Additional message to place at the top of the string, maybe be empty or null.</param>
		/// <returns></returns>
		public static string ToLogString(this Exception ex, bool printTrace = true, string additionalMessage = "")
		{
			var msg = new StringBuilder();

			if (!string.IsNullOrEmpty(additionalMessage))
			{
				msg.Append(additionalMessage);
				msg.Append(Environment.NewLine);
			}

			if (ex == null) return msg.ToString();

			AddExceptionMessagesRecursive(ex, msg);

			if (!HttpContext.Current.IsDebuggingEnabled) return msg.ToString();

			foreach (var i in ex.Data)
			{
				msg.Append("Data :");
				msg.Append(i);
				msg.Append(Environment.NewLine);
			}

			if (printTrace && ex.StackTrace != null)
			{
				msg.Append("StackTrace:");
				msg.Append(Environment.NewLine);
				msg.Append(ex.StackTrace);
				msg.Append(Environment.NewLine);
			}

			if (ex.InnerException != null)
			{
				msg.Append("Inner:");
				msg.Append(Environment.NewLine);
				ToLogString(ex.InnerException, msg);
			}
			return msg.ToString();
		}

		private static void AddExceptionMessagesRecursive(Exception ex, StringBuilder msg)
		{
			Exception orgEx = ex;

			msg.Append("Ex: ");
			msg.Append(Environment.NewLine);
			while (orgEx != null)
			{
				msg.Append(orgEx.Message);
				msg.Append(Environment.NewLine);
				orgEx = orgEx.InnerException;
			}
		}

		/// <summary>
		/// Creates the log string.
		/// </summary>
		/// <param name="ex">The executable.</param>
		/// <param name="msg">The MSG.</param>
		public static void ToLogString(this Exception ex, StringBuilder msg)
		{
			AddExceptionMessagesRecursive(ex, msg);

			if (!HttpContext.Current.IsDebuggingEnabled) return;

			foreach (var i in ex.Data)
			{
				msg.Append("Data :");
				msg.Append(i);
				msg.Append(Environment.NewLine);
			}

			if (ex.InnerException != null)
			{
				msg.Append("Inner:");
				msg.Append(Environment.NewLine);
				ToLogString(ex.InnerException, msg);
			}
		}

		/// <summary>
		/// Gets the adjusted price.
		/// </summary>
		/// <param name="discount">The discount.</param>
		/// <param name="PriceBeforeDiscount">The price before discount.</param>
		/// <returns></returns>
		public static int GetAdjustedPrice(this DiscountProduct discount, int PriceBeforeDiscount)
		{
			if (discount == null || discount.DiscountType == DiscountType.FreeShipping) return PriceBeforeDiscount;

			if (discount.DiscountType == DiscountType.Percentage)
			{
				return PriceBeforeDiscount - (int) Math.Round(PriceBeforeDiscount*discount.DiscountValue/10000m);
			}
			if (discount.DiscountType == DiscountType.Amount)
			{
				return PriceBeforeDiscount - discount.DiscountValue;
			}
			if (discount.DiscountType == DiscountType.NewPrice)
			{
				return discount.DiscountValue;
			}
			return PriceBeforeDiscount;
		}
	}
}