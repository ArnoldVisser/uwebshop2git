﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	internal static class DiscountExtensions
	{
		internal static IEnumerable<IOrderDiscount> HasDiscountForOrder(this IEnumerable<IOrderDiscount> discounts, OrderInfo orderInfo)
		{
			return discounts.Where(discount => IO.Container.Resolve<IDiscountCalculationService>().DiscountAmountForOrder(discount, orderInfo) > 0);
		}
	}
}