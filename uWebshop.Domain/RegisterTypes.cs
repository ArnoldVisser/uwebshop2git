﻿using uWebshop.Domain.Businesslogic;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Repositories;
using uWebshop.Domain.Services;
using VATChecker;

namespace uWebshop.Domain
{
	internal static class RegisterTypes
	{
		public static void Register()
		{
			// CMS unaware services (uwebshop default)
			IO.Container.RegisterType<IDiscountCalculationService, DiscountCalculationService>();
			IO.Container.RegisterType<IUrlRewritingService, UrlRewritingService>();
			IO.Container.RegisterType<ICatalogUrlResolvingService, CatalogUrlResolvingService>();
			IO.Container.RegisterType<IVATCheckService, ViesVatCheckService>();
			IO.Container.RegisterType<IOrderUpdatingService, OrderUpdatingService>();
			IO.Container.RegisterType<IStockService, StockService>();
			IO.Container.RegisterType<IUwebshopRequestService, UwebshopHttpContextRequestService>();
			IO.Container.RegisterType<IInstaller, UwebshopDefaultInstaller>();
			IO.Container.RegisterType<ICouponCodeService, CouponCodeService>();

			IO.Container.RegisterType<IProductUrlService, ProductUrlUsingCurrentStoreService>();
			IO.Container.RegisterType<ICatalogUrlService, CatalogUrlService>();
			IO.Container.RegisterType<ICategoryCatalogUrlService, CategoryCatalogUrlService>();
			IO.Container.RegisterType<IUrlFormatService, UrlFormatService>();
			IO.Container.RegisterType<IStoreProductUrlService, StoreProductUrlService>();

			// entity services (more registered in Umbraco part)
			IO.Container.RegisterType<IProductService, ProductService>();
			IO.Container.RegisterType<IProductVariantService, ProductVariantService>();
			IO.Container.RegisterType<ICategoryService, CategoryService>();
			IO.Container.RegisterType<IOrderService, OrderService>();
			IO.Container.RegisterType<IPaymentProviderService, PaymentProviderService>();
			IO.Container.RegisterType<IShippingProviderService, ShippingProviderService>();
			IO.Container.RegisterType<IDiscountService, DiscountService>();
			// todo: decouple entity services from umbraco

			// repos (mostly umbraco specific)
			IO.Container.RegisterType<ICountryRepository, UwebshopApplicationCachedCountriesRepository>();
			IO.Container.RegisterType<IVATCountryRepository, UwebshopApplicationCachedVATCountriesRepository>();
			IO.Container.RegisterType<IOrderRepository, OrderRepository>();

			IO.Container.RegisterType<IUwebshopConfiguration, UwebshopConfiguration>();
			UwebshopConfiguration.Current = IO.Container.Resolve<IUwebshopConfiguration>(); // hmm, but this code is smaller&faster than Lazy<T>
			UwebshopRequest.Service = IO.Container.Resolve<IUwebshopRequestService>();

			//IO.Container.RegisterInstance(Settings.GetSettings()); // todo: manage cache

			//IShippingProviderUpdateService hmmm
		}
	}
}