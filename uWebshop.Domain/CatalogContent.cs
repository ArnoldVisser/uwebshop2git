﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using System.Xml;
using System.Xml.XPath;
using uWebshop.Domain.Helpers;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.BusinessLogic.Utils;
using umbraco.cms.businesslogic.cache;
using umbraco.cms.businesslogic.propertytype;
using umbraco.cms.businesslogic.web;
using umbraco.DataLayer;
using umbraco.IO;
using umbraco.NodeFactory;
using umbraco.interfaces;

namespace uWebshop.Domain
{
    public class CatalogContent : IRequiresSessionState
    {
        #region properties
        public bool ContentCacheXmlIsEphemeral
        {
            get
            {
                bool returnValue = false;
                string configSetting = ConfigurationManager.AppSettings["umbracoContentXMLUseLocalTemp"];

                if (!string.IsNullOrEmpty(configSetting))
                    if (bool.TryParse(configSetting, out returnValue))
                        return returnValue;

                return false;
            }
        }

        public string ContentCacheXml
        {
            get
            {
                if (ContentCacheXmlIsEphemeral)
                {
                    Log.Add(LogTypes.Debug, 0, "Path: " + Path.Combine(HttpRuntime.CodegenDir, @"UmbracoData\uwbs.config"));
                    return Path.Combine(HttpRuntime.CodegenDir, @"UmbracoData\uwbs.config");
                }
                return IOHelper.returnPath("~/App_Data/uwbs.config", "~/App_Data/uwbs.config");
            }
        }

        private string _umbracoXmlDiskCacheFileName = string.Empty;

        /// <summary>
        /// Gets the path of the umbraco XML disk cache file.
        /// </summary>
        /// <value>The name of the umbraco XML disk cache file.</value>
        public string UmbracoXmlDiskCacheFileName
        {
            get
            {
                if (string.IsNullOrEmpty(_umbracoXmlDiskCacheFileName))
                {
                    _umbracoXmlDiskCacheFileName = IOHelper.MapPath(ContentCacheXml);
                }

                return _umbracoXmlDiskCacheFileName;
            }
            set
            {
                _umbracoXmlDiskCacheFileName = value;
            }
        }

        private readonly string XmlContextContentItemKey = "uwbsXmlCatalogContent";

        // Current content
        private volatile XmlDocument _xmlContent = null;

        // Sync access to disk file
        private static object _readerWriterSyncLock = new object();

        // Sync access to internal cache
        private static object _xmlContentInternalSyncLock = new object();

        // Sync database access
        private static object _dbReadSyncLock = new object();

        private static DocumentType categoryDocumentType = DocumentType.GetByAlias("uwbsCategory");
        private static DocumentType productDocumentType = DocumentType.GetByAlias("uwbsProduct");
        private static DocumentType pricingDocumentType = DocumentType.GetByAlias("uwbsPricing");
        private static DocumentType pricingVariantDocumentType = DocumentType.GetByAlias("uwbsPricingVariant");
        public static Store Store;

        public Dictionary<string, string> Languages = new Dictionary<string, string>();
        
        private int _LastOrderSequenceNumber;

        private int _LastOrderDayNumber;

        public int LastOrderNumber
        {
            get
            {
                if (_LastOrderSequenceNumber == 0)
                {
                    try
                    {
                        var orderDocs = new List<Document>(Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uwbsOrder").Id)).Where(x => x.IsTrashed == false);
                        var orderTexts = orderDocs.Select(orderDoc => orderDoc.Text.Remove(0, orderDoc.Text.Length - 12)).ToList();

                        var newestOrderNumber = orderTexts.OrderByDescending(x => x).FirstOrDefault();

                        //Log.Add(LogTypes.Debug, 0, "newestOrderNumber: " + newestOrderNumber);

                        if (newestOrderNumber != null)
                        {
                            int.TryParse(newestOrderNumber.Substring(newestOrderNumber.Length - 6, 2), out _LastOrderDayNumber);
                            int.TryParse(newestOrderNumber.Substring(newestOrderNumber.Length - 4, 4), out _LastOrderSequenceNumber);
                        }
                    }
                    catch (Exception)
                    { }
                }

                return _LastOrderSequenceNumber;
            }
            set
            {
                _LastOrderSequenceNumber = value;

                if (_LastOrderDayNumber == DateTime.Today.Day) return;
                _LastOrderDayNumber = DateTime.Today.Day;
                _LastOrderSequenceNumber = 1;
            }
        }
        #endregion

        #region Constructors
        public CatalogContent()
        {

        }

        static CatalogContent()
        {
            Trace.WriteLine("Checking for xml content initialisation...");
            Instance.CheckXmlContentPopulation();
        }
        #endregion

        #region Singleton
        public static CatalogContent Instance
        {
            get { return Singleton<CatalogContent>.Instance; }
        }
        #endregion

        #region Properties
        /// <summary>
        /// Get content. First call to this property will initialize xmldoc
        /// subsequent calls will be blocked until initialization is done
        /// Further we cache(in context) xmlContent for each request to ensure that
        /// we always have the same XmlDoc throughout the whole request.
        /// </summary>
        public virtual XmlDocument XmlContent
        {
            get
            {
                if (HttpContext.Current == null)
                    return XmlContentInternal;
                XmlDocument content = HttpContext.Current.Items[XmlContextContentItemKey] as XmlDocument;
                if (content == null)
                {
                    content = XmlContentInternal;
                    HttpContext.Current.Items[XmlContextContentItemKey] = content;
                }

                return content;
            }
        }

        [Obsolete("Please use: content.Instance.XmlContent")]
        public static XmlDocument xmlContent
        {
            get { return Instance.XmlContent; }
        }

        public virtual bool isInitializing
        {
            get { return _xmlContent == null; }
        }

        /// <summary>
        /// Internal reference to XmlContent
        /// </summary>
        protected virtual XmlDocument XmlContentInternal
        {
            get
            {
                CheckXmlContentPopulation();

                return _xmlContent;
            }
            set
            {
                lock (_xmlContentInternalSyncLock)
                {
                    // Clear macro cache
                    Cache.ClearCacheObjectTypes("umbraco.MacroCacheContent");
                    // Clear library cache
                    if (UmbracoSettings.UmbracoLibraryCacheDuration > 0)
                    {
                        Cache.ClearCacheObjectTypes("MS.Internal.Xml.XPath.XPathSelectionIterator");
                    }
                    requestHandler.ClearProcessedRequests();
                    _xmlContent = value;

                    if (!UmbracoSettings.isXmlContentCacheDisabled && UmbracoSettings.continouslyUpdateXmlDiskCache)
                        QueueXmlForPersistence();
                    else
                        // Clear cache...
                        DeleteXmlCache();
                }
            }
        }

        /// <summary>
        /// Triggers the XML content population if necessary.
        /// </summary>
        /// <returns></returns>
        private bool CheckXmlContentPopulation()
        {
            if (isInitializing)
            {
                lock (_xmlContentInternalSyncLock)
                {
                    if (isInitializing)
                    {
                        Trace.WriteLine(string.Format("Initializing content on thread '{0}' (Threadpool? {1})", Thread.CurrentThread.Name, Thread.CurrentThread.IsThreadPoolThread.ToString()));
                        _xmlContent = LoadContent();
                        Trace.WriteLine("Content initialized (loaded)");

                        // Only save new XML cache to disk if we just repopulated it
                        // TODO: Re-architect this so that a call to this method doesn't invoke a new thread for saving disk cache
                        if (!UmbracoSettings.isXmlContentCacheDisabled && !IsValidDiskCachePresent())
                        {
                            QueueXmlForPersistence();
                        }
                        return true;
                    }
                }
            }
            Trace.WriteLine("Content initialized (was already in context)");
            return false;
        }
        #endregion

        protected static ISqlHelper SqlHelper
        {
            get { return Application.SqlHelper; }
        }

        #region Public Methods
        /// <summary>
        /// Sorts the documents.
        /// </summary>
        /// <param name="parentNode">The parent node.</param>
        public static void SortNodes(ref XmlNode parentNode)
        {
            XmlNode n = parentNode.CloneNode(true);

            // remove all children from original node
            string xpath = UmbracoSettings.UseLegacyXmlSchema ? "./node" : "./* [@id]";
            foreach (XmlNode child in parentNode.SelectNodes(xpath))
                parentNode.RemoveChild(child);


            XPathNavigator nav = n.CreateNavigator();
            XPathExpression expr = nav.Compile(xpath);
            expr.AddSort("@sortOrder", XmlSortOrder.Ascending, XmlCaseOrder.None, "", XmlDataType.Number);
            XPathNodeIterator iterator = nav.Select(expr);
            while (iterator.MoveNext())
                parentNode.AppendChild(
                    ((IHasXmlNode)iterator.Current).GetNode());
        }

        /// <summary>
        /// Updates the document cache.
        /// </summary>
        public virtual void UpdateDocumentCache(Document sender, bool isRemoved)
        {
            int id = sender.Id;
            var node = new Node(id);

            XmlNodeList nodesFound = null;
            XmlNode nodeFound = null;
            XmlNode virtualNodeFound = null;

            if (node.NodeTypeAlias == "uwbsLanguageNode" && UpdateLanguageCache(id, isRemoved))
            {
                return;
            }

            #region move
            var oldCulture = Thread.CurrentThread.CurrentCulture;
            var oldUICulture = Thread.CurrentThread.CurrentUICulture;

            foreach (var store in StoreHelper.GetAllStores())
            {
                Store = store;
                var storeAlias = store.Alias;
                var LanguageCode = store.LanguageCode;
                var LanguageCountry = store.CountryCode;

                Thread.CurrentThread.CurrentCulture = store.CultureInfo;
                Thread.CurrentThread.CurrentUICulture = store.CultureInfo;

                try
                {
                    HttpContext.Current.Session["Alias"] = store.Alias;
                }
                catch { }

                var document = new Document(true, id);
                nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), storeAlias.ToLower(), id));

                //virtualNodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@virtualizedId='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), id));


                if (nodeFound != null)
                {
                    int parentId = 0;

                    if (nodeFound.ParentNode.ParentNode.Attributes["id"] != null)
                    {
                        int.TryParse(nodeFound.ParentNode.ParentNode.Attributes["id"].Value, out parentId);
                    }

                    if (document.Parent.Id != parentId && parentId != 0)
                    {
                        var parentNode = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), storeAlias.ToLower(), document.Parent.Id));

                        nodeFound.ParentNode.RemoveChild(nodeFound);

                        switch (node.NodeTypeAlias)
                        {
                            case "uwbsCategory":
                                #region category
                                var categoriesNode = parentNode.SelectSingleNode("categories");

                                if (categoriesNode == null)
                                {
                                    categoriesNode = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("categories");

                                    parentNode.AppendChild(categoriesNode);
                                }

                                categoriesNode.AppendChild(nodeFound);
                                break;
                                #endregion
                            case "uwbsVirtualCategory":
                                #region virtual category
                                var virtualCategoriesNode = parentNode.SelectSingleNode("categories");

                                if (virtualCategoriesNode == null)
                                {
                                    virtualCategoriesNode = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("categories");

                                    parentNode.AppendChild(virtualCategoriesNode);
                                }

                                virtualCategoriesNode.AppendChild(nodeFound);
                                break;
                                #endregion
                            case "uwbsProduct":
                                #region product
                                var productsNode = parentNode.SelectSingleNode("products");

                                if (productsNode == null)
                                {
                                    productsNode = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("products");

                                    parentNode.AppendChild(productsNode);
                                }

                                productsNode.AppendChild(nodeFound);

                                break;
                                #endregion
                            case "uwbsVirtualProduct":
                                #region virtual product
                                var virtualProductsNode = parentNode.SelectSingleNode("products");

                                if (virtualProductsNode == null)
                                {
                                    virtualProductsNode = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("products");

                                    parentNode.AppendChild(virtualProductsNode);
                                }

                                virtualProductsNode.AppendChild(nodeFound);

                                break;
                                #endregion
                            case "uwbsPricing":
                                #region pricing
                                var pricingsNode = parentNode.SelectSingleNode("pricings");

                                if (pricingsNode == null)
                                {
                                    pricingsNode = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("pricings");

                                    parentNode.AppendChild(pricingsNode);
                                }

                                pricingsNode.AppendChild(nodeFound);
                                break;
                                #endregion
                        }
                    }
                }
            }

            Thread.CurrentThread.CurrentCulture = oldCulture;
            Thread.CurrentThread.CurrentUICulture = oldUICulture;
            #endregion

            #region remove
            if (isRemoved)
            {
                var document = new Document(true, id);
                var alias = document.ContentType.Alias;
                var productId = 0;

                if (alias == "uwbsPricing")
                {
                    nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode("//*[@id='" + id + "']");

                    if (nodeFound != null)
                    {
                        productId = int.Parse(nodeFound.ParentNode.ParentNode.Attributes["id"].Value);
                    }
                }

                //if (alias != "uwbsSale" && alias != "uwbsVariantSale")
                if (alias != "uwbsPricingDiscount")
                {
                    nodesFound = Instance.XmlContent.DocumentElement.SelectNodes("//*[@id='" + id + "']");

                    if (nodesFound != null)
                    {
                        foreach (XmlNode n in nodesFound)
                        {
                            n.ParentNode.RemoveChild(n);
                        }
                    }
                }

                switch (alias)
                {
                    case "uwbsPricing":
                        #region pricing
                        if (nodeFound != null && productId > 0)
                        {
                            var productDoc = new Document(true, productId);

                            var author = User.GetUser(0);
                            productDoc.Publish(author);

                            library.UpdateDocumentCache(productDoc.Id);
                        }
                        #endregion
                        break;
                    case "uwbsPricingDiscount":
                        #region pricing discount
                        foreach (var store in StoreHelper.GetAllStores())
                        {
                            Store = store;
                            Store = store;
                            var StoreAlias = store.Alias;
                            var LanguageCode = store.LanguageCode;
                            var LanguageCountry = store.CountryCode;

                            Thread.CurrentThread.CurrentCulture = store.CultureInfo;
                            Thread.CurrentThread.CurrentUICulture = store.CultureInfo;

                            try
                            {
                                HttpContext.Current.Session["Alias"] = store.Alias;
                            }
                            catch { }

                            try
                            {
                                library.setCookie("alias", store.Alias);
                            }
                            catch { }

                            var pricingDiscount = new PricingDiscount(id);

                            nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), StoreAlias.ToLower(), sender.Id));

                            if (nodesFound != null)
                            {
                                foreach (XmlNode n in nodesFound)
                                {
                                    var pricingId = int.Parse(n.ParentNode.Attributes["id"].Value);

                                    nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), StoreAlias.ToLower(), pricingId));

                                    #region update node
                                    if (nodeFound != null)
                                    {
                                        var parent = nodeFound.ParentNode;
                                        parent.RemoveChild(nodeFound);
                                        parent.InnerXml += GetPricingXml(pricingId, true);
                                    }
                                    #endregion
                                }
                            }
                        }
                        #endregion
                        break;
                    case "uwbsSale":
                        #region sale
                        //oldCulture = Thread.CurrentThread.CurrentCulture;
                        //oldUICulture = Thread.CurrentThread.CurrentUICulture;

                        //foreach (var shopAlias in ShopAliasHelper.GetAllShopAlias())
                        //{
                        //    ShopAlias = shopAlias;
                        //    var LanguageAlias = shopAlias.Alias;
                        //    var LanguageCode = shopAlias.LanguageCode;
                        //    var LanguageCountry = shopAlias.CountryCode;

                        //    Thread.CurrentThread.CurrentCulture = shopAlias.CultureInfo;
                        //    Thread.CurrentThread.CurrentUICulture = shopAlias.CultureInfo;
                        //    HttpContext.Current.Session["Alias"] = shopAlias.Alias;

                        //    nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), id));

                        //    if (nodesFound != null)
                        //    {
                        //        foreach (XmlNode n in nodesFound)
                        //        {
                        //            if (n.ParentNode != null)
                        //            {
                        //                var pricingId = int.Parse(n.ParentNode.Attributes["id"].Value);

                        //                n.ParentNode.InnerXml = GetPricingXml(pricingId, true);
                        //            }
                        //        }
                        //    }
                        //}

                        //Thread.CurrentThread.CurrentCulture = oldCulture;
                        //Thread.CurrentThread.CurrentUICulture = oldUICulture;
                        break;
                        #endregion
                    case "uwbsVariantSale":
                        #region variant sale
                        oldCulture = Thread.CurrentThread.CurrentCulture;
                        oldUICulture = Thread.CurrentThread.CurrentUICulture;

                        foreach (var shopAlias in StoreHelper.GetAllStores())
                        {
                            Store = shopAlias;
                            var LanguageAlias = shopAlias.Alias;
                            var LanguageCode = shopAlias.LanguageCode;
                            var LanguageCountry = shopAlias.CountryCode;

                            Thread.CurrentThread.CurrentCulture = shopAlias.CultureInfo;
                            Thread.CurrentThread.CurrentUICulture = shopAlias.CultureInfo;
                            HttpContext.Current.Session["Alias"] = shopAlias.Alias;

                            nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), id));

                            if (nodesFound != null)
                            {
                                foreach (XmlNode n in nodesFound)
                                {
                                    if (n.ParentNode != null)
                                    {
                                        var pricingVariantId = int.Parse(n.ParentNode.Attributes["id"].Value);

                                        n.ParentNode.ParentNode.InnerXml = GetPricingVariantXml(pricingVariantId, true);
                                    }
                                }
                            }
                        }

                        Thread.CurrentThread.CurrentCulture = oldCulture;
                        Thread.CurrentThread.CurrentUICulture = oldUICulture;
                        break;
                        #endregion
                }

                QueueXmlForPersistence();

                return;
            }
            #endregion

            #region publish
            oldCulture = Thread.CurrentThread.CurrentCulture;
            oldUICulture = Thread.CurrentThread.CurrentUICulture;

            foreach (var shopAlias in StoreHelper.GetAllStores())
            {
                Store = shopAlias;
                var LanguageAlias = shopAlias.Alias;
                var LanguageCode = shopAlias.LanguageCode;
                var LanguageCountry = shopAlias.CountryCode;

                Thread.CurrentThread.CurrentCulture = shopAlias.CultureInfo;
                Thread.CurrentThread.CurrentUICulture = shopAlias.CultureInfo;
                HttpContext.Current.Session["Alias"] = shopAlias.Alias;

                switch (node.NodeTypeAlias)
                {
                    case "uwbsCategory":
                        #region category
                        nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Id));

                        virtualNodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Id));

                        if (nodeFound == null)
                        {
                            #region new node
                            var parentNode = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Parent.Id));

                            if (parentNode != null)
                            {
                                var category = new Category(id);

                                if (!string.IsNullOrEmpty(category.Title) && !string.IsNullOrEmpty(category.LocalizedUrl))
                                {
                                    var categoriesNode = parentNode.SelectSingleNode("categories");

                                    if (categoriesNode != null)
                                    {
                                        categoriesNode.InnerXml += GetCategoryXml(sender, false);
                                    }
                                    else
                                    {
                                        var xml = GetCategoryXml(sender, false);

                                        if (node.Parent.NodeTypeAlias == "uwbsCategory")
                                        {
                                            xml = string.Format("<categories>{0}</categories>", xml);
                                        }

                                        parentNode.InnerXml += xml;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region update node
                            var category = new Category(id);

                            if (!string.IsNullOrEmpty(category.Title) && !string.IsNullOrEmpty(category.LocalizedUrl))
                            {
                                var productsXml = string.Empty;
                                var categoriesXml = string.Empty;

                                if (nodeFound.SelectSingleNode("products") != null)
                                {
                                    productsXml = nodeFound.SelectSingleNode("products").OuterXml;
                                }

                                if (nodeFound.SelectSingleNode("categories") != null)
                                {
                                    categoriesXml = nodeFound.SelectSingleNode("categories").OuterXml;
                                }

                                var parent = nodeFound.ParentNode;
                                parent.RemoveChild(nodeFound);
                                parent.InnerXml += GetCategoryXml(sender, true);

                                var catNode = parent.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Id));

                                catNode.InnerXml += categoriesXml;
                                catNode.InnerXml += productsXml;
                            }
                            else
                            {
                                nodeFound.ParentNode.RemoveChild(nodeFound);
                            }
                            #endregion
                        }
                        break;
                        #endregion
                    case "uwbsProduct":
                        #region product
                        nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Id));

                        if (nodeFound == null)
                        {
                            #region new node
                            var parentNode = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Parent.Id));

                            if (parentNode != null)
                            {
                                var product = new Product(id);

                                if (!string.IsNullOrEmpty(product.Title) && !string.IsNullOrEmpty(product.LocalizedUrl))
                                {
                                    var productsNode = parentNode.SelectSingleNode("products");

                                    if (productsNode != null)
                                    {
                                        productsNode.InnerXml += GetProductXml(sender, false);
                                    }
                                    else
                                    {
                                        var xml = GetProductXml(sender, false);

                                        if (node.Parent.NodeTypeAlias == "uwbsCategory")
                                        {
                                            xml = string.Format("<products>{0}</products>", xml);
                                        }

                                        parentNode.InnerXml += xml;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region update node
                            var product = new Product(id);

                            if (!string.IsNullOrEmpty(product.Title) && !string.IsNullOrEmpty(product.LocalizedUrl))
                            {
                                var pricingsXml = string.Empty;

                                if (nodeFound.SelectSingleNode("pricings") != null)
                                {
                                    pricingsXml = nodeFound.SelectSingleNode("pricings").OuterXml;
                                }

                                var parent = nodeFound.ParentNode;
                                parent.RemoveChild(nodeFound);
                                parent.InnerXml += GetProductXml(sender, true);

                                var prodNode = parent.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Id));

                                prodNode.InnerXml += pricingsXml;
                            }
                            else
                            {
                                nodeFound.ParentNode.RemoveChild(nodeFound);
                            }
                            #endregion
                        }
                        break;
                        #endregion
                    case "uwbsPricing":
                        #region pricing
                        var pricing = new Pricing(id);

                        nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Id));

                        if (nodeFound == null)
                        {
                            #region new node
                            var parentNode = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Parent.Id));

                            if (parentNode != null)
                            {
                                if (!string.IsNullOrEmpty(pricing.Title))
                                {
                                    var pricingsNode = parentNode.SelectSingleNode("pricings");

                                    if (pricingsNode != null)
                                    {
                                        pricingsNode.InnerXml += GetPricingXml(id, sender, false);
                                    }
                                    else
                                    {
                                        var xml = GetPricingXml(id, sender, false);

                                        if (node.Parent.NodeTypeAlias == "uwbsProduct")
                                        {
                                            xml = string.Format("<pricings>{0}</pricings>", xml);
                                        }

                                        parentNode.InnerXml += xml;
                                    }
                                }
                            }
                            #endregion
                        }
                        else
                        {
                            #region update node
                            if (!string.IsNullOrEmpty(pricing.Title))
                            {
                                var parent = nodeFound.ParentNode;
                                parent.RemoveChild(nodeFound);
                                parent.InnerXml += GetPricingXml(id, sender, false);
                            }
                            else
                            {
                                nodeFound.ParentNode.RemoveChild(nodeFound);
                            }
                            #endregion
                        }
                        break;
                        #endregion
                    case "uwbsPricingVariant":
                        #region pricingVariant
                        var pricingVariant = new PricingVariant(id);

                        library.UpdateDocumentCache(pricingVariant.ParentId);

                        //nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), node.Id));

                        //if (nodesFound != null)
                        //{
                        //    var pricingVariantXml = GetPricingVariantXml(id, false);

                        //    foreach (XmlNode n in nodesFound)
                        //    {
                        //        n.InnerXml = pricingVariantXml;
                        //    }
                        //}
                        break;
                        #endregion
                    case "uwbsPricingDiscount":
                        #region pricing discount
                        var pricingDiscount = new PricingDiscount(id);

                        nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), pricingDiscount.Id));

                        if (nodesFound != null)
                        {
                            foreach (var p in pricingDiscount.Pricings)
                            {
                                nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), p.Id));

                                #region update node
                                if (nodeFound != null)
                                {
                                    var parent = nodeFound.ParentNode;
                                    parent.RemoveChild(nodeFound);
                                    parent.InnerXml += GetPricingXml(p.Id, sender, false);
                                }
                                #endregion
                            }
                        }
                        #endregion
                        break;
                    case "uwbsSale":
                        #region sale
                        //var sale = new Sale(id);

                        //nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), sale.Id));

                        //foreach (XmlNode n in nodesFound)
                        //{
                        //    var pricingId = int.Parse(n.ParentNode.Attributes["id"].Value);

                        //    #region update node
                        //    var parent = n.ParentNode.ParentNode;
                        //    parent.RemoveChild(n.ParentNode);
                        //    parent.InnerXml += GetPricingXml(pricingId, sender, false);
                        //    #endregion
                        //}

                        //foreach (var p in sale.Pricings)
                        //{
                        //    nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), p.Id));

                        //    #region update node
                        //    var parent = nodeFound.ParentNode;
                        //    parent.RemoveChild(nodeFound);
                        //    parent.InnerXml += GetPricingXml(p.Id, sender, false);
                        //    #endregion
                        //}
                        break;
                        #endregion
                    case "uwbsSettings":
                        #region settings
                        break;
                        #endregion
                    case "uwbsLicense":
                        #region uwbs-license

                        if (!Licensing.IsTrial())
                        {
                            Instance.XmlContent.DocumentElement.InnerXml = Instance.XmlContent.DocumentElement.InnerXml.Replace("[demo]-", string.Empty);
                        }
                        break;
                        #endregion
                    case "uwbsVariantSale":
                        #region variant sale
                        var variantSale = new VariantSale(id);

                        nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), variantSale.Id));

                        if (nodesFound != null)
                        {
                            foreach (XmlNode n in nodesFound)
                            {
                                var pricingVariantId = int.Parse(n.ParentNode.Attributes["id"].Value);

                                n.ParentNode.ParentNode.InnerXml = GetPricingVariantXml(pricingVariantId, false);
                            }

                            foreach (var p in variantSale.PricingVariants)
                            {
                                nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("/root/language[@lang='{0}' and @country='{1}' and @alias='{2}']//*[@id='{3}']", LanguageCode.ToLower(), LanguageCountry.ToLower(), LanguageAlias.ToLower(), p.Id));

                                if (nodeFound != null)
                                {
                                    nodeFound.ParentNode.InnerXml = GetPricingVariantXml(p.Id, false);
                                }
                            }
                        }
                        break;
                        #endregion
                    default:
                        break;
                }
            }

            Thread.CurrentThread.CurrentCulture = oldCulture;
            Thread.CurrentThread.CurrentUICulture = oldUICulture;

            QueueXmlForPersistence();
            #endregion
        }

        public bool UpdateLanguageCache(int pageId, bool isRemoved)
        {
            var node = new Node(pageId);

            try
            {
                if (node.NodeTypeAlias == "uwbsLanguageNode")
                {
                    if (!isRemoved)
                    {
                        var language = new Store(pageId);
                        var catalogNodeId = DomainHelper.GetNodeIdForDocument("uwbsCatalog", "Catalog");

                        var nodesFound = Instance.XmlContent.DocumentElement.SelectNodes(string.Format("//*[@id='{0}']", pageId));
                        if (nodesFound == null || nodesFound.Count == 0)
                        {
                            Log.Add(LogTypes.Debug, 0, "nodesFound == null || nodesFound.Count == 0");

                            var languageElement = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("language");
                            languageElement.SetAttribute("lang", language.LanguageCode.ToLower());
                            languageElement.SetAttribute("country", language.CountryCode.ToLower());
                            languageElement.SetAttribute("currency", language.CurrencyCode.ToLower());
                            languageElement.SetAttribute("alias", language.Alias.ToLower());
                            languageElement.SetAttribute("id", language.Id.ToString());

                            var catalogElement = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("catalog");
                            catalogElement.SetAttribute("id", catalogNodeId.ToString());

                            var categoriesElement = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("categories");

                            catalogElement.AppendChild(categoriesElement);

                            languageElement.AppendChild(catalogElement);

                            Instance.XmlContent.DocumentElement.AppendChild(languageElement);
                        }
                        else
                        {
                            var languageNode = nodesFound[0];
                            if (languageNode != null)
                            {
                                languageNode.Attributes["lang"].Value = language.LanguageCode.ToLower();
                                languageNode.Attributes["country"].Value = language.CountryCode.ToLower();
                                languageNode.Attributes["currency"].Value = language.CurrencyCode.ToLower();
                                languageNode.Attributes["alias"].Value = language.Alias.ToLower();
                                languageNode.Attributes["id"].Value = language.Id.ToString();

                                var catalogNode = languageNode.SelectSingleNode("catalog");
                                if (catalogNode == null)
                                {
                                    var catalogElement = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("catalog");
                                    catalogElement.SetAttribute("id", catalogNodeId.ToString());

                                    var categoriesElement = Instance.XmlContent.DocumentElement.OwnerDocument.CreateElement("categories");

                                    catalogElement.AppendChild(categoriesElement);

                                    languageNode.AppendChild(catalogElement);
                                }
                                else
                                {
                                    languageNode.Attributes["id"].Value = language.Id.ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        var nodeFound = Instance.XmlContent.DocumentElement.SelectSingleNode(string.Format("//*[@id='{0}']", node.Id));

                        if (nodeFound != null)
                        {
                            nodeFound.ParentNode.RemoveChild(nodeFound);
                        }
                    }

                    QueueXmlForPersistence();

                    return true;
                }
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, "UpdateLanguageCache " + ex);
            }

            return false;
        }
        #endregion

        #region Protected & Private methods
        /// <summary>
        /// Invalidates the disk content cache file. Effectively just deletes it.
        /// </summary>
        private void DeleteXmlCache()
        {
            lock (_readerWriterSyncLock)
            {
                if (System.IO.File.Exists(UmbracoXmlDiskCacheFileName))
                {
                    // Reset file attributes, to make sure we can delete file
                    try
                    {
                        System.IO.File.SetAttributes(UmbracoXmlDiskCacheFileName, FileAttributes.Normal);
                    }
                    finally
                    {
                        System.IO.File.Delete(UmbracoXmlDiskCacheFileName);
                    }
                }
            }
        }

        /// <summary>
        /// Clear HTTPContext cache if any
        /// </summary>
        private void ClearContextCache()
        {
            // If running in a context very important to reset context cache orelse new nodes are missing
            if (HttpContext.Current != null && HttpContext.Current.Items.Contains(XmlContextContentItemKey))
                HttpContext.Current.Items.Remove(XmlContextContentItemKey);
        }

        /// <summary>
        /// Load content from either disk or database
        /// </summary>
        /// <returns></returns>
        private XmlDocument LoadContent()
        {
            if (!UmbracoSettings.isXmlContentCacheDisabled && IsValidDiskCachePresent())
            {
                try
                {
                    return LoadContentFromDiskCache();
                }
                catch (Exception e)
                {
                    // This is really bad, loading from cache file failed for some reason, now fallback to loading from database
                    Debug.WriteLine("Content file cache load failed: " + e);
                    DeleteXmlCache();
                }
            }
            return LoadContentFromDatabase();
        }

        private bool IsValidDiskCachePresent()
        {
            if (System.IO.File.Exists(UmbracoXmlDiskCacheFileName))
            {
                // Only return true if we don't have a zero-byte file
                var f = new FileInfo(UmbracoXmlDiskCacheFileName);
                if (f.Length > 0)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Load content from cache file
        /// </summary>
        private XmlDocument LoadContentFromDiskCache()
        {
            lock (_readerWriterSyncLock)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(UmbracoXmlDiskCacheFileName);
                return xmlDoc;
            }
        }

        private void InitContentDocumentBase(XmlDocument xmlDoc)
        {
            xmlDoc.LoadXml("<?xml version=\"1.0\" encoding=\"utf-8\" ?><root />");
        }

        /// <summary>
        /// Load content from database
        /// </summary>
        private XmlDocument LoadContentFromDatabase()
        {
            XmlDocument xmlDoc = new XmlDocument();

            // Alex N - 2010 06 - Very generic try-catch simply because at the moment, unfortunately, this method gets called inside a ThreadPool thread
            // and we need to guarantee it won't tear down the app pool by throwing an unhandled exception
            try
            {

                // Moved User to a local variable - why are we causing user 0 to load from the DB though?
                // Alex N 20100212
                User staticUser = null;
                try
                {
                    staticUser = User.GetCurrent();  //User.GetUser(0);
                }
                catch
                {
                    /* We don't care later if the staticUser is null */
                }

                // Moved this to after the logging since the 2010 schema accesses the DB just to generate the DTD
                InitContentDocumentBase(xmlDoc);

                Hashtable nodes = new Hashtable();
                Hashtable parents = new Hashtable();
                try
                {
                    // Esben Carlsen: At some point we really need to put all data access into to a tier of its own.
                    string sql =
                        @"select umbracoNode.id, umbracoNode.parentId, umbracoNode.sortOrder, cmsContentXml.xml from umbracoNode 
inner join cmsContentXml on cmsContentXml.nodeId = umbracoNode.id and umbracoNode.nodeObjectType = @type
order by umbracoNode.level, umbracoNode.sortOrder";

                    lock (_dbReadSyncLock)
                    {
                        using (IRecordsReader dr = SqlHelper.ExecuteReader(sql, SqlHelper.CreateParameter("@type", new Guid("C66BA18E-EAF3-4CFF-8A22-41B16D66A972"))))
                        {
                            while (dr.Read())
                            {
                                int currentId = dr.GetInt("id");
                                int parentId = dr.GetInt("parentId");

                                xmlDoc.LoadXml(dr.GetString("xml"));
                                nodes.Add(currentId, xmlDoc.FirstChild);

                                if (parents.ContainsKey(parentId))
                                    ((ArrayList)parents[parentId]).Add(currentId);
                                else
                                {
                                    ArrayList a = new ArrayList();
                                    a.Add(currentId);
                                    parents.Add(parentId, a);
                                }
                            }
                        }
                    }

                    // TODO: Why is the following line here, it should have already been generated? Alex N 20100212
                    // Reset
                    InitContentDocumentBase(xmlDoc);

                    try
                    {
                        //GenerateXmlDocument(parents, nodes, -1, xmlDoc.DocumentElement);
                    }
                    catch (Exception ee)
                    {
                        Log.Add(LogTypes.Error, staticUser, -1, string.Format("Error while generating XmlDocument from database: {0}", ee));
                    }
                }
                catch (OutOfMemoryException)
                {
                    Log.Add(LogTypes.Error, staticUser, -1, string.Format("Error Republishing: Out Of Memory. Parents: {0}, Nodes: {1}", parents.Count, nodes.Count));
                }
                catch (Exception ee)
                {
                    Log.Add(LogTypes.Error, staticUser, -1, string.Format("Error Republishing: {0}", ee));
                }
            }
            catch (Exception ee)
            {
                Log.Add(LogTypes.Error, -1, string.Format("Error Republishing: {0}", ee));
            }

            return xmlDoc;
        }

        private static void AddCustomProperties(DocumentType DocumentType, Node node, StringBuilder sb)
        {
            foreach (PropertyType pt in DocumentType.PropertyTypes)
            {
                try
                {
                    IProperty property = null;
                    var alias = string.Empty;


                    if (DocumentType.Alias == "uwbsCategory")
                    {
                        // FIXED FIELDS:
                        // image
                        // title_lang
                        // categoryUrl_langalias
                        // description_langalias
                        // tags_lang
                    }

                    if (DocumentType.Alias == "uwbsProduct")
                    {
                        // FIXED FIELDS:
                        // productNumber
                        // images
                        // downloads

                        // title_langalias
                        // productUrl_langalias
                        // shortDescription_langalias
                        // longDescription_langalias
                        // tags_lang
                    }

                    if (DocumentType.Alias == "uwbsPricing")
                    {
                        // FIXED FIELDS:
                        // itemNumber
                        // itemWeight
                        // productStock
                        // backorder
                        // itemsOrdered

                        // title_langalias
                        // description_langalias
                        // priceWithoutTax_langalias
                        // taxPercentage_langalias
                        // ranges_langalias
                    }

                    if (DocumentType.Alias == "uwbsPricingVariant")
                    {
                        // FIXED FIELDS:
                        // group
                        // weight
                        // stock
                        // itemsOrdered
                        // title_langalias
                        // addedValue_langalias
                    }

                    if (pt.Alias.StartsWith("custom"))
                    {
                        alias = pt.Alias.Substring(6);

                        if (pt.Alias.Contains("_"))
                        {
                            if (alias.EndsWith("_" + Store.Alias))
                            {
                                alias = alias.Substring(0, alias.IndexOf('_'));

                                property = node.GetProperty(pt.Alias);
                            }
                        }
                        else
                        {
                            property = node.GetProperty(pt.Alias);
                        }
                    }

                    #region add element to xml
                    if (property != null)
                    {
                        string value = string.Empty;
                        if (property != null && !string.IsNullOrEmpty(property.Value))
                        {
                            value = property.Value;
                        }
                        sb.AppendFormat("<{0}>", alias);
                        sb.AppendFormat("<![CDATA[{0}]]>", value);
                        sb.AppendFormat("</{0}>", alias);
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    Log.Add(LogTypes.Debug, 0, "Error during adding custom property to xml " + ex.ToString());
                }
            }
        }

        private static string GetCategoryXml(Document document, bool includeSubnodes)
        {
            var category = new Category(document.Id);

            if (category.Disabled == false)
            {
                var sb = new StringBuilder("<category ");
                sb.AppendFormat("id=\"{0}\" ", category.Id);
                sb.AppendFormat("parentId=\"{0}\" ", category.ParentId);
                sb.AppendFormat("sortOrder=\"{0}\" ", category.Node.SortOrder);
                sb.AppendFormat("createDate=\"{0}\" ", category.Node.CreateDate);
                sb.AppendFormat("updateDate=\"{0}\" ", category.Node.UpdateDate);
                sb.AppendFormat("nodeName=\"{0}\" ", category.Name);
                sb.AppendFormat("urlName=\"{0}\" ", category.LocalizedUrl);
                sb.AppendFormat("path=\"{0}\">", document.Path);

                sb.AppendFormat("<path>{0}</path>", document.Path);
                sb.AppendFormat("<urlName>{0}</urlName>", category.LocalizedUrl);
                sb.AppendFormat("<title><![CDATA[{0}]]></title>", category.Title);
                sb.AppendFormat("<description><![CDATA[{0}]]></description>", category.Description);

                #region tags
                sb.Append("<tags>");

                foreach (string tag in category.Tags)
                {
                    if (!string.IsNullOrEmpty(tag))
                    {
                        sb.AppendFormat("<tag><![CDATA[{0}]]></tag>", tag);
                    }
                }

                sb.Append("</tags>");
                #endregion

                if (category.Image != null)
                {
                    sb.AppendFormat("<image id='{0}'>{1}</image>", category.Image.Id, category.Image.RelativePathToFile);
                }

                AddCustomProperties(categoryDocumentType, category.Node, sb);

                sb.Append("</category>");

                return sb.ToString();
            }

            return string.Empty;
        }

        private static string GetVirtualCategoryXml(Document document, bool includeSubnodes)
        {

            var virtualCategory = new VirtualCategory(document.Id);
            var category = new Category(virtualCategory.VirtualizedCategoryId);

            if (category.Disabled == false)
            {
                var sb = new StringBuilder("<category ");
                sb.AppendFormat("id=\"{0}\" ", document.Id);
                sb.AppendFormat("virtualizedId=\"{0}\" ", category.Id);
                sb.AppendFormat("parentId=\"{0}\" ", document.Parent.Id);
                sb.AppendFormat("sortOrder=\"{0}\" ", document.sortOrder);
                sb.AppendFormat("createDate=\"{0}\" ", document.CreateDateTime);
                sb.AppendFormat("updateDate=\"{0}\" ", document.UpdateDate);
                sb.AppendFormat("nodeName=\"{0}\" ", category.Name);
                sb.AppendFormat("urlName=\"{0}\" ", category.LocalizedUrl);
                sb.AppendFormat("path=\"{0}\">", document.Path);

                sb.Append("</category>");

                return sb.ToString();
            }

            return string.Empty;
        }

        private static string GetProductXml(Document document, bool includeSubnodes)
        {
            var product = new Product(document.Id);

            if (product.Enabled == true)
            {
                var sb = new StringBuilder("<product ");
                sb.AppendFormat("id=\"{0}\" ", product.Id);
                sb.AppendFormat("parentId=\"{0}\" ", product.ParentId);
                sb.AppendFormat("sortOrder=\"{0}\" ", product.Node.SortOrder);
                sb.AppendFormat("createDate=\"{0}\" ", product.Node.CreateDate);
                sb.AppendFormat("updateDate=\"{0}\" ", product.Node.UpdateDate);
                sb.AppendFormat("nodeName=\"{0}\" ", product.Name);
                sb.AppendFormat("urlName=\"{0}\" ", product.LocalizedUrl);
                sb.AppendFormat("path=\"{0}\">", document.Path);

                sb.AppendFormat("<path>{0}</path>", document.Path);
                sb.AppendFormat("<number><![CDATA[{0}]]></number>", product.Number);
                sb.AppendFormat("<urlName>{0}</urlName>", product.LocalizedUrl);
                sb.AppendFormat("<title><![CDATA[{0}]]></title>", product.Title);
                sb.AppendFormat("<shortDescription><![CDATA[{0}]]></shortDescription>", product.ShortDescription);
                sb.AppendFormat("<longDescription><![CDATA[{0}]]></longDescription>", product.LongDescription);

                #region tags
                sb.Append("<tags>");

                foreach (string tag in product.Tags)
                {
                    if (!string.IsNullOrEmpty(tag))
                    {
                        sb.Append("<tag><![CDATA[" + tag + "]]></tag>");
                    }
                }

                sb.Append("</tags>");
                #endregion

                #region images
                sb.Append("<images>");

                foreach (var image in product.Images)
                {
                    sb.Append(string.Format("<image id=\"{0}\">{1}</image>", image.Id, image.RelativePathToFile));
                }

                sb.Append("</images>");
                #endregion

                #region files
                sb.Append("<files>");

                foreach (var file in product.Files)
                {
                    sb.Append(string.Format("<file id=\"{0}\">{1}</file>", file.Id, file.RelativePathToFile));
                }

                sb.Append("</files>");
                #endregion

                AddCustomProperties(productDocumentType, product.Node, sb);

                sb.Append("</product>");

                return sb.ToString();
            }

            return string.Empty;
        }

        private static string GetVirtualProductXml(Document document, bool includeSubnodes)
        {
            var virtualProduct = new VirtualProduct(document.Id);
            var product = new Product(virtualProduct.VirtualizedProductId);

            if (product.Enabled == true)
            {
                var sb = new StringBuilder("<product ");
                sb.AppendFormat("id=\"{0}\" ", document.Id);
                sb.AppendFormat("virtualizedId=\"{0}\" ", product.Id);
                sb.AppendFormat("parentId=\"{0}\" ", document.ParentId);
                sb.AppendFormat("sortOrder=\"{0}\" ", document.sortOrder);
                sb.AppendFormat("createDate=\"{0}\" ", document.CreateDateTime);
                sb.AppendFormat("updateDate=\"{0}\" ", document.UpdateDate);
                sb.AppendFormat("nodeName=\"{0}\" ", product.Name);
                sb.AppendFormat("urlName=\"{0}\" ", product.LocalizedUrl);
                sb.AppendFormat("path=\"{0}\">", document.Path);

            

                sb.Append("</product>");

                return sb.ToString();
            }

            return string.Empty;
        }

        private static string GetPricingXml(int pricingId, bool dontCheckDiscount)
        {
            return GetPricingXml(pricingId, null, dontCheckDiscount);
        }

        private static string GetPricingXml(int pricingId, Document document, bool dontCheckDiscount)
        {
            try
            {
                var pricing = new Pricing(pricingId);

                if (pricing.Enabled == true)
                {
                    if (document == null)
                    {
                        document = new Document(true, pricingId);
                    }

                    var sb = new StringBuilder("<pricing ");
                    sb.AppendFormat("id=\"{0}\" ", pricing.Id);
                    sb.AppendFormat("parentId=\"{0}\" ", pricing.ParentId);
                    sb.AppendFormat("sortOrder=\"{0}\" ", pricing.Node.SortOrder);
                    sb.AppendFormat("createDate=\"{0}\" ", pricing.Node.CreateDate);
                    sb.AppendFormat("updateDate=\"{0}\" ", pricing.Node.UpdateDate);
                    sb.AppendFormat("nodeName=\"{0}\" ", pricing.Name);
                    sb.AppendFormat("path=\"{0}\">", pricing.Path);

                    sb.AppendFormat("<path>{0}</path>", document.Path);
                    sb.AppendFormat("<title><![CDATA[{0}]]></title>", pricing.Title);
                    sb.AppendFormat("<number><![CDATA[{0}]]></number>", pricing.Number);
                    sb.AppendFormat("<description><![CDATA[{0}]]></description>", pricing.Description);
                    sb.AppendFormat("<totalItemsOrdered>{0}</totalItemsOrdered>", pricing.TotalItemsOrdered);

                    if (pricing.IsStockEnabled)
                    {
                        sb.AppendFormat("<stock enabled=\"true\">{0}</stock>", pricing.Stock);
                    }
                    else
                    {
                        sb.Append("<stock enabled=\"false\" />");
                    }

                    #region pricing variants
                    sb.Append("<variants>");
                    sb.Append("<variantGroups>");

                    var variantGroups = new List<string>();

                    foreach (var variant in pricing.Variants)
                    {
                        var variantXml = GetPricingVariantXml(variant.Id, false);

                        if (!variantGroups.Contains(variant.PricingVariantGroup))
                        {
                            variantGroups.Add(variant.PricingVariantGroup);
                        }
                    }

                    foreach (var variantGroup in variantGroups)
                    {
                        sb.Append("<variantGroup>");

                        sb.Append(string.Format("<title><![CDATA[{0}]]></title>", variantGroup));

                        sb.Append("<variants>");

                        foreach (var variant in pricing.Variants)
                        {
                            if (variant.PricingVariantGroup == variantGroup)
                            {
                                sb.Append(GetPricingVariantXml(variant.Id, false));
                            }
                        }

                        sb.Append("</variants>");
                        sb.Append("</variantGroup>");
                    }

                    sb.Append("</variantGroups>");
                    sb.Append("</variants>");
                    #endregion

                    sb.Append(string.Format("<priceNoTax><![CDATA[{0}]]></priceNoTax>", pricing.CurrentPriceWithoutTax.ToString("C")));
                    sb.Append(string.Format("<taxPercentage><![CDATA[{0}]]></taxPercentage>", pricing.TaxPercentage.ToString()));
                    sb.Append(string.Format("<taxAmount><![CDATA[{0}]]></taxAmount>", pricing.Tax.ToString("C")));

                    sb.Append(string.Format("<priceWithTax><![CDATA[{0}]]></priceWithTax>", pricing.CurrentPriceWithTax.ToString("C")));
                    sb.Append(string.Format("<priceWithTaxInCents>{0}</priceWithTaxInCents>", (pricing.CurrentPriceWithTax * 100).ToString("0")));

                    sb.Append(string.Format("<priceWithoutTax><![CDATA[{0}]]></priceWithoutTax>", pricing.CurrentPriceWithoutTax.ToString("C")));
                    sb.Append(string.Format("<priceWithoutTaxInCents>{0}</priceWithoutTaxInCents>", (pricing.CurrentPriceWithoutTax * 100).ToString("0")));

                    sb.Append(string.Format("<originalPriceWithTax><![CDATA[{0}]]></originalPriceWithTax>", pricing.PriceWithTax.ToString("C")));
                    sb.Append(string.Format("<originalPriceWithTaxInCents>{0}</originalPriceWithTaxInCents>", (pricing.PriceWithTax * 100).ToString("0")));

                    sb.Append(string.Format("<originalPriceWithoutTax><![CDATA[{0}]]></originalPriceWithoutTax>", pricing.PriceWithoutTax.ToString("C")));
                    sb.Append(string.Format("<originalPriceWithoutTaxInCents>{0}</originalPriceWithoutTaxInCents>", (pricing.PriceWithoutTax * 100).ToString("0")));

                    sb.Append(string.Format("<discountedPriceWithTax><![CDATA[{0}]]></discountedPriceWithTax>", pricing.DiscountedPriceWithTax.ToString("C")));
                    sb.Append(string.Format("<discountedPriceWithTaxInCents>{0}</discountedPriceWithTaxInCents>", (pricing.DiscountedPriceWithTax * 100).ToString("0")));

                    sb.Append(string.Format("<discountedPriceWithoutTax><![CDATA[{0}]]></discountedPriceWithoutTax>", pricing.DiscountedPriceWithoutTax.ToString("C")));
                    sb.Append(string.Format("<discountedPriceWithoutTaxInCents>{0}</discountedPriceWithoutTaxInCents>", (pricing.DiscountedPriceWithoutTax * 100).ToString("0")));

                    AddCustomProperties(pricingDocumentType, pricing.Node, sb);

                    if (!dontCheckDiscount)
                    {
                        if (pricing.IsDiscounted)
                        {
                            sb.Append(string.Format("<sale id=\"{0}\">", pricing.PricingDiscount.Node.Id));
                            sb.Append(string.Format("<title><![CDATA[{0}]]></title>", pricing.PricingDiscount.Title));
                            sb.Append(string.Format("<description><![CDATA[{0}]]></description>", pricing.PricingDiscount.Description));
                            sb.Append(string.Format("<amount>{0}</amount>", pricing.PricingDiscount.DiscountAmount.ToString("C")));
                            sb.Append(string.Format("<percentage>{0}</percentage>", pricing.PricingDiscount.DiscountPercentage.ToString()));
                            sb.Append("</sale>");
                        }
                    }

                    sb.Append("<ranges>");
                    if (pricing.Ranges.Count > 0)
                    {
                        foreach (var range in pricing.Ranges)
                        {
                            sb.Append(string.Format("<range from=\"{0}\" to=\"{1}\" amount=\"{2}\" />", range.From, range.To, range.Amount.ToString("N")));
                        }
                    }

                    sb.Append("</ranges>");

                    sb.Append("</pricing>");

                    return sb.ToString();
                }
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, "GetPricingXml " + ex.ToString());
            }

            return string.Empty;
        }

        private static string GetPricingVariantXml(int pricingVariantId, bool dontCheckDiscount)
        {
            try
            {
                var pricingVariant = new PricingVariant(pricingVariantId);

                if (pricingVariant.Enabled == true)
                {
                    var sb = new StringBuilder();

                    sb.Append(string.Format("<variant id=\"{0}\">", pricingVariant.Id));
                    sb.Append(string.Format("<title><![CDATA[{0}]]></title>", pricingVariant.Title));

                    //if (!dontCheckDiscount)
                    //{
                    //if (variant.IsDiscounted)
                    //{
                    //    sb.Append(string.Format("<addedValue>{0}</addedValue>", variant.DiscountedAmount.ToString("C")));
                    //    sb.Append(string.Format("<sale id=\"{0}\">", variant.VariantSale.Node.Id));
                    //    sb.Append(string.Format("<title><![CDATA[{0}]]></title>", variant.VariantSale.Title));
                    //    sb.Append(string.Format("<description><![CDATA[{0}]]></description>", variant.VariantSale.Description));
                    //    sb.Append(string.Format("<amount>{0}</amount>", variant.VariantSale.DiscountAmount.ToString("C")));
                    //    sb.Append(string.Format("<percentage>{0}</percentage>", variant.VariantSale.DiscountPercentage.ToString()));
                    //    sb.Append("</sale>");
                    //}
                    //else
                    //{
                    //sb.Append(string.Format("<addedValue>{0}</addedValue>", variant.AddedValue.ToString("C")));
                    //sb.Append(string.Format("<addedWeight>{0}</addedWeight>", variant.AddedWeight.ToString("C")));
                    //}
                    //}
                    //else
                    //{
                    sb.Append(string.Format("<addedValue><![CDATA[{0}]]></addedValue>", pricingVariant.AddedValue.ToString("C")));
                    sb.Append(string.Format("<addedWeight>{0}</addedWeight>", pricingVariant.AddedWeight.ToString("C")));
                    sb.AppendFormat("<totalItemsOrdered>{0}</totalItemsOrdered>", pricingVariant.TotalItemsOrdered);
                    if (pricingVariant.IsStockEnabled)
                    {
                        sb.AppendFormat("<stock enabled=\"true\">{0}</stock>", pricingVariant.Stock);
                    }
                    else
                    {
                        sb.Append("<stock enabled=\"false\" />");
                    }
                    //}

                    AddCustomProperties(pricingVariantDocumentType, pricingVariant.Node, sb);

                    sb.Append("</variant>");

                    return sb.ToString();
                }
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, "getpricingvariantxml " + ex);
            }

            return string.Empty;
        }

        public void PersistXmlToFile()
        {
            PersistXmlToFile(_xmlContent);
        }

        /// <summary>
        /// Persist a XmlDocument to the Disk Cache
        /// </summary>
        /// <param name="xmlDoc"></param>
        internal void PersistXmlToFile(XmlDocument xmlDoc)
        {
            lock (_readerWriterSyncLock)
            {
                if (xmlDoc != null)
                {
                    Trace.Write(string.Format("Saving content to disk on thread '{0}' (Threadpool? {1})", Thread.CurrentThread.Name, Thread.CurrentThread.IsThreadPoolThread.ToString()));

                    // Moved the user into a variable and avoided it throwing an error if one can't be loaded (e.g. empty / corrupt db on initial install)
                    User staticUser = null;
                    try
                    {
                        staticUser = User.GetCurrent();
                    }
                    catch
                    { }

                    try
                    {
                        Stopwatch stopWatch = Stopwatch.StartNew();

                        DeleteXmlCache();

                        // Try to create directory for cache path if it doesn't yet exist
                        if (!System.IO.File.Exists(UmbracoXmlDiskCacheFileName) && !Directory.Exists(Path.GetDirectoryName(UmbracoXmlDiskCacheFileName)))
                        {
                            // We're already in a try-catch and saving will fail if this does, so don't need another
                            Directory.CreateDirectory(UmbracoXmlDiskCacheFileName);
                        }

                        xmlDoc.Save(UmbracoXmlDiskCacheFileName);
                    }
                    catch (Exception ee)
                    {
                        // If for whatever reason something goes wrong here, invalidate disk cache
                        DeleteXmlCache();

                        Log.Add(LogTypes.Error, staticUser, -1, string.Format("Xml wasn't saved: {0}", ee));
                    }
                }
            }
        }

        internal const string PersistenceFlagContextKey = "vnc38ykjnkjdnk2jt98ygkx222"; // "vnc38ykjnkjdnk2jt98ygkxjng";
        /// <summary>
        /// Marks a flag in the HttpContext so that, upon page execution completion, the Xml cache will
        /// get persisted to disk. Ensure this method is only called from a thread executing a page request
        /// since umbraco.presentation.requestModule is the only monitor of this flag and is responsible
        /// for enacting the persistence at the PostRequestHandlerExecute stage of the page lifecycle.
        /// </summary>
        private void QueueXmlForPersistence()
        {
            /* Alex Norcliffe 2010 06 03 - removing all launching of ThreadPool threads, instead we just 
             * flag on the context that the Xml should be saved and an event in the requestModule
             * will check for this and call PersistXmlToFile() if necessary */
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Application.Lock();
                if (HttpContext.Current.Application[PersistenceFlagContextKey] != null)
                    HttpContext.Current.Application.Add(PersistenceFlagContextKey, null);
                HttpContext.Current.Application[PersistenceFlagContextKey] = DateTime.Now;
                HttpContext.Current.Application.UnLock();
            }
            else
            {
                //// Save copy of content
                if (UmbracoSettings.CloneXmlCacheOnPublish)
                {
                    XmlDocument xmlContentCopy = CloneXmlDoc(_xmlContent);

                    ThreadPool.QueueUserWorkItem(
                        delegate { PersistXmlToFile(xmlContentCopy); });

                }
                else
                    ThreadPool.QueueUserWorkItem(
                        delegate { PersistXmlToFile(); });
            }
        }

        public bool IsXmlQueuedForPersistenceToFile
        {
            get
            {
                if (HttpContext.Current != null)
                {
                    var val = HttpContext.Current.Application[PersistenceFlagContextKey] != null;
                    if (val != false)
                    {
                        DateTime persistenceTime = DateTime.MinValue;
                        try
                        {
                            persistenceTime = (DateTime)HttpContext.Current.Application[PersistenceFlagContextKey];

                            if (persistenceTime > GetCacheFileUpdateTime())
                            {
                                return true;
                            }
                            else
                            {
                                HttpContext.Current.Application.Lock();
                                HttpContext.Current.Application[PersistenceFlagContextKey] = null;
                                HttpContext.Current.Application.UnLock();
                            }
                        }
                        catch
                        {
                            // Nothing to catch here - we'll just persist
                        }
                    }
                }
                return false;
            }
        }

        internal DateTime GetCacheFileUpdateTime()
        {
            if (System.IO.File.Exists(UmbracoXmlDiskCacheFileName))
            {
                return new FileInfo(UmbracoXmlDiskCacheFileName).LastWriteTime;
            }

            return DateTime.MinValue;
        }

        /// <summary>
        /// Make a copy of a XmlDocument
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        private XmlDocument CloneXmlDoc(XmlDocument xmlDoc)
        {
            // Save copy of content
            XmlDocument xmlCopy = new XmlDocument();
            xmlCopy.LoadXml(xmlDoc.OuterXml);

            return xmlCopy;
        }
        #endregion

    }
}
