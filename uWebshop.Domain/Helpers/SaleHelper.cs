﻿using System.Collections.Generic;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	public static class SaleHelper
	{
		/// <summary>
		/// Gets the custom sales for pricing.
		/// </summary>
		/// <param name="product">The product.</param>
		/// <returns></returns>
		public static List<CustomSale> GetCustomSalesForPricing(Product product)
		{
			var sales = new List<CustomSale>();

			List<ISale> interfaces = InterfaceHelper.GetInterfaces<ISale>();

			if (interfaces != null)
			{
				foreach (ISale iSale in interfaces)
				{
					CustomSale sale = iSale.GetSale(product);

					if (sale != null)
					{
						sales.Add(sale);
					}
				}
			}

			return sales;
		}
	}
}