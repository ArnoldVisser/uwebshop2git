﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using uWebshop.Common;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	///     Helper class with domain related functions
	/// </summary>
	public class DomainHelper
	{
		/// <summary>
		/// Gets all categories.
		/// </summary>
		/// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static IEnumerable<Category> GetAllCategories(bool includeDisabled = false, string storeAlias = null)
		{
			if (String.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}
			return IO.Container.Resolve<ICategoryService>().GetAll(storeAlias, includeDisabled);
			//return GetObjectsByAlias<Category>(Category.NodeAlias).Where(cat => includeDisabled || !cat.Disabled);
		}

		/// <summary>
		///     Returns a list of all products
		/// </summary>
		/// <returns>List of products</returns>
		public static IEnumerable<Product> GetAllProducts(bool includeDisabled = false, string storeAlias = null)
		{
			if (String.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}

			return IO.Container.Resolve<IProductService>().GetAll(storeAlias, includeDisabled);
		}

		/// <summary>
		///     Returns a list of all product variants
		/// </summary>
		/// <returns>List of product variants</returns>
		public static IEnumerable<ProductVariant> GetAllProductVariants(bool includeDisabled = false, string storeAlias = null)
		{
			if (String.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}

			return IO.Container.Resolve<IProductVariantService>().GetAll(storeAlias, includeDisabled);
		}

		/// <summary>
		///     Returns a list of all products
		/// </summary>
		/// <returns>List of products</returns>
		public static IEnumerable<Store> GetAllStores()
		{
			return IO.Container.Resolve<IStoreService>().GetAllStores();
		}

		/// <summary>
		///     Returns a list of all products
		/// </summary>
		/// <returns>List of products</returns>
		public static Store StoreById(int id)
		{
			return StoreHelper.GetById(id);
		}

		/// <summary>
		///     Get a category by it's NodeId
		/// </summary>
		/// <param name="categoryId">NodeId of the category</param>
		/// <returns>Category Object</returns>
		public static Category GetCategoryById(int categoryId)
		{
			// todo: implement as dictionary/hashmap/oid
			return IO.Container.Resolve<ICategoryService>().GetById(categoryId, StoreHelper.CurrentStoreAlias);
			return GetAllCategories().FirstOrDefault(node => node.Id == categoryId);
		}

		/// <summary>
		/// Get a category by it's NodeId
		/// </summary>
		/// <param name="categoryId">NodeId of the category</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns>
		/// Category Object
		/// </returns>
		public static Category GetCategoryById(int categoryId, string storeAlias)
		{
			if (String.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}
			// todo: implement as dictionary/hashmap/oid
			return IO.Container.Resolve<ICategoryService>().GetById(categoryId, storeAlias);
			//return GetAllCategories(false, storeAlias).FirstOrDefault(node => node.Id == categoryId);
		}

		/// <summary>
		/// Gets the category with unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("use GetCategoryById")]
		public static Category GetCategoryWithId(int id)
		{
			return GetCategoryById(id);
		}

		/// <summary>
		///     Get random object by nodeTypeAlias
		/// </summary>
		/// <param name="nodeTypeAlias"></param>
		/// <param name="count"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static IEnumerable<T> GetRandomObjectsByAlias<T>(string nodeTypeAlias, int count)
		{
			IEnumerable<T> objects = GetObjectsByAlias<T>(nodeTypeAlias);

			return RandomizeGenericList(objects, count);
		}

		private static IEnumerable<T> RandomizeGenericList<T>(IEnumerable<T> list, int count)
		{
			List<T> workList = list.ToList();
			var randomList = new List<T>();
			var random = new Random();

			while (workList.Count > 0)
			{
				int idx = random.Next(0, workList.Count);

				randomList.Add(workList[idx]);

				workList.RemoveAt(idx);
			}

			return randomList.Take(count);
		}

		/// <summary>
		///     Get object by nodeTypeAlias
		/// </summary>
		/// <param name="nodeTypeAlias"></param>
		/// <param name="storeAlias"></param>
		/// <param name="startNodeId"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static IEnumerable<T> GetObjectsByAlias<T>(string nodeTypeAlias, string storeAlias = null, int startNodeId = 0)
		{
			return IO.Container.Resolve<ICMSEntityRepository>().GetObjectsByAlias<T>(nodeTypeAlias, storeAlias, startNodeId);
		}

		/// <summary>
		/// Gets the products.
		/// </summary>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <returns></returns>
		public static IEnumerable<Product> GetProducts(int categoryId)
		{
			return GetAllProducts().Where(product => product.ParentId == categoryId || product.HasCategories && product.Categories.Any(cat => cat.Id == categoryId));
		}

		internal static IEnumerable<int> ParseIntegersFromUwebshopProperty(string values)
		{
			if (!String.IsNullOrWhiteSpace(values) && !values.Any(Char.IsLetter))
			{
				if (values.Contains(","))
				{
					return values.Split(',').Select(Int32.Parse);
				}
				if (values.Contains(" "))
				{
					return values.Split(' ').Select(Int32.Parse);
				}
				return new List<int> {Int32.Parse(values)};
			}
			return Enumerable.Empty<int>();
		}

		/// <summary>
		///     Convert the property to a multistore property usting the current store from the order or website
		/// </summary>
		/// <param name="propertyAlias"></param>
		/// <returns></returns>
		public static string MultiStorePropertyAlias(string propertyAlias)
		{
			string storeAlias = StoreHelper.GetCurrentStore().Alias;
			return !String.IsNullOrEmpty(storeAlias) ? StoreHelper.CreateMultiStorePropertyAlias(propertyAlias, storeAlias) : propertyAlias;
		}

		/// <summary>
		///     Serialize an object to XMLString
		/// </summary>
		/// <param name="obj"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static string SerializeObjectToXmlString<T>(T obj)
		{
			CultureInfo currentThread = Thread.CurrentThread.CurrentCulture;
			CultureInfo currentUIThread = Thread.CurrentThread.CurrentUICulture;

			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

			var xmls = new XmlSerializer(typeof (T));
			using (var ms = new MemoryStream())
			{
				var settings = new XmlWriterSettings {Encoding = new UTF8Encoding(), Indent = true, IndentChars = "\t", NewLineChars = Environment.NewLine, ConformanceLevel = ConformanceLevel.Document,};

				using (XmlWriter writer = XmlWriter.Create(ms, settings))
				{
					xmls.Serialize(writer, obj);
				}

				string value = Encoding.UTF8.GetString(ms.ToArray());

				Thread.CurrentThread.CurrentCulture = currentThread;
				Thread.CurrentThread.CurrentUICulture = currentUIThread;

				return value;
			}
		}

		/// <summary>
		///     Deserialize XMLstring to Object
		/// </summary>
		/// <param name="xmlString"></param>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static T DeserializeXmlStringToObject<T>(string xmlString)
		{
			CultureInfo currentThread = Thread.CurrentThread.CurrentCulture;
			CultureInfo currentUIThread = Thread.CurrentThread.CurrentUICulture;

			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");

			var xmls = new XmlSerializer(typeof (T));

			using (var ms = new MemoryStream(Encoding.UTF8.GetBytes(xmlString)))
			{
				var value = (T) xmls.Deserialize(ms);

				Thread.CurrentThread.CurrentCulture = currentThread;
				Thread.CurrentThread.CurrentUICulture = currentUIThread;

				return value;
			}
		}

		/// <summary>
		/// Gets the product by unique identifier.
		/// </summary>
		/// <param name="productId">The product unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static Product GetProductById(int productId, string storeAlias = null)
		{
			if (String.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}
			return IO.Container.Resolve<IProductService>().GetById(productId, storeAlias);
		}

		/// <summary>
		/// Gets the product variant by unique identifier.
		/// </summary>
		/// <param name="productVariantId">The product variant unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static ProductVariant GetProductVariantById(int productVariantId, string storeAlias = null)
		{
			if (String.IsNullOrEmpty(storeAlias))
			{
				storeAlias = StoreHelper.CurrentStoreAlias;
			}
			return IO.Container.Resolve<IProductVariantService>().GetById(productVariantId, storeAlias);
		}

		internal static uWebshopEntity GetUwebShopNode()
		{
			return GetObjectsByAlias<uWebshopEntity>("uWebshop", Constants.NonMultiStoreAlias).FirstOrDefault();
		}

		internal static string BuildUrlFromTemplate(string productUrl, IUwebshopEntity entity)
		{
			if (String.IsNullOrEmpty(productUrl)) return null;
			var productUrlPropertyAliasses = productUrl.Split(',');

			string urlFormat = String.Empty;
			foreach (var productUrlPropertyAlias in productUrlPropertyAliasses)
			{
				if (productUrlPropertyAlias.StartsWith("#"))
				{
					urlFormat += productUrlPropertyAlias.Skip(1);
				}
				else
				{
					urlFormat += StoreHelper.GetMultiStoreItem(entity.Id, productUrlPropertyAlias); // todo: this creates extra examine queries
				}
			}
			return urlFormat;
		}
	}
}