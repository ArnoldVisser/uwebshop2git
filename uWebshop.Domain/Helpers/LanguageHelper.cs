﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.web;
using umbraco.presentation.nodeFactory;

namespace SuperSimpleWebshop.Domain.Helpers
{
    /// <summary>
    /// Helper class with language related functions
    /// </summary>
    public static partial class LanguageHelper
    {
        /// <summary>
        /// Returns a single language
        /// </summary>
        /// <param name="countryCode">Code of the country</param>
        /// <returns>Language</returns>
        public static Language GetLanguageByCountryCode(string countryCode)
        {
            try
            {
                return DomainHelper.GetObjectsByAlias<Language>("sswsLanguageNode").Where(x => x.CountryCode == countryCode).FirstOrDefault();
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the current language alias
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentLanguageAlias()
        {
            string currentAlias = GetPropertyValueRecursive(Node.GetCurrent(), "shopAlias");
            
            return currentAlias.ToUpper();
        }

        public static string GetPropertyValueRecursive(this Node node, string propertyAlias)
        {
            if (node.GetProperty(propertyAlias) != null && !string.IsNullOrEmpty(node.GetProperty(propertyAlias).Value))
                return node.GetProperty(propertyAlias).Value;

            if (node.Parent != null)
                return GetPropertyValueRecursive(node.Parent, propertyAlias);

            return string.Empty;
        }

        /// <summary>
        /// Returns a list of all languages
        /// </summary>
        /// <returns>List of languages</returns>
        public static List<Language> GetAllLanguages()
        {
            var nodes = DomainHelper.GetObjectsByAlias<Node>("sswsLanguageNode").ToList();

            var languages = new List<Language>();
            foreach (var node in nodes)
            {
                languages.Add(new Language(node.Id));
            }

            return languages;
        }

        public static Language GetCurrentLanguage()
        {
            var currentNodeId = Node.GetCurrent().Id;
            var domains = umbraco.library.GetCurrentDomains(currentNodeId);

            if (domains != null && domains.Length > 0)
            {
                return new Language(domains.First().RootNodeId);
            }

            return null;
        }

        public static string GetLocalizedProductURL(int nodeId, string LanguageAlias)
        {
            try
            {
                if (nodeId > 0)
                {
                    var product = new Product(nodeId);

                    string prodURL = "/" + LanguageAlias.ToLower() + "/";
                    prodURL += library.GetDictionaryItem("RewritePathFirst").ToLower().Replace(" ", "-") + "/";

                    var parentNode = product.Node.Parent;
                    var path = "";

                    while (parentNode.NodeTypeAlias == "sswsCategory")
                    {
                        Category subCat = new Category(parentNode.Id);
                        path = subCat.LocalizedUrl.ToLower().Replace(" ", "-") + "/" + path;
                        parentNode = parentNode.Parent;
                    }
                    prodURL += path;
                    prodURL += library.GetDictionaryItem("RewritePathSecond").ToLower().Replace(" ", "-") + "/";
                    prodURL += product.LocalizedUrl.ToLower().Replace(" ", "-");
                    return prodURL;
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string GetLocalizedCategoryURL(int nodeId, string LanguageAlias)
        {
            try
            {
                if (nodeId > 0)
                {
                    var category = new Category(nodeId);

                    string catURL = "/" + LanguageAlias.ToLower() + "/";
                    catURL += library.GetDictionaryItem("RewritePathFirst").ToLower().Replace(" ", "-") + "/";

                    var path = "";

                    if (category.Node.Parent.NodeTypeAlias == "sswsCategory")
                    {
                        var parentNode = category.Node.Parent;

                        while (parentNode.NodeTypeAlias == "sswsCategory")
                        {
                            Category subCat = new Category(parentNode.Id);
                            path = subCat.LocalizedUrl.ToLower().Replace(" ", "-") + "/" + path;
                            parentNode = parentNode.Parent;
                        }
                    }

                    catURL += path;
                    catURL += category.LocalizedUrl.ToLower().Replace(" ", "-");
                    return catURL;
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
