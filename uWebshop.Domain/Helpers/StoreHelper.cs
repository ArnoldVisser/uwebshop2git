﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	///     Helper class with language related functions
	/// </summary>
	public static class StoreHelper
	{
		/// <summary>
		/// All stores cache key
		/// </summary>
		public const string AllStoresCacheKey = "AllStoresCacheKey";
		/// <summary>
		/// The current store alias cache key
		/// </summary>
		public const string CurrentStoreAliasCacheKey = "CurrentStoreAliasCacheKey";
		/// <summary>
		/// The current store alias URL cache key
		/// </summary>
		public const string CurrentStoreAliasUrlCacheKey = "CurrentStoreAliasUrlCacheKey";

		private static IStoreService _storeService;

		internal static IStoreService StoreService
		{
			get { return _storeService ?? (_storeService = IO.Container.Resolve<IStoreService>()); }
			set { _storeService = value; }
		}

		/// <summary>
		///     List of all the default uWebshop document types
		/// </summary>
		internal static IEnumerable<string> StoreDependantDocumentTypeAliasList
		{
			get
			{
				return new List<string> {Category.NodeAlias, Product.NodeAlias, ProductVariant.NodeAlias, DiscountProduct.NodeAlias, DiscountOrder.NodeAlias,
					ShippingProvider.NodeAlias, ShippingProviderMethodNode.NodeAlias, PaymentProvider.NodeAlias, PaymentProviderMethodNode.NodeAlias};
			}
		}

		/// <summary>
		/// Gets the current store alias.
		/// </summary>
		/// <value>
		/// The current store alias.
		/// </value>
		public static string CurrentStoreAlias
		{
			get { return StoreService.CurrentStoreAlias(); }
		}

		/// <summary>
		///     Get the current store
		/// </summary>
		/// <returns></returns>
		public static Store GetCurrentStore()
		{
			return StoreService.GetCurrentStore();
		}

		/// <summary>
		///     Returns a list of all languages
		/// </summary>
		/// <returns>List of languages</returns>
		public static IEnumerable<Store> GetAllStores()
		{
			return StoreService.GetAllStores();
		}

		/// <summary>
		/// Gets the store by alias.
		/// </summary>
		/// <param name="alias">The alias.</param>
		/// <returns></returns>
		public static Store GetByAlias(string alias)
		{
			return StoreService.GetByAlias(alias);
		}

		/// <summary>
		/// Gets the store by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		public static Store GetById(int id)
		{
			return StoreService.GetById(id);
		}

		/// <summary>
		/// Gets the store alias URL.
		/// </summary>
		/// <returns></returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use Store.StoreURL")]
		public static string GetStoreAliasUrl()
		{
			return GetCurrentStore().StoreURL;
		}

		/// <summary>
		///     Get the number formatting for the current store
		/// </summary>
		/// <returns></returns>
		public static CultureInfo GetCurrentCulture()
		{
			Store currentStore = GetCurrentStore();

			CultureInfo cultureInfo = CultureInfo.GetCultures(CultureTypes.SpecificCultures).Select(culture => new {culture, region = new RegionInfo(culture.LCID)}).Where(@t => String.Equals(t.region.ISOCurrencySymbol, currentStore.CurrencyCulture, StringComparison.InvariantCultureIgnoreCase)).Select(@t => t.culture).FirstOrDefault();

			cultureInfo.NumberFormat.CurrencyPositivePattern = 2;
			cultureInfo.NumberFormat.CurrencyNegativePattern = 2;

			return cultureInfo;
		}

		/// <summary>
		///     Get the culture based on the currency code of the store
		/// </summary>
		/// <param name="store"></param>
		/// <returns></returns>
		public static CultureInfo GetCultureByCurrencyCode(Store store)
		{
			return CultureInfo.GetCultures(CultureTypes.SpecificCultures).First(culture => String.Equals(new RegionInfo(culture.LCID).ISOCurrencySymbol, store.CurrencyCulture, StringComparison.InvariantCultureIgnoreCase));
		}

		/// <summary>
		/// Gets the localized URL.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <returns></returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("use NiceUrl()")]
		public static string GetLocalizedUrl(int id, int categoryId = 0)
		{
			return GetNiceUrl(id, categoryId);
		}

		/// <summary>
		/// Creates the Url for a catalog item by Id
		/// </summary>
		/// <param name="id">catalog item Id</param>
		/// <param name="categoryId">the Id of the category used to build the url. input 0 will use currentCategory.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		/// If the categoryId is not in the categories list of the product, it will use the first category
		public static string GetNiceUrl(int id, int categoryId = 0, string storeAlias = null)
		{
			return StoreService.GetNiceUrl(id, categoryId, storeAlias);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="oldStoreAlias"></param>
		/// <param name="newStoreAlias"></param>
		public static void RenameStore(string oldStoreAlias, string newStoreAlias)
		{
			StoreService.RenameStore(oldStoreAlias, newStoreAlias);
		}

		/// <summary>
		///     Install a new uWebshop store
		/// </summary>
		/// <param name="storeAlias">the store alias to use</param>
		public static void UnInstallStore(string storeAlias)
		{
			IO.Container.Resolve<IUmbracoDocumentTypeInstaller>().UnInstallStore(storeAlias);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="contentId"></param>
		/// <param name="propertyAlias"></param>
		/// <param name="storeAlias"></param>
		/// <returns></returns>
		public static string GetMultiStoreItem(int contentId, string propertyAlias, string storeAlias = null)
		{
			return IO.Container.Resolve<ICMSApplication>().GetMultiStoreContentProperty(contentId, propertyAlias, storeAlias);
		}

		/// <summary>
		/// Gets the multi store disable.
		/// </summary>
		/// <param name="contentId">The content unique identifier.</param>
		/// <returns></returns>
		public static bool GetMultiStoreDisable(int contentId)
		{
			// todo: test!!
			var value = IO.Container.Resolve<ICMSApplication>().GetMultiStoreContentProperty(contentId, "disable", null, true);
			return value == "1" || value == "true";
			
			//const string propertyAlias = "disable";
			//Func<string, bool> valueCheck = value => value == "1" || value == "true";
			//string multiStoreAlias = DomainHelper.MultiStorePropertyAlias(propertyAlias);
			//SearchResult examineNode = GetNodeFromExamine(documentId, "GetMultiStoreDisable");
			//if (examineNode != null)
			//{
			//	return GetMultiStoreDisableExamine(examineNode);
			//}
			////Log.Instance.LogDebug("GetMultiStoreDisable fallback to Node [slow!]");
			//// todo: move to umbraco proj
			//var node = new Node(documentId);
			//if (node.Name != null)
			//{
			//	IProperty property = node.GetProperty(propertyAlias);
			//	if (property != null && valueCheck(property.Value))
			//	{
			//		return true;
			//	}
			//	property = node.GetProperty(multiStoreAlias);
			//	if (property != null)
			//	{
			//		return valueCheck(property.Value);
			//	}
			//}
			//return false;
		}
		
		internal static bool ReturnCachedFieldOrLoadValueUsingGetMultiStoreExamineAndUpdateField(int entityId, string fieldName, ref bool? field)
		{
			string newValue = GetMultiStoreItem(entityId, fieldName);
			return field ?? (field = newValue == "enable" || newValue == "1" || newValue == "true" || newValue == String.Empty).GetValueOrDefault();
		}

		internal static bool ReturnCachedFieldOrLoadValueAndUpdateField(string newValue, ref bool? field)
		{
			return field ?? (field = newValue == "enable" || newValue == "1" || newValue == "true" || newValue == String.Empty).GetValueOrDefault();
		}

		internal static bool GetMultiStoreDisableExamine(string storeAlias, IPropertyProvider fields)
		{
			const string propertyAlias = "disable";
			Func<string, bool> valueCheck = value => value == "1" || value == "true";
			if (fields.ContainsKey(propertyAlias) && valueCheck(fields.GetStringValue(propertyAlias)))
			{
				return true;
			}
			string multiStoreAlias = CreateMultiStorePropertyAlias(propertyAlias, storeAlias);
			if (fields.ContainsKey(multiStoreAlias))
			{
				return valueCheck(fields.GetStringValue(multiStoreAlias));
			}
			return false;
		}

		internal static string CreateMultiStorePropertyAlias(string propertyAlias, string storeAlias)
		{
			return String.Format("{0}_{1}", propertyAlias, storeAlias);
		}

		internal static string MakeRTEItemPropertyAliasIfApplicable(string propertyAlias)
		{
			return propertyAlias.StartsWith("description") ? "RTEItem" + propertyAlias : propertyAlias;
		}

		internal static string ReadMultiStoreItemFromPropertiesDictionary(string propertyAlias, string storeAlias, IPropertyProvider fields)
		{
			if (!string.IsNullOrEmpty(storeAlias))
			{
				string multiStoreAlias = CreateMultiStorePropertyAlias(propertyAlias, storeAlias);

				if (fields.ContainsKey(multiStoreAlias))
				{
					string value = fields.GetStringValue(multiStoreAlias);

					if (!String.IsNullOrEmpty(value))
					{
						return value;
					}
				}
			}
			if (fields.ContainsKey(propertyAlias))
			{
				string value = fields.GetStringValue(propertyAlias);

				return value ?? string.Empty;
			}
			return string.Empty;
		}

		internal static int GetMultiStoreIntValue(string propertyName, string storeAlias, IPropertyProvider fields, int defaultValue = 0)
		{
			string propertyValue = ReadMultiStoreItemFromPropertiesDictionary(propertyName, storeAlias, fields);
			int intValue;
			return Int32.TryParse(propertyValue, out intValue) ? intValue : defaultValue;
		}

		internal static double GetMultiStoreDoubleValue(string propertyName, string storeAlias, IPropertyProvider fields, double defaultValue = 0)
		{
			string propertyValue = ReadMultiStoreItemFromPropertiesDictionary(propertyName, storeAlias, fields);
			double doubleValue;
			return double.TryParse(propertyValue, out doubleValue) ? doubleValue : defaultValue;
		}

		internal static decimal GetMultiStoreDecimalValue(string propertyName, string storeAlias, IPropertyProvider fields, decimal defaultValue = 0)
		{
			string propertyValue = ReadMultiStoreItemFromPropertiesDictionary(propertyName, storeAlias, fields);
			decimal decimalValue;
			return decimal.TryParse(propertyValue, out decimalValue) ? decimalValue : defaultValue;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		public static int GetMultiStoreStock(int id)
		{
			// todo: multi store validation
			Store currentStore = GetCurrentStore();

			return currentStore != null && currentStore.UseStoreSpecificStock ? UWebshopStock.GetStock(id, currentStore.Alias) : UWebshopStock.GetStock(id);
		}

		/// <summary>
		///     Returns all the products of the category, including any sublevel categories
		/// </summary>
		public static List<Product> GetProductsRecursive(int categoryId, string storeAlias = null)
		{
			var productList = new List<Product>();
			//var category = new Category(categoryId);
			Category category = DomainHelper.GetCategoryById(categoryId, storeAlias);
			GetProductsFromCategory(productList, category);

			return productList;
		}

		private static void GetProductsFromCategory(List<Product> productList, Category category)
		{
			productList.AddRange(category.Products);

			foreach (Category subCategory in category.Categories)
			{
				GetProductsFromCategory(productList, subCategory); // todo: dit kan loopen als er een loop in de Categories relatie zit
			}
		}

		/// <summary>
		///     Returns all the categories of the category, including any sublevel categories
		/// </summary>
		public static List<Category> GetCategoriesRecursive(int categoryId, string storeAlias = null)
		{
			var categoryList = new List<Category>();
			GetCategoriesFromCategory(categoryList, DomainHelper.GetCategoryById(categoryId, storeAlias));
			return categoryList;
		}

		private static void GetCategoriesFromCategory(ICollection<Category> categoryList, Category mainCategory)
		{
			foreach (Category category in mainCategory.Categories.Where(category => categoryList.All(x => x.Id != category.Id)))
			{
				categoryList.Add(category);
			}

			if (!mainCategory.Categories.Any()) return;
			foreach (Category subCategory in mainCategory.Categories)
			{
				GetCategoriesFromCategory(categoryList, subCategory); // todo: dit kan loopen
			}
		}

		/// <summary>
		///     Returns a list of all countries
		/// </summary>
		/// <returns>List of countries</returns>
		public static List<Country> GetAllCountries()
		{
			return IO.Container.Resolve<ICountryRepository>().GetAllCountries();
		}

		/// <summary>
		///     Returns a list of all countries in the provided storeAlias
		/// </summary>
		/// <param name="storeAlias">Alias of current store</param>
		/// <returns>List of countries</returns>
		public static List<Country> GetAllCountries(string storeAlias)
		{
			return IO.Container.Resolve<ICountryRepository>().GetAllCountries(storeAlias);
		}

		/// <summary>
		///     Returns a list of all regions
		/// </summary>
		/// <returns>List of countries</returns>
		public static List<Region> GetAllRegions()
		{
			return GetAllRegionsForStore("none");
		}

		/// <summary>
		///     Returns a list of all regions in the provided shopalias
		/// </summary>
		/// <param name="storeAlias">Alias of current language</param>
		/// <returns>List of countries</returns>
		public static List<Region> GetAllRegionsForStore(string storeAlias)
		{
			var regions = new List<Region>();

			string regionsXml = "/scripts/regions.xml";
			if (storeAlias != "none")
			{
				regionsXml = String.Format("/scripts/regions_{0}.xml", storeAlias);
			}

			if (System.IO.File.Exists(regionsXml))
			{
				XDocument doc = XDocument.Load(HttpContext.Current.Server.MapPath(regionsXml));

				foreach (XElement country in doc.Descendants("country"))
				{
					XAttribute xAttribute = country.Attribute("code");
					if (xAttribute != null)
						regions.Add(new Region {Name = country.Value, Code = xAttribute.Value});
				}
			}

			return regions;
		}
	}
}