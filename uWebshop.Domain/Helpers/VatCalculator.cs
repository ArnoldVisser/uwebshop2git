﻿using System;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	public class VatCalculator
	{
		/// <summary>
		/// Withouts the vat.
		/// </summary>
		/// <param name="withVat">The with vat.</param>
		/// <param name="vat">The vat.</param>
		/// <returns></returns>
		public static int WithoutVat(int withVat, decimal vat)
		{
			return withVat - VatAmountFromWithVat(withVat, vat);
			//return (int)Math.Ceiling(withVat / (100 + vat) * 100); // correct(?)
		}

		/// <summary>
		/// Withes the vat.
		/// </summary>
		/// <param name="withoutVat">The without vat.</param>
		/// <param name="vat">The vat.</param>
		/// <returns></returns>
		public static int WithVat(int withoutVat, decimal vat)
		{
			return (int) Math.Round(withoutVat*(100 + vat)/100);
		}

		/// <summary>
		/// Vats the amount from without vat.
		/// </summary>
		/// <param name="withoutVat">The without vat.</param>
		/// <param name="vat">The vat.</param>
		/// <returns></returns>
		public static int VatAmountFromWithoutVat(int withoutVat, decimal vat)
		{
			return WithVat(withoutVat, vat) - withoutVat;
		}

		/// <summary>
		/// Vats the amount from with vat.
		/// </summary>
		/// <param name="withVat">The with vat.</param>
		/// <param name="vat">The vat.</param>
		/// <returns></returns>
		public static int VatAmountFromWithVat(int withVat, decimal vat)
		{
			return (int) Math.Round(withVat - (withVat/(100m + vat)*100m)); // verified correct
		}
	}
}