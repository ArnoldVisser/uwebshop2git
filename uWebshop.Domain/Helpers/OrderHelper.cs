﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;
using System.Web.Security;
using System.Xml;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Repositories;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	/// Helper class with order related functions
	/// </summary>
	public class OrderHelper
	{
		private static readonly IOrderRepository OrderRepository = new OrderRepository();

		private static IOrderService OrderService
		{
			get { return IO.Container.Resolve<IOrderService>(); }
		}

		/// <summary>
		/// Sets the order cookie.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		public static void SetOrderCookie(OrderInfo orderInfo)
		{
			string cookieName = "OrderId" + (UwebshopConfiguration.Current.ShareBasketBetweenStores ? string.Empty : StoreHelper.GetCurrentStore().Alias);

			int minutesToAdd = 360;

			int incompleteOrderLifetime = IO.Container.Resolve<ISettingsService>().IncompleOrderLifetime;
			if (incompleteOrderLifetime != 0)
			{
				minutesToAdd = incompleteOrderLifetime;
			}

			var cookie = new HttpCookie(cookieName, orderInfo.UniqueOrderId.ToString()) {Expires = DateTime.Now.AddMinutes(minutesToAdd)};

			HttpContext.Current.Response.Cookies.Set(cookie);
		}

		/// <summary>
		/// Sets the order cookie.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		public static void SetCompletedOrderCookie(OrderInfo orderInfo)
		{
			string cookieName = "CompletedOrderId" + (UwebshopConfiguration.Current.ShareBasketBetweenStores ? string.Empty : StoreHelper.GetCurrentStore().Alias);

			var cookie = new HttpCookie(cookieName, orderInfo.UniqueOrderId.ToString()) { Expires = DateTime.Now.AddMinutes(10) };

			HttpContext.Current.Response.Cookies.Set(cookie);
		}

		/// <summary>
		/// Removes the order cookie.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		public static void RemoveOrderCookie(OrderInfo orderInfo)
		{
			string cookieName = "OrderId" + (UwebshopConfiguration.Current.ShareBasketBetweenStores ? string.Empty : StoreHelper.GetCurrentStore().Alias);

			var cookie = new HttpCookie(cookieName, string.Empty) { Expires = DateTime.Now.AddMinutes(-1) };

			HttpContext.Current.Response.Cookies.Set(cookie);
		}

		/// <summary>
		/// Creates the new order from existing.
		/// </summary>
		/// <param name="orderInfoToCopyFrom">The order information automatic copy from.</param>
		/// <returns></returns>
		public static OrderInfo CreateNewOrderFromExisting(OrderInfo orderInfoToCopyFrom)
		{
			OrderInfo orderInfo = OrderService.CreateCopyOfOrder(orderInfoToCopyFrom);

			SetOrderCookie(orderInfo);

			return orderInfo;
		}

		/// <summary>
		/// Get Current Order based on the cookie value of the customer
		/// if status is NOT incopmlete and NOT waiting for paymentprovider and NOT paid it means the order is in 'payment
		/// progress'
		/// action: create a new COPY of the current order so the customer can't change the original order.
		/// if status is NOT incomplete, and status is PAID or status is OfflinePayment, return null.
		/// </summary>
		/// <param name="overruleCopyOrderOnConfirmedOrder">if set to <c>true</c> don't make a new order when the current one is confirmed.</param>
		/// <returns></returns>
		public static OrderInfo GetOrderInfo(bool overruleCopyOrderOnConfirmedOrder = false)
		{
			var uwebshopRequest = UwebshopRequest.Current;
			var orderInfo = uwebshopRequest.OrderInfo;

			if (orderInfo == null)
			{
				var cookieGuid = OrderIdFromOrderIdCookie();

				if (cookieGuid == Guid.Empty)
				{
					return null;
				}

				orderInfo = GetOrderInfo(cookieGuid);

				uwebshopRequest.OrderInfo = orderInfo;
			}

			if (overruleCopyOrderOnConfirmedOrder)
			{
				return orderInfo;
			}

			// if status is NOT incomplete and NOT waiting for paymentprovider and NOT paid it means the order is in 'payment progress'
			// action: create a new COPY of the current order so the customer can't change the original order.
			if (orderInfo.Status != OrderStatus.OfflinePayment && orderInfo.Status != OrderStatus.Incomplete && orderInfo.Status != OrderStatus.WaitingForPaymentProvider && orderInfo.Paid != true)
			{
				orderInfo = OrderService.CreateCopyOfOrder(orderInfo);

				SetOrderCookie(orderInfo);
			}

			// if status is NOT incomplete, and status is PAID or status is OfflinePayment, return null.
			if (orderInfo.Status == OrderStatus.OfflinePayment || orderInfo.Status != OrderStatus.Incomplete && orderInfo.Paid == true)
			{
				return null;
			}

			return orderInfo;
		}

		/// <summary>
		/// Returns the order Guid based on the "OrderId" cookie of the customer
		/// </summary>
		/// <returns></returns>
		public static Guid OrderIdFromOrderIdCookie()
		{
			string cookieName = "OrderId" + (UwebshopConfiguration.Current.ShareBasketBetweenStores ? string.Empty : StoreHelper.GetCurrentStore().Alias);

			HttpCookie orderIdCookie = HttpContext.Current.Request.Cookies[cookieName];

			if (orderIdCookie != null && !String.IsNullOrEmpty(orderIdCookie.Value))
			{
				Guid uniqueOrderId;
				Guid.TryParse(orderIdCookie.Value, out uniqueOrderId);

				return uniqueOrderId;
			}

			return Guid.Empty;
		}

		/// <summary>
		///     Get Current Order based on the cookie value of the customer
		///     if status is NOT incopmlete and NOT waiting for paymentprovider and NOT paid it means the order is in 'payment
		///     progress'
		///     action: create a new COPY of the current order so the customer can't change the original order.
		///     if status is NOT incomplete, and status is PAID or status is OfflinePayment, create a new order
		/// </summary>
		/// <returns></returns>
		public static OrderInfo GetOrCreateOrderInfo()
		{
			OrderInfo orderInfo = GetOrderInfo() ?? CreateOrder();

			UwebshopRequest.Current.OrderInfo = orderInfo;
			return orderInfo;
		}

		/// <summary>
		///     Get order based on the uniqueOrderId
		/// </summary>
		/// <returns></returns>
		public static OrderInfo GetOrderInfo(Guid uniqueOrderId)
		{
			//Log.Instance.LogDebug(DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " OrderHelper.GetOrderInfo >>>>SQL<<<< SELECT orderInfo");
			OrderInfo currentOrderInfo = OrderRepository.GetOrderInfo(uniqueOrderId);

			return currentOrderInfo ?? CreateOrder();
		}

		/// <summary>
		///     Get order based on the transaction Id of the payment provider
		/// </summary>
		/// <returns></returns>
		public static OrderInfo GetOrderInfo(string transactionId)
		{
			return OrderRepository.GetOrderInfo(transactionId);
		}

		/// <summary>
		/// Get newest order based on the customer Id
		/// </summary>
		/// <returns></returns>
		public static OrderInfo GetOrderInfo(int customerId)
		{
			return OrderRepository.GetOrdersFromCustomer(customerId).OrderByDescending(o => o.OrderDate).FirstOrDefault();
		}

		/// <summary>
		/// Get order based on the customer Id
		/// </summary>
		/// <param name="customerId">The customer unique identifier.</param>
		/// <returns></returns>
		public static IEnumerable<OrderInfo> GetOrdersFromCustomer(int customerId)
		{
			return OrderRepository.GetOrdersFromCustomer(customerId);
		}

		/// <summary>
		/// Get order for current customer
		/// </summary>
		/// <param name="uniqueOrderId">The unique order unique identifier.</param>
		/// <returns></returns>
		public static XmlDocument GetOrderInfoXML(Guid uniqueOrderId)
		{
			return OrderRepository.GetOrderInfoXML(uniqueOrderId);
		}

		/// <summary>
		/// Is the order orderable?
		/// </summary>
		/// <param name="orderinfo">The orderinfo.</param>
		/// <returns></returns>
		public static bool Orderable(OrderInfo orderinfo)
		{
			return !OrderService.OrderContainsOutOfStockItem(orderinfo);
		}

		/// <summary>
		/// Check if the order contains items out of stock
		/// </summary>
		/// <param name="uniqueOrderId">The unique order unique identifier.</param>
		/// <returns></returns>
		public static bool OrderContainsOutOfStockItems(Guid uniqueOrderId)
		{
			return OrderService.OrderContainsOutOfStockItem(GetOrderInfo(uniqueOrderId));
		}

		/// <summary>
		/// Returns all the orders from a customer (member) Id;
		/// </summary>
		/// <param name="customerId">The customer unique identifier.</param>
		/// <returns></returns>
		public static IEnumerable<OrderInfo> GetOrdersForCustomer(int customerId)
		{
			return OrderRepository.GetOrdersFromCustomer(customerId);// uWebshopOrders.GetAllOrderInfos("WHERE customerID = " + customerId).Select(data => OrderInfo.CreateOrderInfoFromOrderData(data));
		}

		/// <summary>
		/// Returns all the order from a customer (member) Login name;
		/// </summary>
		/// <param name="loginName">Name of the login.</param>
		/// <returns></returns>
		public static IEnumerable<OrderInfo> GetOrdersForCustomer(string loginName)
		{
			return OrderRepository.GetOrdersFromCustomer(loginName);// uWebshopOrders.GetAllOrderInfos("WHERE customerUsername = '" + loginName + "'").Select(data => OrderInfo.CreateOrderInfoFromOrderData(data));
		}

		internal static string GenerateOrderNumber(Store store, OrderInfo orderInfo, out int lastOrderReferenceNumber)
		{
			lastOrderReferenceNumber = 0;
			string currentHighestOrderNumber = uWebshopOrders.GetHighestOrderNumberForStore(store.Alias, ref lastOrderReferenceNumber);

			Log.Instance.LogDebug("GenerateOrderNumber currentHighestOrderNumber: " + currentHighestOrderNumber + " lastOrderReferenceNumber: " + lastOrderReferenceNumber);

			string orderNumberPrefix = store.OrderNumberPrefix;
			if (lastOrderReferenceNumber <= 0)
			{
				if (!string.IsNullOrEmpty(currentHighestOrderNumber) && currentHighestOrderNumber.Length >= orderNumberPrefix.Length)
					Int32.TryParse(currentHighestOrderNumber.Substring(orderNumberPrefix.Length, currentHighestOrderNumber.Length - orderNumberPrefix.Length), out lastOrderReferenceNumber);
				else
					Int32.TryParse(currentHighestOrderNumber, out lastOrderReferenceNumber);
			}
			lastOrderReferenceNumber++;
			lastOrderReferenceNumber = Math.Max(lastOrderReferenceNumber, store.OrderNumberStartNumber);


			orderInfo.StoreOrderReferenceId = lastOrderReferenceNumber;

			Log.Instance.LogDebug("GenerateOrderNumber lastOrderReferenceNumber: " + lastOrderReferenceNumber);

			return GenerateOrderNumber(store, orderInfo, lastOrderReferenceNumber, orderNumberPrefix);
		}

		private static string GenerateOrderNumber(Store store, OrderInfo orderInfo, int lastOrderReferenceNumber, string orderNumberPrefix)
		{
			if (!string.IsNullOrEmpty(store.OrderNumberTemplate))
			{
				string template = store.OrderNumberTemplate;
				return template.Replace("#orderId#", lastOrderReferenceNumber.ToString()).Replace("#orderIdPadded#", lastOrderReferenceNumber.ToString("0000")).Replace("#storeAlias#", store.Alias).Replace("#day#", orderInfo.ConfirmDate.GetValueOrDefault().Day.ToString()).Replace("#month#", orderInfo.ConfirmDate.GetValueOrDefault().Month.ToString()).Replace("#year#", orderInfo.ConfirmDate.GetValueOrDefault().Year.ToString());
			}

			return string.Format("{0}{1}", orderNumberPrefix, lastOrderReferenceNumber.ToString("0000"));
		}

		internal static void AssignNewOrderNumberToOrder(OrderInfo orderInfo, Store store)
		{
			int newNumber = uWebshopOrders.AssignNewOrderNumberToOrder(orderInfo.DatabaseId, store.Alias, store.OrderNumberStartNumber);
			orderInfo.StoreOrderReferenceId = newNumber;
			orderInfo.OrderNumber = GenerateOrderNumber(store, orderInfo, newNumber, store.OrderNumberPrefix);
		}

		/// <summary>
		///     Create Order, using the current store
		/// </summary>
		/// <returns></returns>
		public static OrderInfo CreateOrder()
		{
			return OrderService.CreateOrder();
		}

		/// <summary>
		///     Create Order
		/// </summary>
		/// <param name="store"></param>
		/// <returns></returns>
		public static OrderInfo CreateOrder(Store store)
		{
			var order = OrderService.CreateOrder(store);
			OrderService.StoreOrderFirstTimeHackishRefactorPlz(order);
			return order;
		}

		/// <summary>
		/// Gets all orders.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		public static IEnumerable<OrderInfo> GetAllOrders(string storeAlias = null)
		{
			//storeAlias = storeAlias ?? StoreHelper.CurrentStoreAlias;
			// todo: build some caching
			// todo: find a way to do filtering with WHERE clauses
			return uWebshopOrders.GetAllOrderInfos( /*filter*/).Select(data => OrderInfo.CreateOrderInfoFromOrderData(data));
			//return new UwebshopDefaultRepository().GetAll<OrderInfo>();
		}

		/// <summary>
		/// Check if VAT should be applied for this order
		/// </summary>
		/// <returns>
		/// The amount with or without vat
		/// </returns>
		public static decimal GetTotalAmountUsingVatCheck()
		{
			return GetOrderInfo().ChargedAmountInCents/100m;
			//return GetTotalAmountUsingVatCheck(OrderIdFromOrderIdCookie());
		}

		/// <summary>
		/// Check if VAT should be applied for this order
		/// </summary>
		/// <param name="uniqueOrderId">The unique order unique identifier.</param>
		/// <returns>
		/// The amount with or without vat
		/// </returns>
		public static decimal GetTotalAmountUsingVatCheck(Guid uniqueOrderId)
		{
			return GetTotalAmountUsingVatCheckInCents(uniqueOrderId)/100m;
		}

		/// <summary>
		/// Gets the total amount using vat check information cents.
		/// </summary>
		/// <param name="uniqueOrderId">The unique order unique identifier.</param>
		/// <returns></returns>
		public static int GetTotalAmountUsingVatCheckInCents(Guid uniqueOrderId)
		{
			return GetOrderInfo(uniqueOrderId).ChargedAmountInCents;
		}

		/// <summary>
		/// Validates the order.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">ValidateOrder without order</exception>
		public static bool ValidateOrder(OrderInfo orderInfo)
		{
			if (orderInfo == null) throw new ArgumentNullException("ValidateOrder without order");

			Log.Instance.LogDebug("ValidateOrder orderInfo.TermsAccepted: " + orderInfo.TermsAccepted);

			return OrderService.ValidateOrder(orderInfo);
		}

		/// <summary>
		/// Validates the customer.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="clearValidation">if set to <c>true</c> [clear validation].</param>
		/// <returns></returns>
		public static bool ValidateCustomer(OrderInfo orderInfo, bool clearValidation)
		{
			return OrderService.ValidateCustomer(orderInfo, clearValidation);
		}

		/// <summary>
		/// Validates the stock.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="clearValidation">if set to <c>true</c> [clear validation].</param>
		/// <returns></returns>
		public static bool ValidateStock(OrderInfo orderInfo, bool clearValidation)
		{
			return OrderService.ValidateStock(orderInfo, clearValidation);
		}

		/// <summary>
		/// Validates the order lines.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="clearValidation">if set to <c>true</c> [clear validation].</param>
		/// <returns></returns>
		public static bool ValidateOrderLines(OrderInfo orderInfo, bool clearValidation)
		{
			return OrderService.ValidateOrderlines(orderInfo, clearValidation);
		}
		
		/// <summary>
		/// Validates the custom validation.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="clearValidation">if set to <c>true</c> [clear validation].</param>
		/// <returns></returns>
		public static bool ValidateCustomValidation(OrderInfo orderInfo, bool clearValidation)
		{
			return OrderService.ValidateCustomValidations(orderInfo, clearValidation);
		}

		/// <summary>
		///     Returns customer orderline value from a specific order and orderline
		/// </summary>
		/// <param name="orderInfo">The order</param>
		/// <param name="orderLineId">Id of the orderline</param>
		/// <param name="alias">the fieldalias</param>
		/// <returns></returns>
		public static string CustomOrderLineValue(OrderInfo orderInfo, int orderLineId, string alias)
		{
			OrderLine orderline = orderInfo.OrderLines.FirstOrDefault(x => x.OrderLineId == orderLineId);
			if (orderline == null) return string.Empty;

			return ExtraInformationValueHelper(alias, orderline.CustomData);
		}

		/// <summary>
		/// Returns the value based
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="alias">The alias.</param>
		/// <returns></returns>
		public static string CustomerInformationValue(OrderInfo orderInfo, string alias)
		{
			return ExtraInformationValueHelper(alias, orderInfo.CustomerInfo.CustomerInformation);
		}

		/// <summary>
		/// Shippings the information value.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="alias">The alias.</param>
		/// <returns></returns>
		public static string ShippingInformationValue(OrderInfo orderInfo, string alias)
		{
			return ExtraInformationValueHelper(alias, orderInfo.CustomerInfo.ShippingInformation);
		}

		/// <summary>
		/// Gets the extra information value with given alias.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="alias">The alias.</param>
		/// <returns></returns>
		public static string ExtraInformationValue(OrderInfo orderInfo, string alias)
		{
			return ExtraInformationValueHelper(alias, orderInfo.CustomerInfo.ExtraInformation);
		}

		private static string ExtraInformationValueHelper(string alias, XElement data)
		{
			if (data != null)
			{
				XElement element = data.Element(alias);
				if (element != null)
				{
					string value = element.Value;
					if (!String.IsNullOrEmpty(value)) return value;
				}
			}
			if (alias == "customerEmail")
			{
				MembershipUser member = Membership.GetUser(); // todo: decouple
				if (member != null) return member.Email;
			}
			ProfileBase profile = HttpContext.Current.Profile;

			if (profile != null && !profile.IsAnonymous)
			{
				try
				{
					object profileAlias = profile[alias];

					if (profileAlias != null) return profileAlias.ToString();
				}
				catch
				{
					return string.Empty;
				}
			}

			return string.Empty;
		}

		/// <summary>
		/// Orders the contains item.
		/// </summary>
		/// <param name="orderinfo">The orderinfo.</param>
		/// <param name="itemIdsToCheck">The item ids automatic check.</param>
		/// <returns></returns>
		public static bool OrderContainsItem(OrderInfo orderinfo, IEnumerable<int> itemIdsToCheck)
		{
			return OrderService.OrderContainsItem(orderinfo, itemIdsToCheck);
		}

		/// <summary>
		/// Gets the applicable order lines.
		/// </summary>
		/// <param name="orderinfo">The orderinfo.</param>
		/// <param name="itemIdsToCheck">The item ids automatic check.</param>
		/// <returns></returns>
		public static List<OrderLine> GetApplicableOrderLines(OrderInfo orderinfo, IEnumerable<int> itemIdsToCheck)
		{
			return OrderService.GetApplicableOrderLines(orderinfo, itemIdsToCheck);
		}

		/// <summary>
		/// Gets the redirect URL after confirmation.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <param name="confirmedNodeId">The confirmed node unique identifier.</param>
		/// <returns></returns>
		public static string GetRedirectUrlAfterConfirmation(OrderInfo orderInfo, int confirmedNodeId)
		{
			PaymentProvider paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);

			string currentDomain = HttpContext.Current.Request.Url.Authority;

			if (paymentProvider != null)
			{
				Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation paymentProvider: " + paymentProvider.Title + " " + paymentProvider.Type);

				switch (paymentProvider.Type)
				{
					case PaymentProviderType.OfflinePaymentAtCustomer:
					case PaymentProviderType.OfflinePaymentInStore:
						if (!string.IsNullOrEmpty(paymentProvider.SuccesNodeId))
						{
							int succesNodeId;

							int.TryParse(paymentProvider.SuccesNodeId, out succesNodeId);

							if (succesNodeId != 0)
							{
								string urlToReturn = IO.Container.Resolve<ICMSApplication>().GetUrlForContentWithId(succesNodeId);

								urlToReturn = string.Format("//{0}{1}", currentDomain, urlToReturn);
								Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation SuccesNodeId: " + urlToReturn);

								return urlToReturn;
							}
						}
						break;
					case PaymentProviderType.OnlinePayment:
						IPaymentProvider iPaymentProvider = PaymentProviderHelper.GetAllIPaymentProviders().First(x => x.GetName().ToLower() == paymentProvider.Name.ToLower());

						if (iPaymentProvider != null)
						{
							IPaymentRequestHandler handler = PaymentProviderHelper.GetAllPaymentRequestHandlers().FirstOrDefault(x => x.GetName().ToLower() == paymentProvider.Name.ToLower());

							if (handler != null)
							{
								try
								{
									handler.CreatePaymentRequest(orderInfo);
								}
								catch (Exception ex)
								{
									Log.Instance.LogError("GetRedirectUrlAfterConfirmation handler.CreatePaymentRequest(orderInfo) FAILED " + ex);
									return "failed";
								}
							}

							switch (iPaymentProvider.GetParameterRenderMethod())
							{
								case PaymentTransactionMethod.Form:
								case PaymentTransactionMethod.Custom:
									if (!string.IsNullOrEmpty(paymentProvider.ControlNodeId))
									{
										int controlNodeId;

										int.TryParse(paymentProvider.ControlNodeId, out controlNodeId);

										if (controlNodeId != 0)
										{
											string urlToReturn = IO.Container.Resolve<ICMSApplication>().GetUrlForContentWithId(controlNodeId);

											urlToReturn = string.Format("//{0}{1}", currentDomain, urlToReturn);

											Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation ControlNodeId: " + urlToReturn);
											return urlToReturn;
										}
									}
									Log.Instance.LogError("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.Form/Custom FAILED");
									return "failed";
								case PaymentTransactionMethod.QueryString:
									if (!string.IsNullOrWhiteSpace(orderInfo.PaymentInfo.Url))
									{
										string value = string.Format("{0}{1}", orderInfo.PaymentInfo.Url, "?" + orderInfo.PaymentInfo.Parameters);

										if (string.IsNullOrEmpty(orderInfo.PaymentInfo.Parameters))
										{
											value = orderInfo.PaymentInfo.Url;
										}

										Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.QueryString: " + value);

										return value;
									}
									Log.Instance.LogError("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.QueryString FAILED");
									return "failed";
								case PaymentTransactionMethod.ServerPost:
									IPaymentRequestHandler paymentRequestHandlers = PaymentProviderHelper.GetAllPaymentRequestHandlers().First(x => x.GetName().ToLower() == paymentProvider.Name.ToLower());

									Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation PaymentProviderHelper.GetAllPaymentRequestHandlers().Count(): " + PaymentProviderHelper.GetAllPaymentRequestHandlers().Count());


									if (paymentRequestHandlers != null)
									{
										Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation paymentRequestHandlers.GetName(): " + paymentRequestHandlers.GetName());

										string nextURL = paymentRequestHandlers.GetPaymentUrl(orderInfo);

										Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.ServerPost nextURL: " + nextURL);


										if (!String.IsNullOrEmpty(nextURL))
										{
											Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.ServerPost nextURL: " + nextURL);

											return nextURL;
										}

										Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.ServerPost OrderInfo.PaymentInfo.Url: " + orderInfo.PaymentInfo.Url);

										if (!String.IsNullOrEmpty(orderInfo.PaymentInfo.Url))
										{
											Log.Instance.LogDebug("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.ServerPost OrderInfo.PaymentInfo.Url: " + orderInfo.PaymentInfo.Url);

											return orderInfo.PaymentInfo.Url;
										}
									}
									Log.Instance.LogError("GetRedirectUrlAfterConfirmation PaymentTransactionMethod.ServerPost FAILED: " + paymentProvider.Name);
									return "failed";
								case PaymentTransactionMethod.WebClient:
									return "webclient";
							}
						}
						Log.Instance.LogError("GetRedirectUrlAfterConfirmation With Online Payment FAILED");
						return "failed";
				}
			}

			if (confirmedNodeId != 0)
			{
				string confirmNodeId = confirmedNodeId != 0 ? IO.Container.Resolve<ICMSApplication>().GetUrlForContentWithId(confirmedNodeId) : string.Empty;

				confirmNodeId = string.Format("//{0}{1}", currentDomain, confirmNodeId);

				return confirmNodeId;
			}

			string fallback = string.Format("//{0}", currentDomain);

			//Log.Instance.LogDebug( "GetRedirectUrlAfterConfirmation fallback: " + fallback);

			return fallback;
		}

		/// <summary>
		/// Update the Stock of all products variants and discounts of an order
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		public static void UpdateStock(OrderInfo orderInfo)
		{
			Log.Instance.LogDebug("UpdateStock Start");

			if (orderInfo.StockUpdated)
			{
				Log.Instance.LogDebug("UpdateStock: Stock Already Updated: Return");
				return;
			}

			var stockService = IO.Container.Resolve<IStockService>();

			// todo: extremely inefficient (many queries) and not safe (no transaction)
			if (orderInfo.StoreInfo.Store != null && orderInfo.StoreInfo.Store.UseStock)
			{
				Log.Instance.LogDebug("UpdateStock: START stock enabled, store not null");

				if (orderInfo.OrderLines == null)
				{
					Log.Instance.LogDebug("UpdateStock: OrderLines NOT Found");
					return;
				}

				foreach (OrderLine orderLine in orderInfo.OrderLines)
				{
					Log.Instance.LogDebug("UpdateStock: OrderLinesFound");
					// if UseVariantStock, only update variant stock
					if (orderLine.ProductInfo.Product == null)
					{
						Log.Instance.LogDebug("UpdateStock: orderLine.ProductInfo.Product == NULL");
					}
					else if (orderLine.ProductInfo.Product.UseVariantStock && orderLine.ProductInfo.Product.Variants.Any())
					{
						Log.Instance.LogDebug("UpdateStock: start product usevariantstock");
						// if variant stockstatus is enabled update stockstatus + ordercount
						foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants.Where(x => x.Variant.StockStatus))
						{
							Log.Instance.LogDebug("UpdateStock (and ordercount): start product usevariantstock - variant.id: " + variant.Id);
							stockService.SetStock(variant.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), true, orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}

						// if variant stockstatus is disabled update ordercount
						foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants.Where(x => !x.Variant.StockStatus))
						{
							Log.Instance.LogDebug("UpdateOrderCount (not stock): start product usevariantstock - variant.id: " + variant.Id);
							stockService.SetOrderCount(variant.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}
					}
						// if UseVariantStock FALSE, update variant stock + product stock
						//if (orderLine.ProductInfo.Product != null && !orderLine.ProductInfo.Product.UseVariantStock)
					else
					{
						Log.Instance.LogDebug("UpdateStock: orderLine.ProductInfo.Product.UseVariantStock == false");
						// if product stockstatus is enabled update stockstatus + ordercount
						if (orderLine.ProductInfo.Product.StockStatus)
						{
							stockService.SetStock(orderLine.ProductInfo.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), true, orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}
							// if product stockstatus is disabled update ordercount
						else
						{
							stockService.SetOrderCount(orderLine.ProductInfo.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}

						// if variant stockstatus is enabled update stockstatus + ordercount
						foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants.Where(x => x.Variant.StockStatus))
						{
							stockService.SetStock(variant.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), true, orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}

						// if variant stockstatus is disabled update ordercount
						foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants.Where(x => !x.Variant.StockStatus))
						{
							stockService.SetOrderCount(variant.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}
					}
				}
				Log.Instance.LogDebug("UpdateStock: END stock enabled, store not null");
			}
				// if stock is disabled on the store 
			else if (orderInfo.StoreInfo.Store != null && !orderInfo.StoreInfo.Store.UseStock)
			{
				Log.Instance.LogDebug("UpdateStock: START stock disabled, store not null");
				foreach (OrderLine orderLine in orderInfo.OrderLines)
				{
					// if UseVariantStock, only update variant ordercount
					if (orderLine.ProductInfo.Product.UseVariantStock)
					{
						foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants)
						{
							stockService.SetOrderCount(variant.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}
					}
						// if UseVariantStock FALSE, update variant ordercount + product ordercount
					else
					{
						stockService.SetOrderCount(orderLine.ProductInfo.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);

						foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants)
						{
							stockService.SetOrderCount(variant.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
						}
					}
				}

				Log.Instance.LogDebug("UpdateStock: END stock disabled, store not null");
			}

			// update the discount stock
			foreach (IOrderDiscount orderDiscount in orderInfo.OrderDiscounts.Where(discount => discount.CounterEnabled))
			{
				Log.Instance.LogDebug("UpdateStock: start discount stock");
				stockService.SetStock(orderDiscount.OriginalId, 1, false, orderInfo.StoreInfo.Store != null && orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
			}
			foreach (var discountProduct in orderInfo.OrderLines.Select(l => DiscountHelper.GetProductDiscount(l.ProductInfo.DiscountId)).Where(discount => discount != null && discount.CounterEnabled))
			{
				Log.Instance.LogDebug("UpdateStock: start discount stock");
				stockService.SetStock(discountProduct.Id, 1, false, orderInfo.StoreInfo.Store != null && orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
			}

			var couponCodeService = IO.Container.Resolve<ICouponCodeService>();
			var discounts = orderInfo.OrderDiscounts.Select(d => d.Id).Concat(orderInfo.OrderLines.Select(l => l.ProductInfo.DiscountId));
			foreach (var discountId in discounts)
			{
				var coupons = couponCodeService.GetAllForDiscount(discountId).Where(c => c.NumberAvailable > 0)
					.Where(c => orderInfo.CouponCodes.Contains(c.CouponCode));
				foreach (var coupon in coupons)
				{
					coupon.NumberAvailable--;
					couponCodeService.Save(coupon);
				}
			}

			Log.Instance.LogDebug("before stockupdated");
			orderInfo.StockUpdated = true;
			Log.Instance.LogDebug("after stockupdated");
			orderInfo.Save();

			Log.Instance.LogDebug("UpdateStock End (after save)");
		}

		/// <summary>
		/// Return the Stock of all products and variants and discounts of an order
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		public static void ReturnStock(OrderInfo orderInfo)
		{
			if (!orderInfo.StockUpdated) return;

			var stockService = IO.Container.Resolve<IStockService>();

			// todo: extremely inefficient (many queries) and not safe (no transaction)
			if (orderInfo.StoreInfo.Store != null && orderInfo.StoreInfo.Store.UseStock)
			{
				foreach (OrderLine orderLine in orderInfo.OrderLines)
				{
					stockService.ReturnStock(orderLine.ProductInfo.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), true, orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);

					foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants.Where(x => x.Variant.StockStatus))
					{
						stockService.ReturnStock(variant.Id, orderLine.ProductInfo.ItemCount.GetValueOrDefault(1), true, orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
					}
				}
			}

			foreach (IOrderDiscount orderDiscount in orderInfo.OrderDiscounts.Where(discount => discount.CounterEnabled))
			{
				stockService.ReturnStock(orderDiscount.OriginalId, 1, false, orderInfo.StoreInfo.Store != null && orderInfo.StoreInfo.Store.UseStoreSpecificStock ? orderInfo.StoreInfo.Alias : string.Empty);
			}

			orderInfo.StockUpdated = false;
			orderInfo.Save();
		}

		internal static void CopyCustomerToShipping(OrderInfo orderInfo)
		{
			if (orderInfo.CustomerInfo.ShippingInformation == null || (orderInfo.CustomerInfo.ShippingInformation != null && !orderInfo.CustomerInfo.ShippingInformation.Descendants().Any()))
			{
				var xDoc = new XDocument(new XElement(CustomerDatatypes.Shipping.ToString()));
				if (orderInfo.CustomerInfo.customerInformation.Root != null)
				{
					foreach (XElement node in orderInfo.CustomerInfo.customerInformation.Root.Elements())
					{
						if (xDoc.Root != null)
							xDoc.Root.Add(new XElement(node.Name.ToString().Replace("customer", "shipping"), new XCData(node.Value)));
					}

					orderInfo.CustomerInfo.shippingInformation = xDoc;
				}

				orderInfo.Save();
			}
		}
	}
}