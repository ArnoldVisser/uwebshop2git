﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public class PaymentConfigHelper
	{
		/// <summary>
		/// Gets the provider.
		/// </summary>
		/// <value>
		/// The provider.
		/// </value>
		public PaymentProvider Provider { get; private set; }
		/// <summary>
		/// Gets the settings.
		/// </summary>
		/// <value>
		/// The settings.
		/// </value>
		public IConfigSettings Settings { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="PaymentConfigHelper"/> class.
		/// </summary>
		/// <param name="paymentProvider">The payment provider.</param>
		public PaymentConfigHelper(PaymentProvider paymentProvider)
		{
			Provider = paymentProvider;
			Settings = new ConfigSettings(Provider);
		}

		/// <summary>
		/// 
		/// </summary>
		class ConfigSettings : IConfigSettings
		{
			private readonly PaymentProvider _provider;
			private readonly IDictionary<string, string> _dictionary = new Dictionary<string, string>();

			internal ConfigSettings(PaymentProvider provider)
			{
				_provider = provider;

				LoadProviderSettings();
			}

			private void LoadProviderSettings()
			{
				if (!System.IO.File.Exists(HttpContext.Current.Server.MapPath("/config/uWebshop/PaymentProviders.config")))
				{
					Log.Instance.LogError("Could not find PaymentProviders.config file in /config/uWebshop folder.");
					return;
				}

				var doc = new XmlDocument();
				doc.Load(HttpContext.Current.Server.MapPath("/config/uWebshop/PaymentProviders.config"));

				XmlNode providerNode = doc.SelectSingleNode(string.Format("//provider[translate(@title, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')='{0}']", _provider.Node.Name.ToLower()));

				if (providerNode == null)
				{
					Log.Instance.LogError(string.Format("Could not find provider with title: {0} in /config/uWebshop/PaymentProviders.config", _provider.Node.Name));

					return;
				}

				if (providerNode.ChildNodes.Count == 0) return;
				foreach (XmlNode node in providerNode.ChildNodes)
				{
					if (!_dictionary.ContainsKey(node.Name.ToLowerInvariant()))
					{
						_dictionary.Add(node.Name.ToLowerInvariant(), node.InnerText);
					}
				}
			}

			/// <summary>
			/// Determines whether the settings contain given key.
			/// </summary>
			/// <param name="key">The key.</param>
			/// <returns></returns>
			public bool ContainsKey(string key)
			{
				return _dictionary.ContainsKey(key);
			}

			/// <summary>
			/// Gets the setting with the specified key.
			/// </summary>
			/// <value>
			/// The setting>.
			/// </value>
			/// <param name="key">The key.</param>
			/// <returns></returns>
			public string this[string key]
			{
				get
				{
					var keyLowercase = key.ToLowerInvariant();
					if (_provider.TestMode)
					{
						var testKey = "test" + keyLowercase;
						if (_dictionary.ContainsKey(testKey)) return _dictionary[testKey];
					}
					return _dictionary[keyLowercase];
				}
			}

			/// <summary>
			/// Gets the number of loaded settings.
			/// </summary>
			public int Count
			{
				get { return _dictionary.Count; }
			}
		}
	}
}