﻿using System;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Common;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	/// 
	/// </summary>
	public class DiscountHelper
	{
		/// <summary>
		///     Returns a single product discount of a product
		/// </summary>
		/// <returns>Sale</returns>
		public static DiscountProduct GetProductDiscount(int itemId, string storeAlias = null)
		{
			return IO.Container.Resolve<IProductDiscountRepository>().GetDiscountByProductId(itemId, storeAlias ?? StoreHelper.CurrentStoreAlias);
		}

		/// <summary>
		///     Returns a single pricing discount of a product
		/// </summary>
		/// <returns>Sale</returns>
		public static IEnumerable<DiscountOrder> GetOrderDiscounts(Guid UniqueOrderId)
		{
			OrderInfo orderinfo = OrderHelper.GetOrderInfo(UniqueOrderId);

			IEnumerable<DiscountOrder> orderDiscounts = DomainHelper.GetObjectsByAlias<DiscountOrder>(DiscountOrder.NodeAlias);

			if (orderDiscounts != null)
			{
				orderDiscounts = orderDiscounts.Where(x => x.Disable == false);
			}

			if (orderDiscounts != null && orderDiscounts.Any())
			{
				var discountsForOrder = new List<DiscountOrder>();

				foreach (DiscountOrder discount in orderDiscounts.Where(x => x.MinimumOrderAmountInCents >= orderinfo.Grandtotal))
				{
					if (!discount.Disable && (discount.CounterEnabled && discount.Counter > 0 || discount.CounterEnabled == false))
					{
						discountsForOrder.Add(discount);
					}
				}

				// todo: onderstaande wordt helemaal niet gebruikt..
				DiscountOrder highestdiscount = discountsForOrder.FirstOrDefault();
				if (highestdiscount != null)
				{
					decimal highestDiscountGrandTotal = orderinfo.Grandtotal - GetDiscountValueForOrder(highestdiscount, orderinfo);

					// todo: while loop
					foreach (DiscountOrder discount in discountsForOrder)
					{
						if ((orderinfo.Grandtotal - GetDiscountValueForOrder(discount, orderinfo) < highestDiscountGrandTotal))
							highestdiscount = discount;
					}
				}

				discountsForOrder.Add(orderDiscounts.First(x => x.DiscountType >= DiscountType.FreeShipping));

				return discountsForOrder;
			}

			return Enumerable.Empty<DiscountOrder>();
		}

		/// <summary>
		/// Gets the discount value for order.
		/// </summary>
		/// <param name="discountOrder">The discount order.</param>
		/// <param name="orderInfo">The order information.</param>
		/// <returns></returns>
		public static int GetDiscountValueForOrder(DiscountOrder discountOrder, OrderInfo orderInfo)
		{
			if (discountOrder.DiscountType == DiscountType.Percentage)
			{
				// todo: ranges
				return (int) Math.Round(discountOrder.DiscountValue*orderInfo.Grandtotal/10000m);
			}

			if (discountOrder.DiscountType == DiscountType.Amount)
			{
				return discountOrder.DiscountValue;
			}
			return 0;
		}
	}
}