﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using uWebshop.Common;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Helpers
{
	/// <summary>
	///     Helper class with payment provider related functions
	/// </summary>
	public static class PaymentProviderHelper
	{
		/// <summary>
		///     Returns a list of all payment providers
		/// </summary>
		/// <returns>List of payment providers</returns>
		public static List<IPaymentProvider> GetAllIPaymentProviders()
		{
			var toReturn = new List<IPaymentProvider>();

			// Get custom providers from dlls
			toReturn.AddRange(GetInterfaces<IPaymentProvider>());

			// Add dummy providers from nodes
			//var paymentProviders = new OrderRepository().GetAllPaymentProviders().Where(x => x.Type == Common.PaymentProviderType.OfflinePaymentAtCustomer || x.Type == Common.PaymentProviderType.OfflinePaymentInStore);

			//foreach (var paymentProvider in paymentProviders)
			//{
			//    toReturn.Add(new DummyPaymentProvider(paymentProvider.Id));
			//}

			return toReturn;
		}

		/// <summary>
		/// Gets all payment providers.
		/// </summary>
		/// <returns></returns>
		public static IEnumerable<PaymentProvider> GetAllPaymentProviders()
		{
			return IO.Container.Resolve<IPaymentProviderService>().GetAll(StoreHelper.CurrentStoreAlias);
		}

		/// <summary>
		///     Returns a list of all payment providers
		/// </summary>
		/// <returns>List of payment request handlers</returns>
		public static List<IPaymentRequestHandler> GetAllPaymentRequestHandlers()
		{
			return GetInterfaces<IPaymentRequestHandler>();
		}

		/// <summary>
		///     Returns a list of all payment providers
		/// </summary>
		/// <returns>List of payment response handlers</returns>
		public static List<IPaymentResponseHandler> GetAllPaymentResponseHandlers()
		{
			return GetInterfaces<IPaymentResponseHandler>();
		}

		/// <summary>
		///     Returns the paymentmethods available for the current order
		///     If the customer has not chosen a country yet, it will use the countrycode from the shop itself to return the
		///     providers.
		/// </summary>
		/// <returns></returns>
		public static List<PaymentProvider> GetPaymentProvidersForOrder(bool useCountry = true)
		{
			return GetPaymentProvidersForOrder(OrderHelper.GetOrCreateOrderInfo(), useCountry);
		}

		/// <summary>
		///     Returns the paymentmethods available for the current order
		///     If the customer has not chosen a country yet, it will use the countrycode from the shop itself to return the
		///     providers.
		/// </summary>
		/// <returns></returns>
		public static List<PaymentProvider> GetPaymentProvidersForOrder(OrderInfo orderInfo, bool useCountry = true)
		{
			if (orderInfo == null)
			{
				Log.Instance.LogWarning("Asking for payment");
				return new List<PaymentProvider>();
			}

			List<PaymentProvider> paymentProviders;
			if (useCountry)
			{
				string paymentCountry = orderInfo.CustomerInfo.CountryCode;
				if (string.IsNullOrEmpty(paymentCountry))
				{
					Store store = StoreHelper.GetCurrentStore();
					paymentCountry = store.CountryCode;
				}
				paymentProviders = GetAllPaymentProviders().Where(paymentProvider => paymentProvider.Type == PaymentProviderType.OfflinePaymentInStore || paymentProvider.Zone.CountryCodes.Contains(paymentCountry)).Distinct().ToList();
			}
			else
			{
				paymentProviders = GetAllPaymentProviders().ToList();
			}

			// Sort payment providers by node sort order in Umbraco backend
			paymentProviders.Sort((a, b) => a.SortOrder.CompareTo(b.SortOrder));

			return paymentProviders;
		}

		private static List<T> GetInterfaces<T>()
		{
			var instances = new List<T>();

			Type targetType = typeof (T);

			foreach (PaymentProvider paymentProvider in GetAllPaymentProviders())
			{
				string dllName = paymentProvider.DLLName;

				if (string.IsNullOrEmpty(dllName))
				{
					dllName = string.Format("uWebshop.Payment.{0}.dll", paymentProvider.Name);
				}

				string assemblyPathName = string.Format(@"{0}\{1}", HttpContext.Current.Server.MapPath("/bin"), dllName);

				if (System.IO.File.Exists(assemblyPathName))
				{
					Assembly assembly = Assembly.LoadFrom(assemblyPathName);

					if (assembly != null)
					{
						Type[] types = assembly.GetExportedTypes();

						foreach (Type type in types)
						{
							if (!targetType.IsAssignableFrom(type)) continue;
							var operation = (T) Activator.CreateInstance(type);

							instances.Add(operation);
						}
					}
				}
			}

			return instances;
		}

		/// <summary>
		/// Gets the payment zones.
		/// </summary>
		/// <param name="countryCode">The country code.</param>
		/// <returns></returns>
		public static List<Zone> GetPaymentZones(string countryCode)
		{
			return IO.Container.Resolve<IZoneService>().GetAllPaymentZones(StoreHelper.CurrentStoreAlias).Where(x => x.CountryCodes.Contains(countryCode)).ToList();
		}

		/// <summary>
		/// Generates the base URL.
		/// </summary>
		/// <param name="nodeId">The node unique identifier.</param>
		/// <returns></returns>
		[Obsolete("Use GenerateBaseUrl() since NodeId is not used anymore")]
		public static string GenerateBaseUrl(int nodeId = 0)
		{
			return GenerateBaseUrl();
			//return IO.Container.Resolve<ICMSContentService>().GenerateDomainUrlForContent(nodeId);
		}

		/// <summary>
		/// Generate the base Url to use for payment providers
		/// based on the currentNodeId or the current CMS node
		/// </summary>
		/// <returns></returns>
		public static string GenerateBaseUrl()
		{
			string http = "http://";
			if (HttpContext.Current.Request.IsSecureConnection)
			{
				http = "https://";
			}

			string currentDomain = HttpContext.Current.Request.Url.Authority;
			var baseUrl = string.Format("{0}{1}", http, currentDomain);

			Log.Instance.LogDebug("GenerateBaseUrl to return" + baseUrl);

			return baseUrl;
		}

		/// <summary>
		/// Gets the request parameter with name.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public static string Request(string name)
		{
			var context = HttpContext.Current;
			return context == null ? string.Empty : context.Request[name];
		}

		/// <summary>
		/// Gets the URL for content with unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		public static string GetUrlForContentWithId(int id)
		{
			return IO.Container.Resolve<ICMSApplication>().GetUrlForContentWithId(id);
		}

		/// <summary>
		/// Gets the current node unique identifier.
		/// </summary>
		/// <returns></returns>
		public static int GetCurrentNodeId()
		{
			return IO.Container.Resolve<ICMSApplication>().CurrentNodeId();
		}
	}

	public static class PaymentProviderExtensions
	{
		public static string SuccessNodeUrl(this PaymentProvider paymentProvider)
		{
			throw new Exception();
		}
	}

}