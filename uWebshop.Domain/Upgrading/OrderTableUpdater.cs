﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain.Helpers;

namespace uWebshop.Domain.Upgrading
{
	internal class OrderTableUpdater
	{
		public void AddStoreOrderReferenceIdToExistingOrders()
		{
			IEnumerable<OrderInfo> orders = OrderHelper.GetAllOrders().Where(orderinfo => orderinfo != null && orderinfo.Status != OrderStatus.Incomplete);

			foreach (OrderInfo orderInfo in orders.Where(order => !order.StoreOrderReferenceId.HasValue))
			{
				orderInfo.StoreOrderReferenceId = TryParseOrderNumber(orderInfo.OrderNumber);

				if (orderInfo.StoreOrderReferenceId.HasValue)
				{
					uWebshopOrders.StoreOrder(orderInfo.ToOrderData());
				}
				else if (!string.IsNullOrWhiteSpace(orderInfo.OrderNumber))
				{
					Log.Instance.LogWarning("Ordernumber could not be parsed, for order with id " + orderInfo.DatabaseId + ", guid " + orderInfo.UniqueOrderId);
				}
				else if (orderInfo.Status != OrderStatus.Incomplete)
				{
					Log.Instance.LogWarning("Order without ordernumber, id " + orderInfo.DatabaseId + ", guid " + orderInfo.UniqueOrderId);
				}
			}
		}

		internal int? TryParseOrderNumber(string orderNumber)
		{
			if (string.IsNullOrWhiteSpace(orderNumber)) return null;
			int orderReferenceNumber;
			if (int.TryParse(Regex.Match(orderNumber, "\\d+$").Value, out orderReferenceNumber))
				return orderReferenceNumber;
			return null;
		}

		public void UpdateXMLAndFieldsOfExistingOrders()
		{
			List<OrderInfo> orders = OrderHelper.GetAllOrders().Where(orderinfo => orderinfo != null).ToList();

			foreach (OrderInfo orderInfo in orders)
			{
				uWebshopOrders.StoreOrder(orderInfo.ToOrderData());
			}
		}
	}
}