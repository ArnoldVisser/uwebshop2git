﻿using System.Collections.Generic;

namespace uWebshop.Domain
{
	/// <summary>
	/// 
	/// </summary>
	public class DocumentTypeAliasList
	{
		/// <summary>
		///     List of all the default uWebshop document types
		/// </summary>
		public static List<string> List
		{
			get { return new List<string> {"uWebshop", Settings.NodeAlias, Store.StoreRepositoryNodeAlias, Store.NodeAlias, Catalog.NodeAlias, Catalog.CategoryRepositoryNodeAlias, Category.NodeAlias, Catalog.ProductRepositoryNodeAlias, Product.NodeAlias, OrderedProduct.NodeAlias, OrderedProductVariant.NodeAlias, ProductVariant.NodeAlias, "uwbsDiscountRepository", DiscountProduct.SectionNodeAlias, DiscountProduct.NodeAlias, DiscountOrder.SectionNodeAlias, DiscountOrder.NodeAlias, Order.OrderRepositoryNodeAlias, OrderSection.NodeAlias, DateFolder.NodeAlias, Order.NodeAlias, OrderStoreFolder.NodeAlias, ShippingProvider.ShippingProviderRepositoryNodeAlias, ShippingProvider.ShippingProviderSectionNodeAlias, ShippingProvider.NodeAlias, ShippingProviderMethodNode.NodeAlias, ShippingProvider.ShippingProviderZoneSectionNodeAlias, Zone.ShippingZoneNodeAlias, PaymentProvider.NodeAlias, PaymentProviderMethodNode.NodeAlias, PaymentProvider.PaymentProviderRepositoryNodeAlias, PaymentProvider.PaymentProviderSectionNodeAlias, PaymentProvider.PaymentProviderZoneSectionNodeAlias, Zone.PaymentZoneNodeAlias, Email.EmailRepositoryNodeAlias, Email.EmailTemplateStoreSectionNodeAlias, Email.EmailTemplateCustomerSectionNodeAlias, Email.EmailTemplateStoreNodeAlias, Email.EmailTemplateCustomerNodeAlias}; }
		}
	}
}