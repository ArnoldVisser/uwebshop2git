﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain
{
	internal class IO
	{
		static IO()
		{
			Container = new IoCContainer();
		}

		public static IIocContainer Container { get; internal set; }
	}

	// very simpel handmade IoC Container, currently only serving singletons
	internal class IoCContainer : IIocContainer
	{
		private readonly ConcurrentDictionary<Type, object> _instanceMap = new ConcurrentDictionary<Type, object>();
		private readonly ConcurrentDictionary<Type, Type> _typeMap = new ConcurrentDictionary<Type, Type>();

		private IServiceFactory _defaultServiceFactory;

		public void RegisterType<T, T1>() where T1 : T
		{
			_typeMap.AddOrUpdate(typeof (T), typeof (T1), (t1, t2) => typeof (T1));
			object outArg;
			_instanceMap.TryRemove(typeof (T), out outArg);
		}

		public void RegisterInstance<T>(T instance)
		{
			_instanceMap.AddOrUpdate(typeof (T), instance, (t, o) => instance);
		}

		public void SetDefaultServiceFactory(IServiceFactory serviceFactory)
		{
			_defaultServiceFactory = serviceFactory;
		}

		public T Resolve<T>() where T : class
		{
			object resolvedInstance = _instanceMap.GetOrAdd(typeof (T), InstanceFactory);
			if (resolvedInstance != null)
			{
				return (T) resolvedInstance;
			}
			if (_defaultServiceFactory == null) throw new Exception("IoC container could not resolve type " + typeof (T).Name);
			return _defaultServiceFactory.Build<T>();
		}

		private object InstanceFactory(Type type)
		{
			Type resolvedType = _typeMap.ContainsKey(type) ? _typeMap[type] : null;
			if (resolvedType == null)
			{
				if (_defaultServiceFactory != null)
					return null;
				throw new Exception("IoC container could not resolve type " + type.Name);
			}

			ConstructorInfo constructor = resolvedType.GetConstructors().First();
			ParameterInfo[] parameters = constructor.GetParameters();

			return parameters.Any() ? constructor.Invoke(parameters.Select(p => _instanceMap.GetOrAdd(p.ParameterType, InstanceFactory)).ToArray()) : Activator.CreateInstance(resolvedType);
		}
	}
}