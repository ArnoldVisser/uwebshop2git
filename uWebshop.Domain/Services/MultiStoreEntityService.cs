﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal abstract class MultiStoreEntityService<T> : IEntityService<T> where T : class, IUwebshopEntity
	{
		protected readonly ConcurrentDictionary<string, List<T>> _cache = new ConcurrentDictionary<string, List<T>>();
		private readonly IEntityRepository<T> _repository;
		protected readonly IStoreService _storeService;

		protected MultiStoreEntityService(IEntityRepository<T> repository, IStoreService storeService)
		{
			_repository = repository;
			_storeService = storeService;
		}

		public T GetById(int id, string storeAlias)
		{
			T entity = GetAll(storeAlias).FirstOrDefault(p => p.Id == id);
			if (entity != null) return entity;
			entity = _repository.GetById(id, storeAlias);
			List<T> cache;
			if (entity != null && _cache.TryGetValue(storeAlias, out cache))
			{
				cache.Add(entity);
			}
			return entity;
		}

		public List<T> GetAll(string storeAlias, bool includeDisabled = false)
		{
			if (string.IsNullOrEmpty(storeAlias)) throw new Exception("Trying to load multi-store content without a store");
			List<T> entities = _cache.GetOrAdd(storeAlias, alias => _repository.GetAll(alias));
			return includeDisabled ? entities : entities.Where(e => e != null && !e.Disabled).ToList();
		}

		public virtual void ReloadEntityWithId(int id)
		{
			// store loopje zou nog een laagje hoger kunnen eventueel
			// alternatief zou je kunnen loopen over de Keys in de dictionary
			foreach (Store store in _storeService.GetAllStores())
			{
				List<T> cache;
				if (_cache.TryGetValue(store.Alias, out cache)) // this fixes strange IndexReader closed exceptions
				{
					T entity = GetById(id, store.Alias);
					_repository.ReloadData(entity, store.Alias);
					EntityReloaded(entity);
				}
			}
		}

		public virtual void UnloadEntityWithId(int id)
		{
			// store loopje zou nog een laagje hoger kunnen eventueel
			foreach (Store store in _storeService.GetAllStores())
			{
				List<T> cache;
				if (_cache.TryGetValue(store.Alias, out cache))
				{
					cache.RemoveAll(entity => entity.Id == id); // possible todo: cache lock on the list?
				}
			}
		}

		protected virtual void EntityReloaded(T entity)
		{
		}

		public virtual void FullResetCache()
		{
			_cache.Clear();
		}

		protected abstract void AfterEntitiesLoadedFromRepository(List<T> entities, string storeAlias);
	}
}