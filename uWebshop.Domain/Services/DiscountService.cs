﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class DiscountService : IDiscountService
	{
		private readonly IDiscountRepository _discountRepository;
		private readonly IOrderService _orderService;

		public DiscountService(IDiscountRepository discountRepository, IOrderService orderService)
		{
			_discountRepository = discountRepository;
			_orderService = orderService;
		}

		public List<IOrderDiscount> GetApplicableDiscountsForOrder(OrderInfo orderInfo)
		{
			orderInfo.OrderLines.ForEach(line => line.OrderDiscountInCents = 0); // reset
			List<IOrderDiscount> orderDiscounts = _discountRepository.GetAll();
			int orderLinesAmount = orderInfo.OrderLines.Sum(orderline => orderline.GetOrderLineGrandTotalInCents(orderInfo.PricesAreIncludingVAT));

			return orderDiscounts.Where(discount => !discount.Disable && orderLinesAmount >= discount.MinimalOrderAmount && (!discount.RequiredItemIds.Any() || _orderService.OrderContainsItem(orderInfo, discount.RequiredItemIds)) && (!discount.CounterEnabled || discount.Counter > 0)).HasDiscountForOrder(orderInfo).ToList();
		}

		public IOrderDiscount GetOrderDiscountById(int id)
		{
			return _discountRepository.GetById(id);
		}

		public DiscountProduct GetProductDiscountById(int id)
		{
			return DomainHelper.GetObjectsByAlias<DiscountProduct>(DiscountProduct.NodeAlias).FirstOrDefault(discount => discount.Id == id);
		}

		public IDiscount GetById(int id)
		{
			return (IDiscount)GetOrderDiscountById(id) ?? GetProductDiscountById(id);
		}
	}
}