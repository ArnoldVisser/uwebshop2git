﻿using System;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class DiscountCalculationService : IDiscountCalculationService
	{
		private readonly IOrderService _orderService;
		private readonly ICouponCodeService _couponCodeService;

		public DiscountCalculationService(IOrderService orderService, ICouponCodeService couponCodeService)
		{
			_orderService = orderService;
			_couponCodeService = couponCodeService;
		}

		/// <summary>
		///     The actual discount value for the discount, taking into account ranges if present
		/// </summary>
		/// <param name="discount"></param>
		/// <param name="order"></param>
		/// <returns>Discount value</returns>
		public int RangedDiscountValueForOrder(IOrderDiscount discount, OrderInfo order)
		{
			List<Range> ranges = Range.CreateFromString(discount.RangesString);
			if (ranges != null && ranges.Any())
			{
				List<OrderLine> affectedOrderLines = discount.AffectedOrderlines.Any() || discount.RequiredItemIds.Any() ? _orderService.GetApplicableOrderLines(order, discount.AffectedOrderlines.Any() ? discount.AffectedOrderlines : discount.RequiredItemIds) : order.OrderLines;

				int itemCount = affectedOrderLines.Sum(line => line.ProductInfo.ItemCount.GetValueOrDefault(1));

				return ranges.GetRangeAmountForValue(itemCount) ?? discount.DiscountValue;
			}
			return discount.DiscountValue;
		}

		public int DiscountAmountForOrder(IOrderDiscount discount, OrderInfo orderInfo, IAuthenticationProvider authenticationProvider = null)
		{
			if (authenticationProvider == null) authenticationProvider = IO.Container.Resolve<IAuthenticationProvider>();

			if (!string.IsNullOrEmpty(discount.CouponCode) && !orderInfo.CouponCodes.Contains(discount.CouponCode))
			{
				return 0;
			}

			var coupons = _couponCodeService.GetAllForDiscount(discount.Id);
			if (coupons.Any())//
			{
				coupons = coupons.Where(c => c.NumberAvailable > 0);
				if (!coupons.Select(c => c.CouponCode).Intersect(orderInfo.CouponCodes).Any())
					return 0;
			}

			if (discount.MemberGroups.Any() && !discount.MemberGroups.Intersect(authenticationProvider.RolesForCurrentUser).Any())
			{
				return 0;
			}

			if (discount.OncePerCustomer && !string.IsNullOrEmpty(authenticationProvider.CurrentLoginName))
			{
				var ordersforCurrentMember = OrderHelper.GetOrdersForCustomer(authenticationProvider.CurrentLoginName)
					.Where(x => x.Status != OrderStatus.Incomplete && x.Status != OrderStatus.Cancelled && x.Status != OrderStatus.Returned);

				if(ordersforCurrentMember.Any(x => x.OrderDiscounts.Any(d => d.OriginalId == discount.Id)))
				{
					return 0;
				}
			}

			List<OrderLine> applicableOrderLines = !discount.AffectedOrderlines.Any() ? orderInfo.OrderLines : orderInfo.OrderLines.Where(line => discount.AffectedOrderlines.Contains(line.ProductInfo.Id)).ToList();
			// OrderHelper.GetApplicableOrderLines(orderInfo, discount.RequiredItemIds);
			var orderItemPrices = new List<int>();
			foreach (OrderLine line in applicableOrderLines)
				for (int i = 0; i < line.ProductInfo.ItemCount; i++)
					orderItemPrices.Add(line.ProductInfo.PriceInCents); // maak een lijst met de prijzen van alle (losse) items van de order

			int numberOfItemsLeftOutOfSets = discount.NumberOfItemsCondition == 0 ? 0 : applicableOrderLines.Sum(line => line.ProductInfo.ItemCount.GetValueOrDefault(1))%discount.NumberOfItemsCondition;
			List<int> remainingOrderItemPricesOrderedCheapestFirst = orderItemPrices.OrderBy(item => item).Take(orderItemPrices.Count - numberOfItemsLeftOutOfSets).ToList();

			int rangedDiscountValue = RangedDiscountValueForOrder(discount, orderInfo);
			int discountAmount = rangedDiscountValue;
			int maximumDiscountableAmount = 0;
			int timesApplicable = 1;
			if (discount.Condition == DiscountOrderCondition.None)
			{
				maximumDiscountableAmount = applicableOrderLines.Sum(orderline => orderline.OrderLineAmountInCents - orderline.OrderDiscountInCents);
				timesApplicable = applicableOrderLines.Sum(orderLine => orderLine.ProductInfo.ItemCount.GetValueOrDefault(1));
			}
			else if (discount.Condition == DiscountOrderCondition.OnTheXthItem && discount.NumberOfItemsCondition > 0)
			{
				// todo: test
				discountAmount = discountAmount*remainingOrderItemPricesOrderedCheapestFirst.Count()/discount.NumberOfItemsCondition;
				maximumDiscountableAmount = remainingOrderItemPricesOrderedCheapestFirst.Take(orderItemPrices.Count/discount.NumberOfItemsCondition).Sum(); // bereken de korting over de x goedkoopste items, waarbij x het aantal sets in de order is
			}
			else if (discount.Condition == DiscountOrderCondition.PerSetOfXItems && discount.NumberOfItemsCondition > 0)
			{
				timesApplicable = remainingOrderItemPricesOrderedCheapestFirst.Count()/discount.NumberOfItemsCondition;
				discountAmount = discountAmount*remainingOrderItemPricesOrderedCheapestFirst.Count()/discount.NumberOfItemsCondition;
				maximumDiscountableAmount = remainingOrderItemPricesOrderedCheapestFirst.Sum();
			}

			if (discount.DiscountType == DiscountType.Amount)
			{
				return Math.Min(discountAmount, maximumDiscountableAmount);
			}
			if (discount.DiscountType == DiscountType.Percentage)
			{
				return (int) (rangedDiscountValue*maximumDiscountableAmount/10000m);
			}
			if (discount.DiscountType == DiscountType.FreeShipping)
			{
				return orderInfo.ShippingProviderCostsWithVatInCents;
			}
			if (discount.DiscountType == DiscountType.NewPrice)
			{
				if (discount.Condition == DiscountOrderCondition.OnTheXthItem && discount.NumberOfItemsCondition > 0)
				{
					return remainingOrderItemPricesOrderedCheapestFirst.Take(orderItemPrices.Count/discount.NumberOfItemsCondition).Select(price => Math.Max(0, price - discountAmount)).Sum();
				}

				return Math.Max(0, maximumDiscountableAmount - rangedDiscountValue*timesApplicable);
			}

			return 0;
		}
	}
}