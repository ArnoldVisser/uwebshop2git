﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class ShippingProviderService : IShippingProviderService
	{
		private readonly IShippingProviderRepository _shippingProviderRepository;

		public ShippingProviderService(IShippingProviderRepository shippingProviderRepository)
		{
			_shippingProviderRepository = shippingProviderRepository;
		}

		public IEnumerable<ShippingProvider> GetAll(string storeAlias)
		{
			return _shippingProviderRepository.GetAll(storeAlias);
		}

		public ShippingProvider GetPaymentProviderWithName(string paymentProviderName, string storeAlias)
		{
			return _shippingProviderRepository.GetAll(storeAlias).FirstOrDefault(x => x.Name.ToLower() == paymentProviderName.ToLower());
		}

		public ShippingProvider GetById(int id, string storeAlias)
		{
			return _shippingProviderRepository.GetById(id, storeAlias);
		}

		public void LoadData(ShippingProvider paymentProvider)
		{
			_shippingProviderRepository.ReloadData(paymentProvider, StoreHelper.CurrentStoreAlias);
		}
	}
}