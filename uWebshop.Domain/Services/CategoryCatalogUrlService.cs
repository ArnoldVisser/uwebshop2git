﻿using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	class CategoryCatalogUrlService : ICategoryCatalogUrlService
	{
		public string GetUrl(ICategory category)
		{
			var productCategoryUrl = category.UrlName;
			while (category.ParentCategory != null)
			{
				category = category.ParentCategory;
				productCategoryUrl = string.Format("{0}/{1}", category.UrlName, productCategoryUrl);
			}
			return productCategoryUrl;
		}
	}
}