﻿using System.Collections.Generic;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class ProductVariantService : MultiStoreEntityService<ProductVariant>, IProductVariantService
	{
		public ProductVariantService(IProductVariantRepository productVariantRepository, IStoreService storeService) : base(productVariantRepository, storeService)
		{
		}

		protected override void AfterEntitiesLoadedFromRepository(List<ProductVariant> entities, string storeAlias)
		{
		}
	}
}