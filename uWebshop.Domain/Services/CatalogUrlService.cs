﻿using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	class CatalogUrlService : ICatalogUrlService
	{
		public string BuildUrl(string baseUrl, string categoriesUrl, string productUrl)
		{
			return string.Format("{0}/{1}/{2}", baseUrl, categoriesUrl, productUrl);
		}
	}
}