﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.DataAccess;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class OrderUpdatingService : IOrderUpdatingService
	{
		private readonly ICMSApplication _cmsApplication;
		private readonly IProductService _productService;
		private readonly IProductVariantService _productVariantService;
		private readonly ICouponCodeService _couponCodeService;
		private readonly IDiscountService _discountService;

		public OrderUpdatingService(ICMSApplication cmsApplication, IProductService productService, IProductVariantService productVariantService, ICouponCodeService couponCodeService, IDiscountService discountService)
		{
			_cmsApplication = cmsApplication;
			_productService = productService;
			_productVariantService = productVariantService;
			_couponCodeService = couponCodeService;
			_discountService = discountService;
		}

		protected HttpSessionState Session
		{
			get { return HttpContext.Current.Session; }
		}

		/// <summary>
		/// Create, Add, Update the orderline
		/// </summary>
		/// <param name="order"></param>
		/// <param name="orderLineId">The Id of the orderline</param>
		/// <param name="productId">The productId</param>
		/// <param name="action">The action (add, update, delete, deleteall)</param>
		/// <param name="itemCount">The amount of items to be added</param>
		/// <param name="variantsList">The variants ID's added to the pricing</param>
		/// <param name="fields">Custom Fields</param>
		/// <exception cref="System.ArgumentException">
		/// productId and orderLineId equal or lower to 0
		/// or
		/// itemCountToAdd can't be smaller than 0
		/// </exception>
		/// <exception cref="System.Exception">
		/// Orderline not found
		/// </exception>
		public void AddOrUpdateOrderLine(OrderInfo order, int orderLineId, int productId, string action, int itemCount, IEnumerable<int> variantsList, Dictionary<string, string> fields = null)
		{
			//todo: function is too long
			//   separate control and logic
			//todo: function needs testing
			// todo: DIP

			if (!_cmsApplication.RequestIsInCMSBackend(HttpContext.Current))
			{
				if (order == null || order.Paid.GetValueOrDefault(false) || (order.Status != OrderStatus.PaymentFailed && order.Status != OrderStatus.Incomplete && order.Status != OrderStatus.WaitingForPayment))
				{
					Log.Instance.LogDebug("Starting new order for user" + (order == null ? "" : ", previous order: " + order.UniqueOrderId));
					order = OrderHelper.CreateOrder();
				}

				if (order.Status == OrderStatus.PaymentFailed || order.Status == OrderStatus.WaitingForPayment)
				{
					Log.Instance.LogDebug("Orderstatus WILL BE CHANGED FROM: " + order.Status);
					order.Status = OrderStatus.Incomplete;
				}
			}

			// clear some stored values so they can be recalculated
			order.ClearCachedValues();

			//Log.Instance.LogDebug( "AddOrUpdateOrderLine() function START: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			if (itemCount == 0 && action != "update")
				itemCount = 1;

			if (productId <= 0 && orderLineId <= 0)
			{
				throw new ArgumentException("productId <= 0 && orderLineId <= 0");
			}

			if (itemCount < 0)
			{
				throw new ArgumentException("itemCount can't be smaller than 0");
			}

			//Log.Instance.LogDebug( "AddOrUpdateOrderLine() find orderline START: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));

			// todo: make it possible to have multiple products with only difference the text or the image id
			
			Log.Instance.LogDebug("AddOrUpdateOrderLine Before action");

			var variants = variantsList.Where(v => v != 0).OrderBy(v => v);

			OrderLine orderLine = null;

			if (orderLineId != 0 && action != "new")
			{
				orderLine = order.OrderLines.FirstOrDefault(line => line.OrderLineId == orderLineId);
			}
			if (orderLine == null && action != "new")
			{
				orderLine = order.OrderLines.FirstOrDefault(line => line.ProductInfo.Id == productId && line.VariantsMatch(variants));
			}
			if (orderLineId != 0 && orderLine == null && action != "new") throw new Exception("Orderline not found");

			if (productId == 0 && orderLine != null) productId = orderLine.ProductInfo.Id;

			if (action == "add" || action == "new")
			{
				action = "update";
				if (orderLine != null)
				{
					itemCount = (int) (orderLine.ProductInfo.ItemCount + itemCount);
				}
			}
			
			Log.Instance.LogDebug("AddOrUpdateOrderLine Before stock");

			if (productId != 0)
			{
				var requestedItemCount = itemCount;
				var tooMuchStock = false;

				var storeAlias = StoreHelper.CurrentStoreAlias;
				var product = _productService.GetById(productId, storeAlias);

				foreach (var variant in variantsList.Select(variantId => _productVariantService.GetById(variantId, storeAlias)))
				{
					if (variant != null && variant.StockStatus && !variant.BackorderStatus && variant.Stock < itemCount)
					{
						Session.Add(Constants.OrderedItemcountHigherThanStockKey, BasketActions.OrderCountTooHighError);
						itemCount = variant.Stock;
						tooMuchStock = true;
					}
				}

				if (!product.UseVariantStock && product.StockStatus && !product.BackorderStatus && product.Stock < itemCount)
				{
					Session.Add(Constants.OrderedItemcountHigherThanStockKey, BasketActions.OrderCountTooHighError);

					itemCount = product.Stock;
					tooMuchStock = true;
				}

				if (HttpContext.Current != null) // todo: better decoupling
					ClientErrorHandling.SetOrClearErrorMessage(!tooMuchStock, "Ordered higher quantity than available stock. Updated the basked to available stock count", "Stock", requestedItemCount.ToString());
			}

			if (itemCount < 1)
			{
				itemCount = 0;
			}

			if (action == "update" && itemCount == 0)
			{
				action = "delete";
			}

			Log.Instance.LogDebug("AddOrUpdateOrderLine Before update");

			#region update

			if (action == "update")
			{
				BeforeOrderLineUpdatedEventArgs beforeUpdatedEventArgs = order.FireBeforeOrderLineUpdatedEvent(orderLine);

				if (beforeUpdatedEventArgs == null || !beforeUpdatedEventArgs.Cancel) // todo: test the cancel
				{
					if (orderLine == null)
					{
						order.FireBeforeOrderLineCreatedEvent();

						if (orderLineId == 0)
						{
							orderLine = OrderProduct(productId, variants, itemCount, order);

							if (!order.OrderLines.Any())
							{
								orderLine.OrderLineId = 1;
							}
							else
							{
								OrderLine firstOrDefault = order.OrderLines.OrderByDescending(x => x.OrderLineId).FirstOrDefault();
								if (firstOrDefault != null)
								{
									int highestOrderLineId = firstOrDefault.OrderLineId;
									orderLine.OrderLineId = highestOrderLineId + 1;
								}
							}
						}
						if (orderLine == null)
						{
							throw new Exception("Order line not found");
						}

						order.OrderLines.Add(orderLine);

						order.FireAfterOrderLineCreatedEvent(orderLine);
					}

					if (orderLineId != 0)
					{
						orderLine.ProductInfo.ItemCount = itemCount;

						// onderstaande regel gooit variants weg als ze niet in de lijst met ids zitten, is dat een bug of niet?
						orderLine.ProductInfo.ProductVariants = variants.Select(// dit zorgt ervoor dat [Variant]Discount met membership na inloggen wel weer werkt
							variant => new ProductVariantInfo(DomainHelper.GetProductVariantById(variant), orderLine.ProductInfo, itemCount)).ToList();
					}

					orderLine.ProductInfo._productRangePriceInCents = null;

					orderLine.ProductInfo.ChangedOn = DateTime.Now;
					orderLine.ProductInfo.ItemCount = itemCount;
					
					UpdateProductInfoDiscountInformation(orderLine.ProductInfo);

					foreach (ProductVariantInfo variant in orderLine.ProductInfo.ProductVariants)
						variant.ChangedOn = DateTime.Now;

					order.FireAfterOrderLineUpdatedEvent(orderLine);
				}

				//Log.Instance.LogDebug("AddOrUpdateOrderLine() UPDATE END: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			}

			#endregion

			#region delete

			if (action == "delete")
			{
				BeforeOrderLineDeletedEventArgs beforeDeletedEventArgs = order.FireBeforeOrderLineDeletedEvent(orderLine);

				if (beforeDeletedEventArgs == null || !beforeDeletedEventArgs.Cancel)
				{
					order.OrderLines.Remove(orderLine);

					order.FireAfterOrderLineDeletedEvent();
				}
			}

			#endregion

			// UPDATE SHIPPING & SET UPDATESHIPPINGCOSTS TO TRUE AFTER BASKET UPDATE
			//Log.Instance.LogDebug( "AddOrUpdateOrderLine() AutoSelectShippingProvider START: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			//AutoSelectShippingProvider();
			//Log.Instance.LogDebug( "AddOrUpdateOrderLine() AutoSelectShippingProvider END: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			order.ShippingCostsMightBeOutdated = true;
			//Log.Instance.LogDebug( "AddOrUpdateOrderLine() function END: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));

			if (fields == null) return;

			var xDoc = new XDocument(new XElement("Fields"));
			Log.Instance.LogDebug("AddOrUpdateOrderLine Before xdoc");

			AddFieldsToXDocumentBasedOnCMSDocumentType(xDoc, fields, orderLine.ProductInfo.Product.NodeTypeAlias);

			orderLine._customData = xDoc;
		}

		private void UpdateProductInfoDiscountInformation(ProductInfo product)
		{
			if (product.IsDiscounted)
			{
				var discount = IO.Container.Resolve<IProductDiscountRepository>().GetById(product.DiscountId, product.Order.StoreInfo.Alias);
				if (discount == null) return;
				if (discount.DiscountType == DiscountType.Amount)
					product.DiscountAmountInCents = discount.RangedDiscountValue(product.ItemCount.GetValueOrDefault(1));
				else if (discount.DiscountType == DiscountType.Percentage)
					product.DiscountPercentage = discount.RangedDiscountValue(product.ItemCount.GetValueOrDefault(1)) / 100m;
				else if (discount.DiscountType == DiscountType.NewPrice)
				{
					product.DiscountAmountInCents = product.OriginalPriceInCents - discount.RangedDiscountValue(product.ItemCount.GetValueOrDefault(1));
					product.Ranges = new List<Range>(); // NewPrice overrules any ranges
				}
			}
		}

		/// <summary>
		///     Save the order
		/// </summary>
		public void Save(OrderInfo order, bool revalidateOrderOnLoadHack = false, ValidateSaveAction validateSaveAction = ValidateSaveAction.Order)
		{
			// todo: DIP
			order.ReValidateSaveAction = validateSaveAction;
			order.RevalidateOrderOnLoad = revalidateOrderOnLoadHack;

			order.FireBeforeOrderUpdatedEvent();

			#region set order expiration cookie

			if (order.Status == OrderStatus.Incomplete)
			{
				OrderHelper.SetOrderCookie(order);
			}

			#endregion

			// If no member connected to the order, add member to the order
			if (string.IsNullOrEmpty(order.CustomerInfo.LoginName))
			{
				MembershipUser currentMember = Membership.GetUser(); // TODO: dip

				if (currentMember != null)
				{
					uWebshopOrders.SetCustomer(order.UniqueOrderId, currentMember.UserName); // TODO: dip
					order.CustomerInfo.LoginName = currentMember.UserName;
					if (currentMember.ProviderUserKey != null)
					{
						uWebshopOrders.SetCustomerId(order.UniqueOrderId, (int) currentMember.ProviderUserKey);
						order.CustomerInfo.CustomerId = (int) currentMember.ProviderUserKey; // TODO: dip
					}
				}
			}

			if (order.Status == OrderStatus.Incomplete)
			{
				order.OrderDate = DateTime.Now.ToString("f");

				RemoveDiscountsWithCounterZeroFromOrder(order);
			}

			//uWebshopOrders.SetOrderInfo(UniqueOrderId, DomainHelper.SerializeObjectToXmlString(this), Status.ToString());
			uWebshopOrders.StoreOrder(order.ToOrderData());

			order.FireAfterOrderUpdatedEvent();
		}

		// todo: huh?!?!?!?!?!?!? => deze functionaliteit doet niks en is niet nodig
		public void RemoveDiscountsWithCounterZeroFromOrder(OrderInfo order)
		{
			foreach (IOrderDiscount discount in order.OrderDiscounts.Where(x => x.CounterEnabled && x.Counter == 0))
			{
				Log.Instance.LogWarning("Discount " + discount.OriginalId + " removed from order " + order.UniqueOrderId);
				order.OrderDiscounts.Remove(discount);
			}
		}

		public CouponCodeResult AddCoupon(OrderInfo order, string couponCode)
		{
			if (ChangeOrderToIncompleteAndReturnFalseIfNotPossible(order))
				return CouponCodeResult.NotPermitted;

			if (string.IsNullOrEmpty(couponCode))
			{
				return CouponCodeResult.Failed;
			}

			if (order.CouponCodes.Contains(couponCode))
			{
				return CouponCodeResult.AlreadyUsed;
			}

			var discounts = IO.Container.Resolve<ICouponCodeService>().GetAllWithCouponcode(couponCode).Where(coupon => coupon.NumberAvailable > 0)
				.Select(coupon => _discountService.GetById(coupon.DiscountId)).ToList();
			DiscountOrder couponOrderDiscount = DomainHelper.GetObjectsByAlias<DiscountOrder>(DiscountOrder.NodeAlias).FirstOrDefault(x => x.CouponCode == couponCode);
			
			if (couponOrderDiscount != null) discounts.Add(couponOrderDiscount);
			
			if (!discounts.Any())
			{
				return CouponCodeResult.NotFound;
			}

			MembershipUser member = Membership.GetUser();
			bool oncePerCustomer = discounts.All(discount => discount.OncePerCustomer);
			if (oncePerCustomer && member != null)
			{
				IEnumerable<OrderInfo> ordersOfMember = OrderHelper.GetOrdersForCustomer(member.UserName);

				foreach (var discount in discounts)
				{
					if (ordersOfMember.Any(x => x.CouponCodes != null && x.CouponCodes.Contains(couponCode) 
						&& (x.OrderDiscounts.Any(d => d.OriginalId == discount.Id)) || x.OrderLines.Any(l => l.ProductInfo.DiscountId == discount.Id)))
					{
						return CouponCodeResult.OncePerCustomer;
					}
				}
			}

			if (discounts.All(discount => discount.MinimumOrderAmountInCents > 0) && order.GrandtotalInCents < discounts.Max(d => d.MinimumOrderAmountInCents))
			{
				return CouponCodeResult.MinimumOrderAmount;
			}

			if (discounts.All(discount => discount.CounterEnabled && discount.Counter <= 0))
			{
				return CouponCodeResult.OutOfStock;
			}

			order.SetCouponCode(couponCode);

			return CouponCodeResult.Succes;
		}

		public ProviderActionResult AddShippingProvider(OrderInfo order, int shippingProviderId, string shippingProviderMethodId)
		{
			if (ChangeOrderToIncompleteAndReturnFalseIfNotPossible(order))
			{
				Log.Instance.LogDebug("AddShippingMethod: " + ProviderActionResult.NotPermitted);
				return ProviderActionResult.NotPermitted;
			}

			if (shippingProviderId == 0)
			{
				Log.Instance.LogDebug("AddShippingMethod: " + ProviderActionResult.ProviderIdZero);
				return ProviderActionResult.ProviderIdZero;
			}

			//var shippingProvider = ShippingProviderHelper.GetShippingProvidersForOrder(order).FirstOrDefault(x => x.Id == shippingProviderId);
			ShippingProvider shippingProvider = ShippingProviderHelper.GetAllShippingProviders().FirstOrDefault(x => x.Id == shippingProviderId);

			if (shippingProvider == null)
			{
				Log.Instance.LogDebug("AddShippingMethod shippingProvider " + ProviderActionResult.NoCorrectInput + " shippingProviderId: " + shippingProviderId);
				return ProviderActionResult.NoCorrectInput;
			}

			order.ShippingInfo.Id = shippingProviderId;
			order.ShippingInfo.Title = shippingProvider.Title;

			order.ShippingInfo.ShippingType = shippingProvider.Type;

			ShippingProviderMethod shippingMethod = shippingProvider.ShippingProviderMethods.FirstOrDefault(x => x.Id == shippingProviderMethodId);

			if (shippingMethod == null)
			{
				Log.Instance.LogDebug("AddShippingMethod shippingMethod " + ProviderActionResult.NoCorrectInput + " shippingProviderMethodId: " + shippingProviderMethodId);
				return ProviderActionResult.NoCorrectInput;
			}

			order.ShippingInfo.MethodId = shippingProviderMethodId;
			order.ShippingInfo.MethodTitle = shippingMethod.Title;

			order.ShippingProviderAmountInCents = shippingMethod.PriceInCents;

			//ApplyDiscounts(); // maybe todo
			//_calculatedDiscountInCents = null; // recalculate if status=incomplete

			return ProviderActionResult.Success;
		}

		public bool AddCustomerFields(OrderInfo order, Dictionary<string, string> fields, CustomerDatatypes customerDataType)
		{
			if (ChangeOrderToIncompleteAndReturnFalseIfNotPossible(order))
				return false;

			//var xDoc = OrderUpdatingService.CreateXDocumentBasedOnCMSDocumentType(fields, documentAlias);

			var xDoc = new XDocument(new XElement(customerDataType.ToString()));

			if (customerDataType == CustomerDatatypes.Customer)
			{
				if (order.CustomerInfo.customerInformation != null) xDoc = order.CustomerInfo.customerInformation;
			}

			if (customerDataType == CustomerDatatypes.Shipping)
			{
				if (order.CustomerInfo.shippingInformation != null) xDoc = order.CustomerInfo.shippingInformation;
			}

			if (customerDataType == CustomerDatatypes.Extra)
			{
				if (order.CustomerInfo.extraInformation != null) xDoc = order.CustomerInfo.extraInformation;
			}

			foreach (var field in fields.Where(field => xDoc.Root != null && xDoc.Root.Descendants(field.Key).Any()))
			{
				if (xDoc.Root != null) xDoc.Root.Descendants(field.Key).Remove();
			}

			AddFieldsToXDocumentBasedOnCMSDocumentType(xDoc, fields, Order.NodeAlias);

			IDocumentTypeInfo documentType = IO.Container.Resolve<ICMSDocumentTypeService>().GetByAlias(Order.NodeAlias);

			// special fields that also work when field is not created on order document type
			foreach (var field in fields)
			{
				if (field.Key.ToLower() == "customercountry")
				{
					order.CustomerInfo.CountryCode = field.Value;

					if (field.Value.Length > 4)
					{
						Log.Instance.LogDebug(string.Format("customerCountry Length == {0} Value: {1}, Possible added Country.Name instead of Country.Code?", field.Key.Length, field.Value));
					}
				}

				if (field.Key.ToLower() == "shippingcountry")
				{
					order.CustomerInfo.ShippingCountryCode = field.Value;

					if (field.Value.Length > 4)
					{
						Log.Instance.LogDebug(string.Format("shippingcountry Length == {0} Value: {1}, Possible added Country.Name instead of Country.Code?", field.Key.Length, field.Value));
					}
				}

				if (field.Key.ToLower() == "customervatnumber")
				{
					order.SetVATNumber(field.Value);
				}

				if (field.Key.ToLower() == "customerregion")
				{
					order.CustomerInfo.RegionCode = field.Value;
					order._regionalVatInCents = null;

					if (field.Value.Length > 4)
					{
						Log.Instance.LogDebug(string.Format("customerregion Length == {0} Value: {1}, Possible added Country.Name instead of Country.Code?", field.Key.Length, field.Value));
					}
				}
			}

			if (customerDataType == CustomerDatatypes.Customer)
			{
				order.CustomerInfo.customerInformation = xDoc;
				return true;
			}

			if (customerDataType == CustomerDatatypes.Shipping)
			{
				order.CustomerInfo.shippingInformation = xDoc;
				return true;
			}

			if (customerDataType == CustomerDatatypes.Extra)
			{
				order.CustomerInfo.extraInformation = xDoc;
				return true;
			}

			return false;
		}

		public ProviderActionResult AddPaymentProvider(OrderInfo order, int paymentProviderId, string paymentProviderMethodId)
		{
			if (ChangeOrderToIncompleteAndReturnFalseIfNotPossible(order))
				return ProviderActionResult.NotPermitted;

			if (paymentProviderId == 0)
			{
				Log.Instance.LogError("AddPaymentProvider AddPaymentMethod ProviderActionResult.ProviderIdZero");
				return ProviderActionResult.ProviderIdZero;
			}
			PaymentProvider paymentProvider = PaymentProviderHelper.GetPaymentProvidersForOrder(order).SingleOrDefault(x => x.Id == paymentProviderId);

			if (paymentProvider == null)
			{
				Log.Instance.LogError("AddPaymentProvider AddPaymentMethod paymentProvider == null");
				return ProviderActionResult.NoCorrectInput;
			}

			order.PaymentInfo.Id = paymentProviderId;
			order.PaymentInfo.Title = paymentProvider.Title;

			order.PaymentInfo.PaymentType = paymentProvider.Type;

			PaymentProviderMethod paymentMethod = paymentProvider.PaymentProviderMethods.SingleOrDefault(x => x.Id == paymentProviderMethodId);

			if (paymentMethod == null)
			{
				Log.Instance.LogError("AddPaymentProvider AddPaymentMethod paymentMethod == null");
				return ProviderActionResult.NoCorrectInput;
			}

			order.PaymentInfo.MethodId = paymentProviderMethodId;
			order.PaymentInfo.MethodTitle = paymentMethod.Title;

			if (paymentMethod.AmountType == PaymentProviderAmountType.Amount)
				order.PaymentProviderAmount = paymentMethod.PriceInCents;
			else
				order.PaymentProviderOrderPercentage = paymentMethod.PriceInCents;

			return ProviderActionResult.Success;
		}

		public bool ChangeOrderToIncompleteAndReturnFalseIfNotPossible(OrderInfo order)
		{
			if (!_cmsApplication.RequestIsInCMSBackend(HttpContext.Current))
			{
				if (order.Paid.GetValueOrDefault() || order.Status != OrderStatus.PaymentFailed && order.Status != OrderStatus.Incomplete && order.Status != OrderStatus.WaitingForPayment)
				{
					return true;
				}

				if (order.Status == OrderStatus.PaymentFailed || order.Status == OrderStatus.WaitingForPayment)
				{
					Log.Instance.LogDebug("Orderstatus WILL BE CHANGED FROM: " + order.Status);
					order.Status = OrderStatus.Incomplete;
				}
			}
			return false;
		}

		public bool ConfirmOrder(OrderInfo order, bool termsAccepted, int confirmationNodeId)
		{
			order.TermsAccepted = termsAccepted;

			if (!OrderHelper.ValidateOrder(order))
			{
				Save(order, true);
				Log.Instance.LogDebug("ValidateOrder == false");
				RemoveDiscountsWithCounterZeroFromOrder(order);
				return false;
			}

			string incompleteOrderNumber = order.OrderNumber;
			// change the ordernumber to the 'real' one
			int generatedId = 0;
			if (!order.StoreOrderReferenceId.HasValue || order.OrderNumber.StartsWith("[INCOMPLETE]-"))

			{
				if (_cmsApplication.UsesSQLCEDatabase() || _cmsApplication.UsesMySQLDatabase())
				{
					// set final ordernumber for payment provider communication, but not yet save it in the database
					order.OrderNumber = OrderHelper.GenerateOrderNumber(order.StoreInfo.Store, order, out generatedId);
				}
				else
				{
					OrderHelper.AssignNewOrderNumberToOrder(order, order.StoreInfo.Store);
				}
			}

			// generate redirectUrl and communicate with payment provider
			string redirectUrl = OrderHelper.GetRedirectUrlAfterConfirmation(order, confirmationNodeId);

			if (redirectUrl == "failed")
			{
				// if fails, set ordernumber back to incomplete
				order.OrderNumber = incompleteOrderNumber;
				order.StoreOrderReferenceId = null;
				Save(order, true);
				return false;
			}

			// store final ordernumber only in DB when order can be confirmed!
			uWebshopOrders.SetOrderNumber(order.UniqueOrderId, order.OrderNumber, order.StoreInfo.Alias, generatedId);

			order.RedirectUrl = redirectUrl;

			order.Status = OrderStatus.Confirmed;

			Save(order, true);

			return true;
		}

		public static void AddFieldsToXDocumentBasedOnCMSDocumentType(XDocument xDoc, Dictionary<string, string> fields, string documentAlias)
		{
			string replacedOrderlineDocTypeAlias = documentAlias.Replace(Product.NodeAlias, OrderedProduct.NodeAlias);

			IDocumentTypeInfo documentType = IO.Container.Resolve<ICMSDocumentTypeService>().GetByAlias(replacedOrderlineDocTypeAlias) ?? IO.Container.Resolve<ICMSDocumentTypeService>().GetByAlias(OrderedProduct.NodeAlias);


			foreach (var field in fields.Where(x => documentType.Properties.Any(y => y.Alias.ToLower() == x.Key.ToLower())))
			{
				var propertyType = documentType.Properties.FirstOrDefault(prop => prop.Alias.ToLower() == field.Key.ToLower());

				if (propertyType != null)
					ClientErrorHandling.SetOrClearErrorMessage(propertyType.ValidationRegularExpression != null && new Regex(propertyType.ValidationRegularExpression).IsMatch(field.Value), "Error in field: " + field.Key, field.Key, field.Value);
				// todo: customize error message
			}

			List<XNode> xNodeList = fields.Where(field => documentType.Properties.Any(x => x.Alias.ToLower() == field.Key.ToLower())).Select(field => new XElement(field.Key, new XCData(field.Value))).Cast<XNode>().ToList();

			if (xDoc.Root != null)
			{
				xDoc.Root.Add(xNodeList);
			}
		}

		private OrderLine OrderProduct(int productId, IOrderedEnumerable<int> variants, int itemCount, OrderInfo order)
		{
			// load data from dataprovider (now only umbraco), todo: DIP data provider

			var productService = IO.Container.Resolve<IProductService>();

			ProductInfo productInfo = productService.CreateProductInfoByProductId(productId, order, StoreHelper.CurrentStoreAlias, itemCount);
			//new ProductInfo(productService.GetById(productId), order) { ItemCount = itemCount };
			if (variants != null && variants.Any())
			{
				var productVariantService = IO.Container.Resolve<IProductVariantService>();
				productInfo.ProductVariants = variants.Select(variant => new ProductVariantInfo(productVariantService.GetById(variant, StoreHelper.CurrentStoreAlias), productInfo, itemCount)).ToList();
			}
			return new OrderLine(productInfo, order);
		}
	}
}