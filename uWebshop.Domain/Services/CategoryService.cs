﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class CategoryService : MultiStoreEntityService<Category>, ICategoryService
	{
		private readonly ConcurrentDictionary<string, List<Category>> _rootCategoriesCache = new ConcurrentDictionary<string, List<Category>>();

		public CategoryService(ICategoryRepository categoryRepository, IStoreService storeService) : base(categoryRepository, storeService)
		{
		}

		// todo IMPORTANT!: handle changes in structure (move categories)

		//public override void ReloadEntityWithId(int id)
		//{
		//	base.ReloadEntityWithId(id);
		//	// store loopje zou nog een laagje hoger kunnen eventueel
		//	foreach (var store in _storeService.GetAllStores())
		//	{
		//		var entity = GetById(id, store.Alias);
		//		_repository.ReloadData(entity, store.Alias);
		//	}
		//}

		//public override void UnloadEntityWithId(int id)
		//{
		//	base.UnloadEntityWithId(id);
		//}

		// dit kan (of kon) een IndexReader closed (examine) exception geven, hopelijk fixt de eerste .ToList() dat
		public List<Category> GetAllRootCategories(string storeAlias)
		{
			if (string.IsNullOrEmpty(storeAlias)) throw new Exception("Trying to load multi-store content without store");
			return _rootCategoriesCache.GetOrAdd(storeAlias, alias =>
			{
				List<int> categoryRepositoryNodeIds = Catalog.GetCategoryRepositoryNodes().Select(n => n.Id).ToList();
				return GetAll(alias).Where(category => categoryRepositoryNodeIds.Contains(category.ParentId)).ToList();
			});
		}

		public override void FullResetCache()
		{
			base.FullResetCache();
			_rootCategoriesCache.Clear();
		}

		protected override void AfterEntitiesLoadedFromRepository(List<Category> entities, string storeAlias)
		{
			// dit werkt niet omdat GetById voor een deadlock zorgt lijkt het
			//entities.ForEach(category => category.ParentCategory = GetById(category.ParentId, storeAlias));
		}
	}
}