﻿using uWebshop.DataAccess;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class StockService : IStockService
	{
		public int GetStockForUwebshopEntityWithId(int id)
		{
			return StoreHelper.GetMultiStoreStock(id);
		}

		public void SetStock(int itemId, int delta, bool updateOrderCount = true, string storeAlias = null)
		{
			UWebshopStock.SetStock(itemId, delta, updateOrderCount, storeAlias);
		}

		public void SetOrderCount(int itemId, int orderCountToUpdate, string storeAlias = null)
		{
			UWebshopStock.SetOrderCount(itemId, orderCountToUpdate, storeAlias);
		}

		public void ReturnStock(int itemId, int stockToReturn, bool updateOrderCount = true, string storeAlias = null)
		{
			UWebshopStock.ReturnStock(itemId, stockToReturn, updateOrderCount, storeAlias);
		}
	}
}