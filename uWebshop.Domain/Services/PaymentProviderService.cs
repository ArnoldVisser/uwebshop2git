﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class PaymentProviderService : IPaymentProviderService
	{
		private readonly IPaymentProviderRepository _paymentProviderRepository;

		public PaymentProviderService(IPaymentProviderRepository paymentProviderRepository)
		{
			_paymentProviderRepository = paymentProviderRepository;
		}

		public IEnumerable<PaymentProvider> GetAll(string storeAlias)
		{
			return _paymentProviderRepository.GetAll(storeAlias);
		}

		public PaymentProvider GetPaymentProviderWithName(string paymentProviderName, string storeAlias)
		{
			return _paymentProviderRepository.GetAll(storeAlias).FirstOrDefault(x => x.Name.ToLower() == paymentProviderName.ToLower());
		}

		public PaymentProvider GetById(int id, string storeAlias)
		{
			return _paymentProviderRepository.GetById(id, storeAlias);
		}

		public void LoadData(PaymentProvider paymentProvider)
		{
			_paymentProviderRepository.ReloadData(paymentProvider, StoreHelper.CurrentStoreAlias);
		}
	}
}