﻿using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	class ProductUrlUsingCurrentStoreService : IProductUrlService
	{
		private readonly IStoreProductUrlService _storeProductUrlService;
		private readonly IStoreService _storeService;

		public ProductUrlUsingCurrentStoreService(IStoreProductUrlService storeProductUrlService, IStoreService storeService)
		{
			_storeProductUrlService = storeProductUrlService;
			_storeService = storeService;
		}

		public string GetUrl(IProduct product)
		{
			return _storeProductUrlService.GetUrl(product, _storeService.GetCurrentStore());
		}

		public string GetCanonicalUrl(IProduct product)
		{
			return _storeProductUrlService.GetCanonicalUrl(product, _storeService.GetCurrentStore());
		}
	}
}