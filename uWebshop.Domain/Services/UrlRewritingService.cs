﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Common.Interfaces;
using uWebshop.Domain.BaseClasses;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class UrlRewritingService : IUrlRewritingService
	{
		private readonly ICatalogUrlResolvingService _catalogUrlResolvingService;
		private readonly ICatalogUrlSplitterService _catalogUrlSplitterService;
		private readonly ICMSApplication _cmsApplication;
		private readonly IUwebshopConfiguration _configuration;
		private readonly IHttpContextWrapper _httpContextWrapper;
		private readonly IPaymentProviderService _paymentProviderService;

		public UrlRewritingService(ICatalogUrlSplitterService catalogUrlSplitterService, ICatalogUrlResolvingService catalogUrlResolvingService, ICMSApplication cmsApplication, IPaymentProviderService paymentProviderService, IHttpContextWrapper httpContextWrapper, IUwebshopConfiguration configuration)
		{
			_catalogUrlSplitterService = catalogUrlSplitterService;
			_catalogUrlResolvingService = catalogUrlResolvingService;
			_cmsApplication = cmsApplication;
			_paymentProviderService = paymentProviderService;
			_httpContextWrapper = httpContextWrapper;
			_configuration = configuration;
		}

		public ResolveUwebshopEntityUrlResult ResolveUwebshopEntityUrl()
		{
			string absolutePath = _httpContextWrapper.AbsolutePath.ToLowerInvariant();
			return ResolveUwebshopEntityUrl(absolutePath);
		}

		public ResolveUwebshopEntityUrlResult ResolveUwebshopEntityUrl(string absolutePath)
		{
			ResolveUwebshopEntityUrlResult result = default(ResolveUwebshopEntityUrlResult);
			//var absolutePath = _httpContextWrapper.AbsolutePath.ToLowerInvariant();

			if (absolutePath.StartsWith("/umbraco") || absolutePath.StartsWith("/base/") || absolutePath == "/umbraco/webservices/legacyajaxcalls.asmx/getsecondsbeforeuserlogout" || absolutePath == "/umbraco/ping.aspx" || absolutePath == "/" || absolutePath == "/favicon.ico")
				return result;

			if (absolutePath.EndsWith(".aspx")) absolutePath = absolutePath.Remove(absolutePath.Length - 5);

			if (absolutePath == "" || absolutePath == "/" || absolutePath.EndsWith(".js") || absolutePath.EndsWith(".ico") || absolutePath.EndsWith(".gif") || absolutePath.EndsWith(".css") || absolutePath.EndsWith(".jpg") || absolutePath.EndsWith(".jpeg") || absolutePath.EndsWith(".png") || absolutePath.EndsWith(".axd"))
				return result;

			if (_cmsApplication.IsReservedPathOrUrl(absolutePath))
				return result;
			if (_httpContextWrapper.PathPointsToPhysicalFile(absolutePath)) // relatief duur
				return result;

			string temp;
			if (UrlPointsToCatalogRepository(absolutePath, out temp))
				return result;

			if (!StoreHelper.GetAllStores().Any())
				return result;


			string paymentProviderRepositoryCmsNodeName = _cmsApplication.GetPaymentProviderRepositoryCMSNodeUrlName() ?? "PaymentProviders";
			string paymentProviderSectionCmsNodeName = _cmsApplication.GetPaymentProviderSectionCMSNodeUrlName() ?? "PaymentProviders";
			string paymentProviderPath = string.Format("/{0}/{1}/", paymentProviderRepositoryCmsNodeName, paymentProviderSectionCmsNodeName);
			if (absolutePath.Contains(paymentProviderPath))
			{
				absolutePath = absolutePath.TrimEnd('/');
				if (absolutePath.EndsWith(".aspx"))
					absolutePath = absolutePath.Replace(".aspx", string.Empty);

				string paymentProviderName = absolutePath.Split('/').Last();

				PaymentProvider paymentProviderNode = _paymentProviderService.GetPaymentProviderWithName(paymentProviderName, StoreHelper.CurrentStoreAlias);
				if (paymentProviderNode == null) return result;
				UwebshopRequest.Current.PaymentProvider = paymentProviderNode;
				result.Entity = paymentProviderNode;
				return result;
			}

			CatalogUrlResolveServiceResult catalogUrlResolveServiceResult = _catalogUrlSplitterService.DetermineCatalogUrlComponents(absolutePath); // relatief duur
			//if (!string.IsNullOrEmpty(catalogUrlResolveServiceResult.StoreNodeUrl))
				result.StoreUrl = catalogUrlResolveServiceResult.StoreNodeUrl;
			
			//else
			//{
			//	var store = StoreHelper.GetAllStores().FirstOrDefault(s => s.StoreURL.Replace("http://"))
			//}

			if (string.IsNullOrEmpty(catalogUrlResolveServiceResult.CatalogUrl) || catalogUrlResolveServiceResult.CatalogUrl == "/")
				return result;

			List<string> urlParts = catalogUrlResolveServiceResult.CatalogUrl.Trim('/').ToLower().Split('/').Where(urlvalue => urlvalue.Length > 0).ToList();
			string categoryUrlName = string.Join("/", urlParts) + "/";
			string categoryUrlNoProduct = string.Join("/", urlParts.Take(urlParts.Count - 1)) + "/"; // categoryUrlName.Replace(productUrlName, String.Empty);
			string productUrlName = urlParts.Last();

			Product product = _catalogUrlResolvingService.GetProductFromUrlName(categoryUrlNoProduct, productUrlName);
			if (product != null)
			{
				UwebshopRequest uwebshopRequest = UwebshopRequest.Current;
				uwebshopRequest.Category = _catalogUrlResolvingService.GetCategoryFromUrlName(categoryUrlNoProduct);
				uwebshopRequest.Product = product;
				result.Entity = product;
				result.CategoryUrl = categoryUrlNoProduct;
				result.ProductUrl = productUrlName;
				return result;
			}
			Category category = _catalogUrlResolvingService.GetCategoryFromUrlName(categoryUrlName);
			if (category != null)
			{
				UwebshopRequest.Current.Category = category;
				result.Entity = category;
				result.CategoryUrl = categoryUrlName;
			}
			return result;
		}

		public void Rewrite()
		{
			ResolveUwebshopEntityUrlResult result = ResolveUwebshopEntityUrl();
			uWebshopEntity content = result.Entity;

			if (content is PaymentProvider)
			{
				string rewritePath = StoreHelper.GetAllStores().First().StoreUrlWithoutDomain + "?paymentprovider=" + content.Name + _httpContextWrapper.QueryString;

				Log.Instance.LogDebug("Rewrite: StoreHelper.GetAllStores().First().StoreURL: " + StoreHelper.GetAllStores().First().StoreURL);

				if (!rewritePath.StartsWith("/") && !rewritePath.StartsWith("http"))
				{
					Log.Instance.LogDebug("Rewrite !rewritePath.StartsWith(/) && !rewritePath.StartsWith(http): /" + rewritePath);

					_httpContextWrapper.RewritePath("/" + rewritePath);
					return;
				}

				Log.Instance.LogDebug("Rewrite " + rewritePath);
				_httpContextWrapper.RewritePath(rewritePath);

				return;
			}

			if (content is Product)
			{
				// NB: keep &category= for backwards compatibility (bad razor files..)
				_httpContextWrapper.RewritePath(result.StoreUrl + "?resolvedProductId=" + content.Id + "&category=" + result.CategoryUrl + "&product=" + result.ProductUrl + _httpContextWrapper.QueryString);
				return;
			}
			if (content is Category)
			{
				_httpContextWrapper.RewritePath(result.StoreUrl + "?resolvedCategoryId=" + content.Id + "&category=" + result.CategoryUrl + _httpContextWrapper.QueryString);
			}
			//Log.Instance.LogDebug("URL could not be resolved for: " + _httpContextWrapper.AbsolutePath.ToLowerInvariant());

			// todo: fallback not working but making a real fix
			//var request = UwebshopRequest.Current;
			//if (request.CurrentStore == null)
			//{
			//	// kijken 
			//	var url = _httpContextWrapper.Url;
			//	if (!string.IsNullOrWhiteSpace(url.Trim('/')))
			//		request.CurrentStore = StoreHelper.GetAllStores().FirstOrDefault(s => url.Contains(s.StoreURL));
			//}

		}

		public void RedirectPermanentOldCatalogUrls()
		{
			if (!_configuration.PermanentRedirectOldCatalogUrls) return;

			string uwbsCategoryUrlIdentifier = _configuration.LegacyCategoryUrlIdentifier + "/";
			string uwbsProductUrlIdentifier = _configuration.LegacyProductUrlIdentifier + "/";

			string absolutePath = _httpContextWrapper.AbsolutePath.ToLowerInvariant();
			if (string.IsNullOrWhiteSpace(absolutePath) || absolutePath == "/")
				return;
			bool redirect = false;
			if (absolutePath.Contains("/" + uwbsCategoryUrlIdentifier))
			{
				redirect = true;
				absolutePath = absolutePath.Replace(uwbsCategoryUrlIdentifier, "");
			}
			if (absolutePath.Contains("/" + uwbsProductUrlIdentifier))
			{
				redirect = true;
				absolutePath = absolutePath.Replace(uwbsProductUrlIdentifier, "");
			}
			if (redirect)
			{
				_httpContextWrapper.RedirectPermanent(absolutePath);
			}
		}

		public bool UrlPointsToCatalogRepository(string absolutePath, out string catalogPath)
		{
			string catalogRepositoryCmsNodeName = _cmsApplication.GetCatalogRepositoryCMSNodeUrlName();
			string categoryRepositoryCmsNodeName = _cmsApplication.GetCategoryRepositoryCMSNodeUrlName();

			if (catalogRepositoryCmsNodeName != null && categoryRepositoryCmsNodeName != null)
			{
				string uWebshopCmsNodeName = _cmsApplication.GetuWebshopCMSNodeUrlName();

				string catalogpath = string.Format("/{0}/{1}", catalogRepositoryCmsNodeName, categoryRepositoryCmsNodeName);
				string catalogpathWithuWebshop = string.Format("/{0}/{1}/{2}", uWebshopCmsNodeName, catalogRepositoryCmsNodeName, categoryRepositoryCmsNodeName);

				if (absolutePath.StartsWith(catalogpathWithuWebshop))
				{
					catalogPath = absolutePath.Replace(catalogpathWithuWebshop, string.Empty);
					return true;
				}
				if (absolutePath.StartsWith(catalogpath))
				{
					catalogPath = absolutePath.Replace(catalogpath, string.Empty);
					return true;
				}
				// redirecting from internal umbraco urls is done in ApplicationEventHandler.UmbracoDefaultBeforeRequestInit
			}
			catalogPath = string.Empty;
			return false;
		}

		//private static string GetUrlWithStorePartRemovedAndAddStoreToRequestCacheNONEXAMINEALTERNATIVE(string absolutePath, HttpContext context)
		//{
		//	return ""; // not working, because uQuery doesn't work (at this point?)
		//	var dinges = new Node(1149);
		//	context.Response.Write("aa" + dinges.UrlName);


		//	var urlParts = absolutePath.Trim('/').ToLower().Split('/').Where(urlvalue => urlvalue.Length > 0).ToList();

		//	var firstNode = uQuery.GetNodeByUrl(urlParts.Any() ? urlParts.First() : "/"); // werkt niet, waarsch door gemis umbraco context
		//	if (firstNode == null)
		//	{
		//		firstNode = uQuery.GetNodeByUrl("/");
		//		if (firstNode == null)
		//			throw new Exception("dit kan écht niet");
		//	}
		//	var deepestNode = firstNode;
		//	for (int i = 2; i < urlParts.Count; i++)
		//	{
		//		var node = uQuery.GetNodeByUrl(string.Join("/", urlParts.Take(i)));
		//		if (node == null) break;
		//		deepestNode = node;
		//	}

		//	// find store!
		//	// TODO: recursive!
		//	var prop = deepestNode.GetProperty("storepicker");
		//	if (prop != null)
		//	{
		//		var store = DomainHelper.StoreById(int.Parse(prop.Value));
		//		HttpContext.Current.Items[StoreHelper.CurrentStoreCacheKey] = store;
		//	}

		//	var nodeUrl = library.NiceUrl(deepestNode.Id);
		//	return nodeUrl.Length > 1 ? absolutePath.Replace(nodeUrl, "/") : absolutePath;
		//}
	}
}