﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Security;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class OrderService : IOrderService
	{
		private readonly IOrderRepository _orderRepository;
		private readonly IStoreService _storeService;

		public OrderService(IStoreService storeService, IOrderRepository orderRepository)
		{
			_storeService = storeService;
			_orderRepository = orderRepository;
		}

		public OrderInfo CreateOrder()
		{
			return CreateOrder(_storeService.GetCurrentStore());
		}

		public OrderInfo CreateOrder(Store store)
		{
			if (store == null)
			{
				throw new Exception("Trying to create order without store");
			}
			if (string.IsNullOrEmpty(store.Alias))
			{
				throw new Exception("Store without alias. Please (re)publish store and re-index examine External index");
			}

			var order = new OrderInfo();

			order.CreatedInTestMode = store.EnableTestmode;

			order.ShippingCostsMightBeOutdated = true;

			UseDatabaseDiscounts(order);

			order.VATCheckService = IO.Container.Resolve<IVATCheckService>();
			InitializeNewOrder(order);

			order.CustomerInfo.CountryCode = !String.IsNullOrEmpty(store.DefaultCountryCode) ? store.DefaultCountryCode : store.CountryCode;

			MembershipUser currentMember = Membership.GetUser(); // TODO: dip

			if (currentMember != null)
			{
				//uWebshopOrders.SetCustomer(order.UniqueOrderId, currentMember.UserName); heeft toch geen effect (geen row in db)
				order.CustomerInfo.LoginName = currentMember.UserName;
				if (currentMember.ProviderUserKey != null)
				{
					//uWebshopOrders.SetCustomerId(order.UniqueOrderId, (int)currentMember.ProviderUserKey); heeft toch geen effect (geen row in db)
					order.CustomerInfo.CustomerId = (int) currentMember.ProviderUserKey;
				}
			}

			order.AddStore(store);

			OrderHelper.SetOrderCookie(order);

			return order;
		}

		// todo: find a logical name and place for functionality below
		public void UseStoredDiscounts(OrderInfo order, List<IOrderDiscount> discounts)
		{
			order.OrderDiscountsFactory = () => new List<IOrderDiscount>(discounts);
		}

		public void UseDatabaseDiscounts(OrderInfo order)
		{
			var discountService = IO.Container.Resolve<IDiscountService>();
			order.OrderDiscountsFactory = () => discountService.GetApplicableDiscountsForOrder(order).ToList();
		}

		public OrderInfo CreateCopyOfOrder(OrderInfo order)
		{
			InitializeNewOrder(order);
			order.PaymentInfo.TransactionId = string.Empty; // null?
			
			//_orderUpdatingService.Save(order); //uWebshopOrders.SetTransactionId(order.UniqueOrderId, string.Empty); // dit gebeurt toch gewoon via save?

			order.Save();

			return order;
		}

		public void StoreOrderFirstTimeHackishRefactorPlz(OrderInfo order)
		{
			//todo: huh? => refactor to StoreOrder?
			uWebshopOrders.SetOrderInfo(order.UniqueOrderId, DomainHelper.SerializeObjectToXmlString(order), OrderStatus.Incomplete);
		}

		/// <summary>
		///     Does the give order contains items that are out of stock?
		/// </summary>
		/// <param name="order"></param>
		/// <returns></returns>
		public bool OrderContainsOutOfStockItem(OrderInfo order)
		{
			bool value = ValidateStock(order, true, false);

			return !value;
		}

		/// <summary>
		///     Does the ordre contain specific Ids?
		/// </summary>
		/// <param name="orderinfo"></param>
		/// <param name="itemIdsToCheck"></param>
		/// <returns></returns>
		public bool OrderContainsItem(OrderInfo orderinfo, IEnumerable<int> itemIdsToCheck)
		{
			return GetApplicableOrderLines(orderinfo, itemIdsToCheck).Any();
		}

		public List<OrderLine> GetApplicableOrderLines(OrderInfo orderinfo, IEnumerable<int> itemIdsToCheck)
		{
			// todo: test
			var productIds = new List<int>();
			var productVariantIds = new List<int>();

			foreach (int itemId in itemIdsToCheck)
			{
				UwbsNode node = IO.Container.Resolve<ICMSEntityRepository>().GetByGlobalId(itemId);

				if (node == null)
				{
					continue;
				}

				if (node.NodeTypeAlias.StartsWith(Category.NodeAlias))
				{
					Category category = DomainHelper.GetCategoryById(itemId);
					if (category == null || category.Disabled) continue;
					productIds.AddRange(category.ProductsRecursive.Select(product => product.Id));
				}

				if (node.NodeTypeAlias.StartsWith(Product.NodeAlias) && !node.NodeTypeAlias.StartsWith(ProductVariant.NodeAlias))
				{
					productIds.Add(node.Id);
				}

				if (node.NodeTypeAlias.StartsWith(ProductVariant.NodeAlias))
				{
					productVariantIds.Add(node.Id);
				}

				if (node.NodeTypeAlias.StartsWith(PaymentProvider.NodeAlias) && orderinfo.PaymentInfo.Id != node.Id)
				{
					return new List<OrderLine>();
				}

				if (node.NodeTypeAlias == PaymentProviderMethodNode.NodeAlias && orderinfo.PaymentInfo.MethodId != node.Id.ToString())
				{
					return new List<OrderLine>();
				}

				if (node.NodeTypeAlias.StartsWith(ShippingProvider.NodeAlias) && orderinfo.ShippingInfo.Id != node.Id)
				{
					return new List<OrderLine>();
				}

				if (node.NodeTypeAlias == ShippingProviderMethodNode.NodeAlias && orderinfo.ShippingInfo.MethodId != node.Id.ToString())
				{
					return new List<OrderLine>();
				}
				
				if (node.NodeTypeAlias.StartsWith(DiscountProduct.NodeAlias) && orderinfo.OrderLines.Any(x => x.ProductInfo.DiscountId != node.Id))
				{
					return new List<OrderLine>();
				}
			}

			//todo:test
			return orderinfo.OrderLines.Where(orderLine => !productIds.Any() || productIds.Contains(orderLine.ProductInfo.Id)).Where(orderLine => !productVariantIds.Any() || orderLine.ProductInfo.ProductVariants.Any(x => productVariantIds.Contains(x.Id))).ToList();
		}

		/// <summary>
		///     Validate Customer infomormation in the order
		/// </summary>
		/// <param name="orderInfo"></param>
		/// <param name="clearValidation">Clear current order validation</param>
		/// <param name="writeToOrderValidation">Write output to order valdation</param>
		/// <returns></returns>
		public bool ValidateCustomer(OrderInfo orderInfo, bool clearValidation, bool writeToOrderValidation = true)
		{
			if (clearValidation)
			{
				orderInfo.OrderValidationErrors.Clear();
			}

			bool validated = true;

			if (string.IsNullOrEmpty(OrderHelper.CustomerInformationValue(orderInfo, "customerEmail")))
			{
				// no customer address available in the order
				//ClientErrorHandling.uWebshopException(dicCustomerEmailEmpty);
				//Log.Instance.LogDebug( "ORDERVALIDATIONERROR: customerEmail is Empty");
				if (writeToOrderValidation)
				{
					orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = "ValidationCustomerEmailEmpty", Value = "CustomerEmail Is Not Set"});
				}
				validated = false;
			}

			IDocumentTypeInfo orderDocumentType = IO.Container.Resolve<ICMSDocumentTypeService>().GetByAlias(Order.NodeAlias);

			validated &= ValidateInformationDoc(orderInfo, orderInfo.CustomerInfo.customerInformation, orderDocumentType.Properties.Where(property => property.Alias.StartsWith("customer")), writeToOrderValidation);
			validated &= ValidateInformationDoc(orderInfo, orderInfo.CustomerInfo.shippingInformation, orderDocumentType.Properties.Where(property => property.Alias.StartsWith("shipping")), writeToOrderValidation);
			validated &= ValidateInformationDoc(orderInfo, orderInfo.CustomerInfo.extraInformation, orderDocumentType.Properties.Where(property => property.Alias.StartsWith("extra")), writeToOrderValidation);

			return validated;
		}

		/// <summary>
		///     Validate all orderlines for validation issues
		/// </summary>
		/// <param name="orderInfo"></param>
		/// <param name="clearValidation">Clear current order validation</param>
		/// <param name="writeToOrderValidation">Write output to order</param>
		/// <returns></returns>
		public bool ValidateOrderlines(OrderInfo orderInfo, bool clearValidation, bool writeToOrderValidation = true)
		{
			if (clearValidation)
			{
				orderInfo.OrderValidationErrors.Clear();
			}

			bool valid = true;

			foreach (OrderLine orderline in orderInfo.OrderLines)
			{
				string replacedOrderlineDocTypeAlias = orderline.ProductInfo.Product.NodeTypeAlias.Replace(Product.NodeAlias, OrderedProduct.NodeAlias);

				IDocumentTypeInfo orderlineDocType = IO.Container.Resolve<ICMSDocumentTypeService>().GetByAlias(replacedOrderlineDocTypeAlias) ?? IO.Container.Resolve<ICMSDocumentTypeService>().GetByAlias(OrderedProduct.NodeAlias);

				var propertiesToValidate = orderlineDocType.Properties.Where(x => !OrderedProduct.DefaultProperties.Contains(x.Alias)).ToList();

				valid = ValidateInformationDoc(orderInfo, new XDocument(orderline.CustomData), propertiesToValidate, writeToOrderValidation);
			}

			return valid;
		}

		/// <summary>
		///     Validate the currentorder for stock
		/// </summary>
		/// <param name="orderInfo"></param>
		/// <param name="clearValidation">Clear current order validation</param>
		/// <param name="writeToOrderValidation">Write output to order valdation</param>
		/// <returns></returns>
		public bool ValidateStock(OrderInfo orderInfo, bool clearValidation, bool writeToOrderValidation = true)
		{
			if (clearValidation)
			{
				orderInfo.OrderValidationErrors.Clear();
			}

			bool validated = true;

			var allProducts = new Dictionary<int, int>();

			foreach (OrderLine orderline in orderInfo.OrderLines)
			{
				int productId = orderline.ProductInfo.Id;

				if (allProducts.ContainsKey(productId))
				{
					int currentItemCount = allProducts[productId];
					int itemCount = currentItemCount + orderline.ProductInfo.ItemCount.GetValueOrDefault(1);

					allProducts.Remove(productId);
					allProducts.Add(productId, itemCount);
				}
				else
				{
					int itemCount = orderline.ProductInfo.ItemCount.GetValueOrDefault(1);

					allProducts.Remove(productId);
					allProducts.Add(productId, itemCount);
				}
			}

			var allVariants = new Dictionary<int, int>();

			foreach (OrderLine orderline in orderInfo.OrderLines)
			{
				foreach (ProductVariantInfo variant in orderline.ProductInfo.ProductVariants)
				{
					int variantId = variant.Id;

					if (allVariants.ContainsKey(variantId))
					{
						int currentItemCount = allVariants[variantId];
						int itemCount = currentItemCount + orderline.ProductInfo.ItemCount.GetValueOrDefault(1);

						allVariants.Remove(variantId);
						allVariants.Add(variantId, itemCount);
					}
					else
					{
						int itemCount = orderline.ProductInfo.ItemCount.GetValueOrDefault(1);

						allVariants.Remove(variantId);
						allVariants.Add(variantId, itemCount);
					}
				}
			}

			// todo: a bit slow and untestable because of ProductInfo.Product
			foreach (var productKey in allProducts)
			{
				Product product = DomainHelper.GetProductById(productKey.Key);
				if (product == null) continue;

				// if stockstatus is enabled, use stock
				if (product.StockStatus && !product.UseVariantStock)
				{
					// does the product has stock
					if (product.Stock > 0)
					{
						// is the stock equal or more then the itemcount
						//if (product.Stock >= orderline.ProductInfo.ItemCount.GetValueOrDefault(1))
						//{
						//	// ok!
						//}

						// is the stock less then the itemcount, but backorder is enabled
						//if (product.Stock < orderline.ProductInfo.ItemCount.GetValueOrDefault(1) && product.BackorderStatus)
						//{
						//	// ok!
						//}

						if (product.Stock < productKey.Value && !product.BackorderStatus)
						{
							if (writeToOrderValidation)
							{
								orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = product.Id, Key = "ValidationProductOutOfStock", Value = "Product " + product.Title + " Is Out Of Stock",});
							}
							validated = false;
						}
						// if usevariantstock is true, do not update product stock
						//if (product.UseVariantStock)
						//{
						//	// ok!
						//}
					}
					// if product has no stock, but backorder is enabled
					//if (product.Stock <= 0 && product.BackorderStatus)
					//{
					//	// ok!
					//}
					// if variant has no stock, but backorder is disabled
					if (product.Stock <= 0 && !product.BackorderStatus)
					{
						if (writeToOrderValidation)
						{
							orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = product.Id, Key = "ValidationProductOutOfStock", Value = "Product " + product.Title + " Is Out Of Stock",});
						}
						validated = false;
					}
				}

				foreach (var variantkey in allVariants)
				{
					ProductVariant variant = DomainHelper.GetProductVariantById(variantkey.Key);

					// if stockstatus is enabled, use stock
					if (variant.StockStatus)
					{
						// does the variant has stock
						if (variant.Stock > 0)
						{
							// is the stock equal or more then the itemcount
							//if (variant.Variant.Stock >= orderline.ProductInfo.ItemCount.GetValueOrDefault(1))
							//{
							//	// ok!
							//}

							// is the stock less then the itemcount, but backorder is enabled
							//if (variant.Variant.Stock < orderline.ProductInfo.ItemCount.GetValueOrDefault(1) && variant.Variant.BackorderStatus)
							//{
							//	// ok!
							//}

							// is the stock less then the itemcount, but backorder is disabled
							if (variant.Stock < variantkey.Value && !variant.BackorderStatus)
							{
								if (writeToOrderValidation)
								{
									orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = variant.Id, Key = "ValidationProductVariantOutOfStock", Value = "Product " + product.Title + " variant " + variant.Title + " Is Out Of Stock",});
								}
								validated = false;
							}
						}
						// if variant has no stock, but backorder is enabled
						//if (variant.Variant.Stock <= 0 && variant.Variant.BackorderStatus)
						//{
						//	// ok!
						//}
						// if variant has no stock, but backorder is disabled
						if (variant.Stock <= 0 && !variant.BackorderStatus)
						{
							if (writeToOrderValidation)
							{
								orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = variant.Id, Key = "ValidationProductVariantOutOfStock", Value = "Product " + product.Title + " variant " + variant.Title + " Is Out Of Stock",});
							}
							validated = false;
						}
					}

					// if stockstatus is disabled, stock is not needed
					//if (!variant.Variant.StockStatus)
					//{
					//	// ok!
					//}
				}
			}

			return validated;
		}


		/// <summary>
		///     Validate the complete order for validation issues
		/// </summary>
		/// <param name="orderInfo"></param>
		/// <param name="writeToOrderValidation">Write output to order valdation</param>
		/// <returns></returns>
		public bool ValidateOrder(OrderInfo orderInfo, bool writeToOrderValidation = true)
		{
			orderInfo.OrderValidationErrors.Clear();

			bool validated = true;

			if (!orderInfo.TermsAccepted)
			{
				Log.Instance.LogWarning("ORDERVALIDATIONERROR: TERMS NOT ACCEPTED");
				if (writeToOrderValidation)
				{
					orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = "AcceptTermsError", Value = "Terms Not Accepted"});
				}
				validated = false;
			}

			if (orderInfo.Status == OrderStatus.Confirmed && orderInfo.ShippingCostsMightBeOutdated && ShippingProviderHelper.GetShippingProvidersForOrder(orderInfo).Count > 0)
			{
				bool shippingStillMatches = false;
				foreach (ShippingProvider shipPro in ShippingProviderHelper.GetShippingProvidersForOrder(orderInfo))
				{
					if (shipPro.Id == orderInfo.ShippingInfo.Id)
					{
						shippingStillMatches = true;
					}
				}

				if (shippingStillMatches == false)
				{
					Log.Instance.LogWarning("ORDERVALIDATIONERROR: ShippingCostOutdatedError");
					if (writeToOrderValidation)
					{
						orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = "ShippingCostOutdatedError", Value = "Shipping Not Updated Since Last Basket Change"});
					}

					validated = false;
				}
			}

			if (orderInfo.Status == OrderStatus.Confirmed && orderInfo.StoreInfo.Store == null)
			{
				Log.Instance.LogWarning("ORDERVALIDATIONERROR: NoStoreConnectedToThisOrder");
				if (writeToOrderValidation)
				{
					orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = "NoStoreConnectedToThisOrder", Value = "There Is No Store Connected To This Order"});
				}
				validated = false;
			}

			validated &= ValidateCustomer(orderInfo, false);
			validated &= ValidateStock(orderInfo, false);
			validated &= ValidateOrderlines(orderInfo, false);
			validated &= ValidateCustomValidations(orderInfo, false);

			foreach (OrderLine orderline in orderInfo.OrderLines)
			{
				Product product = orderline.ProductInfo.Product;
				if (product == null) continue;
				// for each productvariantgroup on the product node
				foreach (ProductVariantGroup productvariantGroup in product.ProductVariantGroups)
				{
					// test if the productgroup is required
					if (!productvariantGroup.ProductVariants.Any(x => x.Required) || orderline.ProductInfo.ProductVariants.Count(x => x.Group == productvariantGroup.Title) != 0)
						continue;
					// productvariantgroup has required variants (so whole group is required!) but non are in the productinfo == validation error
					Log.Instance.LogWarning("ORDERVALIDATIONERROR: Product Variant Group " + productvariantGroup.Title + " Is Required");
					if (writeToOrderValidation)
					{
						orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = orderline.ProductInfo.Id, Key = "ValidationProductVariantRequired", Value = "Product Variant Group " + productvariantGroup.Title + " Is Required"});
					}
					validated = false;
				}
			}

			if (orderInfo.PaymentInfo.Id != 0)
			{
				PaymentProvider paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);

				if ((paymentProvider.Type != PaymentProviderType.OfflinePaymentAtCustomer && paymentProvider.Type != PaymentProviderType.OfflinePaymentInStore) && !paymentProvider.Zone.CountryCodes.Contains(orderInfo.CustomerInfo.CountryCode))
				{
					// country code for customer does not match zones for payment provider.
					Log.Instance.LogWarning("ORDERVALIDATIONERROR: CUSTOMER COUNTRY DOES NOT MATCH PAYMENT PROVIDER");
					if (writeToOrderValidation)
					{
						orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = orderInfo.PaymentInfo.Id, Key = "ValidationCustomerCountryPaymentProviderMismatch", Value = "The Customer Country Does Not Match Countries Allowed For The Chosen Payment Provider"});
					}
					validated = false;
				}
			}

			if (orderInfo.PaymentInfo.Id == 0 && PaymentProviderHelper.GetPaymentProvidersForOrder(orderInfo).Count > 0)
			{
				Log.Instance.LogWarning("ORDERVALIDATIONERROR: PAYMENT PROVIDERS AVAILABLE BUT NOT CHOSEN");
				if (writeToOrderValidation)
				{
					orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = 0, Key = "ValidationNoPaymentProviderChosen", Value = "No Payment Provider Chosen"});
				}
				validated = false;
			}

			if (orderInfo.ShippingInfo.Id != 0)
			{
				var shippingProvider = ShippingProviderHelper.GetShippingProvider(orderInfo.ShippingInfo.Id);

				if (shippingProvider.Type != ShippingProviderType.Pickup && !shippingProvider.Zone.CountryCodes.Contains(orderInfo.CustomerInfo.ShippingCountryCode))
				{
					// country code for customer does not match zones for shipping provider.
					//ClientErrorHandling.uWebshopException(dicShippingProviderNotAvailable);
					Log.Instance.LogWarning("ORDERVALIDATIONERROR: SHIPPING COUNTRY DOES NOT MATCH SHIPPING PROVIDER");

					if (writeToOrderValidation)
					{
						orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = orderInfo.ShippingInfo.Id, Key = "ValidationShippingCountryShippingProviderMismatch", Value = "The Shipping Country Does Not Match Countries Allowed For The Chosen Shipping Provider"});
					}
					validated = false;
				}
			}

			if (orderInfo.ShippingInfo.Id == 0 && ShippingProviderHelper.GetShippingProvidersForOrder(orderInfo).Count > 0)
			{
				Log.Instance.LogWarning("ORDERVALIDATIONERROR: SHIPPING PROVIDERS AVAILABLE BUT NOT CHOSEN");
				if (writeToOrderValidation)
				{
					orderInfo.OrderValidationErrors.Add(new OrderValidationError {Id = 0, Key = "ValidationNoShippingProviderChosen", Value = "No Shipping Provider Chosen"});
				}
				validated = false;
			}

			// todo: deze validatie kan nooit 'af' gaan, omdat orderInfo.OrderDiscounts al gecheckt is op Counter
			foreach (IOrderDiscount discount in orderInfo.OrderDiscounts.Where(x => x.CounterEnabled && x.Counter == 0))
			{
				Log.Instance.LogWarning("ORDERVALIDATIONERROR OrderDiscountCounter == 0");
				if (writeToOrderValidation)
				{
					orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = "DiscountCountError", Value = "Discount with Id: " + discount.OriginalId + " counter is zero"});
				}
				validated = false;
			}


			return validated;
		}

		public bool ValidateCustomValidations(OrderInfo orderInfo, bool writeToOrderValidation = true)
		{
			bool validated = true;
			foreach (OrderInfo.CustomOrderValidation customValidation in orderInfo.CustomOrderValiations.Where(customValidation => !customValidation.condition(orderInfo)))
			{
				Log.Instance.LogWarning("VALIDATECUSTOMER ERROR CustomOrderValiations: " + customValidation.errorDictionaryItem);
				if (writeToOrderValidation)
				{
					orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = customValidation.errorDictionaryItem(orderInfo)});
				}
				validated = false;
			}
			return validated;
		}

		private void InitializeNewOrder(OrderInfo order)
		{
			order.UniqueOrderId = Guid.NewGuid();
			order.OrderNumber = GenerateIncompleteOrderNumber();
			order.StoreOrderReferenceId = null;
			order.DatabaseId = 0;

			//order.EventsOn = false;
			order.Paid = false;
			order.Status = OrderStatus.Incomplete;
			order.EventsOn = true;
		}

		/// <summary>
		///     GenerateOrderNumberForPreOrder
		/// </summary>
		/// <returns></returns>
		internal string GenerateIncompleteOrderNumber()
		{
			int lastOrderId = _orderRepository.DetermineLastOrderId();

			const string prefix = "[INCOMPLETE]-";
			return String.Format("{0}{1}", prefix, (lastOrderId + 1).ToString("0000"));
		}

		private static bool ValidateInformationDoc(OrderInfo orderInfo, XDocument customerInformationdoc, IEnumerable<IDocumentProperty> customerProperties, bool writeToOrderValidation = true)
		{
			if (customerInformationdoc == null) return true;

			bool validated = true;

			foreach (var propertyType in customerProperties)
			{
				XElement xmlElement = customerInformationdoc.Descendants().FirstOrDefault(x => x.Name.LocalName == propertyType.Alias);
				if (propertyType != null && xmlElement != null && (!string.IsNullOrEmpty(propertyType.ValidationRegularExpression) && !Regex.IsMatch(xmlElement.Value, propertyType.ValidationRegularExpression)))
				{
					Log.Instance.LogWarning("VALIDATECUSTOMER ERROR ValidationErrorRegEx: " + propertyType.Alias + " Is Not Correctly Set");
					if (writeToOrderValidation)
					{
						orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = "ValidationErrorRegEx", Alias = propertyType.Alias, Value = propertyType.Alias + " Is Not Correctly Set",});
					}
					validated = false;
				}

				if (propertyType != null && propertyType.Mandatory && (xmlElement == null || string.IsNullOrEmpty(xmlElement.Value)))
				{
					Log.Instance.LogWarning("VALIDATECUSTOMER ERROR ValidationErrorMandatory: " + propertyType.Alias + " Is Not Set");
					if (writeToOrderValidation)
					{
						orderInfo.OrderValidationErrors.Add(new OrderValidationError {Key = "ValidationErrorMandatory", Alias = propertyType.Alias, Value = propertyType.Alias + " Is Not Set",});
					}
					validated = false;
				}
			}
			return validated;
		}
	}
}