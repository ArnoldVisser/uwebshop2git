﻿using System.Linq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	class StoreProductUrlService : IStoreProductUrlService
	{
		private readonly ICatalogUrlService _catalogUrlService;
		private readonly ICategoryCatalogUrlService _categoryCatalogUrlService;
		private readonly IUrlFormatService _urlFormatService;
		private readonly IUwebshopRequestService _requestService;

		public StoreProductUrlService(ICatalogUrlService catalogUrlService, ICategoryCatalogUrlService categoryCatalogUrlService, IUrlFormatService urlFormatService, IUwebshopRequestService requestService)
		{
			_catalogUrlService = catalogUrlService;
			_categoryCatalogUrlService = categoryCatalogUrlService;
			_urlFormatService = urlFormatService;
			_requestService = requestService;
		}

		public string GetUrl(IProduct product, Store store)
		{
			var category = _requestService.Current.Category;
			if (category == null || !product.Categories.Contains(category))
			{
				return GetCanonicalUrl(product, store);
			}

			return BuildUrl(store, category, product);
		}

		public string GetCanonicalUrl(IProduct product, Store store)
		{
			return BuildUrl(store, product.Categories.FirstOrDefault(), product);
		}

		private string BuildUrl(Store store, ICategory category, IProduct product)
		{
			var storeUrl = store == null ? string.Empty : store.StoreURL.TrimEnd('/');

			// todo: products can get their own url using storeUrl/productUrlName, but resolving and name conflicts need to be fixed
			if (category == null) return storeUrl;

			var resultUrl = _catalogUrlService.BuildUrl(storeUrl, _categoryCatalogUrlService.GetUrl(category), product.UrlName);

			return _urlFormatService.FormatUrl(resultUrl);
		}
	}
}