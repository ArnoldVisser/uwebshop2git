﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class ProductService : MultiStoreEntityService<Product>, IProductService
	{
		private readonly ICategoryService _categoryService;
		private readonly IStockService _stockService;
		private new readonly IStoreService _storeService;

		public ProductService(IProductRepository productRepository, IStoreService storeService, IStockService stockService, ICategoryService categoryService) : base(productRepository, storeService)
		{
			_storeService = storeService;
			_stockService = stockService;
			_categoryService = categoryService;
		}

		public int GetStockForProduct(int productId)
		{
			return _stockService.GetStockForUwebshopEntityWithId(productId);
		}

		public ProductInfo CreateProductInfoByProductId(int productId, IOrderInfo order, string storeAlias, int itemCount)
		{
			return new ProductInfo(GetById(productId, storeAlias), order, itemCount);
		}

		public List<Product> GetAllEnabledAndWithCategory(string storeAlias)
		{
			return GetAll(storeAlias, true).Where(x => !x.Disabled && x.Categories.Any()).ToList();
		}

		public void ReloadWithVATSetting()
		{
			// todo: move functionality to repository
			bool includingVAT = IO.Container.Resolve<ISettingsService>().IncludingVat;
			foreach (Store store in _storeService.GetAllStores())
			{
				GetAll(store.Alias, true).ForEach(product => product.PricesIncludingVat = includingVAT);
			}
		}

		protected override void AfterEntitiesLoadedFromRepository(List<Product> entities, string storeAlias)
		{
		}
	}
}