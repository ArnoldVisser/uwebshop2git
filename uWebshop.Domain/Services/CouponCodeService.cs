﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.DataAccess;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Model;

namespace uWebshop.Domain.Services
{
	internal class CouponCodeService : ICouponCodeService
	{
		public IEnumerable<Coupon> GetAll()
		{
			return UwebshopCoupons.GetAll().Select(c => new Coupon(c));
		}

		public IEnumerable<Coupon> GetAllForDiscount(int discountId)
		{
			return UwebshopCoupons.GetAllForDiscount(discountId).Select(c => new Coupon(c));
		}

		public Coupon Get(int discountId, string couponCode)
		{
			var couponData = UwebshopCoupons.Get(discountId, couponCode);
			return couponData == null ? null : new Coupon(couponData);
		}

		public IEnumerable<Coupon> GetAllWithCouponcode(string couponCode)
		{
			return UwebshopCoupons.GetAllWithCouponcode(couponCode).Select(c => new Coupon(c));
		}

		public void Save(Coupon coupon)
		{
			UwebshopCoupons.Save(coupon.ToData());
		}
	}
}