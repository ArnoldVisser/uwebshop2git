﻿using System;
using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.Services
{
	internal class CatalogUrlResolvingService : ICatalogUrlResolvingService
	{
		private readonly ICategoryService _categoryService;

		public CatalogUrlResolvingService(ICategoryService categoryService)
		{
			_categoryService = categoryService;
		}

		/// <summary>
		///     Return an Product based on the urlname
		/// </summary>
		/// <param name="categoryUrlName"></param>
		/// <param name="productUrlName"></param>
		/// <returns></returns>
		public Product GetProductFromUrlName(string categoryUrlName, string productUrlName)
		{
			if (String.IsNullOrEmpty(productUrlName) || String.IsNullOrEmpty(categoryUrlName)) return null;
			Category categoryNode = GetCategoryFromUrlName(categoryUrlName);

			if (productUrlName.EndsWith("/"))
			{
				productUrlName = productUrlName.Remove(productUrlName.Length - 1);
			}

			if (categoryNode != null && categoryNode.Products != null)
			{
				// throw new Exception("asf " + categoryUrlName + " " + productUrlName + " " + (categoryNode == null) + " " + categoryNode.Products.Where(p => p.LocalizedUrl.ToLowerInvariant() == "toy-boat").Select(p => p.Id).FirstOrDefault());
				return categoryNode.Products.FirstOrDefault(product => product != null && product.UrlName.ToLowerInvariant() == productUrlName.ToLowerInvariant());
			}

			return null;
		}

		/// <summary>
		///     Returns the Category based on the URLname
		/// </summary>
		/// <param name="categoryUrlName"></param>
		/// <returns></returns>
		public Category GetCategoryFromUrlName(string categoryUrlName)
		{
			if (String.IsNullOrEmpty(categoryUrlName) || categoryUrlName == "/")
				return null;

			if (categoryUrlName.EndsWith("/")) // todo trimend
			{
				categoryUrlName = categoryUrlName.Remove(categoryUrlName.Length - 1);
			}

			List<string> catalogUrls = categoryUrlName.ToLower().Split('/').ToList();
			catalogUrls.Reverse();

			List<Category> categories = _categoryService.GetAll(StoreHelper.CurrentStoreAlias);
			IEnumerable<Category> matchingcategories = categories.Where(x => x.UrlName.ToLower() == catalogUrls.FirstOrDefault()); //.ToList();
			IEnumerable<CategoryTreeWalker> possibilities = matchingcategories.Select(x => new CategoryTreeWalker(x)); //.ToList();

			//Nike/Shoes/Running
			//Running/Shoes/Nike
			//Log.Instance.LogDebug( "foreach (var url in catalogUrls.Skip(1)) start: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			foreach (string url in catalogUrls.Skip(1)) // 0ms want leeg
			{
				string urlVariable = url;
				possibilities = possibilities.Where(x => Category.IsAlias(x.ParentNodeTypeAlias) && x.ParentNodeTypeAlias != Catalog.CategoryRepositoryNodeAlias && x.ParentCategory.UrlName.ToLower() == urlVariable
					//&& !x.ParentCategory.Disabled
					).SelectMany(currentLevelCategory => currentLevelCategory.GetParentCategories());
				// TODO!!!!!!!! test
			}

			IEnumerable<CategoryTreeWalker> categoryTreeWalkers = possibilities;

			if (categoryTreeWalkers.Count() > 1)
			{
				categoryTreeWalkers = categoryTreeWalkers.Where(x => x.ParentNodeTypeAlias == Catalog.CategoryRepositoryNodeAlias);
			}

			return categoryTreeWalkers.Select(ctw => ctw.Category).FirstOrDefault();
		}

		private class CategoryTreeWalker
		{
			public Category Category;
			public ICategory ParentCategory;
			public string ParentNodeTypeAlias;

			private CategoryTreeWalker()
			{
			}

			public CategoryTreeWalker(Category category)
			{
				Category = category;
				ParentCategory = category.ParentCategory;
				ParentNodeTypeAlias = category.ParentNodeTypeAlias;
			}

			public IEnumerable<CategoryTreeWalker> GetParentCategories()
			{
				// TODO: IMPORTANT add parents from categories property!!!
				if (ParentCategory == null) return new List<CategoryTreeWalker>();
				return new List<CategoryTreeWalker> {new CategoryTreeWalker {Category = Category, ParentCategory = ParentCategory.ParentCategory, ParentNodeTypeAlias = ParentCategory.ParentNodeTypeAlias}};
				// wrong: return Category.Categories.Select(category => new CategoryTreeWalker {Category = Category, ParentCategory = Category.ParentCategory, ParentNodeTypeAlias = Category.ParentNodeTypeAlias});
			}
		}
	}
}