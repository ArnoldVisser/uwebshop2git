﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.BaseClasses
{
	/// <summary>
	/// 
	/// </summary>
	[DataContract(Name = "MultiStoreUwebshopContent", Namespace = "", IsReference = true)]
	public class MultiStoreUwebshopContent : uWebshopEntity
	{ 
		/// <summary>
		/// The title alias
		/// </summary>
		public const string TitleAlias = "title";
		private int? _template;

		/// <summary>
		///     Gets the title of the content
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = TitleAlias, DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#Title", Description = "#TitleDescription", Mandatory = true, SortOrder = 1)]
		public string Title { get; set; }

		/// <summary>
		///     Gets the URL of the content
		/// </summary>
		[XmlIgnore]
		[ContentPropertyType(Alias = "url", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#Url", Description = "#UrlDescription", Mandatory = false, SortOrder = 2)]
		public string URL { get; set; }

		/// <summary>
		///     Gets the short description of the content
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "metaDescription", DataType = DataType.TextboxMultiple, Tab = ContentTypeTab.Global, Name = "#MetaDescription", Description = "#MetaDescriptionDescription", Mandatory = false, SortOrder = 3)]
		public string MetaDescription { get; set; }

		/// <summary>
		///     Gets the long description of the content
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "description", DataType = DataType.RichText, Tab = ContentTypeTab.Details, Name = "#Description", Description = "#DescriptionDescription", Mandatory = false, SortOrder = 6)]
		public string Description { get; set; }
	
		/// <summary>
		///     Gets the template of the product
		/// </summary>
		[DataMember]
		public int Template
		{
			get
			{
				// TODO: waarom werkt template niet via normale gang met LoadDataFromExamine? ==> omdat 0 = use default en dan moet je uit de contenttype halen..
				if (_template.HasValue && _template.GetValueOrDefault() != 0) return _template.GetValueOrDefault();
				return (_template = Node.template).GetValueOrDefault();


				//var property = StoreHelper.GetMultiStoreItem(Id, "template");

				//if (string.IsNullOrEmpty(property) || property == "0")
				//{
				//    return (_template = Node.template).GetValueOrDefault();
				//}

				//int templateId;
				//int.TryParse(property, out templateId);
				//return (_template = templateId).GetValueOrDefault();
			}
			set { }
		}

		/// <summary>
		///     Is this content enabled?
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "disable", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Global, Name = "#Disable", Description = "#DisableDescription", Mandatory = false, SortOrder = 7)]
		public override bool Disabled { get; set; }

		internal static void LoadDataFromPropertyProvider(MultiStoreUwebshopContent entity, string storeAlias, IPropertyProvider fields)
		{
			entity.Disabled = StoreHelper.GetMultiStoreDisableExamine(storeAlias, fields);

			entity.Title = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary(TitleAlias, storeAlias, fields);
			entity.URL = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("url", storeAlias, fields);
			entity.MetaDescription = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary("metaDescription", storeAlias, fields);

			entity.Description = StoreHelper.ReadMultiStoreItemFromPropertiesDictionary(fields.ContainsKey("RTEItemdescription") ? "RTEItemdescription" : "description", storeAlias, fields);

			entity._template = StoreHelper.GetMultiStoreIntValue("template", storeAlias, fields);
		}

		/// <summary>
		/// Gets the property.
		/// </summary>
		/// <param name="propertyAlias">The property alias.</param>
		/// <returns></returns>
		public string GetProperty(string propertyAlias)
		{
			var property = Node.GetMultiStoreItem(propertyAlias);
			if (property == null) return string.Empty;
			return property.Value;
		}
	}
}