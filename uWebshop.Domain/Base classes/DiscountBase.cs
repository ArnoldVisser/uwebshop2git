﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.Domain.ContentTypes;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Domain.BaseClasses
{
	// todo: !!! proper loading of properties !!!

	/// <summary>
	/// 
	/// </summary>
	[DataContract(Namespace = "", IsReference = true)]
	public abstract class DiscountBase : uWebshopEntity // MultiStoreUwebshopContent
	{
		private string _description;
		private bool? _disable;
		private string _title;

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscountBase"/> class.
		/// </summary>
		/// <param name="nodeId">The node unique identifier.</param>
		protected DiscountBase(int nodeId) : base(nodeId)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DiscountBase"/> class.
		/// </summary>
		protected DiscountBase()
		{
		}

		/// <summary>
		///     Gets the title of the content
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "title", DataType = DataType.String, Tab = ContentTypeTab.Global, Name = "#Title", Description = "#TitleDescription", Mandatory = true, SortOrder = 1)]
		public string Title
		{
			get { return _title ?? (_title = StoreHelper.GetMultiStoreItem(Id, "title") ?? string.Empty); }
			set { }
		}

		/// <summary>
		///     Gets the long description of the content
		/// </summary>
		[DataMember]
		[ContentPropertyType(Alias = "description", DataType = DataType.RichText, Tab = ContentTypeTab.Global, Name = "#Description", Description = "#DescriptionDescription", Mandatory = false, SortOrder = 2)]
		public string Description
		{
			get { return IO.Container.Resolve<ICMSApplication>().ParseInternalLinks(_description ?? (_description = StoreHelper.GetMultiStoreItem(Id, "description") ?? string.Empty)); }
			set { }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [disable].
		/// </summary>
		/// <value>
		///   <c>true</c> if [disable]; otherwise, <c>false</c>.
		/// </value>
		[ContentPropertyType(Alias = "disable", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Global, Name = "#Disable", Description = "#DisableDescription", SortOrder = 3)]
		public bool Disable
		{
			get { return _disable ?? (_disable = StoreHelper.GetMultiStoreDisable(Id)).GetValueOrDefault(); }
			set { }
		}

		
		/// <summary>
		///     Ranges from the range data type
		/// </summary>
		[DataMember]
		public List<Range> Ranges
		{
			get { return Range.CreateFromString(RangesString); }
			set { }
		}

		#region global tab

		private bool? _counterEnabled;
		private DiscountType? _discountType;

		private int? _discountValue;

		private string _rangesString;

		/// <summary>
		///     Type of discount (Percentage, Amount, Free shipping)
		/// </summary>
		[ContentPropertyType(Alias = "discountType", DataType = DataType.DiscountType, Tab = ContentTypeTab.Details, Name = "#DiscountType", Description = "#DiscountTypeDescription", SortOrder = 4)]
		public DiscountType DiscountType
		{
			get
			{
				if (_discountType.HasValue) return _discountType.GetValueOrDefault();
				string property = StoreHelper.GetMultiStoreItem(Id, "discountType");

				if (property != null && !string.IsNullOrEmpty(property))
					return (_discountType = (DiscountType) Enum.Parse(typeof (DiscountType), property)).GetValueOrDefault();

				return (_discountType = DiscountType.Amount).GetValueOrDefault();
			}
			set { _discountType = value; }
		}

		/// <summary>
		///     The discount value in cents or percentage
		/// </summary>
		[ContentPropertyType(Alias = "discount", DataType = DataType.Price, Tab = ContentTypeTab.Details, Name = "#Discount", Description = "#DiscountDescription", SortOrder = 5)]
		public int DiscountValue
		{
			get
			{
				if (!_discountValue.HasValue)
				{
					if (Ranges != null && Ranges.Any())
					{
						Range range = Ranges.FirstOrDefault(x => x.From <= 1 && x.PriceInCents != 0);
						if (range != null)
						{
							_discountValue = range.PriceInCents;
							return _discountValue.GetValueOrDefault();
						}
					}
					int discountValue;
					int.TryParse(StoreHelper.GetMultiStoreItem(Id, "discount") ?? string.Empty, out discountValue);
					_discountValue = discountValue;
				}
				return _discountValue.GetValueOrDefault();
			}
			set { _discountValue = value; }
		}

		/// <summary>
		/// Gets the ranged discount value.
		/// </summary>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public int RangedDiscountValue(int count)
		{
			var range = Ranges.FindRangeForValue(count);
			return range != null ? range.PriceInCents : DiscountValue;
		}

		/// <summary>
		/// Gets or sets the ranges string.
		/// </summary>
		/// <value>
		/// The ranges string.
		/// </value>
		[ContentPropertyType(Alias = "ranges", DataType = DataType.Ranges, Tab = ContentTypeTab.Details, Name = "#Ranges", Description = "#RangesDescription", SortOrder = 5)]
		public string RangesString
		{
			get { return _rangesString ?? (_rangesString = StoreHelper.GetMultiStoreItem(Id, "ranges")); }
			set { _rangesString = value; }
		}

		/// <summary>
		/// Gets or sets a value indicating whether [counter enabled].
		/// </summary>
		/// <value>
		///   <c>true</c> if [counter enabled]; otherwise, <c>false</c>.
		/// </value>
		[ContentPropertyType(Alias = "countdownEnabled", DataType = DataType.TrueFalse, Tab = ContentTypeTab.Details, Name = "#CountdownEnabled", Description = "#CountdownEnabledDescription", SortOrder = 6)]
		public bool CounterEnabled
		{
			get
			{
				if (!_counterEnabled.HasValue)
				{
					var propertyValue = StoreHelper.GetMultiStoreItem(Id, "countdownEnabled");
					_counterEnabled = propertyValue == "enable" || propertyValue == "1" || propertyValue == "true";
				}
				return _counterEnabled.GetValueOrDefault();
			}
			set { }
		}

		/// <summary>
		/// Gets or sets the counter.
		/// </summary>
		/// <value>
		/// The counter.
		/// </value>
		[ContentPropertyType(Alias = "countdown", DataType = DataType.Numeric, Tab = ContentTypeTab.Details, Name = "#Countdown", Description = "#CountdownDescription", SortOrder = 7)]
		public int Counter
		{
			get { return StoreHelper.GetMultiStoreStock(Id); }
			set { }
		}

		#endregion

		#region condition tab
		private List<string> _memberGroups;
		/// <summary>
		///     Membergroups this discount is valid for
		/// </summary>
		[ContentPropertyType(Alias = "memberGroups", DataType = DataType.MemberGroups, Tab = ContentTypeTab.Conditions, Name = "#MemberGroups", Description = "#MemberGroupsDescription", SortOrder = 99)]
		public List<string> MemberGroups
		{
			get { return _memberGroups ?? (_memberGroups = StoreHelper.GetMultiStoreItem(Id, "memberGroups").Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList()); }
			set { _memberGroups = value; }
		}

		#endregion
	}
}