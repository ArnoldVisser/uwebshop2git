﻿namespace uWebshop.Domain.Interfaces
{
	interface IProductUrlService
	{
		string GetUrl(IProduct product);
		string GetCanonicalUrl(IProduct product);
	}
}
