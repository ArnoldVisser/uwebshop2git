﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IIocContainer
	{
		/// <summary>
		/// Resolves this instance.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		T Resolve<T>() where T : class;
		/// <summary>
		/// Registers the type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <typeparam name="T1">The type of the 1.</typeparam>
		void RegisterType<T, T1>() where T1 : T;
		/// <summary>
		/// Registers the instance.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="instance">The instance.</param>
		void RegisterInstance<T>(T instance);
		/// <summary>
		/// Sets the default service factory.
		/// </summary>
		/// <param name="serviceFactory">The service factory.</param>
		void SetDefaultServiceFactory(IServiceFactory serviceFactory);
	}
}