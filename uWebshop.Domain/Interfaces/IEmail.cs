﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IEmail
	{
		/// <summary>
		/// Adds an attachment.
		/// </summary>
		/// <param name="fileName">Name of the file.</param>
		void AddAttachment(string fileName);
	}
}