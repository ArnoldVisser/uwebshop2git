﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IProductRepository : IEntityRepository<Product>
	{
	}
}