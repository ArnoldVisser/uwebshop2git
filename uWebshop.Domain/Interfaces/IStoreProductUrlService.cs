﻿namespace uWebshop.Domain.Interfaces
{
	interface IStoreProductUrlService
	{
		string GetUrl(IProduct product, Store store);
		string GetCanonicalUrl(IProduct product, Store store);
	}
}