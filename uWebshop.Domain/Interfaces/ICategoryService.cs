﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICategoryService : IEntityService<Category>
	{
		/// <summary>
		/// Gets all root categories.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		List<Category> GetAllRootCategories(string storeAlias);

		/// <summary>
		/// Fully resets the cache.
		/// </summary>
		void FullResetCache();
	}
}