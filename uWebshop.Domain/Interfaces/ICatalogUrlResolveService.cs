﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICatalogUrlSplitterService
	{
		/// <summary>
		/// Determines the catalog URL components.
		/// </summary>
		/// <param name="url">The URL.</param>
		/// <returns></returns>
		CatalogUrlResolveServiceResult DetermineCatalogUrlComponents(string url);
	}

	/// <summary>
	/// 
	/// </summary>
	public struct CatalogUrlResolveServiceResult
	{
		/// <summary>
		/// The catalog URL
		/// </summary>
		public string CatalogUrl;
		/// <summary>
		/// The store
		/// </summary>
		public Store Store;
		/// <summary>
		/// The store node URL
		/// </summary>
		public string StoreNodeUrl;
	}
}