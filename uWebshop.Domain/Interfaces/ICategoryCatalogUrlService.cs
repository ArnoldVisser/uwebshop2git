namespace uWebshop.Domain.Interfaces
{
	interface ICategoryCatalogUrlService
	{
		string GetUrl(ICategory category);
	}
}