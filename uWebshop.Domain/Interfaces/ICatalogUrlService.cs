﻿namespace uWebshop.Domain.Interfaces
{
	interface ICatalogUrlService
	{
		string BuildUrl(string baseUrl, string categoriesUrl, string productUrl);
	}
}