﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IProductDiscountRepository
	{
		/// <summary>
		/// Gets the discount by product unique identifier.
		/// </summary>
		/// <param name="productId">The product unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		DiscountProduct GetDiscountByProductId(int productId, string storeAlias);

		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="discountId">The discount unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		DiscountProduct GetById(int discountId, string storeAlias);
	}
}