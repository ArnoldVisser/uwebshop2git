﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICatalogUrlResolvingService
	{
		/// <summary>
		///     Return an Product based on the urlname
		/// </summary>
		/// <param name="categoryUrlName"></param>
		/// <param name="productUrlName"></param>
		/// <returns></returns>
		Product GetProductFromUrlName(string categoryUrlName, string productUrlName);

		/// <summary>
		///     Returns the Category based on the URLname
		/// </summary>
		/// <param name="categoryUrlName"></param>
		/// <returns></returns>
		Category GetCategoryFromUrlName(string categoryUrlName);
	}
}