﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IEntityRepository<T>
	{
		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		T GetById(int id, string storeAlias);
		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		List<T> GetAll(string storeAlias);
		/// <summary>
		/// Reloads the data.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="storeAlias">The store alias.</param>
		void ReloadData(T entity, string storeAlias);
	}
}