﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IProductVariantService : IEntityService<ProductVariant>
	{
		/// <summary>
		/// Fulls the reset cache.
		/// </summary>
		void FullResetCache();
	}
}