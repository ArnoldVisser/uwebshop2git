﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IPaymentProviderService
	{
		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		IEnumerable<PaymentProvider> GetAll(string storeAlias);
		/// <summary>
		/// Gets the name of the payment provider with.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		PaymentProvider GetPaymentProviderWithName(string name, string storeAlias);
		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		PaymentProvider GetById(int id, string storeAlias);
		/// <summary>
		/// Loads the data.
		/// </summary>
		/// <param name="paymentProvider">The payment provider.</param>
		void LoadData(PaymentProvider paymentProvider);
	}
}