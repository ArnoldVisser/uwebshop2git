﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IDiscountService
	{
		/// <summary>
		/// Gets the applicable discounts for order.
		/// </summary>
		/// <param name="orderInfo">The order information.</param>
		/// <returns></returns>
		List<IOrderDiscount> GetApplicableDiscountsForOrder(OrderInfo orderInfo);

		/// <summary>
		/// Gets the order discount by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		IOrderDiscount GetOrderDiscountById(int id);

		/// <summary>
		/// Gets the product discount by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		DiscountProduct GetProductDiscountById(int id);

		/// <summary>
		/// Gets the discount by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		IDiscount GetById(int id);
	}
}