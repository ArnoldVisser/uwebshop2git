﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	internal interface IProduct
	{
		bool HasCategories { get; }
		IEnumerable<ICategory> Categories { get; }
		int Id { get; }
		string UrlName { get; }
		int ParentId { get; }
	}
}