﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IShippingProviderService
	{
		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		IEnumerable<ShippingProvider> GetAll(string storeAlias);
		/// <summary>
		/// Gets the name of the payment provider with.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		ShippingProvider GetPaymentProviderWithName(string name, string storeAlias);
		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		ShippingProvider GetById(int id, string storeAlias);
		/// <summary>
		/// Loads the data.
		/// </summary>
		/// <param name="shippingProvider">The shipping provider.</param>
		void LoadData(ShippingProvider shippingProvider);
	}
}