﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICountryRepository
	{
		/// <summary>
		/// Gets all countries.
		/// </summary>
		/// <returns></returns>
		List<Country> GetAllCountries();
		/// <summary>
		/// Gets all countries.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		List<Country> GetAllCountries(string storeAlias);
	}

	/// <summary>
	/// 
	/// </summary>
	public interface IVATCountryRepository : ICountryRepository
	{
	}
}