namespace uWebshop.Domain.Interfaces
{
	interface IUrlFormatService
	{
		string FormatUrl(string url);
	}
}