﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IDiscount
	{
		/// <summary>
		/// Gets a value indicating whether the discount is once per customer.
		/// </summary>
		/// <value>
		///   <c>true</c> if the discount is once per customer; otherwise, <c>false</c>.
		/// </value>
		bool OncePerCustomer { get; }
		/// <summary>
		/// Gets the unique identifier.
		/// </summary>
		/// <value>
		/// The unique identifier.
		/// </value>
		int Id { get; }
		/// <summary>
		/// Gets the minimum order amount in cents.
		/// </summary>
		/// <value>
		/// The minimum order amount in cents.
		/// </value>
		int MinimumOrderAmountInCents { get; }
		/// <summary>
		/// Gets a value indicating whether the counter is enabled.
		/// </summary>
		/// <value>
		///   <c>true</c> if the counter is enabled; otherwise, <c>false</c>.
		/// </value>
		bool CounterEnabled { get; }
		/// <summary>
		/// Gets the counter.
		/// </summary>
		/// <value>
		/// The counter.
		/// </value>
		int Counter { get; }
	}
}