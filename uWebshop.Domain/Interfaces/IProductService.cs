﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IProductService : IEntityService<Product>
	{
		/// <summary>
		/// Gets the stock for product.
		/// </summary>
		/// <param name="productId">The product unique identifier.</param>
		/// <returns></returns>
		int GetStockForProduct(int productId);

		/// <summary>
		/// Creates the product information by product unique identifier.
		/// </summary>
		/// <param name="productId">The product unique identifier.</param>
		/// <param name="order">The order.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <param name="itemCount"></param>
		/// <returns></returns>
		ProductInfo CreateProductInfoByProductId(int productId, IOrderInfo order, string storeAlias, int itemCount);
		/// <summary>
		/// Gets all enabled and with category.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		List<Product> GetAllEnabledAndWithCategory(string storeAlias);
		/// <summary>
		/// Reloads the with vat setting.
		/// </summary>
		void ReloadWithVATSetting();
		/// <summary>
		/// Fully resets the cache.
		/// </summary>
		void FullResetCache();
	}
}