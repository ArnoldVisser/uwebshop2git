﻿using System.Collections.Generic;
using uWebshop.Common;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IOrderDiscount : IDiscount
	{
		/// <summary>
		/// Gets a value indicating whether this discount is disabled.
		/// </summary>
		/// <value>
		///   <c>true</c> if this discount is disabled; otherwise, <c>false</c>.
		/// </value>
		bool Disable { get; }

		/// <summary>
		///     List of products this discount applies to
		/// </summary>
		List<int> RequiredItemIds { get; }

		/// <summary>
		///     Discount condition (None, OnTheXthItem, PerSetOfItems)
		/// </summary>
		DiscountOrderCondition Condition { get; }

		/// <summary>
		///     Number of items required for OnTheXthItem and PerSetOfItems conditions
		/// </summary>
		int NumberOfItemsCondition { get; }

		/// <summary>
		///     Minimum amount of the order before discount is valid
		/// </summary>
		int MinimalOrderAmount { get; }

		/// <summary>
		///     The discount value in cents or percentage
		/// </summary>
		int DiscountValue { get; }

		/// <summary>
		///     Information on the ranges for this discount
		/// </summary>
		string RangesString { get; }

		/// <summary>
		///     Code of the coupon
		/// </summary>
		string CouponCode { get; }

		/// <summary>
		///     Type of discount (Percentage, Amount, Free shipping)
		/// </summary>
		DiscountType DiscountType { get; }

		/// <summary>
		///     Membergroups this discount is valid for
		/// </summary>
		List<string> MemberGroups { get; }

		/// <summary>
		/// Gets the original unique identifier.
		/// </summary>
		/// <value>
		/// The original unique identifier.
		/// </value>
		int OriginalId { get; }

		/// <summary>
		/// Gets the affected orderlines.
		/// </summary>
		/// <value>
		/// The affected orderlines.
		/// </value>
		List<int> AffectedOrderlines { get; }

		/// <summary>
		/// Gets the title.
		/// </summary>
		/// <value>
		/// The title.
		/// </value>
		string Title { get; }
	}
}