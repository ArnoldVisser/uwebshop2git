﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IUwebshopEntity
	{
		/// <summary>
		/// Gets the unique identifier.
		/// </summary>
		/// <value>
		/// The unique identifier.
		/// </value>
		int Id { get; }
		/// <summary>
		/// Gets a value indicating whether the entity is disabled.
		/// </summary>
		/// <value>
		///   <c>true</c> if disabled; otherwise, <c>false</c>.
		/// </value>
		bool Disabled { get; }
		/// <summary>
		/// Gets the node type alias.
		/// </summary>
		/// <value>
		/// The node type alias.
		/// </value>
		string NodeTypeAlias { get; }
	}
}