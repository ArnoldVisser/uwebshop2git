﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IOrderRepository
	{
		/// <summary>
		///     Returns a single orderinfo
		/// </summary>
		/// <returns>Order</returns>
		OrderInfo GetOrderInfo(Guid uniqueOrderId);

		/// <summary>
		/// Determines the last order unique identifier.
		/// </summary>
		/// <returns></returns>
		int DetermineLastOrderId();

		/// <summary>
		///     Returns a single orderinfo
		/// </summary>
		/// <returns>Order</returns>
		XmlDocument GetOrderInfoXML(Guid uniqueOrderId);

		/// <summary>
		///     Get the order based on the transaction ID returned from the Payment Provider
		/// </summary>
		/// <param name="transactionId"></param>
		/// <returns></returns>
		OrderInfo GetOrderInfo(string transactionId);

		/// <summary>
		/// Saves the order.
		/// </summary>
		/// <param name="orderInfo">The order.</param>
		void SaveOrderInfo(OrderInfo orderInfo);

		/// <summary>
		/// Gets the orders from customer.
		/// </summary>
		/// <param name="customerId">The customer unique identifier.</param>
		/// <returns></returns>
		IEnumerable<OrderInfo> GetOrdersFromCustomer(int customerId);

		/// <summary>
		/// Get the orders based on the customer ID
		/// </summary>
		/// <param name="customerUsername">The customer username.</param>
		/// <returns>
		/// All the orders for the customer
		/// </returns>
		IEnumerable<OrderInfo> GetOrdersFromCustomer(string customerUsername);
	}
}