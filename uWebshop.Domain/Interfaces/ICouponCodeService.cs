﻿using System;
using System.Collections.Generic;
using System.Text;
using uWebshop.Domain.Model;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICouponCodeService
	{
		/// <summary>
		/// Gets all.
		/// </summary>
		/// <returns></returns>
		IEnumerable<Coupon> GetAll();
		/// <summary>
		/// Gets all for discount.
		/// </summary>
		/// <param name="discountId">The discount unique identifier.</param>
		/// <returns></returns>
		IEnumerable<Coupon> GetAllForDiscount(int discountId);
		/// <summary>
		/// Gets the specified discount unique identifier.
		/// </summary>
		/// <param name="discountId">The discount unique identifier.</param>
		/// <param name="couponCode">The coupon code.</param>
		/// <returns></returns>
		Coupon Get(int discountId, string couponCode);
		/// <summary>
		/// Gets all with couponcode.
		/// </summary>
		/// <param name="couponCode">The coupon code.</param>
		/// <returns></returns>
		IEnumerable<Coupon> GetAllWithCouponcode(string couponCode);

		/// <summary>
		/// Saves the specified coupon.
		/// </summary>
		/// <param name="coupon">The coupon.</param>
		void Save(Coupon coupon);
	}
}
