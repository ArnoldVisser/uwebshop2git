﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	internal interface IStoreRepository
	{
		List<Store> GetAll();
		Store TryGetStoreFromBackendCurrentOrder();
		Store TryGetStoreFromCurrentNode();
		Store GetById(int id);
	}
}