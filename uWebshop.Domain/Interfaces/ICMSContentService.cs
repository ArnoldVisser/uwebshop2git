﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICMSContentService
	{
		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		IUwebshopReadonlyContent GetReadonlyById(int id);

		/// <summary>
		/// Generates the domain URL for content with unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		string GenerateDomainUrlForContent(int id);

		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		IUwebshopContent GetById(int id);

		/// <summary>
		/// Gets the file by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		File GetFileById(int id);

		/// <summary>
		/// Gets the image by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		Image GetImageById(int id);
	}
}
