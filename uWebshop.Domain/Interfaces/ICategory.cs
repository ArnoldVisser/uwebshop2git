﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ICategory : IUwebshopEntity
	{
		ICategory ParentCategory { get; }
		string UrlName { get; }
		string ParentNodeTypeAlias { get; }
		string Title { get; }
		string MetaDescription { get; }
		string[] Tags { get; }
		string Description { get; }
	}
}