﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IStoreService
	{
		/// <summary>
		/// Gets the current store.
		/// </summary>
		/// <returns></returns>
		Store GetCurrentStore(); //might be wrong because Umbraco implementation relies on HttpContext.Current (& much more) and has too much logic, so splitting up will be neccessary soon
		/// <summary>
		/// Currents the store alias.
		/// </summary>
		/// <returns></returns>
		string CurrentStoreAlias();
		/// <summary>
		/// Gets all stores.
		/// </summary>
		/// <returns></returns>
		IEnumerable<Store> GetAllStores();
		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		Store GetById(int id);
		/// <summary>
		/// Gets the by alias.
		/// </summary>
		/// <param name="alias">The alias.</param>
		/// <returns></returns>
		Store GetByAlias(string alias);
		/// <summary>
		/// Loads the store URL.
		/// </summary>
		/// <param name="store">The store.</param>
		void LoadStoreUrl(Store store);
		/// <summary>
		/// Gets the product nice URL.
		/// </summary>
		/// <param name="product">The product.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		string GetProductNiceUrl(Product product, string storeAlias = null, bool getCanonicalUrl = false);
		/// <summary>
		/// Gets the category nice URL.
		/// </summary>
		/// <param name="category">The category.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		string GetCategoryNiceUrl(ICategory category, string storeAlias = null);
		
		/// <summary>
		/// Renames the store.
		/// </summary>
		/// <param name="oldStoreAlias">The old store alias.</param>
		/// <param name="newStoreAlias">The new store alias.</param>
		void RenameStore(string oldStoreAlias, string newStoreAlias);

		/// <summary>
		/// Gets the nice URL.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <param name="categoryId">The category unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		string GetNiceUrl(int id, int categoryId, string storeAlias);
	}
}