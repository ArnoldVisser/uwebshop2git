﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface IDiscountRepository
	{
		/// <summary>
		/// Get all discounts.
		/// </summary>
		/// <returns></returns>
		List<IOrderDiscount> GetAll();
		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <returns></returns>
		IOrderDiscount GetById(int id);
	}
}