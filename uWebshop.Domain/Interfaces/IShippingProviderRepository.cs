﻿namespace uWebshop.Domain.Interfaces
{
	interface IShippingProviderRepository : IEntityRepository<ShippingProvider>
	{
	}
}
