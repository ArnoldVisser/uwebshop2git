﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	internal interface IZoneService
	{
		/// <summary>
		///     Returns all the payment zones
		/// </summary>
		List<Zone> GetAllPaymentZones(string storeAlias);

		/// <summary>
		///     Returns all the shipping zones
		/// </summary>
		List<Zone> GetAllShippingZones(string storeAlias);

		/// <summary>
		/// Returns the zone matching with the id or the fallback zone (all countries) when not found
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		Zone GetByIdOrFallbackZone(int id, string storeAlias);
	}
}