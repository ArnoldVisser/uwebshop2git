﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ISettingsService
	{
		/// <summary>
		///     Are all prices in the store including VAT?
		/// </summary>
		bool IncludingVat { get; }

		/// <summary>
		///     Render all url's lowercase instead of Case Sensitive
		/// </summary>
		bool UseLowercaseUrls { get; }

		/// <summary>
		///     Lifetime of an incomplete order in minutes
		/// </summary>
		int IncompleOrderLifetime { get; }
	}
}