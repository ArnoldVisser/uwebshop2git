﻿using System.Collections.Generic;

namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IEntityService<T>
	{
		/// <summary>
		/// Gets the by unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		/// <param name="storeAlias">The store alias.</param>
		/// <returns></returns>
		T GetById(int id, string storeAlias);
		/// <summary>
		/// Gets all.
		/// </summary>
		/// <param name="storeAlias">The store alias.</param>
		/// <param name="includeDisabled">if set to <c>true</c> [include disabled].</param>
		/// <returns></returns>
		List<T> GetAll(string storeAlias, bool includeDisabled = false);
		/// <summary>
		/// Reloads the entity with unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		void ReloadEntityWithId(int id);
		/// <summary>
		/// Unloads the entity with unique identifier.
		/// </summary>
		/// <param name="id">The unique identifier.</param>
		void UnloadEntityWithId(int id);
	}
}