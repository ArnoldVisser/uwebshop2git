﻿namespace uWebshop.Domain.Interfaces
{
	/// <summary>
	/// 
	/// </summary>
	public interface ISale
	{
		/// <summary>
		/// Gets the sale.
		/// </summary>
		/// <param name="product">The product.</param>
		/// <returns></returns>
		CustomSale GetSale(Product product);
	}

	//public interface IVariantSale
	//{
	//    CustomVariantSale GetVariantSale(PricingVariant pricingvariant);
	//}
}