﻿using System.Collections.Generic;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Repositories
{
	public class TestProductService : IProductService
	{
		// todo: move this functionality to a fake repository
		public const int ProductId1 = 1234;
		public Product GetById(int id, string storeAlias)
		{
			if (id == ProductId1) return new Product { Id = ProductId1, PriceInCents = 995, Disabled = false, StockStatus = true};
			return null;
		}

		public int GetStockForProduct(int productId)
		{
			return 1; // hmm
		}

		public ProductInfo CreateProductInfoByProductId(int productId, IOrderInfo order, string storeAlias, int itemCount)
		{
			if (productId == ProductId1) return DefaultFactoriesAndSharedFunctionality.CreateProductInfo(995, 1);
			return null;
		}

		public List<Product> GetAllEnabledAndWithCategory(string storeAlias)
		{
			throw new System.NotImplementedException();
		}

		public void ReloadWithVATSetting()
		{
			
		}

		public void FullResetCache()
		{
			
		}

		public List<Product> GetAll(string storeAlias, bool includeDisabled = false)
		{
			throw new System.NotImplementedException();
		}

		public void ReloadEntityWithId(int id)
		{
			
		}

		public void UnloadEntityWithId(int id)
		{
			
		}
	}
}