﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Repositories
{
	// todo: zou deze class overbodig zijn geworden?
	public class TestStoreService : IStoreService
	{
		public Store GetCurrentStore()
		{
			return TestStoreRepository.Stores.First();
		}

		public string CurrentStoreAlias()
		{
			return GetCurrentStore().Alias;
		}

		public IEnumerable<Store> GetAllStores()
		{
			return new List<Store> {GetCurrentStore()};
		}

		public Store GetById(int id)
		{
			return GetCurrentStore();
		}

		public Store GetByAlias(string alias)
		{
			return GetCurrentStore();
		}

		public void LoadStoreUrl(Store store)
		{
			//store.StoreURL = "http://my.uwebshop.com/";
		}

		public string GetProductNiceUrl(Product product, string storeAlias = null, bool getCanonicalUrl = false)
		{
			return string.Empty;
		}

		public string GetCategoryNiceUrl(ICategory category, string storeAlias = null)
		{
			return string.Empty;
		}

		public void RenameStore(string oldStoreAlias, string newStoreAlias)
		{
			GetByAlias(oldStoreAlias).Alias = newStoreAlias;
		}

		public string GetNiceUrl(int id, int categoryId, string storeAlias)
		{
			return string.Empty;
		}
	}
}
