﻿using System.Collections.Generic;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Repositories
{
	public class TestOrderService : IOrderService
	{
		public OrderInfo CreateOrder()
		{
			return CreateOrder(DefaultFactoriesAndSharedFunctionality.CreateDefaultStore());
		}

		public OrderInfo CreateOrder(Store store)
		{
			return DefaultFactoriesAndSharedFunctionality.CreateIncompleteOrderInfo();
		}

		public OrderInfo CreateCopyOfOrder(OrderInfo orderInfo)
		{
			throw new System.NotImplementedException();
		}

		public void StoreOrderFirstTimeHackishRefactorPlz(OrderInfo order)
		{
		}

		public bool OrderContainsOutOfStockItem(OrderInfo orderinfo)
		{
			return false;
		}

		public List<OrderLine> GetApplicableOrderLines(OrderInfo orderinfo, IEnumerable<int> itemIdsToCheck)
		{
			return orderinfo.OrderLines;
		}

		public bool OrderContainsItem(OrderInfo orderinfo, IEnumerable<int> itemIdsToCheck)
		{
			return false;
		}

		public bool ValidateOrder(OrderInfo orderInfo, bool writeToOrderValidation = true)
		{
			return false;
		}

		public bool ValidateCustomer(OrderInfo orderinfo, bool clearValidation, bool writeToOrderValidation = true)
		{
			return false;
		}

		public bool ValidateStock(OrderInfo orderinfo, bool clearValidation, bool writeToOrderValidation = true)
		{
			return false;
		}

		public bool ValidateOrderlines(OrderInfo orderInfo, bool clearValidation, bool writeToOrderValidation = true)
		{
			return false;
		}

		public bool ValidateCustomValidations(OrderInfo orderInfo, bool writeToOrderValidation = true)
		{
			return false;
		}

		public void UseStoredDiscounts(OrderInfo order, List<IOrderDiscount> discounts)
		{
			order.OrderDiscountsFactory = () => discounts;
		}

		public void UseDatabaseDiscounts(OrderInfo order)
		{
			// hmmm
		}
	}
}