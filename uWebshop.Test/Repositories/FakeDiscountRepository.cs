﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Repositories
{
	class FakeDiscountRepository : IDiscountRepository
	{
		public List<IOrderDiscount> Entities = new List<IOrderDiscount>();
		public List<IOrderDiscount> GetAll()
		{
			return Entities;
		}

		public IOrderDiscount GetById(int id)
		{
			return Entities.FirstOrDefault(d => d.OriginalId == id);
		}
	}
}
