﻿using uWebshop.Domain;

namespace uWebshop.Test.Stubs
{
	public class StubStore : Store
	{
		public override string Alias { get; protected internal set; }

		public static Store CreateDefaultStore()
		{
			return new StubStore
			{
				NodeTypeAlias = "uwbsStore",
				Alias = "TestStore",
				DefaultCountryCode = "NL",
				Id = 1,
				GlobalVat = 19,
				StoreURL = "http://my.uwebshop.com/",
				SortOrder = 1,
				Culture = "NL",
				CountryCode = "NL",
				CurrencyCulture = "NL",
				UseStock = true,
				UseStoreSpecificStock = false,
			};
		}
	}
}