﻿using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Stubs
{
	public class StubUrlSplitter : ICatalogUrlSplitterService
	{
		private readonly string _shopPart;
		private readonly string _catalogPart;

		public StubUrlSplitter()
		{
			
		}
		public StubUrlSplitter(string shopPart, string catalogPart)
		{
			_shopPart = shopPart;
			_catalogPart = catalogPart;
		}

		public CatalogUrlResolveServiceResult DetermineCatalogUrlComponents(string url)
		{
			return new CatalogUrlResolveServiceResult {CatalogUrl = _catalogPart, StoreNodeUrl = _shopPart};
		}
	}
}