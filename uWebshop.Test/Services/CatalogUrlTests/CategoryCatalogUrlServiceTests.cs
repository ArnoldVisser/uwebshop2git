﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Moq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Services.CatalogUrlTests
{
	[TestFixture]
	public class CategoryCatalogUrlServiceTests
	{
		private ICategoryCatalogUrlService _categoryCatalogUrlService;
		private MockCategory _cat1;
		private MockCategory _cat2;
		private MockCategory _cat3;

		[SetUp]
		public void Setup()
		{
			_categoryCatalogUrlService = IOC.CategoryCatalogUrlService.Actual().Resolve();
			_cat1 = new MockCategory { UrlName = "cat1" };
			_cat2 = new MockCategory { UrlName = "cat2" };
			_cat3 = new MockCategory { UrlName = "cat3" };
		}

		[Test]
		public void GetUrl_ForSingleRootCategory_ShouldBeUrlName()
		{
			var actual = _categoryCatalogUrlService.GetUrl(_cat1);

			Assert.AreEqual("cat1", actual);
		}

		[Test]
		public void GetUrl_ForTwoCategories_ShouldBeUrlNamesAppendedBySlash()
		{
			_cat2.ParentCategory = _cat1;

			var actual = _categoryCatalogUrlService.GetUrl(_cat2);

			Assert.AreEqual("cat1/cat2", actual);
		}

		[Test]
		public void GetUrl_ForThreeCategories_ShouldBeUrlNamesAppendedBySlash()
		{
			_cat3.ParentCategory = _cat2;
			_cat2.ParentCategory = _cat1;

			var actual = _categoryCatalogUrlService.GetUrl(_cat3);

			Assert.AreEqual("cat1/cat2/cat3", actual);
		}
		
		class MockCategory : ICategory
		{
			public ICategory ParentCategory { get; set; }
			public string UrlName { get; set; }

			#region not implemented
			public int Id
			{
				get { throw new NotImplementedException(); }
			}

			public bool Disabled
			{
				get { throw new NotImplementedException(); }
			}

			public string NodeTypeAlias
			{
				get { throw new NotImplementedException(); }
			}

			public string ParentNodeTypeAlias
			{
				get { throw new NotImplementedException(); }
			}

			public string Title
			{
				get { throw new NotImplementedException(); }
			}

			public string MetaDescription
			{
				get { throw new NotImplementedException(); }
			}

			public string[] Tags
			{
				get { throw new NotImplementedException(); }
			}

			public string Description
			{
				get { throw new NotImplementedException(); }
			}
			#endregion
		}
	}
}
