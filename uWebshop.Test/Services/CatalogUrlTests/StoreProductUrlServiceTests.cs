﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Moq;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Services.CatalogUrlTests
{
	[TestFixture]
	public class StoreProductUrlServiceTests
	{
		private IStoreProductUrlService _storeProductUrlService;

		[SetUp]
		public void Setup()
		{

			_storeProductUrlService = IOC.StoreProductUrlService.Actual().Resolve();
		}

		public void NoCurrentCategory()
		{
			
		}

		public void CurrentCategoryNotInCategories()
		{
			
		}
	}
}
