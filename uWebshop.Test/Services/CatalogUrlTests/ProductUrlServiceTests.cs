﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Services.CatalogUrlTests
{
	[TestFixture]
	public class ProductUrlServiceTests
	{
		private IProductUrlService _productUrlService;
		private Mock<IStoreProductUrlService> _storeProductUrlServiceMock;
		private Mock<IStoreService> _storeServiceMock;
		private Store _store;
		private Product _product;
			
		[SetUp]
		public void Setup()
		{
			_storeProductUrlServiceMock = IOC.StoreProductUrlService.SetupNewMock();
			_storeServiceMock = IOC.StoreService.SetupNewMock();
			_store = new Store();
			_storeServiceMock.Setup(m => m.GetCurrentStore()).Returns(_store);
			_product = new Product();
			_productUrlService = IOC.ProductUrlService.Actual().Resolve();
		}

		[Test]
		public void TestGetUrl()
		{
			_productUrlService.GetUrl(_product);

			_storeProductUrlServiceMock.Verify(m => m.GetUrl(_product, _store));
		}

		[Test]
		public void TestGetCanonicalUrl()
		{
			_productUrlService.GetCanonicalUrl(_product);

			_storeProductUrlServiceMock.Verify(m => m.GetCanonicalUrl(_product, _store));
		}
	}


}
