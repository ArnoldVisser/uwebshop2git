﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Mocks
{
	class FakeDiscountService : IDiscountService
	{
		private readonly IDiscountRepository _discountRepository;

		public FakeDiscountService(IDiscountRepository discountRepository)
		{
			_discountRepository = discountRepository;
		}

		public List<IOrderDiscount> GetApplicableDiscountsForOrder(OrderInfo orderInfo)
		{
			return _discountRepository.GetAll();
		}

		public IOrderDiscount GetOrderDiscountById(int id)
		{
			throw new NotImplementedException();
		}

		public DiscountProduct GetProductDiscountById(int id)
		{
			throw new NotImplementedException();
		}

		public IDiscount GetById(int id)
		{
			throw new NotImplementedException();
		}
	}
}
