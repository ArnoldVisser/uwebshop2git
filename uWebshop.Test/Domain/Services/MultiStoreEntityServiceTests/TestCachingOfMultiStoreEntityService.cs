﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using uWebshop.Test.Services.OrderUpdatingsServiceTests;

namespace uWebshop.Test.Domain.Services.MultiStoreEntityServiceTests
{
	[TestFixture]
	public class TestCachingOfMultiStoreEntityService
	{
		private Mock<IProductRepository> _productRepositoryMock;
		private IProductService _productService;
		private const string StoreAlias = "store";

		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			IOC.ProductRepository.Mock(out _productRepositoryMock);
			_productService = IOC.ProductService.Actual().Resolve();
			_productRepositoryMock.Setup(m => m.GetAll(StoreAlias)).Returns(new List<Product>());
		}
		
		[Test]
		public void GetAll_CallingTwice_ShouldCallRepoOnce()
		{
			_productService.GetAll(StoreAlias);
			_productService.GetAll(StoreAlias);

			_productRepositoryMock.Verify(m => m.GetAll(StoreAlias), Times.Once());
		}

		[Test]
		public void GetById_CallingTwice_ShouldCallRepoOnce()
		{
			_productService.GetById(1, StoreAlias);
			_productService.GetById(1, StoreAlias);

			_productRepositoryMock.Verify(m => m.GetAll(StoreAlias), Times.Once());
		}

	}


}
