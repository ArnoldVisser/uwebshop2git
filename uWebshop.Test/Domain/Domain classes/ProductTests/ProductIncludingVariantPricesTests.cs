﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Test.Mocks;
using uWebshop.Test.Services.OrderUpdatingsServiceTests;

namespace uWebshop.Test.Domain.Domain_classes.ProductTests
{
    [TestFixture]
    public class ProductIncludingVariantPricesTests
    {
        private ProductVariant variant;
		private Product product;

        [SetUp]
        public void Setup()
        {
            IOC.UnitTest();
			product = new Product { OriginalPriceInCents = 1000, Id = 1 };
            variant = new ProductVariant { OriginalPriceInCents = 100 };
            product.Variants = new List<ProductVariant> { variant };
            variant.Product = product;
        }

        [Test]
        public void PriceIncludingProductPriceInCents_NotDiscounted_GivesProductPlusVariant()
        {
            Assert.AreEqual(1100, variant.PriceIncludingProductPriceInCents);
        }

        [Test]
        public void PriceIncludingProductPriceInCents_ProductDiscountExcludingVariants_GivesDiscountedProductPlusVariant()
        {
            IOC.ProductDiscountRepository.SetupNewMock()
			   .Setup(m => m.GetDiscountByProductId(1, It.IsAny<string>()))
               .Returns(new DiscountProduct
                   {
                       DiscountValue = 10*100,
                       DiscountType = DiscountType.Percentage,
                       ExcludeVariants = true,
                   });

            Assert.AreEqual(900, product.PriceInCents);
            Assert.AreEqual(100, variant.PriceInCents);

            Assert.AreEqual(1000, variant.PriceIncludingProductPriceInCents);
        }

        [Test]
        public void PriceIncludingProductPriceInCents_ProductDiscountIncludingVariants_GivesDiscountedProductPlusVariant()
        {
            IOC.ProductDiscountRepository.SetupNewMock()
               .Setup(m => m.GetDiscountByProductId(1, It.IsAny<string>()))
               .Returns(new DiscountProduct
               {
                   DiscountValue = 10 * 100,
                   DiscountType = DiscountType.Percentage,
                   ExcludeVariants = false,
               });

            Assert.AreEqual(900, product.PriceInCents);

            Assert.IsTrue(variant.Product.IsDiscounted && !variant.Product.ProductDiscount.ExcludeVariants && variant.Product.ProductDiscount.DiscountType == DiscountType.Percentage);
            Assert.AreEqual(990, variant.PriceIncludingProductPriceInCents);
        }
    }
}
