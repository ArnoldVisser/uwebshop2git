﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Moq;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Test.Services.OrderUpdatingsServiceTests;

namespace uWebshop.Test.Domain.Domain_classes.ProductTests
{
	[TestFixture]
	public class OrderablePropertyTests
	{
		[Test]
		public void Orderable_StockTrueVariantFalseBackorderFalseStock100_ShouldReturnTrue()
		{
			IOC.UnitTest();
			IOC.StockService.SetupNewMock().Setup(m => m.GetStockForUwebshopEntityWithId(It.IsAny<int>())).Returns(100);
			var product = new Product { UseVariantStock = false, StockStatus = true, BackorderStatus = false, Variants = Enumerable.Empty<ProductVariant>() };

			Assert.IsTrue(product.Orderable);
		}

		[TestCase(true, false,  100, true)]
		[TestCase(true, false,  0, false)]
		[TestCase(true, true,   100, true)]
		[TestCase(true, true,   0, true)]
		[TestCase(false, true,  100, true)]
		[TestCase(false, true,  0, true)]
		[TestCase(false, false, 100, true)]
		[TestCase(false, false, 0, true)]
		public void OrderableTestCases_NoVariants(bool stockStatus, bool backOrderStatus, int stock, bool expected)
		{
			IOC.UnitTest();
			IOC.StockService.SetupNewMock().Setup(m => m.GetStockForUwebshopEntityWithId(It.IsAny<int>())).Returns(stock);
			IOC.ProductService.Actual();
			var product = new Product { UseVariantStock = false, StockStatus = stockStatus, BackorderStatus = backOrderStatus, Variants = Enumerable.Empty<ProductVariant>() };

			Assert.AreEqual(expected, product.Orderable);
		}

	}
}
