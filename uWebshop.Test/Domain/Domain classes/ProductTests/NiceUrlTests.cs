﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using Moq;

namespace uWebshop.Test.Domain.Domain_classes.ProductTests
{
	[TestFixture]
	public class NiceUrlTests
	{
		private Product _product;
		private Mock<IStoreService> _storeService;

		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			IOC.StoreService.Mock(out _storeService);
			_product = new Product();
		}

		[Test]
		public void CallingNiceUrl_WithoutArguments_ShouldCallStoreServiceWithFalseCanonical()
		{
			_product.NiceUrl();

			_storeService.Verify(m => m.GetProductNiceUrl(_product, It.IsAny<string>(), false));
		}

		[Test]
		public void CallingNiceUrl_WithFalseCanonical_ShouldCallStoreServiceWithFalseCanonical()
		{
			_product.NiceUrl(false);

			_storeService.Verify(m => m.GetProductNiceUrl(_product, It.IsAny<string>(), false));
		}

		[Test]
		public void CallingNiceUrl_WithTrueCanonical_ShouldCallStoreServiceWithTrueCanonical()
		{
			_product.NiceUrl(true);

			_storeService.Verify(m => m.GetProductNiceUrl(_product, It.IsAny<string>(), true));
		}

		[Test]
		public void CallingUrl_ShouldCallStoreServiceWithTrueCanonical()
		{
			var url = _product.Url;

			_storeService.Verify(m => m.GetProductNiceUrl(_product, It.IsAny<string>(), true));
		}
	}
}
