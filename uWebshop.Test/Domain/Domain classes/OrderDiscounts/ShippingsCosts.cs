﻿using NUnit.Framework;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Test.Domain.Domain_classes.OrderDiscounts
{
	[TestFixture]
	public class ShippingsCosts
	{
		private IDiscountCalculationService _discountCalculationService;

		[TestFixtureSetUp]
		public void SetUp()
		{
			IOC.UnitTest();
			IOC.SettingsService.InclVat();
			//IOC.Config<IDiscountCalculator>().Actual(); 
			//IOC.DiscountCalculator.Actual(); _discountCalculator = IOC.DiscountCalculator.Resolve();
			_discountCalculationService = IOC.DiscountCalculationService.Actual().Resolve();
		}

		[Test]
		public void FreeShippingDiscount_ShouldGiveDiscountAmountEqualToShippingCosts()
		{
			var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, 1));
			var discount = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithFreeShipping();
			orderInfo.ShippingProviderAmountInCents = 195;

			Assert.AreEqual(orderInfo.ShippingProviderAmountInCents, _discountCalculationService.DiscountAmountForOrder(discount, orderInfo));
			DefaultFactoriesAndSharedFunctionality.SetDiscountsOnOrderInfo(orderInfo, discount);
			Assert.AreEqual(195, orderInfo.DiscountAmountInCents);
		}
	}
}
