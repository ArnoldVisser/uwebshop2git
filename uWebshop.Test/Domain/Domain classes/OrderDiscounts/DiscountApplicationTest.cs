﻿using NUnit.Framework;
using uWebshop.Common;

namespace uWebshop.Test.Domain.Domain_classes.OrderDiscounts
{
	[TestFixture]
	public class DiscountApplicationTest
	{
		[TestFixtureSetUp]
		public void SetUp()
		{
			IOC.UnitTest();
			IOC.DiscountCalculationService.Actual();
			IOC.SettingsService.InclVat();
		}

		[Test]
		public void AddingACouponToAnIncompleteOrderAfterDeserialization_ShouldApplyCouponDiscount()
		{
			var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, 1));
			var discount = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithAmount(100, DiscountOrderCondition.None, 0);
			discount.CouponCode = "coupon";
			DefaultFactoriesAndSharedFunctionality.SetDiscountsOnOrderInfo(orderInfo, discount);

			Assert.AreEqual(0, orderInfo.DiscountAmountInCents);
			// fake deserialization (for sake of speed)
			//orderInfo.DiscountService = new DiscountService(new ListRepository<IOrderDiscount>(orderInfo.DiscountService.GetApplicableDiscountsForOrder(orderInfo).Select(disc => new OrderDiscount(disc))));
			//orderInfo.Discounts = orderInfo.Discounts.ToList();

			Assert.AreEqual(0, orderInfo.DiscountAmountInCents);

			Assert.False(orderInfo.LegacyDataReadBackMode);
			orderInfo.SetCouponCode("coupon");
			Assert.Null(orderInfo._discountAmountInCents);

			//Assert.False(orderInfo._discountAmountInCents.HasValue);
			Assert.AreEqual(100, orderInfo.DiscountAmountInCents);
		}
	}
}
