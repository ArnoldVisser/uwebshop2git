﻿using NUnit.Framework;

namespace uWebshop.Test.AppliedDiscountsInformationTest
{
	[TestFixture]
	public class AppliedDiscountsInformationTest
	{
		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			IOC.DiscountCalculationService.Actual();
		}

		[Test]
		public void OrderInfoWithApplicableDiscount_ShouldReturnInformationOnThatDiscount()
		{
			var product = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, 1);
			var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(product);
			var discount = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithPercentage(10);
			DefaultFactoriesAndSharedFunctionality.SetDiscountsOnOrderInfo(orderInfo, discount);

			Assert.AreEqual(1, orderInfo.AppliedDiscountsInformation.Count);
		}
	}
}
