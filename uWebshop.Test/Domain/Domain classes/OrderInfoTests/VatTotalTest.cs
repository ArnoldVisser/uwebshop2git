﻿using NUnit.Framework;

namespace uWebshop.Test.Domain.Domain_classes
{
	[TestFixture]
	public class VatTotalTest
	{
		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			IOC.DiscountCalculationService.Actual();
		}

		[TestCase(true, 0, 1)]
		[TestCase(true, 19, 1)]
		[TestCase(true, 19, 2)]
		[TestCase(false, 0, 1)]
		[TestCase(false, 19, 1)]
		[TestCase(false, 19, 2)]
		public void OrderWithOneLine_ShouldGiveVatAmountOfProduct(bool includingVat, decimal vat, int numberOfItemsInOrderLine)
		{
			IOC.SettingsService.ExclVat();
			var productInfo = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, numberOfItemsInOrderLine, vat);
			var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(productInfo);

			Assert.AreEqual(productInfo.VatAmountInCents*numberOfItemsInOrderLine, orderInfo.TotalVatInCents);
		}

		[TestCase(true, 0, 1)]
		//[TestCase(true, 10, 1)] afrondingsbug in test
		[TestCase(true, 10, 2)]
		[TestCase(false, 0, 1)]
		[TestCase(false, 10, 1)]
		[TestCase(false, 10, 2)]
		public void OrderWithTwoLinesAndDiscount_ShouldGiveVatAmountOfProductMinusOrderDiscount(bool includingVat, decimal vat, int numberOfItemsInOrderLine)
		{
			if (includingVat)
				IOC.SettingsService.InclVat();
			else
				IOC.SettingsService.ExclVat();
			var productInfo1 = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, numberOfItemsInOrderLine, vat);
			var productInfo2 = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(3500, numberOfItemsInOrderLine, vat);
			var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(productInfo1, productInfo2);
			var discount = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithPercentage(50);
			DefaultFactoriesAndSharedFunctionality.SetDiscountsOnOrderInfo(orderInfo, discount);

			Assert.AreEqual((productInfo1.VatAmountInCents + productInfo2.VatAmountInCents)*numberOfItemsInOrderLine/2, orderInfo.TotalVatInCents);
		}

		[Test]
		public void MultipleAssertsIncludingVat()
		{
			IOC.SettingsService.InclVat();
			var productInfo1 = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1100, 1, 10);
			var productInfo2 = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(3850, 1, 10);
			var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(productInfo1, productInfo2);
			var discount = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithPercentage(50);
			DefaultFactoriesAndSharedFunctionality.SetDiscountsOnOrderInfo(orderInfo, discount);

			Assert.AreEqual(50 + 175, orderInfo.TotalVatInCents);
			Assert.AreEqual(2475, orderInfo.OrderTotalInCents);
			Assert.AreEqual(2250, orderInfo.SubtotalInCents);
			Assert.AreEqual(2250 + 225, orderInfo.GrandtotalInCents);
			Assert.AreEqual(50 + 175, orderInfo.TotalVatInCents);
		}


		[Test]
		public void MultipleAssertsExcludingVat()
		{
			IOC.SettingsService.ExclVat();
			var productInfo1 = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, 1, 10);
			var productInfo2 = DefaultFactoriesAndSharedFunctionality.CreateProductInfo(3500, 1, 10);
			var orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(productInfo1, productInfo2);
			var discount = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithPercentage(50);
			DefaultFactoriesAndSharedFunctionality.SetDiscountsOnOrderInfo(orderInfo, discount);

			Assert.AreEqual(50 + 175, orderInfo.TotalVatInCents);
			Assert.AreEqual(2250, orderInfo.OrderTotalInCents);
			Assert.AreEqual(2250, orderInfo.SubtotalInCents);
			Assert.AreEqual(2250 + 225, orderInfo.GrandtotalInCents);
			Assert.AreEqual(50 + 175, orderInfo.TotalVatInCents);
		}
	}
}