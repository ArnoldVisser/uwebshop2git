﻿using System.Linq;
using NUnit.Framework;
using uWebshop.Domain;
using uWebshop.Domain.OrderDTO;

namespace uWebshop.Test.Domain.Businesslogic.DiscountCalculatorTests
{
	[TestFixture]
	public class CombinationsOfConditionsAndEffectsTest
	{
		private OrderInfo _orderInfo;
		private OrderDiscount _orderDiscount;

		[SetUp]
		public void Setup()
		{
			IOC.IntegrationTest();
			//IOC.DiscountCalculationService.Actual();
			//IOC.DiscountService.Actual();
			
			_orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, 1));
			
			_orderDiscount = DefaultFactoriesAndSharedFunctionality.CreateDefaultOrderDiscountWithFreeShipping();
			IOC.DiscountRepository.SetupFake(_orderDiscount);
		}
		[Test]
		public void MinimumOrderAmountAndFreeShippingOnOrderWithSufficientAmount_ShouldGiveShippingAmount()
		{
			_orderDiscount.MinimalOrderAmount = 500;
			_orderInfo.ShippingProviderAmountInCents = 195;

			Assert.AreEqual(195, _orderInfo.DiscountAmountInCents);
		}
		[Test]
		public void MinimumOrderAmountAndFreeShippingOnOrderWithInsufficientAmount_ShouldNotGiveDiscount()
		{
			_orderDiscount.MinimalOrderAmount = 1500;
			_orderInfo.ShippingProviderAmountInCents = 195;

			Assert.AreEqual(0, _orderInfo.DiscountAmountInCents);
		}
	}
}
