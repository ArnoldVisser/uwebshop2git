﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using uWebshop.Common;
using uWebshop.Common.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.OrderDTO;

namespace uWebshop.Test.Domain.Businesslogic.DiscountService
{
	[TestFixture]
	public class GetApplicableDiscountsForOrderTest
	{
		private OrderInfo _orderInfo;
		private Mock<IOrderDiscount> _mockDiscount;
		private IDiscountService _discountService;
		private Mock<IOrderService> _mockOrderService;

		[SetUp]
		public void Setup()
		{
			IOC.UnitTest();
			IOC.OrderService.Mock(out _mockOrderService);
			IOC.DiscountService.Actual();
			IOC.DiscountCalculationService.SetupNewMock()
				.Setup(m => m.DiscountAmountForOrder(It.IsAny<IOrderDiscount>(), It.IsAny<OrderInfo>(), It.IsAny<IAuthenticationProvider>())).Returns(1);

			_orderInfo = DefaultFactoriesAndSharedFunctionality.CreateOrderInfo(DefaultFactoriesAndSharedFunctionality.CreateProductInfo(1000, 1));
			
			_mockDiscount = new Mock<IOrderDiscount>();
			_mockDiscount.SetupGet(m => m.CounterEnabled).Returns(false);
			_mockDiscount.SetupGet(m => m.RequiredItemIds).Returns(new List<int>());

			IOC.DiscountRepository.SetupFake(new[] { _mockDiscount.Object });

			_discountService = IOC.DiscountService.Resolve();
		}

		[Test]
		public void OrderAmountAboveMinimum_ShouldReturnDiscount()
		{
			_mockDiscount.SetupGet(m => m.MinimalOrderAmount).Returns(500);

			Assert.AreEqual(_mockDiscount.Object, _discountService.GetApplicableDiscountsForOrder(_orderInfo).Single());
		}

		[Test]
		public void OrderAmountBelowMinimum_ShouldNotReturnDiscount()
		{
			_mockDiscount.SetupGet(m => m.MinimalOrderAmount).Returns(1500);

			Assert.False(_discountService.GetApplicableDiscountsForOrder(_orderInfo).Any());
		}

		[Test]
		public void CounterEnabledFalse_ShouldReturnDiscount()
		{
			_mockDiscount.SetupGet(m=>m.CounterEnabled).Returns(false);

			Assert.AreEqual(_mockDiscount.Object, _discountService.GetApplicableDiscountsForOrder(_orderInfo).Single());
			_mockDiscount.VerifyGet(m=>m.CounterEnabled, Times.AtLeastOnce());
			_mockDiscount.VerifyGet(m=>m.Counter, Times.Never());
		}

		[Test]
		public void CounterEnabledTrueAndCounterAboveZero_ShouldReturnDiscount()
		{
			_mockDiscount.SetupGet(m => m.CounterEnabled).Returns(true);
			_mockDiscount.SetupGet(m => m.Counter).Returns(1);

			Assert.AreEqual(_mockDiscount.Object, _discountService.GetApplicableDiscountsForOrder(_orderInfo).Single());
			_mockDiscount.VerifyGet(m => m.CounterEnabled, Times.AtLeastOnce());
			_mockDiscount.VerifyGet(m => m.Counter, Times.AtLeastOnce());
		}

		[Test]
		public void CounterEnabledTrueAndCounterZero_ShouldNotReturnDiscount()
		{
			_mockDiscount.SetupGet(m => m.CounterEnabled).Returns(true);
			_mockDiscount.SetupGet(m => m.Counter).Returns(0);

			Assert.False(_discountService.GetApplicableDiscountsForOrder(_orderInfo).Any());
			_mockDiscount.VerifyGet(m => m.CounterEnabled, Times.AtLeastOnce());
			_mockDiscount.VerifyGet(m => m.Counter, Times.AtLeastOnce());
		}

		[Test]
		public void RequiredItemIdsEmpty_ShouldReturnDiscount()
		{
			_mockDiscount.SetupGet(m => m.RequiredItemIds).Returns(new List<int>());

			Assert.AreEqual(_mockDiscount.Object, _discountService.GetApplicableDiscountsForOrder(_orderInfo).Single());
			_mockDiscount.VerifyGet(m => m.RequiredItemIds, Times.AtLeastOnce());
		}

		[Test]
		public void RequiredItemIdsWithOrderContainsItemTrue_ShouldReturnDiscount()
		{
			var itemIds = new List<int> {1234};
			_mockDiscount.SetupGet(m => m.RequiredItemIds).Returns(itemIds);
			_mockOrderService.Setup(m => m.OrderContainsItem(It.IsAny<OrderInfo>(), It.IsAny<List<int>>())).Returns(true);

			var discount = _discountService.GetApplicableDiscountsForOrder(_orderInfo).Single();

			Assert.AreEqual(_mockDiscount.Object, discount);
			_mockDiscount.VerifyGet(m => m.RequiredItemIds, Times.AtLeastOnce());
			_mockOrderService.Verify(m => m.OrderContainsItem(_orderInfo, itemIds), Times.Once());
		}

		[Test]
		public void RequiredItemIdsWithOrderContainsItemFalse_ShouldNotReturnDiscount()
		{
			var itemIds = new List<int> { 1234 };
			_mockDiscount.SetupGet(m => m.RequiredItemIds).Returns(itemIds);
			_mockOrderService.Setup(m => m.OrderContainsItem(It.IsAny<OrderInfo>(), It.IsAny<List<int>>())).Returns(false);

			Assert.False(_discountService.GetApplicableDiscountsForOrder(_orderInfo).Any());
			_mockDiscount.VerifyGet(m => m.RequiredItemIds, Times.AtLeastOnce());
			_mockOrderService.Verify(m => m.OrderContainsItem(_orderInfo, itemIds), Times.Once());
		}
	}
}
