﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.DataLayer;

namespace uWebshop.DataAccess
{
	/// <summary>
	/// 
	/// </summary>
	public class UwebshopCoupons
	{
		public static List<CouponData> GetAll(string where = null)
		{
			if (where == null) where = string.Empty;

			var coupons = new List<CouponData>();
			var sqlHelper = DataLayerHelper.CreateSqlHelper(GlobalSettings.DbDSN);

			using (var reader = sqlHelper.ExecuteReader("SELECT * FROM uWebshopCoupons " + where))
			{
				while (reader.Read())
				{
					coupons.Add(new CouponData(reader));
				}
				return coupons;
			}
		}

		public static List<CouponData> GetAllForDiscount(int discountId)
		{
			var sqlHelper = DataLayerHelper.CreateSqlHelper(GlobalSettings.DbDSN);
			var coupons = new List<CouponData>();

			using (var reader = sqlHelper.ExecuteReader(
				"SELECT * FROM uWebshopCoupons WHERE DiscountId = @discountId",
				sqlHelper.CreateParameter("@discountId", discountId)))
			{
				while (reader.Read())
				{
					coupons.Add(new CouponData(reader));
				}
				return coupons;
			}
		}

		public static CouponData Get(int discountId, string couponCode)
		{
			var sqlHelper = DataLayerHelper.CreateSqlHelper(GlobalSettings.DbDSN);

			using (var reader = sqlHelper.ExecuteReader(
				"SELECT * FROM uWebshopCoupons WHERE DiscountId = @discountId AND CouponCode = @couponCode",
				sqlHelper.CreateParameter("@discountId", discountId),
				sqlHelper.CreateParameter("@couponCode", couponCode)))
			{
				while (reader.Read())
				{
					var orderInfo = reader.GetString("orderInfo");
					if (!string.IsNullOrEmpty(orderInfo))
					{
						return new CouponData(reader);
					}
				}
				return null;
			}
		}

		public static List<CouponData> GetAllWithCouponcode(string couponCode)
		{
			var sqlHelper = DataLayerHelper.CreateSqlHelper(GlobalSettings.DbDSN);
			var coupons = new List<CouponData>();

			using (var reader = sqlHelper.ExecuteReader(
				"SELECT * FROM uWebshopCoupons WHERE CouponCode = @couponCode",
				sqlHelper.CreateParameter("@couponCode", couponCode)))
			{
				while (reader.Read())
				{
					coupons.Add(new CouponData(reader));
				}
				return coupons;
			}
		}

		public static void InstallCouponsTable()
		{
			var sqlHelper = DataLayerHelper.CreateSqlHelper(GlobalSettings.DbDSN);

			try
			{
				sqlHelper.ExecuteNonQuery(
					@"CREATE TABLE 
					[uWebshopCoupons](
					[DiscountId] [int] NOT NULL,
					[CouponCode] nvarchar (500) NOT NULL, 
					[NumberAvailable] [int] NOT NULL)");
			}
			catch (Exception ex)
			{
				Log.Add(LogTypes.Debug, 0, "InstallCouponsTable Catch: Already Exists?");
			}
		}

		public static void Save(int discountId, IEnumerable<CouponData> coupons)
		{
			var sqlHelper = DataLayerHelper.CreateSqlHelper(GlobalSettings.DbDSN);

			sqlHelper.ExecuteNonQuery("delete from [uWebshopCoupons] WHERE DiscountId = @discountId", sqlHelper.CreateParameter("@discountId", discountId));

			if (coupons.Any())
			sqlHelper.ExecuteNonQuery("insert into [uWebshopCoupons] (DiscountId, CouponCode, NumberAvailable) VALUES " +
				string.Join(", ", coupons.Select(c => "(" + c.DiscountId + ", '" + c.CouponCode + "', " + c.NumberAvailable + ")").ToArray()));
		}

		public static void Save(CouponData coupon)
		{
			var sqlHelper = DataLayerHelper.CreateSqlHelper(GlobalSettings.DbDSN);

			sqlHelper.ExecuteNonQuery("update [uWebshopCoupons] set NumberAvailable = @NumberAvailable WHERE DiscountId = @DiscountId and CouponCode = '@CouponCode'",
				sqlHelper.CreateParameter("@DiscountId", coupon.DiscountId),
				sqlHelper.CreateParameter("@CouponCode", coupon.CouponCode),
				sqlHelper.CreateParameter("@NumberAvailable", coupon.NumberAvailable));
		}
	}

	public class CouponData
	{
		public int DiscountId;
		public string CouponCode;
		public int NumberAvailable;

		public CouponData() { }
		public CouponData(IRecordsReader reader)
		{
			DiscountId = reader.GetInt("DiscountId");
			CouponCode = reader.GetString("CouponCode");
			NumberAvailable = reader.GetInt("NumberAvailable");
		}

		public CouponData(int discountId, string couponCode, int numberAvailable)
		{
			DiscountId = discountId;
			CouponCode = couponCode;
			NumberAvailable = numberAvailable;
		}
	}
}
