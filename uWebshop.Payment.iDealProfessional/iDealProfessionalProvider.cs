﻿using System;
using System.Collections.Generic;
using System.Web;
using ING.iDealAdvanced;
using ING.iDealAdvanced.Data;
using uWebshop.Domain.Interfaces;
using uWebshop.Common;
using uWebshop.Domain;
using umbraco.BusinessLogic;
using System.Web.Caching;

namespace uWebshop.Payment.iDealProfessional
{
    public class iDealPrefessionalProvider : IPaymentProvider
    {
        #region IPaymentProvider Members
        public string GetName()
        {
            return "iDealProfessional";
        }

        PaymentTransactionMethod IPaymentProvider.GetParameterRenderMethod()
        {
            return PaymentTransactionMethod.QueryString;
        }
        
        public List<PaymentProviderMethod> GetAllPaymentMethods(int id)
        {
            Log.Add(LogTypes.Debug, 0, "iDealPrefessionalProvider Start GetAllPaymentMethods");
            var paymentMethods = new List<PaymentProviderMethod>();

            try
            {
                var issuers = HttpContext.Current.Cache["Issuers"] as Issuers;
                
                if (issuers == null)
                {
                    try
                    {
                        try
                        {
                            var connector = new Connector();
                            issuers = connector.GetIssuerList();
                            HttpContext.Current.Cache.Add("Issuers", issuers, null, DateTime.Now.AddDays(1),
                                                          Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                            Log.Add(LogTypes.Debug, 0, "iDEAL Issuer list from iDEAL");
                        }
                        catch (IDealException ex)
                        {
                            Log.Add(LogTypes.Debug, 0, "iDeal Issuers " + ex.ErrorRes.Error.errorCode);
                            Log.Add(LogTypes.Debug, 0, "iDeal Issuers " + ex.ErrorRes.Error.errorDetail);
                            Log.Add(LogTypes.Debug, 0, "iDeal Issuers " + ex.ErrorRes.Error.errorMessage);
                            return null;
                        }
                    }
                    catch(Exception ex)
                    {
                        Log.Add(LogTypes.Debug, 0, "iDeal Connector Expeption " + ex);
                    }
                }
                else
                {
                    Log.Add(LogTypes.Debug, 0, "iDEAL Issuer list from CACHE");
                }

                foreach (var issuer in issuers.ShortList)
                {
                    var paymentImageId = 0;

                    int.TryParse(umbraco.library.GetDictionaryItem(issuer.Name + "LogoId"), out paymentImageId);

                    paymentMethods.Add(new PaymentProviderMethod
                    {
                        Id = issuer.Id,
                        Description = issuer.Name,
                        Title = issuer.Name,
                        Name = issuer.Name,
                        ProviderName = GetName()
                    });
                }

                foreach (var issuer in issuers.LongList)
                {
                    var paymentImageId = 0;

                    int.TryParse(umbraco.library.GetDictionaryItem(issuer.Name + "LogoId"), out paymentImageId);

                    paymentMethods.Add(new PaymentProviderMethod
                    {
                        Id = issuer.Id,
                        Description = issuer.Name,
                        Title = issuer.Name,
                        Name = issuer.Name,
                        ProviderName = GetName()
                    });
                }

               
            }
            catch (IDealException ex)
            {
                Log.Add(LogTypes.Debug, 0, "iDeal Issuers " + ex.ErrorRes.Error.errorCode);
                Log.Add(LogTypes.Debug, 0, "iDeal Issuers " + ex.ErrorRes.Error.errorDetail);
                Log.Add(LogTypes.Debug, 0, "iDeal Issuers " + ex.ErrorRes.Error.errorMessage);
                return null;
            }

            Log.Add(LogTypes.Debug, 0, "iDealPrefessionalProvider End GetAllPaymentMethods");
            return paymentMethods;
        }
        
        #endregion
    }
}