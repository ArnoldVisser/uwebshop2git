﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ING.iDealAdvanced;
using ING.iDealAdvanced.Data;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain;
using uWebshop.Domain.Repositories;
using System.Security.Cryptography;
using uWebshop.Common;
using umbraco.BusinessLogic;
using uWebshop.Domain.Helpers;
using umbraco;
using umbraco.NodeFactory;

namespace uWebshop.Payment.iDealProfessional
{
    public class iDealProfessionalRequestHandler : IPaymentRequestHandler
    {
        #region IPaymentRequestHandler Members
        /// <summary>
        /// Gets the payment provider name
        /// </summary>
        /// <returns>Name of the payment provider</returns>
        public string GetName()
        {
            return "iDealProfessional";
        }

        public PaymentRequest CreatePaymentRequest(OrderInfo OrderInfo)
        {
            RequestTransaction(OrderInfo);

            return null;
        }

        public string GetPaymentUrl(OrderInfo OrderInfo)
        {
            throw new NotImplementedException();
        }
        #endregion

        private void RequestTransaction(OrderInfo orderInfo)
        {
            var paymentProvider = new PaymentProvider(orderInfo.PaymentInfo.Id);
            var helper = new PaymentConfigHelper(paymentProvider);

            try
            {
                var transaction = new Transaction();
            
                decimal grandTotal = 0;

                if (paymentProvider.TestMode)
                {
                    //Test mode
                    grandTotal = helper.Settings["testAmountInCents"] == "0" ? orderInfo.GrandtotalInCents : decimal.Parse(helper.Settings["testAmountInCents"]);
                }
                else
                {
                    grandTotal = long.Parse(orderInfo.GrandtotalInCents.ToString());
                }

                transaction.Amount = long.Parse(grandTotal.ToString());
                transaction.Description = orderInfo.OrderNumber;
                transaction.PurchaseId = orderInfo.OrderNodeId.ToString();
                transaction.IssuerId = orderInfo.PaymentInfo.MethodId;
                transaction.EntranceCode = orderInfo.OrderNodeId.ToString();

                #region build urls
                var currentNodeId = Node.GetCurrent().Id;
                var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);


                var returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.SuccesNodeId)));
                #endregion

                var connector = new Connector();
                //connector.ExpirationPeriod = HttpUtility.HtmlEncode();
                connector.MerchantReturnUrl = new Uri(returnUrl);

                transaction = connector.RequestTransaction(transaction);

                orderInfo.PaymentInfo.Url = HttpUtility.HtmlDecode(transaction.IssuerAuthenticationUrl.ToString());

                orderInfo.Save();
            }
            catch (IDealException ex)
            {
                Log.Add(LogTypes.Debug, 0, "iDeal Transaction " + ex.ErrorRes.Error.consumerMessage);
            }
        }

        /// <summary>
        /// Returns the URL to redirect to
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public string GetPaymentUrl(Order order)
        {
            return string.Empty;
        }
    }
}