﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using ING.iDealAdvanced;
using ING.iDealAdvanced.Data;
using umbraco.BusinessLogic;
using uWebshop.Domain;
using uWebshop.Common;
using umbraco;
using uWebshop.Domain.Helpers;
using System.Threading;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Repositories;
using umbraco.NodeFactory;

namespace uWebshop.Payment.iDealProfessional
{
    public class iDealProfessionalResponseHandler : IPaymentResponseHandler
    {
        #region IPaymentResponseHandler Members
        public string GetName()
        {
            return "iDealProfessional";
        }

        public string HandlePaymentResponse()
        {
            var orderId = library.Request("ec");
            var transactionId = library.Request("trxid");

            var orderNode = new Order(int.Parse(orderId));

            var orderInfo = OrderHelper.GetOrderInfo(orderNode.UniqueOrderId);

            if (orderInfo != null)
            {
                try
                {
                    var connector = new Connector();

                    var transaction = connector.RequestTransactionStatus(transactionId);

                    var paymentProvider = new PaymentProvider(orderInfo.PaymentInfo.Id);
                    var helper = new PaymentConfigHelper(paymentProvider);

                    #region build urls
                    var currentNodeId = Node.GetCurrent().Id;
                    var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);


                    var returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.SuccesNodeId)));
                    var cancelUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.ErrorNodeId)));
                    #endregion

                    var status = transaction.Status.ToString();

                    var redirectUrl = baseUrl;

                    switch (status)
                    {
                        case "Success":
                            orderInfo.Status = OrderStatus.ReadyForDispatch;

                            redirectUrl = returnUrl;

                            break;
                        case "Cancelled":
                        case "Expired":
                        case "Failure":
                            orderInfo.Status = OrderStatus.PaymentFailed;
                            orderInfo.PaymentInfo.ErrorMessage = status;

                            redirectUrl = cancelUrl;
                            break;
                        case "Open":
                            orderInfo.Status = OrderStatus.WaitingForPayment;
                            break;
                    }

                    orderInfo.Save();

                    HttpContext.Current.Response.Redirect(redirectUrl);

                }
                catch (IDealException ex)
                {
                    Log.Add(LogTypes.Debug, 0, "iDeal Transaction " + ex.ErrorRes.Error.consumerMessage);
                }
            }
            return null;
        }
        #endregion

     
    }


}