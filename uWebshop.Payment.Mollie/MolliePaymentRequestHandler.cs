﻿using System;
using Mollie.iDEAL;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.Mollie
{
	public class MolliePaymentRequestHandler : IPaymentRequestHandler
	{
		public string GetName()
		{
			return "Mollie";
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return orderInfo.PaymentInfo.Url;
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			if (orderInfo == null) throw new ArgumentNullException("orderInfo");
			try
			{
				var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
				var helper = new PaymentConfigHelper(paymentProvider);

				var partnerId = helper.Settings["PartnerId"];
				if (string.IsNullOrEmpty(partnerId))
				{
					Log.Instance.LogError("Mollie: Missing PaymentProvider.Config  field with name: PartnerId, paymentNodeId: " + paymentProvider.Id);
				}

				var baseUrl = PaymentProviderHelper.GenerateBaseUrl();

				var returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(int.Parse(paymentProvider.SuccesNodeId)));
				var reportUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(paymentProvider.Id));

				var testMode = paymentProvider.TestMode;

				var idealFetch = new IdealFetch
					(
					partnerId,
					testMode,
					orderInfo.OrderNumber,
					reportUrl,
					returnUrl,
					orderInfo.PaymentInfo.MethodId,
					orderInfo.ChargedAmount
					);

				if (!idealFetch.Error)
				{
					var transactionId = idealFetch.TransactionId;

					if (transactionId != null)
					{
						orderInfo.PaymentInfo.TransactionId = transactionId;

						orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;
						orderInfo.PaymentInfo.Url = idealFetch.Url;
						orderInfo.PaymentInfo.TransactionId = transactionId;
						//orderInfo.PaymentInfo.Parameters = orderInfo.PaymentInfo.Url.Split('?')[1];

						uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, transactionId);
					}
				}
				else
				{
					Log.Instance.LogError("MolliePaymentRequestHandler.CreatePaymentRequest: idealFetch.Error: " + idealFetch.ErrorMessage);
				}
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("MolliePaymentRequestHandler.CreatePaymentRequest: " + ex);
			}

			return null;
		}
	}
}