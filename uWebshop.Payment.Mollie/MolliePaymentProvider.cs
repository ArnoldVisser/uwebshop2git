﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mollie.iDEAL;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.Mollie
{
	public class MolliePaymentProvider : IPaymentProvider
	{
		#region IPaymentProvider Members

		public string GetName()
		{
			return "Mollie";
		}

		PaymentTransactionMethod IPaymentProvider.GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(id);

			var helper = new PaymentConfigHelper(paymentProvider);

			var testMode = paymentProvider.TestMode;

			var banks = new IdealBanks(helper.Settings["PartnerId"], testMode);

			var paymentMethods = new List<PaymentProviderMethod>();

			foreach (var bank in banks.Banks)
			{
				var paymentImageId = 0;

				int.TryParse(umbraco.library.GetDictionaryItem(bank.Name + "LogoId"), out paymentImageId);

				paymentMethods.Add(new PaymentProviderMethod
					{
						Id = bank.Id,
						Description = string.Format("iDEAL via {0}", bank.Name),
						Title = bank.Name,
						Name = bank.Name,
						ProviderName = GetName()
					});
			}

			return paymentMethods;
		}

		#endregion
	}
}