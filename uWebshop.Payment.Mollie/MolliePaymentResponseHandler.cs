﻿using System;
using Mollie.iDEAL;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.Mollie
{
	public class MolliePaymentResponseHandler : IPaymentResponseHandler
	{
		public string GetName()
		{
			return "Mollie";
		}

		public string HandlePaymentResponse()
		{

			try
			{
				//Mollie gives us the transaction Id.
				var transactionId = library.Request("transaction_id");

				Log.Instance.LogDebug( "Mollie Transaction Id: " + transactionId);

				if (string.IsNullOrEmpty(transactionId))
				{
					Log.Instance.LogError("Mollie: TransactionId == IsNullOrEmpty");
					return string.Empty;
				}

				var orderInfo = OrderHelper.GetOrderInfo(transactionId);

				if (orderInfo == null)
				{
					Log.Instance.LogError("Mollie: Order Not Found For TransactionId");
					return string.Empty;
				}

				if (orderInfo.Paid != false)
				{
					Log.Instance.LogError("Mollie: Order already paid!");
					return string.Empty;
				}

				var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
				var helper = new PaymentConfigHelper(paymentProvider);

				var partnerId = helper.Settings["PartnerId"];
				if (string.IsNullOrEmpty(partnerId))
				{
					Log.Instance.LogError("Mollie: Missing PaymentProvider.Config  field with name: PartnerId, paymentProviderNodeId: " + paymentProvider.Id);
					return string.Empty;
				}

				var testMode = paymentProvider.TestMode;

				var idealCheck = new IdealCheck(partnerId, testMode, transactionId);

				if (idealCheck.Error)
				{
					if (idealCheck.Message != null)
						Log.Instance.LogError(
						        string.Format("Mollie idealCheck.Error Error! idealCheck.Message: {0}", idealCheck.Message));
					if (idealCheck.ErrorMessage != null)
						Log.Instance.LogError(
						        string.Format("Mollie idealCheck.Error Error! idealCheck.ErrorMessage: {0}", idealCheck.ErrorMessage));

					if (orderInfo.Status == OrderStatus.ReadyForDispatch)
					{
						return string.Empty;
					}

					orderInfo.Paid = false;
					orderInfo.Status = OrderStatus.PaymentFailed;
					
					if (idealCheck.ErrorMessage != null)
					{
						orderInfo.PaymentInfo.ErrorMessage = idealCheck.ErrorMessage;
					}
				}

				if (idealCheck.Payed)
				{
					orderInfo.Paid = true;
					orderInfo.Status = OrderStatus.ReadyForDispatch;
				}
				else
				{
					if (idealCheck.Message != null)
						Log.Instance.LogError(
						        string.Format("Mollie idealCheck.Payed Error! idealCheck.Message: {0}", idealCheck.Message));
					if (idealCheck.ErrorMessage != null)
						Log.Instance.LogError(
						        string.Format("Mollie idealCheck.Payed Error! idealCheck.ErrorMessage: {0}", idealCheck.ErrorMessage));

					orderInfo.Paid = false;
					orderInfo.Status = OrderStatus.PaymentFailed;
					orderInfo.PaymentInfo.ErrorMessage = idealCheck.ErrorMessage;
				}

				orderInfo.Save();
			}
			catch (Exception ex)
			{
				Log.Instance.LogError("MolliePaymentResponseHandler.HandlePaymentResponse: " + ex);
			}

			return null;
		}
	}
}