﻿using System;
using System.IO;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Xml;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Web.Routing;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.cms.businesslogic.web;
using uWebshop.Umbraco.Businesslogic;

namespace uWebshop.Umbraco6.Mvc
{
	public class Application : ApplicationEventHandler
	{
		protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			base.ApplicationStarting(umbracoApplication, applicationContext);

		    if (InternalHelpers.MvcRenderMode)
		    {
		        ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByPageIdQuery, CatalogContentFinder>();
		    }
		}
    }

	public class CatalogContentFinder : IContentFinder
	{
		public bool TryFindContent(PublishedContentRequest contentRequest)
		{
			var uwebshopRequest = UwebshopRequest.Current;
			var content = uwebshopRequest.Product ?? (IUwebshopEntity)uwebshopRequest.Category ?? uwebshopRequest.PaymentProvider ?? // in case ResolveUwebshopEntityUrl was already called from the module
			              IO.Container.Resolve<IUrlRewritingService>().ResolveUwebshopEntityUrl().Entity;

			if (content is PaymentProvider)
			{
				var paymentProvider = content as PaymentProvider;

				Log.Instance.LogDebug("UmbracoDefaultAfterRequestInit paymentProvider: " + paymentProvider.Name);

				new PaymentRequestHandler().HandleuWebshopPaymentRequest(paymentProvider.Id);

				var publishedContent = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetById(paymentProvider.Id);
				if (publishedContent == null) return false;
				contentRequest.PublishedContent = publishedContent;
				return true;
			}

			if (content is Category)
			{
				var categoryFromUrl = content as Category;

				if (categoryFromUrl.Disabled) return false;

				if (Access.HasAccess(categoryFromUrl.Id, categoryFromUrl.Path, Membership.GetUser()))
				{
					var doc = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetById(content.Id);
					if (doc != null)
					{
						contentRequest.PublishedContent = doc;
						var altTemplate = HttpContext.Current.Request["altTemplate"];
						contentRequest.TrySetTemplate(altTemplate);
						return true;
					}
				}
				else
				{
					if (HttpContext.Current.User.Identity.IsAuthenticated)
					{
						contentRequest.SetRedirect(library.NiceUrl(Access.GetErrorPage(categoryFromUrl.Path)));
					}
					contentRequest.SetRedirect(library.NiceUrl(Access.GetLoginPage(categoryFromUrl.Path)));
					return true;
				}
			}

			else if (content is Product)
			{
				var productFromUrl = content as Product;
				if (productFromUrl.Disabled) return false;

				if (Access.HasAccess(productFromUrl.Id, productFromUrl.Path, Membership.GetUser()))
				{
					var doc = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetById(content.Id);
					if (doc != null)
					{
						contentRequest.PublishedContent = doc;
						var altTemplate = HttpContext.Current.Request["altTemplate"];
						contentRequest.TrySetTemplate(altTemplate);
						return true;
					}
				}
				else
				{
					if (HttpContext.Current.User.Identity.IsAuthenticated)
					{
						contentRequest.SetRedirect(library.NiceUrl(Access.GetErrorPage(productFromUrl.Path)));
					}
					contentRequest.SetRedirect(library.NiceUrl(Access.GetLoginPage(productFromUrl.Path)));
					return true;
				}
			}
			return false;
		}
	}
}
