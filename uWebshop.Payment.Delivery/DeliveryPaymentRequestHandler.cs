﻿using SuperSimpleWebshop.Domain;
using SuperSimpleWebshop.Domain.Interfaces;

namespace SuperSimpleWebshop.Payment.Delivery
{
    public class DeliveryPaymentRequestHandler : IPaymentRequestHandler
    {
        #region IPaymentRequestHandler Members
        public string GetName()
        {
            return "Delivery";
        }

        public PaymentRequest CreatePaymentRequest(Order order)
        {
            return null;
        }
        #endregion
    }
}
