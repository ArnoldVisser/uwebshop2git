﻿using System.Collections.Generic;
using SuperSimpleWebshop.Common;
using SuperSimpleWebshop.Domain;
using SuperSimpleWebshop.Domain.Helpers;
using SuperSimpleWebshop.Domain.Interfaces;

namespace SuperSimpleWebshop.Payment.Delivery
{
    public class DeliveryPaymentProvider : IPaymentProvider
    {
        #region IPaymentProvider Members
        public string GetName()
        {
            return "Delivery";
        }

        public PaymentParameterRenderMethod GetParameterRenderMethod()
        {
            return PaymentParameterRenderMethod.Custom;
        }

        public List<PaymentMethod> GetAllPaymentMethods(string name)
        {
            var paymentNodeID = DomainHelper.GetNodeIdForDocument("sswsPaymentProvider", "Delivery");

            var provider = new PaymentProvider(paymentNodeID);

            var paymentMethods = new List<PaymentMethod>();
            paymentMethods.Add(new PaymentMethod
            {
                Description = provider.PaymentProviderTitle,
                //Id = "Delivery",
                Name = "Delivery"
            });

            return paymentMethods;
        }
        #endregion
    }
}
