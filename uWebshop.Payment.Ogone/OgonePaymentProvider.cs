﻿using System.Collections.Generic;
using System.Linq;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using umbraco;

namespace uWebshop.Payment.Ogone
{
	public class OgonePaymentProvider : IPaymentProvider
	{
		#region IPaymentProvider Members

		public string GetName()
		{
			return "Ogone";
		}

		PaymentTransactionMethod IPaymentProvider.GetParameterRenderMethod()
		{
			return PaymentTransactionMethod.QueryString;
		}

		public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
		{
			var paymentMethods = new List<PaymentProviderMethod>();

			return paymentMethods;
		}

		#endregion
	}
}