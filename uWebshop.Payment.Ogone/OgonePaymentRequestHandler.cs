﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Payment.Ogone
{
	public class OgonePaymentRequestHandler : IPaymentRequestHandler
	{
		public string GetName()
		{
			return "Ogone";
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			//#region build urls
			//var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);

			//if (!paymentProviderNodeUrl.StartsWith("http"))
			//{
			//	var currentNodeId = Node.GetCurrent().Id;
			//	var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);
			//	paymentProviderNodeUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));
			//}
			//var reportUrl = paymentProviderNodeUrl;

			#region build urls

			var baseUrl = PaymentProviderHelper.GenerateBaseUrl();

			var successNodeId = 0;
			int.TryParse(paymentProvider.SuccesNodeId, out successNodeId);

			var returnUrl = baseUrl;

			if (successNodeId != 0)
			{
				returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(successNodeId));
			}

			Log.Instance.LogDebug("Ogone returnUrl " + returnUrl + ", paymentProviderNodeId: " + paymentProvider.Id);

			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);

			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));

			Log.Instance.LogDebug("Ogone reportUrl " + reportUrl + ", paymentProviderNodeId: " + paymentProvider.Id);

			#endregion


			#region config helper

			var pspId = helper.Settings["PSPID"];
			if (string.IsNullOrEmpty(pspId))
			{
				Log.Instance.LogError("Ogone: Missing PaymentProvider.Config  field with name: PSPID" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			var shaInSignature = helper.Settings["SHAInSignature"];
			if (string.IsNullOrEmpty(shaInSignature))
			{
				Log.Instance.LogError("Ogone: Missing PaymentProvider.Config  field with name: SHAInSignature" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			var secureHashAlgorithm = helper.Settings["SecureHashAlgorithm"];
			if (string.IsNullOrEmpty(secureHashAlgorithm))
			{
				Log.Instance.LogError("Ogone: Missing PaymentProvider.Config  field with name: SecureHashAlgorithm (value should be: SHA1/SHA256/SHA512)" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var testURL = helper.Settings["testURL"];
			if (string.IsNullOrEmpty(testURL))
			{
				Log.Instance.LogError("Ogone: Missing PaymentProvider.Config  field with name: testURL" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var liveUrl = helper.Settings["url"];
			if (string.IsNullOrEmpty(liveUrl))
			{
				Log.Instance.LogError("Ogone: Missing PaymentProvider.Config  field with name: url" + ", paymentProviderNodeId: " + paymentProvider.Id);
			}
			
			#endregion


			var request = new PaymentRequest();
			request.Parameters.Add("PSPID", pspId);
			request.Parameters.Add("ORDERID", orderInfo.OrderNumber);

			var amount = orderInfo.ChargedAmountInCents;

			request.Parameters.Add("AMOUNT", amount.ToString());
			var ri = new RegionInfo(orderInfo.StoreInfo.Store.CurrencyCultureInfo.LCID);
			request.Parameters.Add("CURRENCY", ri.ISOCurrencySymbol);
			request.Parameters.Add("LANGUAGE", orderInfo.StoreInfo.LanguageCode + "_" + orderInfo.StoreInfo.CountryCode);
			//request.Parameters.Add("CN", order.CustomerFirstName + " " + order.CustomerLastName);

			request.Parameters.Add("EMAIL", OrderHelper.CustomerInformationValue(orderInfo, "customerEmail"));

			request.Parameters.Add("ACCEPTURL", reportUrl);
			request.Parameters.Add("DECLINEURL", reportUrl);
			request.Parameters.Add("EXCEPTIONURL", reportUrl);
			request.Parameters.Add("CANCELURL", reportUrl);

			//action => 'Normal Authorization' (with operation => 'SAL' (default))
			//action => 'Authorization Only' (with operation => 'RES' (default)) then action => 'Post Authorization' (with operation => 'SAS' (default))
			request.Parameters.Add("OPERATION", "SAL");

			var transactionId = orderInfo.UniqueOrderId.ToString().Replace("-", "");

			request.Parameters.Add("PARAMPLUS", string.Format("TransactionId={0}", transactionId));


			if (orderInfo.PaymentInfo.MethodTitle.Contains('|'))
			{
				var temp = orderInfo.PaymentInfo.MethodTitle.Split('|');

				request.Parameters.Add("PM", temp[0]);
				request.Parameters.Add("BRAND", temp[1]);
			}
			else
			{
				request.Parameters.Add("PM", orderInfo.PaymentInfo.MethodTitle);
			}

			var stringToHash = string.Empty;
			var sortedParameters = request.Parameters.OrderBy(x => x.Key);

			foreach (var parameter in sortedParameters)
			{
				stringToHash += string.Format("{0}={1}{2}", parameter.Key, parameter.Value, shaInSignature);
			}

			switch (secureHashAlgorithm)
			{
				case "SHA1":
					request.Parameters.Add("SHASIGN", GetSHA1Hash(stringToHash).ToUpper());
					break;
				case "SHA256":
					request.Parameters.Add("SHASIGN", GetSHA256Hash(stringToHash).ToUpper());
					break;
				case "SHA512":
					request.Parameters.Add("SHASIGN", GetSHA512Hash(stringToHash).ToUpper());
					break;
			}

			#region URLforTestMode

			// check if provider is in testmode to send request to right URL

			request.PaymentUrlBase = paymentProvider.TestMode ? testURL : liveUrl;

			#endregion

			orderInfo.PaymentInfo.Url = request.PaymentUrl;
			orderInfo.PaymentInfo.Parameters = request.ParametersAsString;
			orderInfo.PaymentInfo.TransactionId = transactionId;

			uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, transactionId);

			return request;
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return string.Empty;
		}


		// Create an sha1 hash string of this string
		public static string GetSHA1Hash(string str)
		{
			SHA1Managed x = new SHA1Managed();

			byte[] data = Encoding.ASCII.GetBytes(str);

			data = x.ComputeHash(data);

			return BitConverter.ToString(data).Replace("-", "").ToLower();
		}

		// Create an sha256 hash string of this string
		public static string GetSHA256Hash(string str)
		{
			SHA256Managed x = new SHA256Managed();

			byte[] data = Encoding.ASCII.GetBytes(str);

			data = x.ComputeHash(data);

			return BitConverter.ToString(data).Replace("-", "").ToLower();
		}

		// Create an sha512 hash string of this string
		public static string GetSHA512Hash(string str)
		{
			SHA512Managed x = new SHA512Managed();

			byte[] data = Encoding.ASCII.GetBytes(str);

			data = x.ComputeHash(data);

			return BitConverter.ToString(data).Replace("-", "").ToLower();
		}
	}
}