﻿using System;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.AuthorizeNet
{
    public class AuthorizeNetPaymentResultHandler : IPaymentResultHandler
    {
        #region IPaymentResultHandler Members
        public string GetName()
        {
            return "AuthorizeNet";
        }

        public string GetResultHtml(Domain.Order order)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
