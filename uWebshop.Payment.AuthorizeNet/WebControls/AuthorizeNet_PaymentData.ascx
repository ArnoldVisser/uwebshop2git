﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuthorizeNet_PaymentData.ascx.cs" Inherits="uWebshop.Payment.AuthorizeNet.WebControls.AuthorizeNet_PaymentData" %>

<form id="form1" runat="server">
    <%=AuthorizeNet.Helpers.CheckoutFormBuilders.CreditCardForm(true) %>

    <asp:Button ID="btnPay" OnClick="btnPay_Click" Text="Pay" runat="server" />
</form>
