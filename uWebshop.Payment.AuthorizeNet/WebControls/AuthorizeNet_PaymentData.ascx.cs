﻿using System;
using System.Linq;
using AuthorizeNet;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco;
using umbraco.BusinessLogic;

namespace uWebshop.Payment.AuthorizeNet.WebControls
{
    public partial class AuthorizeNet_PaymentData : System.Web.UI.UserControl
    {
        #region properties
        private Domain.Order Order;
        #endregion

        #region event handlers
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnPay_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(library.RequestCookies("PaymentOrderId")))
            {
                Order = new Domain.Order(library.RequestCookies("PaymentOrderId"));

                //pull from the store
                var gate = OpenGateway();

                //build the request from the Form post
                var apiRequest = CheckoutFormReaders.BuildAuthAndCaptureFromPost();

                //set the amount - you can also set this from the page itself
                //you have to have a field named "x_amount"
                apiRequest.Queue(ApiFields.Amount, Order.OrderInfo.TotalAmount.ToString());
                apiRequest.Queue(ApiFields.InvoiceNumber, Order.OrderInfo.OrderNumber);
                apiRequest.Queue(ApiFields.FirstName, Order.CustomerFirstName);
                apiRequest.Queue(ApiFields.LastName, Order.CustomerLastName);

                try
                {
                    string countryName = ShippingHelper.GetAllCountries().Where(x => x.Code == Order.OrderInfo.CustomerInfo.CountryCode).FirstOrDefault().Name;

                    apiRequest.Queue(ApiFields.Country, countryName);
                }
                catch { }

                //Log.Add(LogTypes.Debug, 0, "apiRequest.TestRequest: " + apiRequest.TestRequest);
                //Log.Add(LogTypes.Debug, 0, "apiRequest.ApiAction: " + apiRequest.ApiAction.ToString());
                //Log.Add(LogTypes.Debug, 0, "apiRequest.CardNum: " + apiRequest.CardNum.ToString());

                //send to Auth.NET
                var response = gate.Send(apiRequest);

                //Log.Add(LogTypes.Debug, 0, "response.Amount: " + response.Amount.ToString());
                //Log.Add(LogTypes.Debug, 0, "Order.OrderInfo.TotalAmount: " + Order.OrderInfo.TotalAmount.ToString());
                //Log.Add(LogTypes.Debug, 0, "response.Approved: " + response.Approved.ToString());
                //Log.Add(LogTypes.Debug, 0, "response.ResponseCode: " + response.ResponseCode.ToString());
                //Log.Add(LogTypes.Debug, 0, "response.Message: " + response.Message.ToString());
                //Log.Add(LogTypes.Debug, 0, "response.AuthorizationCode: " + response.AuthorizationCode.ToString());
                //Log.Add(LogTypes.Debug, 0, "response.InvoiceNumber: " + response.InvoiceNumber.ToString());
                //Log.Add(LogTypes.Debug, 0, "response.CardNumber: " + response.CardNumber.ToString());
                //Log.Add(LogTypes.Debug, 0, "response.TransactionID: " + response.TransactionID.ToString());

                //be sure the amount paid is the amount required
                if (response.Amount < Order.OrderInfo.TotalAmount)
                {
                    Order.OrderInfo.Status = OrderStatus.Ready_for_Dispatch;
                    Order.Save();

                    var nodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmationFailed"), ShopAliasHelper.GetCurrentShopAlias());
                    Response.Redirect(library.NiceUrl(nodeId));
                }

                if (response.Approved)
                {
                    Order.OrderInfo.Status = OrderStatus.Ready_for_Dispatch;
                    Order.Save();

                    var nodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmation"), ShopAliasHelper.GetCurrentShopAlias());
                    Response.Redirect(library.NiceUrl(nodeId));
                }
                else
                {
                    Order.OrderInfo.Status = OrderStatus.Payment_Failed;
                    Order.Save();

                    var nodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmationFailed"), ShopAliasHelper.GetCurrentShopAlias());
                    Response.Redirect(library.NiceUrl(nodeId));
                }
            }
        }
        #endregion

        #region private methods
        private IGateway OpenGateway()
        {
            var paymentConfigHelper = new PaymentConfigHelper(Order.OrderInfo.PaymentInfo.PaymentProviderName);

            //we used the form builder so we can now just load it up
            //using the form reader
            var login = paymentConfigHelper.Settings["APIid"];
            var transactionKey = paymentConfigHelper.Settings["TransactionKey"];

            Log.Add(LogTypes.Debug, 0, "login: " + login);
            Log.Add(LogTypes.Debug, 0, "transactionKey: " + transactionKey);

            //this is set to test mode - change as needed.
            return new Gateway(login, transactionKey, true);
        }
        #endregion
    }
}
