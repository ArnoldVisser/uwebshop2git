﻿using System.Collections.Generic;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.AuthorizeNet
{
    public class AuthorizeNetPaymentProvider : IPaymentProvider
    {
        #region IPaymentProvider Members

        public string GetName()
        {
            return "AuthorizeNet";
        }

        public Common.PaymentParameterRenderMethod GetParameterRenderMethod()
        {
            return Common.PaymentParameterRenderMethod.QueryString;
        }

        public List<Domain.PaymentProviderMethod> GetAllPaymentMethods(string name)
        {
            var helper = new PaymentConfigHelper(name);

            var paymentMethods = helper.PaymentProviderMethods;

            foreach (var p in paymentMethods)
            {
                p.ProviderName = GetName();
                p.ProviderNodeName = name;
                p.Description = p.Name;
            }

            return paymentMethods;
        }
        #endregion
    }
}
