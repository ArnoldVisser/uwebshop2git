﻿using System;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.AuthorizeNet
{
    public class AuthorizeNetPaymentResponseHandler : IPaymentResponseHandler
    {
        #region IPaymentResponseHandler Members
        public string GetName()
        {
            return "AuthorizeNet";
        }

        public string HandlePaymentResponse()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
