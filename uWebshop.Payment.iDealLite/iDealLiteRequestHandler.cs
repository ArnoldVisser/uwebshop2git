﻿using System;
using uWebshop.DataAccess;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain;
using System.Security.Cryptography;
using uWebshop.Common;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.NodeFactory;

namespace uWebshop.Payment.iDealLite
{
	public class iDealLiteRequestHandler : IPaymentRequestHandler
	{
		#region IPaymentRequestHandler Members
		/// <summary>
		/// Gets the payment provider name
		/// </summary>
		/// <returns>Name of the payment provider</returns>
		public string GetName()
		{
			return "iDealLite";
		}

		/// <summary>
		/// Creates a payment request for this payment provider
		/// </summary>
		/// <param name="orderInfo">Order to base the payment request on</param>
		/// <returns>Payment request</returns>
		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			var request = new PaymentRequest();
		
			var uniqueId = DateTime.Now.ToString("yyyyMMddHHmmss");
			
			var merchantKey = helper.Settings["merchantkey"];
			var testAmountKey = helper.Settings["testAmountInCents"];
			var accountIdKey = helper.Settings["accountId"];
			var subIdKey = helper.Settings["subID"];
			var domainNameKey = helper.Settings["domainName"];
			var testUrlKey = helper.Settings["testURL"];
			var urlKey =  helper.Settings["url"];

			if (string.IsNullOrEmpty(merchantKey))
			{
				throw new Exception("merchantkey not found");
			}
			if (string.IsNullOrEmpty(testAmountKey))
			{
				throw new Exception("testAmountInCents not found");
			}
			if (string.IsNullOrEmpty(accountIdKey))
			{
				throw new Exception("accountId not found");
			}
			if (string.IsNullOrEmpty(subIdKey))
			{
				throw new Exception("subID not found");
			}
			if (string.IsNullOrEmpty(domainNameKey))
			{
				throw new Exception("domainName not found");
			}
			if (string.IsNullOrEmpty(testUrlKey))
			{
				throw new Exception("testURL not found");
			}
			if (string.IsNullOrEmpty(urlKey))
			{
				throw new Exception("url not found");
			}


			//Description item 1 declaren
			var itemDescription1 = "Bestelling: " + orderInfo.OrderNumber;
		
			//Valid until: a week (7 days)
			var validUntil = DateTime.Now.AddDays(7).ToString("yyyy-MM-ddTHH:mm:ss") + ":0000Z";

		
			string totalAmountAsString;

			if (paymentProvider.TestMode)
			{
				var testAmount = 0;

				int.TryParse(testAmountKey, out testAmount);
				//Test mode
				if(testAmount == 0)
				{//test ammount is 0, so get normal value
					totalAmountAsString = orderInfo.ChargedAmount.ToString();
				}
				else
				{
					//use test ammount
					totalAmountAsString = testAmount.ToString();
				}
			}
			else
			{
				totalAmountAsString = orderInfo.Grandtotal.ToString();
			}

			

			//change string into double

			var totalAmountAsDouble = double.Parse(totalAmountAsString);
			//make 2 decimals and change into cents
			totalAmountAsDouble = Math.Round(totalAmountAsDouble, 2) * 100;
			//change cents variable back into string
			totalAmountAsString = totalAmountAsDouble.ToString();
			
			 // retrieve accountId from uwbsPayment.config <accountId></accountId>
			request.Parameters.Add("merchantID", accountIdKey);
			// retrieve subID from uwbsPayment.config <subID></subID>
			request.Parameters.Add("subID", subIdKey);
			request.Parameters.Add("amount", totalAmountAsString);
			request.Parameters.Add("purchaseID", uniqueId);
			request.Parameters.Add("language", "nl");
			request.Parameters.Add("currency", "EUR");
			request.Parameters.Add("paymentType", "ideal");
			request.Parameters.Add("description", itemDescription1);
			request.Parameters.Add("validUntil", validUntil);

			request.Parameters.Add("itemNumber1", "1");
			request.Parameters.Add("itemQuantity1", "1");
			request.Parameters.Add("itemPrice1", totalAmountAsString);
			request.Parameters.Add("itemDescription1", "Bestelling: " + domainNameKey);
		
			var idealstring = merchantKey + accountIdKey + subIdKey + totalAmountAsString + uniqueId + "ideal" + validUntil + "1" + "Bestelling: " + domainNameKey + "1" + totalAmountAsString;
			idealstring = idealstring.Replace(" ", "");

			var encoding = new System.Text.ASCIIEncoding();
			var hashBytes = encoding.GetBytes(idealstring);
			//'Compute the SHA-1 hash
			var sha1 = new SHA1CryptoServiceProvider();
			var cryptIdealString = "";
			var cryptIdealbyte = sha1.ComputeHash(hashBytes);
			var i = default(Int32);
			for (i = 0; i <= 19; i++)
			{
				var tmp = cryptIdealbyte[i].ToString("X");
				if (tmp.Length == 1)
				{
					tmp = "0" + tmp;
				}
				cryptIdealString = cryptIdealString + tmp;
			}
			cryptIdealString = cryptIdealString.ToLower();
			
			request.Parameters.Add("hash", cryptIdealString);
			
			#region testmode
		  
			var apiURL = paymentProvider.TestMode ? testUrlKey : urlKey;

			#endregion

			orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;
			
			#region URLforTestMode
			// check if provider is in testmode to send request to right URL

			request.PaymentUrlBase = apiURL;
		   
			#endregion

			orderInfo.PaymentInfo.Url = apiURL;
			orderInfo.PaymentInfo.Parameters = request.ParametersAsString;
		
			uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, uniqueId);
			orderInfo.Save();

			return request;
		}
		#endregion

		/// <summary>
		/// Returns the URL to redirect to
		/// </summary>
		/// <param name="orderInfo"></param>
		/// <returns></returns>
		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return orderInfo.PaymentInfo.Url;
		}
	}
}