﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco;
using uWebshop.Common;
using umbraco.NodeFactory;

namespace uWebshop.Payment.iDealLite
{
    public partial class iDealLiteSuccesHandler : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
	        if (Request["PaymentStatus"] == null) return;
	        var paymentStatus = Request["PaymentStatus"];

	        if (string.IsNullOrEmpty(library.RequestCookies("PaymentOrderId")) || paymentStatus != "success") return;

	        var order = OrderHelper.GetOrderInfo(library.RequestCookies("PaymentOrderId"));

	        order.Status = OrderStatus.ReadyForDispatch;
	        order.Paid = true;
	        order.Save();

	        Response.Redirect(library.NiceUrl(Node.GetCurrent().Id), false);
        }
    }
}