﻿using System.Collections.Generic;
using uWebshop.Domain.Interfaces;
using uWebshop.Common;
using uWebshop.Domain;

namespace uWebshop.Payment.iDealLite
{
    public class iDealLiteProvider : IPaymentProvider
    {
        #region IPaymentProvider Members
        public string GetName()
        {
            return "iDealLite";
        }

	    PaymentTransactionMethod IPaymentProvider.GetParameterRenderMethod()
	    {
			return PaymentTransactionMethod.QueryString;
	    }

	    public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
	    {
			var paymentProviderMethodList = new List<PaymentProviderMethod>
				{
					new PaymentProviderMethod
						{
							Id = "iDealLite",
							ProviderName = GetName(),
							Title = "iDeal",
							Description = "iDeal"
						}
				};

			return paymentProviderMethodList;
	    }
        #endregion
    }
}