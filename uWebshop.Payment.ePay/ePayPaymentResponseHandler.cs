﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using umbraco;
using umbraco.NodeFactory;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.ePay
{
	public class ePayPaymentResponseHandler : IPaymentResponseHandler
	{
		public string GetName()
		{
			return "ePay";
		}

		public string HandlePaymentResponse()
		{
			var orderId = HttpContext.Current.Request.QueryString["orderid"];

			var paymentProvider =
					PaymentProviderHelper.GetAllPaymentProviders().FirstOrDefault(x => x.Name.ToLower() == GetName().ToLower());

			var helper = new PaymentConfigHelper(paymentProvider);

			if (paymentProvider != null)
			{
				#region build urls

				var currentNodeId = Node.GetCurrent().Id;
				var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

				var successNodeId = 0;
				int.TryParse(paymentProvider.SuccesNodeId, out successNodeId);

				var acceptUrl = baseUrl;

				if (successNodeId != 0)
				{
					acceptUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(successNodeId));
				}

				var failedNodeId = 0;
				int.TryParse(paymentProvider.ErrorNodeId, out failedNodeId);

				var failedUrl = baseUrl;

				if (failedNodeId != 0)
				{
					failedUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(failedNodeId));
				}

				#endregion

				var redirectUrl = failedUrl;

				if (!string.IsNullOrEmpty(orderId))
				{
					var order = OrderHelper.GetOrderInfo(orderId);

					var amount = HttpContext.Current.Request.QueryString["amount"];

					if (amount == order.ChargedAmountInCents.ToString())
					{
						order.Paid = true;
						order.Status = OrderStatus.ReadyForDispatch;
						order.Save();

						redirectUrl = acceptUrl;
					}
					else
					{
						order.Paid = false;
						order.Save();
					}
				}


				HttpContext.Current.Response.Redirect(redirectUrl);
			}

			return string.Empty;
		}
	}
}