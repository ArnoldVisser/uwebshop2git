﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using umbraco;
using umbraco.NodeFactory;
using uWebshop.Common;
using uWebshop.DataAccess;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;

namespace uWebshop.Payment.ePay
{
	public class ePayPaymentRequestHandler : IPaymentRequestHandler
	{
		public string GetName()
		{
			return "ePay";
		}

		public PaymentRequest CreatePaymentRequest(OrderInfo orderInfo)
		{
			var paymentProvider = PaymentProvider.GetPaymentProvider(orderInfo.PaymentInfo.Id);
			var helper = new PaymentConfigHelper(paymentProvider);

			#region build urls

			var currentNodeId = Node.GetCurrent().Id;
			var baseUrl = PaymentProviderHelper.GenerateBaseUrl(currentNodeId);

			var successNodeId = 0;
			int.TryParse(paymentProvider.SuccesNodeId, out successNodeId);

			var acceptUrl = baseUrl;

			if (successNodeId != 0)
			{
				acceptUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(successNodeId));
			}

			var failedNodeId = 0;
			int.TryParse(paymentProvider.ErrorNodeId, out failedNodeId);

			var failedUrl = baseUrl;

			if (failedNodeId != 0)
			{
				failedUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(failedNodeId));
			}

			Log.Instance.LogDebug("ePay returnUrl " + acceptUrl + ", paymentProviderNodeId: " + paymentProvider.Id);

			var paymentProviderNodeUrl = library.NiceUrl(paymentProvider.Id);

			var reportUrl = string.Format("{0}{1}", baseUrl, paymentProviderNodeUrl.Replace(".aspx", string.Empty));

			Log.Instance.LogDebug("ePay reportUrl " + reportUrl + ", paymentProviderNodeId: " + paymentProvider.Id);

			#endregion

			#region config helper

			var merchantnumber = helper.Settings["merchantnumber"];
			if (string.IsNullOrEmpty(merchantnumber))
			{
				Log.Instance.LogDebug("ePay: Missing PaymentProvider.Config  field with name: merchantnumber" +
				                      ", paymentProviderNodeId: " + paymentProvider.Id);
			}

			var liveUrl = helper.Settings["url"];
			if (string.IsNullOrEmpty(liveUrl))
			{
				Log.Instance.LogDebug("ePay: Missing PaymentProvider.Config field with name: url" + ", paymentProviderNodeId: " +
				                      paymentProvider.Id);
			}

			var uniqueId = orderInfo.OrderNumber + "x" + DateTime.Now.ToString("hhmmss");

			Log.Instance.LogDebug("ePay uniqueId " + uniqueId + ", paymentProviderNodeId: " + paymentProvider.Id);

			//<provider title="ePay">
			//  <merchantnumber>#YOUR Merchant Number#</accountId>
			//  <url>https://ssl.ditonlinebetalingssystem.dk/integration/ewindow/Default.aspx</url>
			//</provider> 

			#endregion

			var request = new PaymentRequest();

			// retrieve Account ID
			request.Parameters.Add("merchantnumber", merchantnumber);
			request.Parameters.Add("amount", orderInfo.ChargedAmountInCents.ToString());
			request.Parameters.Add("orderid", uniqueId);


			request.Parameters.Add("callbackurl", reportUrl);
			request.Parameters.Add("accepturl", reportUrl);
			request.Parameters.Add("cancelurl", failedUrl);


			var ri = new RegionInfo(orderInfo.StoreInfo.Store.CurrencyCultureInfo.LCID);
			request.Parameters.Add("currency", ri.ISOCurrencySymbol);
			request.Parameters.Add("windowstate", "3");

			orderInfo.PaymentInfo.TransactionMethod = PaymentTransactionMethod.QueryString;

			#region URLforTestMode

			// check if provider is in testmode to send request to right URL
			request.PaymentUrlBase = liveUrl;

			#endregion

			uWebshopOrders.SetTransactionId(orderInfo.UniqueOrderId, uniqueId);
			orderInfo.PaymentInfo.TransactionId = uniqueId;
			orderInfo.PaymentInfo.Url = request.PaymentUrl;
			orderInfo.PaymentInfo.Parameters = request.ParametersAsString;

			return request;
		}

		public string GetPaymentUrl(OrderInfo orderInfo)
		{
			return orderInfo.PaymentInfo.Url;
		}
	}
}