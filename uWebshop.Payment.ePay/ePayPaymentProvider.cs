﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Xml.Linq;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using umbraco;

namespace uWebshop.Payment.ePay
{
    public class ePayPaymentProvider : IPaymentProvider
    {
        public string GetName()
        {
            return "ePay";
        }

        public PaymentTransactionMethod GetParameterRenderMethod()
        {
			return PaymentTransactionMethod.QueryString;
        }

	    public IEnumerable<PaymentProviderMethod> GetAllPaymentMethods(int id)
	    {
			var paymentProviderMethodList = new List<PaymentProviderMethod>
				{
					new PaymentProviderMethod
						{
							Id = "ePay",
							ProviderName = GetName(),
							Title = "ePay",
							Description = "ePay Payment Provider"
						}
				};

			return paymentProviderMethodList;
	    }

	 
    }
}