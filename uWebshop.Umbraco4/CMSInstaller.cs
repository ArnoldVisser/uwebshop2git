﻿using System;
using System.Collections.Generic;
using System.Linq;
using uWebshop.DataAccess;
using uWebshop.DataTypes.DiscountOrderCondition;
using uWebshop.DataTypes.EmailTemplateSelector;
using uWebshop.DataTypes.EnableDisable;
using uWebshop.DataTypes.LanguagePicker;
using uWebshop.DataTypes.MemberGroupPicker;
using uWebshop.DataTypes.OrderStatusPicker;
using uWebshop.DataTypes.OrderStatusSection;
using uWebshop.DataTypes.StockUpdate;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Common;
using uWebshop.DataTypes.StorePicker;
using uWebshop.Domain;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.DataLayer;
using umbraco.cms.businesslogic.template;
using umbraco.cms.businesslogic.web;
using DataTypeDefinition = umbraco.cms.businesslogic.datatype.DataTypeDefinition;
using uWebshop.DataTypes.OrderCountViewer;
using uWebshop.DataTypes.StoreTemplatePicker;
using uWebshop.DataTypes.OrderInfoViewer;
using uWebshop.DataTypes.Ranges;
using uWebshop.DataTypes.ZoneSelector;
using uWebshop.DataTypes.DiscountType;
using uWebshop.DataTypes.ShippingProviderType;
using uWebshop.DataTypes.ShippingRangeType;
using uWebshop.DataTypes.PaymentProviderType;
using uWebshop.DataTypes.Price;
using umbraco.BasePages;
using Examine;
using Helpers = uWebshop.Umbraco.Helpers;
using uWebshop.DataTypes.RazorWrapper;
using uWebshop.DataTypes.PaymentProviderAmountType;
using uWebshop.DataTypes.CountySelector;
using uWebshop.DataTypes.CouponCodeEditor;
using uWebshop.DataTypes.EmailPreview;

namespace uWebshop.Umbraco4
{
	class CMSInstaller : ICMSInstaller
	{
		private static bool _update;
		private static int _uWebshopDocId;

		public void Install()
		{
			Helpers.uWebshopInstalleruWebshopPaymentsHandlerTemplate();

			var author = new User(0);

			umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer Create Document Types Start: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			// create document types if they don't exists yet;
			foreach (var documentTypeAlias in DocumentTypeAliasList.List)
			{
				var dt = DocumentType.GetByAlias(documentTypeAlias);

				if (dt != null) continue;
				var documentType = DocumentType.MakeNew(author, documentTypeAlias);
				documentType.Alias = documentTypeAlias;
				documentType.Text = documentTypeAlias.Replace("uwbs", string.Empty);
				documentType.Description = documentTypeAlias.Replace("uwbs", string.Empty);
				documentType.Save();
			}
			umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer Create Document Types End: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));

			// assign the documen types to their master document types if master document type is 0
			// if user already set a different master document type it will not be changed
			try
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer Sort Document Types Start: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
				SortDocumentTypes(DocumentTypeAliasList.List);
				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer Sort Document Types End: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			}
			catch
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Installer: SortDocumentTypes failed");
			}
			// set wich children are allowed under the document type
			try
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer SetAllowedChildren Start: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
				SetAllowedChildren();
				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer SetAllowedChildren End: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			}
			catch
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Installer: SetAllowedChildren failed");
			}
			// create the default uWebshop tree
			try
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer CreateDocuments Start: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
				CreateDocuments(DocumentTypeAliasList.List);
				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer CreateDocuments End: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));


				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer CreateDocuments Publish Start: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
				//if (!_update)
				//{
				//	try
				//	{
				//		if (_uWebshopDocId > 0)
				//		{
				//			var uWebshopDocument = new Document(_uWebshopDocId);


				//			PublishWithChildrenWithResult(uWebshopDocument, author);
				//		}
				//		else
				//		{
				//			umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Installer: uWebshopDocId == 0 No Nodes Published");
				//		}
				//	}
				//	catch
				//	{
				//		umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Installer: PublishWithChildrenWithResult");
				//	}

				//	try
				//	{
				//		if (_uWebshopDocId > 0)
				//		{
				//			var uWebshopDocument = new Document(_uWebshopDocId);

				//			UpdateDocumentCache(uWebshopDocument);
				//		}
				//		else
				//		{
				//			umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Installer: uWebshopDocId == 0 No Nodes Published");
				//		}
				//	}
				//	catch
				//	{
				//		umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Installer: UpdateDocumentCache");
				//	}
				//}
				umbraco.BusinessLogic.Log.Add(LogTypes.Debug, 0, "uWebshop Installer CreateDocuments Publish End: " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt"));
			}
			catch
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Installer: CreateDocuments failed");
			}
		}

		public bool InstallStorePickerOnNodeWithId(int nodeId, out string feedbackSmall, out string feedbackLarge)
		{
			var doc = new Document(nodeId);
			var selectedDoctype = doc.ContentType;

			var storePickerDataTypeDef = new StorePickerDataType();
			var storePickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == storePickerDataTypeDef.Id);

			if (storePickerDataType != null)
			{
				selectedDoctype.AddPropertyType(storePickerDataType, Constants.StorePickerAlias, "#StorePicker");

				feedbackSmall = "StorePicker Installed!";
				feedbackLarge = "StorePicker installed on " +
				                doc.ContentType.Alias +
				                " document type";
				return true;
			}
			else
			{
				feedbackSmall = "StorePicker Install Failed";
				feedbackLarge = "StorePicker install failed on " +
				                doc.ContentType.Alias + " document type";
				return false;
			}
		}

		public bool InstallSandBoxStarterkit(out bool storePresent)
		{
			var author = new User(0);
			var categoryDocType = DocumentType.GetByAlias("uwbsCategory");
			var productDocType = DocumentType.GetByAlias("uwbsProduct");

			var categoryTemplate = Template.GetByAlias("uwbsCategory");
			var productTemplate = Template.GetByAlias("uwbsProduct");

			categoryDocType.allowedTemplates = new[] { categoryTemplate };
			categoryDocType.DefaultTemplate = categoryTemplate.Id;

			categoryDocType.Save();

			productDocType.allowedTemplates = new[] { productTemplate };
			productDocType.DefaultTemplate = productTemplate.Id;

			productDocType.Save();

			int uWebshopDocId = 0;
			int homepageDocId = 0;

			var installedHomePageDt = DocumentType.GetByAlias("uwbsHomepage");

			var storePickerDataTypeDef = new StorePickerDataType();
			var storePickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == storePickerDataTypeDef.Id);

			if (storePickerDataType != null)
			{
				if (installedHomePageDt.getPropertyType(Constants.StorePickerAlias) == null)
				{
					installedHomePageDt.AddPropertyType(storePickerDataType, Constants.StorePickerAlias, "#StorePicker");
				}
			}

			var uWbsDocType = DocumentType.GetByAlias("uWebshop");

			var uWebshopDocument = Document.GetDocumentsOfDocumentType(uWbsDocType.Id).FirstOrDefault(x => !x.IsTrashed);

			if (uWebshopDocument != null)
			{
				uWebshopDocId = uWebshopDocument.Id;
			}

			var storeDocumentType = DocumentType.GetByAlias(Store.NodeAlias);
			storePresent = false;
			if (storeDocumentType != null)
			{
				var storeDocuments = Document.GetDocumentsOfDocumentType(storeDocumentType.Id);
				storePresent = storeDocuments.Any(x => !x.IsTrashed);
			}

			var installedHomePageDocs = Document.GetDocumentsOfDocumentType(installedHomePageDt.Id);

			var installedHomePageDoc = installedHomePageDocs.OrderBy(x => x.CreateDateTime).Last(x => !x.IsTrashed);

			if (installedHomePageDoc != null)
			{
				homepageDocId = installedHomePageDoc.Id;
				installedHomePageDoc.sortOrder = 1;
				installedHomePageDoc.Save();
			}

			var catalogDt = DocumentType.GetByAlias(Catalog.CategoryRepositoryNodeAlias);
			{
				var categoryRepository = Document.GetDocumentsOfDocumentType(catalogDt.Id).FirstOrDefault(x => !x.IsTrashed);

				if (categoryRepository != null)
				{
					var categoryDt = DocumentType.GetByAlias(Category.NodeAlias);
					if (categoryDt != null)
					{
						var testCategory = Document.MakeNew("Your First Category", categoryDt, author, categoryRepository.Id);

						testCategory.SetProperty("title", "Your First Category");
						testCategory.SetProperty("url", "Your First Category");
						testCategory.SetProperty("metaDescription", "Your First Category");
						testCategory.Save();

						var productDt = DocumentType.GetByAlias(Product.NodeAlias);

						if (productDt != null)
						{
							var testProduct = Document.MakeNew("Your First Product", productDt, author, testCategory.Id);

							testProduct.SetProperty("title", "Your First Product");
							testProduct.SetProperty("url", "Your First Product");
							testProduct.SetProperty("sku", "PROD001");
							testProduct.SetProperty("price", "10000");
							UWebshopStock.UpdateStock(testProduct.Id, 5, false, string.Empty);
							testProduct.Save();
							//testProduct.Publish(author);

							var testProduct2 = Document.MakeNew("Your Second Product", productDt, author, testCategory.Id);

							testProduct2.SetProperty("title", "Your Second Product");
							testProduct2.SetProperty("url", "Your Second Product");
							testProduct2.SetProperty("sku", "PROD002");
							testProduct2.SetProperty("price", "5000");
							testProduct2.SetProperty("backorderStatus", "disable");
							UWebshopStock.UpdateStock(testProduct2.Id, 10, false, string.Empty);
							testProduct2.Save();
							//testProduct2.Publish(author);

							//if (umbraco.GlobalSettings.VersionMajor < 6)
							//{
							//	library.UpdateDocumentCache(testProduct.Id);
							//	library.UpdateDocumentCache(testProduct2.Id);
							//}
							var productVariantDt = DocumentType.GetByAlias(ProductVariant.NodeAlias);

							if (productVariantDt != null)
							{
								var testProductVariant = Document.MakeNew("Your First Color Variant", productVariantDt, author, testProduct.Id);

								testProductVariant.SetProperty("title", "Orange");
								testProductVariant.SetProperty("sku", "VARCOL001");
								testProductVariant.SetProperty("group", "colors");
								testProductVariant.SetProperty("price", "1000");
								testProductVariant.SetProperty("backorderStatus", "disable");
								UWebshopStock.UpdateStock(testProductVariant.Id, 5, false, string.Empty);
								testProductVariant.Save();
								//testProductVariant.Publish(author);

								var testProductVariant2 = Document.MakeNew("Your Second Color Variant", productVariantDt, author, testProduct.Id);

								testProductVariant2.SetProperty("title", "Blue");
								testProductVariant2.SetProperty("sku", "VARCOL002");
								testProductVariant2.SetProperty("group", "colors");
								testProductVariant2.SetProperty("price", "2000");
								testProductVariant2.SetProperty("backorderStatus", "disable");
								UWebshopStock.UpdateStock(testProductVariant2.Id, 5, false, string.Empty);
								testProductVariant2.Save();
								//testProductVariant2.Publish(author);

								var testProductVariant3 = Document.MakeNew("Your First Type Variant", productVariantDt, author, testProduct.Id);

								testProductVariant3.SetProperty("title", "Manual");
								testProductVariant3.SetProperty("sku", "VARTYP001");
								testProductVariant3.SetProperty("group", "type");
								testProductVariant3.SetProperty("price", "500");
								testProductVariant3.SetProperty("backorderStatus", "disable");
								UWebshopStock.UpdateStock(testProductVariant3.Id, 5, false, string.Empty);
								testProductVariant3.Save();
								//testProductVariant3.Publish(author);

								var testProductVariant4 = Document.MakeNew("Your Second Type Variant", productVariantDt, author, testProduct.Id);

								testProductVariant4.SetProperty("title", "Automatic");
								testProductVariant4.SetProperty("sku", "VARTYP002");
								testProductVariant4.SetProperty("group", "type");
								testProductVariant4.SetProperty("price", "200");
								testProductVariant4.SetProperty("backorderStatus", "disable");
								UWebshopStock.UpdateStock(testProductVariant4.Id, 5, false, string.Empty);
								testProductVariant4.Save();
								//testProductVariant4.Publish(author);

								//if (umbraco.GlobalSettings.VersionMajor < 6)
								//{
								//	library.UpdateDocumentCache(testProductVariant.Id);
								//	library.UpdateDocumentCache(testProductVariant2.Id);
								//	library.UpdateDocumentCache(testProductVariant3.Id);
								//	library.UpdateDocumentCache(testProductVariant4.Id);
								//}

							}
						}
					}
				}
			}

			var uwebshopDt = DocumentType.GetByAlias("uWebshop");
			if (uwebshopDt != null)
			{
				var uWebshopDocs = Document.GetDocumentsOfDocumentType(uwebshopDt.Id);

				if (uWebshopDocs != null)
				{
					var uWebshopDoc = uWebshopDocs.First(x => !x.IsTrashed);

					uWebshopDoc.sortOrder = 10;
					uWebshopDoc.Save();
				}
			}

			var storeDt = DocumentType.GetByAlias(Store.NodeAlias);
			if (storeDt != null)
			{
				var storeDocs = Document.GetDocumentsOfDocumentType(storeDt.Id);

				if (storeDocs != null && storeDocs.Any(x => !x.IsTrashed) && storeDocs.All(x => x.Published == false))
				{
					var storeDocument = storeDocs.FirstOrDefault();

					if (storeDocument != null)
					{
						storeDocument.SetProperty("orderNumberPrefix", storeDocument.Text);
						storeDocument.SetProperty("globalVat", "0");
						storeDocument.SetProperty("countryCode", "DK");
						storeDocument.SetProperty("storeEmailFrom", string.Format("info@{0}.com", storeDocument.Text));
						storeDocument.SetProperty("storeEmailTo", string.Format("info@{0}.com", storeDocument.Text));
						storeDocument.SetProperty("storeEmailFromName", "uWebshop Demo");

						storeDocument.Save();
						storeDocument.Publish(new User(0));
					}
				}
				else
				{
					Umbraco.Helpers.InstallStore("uWebshop");

					var newStoreDocs = Document.GetDocumentsOfDocumentType(storeDt.Id);

					var storeDocument = newStoreDocs.FirstOrDefault(x => !x.IsTrashed);

					if (storeDocument != null)
					{
						storeDocument.SetProperty("orderNumberPrefix", storeDocument.Text);
						storeDocument.SetProperty("globalVat", "0");
						storeDocument.SetProperty("countryCode", "DK");
						storeDocument.SetProperty("storeEmailFrom", string.Format("starterkitdemo@{0}.com", storeDocument.Text));
						storeDocument.SetProperty("storeEmailTo", string.Format("starterkitdemo@{0}.com", storeDocument.Text));
						storeDocument.SetProperty("storeEmailFromName", "uWebshop Demo");

						storeDocument.Save();
						storeDocument.Publish(new User(0));
					}
					BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.info, "Automatically Created Store", "Please Republish All uWebshop Nodes");
				}
			}

			try
			{
				library.RefreshContent();
			}
			catch
			{

			}
			try
			{
				if (uWebshopDocId > 0)
				{
					//var uWebshopDocument = new Document(uWebshopDocId);
					PublishWithChildrenWithResult(uWebshopDocument, author);
				}
				else
				{
					umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: uWebshopDocId == 0 No Nodes Published");
				}
			}
			catch
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: PublishWithChildrenWithResult");
			}

			try
			{
				if (uWebshopDocId > 0)
				{
					//var uWebshopDocument = new Document(uWebshopDocId);
					UpdateDocumentCache(uWebshopDocument);
				}
				else
				{
					umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: uWebshopDocId == 0 No Nodes Published");
				}
			}
			catch
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop  SandboxInstaller: UpdateDocumentCache");
			}

			try
			{
				if (homepageDocId > 0)
				{
					PublishWithChildrenWithResult(new Document(homepageDocId), author);
				}
				else
				{
					umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: homePageDoc == 0 No Nodes Published");
				}
			}
			catch
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer homePageDoc: PublishWithChildrenWithResult");
			}

			try
			{
				if (homepageDocId > 0)
				{
					UpdateDocumentCache(new Document(homepageDocId));
				}
				else
				{
					umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop Sandbox Installer: homePageDoc == 0 No Nodes Published");
				}
			}
			catch
			{
				umbraco.BusinessLogic.Log.Add(LogTypes.Error, 0, "uWebshop  SandboxInstaller homePageDoc: UpdateDocumentCache");
			}

			BasePage.Current.ClientTools.RefreshTree("content");

			try
			{
				ExamineManager.Instance.IndexProviderCollection["ExternalIndexer"].RebuildIndex();
			}
			catch
			{
				BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error,
															  "ExternalIndexer Error",
															  "Please Republish All uWebshop Nodes");
				uWebshop.Domain.Log.Instance.LogError("uWebshop Installer: Could Not Rebuild ExternalIndexer; Publish uWebshop node + children manually");
				return false;
			}
			return true;
		}

		

		/// <summary>
		///  assign uWebshop document types to the proper parent, if they don't have a parent yet
		/// </summary>
		/// <param name="documentTypeAliasList">documenttypeAliasList as string</param>
		private static void SortDocumentTypes(IEnumerable<string> documentTypeAliasList)
		{
			var author = new User(0);

			#region ZoneSelectorDataType
			var zoneSelectorDataTypeDef = new ZoneSelectorDataType();

			var zoneSelectorDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == zoneSelectorDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, zoneSelectorDataTypeDef.DataTypeName, zoneSelectorDataTypeDef.Id);

			zoneSelectorDataType.DataType = zoneSelectorDataTypeDef;
			zoneSelectorDataType.Save();
			#endregion

			#region StoreTemplatePickerDataType
			var storeTemplatePickerDataTypeDef = new StoreTemplatePickerDataType();

			var storeTemplatePickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == storeTemplatePickerDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, storeTemplatePickerDataTypeDef.DataTypeName, storeTemplatePickerDataTypeDef.Id);

			storeTemplatePickerDataType.DataType = storeTemplatePickerDataTypeDef;
			storeTemplatePickerDataType.Save();
			#endregion
			
			#region StorePickerDataType
			var storePickerDataTypeDef = new StorePickerDataType();
			var storePickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == storePickerDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, storePickerDataTypeDef.DataTypeName, storePickerDataTypeDef.Id);

			storePickerDataType.DataType = storePickerDataTypeDef;
			storePickerDataType.Save();
			#endregion

			#region StockUpdateDataType
			var stockUpdaterDataTypeDef = new StockUpdateDataType();
			var stockUpdateDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == stockUpdaterDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, stockUpdaterDataTypeDef.DataTypeName, stockUpdaterDataTypeDef.Id);

			stockUpdateDataType.DataType = stockUpdaterDataTypeDef;
			stockUpdateDataType.Save();

		
			#endregion

			#region ShippingRangeTypeDataType
			var shippingRangeTypeDataTypeDef = new ShippingRangeTypeDataType();

			var shippingRangeTypeDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == shippingRangeTypeDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, shippingRangeTypeDataTypeDef.DataTypeName, shippingRangeTypeDataTypeDef.Id);

			shippingRangeTypeDataType.DataType = shippingRangeTypeDataTypeDef;
			shippingRangeTypeDataType.Save();

			#endregion
			
			#region ShippingProviderTypeDataType
			var shippingProviderDataTypeDef = new ShippingProviderTypeDataType();
			var shippingProviderTypeDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == shippingProviderDataTypeDef.Id) ??
			DataTypeDefinition.MakeNew(author, shippingProviderDataTypeDef.DataTypeName, shippingProviderDataTypeDef.Id);

			shippingProviderTypeDataType.DataType = shippingProviderDataTypeDef;
			shippingProviderTypeDataType.Save();
			#endregion
			
			#region RazorWrapperDataType
			var razorWrapperTemplateDef = new RazorWrapperDataType();
			var razorPickerTemplateDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == razorWrapperTemplateDef.Id) ??
			DataTypeDefinition.MakeNew(author, razorWrapperTemplateDef.DataTypeName, razorWrapperTemplateDef.Id);

			razorPickerTemplateDataType.DataType = razorWrapperTemplateDef;
			razorPickerTemplateDataType.Save();
			#endregion
			
			#region RangesDataType
			var rangeSelectorDataTypeDef = new RangesDataType();

			var rangeSelectorDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == rangeSelectorDataTypeDef.Id) ??
			DataTypeDefinition.MakeNew(author, rangeSelectorDataTypeDef.DataTypeName, rangeSelectorDataTypeDef.Id);

			rangeSelectorDataType.DataType = rangeSelectorDataTypeDef;
			rangeSelectorDataType.Save();
			#endregion
			
			#region PriceDataType
			var priceDataTypeDef = new PriceDataType();

			var priceDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == priceDataTypeDef.Id) ??
			DataTypeDefinition.MakeNew(author, priceDataTypeDef.DataTypeName, priceDataTypeDef.Id);

			priceDataType.DataType = priceDataTypeDef;
			priceDataType.Save();

			#endregion

			#region PaymentProviderTypeDataType
			var paymentProviderDataTypeDef = new PaymentProviderTypeDataType();
			
			var paymentProviderTypeDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == paymentProviderDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, paymentProviderDataTypeDef.DataTypeName, paymentProviderDataTypeDef.Id);

			paymentProviderTypeDataType.DataType = paymentProviderDataTypeDef;
			paymentProviderTypeDataType.Save();
			#endregion
			
			#region PaymentProviderAmountTypeDataType
			var paymentProviderAmountDataTypeDef = new PaymentProviderAmountTypeDataType();

			var paymentProviderAmountDataType =
			DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == paymentProviderAmountDataTypeDef.Id) ??
			DataTypeDefinition.MakeNew(author, paymentProviderAmountDataTypeDef.DataTypeName, paymentProviderAmountDataTypeDef.Id);

			paymentProviderAmountDataType.DataType = paymentProviderAmountDataTypeDef;
			paymentProviderAmountDataType.Save();

			#endregion
			
			#region OrderStatusSectionDataType
			var orderSectionPickerDataTypeDef = new OrderStatusSectionDataType();
			var orderStatusSectionDataType =
			DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == orderSectionPickerDataTypeDef.Id) ??
			DataTypeDefinition.MakeNew(author, orderSectionPickerDataTypeDef.DataTypeName, orderSectionPickerDataTypeDef.Id);

			orderStatusSectionDataType.DataType = orderSectionPickerDataTypeDef;
			orderStatusSectionDataType.Save();

			#endregion

			#region OrderStatusPickerDataType
			var orderStatusPickerDataTypeDef = new OrderStatusPickerDataType();


			var orderStatusPickerDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == orderStatusPickerDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, orderStatusPickerDataTypeDef.DataTypeName, orderStatusPickerDataTypeDef.Id);

			orderStatusPickerDataType.DataType = orderStatusPickerDataTypeDef;
			orderStatusPickerDataType.Save();
			#endregion

			#region OrderInfoViewerDataType
			var orderInfoViewerDataTypeDef = new OrderInfoViewerDataType();

			var overInfoViewerDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == orderInfoViewerDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, orderInfoViewerDataTypeDef.DataTypeName, orderInfoViewerDataTypeDef.Id);

			overInfoViewerDataType.DataType = orderInfoViewerDataTypeDef;
			overInfoViewerDataType.Save();

			#endregion

			#region OrderCountDataType
			var orderCountDataTypeDef = new OrderCountDataType();
			var orderCountDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == orderCountDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, orderCountDataTypeDef.DataTypeName, orderCountDataTypeDef.Id);

			orderCountDataType.DataType = orderCountDataTypeDef;
			orderCountDataType.Save();

			#endregion
			
			#region MemberGroupPickerDataType
			var membergroupPickerDataTypeDef = new MemberGroupPickerDataType();
			
			var membergroupPickerDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == membergroupPickerDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, membergroupPickerDataTypeDef.DataTypeName, membergroupPickerDataTypeDef.Id);

			membergroupPickerDataType.DataType = membergroupPickerDataTypeDef;
			membergroupPickerDataType.Save();
			#endregion

			#region LanguagePickerDataType
			var languagePickerDataTypeDef = new LanguagePickerDataType();

			var languagePickerDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == languagePickerDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, languagePickerDataTypeDef.DataTypeName, languagePickerDataTypeDef.Id);

			languagePickerDataType.DataType = languagePickerDataTypeDef;
			languagePickerDataType.Save();
			#endregion

			#region EnableDisableDataType
			var enableDisableDataTypeDef = new EnableDisableDataType();

			var enableDisableDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == enableDisableDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, enableDisableDataTypeDef.DataTypeName, enableDisableDataTypeDef.Id);

			enableDisableDataType.DataType = enableDisableDataTypeDef;
			enableDisableDataType.Save();
			#endregion

			#region EmailTemplateSelectorDataType
			var xsltTemplateSelectorDataTypeDef = new EmailTemplateSelectorDataType();
			var xsltTemplatePickerDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == xsltTemplateSelectorDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, xsltTemplateSelectorDataTypeDef.DataTypeName, xsltTemplateSelectorDataTypeDef.Id);

			xsltTemplatePickerDataType.DataType = xsltTemplateSelectorDataTypeDef;
			xsltTemplatePickerDataType.Save();
			#endregion
			
			#region EmailPreviewDataType

			var emailPreviewDataTypeDef = new EmailPreviewDataType();

			var emailPreviewDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == emailPreviewDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, emailPreviewDataTypeDef.DataTypeName, emailPreviewDataTypeDef.Id);

			emailPreviewDataType.DataType = emailPreviewDataTypeDef;
			emailPreviewDataType.Save();

			#endregion

			#region DiscountTypeDataType
			var discountTypeDataTypeDef = new DiscountTypeDataType();
			
			var discountTypeDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == discountTypeDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, discountTypeDataTypeDef.DataTypeName, discountTypeDataTypeDef.Id);

			discountTypeDataType.DataType = discountTypeDataTypeDef;
			discountTypeDataType.Save();

			#endregion

			#region DiscountOrderConditionDataType
			var discountOrderConditionDataTypeDef = new DiscountOrderConditionDataType();
			var discountOrderConditionDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == discountOrderConditionDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, discountOrderConditionDataTypeDef.DataTypeName, discountOrderConditionDataTypeDef.Id);

			discountOrderConditionDataType.DataType = discountOrderConditionDataTypeDef;
			discountOrderConditionDataType.Save();
			#endregion

			#region CouponCodeDataType
			var CouponCodeDataTypeDef = new CouponCodeDataType();
			var CouponCodeDataType =
				DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == CouponCodeDataTypeDef.Id) ??
				DataTypeDefinition.MakeNew(author, CouponCodeDataTypeDef.DataTypeName, CouponCodeDataTypeDef.Id);
			CouponCodeDataType.DataType = CouponCodeDataTypeDef;
			CouponCodeDataType.Save();
			#endregion

			#region CountrySelectorDataType
			var countrySelectorDataTypeDef = new CountrySelectorDataType();

			var countrySelectorDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == countrySelectorDataTypeDef.Id) ??
			DataTypeDefinition.MakeNew(author, countrySelectorDataTypeDef.DataTypeName, countrySelectorDataTypeDef.Id);
			countrySelectorDataType.DataType = countrySelectorDataTypeDef;
			countrySelectorDataType.Save();
			#endregion

			#region VATPickerDataType
			var vatPercentagePicker = DataTypeDefinition.GetAll().FirstOrDefault(x => x.Text == "VAT Picker" || x.Text == "uWebshop Vat Picker");

			if (vatPercentagePicker == null)
			{
				vatPercentagePicker = DataTypeDefinition.MakeNew(author, "uWebshop Vat Picker", new Guid("69d3f953-b565-4269-9d68-4b39e13c70e5"));
				vatPercentagePicker.DataType = new umbraco.editorControls.dropdownlist.DropdownListDataType();

				int so = -1;

				var SqlHelper = Application.SqlHelper;
				try
				{
					so = SqlHelper.ExecuteScalar<int>("select max(sortorder) from cmsDataTypePreValues where datatypenodeid = @dtdefid",
													  SqlHelper.CreateParameter("@dtdefid", vatPercentagePicker.Id));
					so++;
				}
				catch { }

				var prevalues = new[] {"0", "6", "15", "19", "21"};

				foreach (var value in prevalues)
				{
					IParameter[] SqlParams = new IParameter[]
						{
							SqlHelper.CreateParameter("@value", value),
							SqlHelper.CreateParameter("@dtdefid", vatPercentagePicker.Id),
							SqlHelper.CreateParameter("@so", so++)
						};
					SqlHelper.ExecuteNonQuery("insert into cmsDataTypePreValues (datatypenodeid,[value],sortorder,alias) values (@dtdefid,@value,@so,'')", SqlParams);
				}
			}
			#endregion 

			#region Umbraco Default DataTypes

			var nodePickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.Text == "Content Picker");

			var mediaPickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.Text == "Media Picker");
			//DataTypeDefinition.GetByDataTypeId(Guid.Parse("ead69342-f06d-4253-83ac-28000225583b"));
			var labelDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.Text == "Label");
			//DataTypeDefinition.GetByDataTypeId(Guid.Parse("6c738306-4c17-4d88-b9bd-6546f3771597"));
			var numericDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.Text == "Numeric");
			//DataTypeDefinition.GetByDataTypeId(Guid.Parse("1413afcb-d19a-4173-8e9a-68288d2a73b8"));
			var richTextDataType = DataTypeDefinition.GetAll().SingleOrDefault(x => x.Text == "Richtext editor");

			//DataTypeDefinition.GetByDataTypeId(Guid.Parse("5e9b75ae-face-41c8-b47e-5f4b0fd82f83"));
			var tagsDataType = DataTypeDefinition.GetAll().SingleOrDefault(x => x.Text == "Tags");

			var textboxMultipleDataType = DataTypeDefinition.GetAll().SingleOrDefault(x => x.Text == "Textbox multiple");

			var textstringDataType =
				DataTypeDefinition.GetAll().SingleOrDefault(x => x.Text == "Textstring");
			var trueFalseDataType =
				DataTypeDefinition.GetAll().SingleOrDefault(x => x.Text == "True/false");

			#endregion


			if (documentTypeAliasList == null) throw new ArgumentNullException("documentTypeAliasList");
			foreach (var documentTypeAlias in documentTypeAliasList)
			{
				var dt = DocumentType.GetByAlias(documentTypeAlias);

				var globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Global");
				var detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Details");
				var priceTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Price");
				var emailTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Email Settings");
				var customerTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Customer");
				var customTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Custom");
				var discountTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Discount");
				var conditionTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Conditions");
				var filterTab = dt.getVirtualTabs.FirstOrDefault(x => x.Caption == "Filter");

				switch (dt.Alias)
				{
					#region uWebshop
					case "uWebshop":
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#uWebshopSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "uwebshop16x16.png";
						}
						dt.Save();
						break;
					#endregion
					#region StoreRepository
					case Store.StoreRepositoryNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#StoreSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "store-network.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						dt.Save();
						break;
					#endregion
					#region Catalog
					case Catalog.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#CatalogSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "clipboard-list.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						dt.Save();
						break;
					#endregion
					#region DiscountRepository
					case "uwbsDiscountRepository":
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#DiscountSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "megaphone.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						dt.Save();
						break;
					#endregion
					#region OrderRepository
					case Order.OrderRepositoryNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#OrdersSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "drawer.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						dt.Save();
						break;
					#endregion
					#region PaymentProviderRepository
					case PaymentProvider.PaymentProviderRepositoryNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#PaymentProviderSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "wallet.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						dt.Save();

						break;
					#endregion
					#region ShippingProviderRepository
					case ShippingProvider.ShippingProviderRepositoryNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ShippingProviderSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "truck-box-label.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						dt.Save();

						break;
					#endregion
					#region EmailRepository
					case Email.EmailRepositoryNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#EmailSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "mails-stack.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						dt.Save();

						break;
					#endregion
					#region Settings
					case Settings.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#SettingsSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "toolbox.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias("uWebshop").Id;

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (dt.getPropertyType("includingVat") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "includingVat", "#IncludingVat");
							pt.Description = "#IncludingVatSettingDescription";
							pt.Mandatory = false;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("lowercaseUrls") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "lowercaseUrls", "#LowercaseUrls");
							pt.Description = "#LowercaseUrlsSettingDescription";
							pt.Mandatory = false;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("incompleteOrderLifetime") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "incompleteOrderLifetime",
														"#IncompleteOrderLifetime");
							pt.Description = "#OrderLifetimeSettingDescription";
							pt.Mandatory = false;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}


						dt.Save();

						break;
					#endregion
					case Store.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#StoreNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "store.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Store.StoreRepositoryNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (emailTab == null)
						{
							var createTab = dt.AddVirtualTab("Email Settings");
							emailTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("storeCulture") == null)
						{
							var storeAliasPt = dt.AddPropertyType(languagePickerDataType, "storeCulture", "#StoreCulture");
							storeAliasPt.Description = "#StoreCultureDescription";
							storeAliasPt.Mandatory = true;
							if (globalTab != null) storeAliasPt.TabId = globalTab.Id;
						}

						if (dt.getPropertyType("countryCode") == null)
						{
							var storeAliasPt = dt.AddPropertyType(textstringDataType, "countryCode",
																  "#CountryCode");
							storeAliasPt.Description = "#CountryCodeDescription";
							storeAliasPt.Mandatory = true;
							if (globalTab != null) storeAliasPt.TabId = globalTab.Id;
						}


						if (dt.getPropertyType("currencyCulture") == null)
						{
							var storeAliasPt = dt.AddPropertyType(languagePickerDataType, "currencyCulture",
																  "#CurrencyCulture");
							storeAliasPt.Description = "#CurrencyCultureDescription";
							storeAliasPt.Mandatory = true;
							if (globalTab != null) storeAliasPt.TabId = globalTab.Id;
							storeAliasPt.Save();
						}

						if (dt.getPropertyType("globalVat") == null)
						{
							var pt = dt.AddPropertyType(vatPercentagePicker, "globalVat", "#GlobalVat");
							pt.Description = "#GlobalVatDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderNumberPrefix") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "orderNumberPrefix", "#OrderNumberPrefix");
							pt.Description = "#OrderNumberPrefixDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderNumberTemplate") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "orderNumberTemplate", "#OrderNumberTemplate");
							pt.Description = "#OrderNumberTemplateDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderNumberStartNumber") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "orderNumberStartNumber", "#OrderNumberStartNumber");
							pt.Description = "#OrderNumberStartNumberDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("enableStock") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "enableStock", "#EnableStock");
							pt.Description = "#EnableStockDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("storeStock") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "storeStock", "#StoreStock");
							pt.Description = "#StoreNodeStoreStockSettingDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("useBackorders") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "useBackorders", "#UseBackorders");
							pt.Description = "#UseBackordersDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("enableTestmode") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "enableTestmode", "#EnableTestmode");
							pt.Description = "#EnableTestmodeDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("storeEmailFrom") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "storeEmailFrom", "#EmailFrom");
							pt.Description = "#StoreEmailFromDescription";
							pt.Mandatory = true;
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("storeEmailFromName") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "storeEmailFromName", "#StoreEmailFromName");
							pt.Description = "#StoreEmailFromNameDescription";
							pt.Mandatory = true;
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("storeEmailTo") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "storeEmailTo", "#StoreEmailTo");
							pt.Description = "#StoreEmailToDescription";
							pt.Mandatory = true;
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("accountEmailCreated") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "accountEmailCreated",
														"#AccountEmailCreated");
							pt.Description =
								"#AccountEmailCreatedDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("accountForgotPassword") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "accountForgotPassword",
														"#AccountForgotPassword");
							pt.Description =
								"#AccountForgotPasswordDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("confirmationEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "confirmationEmailStore",
														"#ConfirmationEmailStore");
							pt.Description =
								"#ConfirmationEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("confirmationEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "confirmationEmailCustomer",
														"#ConfirmationEmailCustomer");
							pt.Description =
								"#ConfirmationEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("onlinePaymentEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "onlinePaymentEmailStore",
														"#OnlinePaymentEmailStore");
							pt.Description =
								"#OnlinePaymentEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("onlinePaymentEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "onlinePaymentEmailCustomer",
														"#OnlinePaymentEmailCustomer");
							pt.Description =
								"#OnlinePaymentEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}


						if (dt.getPropertyType("offlinePaymentEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "offlinePaymentEmailStore",
														"#OfflinePaymentEmailStore");
							pt.Description =
								"#OfflinePaymentEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("offlinePaymentEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "offlinePaymentEmailCustomer",
														"#OfflinePaymentEmailCustomer");
							pt.Description =
								"#OfflinePaymentEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("paymentFailedEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "paymentFailedEmailStore",
														"#PaymentFailedEmailStore");
							pt.Description =
								"#PaymentFailedEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("paymentFailedEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "paymentFailedEmailCustomer",
														"#PaymentFailedEmailCustomer");
							pt.Description =
								"#PaymentFailedEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("dispatchedEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "dispatchedEmailStore",
														"#DispatchedEmailStore");
							pt.Description =
								"#DispatchedEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("dispatchedEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "dispatchedEmailCustomer",
														"#DispatchedEmailCustomer");
							pt.Description =
								"#DispatchedEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("cancelEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "cancelEmailStore", "#CancelEmailStore");
							pt.Description =
								"#CancelEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("cancelEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "cancelEmailCustomer",
														"#CancelEmailCustomer");
							pt.Description =
								"#CancelEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("closedEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "closedEmailStore", "#ClosedEmailStore");
							pt.Description =
								"#ClosedEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("closedEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "closedEmailCustomer",
														"#ClosedEmailCustomer");
							pt.Description =
								"#ClosedEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}


						if (dt.getPropertyType("pendingEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "pendingEmailStore", "#PendingEmailStore");
							pt.Description =
								"PendingEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("pendingEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "pendingEmailCustomer",
														"#PendingEmailCustomer");
							pt.Description =
								"#PendingEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("temporaryOutOfStockEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "temporaryOutOfStockEmailStore",
														"#TemporaryOutOfStockEmailStore");
							pt.Description =
								"#TemporaryOutOfStockEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("temporaryOutOfStockEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "temporaryOutOfStockEmailCustomer",
														"#TemporaryOutOfStockEmailCustomer");
							pt.Description =
								"#TemporaryOutOfStockEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("undeliverableEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "undeliverableEmailStore",
														"#UndeliverableEmailStore");
							pt.Description =
								"#UndeliverableEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("undeliverableEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "undeliverableEmailCustomer",
														"#UndeliverableEmailCustomer");
							pt.Description =
								"#UndeliverableEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("returnEmailStore") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "returnEmailStore", "#ReturnEmailStore");
							pt.Description =
								"#ReturnEmailStoreDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("returnEmailCustomer") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "returnEmailCustomer",
														"#ReturnEmailCustomer");
							pt.Description =
								"#ReturnEmailCustomerDescription";
							if (emailTab != null) pt.TabId = emailTab.Id;
							pt.Save();
						}
						
						dt.Save();
						break;
					case Catalog.CategoryRepositoryNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#CatagoryRespositorySectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "folder-search-result.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(Catalog.NodeAlias).Id;

						dt.Save();
						break;
					case Catalog.ProductRepositoryNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ProductRespositorySectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "box-search-result.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(Catalog.NodeAlias).Id;

						dt.Save();

						break;
					case Category.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#CategoryNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "folder.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Catalog.CategoryRepositoryNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (detailsTab == null)
						{
							var createTab = dt.AddVirtualTab("Details");
							detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("url") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "url", "#Url");
							pt.Description = "#UrlDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("metaTags") == null)
						{
							var pt = dt.AddPropertyType(tagsDataType, "metaTags", "#Tags");
							pt.Description = "#TagsDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("metaDescription") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "metaDescription",
														"#MetaDescription");
							pt.Description = "#MetaDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("disable") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "disable", "#Disable");
							pt.Description = "#DisableDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("categories") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "categories", "#SubCategories");
							pt.Description = "#SubcategoriesDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(richTextDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("images") == null)
						{
							var pt = dt.AddPropertyType(mediaPickerDataType, "images", "#Image");
							pt.Description = "#ImagesDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						dt.Save();
						break;

					case Product.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ProductNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "box-label.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Catalog.ProductRepositoryNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (detailsTab == null)
						{
							var createTab = dt.AddVirtualTab("Details");
							detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (priceTab == null)
						{
							var createPriceTab = dt.AddVirtualTab("Price");
							priceTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createPriceTab);
							dt.Save();
						}

						if (dt.getPropertyType(Product.TitleAlias) == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, Product.TitleAlias, "#Title");
							pt.Description = "#TitleDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("url") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "url", "#Url");
							pt.Description = "#UrlDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("sku") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "sku", "#SKU");
							pt.Description = "#SKUDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("metaTags") == null)
						{
							var pt = dt.AddPropertyType(tagsDataType, "metaTags", "#Tags");
							pt.Description = "#TagsDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("metaDescription") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "metaDescription",
														"#MetaDescription");
							pt.Description = "#MetaDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("disable") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "disable", "#Disable");
							pt.Description = "#DisableDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("categories") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "categories", "#Categories");
							pt.Description = "#CategoriesDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(richTextDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("images") == null)
						{
							var pt = dt.AddPropertyType(mediaPickerDataType, "images", "#Image");
							pt.Description = "#ImagesDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}


						if (dt.getPropertyType("length") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "length", "#Length");
							pt.Description = "#LengthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("width") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "width", "#Width");
							pt.Description = "#WidthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("height") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "height", "#Height");
							pt.Description = "#HeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}
						if (dt.getPropertyType("weight") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "weight", "#Weight");
							pt.Description = "#WeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("price") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "price", "#Price");
							pt.Description = "#PriceDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("ranges") == null)
						{
							var pt = dt.AddPropertyType(rangeSelectorDataType, "ranges", "#PriceRanges");
							pt.Description = "#RangesDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("vat") == null)
						{
							var pt = dt.AddPropertyType(vatPercentagePicker, "vat", "#Vat");
							pt.Description = "#VatDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("stockStatus") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "stockStatus", "#StockStatus");
							pt.Description = "#StockStatusDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType(Product.StockAlias) == null)
						{
							var pt = dt.AddPropertyType(stockUpdateDataType, Product.StockAlias, "#StockUpdate");
							pt.Description = "#StockDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("ordered") == null)
						{
							var pt = dt.AddPropertyType(orderCountDataType, "ordered", "#OrderCount");
							pt.Description = "#OrderedDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("useVariantStock") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "useVariantStock", "#UseVariantStock");
							pt.Description = "#UseVariantStockDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("backorderStatus") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "backorderStatus", "#BackorderStatus");
							pt.Description = "#BackorderDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case OrderedProduct.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#OrderedProductNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "box-label.png";
						}

						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias).Id;


						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (detailsTab == null)
						{
							var createTab = dt.AddVirtualTab("Details");
							detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (customTab == null)
						{
							var createTab = dt.AddVirtualTab("Custom");
							customTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (discountTab == null)
						{
							var createTab = dt.AddVirtualTab("Discount");
							discountTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("productId") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "productId", "#ProductId");
							pt.Description = "#ProductIdDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("sku") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "sku", "#Sku");
							pt.Description = "#SKUDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("price") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "price", "#Price");
							pt.Description = "#PriceDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("ranges") == null)
						{
							var pt = dt.AddPropertyType(rangeSelectorDataType, "ranges", "#Ranges");
							pt.Description = "#RangesDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("vat") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "vat", "#Vat");
							pt.Description = "#VatDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("itemCount") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "itemCount", "#ItemCount");
							pt.Description = "#ItemCountDescription ";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("length") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "length", "#Length");
							pt.Description = "#LengthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("width") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "width", "#Width");
							pt.Description = "#WidthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("height") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "height", "#Height");
							pt.Description = "#HeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("weight") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "weight", "#Weight");
							pt.Description = "#WeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("text") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "text", "#Text");
							pt.Description = "#TextDescription";
							if (customTab != null) pt.TabId = customTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("image") == null)
						{
							var pt = dt.AddPropertyType(mediaPickerDataType, "image", "#image");
							pt.Description = "#ImageDescription";
							if (customTab != null) pt.TabId = customTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderedProductDiscountPercentage") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "orderedProductDiscountPercentage", "#OrderedProductDiscountPercentage");
							pt.Description = "#OrderDiscountPercentageDescription";
							if (discountTab != null) pt.TabId = discountTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderedProductDiscountAmount") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "orderedProductDiscountAmount", "#OrderedProductDiscountAmount");
							pt.Description = "#OrderDiscountAmountDescription";
							if (discountTab != null) pt.TabId = discountTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case ProductVariant.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ProductVariantNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "magnet-small.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Catalog.ProductRepositoryNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (detailsTab == null)
						{
							var createTab = dt.AddVirtualTab("Details");
							detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (priceTab == null)
						{
							var createPriceTab = dt.AddVirtualTab("Price");
							priceTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createPriceTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("sku") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "sku", "#SKU");
							pt.Description = "SKUDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("group") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "group", "#Group");
							pt.Description = "#GroupDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(richTextDataType, "description", "#Description");
							pt.Description = "DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("disable") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "disable", "#Disable");
							pt.Description = "DisableDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("length") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "length", "#Length");
							pt.Description = "#LengthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("width") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "width", "#Width");
							pt.Description = "#WidthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("height") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "height", "#Height");
							pt.Description = "#HeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}
						if (dt.getPropertyType("weight") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "weight", "#Weight");
							pt.Description = "#WeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("price") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "price", "#Price");
							pt.Description = "#PriceDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("requiredVariant") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "requiredVariant", "#RequiredVariant");
							pt.Description = "#RequiredVariantDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("stockStatus") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "stockStatus", "#StockStatus");
							pt.Description = "#StockStatusDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("stock") == null)
						{
							var pt = dt.AddPropertyType(stockUpdateDataType, "stock", "#StockUpdate");
							pt.Description = "#StockDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("ordered") == null)
						{
							var pt = dt.AddPropertyType(orderCountDataType, "ordered", "#OrderCount");
							pt.Description = "#OrderedDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("backorderStatus") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "backorderStatus", "#BackorderStatus");
							pt.Description = "#BackorderDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case OrderedProductVariant.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#OrderedProductVariantNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "magnet-small.png";
						}

						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias).Id;

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (detailsTab == null)
						{
							var createTab = dt.AddVirtualTab("Details");
							detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("variantId") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "variantId", "#VariantId");
							pt.Description = "#VariatnIdDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("sku") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "sku", "#Sku");
							pt.Description = "#SKUDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("group") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "group", "#Group");
							pt.Description = "#GroupDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("price") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "price", "#Price");
							pt.Description = "#PriceDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("length") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "length", "#Length");
							pt.Description = "#LengthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("width") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "width", "#Width");
							pt.Description = "#WidthDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("height") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "height", "#Height");
							pt.Description = "#HeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("weight") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "weight", "#Weight");
							pt.Description = "#WeightDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("discountPercentage") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "discountPercentage", "#DiscountPercentage");
							pt.Description = "#DiscountPercentageDescription";
							if (discountTab != null) pt.TabId = discountTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("discountAmount") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "discountAmount", "#DiscountAmount");
							pt.Description = "#DiscountAmountDescription";
							if (discountTab != null) pt.TabId = discountTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case OrderSection.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#OrderSectionNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "folder-open-table.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (dt.getPropertyType("orderSection") == null)
						{
							var pt = dt.AddPropertyType(orderStatusSectionDataType, "orderSection", "#OrderSection");
							pt.Description = "#OrderSectionNodeSectionPickerDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case DateFolder.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#OrderSectionDateFolderNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "calendar-month.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias).Id;
						}

						dt.Save();
						break;
					case OrderStoreFolder.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#OrderSectionStoreFolderNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "store.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias).Id;
						}
						dt.Save();
						break;
					case "uwbsDiscountProductSection":
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#DiscountProductsNodeSection";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "inbox-document.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias("uwbsDiscountRepository").Id;
						}

						dt.Save();
						break;
					case "uwbsDiscountOrderSection":
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#DiscountOrdersNodeSection";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "inbox-document.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias("uwbsDiscountRepository").Id;
						}
						dt.Save();
						break;
					case DiscountProduct.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#DiscountProductNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "price-tag--minus.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias("uwbsDiscountProductSection").Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (conditionTab == null)
						{
							var createTab = dt.AddVirtualTab("Conditions");
							conditionTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("products") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "products", "#Products");
							pt.Description = "#ProductsDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("discount") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "discount", "#DiscountValue");
							pt.Description = "#DiscountValueDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("discountType") == null)
						{
							var pt = dt.AddPropertyType(discountTypeDataType, "discountType", "#DiscountType");
							pt.Description = "#DiscountTypeDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("countdownEnabled") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "countdownEnabled", "#CountdownEnabled");
							pt.Description = "#CountDownEnabledDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("countdown") == null)
						{
							var pt = dt.AddPropertyType(stockUpdateDataType, "countdown", "#Countdown");
							pt.Description = "#CountDownDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("memberGroups") == null)
						{
							var pt = dt.AddPropertyType(membergroupPickerDataType, "memberGroups", "#MemberGroup");
							pt.Description = "#MemberGroupDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						dt.IconUrl = "price-tag--minus.png";
						dt.Save();
						break;
					case DiscountOrder.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#DiscountOrderNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "scissors.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias("uwbsDiscountOrderSection").Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (conditionTab == null)
						{
							var createTab = dt.AddVirtualTab("Conditions");
							conditionTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (filterTab == null)
						{
							var createTab = dt.AddVirtualTab("Filter");
							filterTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("discount") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "discount", "#DiscountValue");
							pt.Description = "#DiscountValueDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("ranges") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "ranges", "#Ranges");
							pt.Description = "#DiscountRangesDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("discountType") == null)
						{
							var pt = dt.AddPropertyType(discountTypeDataType, "discountType", "#DiscountType");
							pt.Description = "DiscountTypeDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("ranges") == null)
						{
							var pt = dt.AddPropertyType(rangeSelectorDataType, "ranges", "#DiscountRanges");
							pt.Description = "#DiscountOrderRangesDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("countdownEnabled") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "countdownEnabled", "#CountdownEnabled");
							pt.Description = "#CountDownEnabledDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("countdown") == null)
						{
							var pt = dt.AddPropertyType(stockUpdateDataType, "countdown", "#Countdown");
							pt.Description = "#CountDownDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("items") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "items", "#Items");
							pt.Description = "#ItemsDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderCondition") == null)
						{
							var pt = dt.AddPropertyType(discountOrderConditionDataType, "orderCondition", "#OrderCondition");
							pt.Description = "#DiscountConditionDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("numberOfItemsCondition") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "numberOfItemsCondition", "#NumberOfItemsCondition");
							pt.Description = "#NumberOfItemsDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("couponCode") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "couponCode", "#CouponCode");
							pt.Description = "#CouponCodeDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("couponCode") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "couponCode", "#CouponCode");
							pt.Description = "#CouponCodeDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("couponCodes") == null)
						{
							var pt = dt.AddPropertyType(CouponCodeDataType, "couponCodes", "#CouponCodes");
							pt.Description = "#CouponCodesDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("minimumAmount") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "minimumAmount", "#MinimumAmount");
							pt.Description = "#MinimumAmountDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("oncePerCustomer") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "oncePerCustomer", "#OncePerCustomer");
							pt.Description = "#OncePerCustomerDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("memberGroups") == null)
						{
							var pt = dt.AddPropertyType(membergroupPickerDataType, "memberGroups", "#MemberGroup");
							pt.Description = "#MemberGroupDescription";
							if (conditionTab != null) pt.TabId = conditionTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("affectedOrderlines") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "affectedOrderlines", "#AffectedOrderlines");
							pt.Description = "#AffectedOrderlinesDescription";
							if (filterTab != null) pt.TabId = filterTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case Order.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#OrderNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "clipboard-invoice.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createGlobalTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createGlobalTab);
							dt.Save();
						}

						if (customerTab == null)
						{
							var createTab = dt.AddVirtualTab("Customer");
							customerTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("orderStatusPicker") == null)
						{
							var pt = dt.AddPropertyType(orderStatusPickerDataType, "orderStatusPicker", "#OrderStatusPicker");
							pt.Description = "#OrderStatusPickerDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderPaid") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "orderPaid", "#OrderPaid");
							pt.Description = "#OrderPaidDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderDetails") == null)
						{
							var pt = dt.AddPropertyType(overInfoViewerDataType, "orderDetails", "#OrderDetails");
							pt.Description = "#DetailsDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("orderGuid") == null)
						{
							var pt = dt.AddPropertyType(labelDataType, "orderGuid", "#OrderGuid");
							pt.Description = "#GuidDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("customerEmail") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "customerEmail", "#Email");
							pt.Description = "#OrderNodeCustomerEmailDescription";
							if (customerTab != null) pt.TabId = customerTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("customerFirstName") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "customerFirstName", "#FirstName");
							pt.Description = "#CustomerFirstNameDescription";
							if (customerTab != null) pt.TabId = customerTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("customerLastName") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "customerLastName", "#LastName");
							pt.Description = "#CustomerLastNameDescription";
							if (customerTab != null) pt.TabId = customerTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case ShippingProvider.ShippingProviderSectionNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ShippingProvidersSectionNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "baggage-cart.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(ShippingProvider.ShippingProviderRepositoryNodeAlias).Id;

						dt.Save();
						break;
					case ShippingProvider.ShippingProviderZoneSectionNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ShippingProviderZonesSectionNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "globe-green.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(ShippingProvider.ShippingProviderRepositoryNodeAlias).Id;

						dt.Save();
						break;
					case ShippingProvider.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ShippingProviderNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "baggage-cart-box-label.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(ShippingProvider.ShippingProviderSectionNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (detailsTab == null)
						{
							var createTab = dt.AddVirtualTab("Details");
							detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("image") == null)
						{
							var pt = dt.AddPropertyType(mediaPickerDataType, "image", "#Image");
							pt.Description = "#ImageDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("type") == null)
						{
							var pt = dt.AddPropertyType(shippingProviderTypeDataType, "type", "#ShippingProviderType");
							pt.Description = "#ShippingTypeDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("zone") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "zone", "#Zone");
							pt.Description = "#ShippingZoneDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("rangeType") == null)
						{
							var pt = dt.AddPropertyType(shippingRangeTypeDataType, "rangeType", "#RangeType");
							pt.Description = "#RangeDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("rangeStart") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "rangeStart", "#RangeStart");
							pt.Description = "#RangeStartDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("rangeEnd") == null)
						{
							var pt = dt.AddPropertyType(numericDataType, "rangeEnd", "#RangeEnd");
							pt.Description = "#RangeEndDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("overrule") == null)
						{
							var pt = dt.AddPropertyType(trueFalseDataType, "overrule", "#Overrule");
							pt.Description = "#OverruleDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case ShippingProviderMethodNode.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ShippingProviderMethodNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "magnet-small.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(ShippingProvider.ShippingProviderSectionNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (priceTab == null)
						{
							var createTab = dt.AddVirtualTab("Price");
							priceTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "ShippingProviderMethodNodeTitleDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("price") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "price", "#Price");
							pt.Description = "#PriceDescription";
							pt.Mandatory = true;
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("vat") == null)
						{
							var pt = dt.AddPropertyType(vatPercentagePicker, "vat", "#Vat");
							pt.Description = "#VatDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case Zone.ShippingZoneNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#ShippingProviderZoneNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "map-pin.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(ShippingProvider.ShippingProviderZoneSectionNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("zone") == null)
						{
							var pt = dt.AddPropertyType(zoneSelectorDataType, "zone", "#Zones");
							pt.Description = "#ZonesDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}


						dt.Save();
						break;
					case PaymentProvider.PaymentProviderSectionNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#PaymentProvidersSectionNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "bank.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(PaymentProvider.PaymentProviderRepositoryNodeAlias).Id;

						dt.Save();
						break;
					case PaymentProvider.PaymentProviderZoneSectionNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#PaymentProviderZonesSectionNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "globe-green.png";
						}
						if (dt.MasterContentType == 0)
							dt.MasterContentType = DocumentType.GetByAlias(PaymentProvider.PaymentProviderRepositoryNodeAlias).Id;

						dt.Save();
						break;
					case PaymentProvider.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#PaymentProviderNodeDescription";
						}

						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "credit-card.png";
						}

						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(PaymentProvider.PaymentProviderSectionNodeAlias).Id;
						}

						var paymentProviderTemplate = Template.GetByAlias("uWebshopPaymentsHandler");

						if (paymentProviderTemplate != null)
						{
							if (!dt.allowedTemplates.Contains(paymentProviderTemplate))
							{
								dt.allowedTemplates = new[] { paymentProviderTemplate };
								dt.DefaultTemplate = paymentProviderTemplate.Id;
							}
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (detailsTab == null)
						{
							var createTab = dt.AddVirtualTab("Details");
							detailsTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Mandatory = true;
							pt.Description = "#TitleDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("image") == null)
						{
							var pt = dt.AddPropertyType(mediaPickerDataType, "image", "#Image");
							pt.Description = "#ImageDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("type") == null)
						{
							var pt = dt.AddPropertyType(paymentProviderTypeDataType, "type", "#PaymentProviderType");
							pt.Mandatory = true;
							pt.Description = "#PaymentTypeDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("zone") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "zone", "#Zone");
							pt.Mandatory = true;
							pt.Description = "#ZoneDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("testMode") == null)
						{
							var pt = dt.AddPropertyType(enableDisableDataType, "testMode", "#TestMode");
							pt.Description = "#TestModeDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}
						
						if (dt.getPropertyType("successNode") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "successNode", "#SuccessNode");
							pt.Description = "#SuccessNodeDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Mandatory = true;
							pt.Save();
						}

						if (dt.getPropertyType("errorNode") == null)
						{
							var pt = dt.AddPropertyType(nodePickerDataType, "errorNode", "#ErrorNode");
							pt.Description = "#ErrorNodeDescription";
							if (detailsTab != null) pt.TabId = detailsTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
					case PaymentProviderMethodNode.NodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#PaymentProviderMethodNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "magnet-small.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(PaymentProvider.PaymentProviderSectionNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (priceTab == null)
						{
							var createPriceTab = dt.AddVirtualTab("Price");
							priceTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createPriceTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							pt.Mandatory = true;
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("price") == null)
						{
							var pt = dt.AddPropertyType(priceDataType, "price", "#Price");
							pt.Description = "#PriceDescription";
							pt.Mandatory = true;
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("amountType") == null)
						{
							var pt = dt.AddPropertyType(paymentProviderAmountDataType, "amountType", "#AmountType");
							pt.Description = "#AmountTypeDescription";
							pt.Mandatory = true;
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("vat") == null)
						{
							var pt = dt.AddPropertyType(vatPercentagePicker, "vat", "#Vat");
							pt.Description = "#PaymentProviderMethodNodeVatDescription";
							if (priceTab != null) pt.TabId = priceTab.Id;
							pt.Save();
						}
						
						dt.Save();
						break;
					case Zone.PaymentZoneNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#PaymentProvderZoneNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "map-pin.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(PaymentProvider.PaymentProviderZoneSectionNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("zone") == null)
						{
							var pt = dt.AddPropertyType(zoneSelectorDataType, "zone", "#Zones");
							pt.Description = "#ZonesDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}


						dt.Save();
						break;
					case Email.EmailTemplateStoreSectionNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#EmailToStoreEmailSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "mail-air.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Email.EmailRepositoryNodeAlias).Id;
						}
						dt.Save();
						break;
					case Email.EmailTemplateCustomerSectionNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#EmailToCustomerEmailSectionDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "mail-air.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Email.EmailRepositoryNodeAlias).Id;
						}

						dt.Save();
						break;
					case Email.EmailTemplateStoreNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#EmailStoreTemplateNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "mail-open-document-text.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Email.EmailTemplateStoreSectionNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("xslttemplate") == null)
						{
							var pt = dt.AddPropertyType(xsltTemplatePickerDataType, "xslttemplate", "#Template");
							pt.Description = "#TemplateDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}


						dt.Save();
						break;
					case Email.EmailTemplateCustomerNodeAlias:
						if (string.IsNullOrEmpty(dt.Description))
						{
							dt.Description = "#EmailCustomerTemplateNodeDescription";
						}
						if (string.IsNullOrEmpty(dt.IconUrl) || dt.IconUrl == "folder.gif")
						{
							dt.IconUrl = "mail-open-document-text.png";
						}
						if (dt.MasterContentType == 0)
						{
							dt.MasterContentType = DocumentType.GetByAlias(Email.EmailTemplateCustomerSectionNodeAlias).Id;
						}

						if (globalTab == null)
						{
							var createTab = dt.AddVirtualTab("Global");
							globalTab = dt.getVirtualTabs.FirstOrDefault(x => x.Id == createTab);
							dt.Save();
						}

						if (dt.getPropertyType("title") == null)
						{
							var pt = dt.AddPropertyType(textstringDataType, "title", "#Title");
							pt.Description = "#TitleDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("description") == null)
						{
							var pt = dt.AddPropertyType(textboxMultipleDataType, "description", "#Description");
							pt.Description = "#DescriptionDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						if (dt.getPropertyType("xslttemplate") == null)
						{
							var pt = dt.AddPropertyType(xsltTemplatePickerDataType, "xslttemplate", "#Template");
							pt.Description = "#TemplateDescription";
							if (globalTab != null) pt.TabId = globalTab.Id;
							pt.Save();
						}

						dt.Save();
						break;
				}
			}
		}

		/// <summary>
		/// Set the allowed children documents
		/// </summary>
		private static void SetAllowedChildren()
		{
			var uWebshopDT = DocumentType.GetByAlias("uWebshop");
			var uwbsStoreRepositoryDT = DocumentType.GetByAlias(Store.StoreRepositoryNodeAlias);
			var uwbsStoreDT = DocumentType.GetByAlias(Store.NodeAlias);
			var uwbsCatalogDT = DocumentType.GetByAlias(Catalog.NodeAlias);
			var uwbsCategoryRepositoryDT = DocumentType.GetByAlias(Catalog.CategoryRepositoryNodeAlias);
			var uwbsCategoryDT = DocumentType.GetByAlias(Category.NodeAlias);
			var uwbsProductRepositoryDT = DocumentType.GetByAlias(Catalog.ProductRepositoryNodeAlias);
			var uwbsProductDT = DocumentType.GetByAlias(Product.NodeAlias);
			var uwbsOrderedProductDT = DocumentType.GetByAlias(OrderedProduct.NodeAlias);
			var uwbsOrderedProductVariantDT = DocumentType.GetByAlias(OrderedProductVariant.NodeAlias);
			var uwbsProductVariantDT = DocumentType.GetByAlias(ProductVariant.NodeAlias);
			var uwbsDiscountRepositoryDT = DocumentType.GetByAlias("uwbsDiscountRepository");
			var uwbsDiscountProductSectionDT = DocumentType.GetByAlias("uwbsDiscountProductSection");
			var uwbsDiscountOrderSectionDT = DocumentType.GetByAlias("uwbsDiscountOrderSection");
			var uwbsDiscountProductDT = DocumentType.GetByAlias(DiscountProduct.NodeAlias);
			var uwbsDiscountOrderDT = DocumentType.GetByAlias(DiscountOrder.NodeAlias);
			var uwbsOrderRespositoryDT = DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias);
			var uwbsOrderSectionDT = DocumentType.GetByAlias(OrderSection.NodeAlias);
			var uwbsOrderDateFolderDT = DocumentType.GetByAlias(DateFolder.NodeAlias);
			var uwbsOrderStoreFolderDT = DocumentType.GetByAlias(OrderStoreFolder.NodeAlias);
			var uwbsOrderDT = DocumentType.GetByAlias(Order.NodeAlias);
			var uwbsShippingProviderRepositoryDT = DocumentType.GetByAlias(ShippingProvider.ShippingProviderRepositoryNodeAlias);
			var uwbsShippingProviderSectionDT = DocumentType.GetByAlias(ShippingProvider.ShippingProviderSectionNodeAlias);
			var uwbsShippingProviderDT = DocumentType.GetByAlias(ShippingProvider.NodeAlias);
			var uwbsShippingProviderMethodDT = DocumentType.GetByAlias(ShippingProviderMethodNode.NodeAlias);
			var uwbsShippingProviderZoneSectionDT = DocumentType.GetByAlias(ShippingProvider.ShippingProviderZoneSectionNodeAlias);
			var uwbsShippingProviderZoneDT = DocumentType.GetByAlias(Zone.ShippingZoneNodeAlias);
			var uwbsPaymentProviderRepositoryDT = DocumentType.GetByAlias(PaymentProvider.PaymentProviderRepositoryNodeAlias);
			var uwbsPaymentProviderSectionDT = DocumentType.GetByAlias(PaymentProvider.PaymentProviderSectionNodeAlias);
			var uwbsPaymentProviderDT = DocumentType.GetByAlias(PaymentProvider.NodeAlias);
			var uwbsPaymentProviderMethodDT = DocumentType.GetByAlias(PaymentProviderMethodNode.NodeAlias);
			var uwbsPaymentProviderZoneSectionDT = DocumentType.GetByAlias(PaymentProvider.PaymentProviderZoneSectionNodeAlias);
			var uwbsPaymentProviderZoneDT = DocumentType.GetByAlias(Zone.PaymentZoneNodeAlias);
			var uwbsEmailRepositoryDT = DocumentType.GetByAlias(Email.EmailRepositoryNodeAlias);
			var uwbsEmailTemplateStoreSectionDT = DocumentType.GetByAlias(Email.EmailTemplateStoreSectionNodeAlias);
			var uwbsEmailTemplateCustomerSectionDT = DocumentType.GetByAlias(Email.EmailTemplateCustomerSectionNodeAlias);
			var uwbsEmailTemplateStoreDT = DocumentType.GetByAlias(Email.EmailTemplateStoreNodeAlias);
			var uwbsEmailTemplateCustomerDT = DocumentType.GetByAlias(Email.EmailTemplateCustomerNodeAlias);
			var uwbsSettingsDT = DocumentType.GetByAlias(Settings.NodeAlias);

			uWebshopDT.AllowedChildContentTypeIDs = new[]
				{
					uwbsStoreRepositoryDT.Id, uwbsCatalogDT.Id, uwbsDiscountRepositoryDT.Id,
					uwbsShippingProviderRepositoryDT.Id, uwbsPaymentProviderRepositoryDT.Id, uwbsEmailRepositoryDT.Id,
					uwbsSettingsDT.Id
				};
			uWebshopDT.Save();

			uwbsStoreRepositoryDT.AllowedChildContentTypeIDs = new[] { uwbsStoreDT.Id };
			uwbsStoreRepositoryDT.Save();

			uwbsCatalogDT.AllowedChildContentTypeIDs = new[] { uwbsCategoryRepositoryDT.Id, uwbsProductRepositoryDT.Id };
			uwbsCatalogDT.Save();

			uwbsCategoryRepositoryDT.AllowedChildContentTypeIDs = new[] { uwbsCategoryDT.Id };
			uwbsCategoryRepositoryDT.Save();

			uwbsCategoryDT.AllowedChildContentTypeIDs = new[] { uwbsCategoryDT.Id, uwbsProductDT.Id };
			uwbsCategoryDT.Save();

			uwbsProductRepositoryDT.AllowedChildContentTypeIDs = new[] { uwbsProductDT.Id };
			uwbsProductRepositoryDT.Save();

			uwbsProductDT.AllowedChildContentTypeIDs = new[] { uwbsProductVariantDT.Id };
			uwbsProductDT.Save();

			uwbsDiscountRepositoryDT.AllowedChildContentTypeIDs = new[] { uwbsDiscountOrderSectionDT.Id, uwbsDiscountProductSectionDT.Id };
			uwbsDiscountRepositoryDT.Save();

			uwbsDiscountProductSectionDT.AllowedChildContentTypeIDs = new[] { uwbsDiscountProductDT.Id };
			uwbsDiscountProductSectionDT.Save();

			uwbsDiscountOrderSectionDT.AllowedChildContentTypeIDs = new[] { uwbsDiscountOrderDT.Id };
			uwbsDiscountOrderSectionDT.Save();

			uwbsOrderRespositoryDT.AllowedChildContentTypeIDs = new[] { uwbsOrderSectionDT.Id };
			uwbsOrderRespositoryDT.Save();

			uwbsOrderSectionDT.AllowedChildContentTypeIDs = new[] { uwbsOrderStoreFolderDT.Id, uwbsOrderDateFolderDT.Id };
			uwbsOrderSectionDT.Save();

			uwbsOrderStoreFolderDT.AllowedChildContentTypeIDs = new[] { uwbsOrderDateFolderDT.Id };
			uwbsOrderStoreFolderDT.Save();

			uwbsOrderDateFolderDT.AllowedChildContentTypeIDs = new[] { uwbsOrderDateFolderDT.Id, uwbsOrderDT.Id };
			uwbsOrderDateFolderDT.Save();

			uwbsOrderDT.AllowedChildContentTypeIDs = new[] { uwbsOrderedProductDT.Id };
			uwbsOrderDT.Save();

			uwbsOrderedProductDT.AllowedChildContentTypeIDs = new[] { uwbsOrderedProductVariantDT.Id };
			uwbsOrderedProductDT.Save();

			uwbsShippingProviderRepositoryDT.AllowedChildContentTypeIDs = new[] { uwbsShippingProviderSectionDT.Id, uwbsShippingProviderZoneSectionDT.Id };
			uwbsShippingProviderRepositoryDT.Save();

			uwbsShippingProviderSectionDT.AllowedChildContentTypeIDs = new[] { uwbsShippingProviderDT.Id };
			uwbsShippingProviderSectionDT.Save();

			uwbsShippingProviderDT.AllowedChildContentTypeIDs = new[] { uwbsShippingProviderMethodDT.Id };
			uwbsShippingProviderDT.Save();

			uwbsShippingProviderZoneSectionDT.AllowedChildContentTypeIDs = new[] { uwbsShippingProviderZoneDT.Id };
			uwbsShippingProviderZoneSectionDT.Save();

			uwbsPaymentProviderRepositoryDT.AllowedChildContentTypeIDs = new[] { uwbsPaymentProviderSectionDT.Id, uwbsPaymentProviderZoneSectionDT.Id };
			uwbsPaymentProviderRepositoryDT.Save();

			uwbsPaymentProviderSectionDT.AllowedChildContentTypeIDs = new[] { uwbsPaymentProviderDT.Id };
			uwbsPaymentProviderSectionDT.Save();

			uwbsPaymentProviderDT.AllowedChildContentTypeIDs = new[] { uwbsPaymentProviderMethodDT.Id };
			uwbsPaymentProviderDT.Save();

			uwbsPaymentProviderZoneSectionDT.AllowedChildContentTypeIDs = new[] { uwbsPaymentProviderZoneDT.Id };
			uwbsPaymentProviderZoneSectionDT.Save();

			uwbsEmailRepositoryDT.AllowedChildContentTypeIDs = new[] { uwbsEmailTemplateStoreSectionDT.Id, uwbsEmailTemplateCustomerSectionDT.Id };
			uwbsEmailRepositoryDT.Save();

			uwbsEmailTemplateStoreSectionDT.AllowedChildContentTypeIDs = new[] { uwbsEmailTemplateStoreDT.Id };
			uwbsEmailTemplateStoreSectionDT.Save();

			uwbsEmailTemplateCustomerSectionDT.AllowedChildContentTypeIDs = new[] { uwbsEmailTemplateCustomerDT.Id };
			uwbsEmailTemplateCustomerSectionDT.Save();
		}

		/// <summary>
		/// Create the default uWebshop documents in the content tree
		/// </summary>
		/// <param name="documentTypeAliasList"></param>
		private static void CreateDocuments(IEnumerable<string> documentTypeAliasList)
		{
			var author = new User(0);

			_uWebshopDocId = 0;
			_update = false;

			if (documentTypeAliasList == null) throw new ArgumentNullException("documentTypeAliasList");
			foreach (var documentTypeAlias in documentTypeAliasList)
			{
				var dt = DocumentType.GetByAlias(documentTypeAlias);

				switch (dt.Alias)
				{
					case "uWebshop":
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any())
						{
							var uWebshopDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc == null)
							{
								var doc = Document.MakeNew("uWebshop", dt, author, -1);
								doc.Save();
								_uWebshopDocId = doc.Id;
							}
							else
							{
								_update = true;
							}
							//doc.Publish(author);
						}
						break;
					case Store.StoreRepositoryNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any())
						{
							var uWebshopDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Stores", dt, author, uWebshopDoc.Id);
								doc.sortOrder = 10;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case Catalog.NodeAlias:

						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Catalog", dt, author, uWebshopDoc.Id);
								doc.sortOrder = 20;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case Order.OrderRepositoryNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Orders", dt, author, uWebshopDoc.Id);
								doc.sortOrder = 25;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case OrderSection.NodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc = Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias(Order.OrderRepositoryNodeAlias).Id).FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc1 = Document.MakeNew("Confirmed Orders", dt, author, uWebshopDoc.Id);
								doc1.SetProperty("orderSection", OrderStatus.Confirmed.ToString());
								var doc2 = Document.MakeNew("Unpaid Orders", dt, author, uWebshopDoc.Id);
								doc2.SetProperty("orderSection", OrderStatus.WaitingForPayment.ToString());
								var doc3 = Document.MakeNew("Paid Orders", dt, author, uWebshopDoc.Id);
								doc3.SetProperty("orderSection", OrderStatus.ReadyForDispatch.ToString());
								var doc4 = Document.MakeNew("Dispatched Orders", dt, author, uWebshopDoc.Id);
								doc4.SetProperty("orderSection", OrderStatus.Dispatched.ToString());
								var doc5 = Document.MakeNew("Completed Orders", dt, author, uWebshopDoc.Id);
								doc5.SetProperty("orderSection", OrderStatus.Closed.ToString());

								doc1.sortOrder = 10;
								doc1.Save();
								doc2.sortOrder = 20;
								doc2.Save();
								doc3.sortOrder = 30;
								doc3.Save();
								doc4.sortOrder = 40;
								doc4.Save();
								doc5.sortOrder = 50;
								doc5.Save();
								//doc1.Publish(author);
								//doc2.Publish(author);
								//doc3.Publish(author);
								//doc4.Publish(author);
								//doc5.Publish(author);
							}
						}
						break;
					case "uwbsDiscountRepository":
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc = Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Discounts", dt, author, uWebshopDoc.Id);
								doc.sortOrder = 30;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case PaymentProvider.PaymentProviderRepositoryNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc = Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Payment Providers", dt, author, uWebshopDoc.Id);
								doc.sortOrder = 40;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case ShippingProvider.ShippingProviderRepositoryNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc = Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Shipping Providers", dt, author, uWebshopDoc.Id);
								doc.sortOrder = 50;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case Email.EmailRepositoryNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc = Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Emails", dt, author, uWebshopDoc.Id);
								doc.sortOrder = 60;
								doc.Save();
								//doc.Publish(author);
							}
						}

						break;
					case Settings.NodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var uWebshopDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uWebshop").Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (uWebshopDoc != null)
							{
								var doc = Document.MakeNew("Settings", dt, author, uWebshopDoc.Id);
								doc.SetProperty("incompleteOrderLifetime", 360);
								doc.sortOrder = 99;
								doc.Save();
								//doc.Publish(author);
							}
						}

						break;
					case Store.NodeAlias:
						break;
					case Catalog.CategoryRepositoryNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var catalogDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias(Catalog.NodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (catalogDoc != null)
							{
								var doc = Document.MakeNew("Categories", dt, author, catalogDoc.Id);
								doc.sortOrder = 10;
								doc.Save();
								//doc.Publish(author);
							}
						}

						break;
					case Catalog.ProductRepositoryNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var catalogDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias(Catalog.NodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (catalogDoc != null)
							{
								var doc = Document.MakeNew("Products", dt, author, catalogDoc.Id);
								doc.sortOrder = 20;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case Category.NodeAlias:
						break;
					case Product.NodeAlias:
						break;
					case ProductVariant.NodeAlias:
						break;
					case "uwbsDiscountProductSection":
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uwbsDiscountRepository").Id)
										.
										 FirstOrDefault(x => !x.IsTrashed);

							if (parentDoc != null)
							{
								var doc = Document.MakeNew("Product Discounts", dt, author, parentDoc.Id);

								doc.sortOrder = 10;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case "uwbsDiscountOrderSection":
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any())
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias("uwbsDiscountRepository").Id)
										.
										 FirstOrDefault();

							if (parentDoc != null)
							{
								var doc = Document.MakeNew("Order Discounts", dt, author, parentDoc.Id);

								doc.sortOrder = 10;
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case DiscountProduct.NodeAlias:
						break;
					case DiscountOrder.NodeAlias:
						break;
					//case DiscountCoupon.NodeAlias:
					//    break;
					case ShippingProvider.ShippingProviderSectionNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(
									DocumentType.GetByAlias(ShippingProvider.ShippingProviderRepositoryNodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (parentDoc != null)
							{
								var doc =
									Document.MakeNew("Shipping Providers", dt, author,
													 parentDoc.Id);
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case ShippingProvider.ShippingProviderZoneSectionNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(
									DocumentType.GetByAlias(ShippingProvider.ShippingProviderRepositoryNodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (parentDoc != null)
							{
								var doc =
									Document.MakeNew("Shipping Provider Zones", dt, author,
													 parentDoc.Id);
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case ShippingProvider.NodeAlias:
						break;
					case ShippingProviderMethodNode.NodeAlias:
						break;
					case Zone.ShippingZoneNodeAlias:
						break;
					case PaymentProvider.PaymentProviderSectionNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(
									DocumentType.GetByAlias(PaymentProvider.PaymentProviderRepositoryNodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (parentDoc != null)
							{
								var doc =
									Document.MakeNew("Payment Providers", dt, author, parentDoc.Id);
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case PaymentProvider.PaymentProviderZoneSectionNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(
									DocumentType.GetByAlias(PaymentProvider.PaymentProviderRepositoryNodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (parentDoc != null)
							{
								var doc =
									Document.MakeNew("Payment Provider Zones", dt, author, parentDoc.Id);
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case PaymentProvider.NodeAlias:
						break;
					case PaymentProviderMethodNode.NodeAlias:
						break;
					case Zone.PaymentZoneNodeAlias:
						break;
					case Email.EmailTemplateStoreSectionNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias(Email.EmailRepositoryNodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (parentDoc != null)
							{
								var doc = Document.MakeNew("Shop Emails", dt, author, parentDoc.Id);
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case Email.EmailTemplateCustomerSectionNodeAlias:
						if (!Document.GetDocumentsOfDocumentType(dt.Id).Any(x => !x.IsTrashed))
						{
							var parentDoc =
								Document.GetDocumentsOfDocumentType(DocumentType.GetByAlias(Email.EmailRepositoryNodeAlias).Id).
										 FirstOrDefault(x => !x.IsTrashed);

							if (parentDoc != null)
							{
								var doc = Document.MakeNew("Customer Emails", dt, author, parentDoc.Id);
								doc.Save();
								//doc.Publish(author);
							}
						}
						break;
					case "uwbsEmailTemplate":
						break;
				}
			}




		}

		private static void PublishWithChildrenWithResult(Document doc, User u)
		{
			if (doc.PublishWithResult(u))
			{
				foreach (var dc in doc.Children.ToList())
				{
					dc.PublishWithChildrenWithResult(u);
				}
			}
			else
			{
				return;
			}

			return;
		}

		private static void UpdateDocumentCache(Document doc)
		{
			library.UpdateDocumentCache(doc.Id);

			foreach (var dc in doc.Children.ToList())
			{
				library.UpdateDocumentCache(dc.Id);
			}
		}
	}
}
