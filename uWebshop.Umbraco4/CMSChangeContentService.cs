﻿using umbraco;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.web;
using uWebshop.Umbraco.Interfaces;

namespace uWebshop.Umbraco4
{
	internal class CMSChangeContentService : ICMSChangeContentService
	{
		public ICMSContent GetById(int nodeId)
		{
			return new CMSContent(nodeId);
		}

		class CMSContent : ICMSContent
		{
			public CMSContent(int id)
			{
				_document = new Document(id);
			}

			private readonly Document _document;

			public void SetValue(string propertyAlias, string value)
			{
				_document.SetProperty(propertyAlias, value);
			}

			public void SaveAndPublish()
			{
				_document.Save();
				_document.Publish(new User(0));
			}

			public string ContentTypeAlias
			{
				get { return _document.ContentType.Alias; }
			}

			public bool HasProperty(string propertyAlias)
			{
				return _document.HasProperty(propertyAlias);
			}
		}
	}
}
