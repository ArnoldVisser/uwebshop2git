﻿using System;
using uWebshop.Common.Interfaces;
using umbraco;
using umbraco.BusinessLogic;

namespace uWebshop.Umbraco4
{
	internal class UmbracoLoggingService : ILoggingService
	{
		private static bool? _uWebshopDebugMessages;
		private static bool UWebshopDebugMessages
		{
			get { return _uWebshopDebugMessages ?? (_uWebshopDebugMessages =
				System.Web.Configuration.WebConfigurationManager.AppSettings["uWebshopDebugMessages"] == "true" || GlobalSettings.DebugMode).GetValueOrDefault(); }
		}

		private static bool? _uWebshopYSODOnError;
		private static bool UWebshopYSODOnError
		{
			get
			{
				return _uWebshopYSODOnError ?? (_uWebshopYSODOnError =
					System.Web.Configuration.WebConfigurationManager.AppSettings["uWebshopYSODOnError"] == "true").GetValueOrDefault();
			}
		}

		public void LogError(Exception exception, string message = null)
		{
			Log.Add(LogTypes.Error, 0, (string.IsNullOrEmpty(message) ? "" : message + " ") + exception);
			if (UWebshopYSODOnError)
			{
				throw exception;
			}
		}

		public void LogError(string message)
		{
			Log.Add(LogTypes.Error, 0, message);
		}

		public void LogWarning(string message)
		{
			Log.Add(LogTypes.Notify, 0, message);
		}

		public void LogDebug(string message)
		{
			if (UWebshopDebugMessages)
				Log.Add(LogTypes.Debug, 0, message);
		}
	}
}
