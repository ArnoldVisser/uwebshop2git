﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using umbraco;
using umbraco.BasePages;
using umbraco.BusinessLogic;
using umbraco.NodeFactory;
using umbraco.cms.businesslogic.datatype;
using umbraco.cms.businesslogic.web;
using Log = uWebshop.Domain.Log;

namespace uWebshop.Umbraco4
{
	internal class UmbracoDocumentTypeInstaller : IUmbracoDocumentTypeInstaller
	{
		public void InstallStore(string storeAlias)
		{
			var trueFalseDataType = DataTypeDefinition.GetAll().SingleOrDefault(x => x.Text == "True/false");
			var storeTemplatePickerDataType = DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == new Guid("2ad05995-470e-47d9-956d-dd2ec892343d"))
											  ?? DataTypeDefinition.GetAll().FirstOrDefault(x => x.UniqueId == new Guid("a20c7c00-09f1-448d-9656-f5cb012107af"));

			if (storeTemplatePickerDataType == null)
				throw new Exception("Could not find storeTemplatePickerDataType");

			// add disable true/false propertytypes to the document types that require them
			foreach (var documentTypeAlias in StoreHelper.StoreDependantDocumentTypeAliasList)
			{
				var dt = DocumentType.GetByAlias(documentTypeAlias);

				if (dt == null) continue;

				if (dt.getVirtualTabs.All(x => x.Caption != storeAlias))
				{
					dt.AddVirtualTab(storeAlias);
					dt.Save();
				}
				else return;

				if (dt.getPropertyType(String.Format("disable_{0}", storeAlias)) == null)
				{
					dt.AddPropertyType(trueFalseDataType, String.Format("disable_{0}", storeAlias), "#Disable");
					dt.Save();
				}

				if (Category.IsAlias(documentTypeAlias) || Product.IsAlias(documentTypeAlias))
				{
					if (dt.getPropertyType(String.Format("template_{0}", storeAlias)) == null)
					{
						dt.AddPropertyType(storeTemplatePickerDataType, String.Format("template_{0}", storeAlias), "#Template");
						dt.Save();
					}
				}

				// get tabs
				var dtTabs = dt.getVirtualTabs;

				var storeAliasTab = dtTabs.FirstOrDefault(x => x.Caption.ToLower() == storeAlias.ToLower());
				if (storeAliasTab != null)
				{
					dt.getPropertyType(String.Format("disable_{0}", storeAlias)).TabId = storeAliasTab.Id;

					if (Category.IsAlias(documentTypeAlias) || Product.IsAlias(documentTypeAlias))
					{
						dt.getPropertyType(String.Format("template_{0}", storeAlias)).TabId = storeAliasTab.Id;
					}

					dt.Save();
				}
			}
		}

		public void UnInstallStore(string storeAlias)
		{
			foreach (var documentTypeAlias in StoreHelper.StoreDependantDocumentTypeAliasList)
			{
				var dt = DocumentType.GetByAlias(documentTypeAlias);

				if (dt == null) continue;
				var storeAliasTab = dt.getVirtualTabs.Single(x => x.Caption == storeAlias);

				foreach (var property in storeAliasTab.GetAllPropertyTypes())
				{
					property.delete();
				}
				dt.DeleteVirtualTab(storeAliasTab.Id);

				dt.Save();
			}

			library.RefreshContent();
		}

		// untested internally used functionality, keep as small as possible
		public void CreateOrderDocument(OrderInfo orderInfo)
		{
			// todo: check whether document has already been created, in that case focus on it

			if (orderInfo.OrderNodeId != 0)
			{
				try
				{
					var existingDocument = new Document(orderInfo.OrderNodeId);
					if (!existingDocument.IsTrashed)
					{
						var path = existingDocument.Path;
						if (path != null && path.Length > 1 && BasePage.Current != null && orderInfo.OrderNodeId != 0)
						{
							BasePage.Current.ClientTools.SyncTree(path, true);
							BasePage.Current.ClientTools.ChangeContentFrameUrl(String.Concat("editContent.aspx?id=", orderInfo.OrderNodeId));
							return;
						}
					}
				}
				catch
				{

				}
			}

			#region create order document

			var orderRepository = DomainHelper.GetObjectsByAlias<OrderSection>(Order.OrderRepositoryNodeAlias).FirstOrDefault();
			if (orderRepository == null)
				return;

			var orderRepositoryNode = new Node(orderRepository.Id);

			//if (parentDocumentId == 0)
			//{
			//	var currentSectionNode = DomainHelper.GetObjectsByAlias<OrderSection>(OrderSection.NodeAlias)
			//											.FirstOrDefault(x => x.OrderStatusInSection == OrderStatus.Confirmed);
			//	if (currentSectionNode == null || currentSectionNode.Node == null)
			//	{
			//		Log.Instance.LogError("No confirmed order section found!");
			//		return;
			//	}

			//	currentSectionNodeNode = currentSectionNode.Node;
			//	parentDocumentId = currentSectionNodeNode.Id;
			//}
			var author = User.GetUser(0);

			const string dateFolderAlias = DateFolder.NodeAlias;

			#region store folder

			var storeFolder = orderRepositoryNode.Children.Cast<Node>().FirstOrDefault(node => node.Name == orderInfo.StoreInfo.Alias);

			Log.Instance.LogDebug("CreateOrderDocument STEP 6");
			if (storeFolder == null)
			{
				var storeFolderDoc = Document.MakeNew(orderInfo.StoreInfo.Alias, DocumentType.GetByAlias(OrderStoreFolder.NodeAlias), author, orderRepositoryNode.Id);
				storeFolderDoc.Publish(author);
				library.UpdateDocumentCache(storeFolderDoc.Id);
				//Log.Add(LogTypes.Error, yearDoc.Id, "Year Node Created");
				storeFolder = new Node(storeFolderDoc.Id);
			}

			Log.Instance.LogDebug("CreateOrderDocument STEP 7");

			#endregion

			#region datefolders

			var year = orderInfo.ConfirmDate.GetValueOrDefault().ToString("yyyy");
			var month = orderInfo.ConfirmDate.GetValueOrDefault().ToString("MM");
			var day = orderInfo.ConfirmDate.GetValueOrDefault().ToString("dd");

			Log.Instance.LogDebug("CreateOrderDocument STEP 8");

			#region year folder

			var yearNode = storeFolder.Children.Cast<Node>().FirstOrDefault(node => node.Name == year);

			if (yearNode == null)
			{
				var yearDoc = Document.MakeNew(year, DocumentType.GetByAlias(dateFolderAlias), author, storeFolder.Id);
				yearDoc.Publish(author);
				library.UpdateDocumentCache(yearDoc.Id);
				//Log.Add(LogTypes.Error, yearDoc.Id, "Year Node Created");
				yearNode = new Node(yearDoc.Id);
			}

			#endregion

			Log.Instance.LogDebug("CreateOrderDocument STEP 9");

			#region month folder

			var monthNode = yearNode.Children.Cast<Node>().FirstOrDefault(node => node.Name == month);

			if (monthNode == null)
			{
				var monthDoc = Document.MakeNew(month, DocumentType.GetByAlias(dateFolderAlias), author, yearNode.Id);
				monthDoc.Publish(author);
				library.UpdateDocumentCache(monthDoc.Id);
				//Log.Add(LogTypes.Error, monthDoc.Id, "Month Node Created");
				monthNode = new Node(monthDoc.Id);
			}

			#endregion

			Log.Instance.LogDebug("CreateOrderDocument STEP 10");

			#region day folder

			var dayNode = monthNode.Children.Cast<Node>().FirstOrDefault(node => node.Name == day);

			if (dayNode == null)
			{
				var dayDoc = Document.MakeNew(day, DocumentType.GetByAlias(dateFolderAlias), author, monthNode.Id);
				dayDoc.Publish(author);
				library.UpdateDocumentCache(dayDoc.Id);
				//Log.Add(LogTypes.Error, dayDoc.Id, "Day Node Created");
				dayNode = new Node(dayDoc.Id);
			}

			#endregion

			Log.Instance.LogDebug("CreateOrderDocument STEP 11");

			#endregion

			var documentType = DocumentType.GetByAlias(Order.NodeAlias);

			var orderDocument = Document.MakeNew(orderInfo.OrderNumber, documentType, author, dayNode.Id);
			orderDocument.SetProperty("orderGuid", orderInfo.UniqueOrderId.ToString());
			orderDocument.SetProperty("orderPaid", orderInfo.Paid);

			if (orderInfo.CustomerInfo.CustomerInformation != null)
			{
				foreach (var customerProperty in orderDocument.GenericProperties.Where(x => x.PropertyType.Alias.StartsWith("customer")))
				{
					var element = orderInfo.CustomerInfo.CustomerInformation.Element(customerProperty.PropertyType.Alias);
					if (element != null)
					{
						customerProperty.Value = element.Value;
					}
				}
			}

			if (orderInfo.CustomerInfo.ShippingInformation != null)
			{
				foreach (var shippingProperty in orderDocument.GenericProperties.Where(x => x.PropertyType.Alias.StartsWith("shipping")))
				{
					var element = orderInfo.CustomerInfo.ShippingInformation.Element(shippingProperty.PropertyType.Alias);
					if (element != null)
					{
						shippingProperty.Value = element.Value;
					}
				}
			}

			if (orderInfo.CustomerInfo.ExtraInformation != null)
			{
				foreach (var extraProperty in orderDocument.GenericProperties.Where(x => x.PropertyType.Alias.StartsWith("extra")))
				{
					var element = orderInfo.CustomerInfo.ExtraInformation.Element(extraProperty.PropertyType.Alias);
					if (element != null)
					{
						extraProperty.Value = element.Value;
					}
				}
			}

			#endregion

			foreach (var orderline in orderInfo.OrderLines)
			{
				var productInfo = orderline.ProductInfo;
				var orderedProductAlias = DocumentType.GetAllAsList().Select(dt => dt.Alias).FirstOrDefault(alias => productInfo.DocTypeAlias != null
						&& alias == productInfo.DocTypeAlias.Replace("uwbsProduct", "uwbsOrderedProduct")) ?? OrderedProduct.NodeAlias;

				var orderedProductDoc = Document.MakeNew(productInfo.Title, DocumentType.GetByAlias(orderedProductAlias), new User(0), orderDocument.Id);

				// possible todo: use setters on orderedProduct instead of getProperty()

				SetProperty(orderedProductDoc, "productId", productInfo.Id);
				SetProperty(orderedProductDoc, Product.TitleAlias, productInfo.Title);
				SetProperty(orderedProductDoc, "sku", productInfo.SKU);
				SetProperty(orderedProductDoc, "weight", productInfo.Weight.ToString());
				SetProperty(orderedProductDoc, "length", productInfo.Length.ToString());
				SetProperty(orderedProductDoc, "height", productInfo.Height.ToString());
				SetProperty(orderedProductDoc, "width", productInfo.Width.ToString());
				SetProperty(orderedProductDoc, "orderedProductDiscountPercentage", productInfo.DiscountPercentage.ToString());
				SetProperty(orderedProductDoc, "orderedProductDiscountAmount", productInfo.DiscountAmountInCents.ToString());
				SetProperty(orderedProductDoc, "vat", productInfo.Vat.ToString());
				SetProperty(orderedProductDoc, "price", productInfo.OriginalPriceInCents.ToString());
				SetProperty(orderedProductDoc, "ranges", productInfo.Ranges != null ? String.Join("#", productInfo.Ranges.Select(range => range.ToString())) : string.Empty);
				// todo: instellingsafhankelijk
				SetProperty(orderedProductDoc, "itemCount", productInfo.ItemCount.GetValueOrDefault(1).ToString());

				if (orderline.CustomData != null)
				{
					foreach (var customerProperty in orderedProductDoc.GenericProperties)
					{
						var element = orderline.CustomData.Element(customerProperty.PropertyType.Alias);
						if (element != null)
						{
							customerProperty.Value = element.Value;
						}
					}
				}

				orderedProductDoc.Save();

				// todo: variants
				foreach (var variant in orderline.ProductInfo.ProductVariants)
				{
					var variantDoc = Document.MakeNew(variant.Title, DocumentType.GetByAlias(OrderedProductVariant.NodeAlias), new User(0), orderedProductDoc.Id);

					SetProperty(variantDoc, "variantId", variant.Id.ToString());
					SetProperty(variantDoc, "title", variant.Title);
					SetProperty(variantDoc, "group", variant.Group);
					SetProperty(variantDoc, "weight", variant.Weight.ToString());
					SetProperty(variantDoc, "price", variant.PriceInCents.ToString());
					SetProperty(variantDoc, "discountPercentage", variant.DiscountPercentage.ToString());
					SetProperty(variantDoc, "discountAmount", variant.DiscountAmountInCents.ToString());

					variantDoc.Save();
				}
			}

			orderInfo.OrderNodeId = orderDocument.Id;
			orderInfo.Save();

			orderDocument.Save();

			if (orderDocument.Path == null)
			{
				throw new Exception("orderdocument.Path == Null");
			}

			if (!string.IsNullOrEmpty(orderDocument.Path))
				if (BasePage.Current != null && orderInfo.OrderNodeId != 0)
				{
					BasePage.Current.ClientTools.SyncTree(orderDocument.Path, true);
					BasePage.Current.ClientTools.ChangeContentFrameUrl(String.Concat("editContent.aspx?id=", orderInfo.OrderNodeId));
				}
		}

		private static void SetProperty(Document orderedProductDoc, string propertyAlias, object value)
		{
			if (orderedProductDoc.getProperty(propertyAlias) != null)
				orderedProductDoc.SetProperty(propertyAlias, value);
		}
	}
}
