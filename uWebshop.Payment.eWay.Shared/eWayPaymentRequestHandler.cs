﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Xml;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using uWebshop.Domain.Interfaces;
using uWebshop.Domain.Repositories;
using umbraco;
using umbraco.BusinessLogic;
using umbraco.NodeFactory;
using System.Web;

namespace uWebshop.Payment.eWay.Shared
{
    public class eWayPaymentRequestHandler : IPaymentRequestHandler
    {
        #region IPaymentRequestHandler Members
        public string GetName()
        {
            return "eWay";
        }

        public Domain.PaymentRequest CreatePaymentRequest(Domain.Order order)
        {
            var helper = new PaymentConfigHelper(order.OrderInfo.PaymentInfo.PaymentProviderName);


            var paymentProvider = new OrderRepository().GetAllPaymentProviders().Where(x => x.Name == order.OrderInfo.PaymentInfo.PaymentProviderName).FirstOrDefault();

            #region build urls
            var baseUrl = string.Format("http://{0}", library.GetCurrentDomains(Node.GetCurrent().Id).FirstOrDefault().Name);
            baseUrl = baseUrl.Substring(0, baseUrl.LastIndexOf("/"));

            if (baseUrl == string.Empty || baseUrl == "http:/")
            {
                baseUrl = "http://" + HttpContext.Current.Request.Url.Authority;
            }

            var nodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentData"), ShopAliasHelper.GetCurrentShopAlias());
            var returnUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(nodeId));
            #endregion

            #region fill payment request parameters
            var request = new PaymentRequest();
            request.Parameters.Add("CustomerID", helper.Settings["customerId"]);
            request.Parameters.Add("UserName", helper.Settings["userName"]);
            request.Parameters.Add("Amount", Decimal.Round(OrderHelper.GetTotalAmountUsingVATCheck(),2).ToString());

            Log.Add(LogTypes.Debug, 0, "OrderHelper.GetTotalAmountUsingVATCheck().ToString(): " + Decimal.Round(OrderHelper.GetTotalAmountUsingVATCheck(), 2).ToString());

            request.Parameters.Add("Currency", order.OrderInfo.ShopAlias.CurrencyCode);
            //request.Parameters.Add("Currency", "AUD");
            request.Parameters.Add("Language", order.OrderInfo.ShopAlias.LanguageCode);

            if (order.Node.GetProperty("customerFirstName") != null)
            {
                request.Parameters.Add("customerFirstName", order.CustomerFirstName);
            }
            if (order.Node.GetProperty("customerLastName") != null)
            {
                request.Parameters.Add("customerLastName", order.CustomerLastName);
            }
            if(order.Node.GetProperty("customerStreet") != null) {
                string Address = string.Empty;
                if (order.Node.GetProperty("customerStreet") != null)
                {
                    Address = order.Node.GetProperty("customerStreet").Value;
                }
                if (order.Node.GetProperty("customerStreetNumber") != null)
                {
                    Address += " " + order.Node.GetProperty("customerStreetNumber").Value;
                }
                request.Parameters.Add("CustomerAddress", Address);
            }
            if (order.Node.GetProperty("customerCity") != null)
            {
                request.Parameters.Add("CustomerCity", order.Node.GetProperty("customerCity").Value);
            }
            if (order.Node.GetProperty("customerState") != null)
            {
                request.Parameters.Add("CustomerState", order.Node.GetProperty("customerState").Value);
            }
            if (order.Node.GetProperty("customerPostalcode") != null)
            {
                request.Parameters.Add("CustomerPostCode", order.Node.GetProperty("customerPostalcode").Value);
            }

            request.Parameters.Add("CustomerCountry", order.OrderInfo.CustomerInfo.CountryName);
            request.Parameters.Add("CustomerEmail", order.CustomerEmail);
            request.Parameters.Add("InvoiceDescription", order.UniqueId.ToString());

            order.OrderInfo.PaymentInfo.PaymentTransactionId = order.UniqueId.ToString();
            request.Parameters.Add("MerchantReference", order.UniqueId.ToString());
            request.Parameters.Add("CancelURL", returnUrl);
            request.Parameters.Add("ReturnUrl", returnUrl);
            #endregion

            string apiURL = helper.Settings["requestGateway"] + "?" + request.ParametersAsString;

            try
            {
                var resultXML = string.Empty;

                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(apiURL);
                objRequest.Method = WebRequestMethods.Http.Get;

                try
                {
                    HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();

                    //get the response from the transaction generate page
                    using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                    {
                        resultXML = sr.ReadToEnd();
                        // Close and clean up the StreamReader
                        sr.Close();
                    }

                    

                    //parse the result message
                    TransactionRequestResult resultObj = ParseResults(resultXML);

                    //<?xml version="1.0" encoding="UTF-8" ?>
                    //<TransactionResponse>
                    //  <AuthCode>315153</AuthCode>
                    //  <ResponseCode>00</ResponseCode>
                    //  <ReturnAmount>10.00</ReturnAmount>
                    //  <TrxnNumber>1005039</TrxnNumber>
                    //  <TrxnStatus>true</TrxnStatus>
                    //  <TrxnResponseMessage>AUTH CODE:315153</TrxnResponseMessage>
                    //  <MerchantReference>513456</MerchantReference>
                    //  <MerchantInvoice>Inv 21540</MerchantInvoice>
                    //</redirecttransaction>

                    Log.Add(LogTypes.Debug, 0, "eWay response esultObj resultXML: " + resultXML);

                    order.OrderInfo.PaymentInfo.PaymentParameterRenderMethod = PaymentParameterRenderMethod.QueryString;
                    order.OrderInfo.PaymentInfo.PaymentURL = resultObj.URI;
                    order.OrderInfo.PaymentInfo.PaymentTransactionId = order.UniqueOrderId.ToString();

                    if (!resultObj.Result)
                    {
                        Log.Add(LogTypes.Error, 0, "eWay error: " + resultObj.Error);
                    }
                }
                catch (Exception ex)
                {
                    Log.Add(LogTypes.Error, 0, "eWay error: " + ex.ToString());
                }
            }
            catch (Exception ex2)
            {
                Log.Add(LogTypes.Error, 0, "eWay create payment request error: " + ex2.ToString());
            }

            return request;
        }

        /// <summary>
        /// Parse the result of the transaction request and save the appropriate fields in an object to be used later
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        private TransactionRequestResult ParseResults(string xml)
        {
            string _currentNode;
            StringReader _sr = new StringReader(xml);
            XmlTextReader _xtr = new XmlTextReader(_sr);
            _xtr.XmlResolver = null;
            _xtr.WhitespaceHandling = WhitespaceHandling.None;

            // get the root node
            _xtr.Read();

            TransactionRequestResult res = new TransactionRequestResult();

            if ((_xtr.NodeType == XmlNodeType.Element) && (_xtr.Name == "TransactionRequest"))
            {
                while (_xtr.Read())
                {
                    if ((_xtr.NodeType == XmlNodeType.Element) && (!_xtr.IsEmptyElement))
                    {
                        _currentNode = _xtr.Name;
                        _xtr.Read();
                        if (_xtr.NodeType == XmlNodeType.Text)
                        {
                            switch (_currentNode)
                            {
                                case "Result":
                                    res.Result = bool.Parse(_xtr.Value);
                                    break;

                                case "URI":
                                    res.URI = _xtr.Value;
                                    break;

                                case "Error":
                                    res.Error = _xtr.Value;
                                    break;
                            }
                        }
                    }
                }
            }

            return res;
        }
        #endregion

        /// <summary>
        /// Returns the URL to redirect to
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public string GetPaymentUrl(Order order)
        {
            return string.Empty;
        }
    }

    public class TransactionRequestResult
    {
        public bool Result;
        public string URI;
        public string Error;
    }


}
