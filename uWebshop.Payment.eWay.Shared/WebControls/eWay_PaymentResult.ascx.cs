﻿using System;
using System.Linq;
using System.Web;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Helpers;
using umbraco;
using umbraco.NodeFactory;
using System.Xml;
using System.IO;
using System.Net;
using System.Web.UI;
using umbraco.BusinessLogic;

namespace uWebshop.Payment.eWay.Shared.WebControls
{
    public partial class eWay_PaymentResult : System.Web.UI.UserControl
    {
        public string _AuthCode = string.Empty;
        public string _ResponseCode = string.Empty;
        public string _ReturnAmount = string.Empty;
        public string _TrxnNumber = string.Empty;
        public string _TrxnStatus = string.Empty;
        public string _MerchnatOption1 = string.Empty;
        public string _MerchnatOption2 = string.Empty;
        public string _MerchnatOption3 = string.Empty;
        public string _ReferenceNumber = string.Empty;
        public string _ReferenceInvoice = string.Empty;
        public string _TrxnResponseMessage = string.Empty;
        public string _ErrorMessage = string.Empty;
        public Order Order { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            string uniqueId = library.RequestCookies("PaymentOrderId");

            if (string.IsNullOrEmpty(uniqueId))
            {
                uniqueId = library.RequestCookies("CompletedOrderId");
            }

            if (!string.IsNullOrEmpty(uniqueId))
            {
                library.setCookie("PaymentOrderId", string.Empty);
                library.setCookie("TransactionId", string.Empty);

                Log.Add(LogTypes.Debug, Node.GetCurrent().Id, "eWay Shared uniqueId: " + uniqueId);
                Order = new Order(uniqueId);

                if (Order != null)
                {
                    Log.Add(LogTypes.Debug, Node.GetCurrent().Id, "eWay Shared Order: " + Order.OrderNumber);

                    string accessPaymentCode = string.Empty;
                    if (HttpContext.Current.Request.Form["AccessPaymentCode"] != null)
                        accessPaymentCode = HttpContext.Current.Request.Form["AccessPaymentCode"].ToString();

                    Log.Add(LogTypes.Debug, Node.GetCurrent().Id, "eWay Shared accessPaymentCode: " + accessPaymentCode);

                    if (CheckAccessCode(accessPaymentCode))
                    {
                        if (_TrxnStatus.ToLower().Equals("true"))
                        {
                            Order.OrderInfo.Status = OrderStatus.Ready_for_Dispatch;
                        }
                        else
                        {
                            Order.OrderInfo.Status = OrderStatus.Payment_Failed;
                        }

                        Order.Save();

                        var baseUrl = string.Format("http://{0}", library.GetCurrentDomains(Node.GetCurrent().Id).FirstOrDefault().Name);
                        baseUrl = baseUrl.Substring(0, baseUrl.LastIndexOf("/"));

                        if (Order.OrderInfo.Status == OrderStatus.Ready_for_Dispatch)
                        {
                            var successNodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmation"), ShopAliasHelper.GetCurrentShopAlias());
                            var successUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(successNodeId));

                            // redirect to payment success
                            Response.Redirect(successUrl);
                        }

                        if (Order.OrderInfo.Status == OrderStatus.Payment_Failed)
                        {
                            var failedNodeId = DomainHelper.GetNodeIdForDocument("uwbsOrderProcessNode", library.GetDictionaryItem("PaymentConfirmationFailed"), ShopAliasHelper.GetCurrentShopAlias());
                            var failedUrl = string.Format("{0}{1}", baseUrl, library.NiceUrl(failedNodeId));

                            // redirect to payment failed
                            Response.Redirect(failedUrl);
                        }
                    }
                }
            }
        }

        protected bool CheckAccessCode(string AccessPaymentCode)
        {
            var helper = new PaymentConfigHelper(Order.OrderInfo.PaymentInfo.PaymentProviderName);

            //POST to Payment gateway the access code returned
            string strPost = "CustomerID=" + helper.Settings["customerId"];
            strPost += Format("AccessPaymentCode", AccessPaymentCode);
            strPost += Format("UserName", helper.Settings["userName"]);

            string url = helper.Settings["resultGateway"] + "?" + strPost;

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = WebRequestMethods.Http.Get;
            string resultXML = string.Empty;

            try
            {
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();

                //get the response from the transaction generate page
                using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
                {
                    resultXML = sr.ReadToEnd();
                    // Close and clean up the StreamReader
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                Log.Add(LogTypes.Debug, 0, "protected bool CheckAccessCode(string AccessPaymentCode): " + ex.ToString());
                return false;
            }

            //parse the results save the values
            ParseAndSaveXMLResult(resultXML);

            return true;
        }

        /// <summary>
        /// Parse the XML Returned and save all the values to be displayed to user
        /// </summary>
        /// <param name="resultXML">XML of the transaction result</param>
        private void ParseAndSaveXMLResult(string resultXML)
        {
            string _currentNode;
            StringReader _sr = new StringReader(resultXML);
            XmlTextReader _xtr = new XmlTextReader(_sr);
            _xtr.XmlResolver = null;
            _xtr.WhitespaceHandling = WhitespaceHandling.None;

            // get the root node
            _xtr.Read();

            if ((_xtr.NodeType == XmlNodeType.Element) && (_xtr.Name == "TransactionResponse"))
            {
                while (_xtr.Read())
                {
                    if ((_xtr.NodeType == XmlNodeType.Element) && (!_xtr.IsEmptyElement))
                    {
                        _currentNode = _xtr.Name;
                        _xtr.Read();
                        if (_xtr.NodeType == XmlNodeType.Text)
                        {
                            switch (_currentNode)
                            {

                                case "AuthCode":
                                    _AuthCode = _xtr.Value;
                                    break;
                                case "ResponseCode":
                                    _ResponseCode = _xtr.Value;
                                    break;
                                case "ReturnAmount":
                                    _ReturnAmount = _xtr.Value;
                                    break;
                                case "TrxnStatus":
                                    _TrxnStatus = _xtr.Value;
                                    break;
                                case "TrxnNumber":
                                    _TrxnNumber = _xtr.Value;
                                    break;
                                case "MerchantOption1":
                                    _MerchnatOption1 = _xtr.Value;
                                    break;
                                case "MerchantOption2":
                                    _MerchnatOption2 = _xtr.Value;
                                    break;
                                case "MerchantOption3":
                                    _MerchnatOption3 = _xtr.Value;
                                    break;
                                case "MerchantInvoice":
                                    _ReferenceInvoice = _xtr.Value;
                                    break;
                                case "MerchantReference":
                                    _ReferenceNumber = _xtr.Value;
                                    break;

                                case "TrxnResponseMessage":
                                    _TrxnResponseMessage = _xtr.Value;
                                    break;
                                case "ErrorMessage":
                                    _ErrorMessage = _xtr.Value;
                                    break;

                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Format the string needed to post to the Generate page
        /// </summary>
        /// <param name="fieldName">eWAY Parameter Name</param>
        /// <param name="value">Value of Parameter</param>
        /// <returns>Formated value for the URL</returns>
        private string Format(string fieldName, string value)
        {
            if (!string.IsNullOrEmpty(value))
                return "&" + fieldName + "=" + value;
            else
                return "";
        }
    }
}