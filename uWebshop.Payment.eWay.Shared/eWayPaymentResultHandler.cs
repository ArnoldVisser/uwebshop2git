﻿using System;
using System.IO;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Xml;
using uWebshop.Common;
using uWebshop.Domain;
using uWebshop.Domain.Interfaces;
using umbraco;

namespace uWebshop.Payment.eWay.Shared
{
    public class eWayPaymentResultHandler : IPaymentResultHandler
    {
        #region IPaymentResultHandler Members
        public string GetName()
        {
            return "eWay";
        }

        public string GetResultHtml(Domain.Order order)
        {
            return string.Empty;
        }
        #endregion
    }
}
